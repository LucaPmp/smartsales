package it.wind.smartsales.fragments.wireline;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import it.wind.smartsales.R;
import it.wind.smartsales.entities.OffertaFisso;
import it.wind.smartsales.entities.OpzioneFisso;
import it.wind.smartsales.entities.Terminale;

public class WirelineInternetOfferStep4Fragment extends WirelineInnerFragment {


    public WirelineInternetOfferStep4Fragment() {
    }

    public static WirelineInternetOfferStep4Fragment newInstance(OffertaFisso offer, ArrayList<Terminale> terminals, ArrayList<OpzioneFisso> options) {
        WirelineInternetOfferStep4Fragment fragment = new WirelineInternetOfferStep4Fragment();
        fragment.setArguments(createArguments(offer, terminals, options));
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_create_offer_wireline_internet_step_4, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    @Override
    public void enableContinue() {

    }

    @Override
    public int getPosition() {
        return 4;
    }
}