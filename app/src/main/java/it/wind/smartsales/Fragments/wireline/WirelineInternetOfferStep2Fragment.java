package it.wind.smartsales.fragments.wireline;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import it.wind.smartsales.R;
import it.wind.smartsales.adapter.WirelineOfferAdapter;
import it.wind.smartsales.customviews.HorizontalSpaceItemDecoration;
import it.wind.smartsales.dao.CatalogoOfferteFissoDAO;
import it.wind.smartsales.entities.OffertaFisso;

public class WirelineInternetOfferStep2Fragment extends WirelineInnerFragment {

    private WirelineOfferAdapter wsoAdapter;
    private WirelineOfferAdapter allAdapter;
    private Bundle savedState;
    private ArrayList<OffertaFisso> offerteWSO;
    private ArrayList<OffertaFisso> offerteAllInclusive;

    public WirelineInternetOfferStep2Fragment() {
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        savedState = saveState(); /* vstup defined here for sure */
    }


    private Bundle saveState() {
        Bundle outState = new Bundle();
        outState.putSerializable("offerteWSO", offerteWSO);
        outState.putSerializable("offerteAllInclusive", offerteAllInclusive);
        return outState;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_create_offer_wireline_internet_step_2, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        RecyclerView wso = (RecyclerView) view.findViewById(R.id.wireline_offer_wind_smart_offer);
        RecyclerView all = (RecyclerView) view.findViewById(R.id.wireline_offer_all_inclusve);

        final Step2Interaction step2Interaction = new Step2Interaction() {
            @Override
            public void enableContinue() {
                WirelineInternetOfferStep2Fragment.this.enableContinue();
            }

            @Override
            public boolean onOfferSelected(OffertaFisso offerta) {
                return !atLeastOneSelected();
            }

            @Override
            public void openInfo(OffertaFisso offerta) {

            }
        };
        if (savedState != null) {
            offerteWSO = (ArrayList<OffertaFisso>) savedState.getSerializable("offerteWSO");
            offerteAllInclusive = (ArrayList<OffertaFisso>) savedState.getSerializable("offerteAllInclusive");
        } else {
            offerteWSO = CatalogoOfferteFissoDAO.getOfferte(getActivity(), CatalogoOfferteFissoDAO.WIND_SMART_OFFICE);
            offerteAllInclusive = CatalogoOfferteFissoDAO.getOfferte(getActivity(), CatalogoOfferteFissoDAO.ALL_INCLUSIVE);
        }

        wsoAdapter = new WirelineOfferAdapter(offerteWSO, step2Interaction);
        wso.setAdapter(wsoAdapter);
        wso.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        wso.addItemDecoration(new HorizontalSpaceItemDecoration(80));


        allAdapter = new WirelineOfferAdapter(offerteAllInclusive, step2Interaction);
        all.setAdapter(allAdapter);
        all.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        all.addItemDecoration(new HorizontalSpaceItemDecoration(80));

    }

    @Override
    public void enableContinue() {
        if (listener != null) {
            listener.setEnabled(atLeastOneSelected());
        }
    }

    private boolean atLeastOneSelected() {
        return wsoAdapter.atLeastOneSelected() != null || allAdapter.atLeastOneSelected() != null;
    }

    @Override
    public int getPosition() {
        return 2;
    }

    @Override
    public OffertaFisso getOffer() {
        OffertaFisso wso = wsoAdapter.atLeastOneSelected();
        OffertaFisso all = allAdapter.atLeastOneSelected();

        if (wso != null)
            offer = wso;
        if (all != null)
            offer = all;

        return super.getOffer();
    }

    public interface Step2Interaction {

        void enableContinue();

        boolean onOfferSelected(OffertaFisso offerta);

        void openInfo(OffertaFisso offerta);
    }
}