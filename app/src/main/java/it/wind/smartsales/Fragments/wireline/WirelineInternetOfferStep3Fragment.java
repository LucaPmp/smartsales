package it.wind.smartsales.fragments.wireline;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Locale;

import it.wind.smartsales.R;
import it.wind.smartsales.adapter.WirelineOptionAdapter;
import it.wind.smartsales.adapter.WirelineTerminalAdapter;
import it.wind.smartsales.commons.SelectedOnClickListener;
import it.wind.smartsales.customviews.HorizontalSpaceItemDecoration;
import it.wind.smartsales.dao.CatalogoOpzioniFissoDAO;
import it.wind.smartsales.dao.CatalogoTerminaliDAO;
import it.wind.smartsales.entities.OffertaFisso;
import it.wind.smartsales.entities.OpzioneFisso;
import it.wind.smartsales.entities.Terminale;

public class WirelineInternetOfferStep3Fragment extends WirelineInnerFragment {

    private static final int ALL_INCLUSIVE_MIN = 3;
    private static final int OTHER_MIN = 1;
    private static final int MAX = 60;
    private WirelineTerminalAdapter terminalAdapter;
    private WirelineOptionAdapter optionAdapter;

    public WirelineInternetOfferStep3Fragment() {
    }

    public static WirelineInternetOfferStep3Fragment newInstance(OffertaFisso offer) {
        WirelineInternetOfferStep3Fragment fragment = new WirelineInternetOfferStep3Fragment();
        fragment.setArguments(createArguments(offer));
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_create_offer_wireline_internet_step_3, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ((TextView) view.findViewById(R.id.offer_description_1)).setText(getOffer().getOfferDescription());
        ((TextView) view.findViewById(R.id.offer_description_2)).setText(getOffer().getOfferType());
        ((TextView) view.findViewById(R.id.canone)).setText(getOffer().getCanoneMensile());
        final TextView numberCount = (TextView) view.findViewById(R.id.offer_num);
        int min;
        if (getOffer().getOfferType().equalsIgnoreCase("ALL INCLUSIVE AZIENDE"))
            min = ALL_INCLUSIVE_MIN;
        else
            min = OTHER_MIN;
        final int fMin = min;
        numberCount.setText(String.format(Locale.getDefault(), "%03d", min));

        //ALL INCLUSIVE AZIENDE <3 altrimetni <1

        view.findViewById(R.id.offer_arrow_left).setOnClickListener(new SelectedOnClickListener() {
            @Override
            public void performClick(View view) {
                int count = Integer.valueOf(numberCount.getText().toString()) - 1;
                String text = String.format(Locale.getDefault(), "%03d", count > fMin ? count : fMin);
                numberCount.setText(text);
            }
        });

        view.findViewById(R.id.offer_arrow_right).setOnClickListener(new SelectedOnClickListener() {
            @Override
            public void performClick(View view) {
                int count = Integer.valueOf(numberCount.getText().toString()) + 1;
                String text = String.format(Locale.getDefault(), "%03d", count <= MAX ? count : MAX);
                numberCount.setText(text);
            }
        });


        RecyclerView deviceList = (RecyclerView) view.findViewById(R.id.devices_list);
        terminalAdapter = new WirelineTerminalAdapter(new WirelineTerminalAdapter.WirelineTerminalListener() {
            @Override
            public void updateTerminaleNumber(Terminale terminale) {
                addTerminal(terminale);
            }
        }, CatalogoTerminaliDAO.getTerminaliFisso(getActivity(), getOfferId(), getOfferType()));
        deviceList.setAdapter(terminalAdapter);
        deviceList.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        deviceList.addItemDecoration(new HorizontalSpaceItemDecoration(80));

        RecyclerView option = (RecyclerView) view.findViewById(R.id.options_list);
        optionAdapter = new WirelineOptionAdapter(new WirelineOptionAdapter.WirelineOptionListener() {
            @Override
            public void updateOptions(OpzioneFisso terminale) {

            }
        }, CatalogoOpzioniFissoDAO.getOpzioniById(getActivity(), getOfferId()));
        option.setAdapter(optionAdapter);
        option.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));

        final TextView wirelineNumberCount = (TextView) view.findViewById(R.id.wireline_number);
        final View leftArrow = view.findViewById(R.id.wireline_number_arrow_left);
        final View rightArrow = view.findViewById(R.id.wireline_number_arrow_right);
        wirelineNumberCount.setText(String.format(Locale.getDefault(), "%03d", OTHER_MIN));
        leftArrow.setOnClickListener(new SelectedOnClickListener() {
            @Override
            public void performClick(View view) {
                int count = Integer.valueOf(wirelineNumberCount.getText().toString()) - 1;
                wirelineNumberCount.setText(String.format(Locale.getDefault(), "%03d", count > OTHER_MIN ? count : OTHER_MIN));
            }
        });
        rightArrow.setOnClickListener(new SelectedOnClickListener() {
            @Override
            public void performClick(View view) {
                int count = Integer.valueOf(wirelineNumberCount.getText().toString()) + 1;
                wirelineNumberCount.setText(String.format(Locale.getDefault(), "%03d", count <= MAX ? count : MAX));
            }
        });


        final TextView numberCountInternal = (TextView) view.findViewById(R.id.wireline_internal_number);
        final View leftArrowInternal = view.findViewById(R.id.wireline_internal_number_arrow_left);
        final View rightArrowInternal = view.findViewById(R.id.wireline_internal_number_arrow_right);
        numberCountInternal.setText(String.format(Locale.getDefault(), "%03d", OTHER_MIN));
        leftArrowInternal.setOnClickListener(new SelectedOnClickListener() {
            @Override
            public void performClick(View view) {
                int count = Integer.valueOf(numberCountInternal.getText().toString()) - 1;
                numberCountInternal.setText(String.format(Locale.getDefault(), "%03d", count > OTHER_MIN ? count : OTHER_MIN));
            }
        });
        rightArrowInternal.setOnClickListener(new SelectedOnClickListener() {
            @Override
            public void performClick(View view) {
                int count = Integer.valueOf(numberCountInternal.getText().toString()) + 1;
                numberCountInternal.setText(String.format(Locale.getDefault(), "%03d", count <= MAX ? count : MAX));
            }
        });
        enableContinue();
    }

    @Override
    public void enableContinue() {
        listener.setEnabled(true);
    }

    @Override
    public ArrayList<Terminale> getTerminals() {
        return terminalAdapter.getTerminals();
    }

    @Override
    public ArrayList<OpzioneFisso> getOptions() {
        return optionAdapter.getSelectedOptions();
    }

    @Override
    public int getPosition() {
        return 3;
    }

}