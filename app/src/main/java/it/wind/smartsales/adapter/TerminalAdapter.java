package it.wind.smartsales.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;

import java.util.ArrayList;

import it.wind.smartsales.R;
import it.wind.smartsales.SmartSalesApplication;
import it.wind.smartsales.commons.Utils;
import it.wind.smartsales.entities.Terminale;

/**
 * Created by giuseppe.mangino on 14/07/2016.
 */
public class TerminalAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private ArrayList<Terminale> listTerminali;
    private final static int NORMAL = 0;
    private final static int EMPTY = 1;

    public TerminalAdapter(ArrayList<Terminale> listTerminali) {
        this.listTerminali = listTerminali;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v;
        if (viewType == NORMAL) {
            v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_terminal, parent, false);
            return new TerminalViewHolder(v);
        } else {
            v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_terminal_empty, parent, false);
            return new EmptyViewHolder(v);
        }
    }

    @Override
    public int getItemViewType(int position) {
        return listTerminali != null && listTerminali.size() > 0 ? NORMAL : EMPTY;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder extHolder, int position) {
        if (extHolder instanceof EmptyViewHolder)
            return;

        TerminalViewHolder holder = (TerminalViewHolder) extHolder;

        Terminale terminal = listTerminali.get(position);
        final String euroSuffix = holder.brand.getContext().getString(R.string.euro_price_suffix);
        holder.brand.setText(terminal.getTerminaleBrand());
        holder.modello.setText(terminal.getShortDesc());
        holder.priceMonthRateMnp.setText(terminal.getRataMensileMnp() + euroSuffix);
        holder.containerRataMnp.setVisibility(terminal.isRataMnpVisible() ? View.VISIBLE : View.INVISIBLE);

        if (terminal.isPrezzoListinoVisible()) {
            holder.priceMonthRate.setText(terminal.getPrezzoListino() + " €");
            holder.labelMonthRate.setText("Prezzo Listino");
            holder.containerRata.setVisibility(View.VISIBLE);
            holder.containerRataMnp.setVisibility(View.INVISIBLE);
        } else {
            holder.labelMonthRate.setText(holder.brand.getContext().getString(R.string.month_rate));
            holder.priceMonthRate.setText(terminal.getRataMensile() + euroSuffix);
            holder.containerRata.setVisibility(terminal.isRataVisible() ? View.VISIBLE : View.INVISIBLE);
        }

        ImageLoader imgLoader = SmartSalesApplication.getInstance().getImageLoader();
        imgLoader.get(terminal.getImageUrl(), ImageLoader.getImageListener(holder.image,
                Utils.getPlaceholderBasedOnOem(terminal.getOem()), R.drawable.placeholder_altro));
        holder.image.setImageUrl(terminal.getImageUrl(), imgLoader);
    }

    @Override
    public int getItemCount() {
        return listTerminali != null && listTerminali.size() > 0 ? listTerminali.size() : 1;
    }

    public Terminale getItem(int position) {
        return listTerminali.get(position);
    }

    protected void onItemClick(final TerminalViewHolder viewHolder, final View view, final int position) {
    }


    public final class TerminalViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TerminalAdapter adapter = TerminalAdapter.this;
        public LinearLayout containerRata;
        public LinearLayout containerRataMnp;
        public TextView brand;
        public TextView modello;
        public TextView priceMonthRateMnp;
        public TextView priceMonthRate;
        public TextView labelMonthRate;
        public NetworkImageView image;

        public TerminalViewHolder(View v) {
            super(v);
            containerRata = (LinearLayout) v.findViewById(R.id.container_rata);
            containerRataMnp = (LinearLayout) v.findViewById(R.id.container_rata_mnp);
            brand = (TextView) v.findViewById(R.id.brand_title);
            modello = (TextView) v.findViewById(R.id.type_title);
            image = (NetworkImageView) v.findViewById(R.id.image_product);
            priceMonthRateMnp = (TextView) v.findViewById(R.id.price_month_rate_mnp);
            priceMonthRate = (TextView) v.findViewById(R.id.price_month_rate);
            labelMonthRate = (TextView) v.findViewById(R.id.label_month_rate);
        }

        @Override
        public void onClick(View view) {
            adapter.onItemClick(this, view, getAdapterPosition());
        }

    }

    private class EmptyViewHolder extends RecyclerView.ViewHolder {
        public EmptyViewHolder(View v) {
            super(v);
        }
    }
}