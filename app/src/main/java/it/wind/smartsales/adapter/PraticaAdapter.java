package it.wind.smartsales.adapter;

/**
 * Created by luca.pompa on 11/05/2016.
 */

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import it.wind.smartsales.R;
import it.wind.smartsales.customviews.AspectRatioView;
import it.wind.smartsales.dao.PraticheDAO;
import it.wind.smartsales.entities.Pratica;

public class PraticaAdapter extends RecyclerView.Adapter<PraticaAdapter.MyViewHolder> {

    private List<Pratica> praticheList;
    private Context currentContext;

    public PraticaAdapter(List<Pratica> praticasList) {
        this.praticheList = praticasList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.pratica_row, parent, false);

        this.currentContext = parent.getContext();
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Pratica pratica = praticheList.get(position);
        holder.nomePratica.setText(pratica.getNome_pratica() == null ? "PROPOSTA DI CONTRATTO" : pratica.getNome_pratica());

        holder.operationDate.setText(new SimpleDateFormat("dd/MM/yyyy").format(pratica.getLast_update()));
        holder.operationTime.setText(new SimpleDateFormat("HH:mm:ss").format(pratica.getLast_update()));
        if (TextUtils.isEmpty(pratica.getP_iva())) {
            holder.partitaIVALabel.setVisibility(View.GONE);
        }
        holder.partitaIVA.setText(pratica.getP_iva());

        Date nowMinus30 = new Date();
        Calendar c = Calendar.getInstance();
        c.setTime(nowMinus30);
        c.add(Calendar.DAY_OF_YEAR, -30);
        nowMinus30 = c.getTime();

        if (pratica.getLast_update().before(nowMinus30)) {
            holder.imageUrgency.setVisibility(View.VISIBLE);
        } else {
            holder.imageUrgency.setVisibility(View.INVISIBLE);
        }

        holder.percCompletamento.setText(calculatePercentualeCompletamento(pratica));


        Drawable res = null;
        switch (pratica.getStep()) {
            case PraticheDAO.STEP1:
                res = holder.percCompletamentoWidget.getContext().getResources().getDrawable(R.drawable.bg_10);
                break;
            case PraticheDAO.STEP2:
                res = holder.percCompletamentoWidget.getContext().getResources().getDrawable(R.drawable.bg_20);
                break;
            case PraticheDAO.STEP3:
                res = holder.percCompletamentoWidget.getContext().getResources().getDrawable(R.drawable.bg_30);
                break;
            case PraticheDAO.STEP4:
                res = holder.percCompletamentoWidget.getContext().getResources().getDrawable(R.drawable.bg_40);
                break;
            case PraticheDAO.STEP5:
                res = holder.percCompletamentoWidget.getContext().getResources().getDrawable(R.drawable.bg_50);
                break;
            case PraticheDAO.STEP6:
                res = holder.percCompletamentoWidget.getContext().getResources().getDrawable(R.drawable.bg_60);
                break;
            case PraticheDAO.STEP7:
                res = holder.percCompletamentoWidget.getContext().getResources().getDrawable(R.drawable.bg_70);
                break;
            case PraticheDAO.STEP8:
                res = holder.percCompletamentoWidget.getContext().getResources().getDrawable(R.drawable.bg_80);
                break;
            case PraticheDAO.STEP9:
                res = holder.percCompletamentoWidget.getContext().getResources().getDrawable(R.drawable.bg_90);
                break;
            case PraticheDAO.STEP10:
                res = holder.percCompletamentoWidget.getContext().getResources().getDrawable(R.drawable.bg_100);
                break;
        }
        holder.percCompletamentoWidget.setBackground(res);


    }

    private String calculatePercentualeCompletamento(Pratica firstPratica) {
        String result = null;

        switch (firstPratica.getStep()) {
            case PraticheDAO.STEP1:
                result = "10%";
                break;
            case PraticheDAO.STEP2:
                result = "20%";
                break;
            case PraticheDAO.STEP3:
                result = "30%";
                break;
            case PraticheDAO.STEP4:
                result = "40%";
                break;
            case PraticheDAO.STEP5:
                result = "50%";
                break;
            case PraticheDAO.STEP6:
                result = "60%";
                break;
            case PraticheDAO.STEP7:
                result = "70%";
                break;
            case PraticheDAO.STEP8:
                result = "80%";
                break;
            case PraticheDAO.STEP9:
                result = "90%";
                break;
            case PraticheDAO.STEP10:
                result = "100%";
                break;
        }

        return result;
    }

    @Override
    public int getItemCount() {
        return praticheList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView nomePratica, operationTime, operationDate, partitaIVA, partitaIVALabel, percCompletamento;
        public AspectRatioView percCompletamentoWidget, imageUrgency;

        public MyViewHolder(View view) {
            super(view);
            nomePratica = (TextView) view.findViewById(R.id.nomePratica);
            operationTime = (TextView) view.findViewById(R.id.operationTime);
            operationDate = (TextView) view.findViewById(R.id.operationDate);
            partitaIVA = (TextView) view.findViewById(R.id.partitaIVA);
            partitaIVALabel = (TextView) view.findViewById(R.id.partitaIVALabel);
            percCompletamento = (TextView) view.findViewById(R.id.percCompletamento);
            percCompletamentoWidget = (AspectRatioView) view.findViewById(R.id.percCompletamentoWidget);

            imageUrgency = (AspectRatioView) view.findViewById(R.id.imageUrgency);
        }
    }
}