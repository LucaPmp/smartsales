package it.wind.smartsales.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import it.wind.smartsales.R;
import it.wind.smartsales.customviews.AspectRatioView;
import it.wind.smartsales.entities.OffertaFisso;
import it.wind.smartsales.fragments.wireline.WirelineInternetOfferStep2Fragment;

/**
 * Created by a.finocchiaro on 18/07/2016.
 * project: smartsales
 */
public class WirelineOfferAdapter extends RecyclerView.Adapter<WirelineOfferAdapter.ViewHolder> {

    private final List<OffertaFisso> data;
    private WirelineInternetOfferStep2Fragment.Step2Interaction listener;

    public WirelineOfferAdapter(List<OffertaFisso> list, WirelineInternetOfferStep2Fragment.Step2Interaction listener) {
        this.data = list;
        this.listener = listener;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_mobile_offer, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final OffertaFisso offerta = data.get(position);

        holder.item.setSelected(offerta.isSelected());

        holder.offerDescription1.setText(offerta.getOfferDescription());
        holder.offerDescription2.setText(offerta.getOfferDescriptionUpper());
        holder.canone.setText(offerta.getCanoneMensile().replace(".00", "") + "€");

        holder.item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final boolean selected = listener.onOfferSelected(offerta);
                offerta.setSelected(selected);
                v.setSelected(selected);
                listener.enableContinue();
                notifyDataSetChanged();
            }
        });

        holder.infoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.openInfo(offerta);
            }
        });

    }

    public OffertaFisso atLeastOneSelected() {
        for (OffertaFisso offerta : data)
            if (offerta.isSelected())
                return offerta;
        return null;
    }


    @Override
    public int getItemCount() {
        return data != null ? data.size() : 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public LinearLayout item;
        public TextView offerDescription1;
        public TextView offerDescription2;
        public TextView canone;
        public AspectRatioView infoButton;

        public ViewHolder(View v) {
            super(v);
            item = (LinearLayout) v.findViewById(R.id.item_layout);
            offerDescription1 = (TextView) v.findViewById(R.id.offer_description_1);
            offerDescription2 = (TextView) v.findViewById(R.id.offer_description_2);
            canone = (TextView) v.findViewById(R.id.canone);
            infoButton = (AspectRatioView) v.findViewById(R.id.info_button);
        }
    }
}
