package it.wind.smartsales.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import it.wind.smartsales.R;
import it.wind.smartsales.entities.OffertaMobile;
import it.wind.smartsales.entities.Terminale;
import it.wind.smartsales.fragments.DeviceDialogFragment;

/**
 * Created by luca.quaranta on 13/11/2015.
 */
public class DeviceAdapter extends RecyclerView.Adapter<DeviceAdapter.ViewHolder> {

    private static final String DEFAULT_VALUE = "Nessun Valore";

    private ArrayList<Terminale> terminaleArrayList = new ArrayList<>();
    private OffertaMobile offertaMobile;
    private DeviceDialogFragment deviceDialogFragment;

    public DeviceAdapter(DeviceDialogFragment deviceDialogFragment, OffertaMobile offertaMobile, ArrayList<Terminale> terminaleArrayList) {
        this.deviceDialogFragment = deviceDialogFragment;
        this.terminaleArrayList.addAll(terminaleArrayList);
        Terminale terminaleDefault = new Terminale();
        terminaleDefault.setTerminaleBrand("");
        terminaleDefault.setTerminaleDescription(DEFAULT_VALUE);
        this.terminaleArrayList.add(0, terminaleDefault);
        this.offertaMobile = offertaMobile;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_mobile_device, parent, false);
        // set the view's size, margins, paddings and layout parameters

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        final Terminale terminale = terminaleArrayList.get(position);

        holder.label.setText(terminale.getTerminaleBrand() + " " + terminale.getTerminaleDescription());
    }

    @Override
    public int getItemCount() {
        return terminaleArrayList.size();
    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public LinearLayout itemLayout;
        public TextView label;

        public ViewHolder(View v) {
            super(v);
            itemLayout = (LinearLayout) v.findViewById(R.id.item_layout);
            label = (TextView) v.findViewById(R.id.label);

            itemLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (terminaleArrayList.get(getAdapterPosition()).getTerminaleDescription().compareToIgnoreCase(DEFAULT_VALUE) != 0){
                        deviceDialogFragment.onDeviceClicked(terminaleArrayList.get(getAdapterPosition()));
                    } else {
                        deviceDialogFragment.onDeviceClicked(null);
                    }
                }
            });
        }
    }
}
