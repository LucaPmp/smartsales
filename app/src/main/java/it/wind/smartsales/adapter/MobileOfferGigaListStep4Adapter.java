package it.wind.smartsales.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import it.wind.smartsales.R;
import it.wind.smartsales.customviews.AspectRatioView;
import it.wind.smartsales.entities.AddOnMobile;
import it.wind.smartsales.entities.OffertaMobile;
import it.wind.smartsales.fragments.MobileInternetCreateOfferStep4Fragment;
import it.wind.smartsales.fragments.selectoffer.MobileInternetSelectOfferStep4Fragment;

/**
 * Created by luca.quaranta on 13/11/2015.
 */
public class MobileOfferGigaListStep4Adapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    //private ArrayList<OffertaMobile> offerteMobile;
    private ArrayList<String> offerteSelezionate;
    private ArrayList<AddOnMobile> gigaArray;
    private MobileInternetCreateOfferStep4Fragment mobileInternetCreateOfferStep4Fragment;
    private MobileInternetSelectOfferStep4Fragment mobileInternetSelectOfferStep4Fragment;
    private int RIC_SIM;
    private int ABB_SIM;
    private double Ric_tot;
    private double Abb_tot;


    public MobileOfferGigaListStep4Adapter(MobileInternetCreateOfferStep4Fragment mobileInternetCreateOfferStep4Fragment, ArrayList<OffertaMobile> offerteMobile, ArrayList<String> offerteSelezionate, ArrayList<AddOnMobile> gigaArray) {
        this.mobileInternetCreateOfferStep4Fragment = mobileInternetCreateOfferStep4Fragment;
        //this.offerteMobile = offerteMobile;
        this.offerteSelezionate = offerteSelezionate;
        this.gigaArray = gigaArray;
    }
    public MobileOfferGigaListStep4Adapter(MobileInternetSelectOfferStep4Fragment mobileInternetSelectOfferStep4Fragment, ArrayList<OffertaMobile> offerteMobile, ArrayList<String> offerteSelezionate, ArrayList<AddOnMobile> gigaArray) {
        this.mobileInternetSelectOfferStep4Fragment = mobileInternetSelectOfferStep4Fragment;
        //this.offerteMobile = offerteMobile;
        this.offerteSelezionate = offerteSelezionate;
        this.gigaArray = gigaArray;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view

        RecyclerView.ViewHolder vh;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            View v = inflater.inflate(R.layout.row_giga_offerta_sconti, parent, false);
            vh = new ViewHolder1(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
            ViewHolder1 vh1 = (ViewHolder1) holder;
            offertamobilevalori(vh1, position);



    }


    private void offertamobilevalori(final ViewHolder1 holder, final int position ) {
        //final OffertaMobile offertaMobile = offerteMobile.get(position);
        final AddOnMobile gigaAddOn = gigaArray.get(position);

            if(gigaArray.get(position).getSelectedNumSimStep3()!=0) {
                holder.numero_pacchetti_giga_label.setText(String.valueOf(gigaArray.get(position).getSelectedNumSimStep3()));
                holder.nome_pacchetto_giga_label.setText("Pacchetti da " + gigaArray.get(position).getDescFascia());
                holder.scontoUp.setActivated(true);
                holder.scontoUp.setEnabled(true);
                if(gigaArray.get(position).getScontoGigaApplicato().equals("")){
                    gigaArray.get(position).setScontoGigaApplicato("0,00");
                    holder.sconto.setText(gigaArray.get(position).getScontoGigaApplicato()+"€");
                }else{
                    holder.sconto.setText(gigaArray.get(position).getScontoGigaApplicato()+"€");
                }
                holder.scontoDown.setActivated(true);
                holder.scontoDown.setEnabled(true);

                if(gigaArray.get(position).getScontoGigaApplicato().equals("0,00")|| gigaArray.get(position).getScontoGigaApplicato().equals("")) {
                    holder.ok_ko.setActivated(true);
                    holder.ok_ko.setEnabled(true);
                }else{
                    holder.ok_ko.setActivated(false);
                    holder.ok_ko.setEnabled(false);
                }
            }else{
                holder.numero_pacchetti_giga_label.setText(" ");
                holder.nome_pacchetto_giga_label.setVisibility(View.GONE);
                holder.scontoUp.setVisibility(View.GONE);
                holder.sconto.setVisibility(View.GONE);
                holder.ok_ko.setVisibility(View.GONE);
                holder.scontoDown.setVisibility(View.GONE);
            }

        holder.scontoUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String sconto;
                if(mobileInternetCreateOfferStep4Fragment!=null) {
                    sconto = mobileInternetCreateOfferStep4Fragment.incrementScontoGiga(gigaAddOn);
                    mobileInternetCreateOfferStep4Fragment.checkFattibilita();
                }else{
                    sconto = mobileInternetSelectOfferStep4Fragment.incrementScontoGiga(gigaAddOn);
                    mobileInternetSelectOfferStep4Fragment.checkFattibilita();
                }
                holder.sconto.setText(sconto);
                gigaAddOn.setScontoGigaApplicato(sconto);


                // da mettere l'importo dello sconto nella pratica
                // offertaMobile.setSelectedNumSimStep4(mobileInternetCreateOfferStep4Fragment.decrementOfferNumSim(offertaMobile));
                //if (offertaMobile.getSelectedNumSimStep4() == 0){
                //    offertaMobile.setScontoApplicato(offertaMobile.getPercentualeScontoArrayList().get(0).getPercentualeSconto());
                //}
                //offertaMobile.setAutorizzabilita(mobileInternetCreateOfferStep4Fragment.checkAutorization(offertaMobile));
                //holder.autorizzabilita.setText(offertaMobile.getAutorizzabilita());
                //mobileInternetCreateOfferStep4Fragment.checkFattibilita();
                notifyDataSetChanged();
            }
        });

        holder.scontoDown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String sconto;
                if(mobileInternetCreateOfferStep4Fragment!=null){
                    sconto = mobileInternetCreateOfferStep4Fragment.decrementScontoGiga(gigaAddOn);
                    mobileInternetCreateOfferStep4Fragment.checkFattibilita();
                }else{
                    sconto = mobileInternetSelectOfferStep4Fragment.decrementScontoGiga(gigaAddOn);
                    mobileInternetSelectOfferStep4Fragment.checkFattibilita();
                }
                holder.sconto.setText(sconto);
                gigaAddOn.setScontoGigaApplicato(sconto);

                /*offertaMobile.setScontoApplicato(mobileInternetCreateOfferStep4Fragment.incrementSconto(offertaMobile));
                offertaMobile.setAutorizzabilita(mobileInternetCreateOfferStep4Fragment.checkAutorization(offertaMobile));
                holder.autorizzabilita.setText(offertaMobile.getAutorizzabilita());
                mobileInternetCreateOfferStep4Fragment.checkFattibilita();*/
                notifyDataSetChanged();
            }
        });

    }


    public void updateOfferte(ArrayList<String> offerteSelezionate) {
        this.offerteSelezionate = offerteSelezionate;
    }

    @Override
    public int getItemCount() {
        return gigaArray.size();
    }

    @Override
    public int getItemViewType(int i){

       return i;

    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public class ViewHolder1 extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView numero_pacchetti_giga_label;
        public TextView nome_pacchetto_giga_label;
        public TextView sconto;
        public AspectRatioView scontoUp;
        public AspectRatioView scontoDown;
        public AspectRatioView ok_ko;


        public ViewHolder1(View v) {
            super(v);
            numero_pacchetti_giga_label = (TextView) v.findViewById(R.id.numero_pacchetti_giga_label);
            nome_pacchetto_giga_label = (TextView) v.findViewById(R.id.nome_pacchetto_giga_label);
            sconto = (TextView) v.findViewById(R.id.sconto);

            scontoUp = (AspectRatioView) v.findViewById(R.id.sconto_up);
            scontoDown = (AspectRatioView) v.findViewById(R.id.sconto_down);
            ok_ko  = (AspectRatioView) v.findViewById(R.id.icon_ok_ko);
            ok_ko.setActivated(true);
            ok_ko.setEnabled(true);


        }
    }


}
