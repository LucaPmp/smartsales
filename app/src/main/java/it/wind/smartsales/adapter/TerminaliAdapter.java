package it.wind.smartsales.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.util.ArrayList;

import it.wind.smartsales.R;
import it.wind.smartsales.customviews.AspectRatioView;
import it.wind.smartsales.entities.Terminale;
import it.wind.smartsales.entities.TerminaleBean;
import it.wind.smartsales.fragments.MobileInternetCreateOfferStep3Fragment;

/**
 * Created by luca.quaranta on 13/11/2015.
 */
public class TerminaliAdapter extends RecyclerView.Adapter<TerminaliAdapter.ViewHolder> {
    private ArrayList<Terminale> terminali;
    private ArrayList<TerminaleBean> terminaliSelezionati;
    private MobileInternetCreateOfferStep3Fragment mobileInternetCreateOfferStep3Fragment;

    public TerminaliAdapter(MobileInternetCreateOfferStep3Fragment mobileInternetCreateOfferStep3Fragment, ArrayList<Terminale> terminali, ArrayList<TerminaleBean> terminaliSelezionati) {
        this.mobileInternetCreateOfferStep3Fragment = mobileInternetCreateOfferStep3Fragment;
        this.terminali = terminali;
        this.terminaliSelezionati = terminaliSelezionati;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_mobile_terminale, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final Terminale terminale = terminali.get(position);
        TerminaleBean terminaleBean = null;
        for (TerminaleBean terminaleBean1 : terminaliSelezionati) {
            if (terminaleBean1.getIdTerminale().compareToIgnoreCase(terminale.getIdTerminale()) == 0) {
                terminaleBean = terminaleBean1;
                break;
            }
        }

        if (terminaleBean != null) {
            holder.mnpNum.setText(new DecimalFormat("000").format(terminaleBean.getSelectedNumSimStep3Mnp()));
            holder.nuovaLineaNum.setText(new DecimalFormat("000").format(terminaleBean.getSelectedNumSimStep3NuovaLinea()));
            if (terminaleBean.getSelectedNumSimStep3Mnp() == 0 && terminaleBean.getSelectedNumSimStep3NuovaLinea() == 0) {
                holder.itemLayout.setSelected(false);
            } else {
                holder.itemLayout.setSelected(true);
            }
        } else {
            holder.mnpNum.setText(new DecimalFormat("000").format(0));
            holder.nuovaLineaNum.setText(new DecimalFormat("000").format(0));
            holder.itemLayout.setSelected(false);
        }

        holder.oem.setText(terminale.getTerminaleBrand());
        holder.modello.setText(terminale.getTerminaleDescription());
        holder.prezzo.setText(terminale.getRataMensile().replace(".00", "") + "€");

        switch (terminale.getOem()) {
            case "Apple":
                Picasso.with(holder.image.getContext()).load(terminale.getImageUrl()).placeholder(R.drawable.placeholder_apple).into(holder.image);
                break;
            case "Windows Phone":
                Picasso.with(holder.image.getContext()).load(terminale.getImageUrl()).placeholder(R.drawable.placeholder_windows).into(holder.image);
                break;
            case "Samsung":
                Picasso.with(holder.image.getContext()).load(terminale.getImageUrl()).placeholder(R.drawable.placeholder_samsung).into(holder.image);
                break;
            case "Altro":
                Picasso.with(holder.image.getContext()).load(terminale.getImageUrl()).placeholder(R.drawable.placeholder_altro).into(holder.image);
                break;
        }

        holder.mnpArrowUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TerminaleBean terminaleBean2 = null;
                for (TerminaleBean terminaleBean1 : terminaliSelezionati) {
                    if (terminaleBean1.getIdTerminale().compareToIgnoreCase(terminale.getIdTerminale()) == 0) {
                        terminaleBean2 = terminaleBean1;
                        break;
                    }
                }
                if (terminaleBean2 == null) {
                    terminaleBean2 = new TerminaleBean();
                    terminaleBean2.setIdTerminale(terminale.getIdTerminale());
                }
                mobileInternetCreateOfferStep3Fragment.incrementMnpNumSim(terminaleBean2);
                notifyDataSetChanged();
            }
        });

        holder.mnpArrowDown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TerminaleBean terminaleBean2 = null;
                for (TerminaleBean terminaleBean1 : terminaliSelezionati) {
                    if (terminaleBean1.getIdTerminale().compareToIgnoreCase(terminale.getIdTerminale()) == 0) {
                        terminaleBean2 = terminaleBean1;
                        break;
                    }
                }
                if (terminaleBean2 == null) {
                    terminaleBean2 = new TerminaleBean();
                    terminaleBean2.setIdTerminale(terminale.getIdTerminale());
                }
                mobileInternetCreateOfferStep3Fragment.decrementMnpNumSim(terminaleBean2);
                notifyDataSetChanged();
            }
        });

        holder.nuovaLineaArrowUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TerminaleBean terminaleBean2 = null;
                for (TerminaleBean terminaleBean1 : terminaliSelezionati) {
                    if (terminaleBean1.getIdTerminale().compareToIgnoreCase(terminale.getIdTerminale()) == 0) {
                        terminaleBean2 = terminaleBean1;
                        break;
                    }
                }
                if (terminaleBean2 == null) {
                    terminaleBean2 = new TerminaleBean();
                    terminaleBean2.setIdTerminale(terminale.getIdTerminale());
                }
                mobileInternetCreateOfferStep3Fragment.incrementNuovaLineaNumSim(terminaleBean2);
                notifyDataSetChanged();
            }
        });

        holder.nuovaLineaArrowDown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TerminaleBean terminaleBean2 = null;
                for (TerminaleBean terminaleBean1 : terminaliSelezionati) {
                    if (terminaleBean1.getIdTerminale().compareToIgnoreCase(terminale.getIdTerminale()) == 0) {
                        terminaleBean2 = terminaleBean1;
                        break;
                    }
                }
                if (terminaleBean2 == null) {
                    terminaleBean2 = new TerminaleBean();
                    terminaleBean2.setIdTerminale(terminale.getIdTerminale());
                }
                mobileInternetCreateOfferStep3Fragment.decrementNuovaLineaNumSim(terminaleBean2);
                notifyDataSetChanged();
            }
        });

    }

    public void updateTerminaliSelezionati(ArrayList<TerminaleBean> terminaliSelezionati) {
        this.terminaliSelezionati = terminaliSelezionati;
    }

    @Override
    public int getItemCount() {
        return terminali.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public LinearLayout itemLayout;
        public TextView oem;
        public TextView modello;
        public TextView prezzo;
        public AspectRatioView image;
        public TextView mnpNum;
        public AspectRatioView mnpArrowUp;
        public AspectRatioView mnpArrowDown;
        public TextView nuovaLineaNum;
        public AspectRatioView nuovaLineaArrowUp;
        public AspectRatioView nuovaLineaArrowDown;

        public ViewHolder(View v) {
            super(v);
            itemLayout = (LinearLayout) v.findViewById(R.id.item_layout);
            oem = (TextView) v.findViewById(R.id.oem);
            modello = (TextView) v.findViewById(R.id.modello);
            prezzo = (TextView) v.findViewById(R.id.terminale_prezzo);
            image = (AspectRatioView) v.findViewById(R.id.image);
            mnpNum = (TextView) v.findViewById(R.id.terminale_mnp_num);
            mnpArrowUp = (AspectRatioView) v.findViewById(R.id.terminale_mnp_arrow_up);
            mnpArrowDown = (AspectRatioView) v.findViewById(R.id.terminale_mnp_arrow_down);
            nuovaLineaNum = (TextView) v.findViewById(R.id.terminale_nuova_linea_num);
            nuovaLineaArrowUp = (AspectRatioView) v.findViewById(R.id.terminale_nuova_linea_arrow_up);
            nuovaLineaArrowDown = (AspectRatioView) v.findViewById(R.id.terminale_nuova_linea_arrow_down);
        }
    }
}
