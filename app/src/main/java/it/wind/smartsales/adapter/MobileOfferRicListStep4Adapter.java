package it.wind.smartsales.adapter;

import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import it.wind.smartsales.R;
import it.wind.smartsales.customviews.AspectRatioView;
import it.wind.smartsales.entities.OffertaMobile;
import it.wind.smartsales.fragments.MobileInternetCreateOfferStep4Fragment;
import it.wind.smartsales.fragments.selectoffer.MobileInternetSelectOfferStep4Fragment;

/**
 * Created by luca.quaranta on 13/11/2015.
 */
public class MobileOfferRicListStep4Adapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<OffertaMobile> offerteMobile;
    private ArrayList<String> offerteSelezionate;
    private MobileInternetCreateOfferStep4Fragment mobileInternetCreateOfferStep4Fragment;
    private MobileInternetSelectOfferStep4Fragment mobileInternetSelectOfferStep4Fragment;
    private int RIC_SIM;
    private int ABB_SIM;
    private double Ric_tot;
    private double Abb_tot;
    private boolean active = true;
    private boolean statoAutorizzazione = true;

    public MobileOfferRicListStep4Adapter(MobileInternetCreateOfferStep4Fragment mobileInternetCreateOfferStep4Fragment, ArrayList<OffertaMobile> offerteMobile, ArrayList<String> offerteSelezionate) {
        this.mobileInternetCreateOfferStep4Fragment = mobileInternetCreateOfferStep4Fragment;
        this.offerteMobile = offerteMobile;
        this.offerteSelezionate = offerteSelezionate;
    }
    public MobileOfferRicListStep4Adapter(MobileInternetSelectOfferStep4Fragment mobileInternetSelectOfferStep4Fragment, ArrayList<OffertaMobile> offerteMobile, ArrayList<String> offerteSelezionate) {
        this.mobileInternetSelectOfferStep4Fragment = mobileInternetSelectOfferStep4Fragment;
        this.offerteMobile = offerteMobile;
        this.offerteSelezionate = offerteSelezionate;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view

        RecyclerView.ViewHolder vh;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            View v = inflater.inflate(R.layout.row_ricaricabile_offerta_sconti, parent, false);
            vh = new ViewHolder1(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
            ViewHolder1 vh1 = (ViewHolder1) holder;
            offertamobilevalori(vh1, position);



    }


    private void offertamobilevalori(final ViewHolder1 holder, int position ) {
        final OffertaMobile offertaMobile = offerteMobile.get(position);

        if(offertaMobile.getOfferDescriptionUpper().equals("ONLY GIGA")) {
            holder.numero_sim_ricaricabili_label.setText(String.valueOf(offertaMobile.getSelectedNumSimStep3()));
            holder.nome_offerta_ricaricabile_label.setText("SIM " + offertaMobile.getOfferDescriptionUpper());
            holder.scontoUp.setVisibility(View.GONE);
            holder.scontoUp.setVisibility(View.GONE);
            holder.scontoDown.setVisibility(View.GONE);
            holder.scontoDown.setVisibility(View.GONE);
            holder.ok_ko.setVisibility(View.GONE);
            holder.sconto.setVisibility(View.GONE);
        }else if(offertaMobile.getOfferDescriptionUpper().equals("Top Mondo")) {
            holder.numero_sim_ricaricabili_label.setText(String.valueOf(offertaMobile.getSelectedNumSimStep3()));
            holder.nome_offerta_ricaricabile_label.setText("" + offertaMobile.getOfferDescriptionUpper());
            holder.scontoUp.setVisibility(View.GONE);
            holder.scontoUp.setVisibility(View.GONE);
            holder.scontoDown.setVisibility(View.GONE);
            holder.scontoDown.setVisibility(View.GONE);
            holder.ok_ko.setVisibility(View.GONE);
            holder.sconto.setVisibility(View.GONE);
        }else if(offertaMobile.getOfferDescriptionUpper().equals("Premium")) {
            holder.numero_sim_ricaricabili_label.setText(String.valueOf(offertaMobile.getSelectedNumSimStep3()));
            holder.nome_offerta_ricaricabile_label.setText("" + offertaMobile.getOfferDescriptionUpper());
            holder.scontoUp.setVisibility(View.GONE);
            holder.scontoUp.setVisibility(View.GONE);
            holder.scontoDown.setVisibility(View.GONE);
            holder.scontoDown.setVisibility(View.GONE);
            holder.ok_ko.setVisibility(View.GONE);
            holder.sconto.setVisibility(View.GONE);
        }else if(!offertaMobile.getOfferDescriptionUpper().equals("null")) {
            holder.numero_sim_ricaricabili_label.setText(String.valueOf(offertaMobile.getSelectedNumSimStep3()));
            holder.nome_offerta_ricaricabile_label.setText("SIM " + offertaMobile.getOfferDescriptionUpper());
            holder.scontoUp.setActivated(true);
            holder.scontoUp.setEnabled(true);
            holder.scontoDown.setActivated(true);
            holder.scontoDown.setEnabled(true);
            if(offertaMobile.getAutorizzabilita().equals("Pre-Autorizzato")||TextUtils.isEmpty(offertaMobile.getAutorizzabilita())){
                holder.ok_ko.setActivated(true);
                holder.ok_ko.setEnabled(true);
            }else{
                holder.ok_ko.setActivated(false);
                holder.ok_ko.setEnabled(false);
            }
            holder.sconto.setText((offertaMobile.getScontoApplicato()+",00€"));
        }else {
            holder.numero_sim_ricaricabili_label.setText(" ");
            holder.nome_offerta_ricaricabile_label.setVisibility(View.GONE);
            holder.scontoUp.setVisibility(View.GONE);
            holder.sconto.setVisibility(View.GONE);
            holder.ok_ko.setVisibility(View.GONE);
            holder.scontoDown.setVisibility(View.GONE);
        }

        holder.scontoUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mobileInternetCreateOfferStep4Fragment!=null) {
                    holder.sconto.setText(mobileInternetCreateOfferStep4Fragment.incrementScontoRicaricabile(offertaMobile));
                    statoAutorizzazione = mobileInternetCreateOfferStep4Fragment.checkAutorizationRicaricabili(offertaMobile);
                    mobileInternetCreateOfferStep4Fragment.checkFattibilita();
                }else{
                    holder.sconto.setText(mobileInternetSelectOfferStep4Fragment.incrementScontoRicaricabile(offertaMobile));
                    statoAutorizzazione = mobileInternetSelectOfferStep4Fragment.checkAutorizationRicaricabili(offertaMobile);
                    mobileInternetSelectOfferStep4Fragment.checkFattibilita();
                }//holder.sconto.setText(mobileInternetCreateOfferStep4Fragment.incrementScontoRicaricabile(offertaMobile));
                // da mettere l'importo dello sconto nella pratica
                // offertaMobile.setSelectedNumSimStep4(mobileInternetCreateOfferStep4Fragment.decrementOfferNumSim(offertaMobile));
                //if (offertaMobile.getSelectedNumSimStep4() == 0){
                //    offertaMobile.setScontoApplicato(offertaMobile.getPercentualeScontoArrayList().get(0).getPercentualeSconto());
                //}
                //offertaMobile.setAutorizzabilita(mobileInternetCreateOfferStep4Fragment.checkAutorization(offertaMobile));
                //holder.autorizzabilita.setText(offertaMobile.getAutorizzabilita());
                //mobileInternetCreateOfferStep4Fragment.checkFattibilita();
                notifyDataSetChanged();
            }
        });

        holder.scontoDown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mobileInternetCreateOfferStep4Fragment!=null) {
                    holder.sconto.setText(mobileInternetCreateOfferStep4Fragment.decrementScontoRicaricabile(offertaMobile));
                    statoAutorizzazione = mobileInternetCreateOfferStep4Fragment.checkAutorizationRicaricabili(offertaMobile);
                    mobileInternetCreateOfferStep4Fragment.checkFattibilita();
                }else{
                    holder.sconto.setText(mobileInternetSelectOfferStep4Fragment.decrementScontoRicaricabile(offertaMobile));
                    statoAutorizzazione = mobileInternetSelectOfferStep4Fragment.checkAutorizationRicaricabili(offertaMobile);
                    mobileInternetSelectOfferStep4Fragment.checkFattibilita();
                }
                /*offertaMobile.setScontoApplicato(mobileInternetCreateOfferStep4Fragment.incrementSconto(offertaMobile));
                offertaMobile.setAutorizzabilita(mobileInternetCreateOfferStep4Fragment.checkAutorization(offertaMobile));
                holder.autorizzabilita.setText(offertaMobile.getAutorizzabilita());
                mobileInternetCreateOfferStep4Fragment.checkFattibilita();*/
                notifyDataSetChanged();
            }
        });

    }


    public void updateOfferte(ArrayList<String> offerteSelezionate) {
        this.offerteSelezionate = offerteSelezionate;
    }

    @Override
    public int getItemCount() {
        return offerteMobile.size();
    }

    @Override
    public int getItemViewType(int i){

       return i;

    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public class ViewHolder1 extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView numero_sim_ricaricabili_label;
        public TextView nome_offerta_ricaricabile_label;
        public TextView sconto;
        public AspectRatioView scontoUp;
        public AspectRatioView scontoDown;
        public AspectRatioView ok_ko;


        public ViewHolder1(View v) {
            super(v);
            numero_sim_ricaricabili_label = (TextView) v.findViewById(R.id.numero_sim_ricaricabili_label);
            nome_offerta_ricaricabile_label = (TextView) v.findViewById(R.id.nome_offerta_ricaricabile_label);
            sconto = (TextView) v.findViewById(R.id.sconto);

            scontoUp = (AspectRatioView) v.findViewById(R.id.sconto_up);
            scontoDown = (AspectRatioView) v.findViewById(R.id.sconto_down);
            ok_ko  = (AspectRatioView) v.findViewById(R.id.icon_ok_ko);


        }
    }


}
