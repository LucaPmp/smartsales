package it.wind.smartsales.adapter;

import android.app.Activity;
import android.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import it.wind.smartsales.R;
import it.wind.smartsales.commons.SelectedOnClickListener;
import it.wind.smartsales.entities.OpzioneFisso;
import it.wind.smartsales.fragments.wireline.RaggiungimentoSogliaDialogFragment;
import it.wind.smartsales.fragments.wireline.SimIccidDialogFragment;

/**
 * Created by a.finocchiaro on 18/07/2016.
 * project: smartsales
 */
public class WirelineOptionAdapter extends RecyclerView.Adapter<WirelineOptionAdapter.ViewHolder> {

    private final static int MIN = 0;
    private final static int MAX = 60;
    private final List<OpzioneFisso> data;
    private final WirelineOptionListener listener;
    private FragmentManager fm;

    public WirelineOptionAdapter(WirelineOptionListener listener, List<OpzioneFisso> list) {
        this.listener = listener;
        this.data = list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ViewHolder viewHolder = new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_wireline_option, parent, false));
        Activity activity = (Activity) (parent.getContext());
        fm = activity.getFragmentManager();
        return viewHolder;

    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final OpzioneFisso opzione = data.get(position);
        holder.description.setText(opzione.getOpzioneDesc());
        holder.price.setText(holder.price.getContext().getResources().getString(R.string.price_label, opzione.getCanoneMensile()));
        holder.description.setText(opzione.getOpzioneDesc());

        holder.optionFrame.setSelected(opzione.getNumber() > 0);
        holder.number.setText(String.format(Locale.getDefault(), "%03d", opzione.getNumber()));

        holder.left.setOnClickListener(new SelectedOnClickListener() {
            @Override
            public void performClick(final View view) {

                int count = Integer.valueOf(holder.number.getText().toString()) - 1;


                updateOpzione(count, opzione);
            }
        });
        holder.right.setOnClickListener(new SelectedOnClickListener() {
            @Override
            public void performClick(View view) {
                int count = Integer.valueOf(holder.number.getText().toString()) + 1;


                updateOpzione(count, opzione);
            }
        });


        switch (opzione.getButtonType().toUpperCase()) {
            case "SPINNER": {
                holder.switchCompat.setVisibility(View.VISIBLE);
                holder.button.setVisibility(View.GONE);

                holder.switchCompat.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        updateOpzione(view.isSelected(), opzione);
                    }
                });

            }
            break;
            case "BUTTON": {
                holder.switchCompat.setVisibility(View.GONE);
                holder.button.setVisibility(View.VISIBLE);

            }
            break;
            case "Pop Up 1": {
                holder.switchCompat.setVisibility(View.VISIBLE);
                holder.button.setVisibility(View.GONE);

                holder.switchCompat.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(final View view) {
                        SimIccidDialogFragment.newInstance(new SimIccidDialogFragment.SimIccdListener() {
                            @Override
                            public void onResponse(boolean hasDone, String simIccid) {
                                updateOpzione(hasDone, opzione);
                            }
                        }).show(fm, "");
                    }
                });
            }
            break;
            case "Pop Up 2": {
                holder.switchCompat.setVisibility(View.VISIBLE);
                holder.button.setVisibility(View.GONE);

                holder.switchCompat.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(final View view) {
                        RaggiungimentoSogliaDialogFragment.newInstance(new RaggiungimentoSogliaDialogFragment.RaggiungimentoSogliaDialogFragmentListener() {
                            @Override
                            public void onResponse(boolean hasDone, String numero, String email) {
                                updateOpzione(hasDone, opzione);
                            }
                        }).show(fm, "");
                    }
                });
            }
            break;
            default: {
                holder.switchCompat.setVisibility(View.GONE);
                holder.button.setVisibility(View.GONE);
            }
        }
        holder.description.setText(opzione.getOpzioneDesc());
    }

    private void updateOpzione(int count, OpzioneFisso opzione) {
        opzione.setNumber(count > MIN ? count < MAX ? count : MAX : MIN);
        listener.updateOptions(opzione);
        notifyDataSetChanged();
    }

    private void updateOpzione(boolean hasDone, OpzioneFisso opzione) {
        opzione.setNumber(hasDone ? 1 : 0);
        listener.updateOptions(opzione);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return data != null ? data.size() : MIN;
    }

    public ArrayList<OpzioneFisso> getSelectedOptions() {
        ArrayList<OpzioneFisso> result = new ArrayList<>();
        for (OpzioneFisso opzioneFisso : data) {
            if (opzioneFisso.getNumber() > 0)
                result.add(opzioneFisso);
        }
        return result;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public final TextView description;
        public final TextView price;
        public final View optionFrame;
        public final View info;
        public final View left;
        public final TextView number;
        public final View right;
        public final View button;
        public final SwitchCompat switchCompat;

        public ViewHolder(View v) {
            super(v);
            this.description = (TextView) v.findViewById(R.id.adapter_wireline_option_desc);
            this.price = (TextView) v.findViewById(R.id.adapter_wireline_option_price);
            this.optionFrame = v.findViewById(R.id.adapter_wireline_option_frame);
            this.info = v.findViewById(R.id.adapter_wireline_info);
            this.left = v.findViewById(R.id.adapter_wireline_arrow_left);
            this.number = (TextView) v.findViewById(R.id.adapter_wireline_number);
            this.right = v.findViewById(R.id.adapter_wireline_arrow_right);
            this.button = v.findViewById(R.id.counter_row);
            this.switchCompat = (SwitchCompat) v.findViewById(R.id.adapter_wireline_switch);
        }
    }


    public interface WirelineOptionListener {

        void updateOptions(OpzioneFisso terminale);

    }
}