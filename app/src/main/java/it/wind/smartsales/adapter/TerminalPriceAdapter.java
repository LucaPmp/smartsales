package it.wind.smartsales.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import it.wind.smartsales.R;
import it.wind.smartsales.entities.Terminale;

/**
 * Created by giuseppe.mangino on 18/07/2016.
 */
public class TerminalPriceAdapter extends RecyclerView.Adapter<TerminalPriceAdapter.ViewHolder> {
    private ArrayList<Terminale> terminalList;

    public TerminalPriceAdapter(ArrayList<Terminale> terminalList) {
        this.terminalList = terminalList;
    }

    @Override
    public TerminalPriceAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.adapter_terminal_price, parent, false);
        TerminalPriceAdapter.ViewHolder vh = new TerminalPriceAdapter.ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(TerminalPriceAdapter.ViewHolder holder, int position) {
        final Terminale item = terminalList.get(position);
        final String dev = item.getTerminaleBrand() + " " + item.getShortDesc() + " "
                + item.getCapacity() + " - " + item.getColor();
        final String euroSuffix = holder.container.getContext().getString(R.string.euro_price_suffix);

        holder.container.setVisibility(item.getSelectedTerminals() != 0 ? View.VISIBLE : View.GONE);
        holder.container_mnp.setVisibility(item.getSelectedTerminalsMnp() != 0 ? View.VISIBLE : View.GONE);
        holder.price.setText(item.getSelectedTerminals() + " X " + item.getRataMensile() + euroSuffix);
        holder.device.setText(dev);
        holder.device_mnp.setText(dev + " MNP");
        holder.price_mnp.setText(item.getSelectedTerminalsMnp() + " X " + item.getRataMensileMnp() + euroSuffix);
    }

    @Override
    public int getItemCount() {
        return terminalList.size();
    }

    public Terminale getItem(int position) {
        return terminalList.get(position);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public LinearLayout container_mnp;
        public LinearLayout container;
        public TextView device_mnp;
        public TextView price_mnp;
        public TextView device;
        public TextView price;

        public ViewHolder(View v) {
            super(v);
            container_mnp = (LinearLayout) v.findViewById(R.id.container_mnp);
            container = (LinearLayout) v.findViewById(R.id.container);
            device_mnp = (TextView) v.findViewById(R.id.device_mnp);
            price_mnp = (TextView) v.findViewById(R.id.price_mnp);
            device = (TextView) v.findViewById(R.id.device);
            price = (TextView) v.findViewById(R.id.price);
        }
    }
}