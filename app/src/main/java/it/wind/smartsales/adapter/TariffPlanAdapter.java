package it.wind.smartsales.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import it.wind.smartsales.R;
import it.wind.smartsales.commons.Constants;
import it.wind.smartsales.entities.PercentageDiscount;
import it.wind.smartsales.entities.TariffPlan;
import it.wind.smartsales.fragments.ScontiFragment;

/**
 * Created by luca.quaranta on 13/11/2015.
 */
public class TariffPlanAdapter extends RecyclerView.Adapter<TariffPlanAdapter.ViewHolder> {

    private ArrayList<TariffPlan> tariffPlans;
    private ScontiFragment scontiFragment;

    public TariffPlanAdapter(ScontiFragment scontiFragment, ArrayList<TariffPlan> tariffPlans) {
        this.scontiFragment = scontiFragment;
        this.tariffPlans = tariffPlans;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_tariff_plan, parent, false);
        // set the view's size, margins, paddings and layout parameters

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final TariffPlan tariffPlan = tariffPlans.get(position);
//        holder.tariffPlanDescription.setText(tariffPlan.getDescription());

        List<Integer> numSimList = new ArrayList<>();
        for (int i = 0; i<=100; i++){
            numSimList.add(i);
        }
        ArrayAdapter<Integer> numSimDataAdapter = new ArrayAdapter<Integer>(holder.tariffPlanNumSim.getContext(), android.R.layout.simple_spinner_dropdown_item, numSimList);
        numSimDataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        holder.tariffPlanNumSim.setAdapter(numSimDataAdapter);

        List<String> scontoCanoneList = new ArrayList<>();
        for (PercentageDiscount percentageDiscount : tariffPlan.getPercentageDiscounts()){
            scontoCanoneList.add(String.format("%.2f", percentageDiscount.getDiscount()) + "%");
        }
        ArrayAdapter<String> scontoCanoneDataAdapter = new ArrayAdapter<String>(holder.tariffPlanNumSim.getContext(), android.R.layout.simple_spinner_dropdown_item, scontoCanoneList);
        scontoCanoneDataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        holder.tariffPlanScontoCanone.setAdapter(scontoCanoneDataAdapter);

//        holder.tariffPlanCanonePromo.setText(String.format("%.2f", tariffPlan.getPromoFee()));

        tariffPlan.setSelectedDiscountPercentage(tariffPlan.getPercentageDiscounts().get(0).getDiscount());
        tariffPlan.setSelectedNumSim(0);

//        holder.tariffPlanCanoneScontato.setText(String.format("%.2f", tariffPlan.getPromoFee() - (tariffPlan.getGrossFee() * tariffPlan.getSelectedDiscountPercentage() / 100)));

        holder.tariffPlanScontoCanone.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position2, long id) {
                tariffPlan.setSelectedDiscountPercentage(tariffPlan.getPercentageDiscounts().get(position2).getDiscount());
//                holder.tariffPlanCanoneScontato.setText(String.format("%.2f", tariffPlan.getPromoFee() - (tariffPlan.getGrossFee() * tariffPlan.getSelectedDiscountPercentage() / 100)));
                tariffPlan.setSelectedPreAutorizzazione(scontiFragment.checkAutorization(position));
                holder.tariffPlanAutorizzabilita.setText(tariffPlan.getSelectedPreAutorizzazione());

                switch (tariffPlan.getSelectedPreAutorizzazione()){
                    case Constants.TariffPlanPreAutirization.OK:
                        holder.tariffPlanAutorizzabilita.setBackgroundColor((view.getContext()).getResources().getColor(android.R.color.holo_green_light));
                        break;
                    case Constants.TariffPlanPreAutirization.NULL:
                        holder.tariffPlanAutorizzabilita.setBackgroundColor((view.getContext()).getResources().getColor(android.R.color.white));
                        break;
                    case Constants.TariffPlanPreAutirization.OVER_MAX:
                        holder.tariffPlanAutorizzabilita.setBackgroundColor((view.getContext()).getResources().getColor(android.R.color.holo_red_light));
                        break;
                }

                scontiFragment.checkFattibilita();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        holder.tariffPlanNumSim.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position2, long id) {
                tariffPlan.setSelectedNumSim(position2);
                tariffPlan.setSelectedPreAutorizzazione(scontiFragment.checkAutorization(position));
                holder.tariffPlanAutorizzabilita.setText(tariffPlan.getSelectedPreAutorizzazione());

                switch (tariffPlan.getSelectedPreAutorizzazione()){
                    case Constants.TariffPlanPreAutirization.OK:
                        holder.tariffPlanAutorizzabilita.setBackgroundColor((view.getContext()).getResources().getColor(android.R.color.holo_green_light));
                        break;
                    case Constants.TariffPlanPreAutirization.NULL:
                        holder.tariffPlanAutorizzabilita.setBackgroundColor((view.getContext()).getResources().getColor(android.R.color.white));
                        break;
                    case Constants.TariffPlanPreAutirization.OVER_MAX:
                        holder.tariffPlanAutorizzabilita.setBackgroundColor((view.getContext()).getResources().getColor(android.R.color.holo_red_light));
                        break;
                }

                scontiFragment.checkFattibilita();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    @Override
    public int getItemCount() {
        return tariffPlans.size();
    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView tariffPlanDescription, tariffPlanCanonePromo, tariffPlanCanoneScontato, tariffPlanAutorizzabilita;
        public Spinner tariffPlanNumSim, tariffPlanScontoCanone;

        public ViewHolder(View v) {
            super(v);
            tariffPlanDescription = (TextView) v.findViewById(R.id.tariff_plan_description);
            tariffPlanNumSim = (Spinner) v.findViewById(R.id.tariff_plan_num_sim);
            tariffPlanScontoCanone = (Spinner) v.findViewById(R.id.tariff_plan_sconto_canone);
            tariffPlanCanonePromo = (TextView) v.findViewById(R.id.tariff_plan_canone_promo);
            tariffPlanCanoneScontato = (TextView) v.findViewById(R.id.tariff_plan_canone_scontato);
            tariffPlanAutorizzabilita = (TextView) v.findViewById(R.id.tariff_plan_autorizzabilita);
        }
    }
}
