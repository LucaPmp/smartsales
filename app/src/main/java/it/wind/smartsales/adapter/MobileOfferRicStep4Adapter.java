package it.wind.smartsales.adapter;

import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.ArrayList;

import it.wind.smartsales.R;
import it.wind.smartsales.customviews.AspectRatioView;
import it.wind.smartsales.customviews.VerticalSpaceItemDecoration;
import it.wind.smartsales.entities.OffertaMobile;
import it.wind.smartsales.fragments.MobileInternetCreateOfferStep4Fragment;
import it.wind.smartsales.fragments.selectoffer.MobileInternetSelectOfferStep4Fragment;

/**
 * Created by luca.quaranta on 13/11/2015.
 */
public class MobileOfferRicStep4Adapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<OffertaMobile> offerteMobile;
    private ArrayList<String> offerteSelezionate;
    private MobileInternetCreateOfferStep4Fragment mobileInternetCreateOfferStep4Fragment;
    private MobileInternetSelectOfferStep4Fragment mobileInternetSelectOfferStep4Fragment;
    private RecyclerView.Adapter list_Ricaricabili;
    private RecyclerView.Adapter list_Giga;
    private int RIC_SIM;
    private int ABB_SIM;
    private float ric_tot;
    private float giga_tot;
    private float abb_tot;
    private RecyclerView.LayoutManager mLayoutManagerMobileOffer;
    private boolean mostraRicGiga;

    public MobileOfferRicStep4Adapter(MobileInternetCreateOfferStep4Fragment mobileInternetCreateOfferStep4Fragment, ArrayList<OffertaMobile> offerteMobile, ArrayList<String> offerteSelezionate, RecyclerView.Adapter list_Ricaricabili, RecyclerView.Adapter list_Giga, boolean mostraRicGiga) {
        this.mobileInternetCreateOfferStep4Fragment = mobileInternetCreateOfferStep4Fragment;
        this.offerteMobile = offerteMobile;
        this.offerteSelezionate = offerteSelezionate;
        this.list_Ricaricabili = list_Ricaricabili;
        this.list_Giga = list_Giga;
        this.mLayoutManagerMobileOffer = mLayoutManagerMobileOffer;
        this.mostraRicGiga = mostraRicGiga;
    }
    public MobileOfferRicStep4Adapter(MobileInternetSelectOfferStep4Fragment mobileInternetSelectOfferStep4Fragment, ArrayList<OffertaMobile> offerteMobile, ArrayList<String> offerteSelezionate, RecyclerView.Adapter list_Ricaricabili, RecyclerView.Adapter list_Giga) {
        this.mobileInternetSelectOfferStep4Fragment = mobileInternetSelectOfferStep4Fragment;
        this.offerteMobile = offerteMobile;
        this.offerteSelezionate = offerteSelezionate;
        this.list_Ricaricabili = list_Ricaricabili;
        this.list_Giga = list_Giga;
        this.mLayoutManagerMobileOffer = mLayoutManagerMobileOffer;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view

        RecyclerView.ViewHolder vh = null;
        if(mostraRicGiga) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            if (viewType < offerteMobile.size() - 2) {
                View v = inflater.inflate(R.layout.list_item_mobile_offer_step_4, parent, false);
                vh = new ViewHolder1(v);

            } else if (viewType == offerteMobile.size() - 2) {
                View v1 = inflater.inflate(R.layout.list_item_mobile_ricaricabile_offer_step_4, parent, false);
                vh = new ViewHolder2(v1);

            } else if (viewType == offerteMobile.size() - 1) {
                View v2 = inflater.inflate(R.layout.list_item_mobile_giga_offer_step_4, parent, false);
                vh = new ViewHolder3(v2);

            }

            return vh;
        }else{
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());

                View v = inflater.inflate(R.layout.list_item_mobile_offer_step_4, parent, false);
                vh = new ViewHolder1(v);
                return vh;
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        if(mostraRicGiga) {
            if (position < offerteMobile.size() - 2) {
                ViewHolder1 vh1 = (ViewHolder1) holder;
                offertamobilevalori(vh1, position);
            }
            //MW popola la lista del riepilogo SIM Rica.
            if (position == offerteMobile.size() - 2) {
                ViewHolder2 vh2 = (ViewHolder2) holder;
                offertamobilevalori1(vh2, position);
            }
            //MW popola la lista del riepilogo GIGA.
            if (position == offerteMobile.size() - 1) {
                ViewHolder3 vh3 = (ViewHolder3) holder;
                offertamobilevalori2(vh3, position);
            }
        }else{
            ViewHolder1 vh1 = (ViewHolder1) holder;
            offertamobilevalori(vh1, position);
        }

    }


    private void offertamobilevalori(final ViewHolder1 holder, int position ) {
        final OffertaMobile offertaMobile = offerteMobile.get(position);

        holder.offerDescription.setText(offertaMobile.getOfferDescription());
        holder.offerDescriptionUpper.setText(offertaMobile.getOfferDescriptionUpper());

        holder.numSim.setText(new DecimalFormat("000").format(offertaMobile.getSelectedNumSimStep4()));

        if (!offertaMobile.getPercentualeScontoArrayList().isEmpty()||offertaMobile.getIdOfferta()==11) {
            holder.sconto.setText(String.format("%.2f", Float.valueOf(offertaMobile.getScontoApplicato().replace(",", "."))) + "%");
            //holder.autorizzabilita.setText(mobileInternetCreateOfferStep4Fragment.checkAutorization(offertaMobile));
            holder.autorizzabilita.setVisibility(View.GONE);
            if(offertaMobile.getAutorizzabilita().equals("Pre-Autorizzato")) {
                holder.ok_ko.setActivated(true);
                holder.ok_ko.setEnabled(true);
            }else{
                holder.ok_ko.setActivated(false);
                holder.ok_ko.setEnabled(false);
            }
        }

        if (TextUtils.isEmpty(offertaMobile.getPercentualeScontoPenale())){
            holder.scontoPenale.setBackground(holder.scontoPenale.getContext().getResources().getDrawable(R.drawable.sconto_penale_disabled));
            holder.scontoPenale.setTextColor(holder.scontoPenale.getContext().getResources().getColor(android.R.color.darker_gray));
            holder.scontoPenale.setEnabled(false);
        } else {
            if (!offertaMobile.isScontoPenaleApplicato()) {
                holder.scontoPenale.setBackground(holder.scontoPenale.getContext().getResources().getDrawable(R.drawable.sconto_penale_not_selected));
                holder.scontoPenale.setTextColor(holder.scontoPenale.getContext().getResources().getColor(R.color.colorAccent));
            } else {
                holder.scontoPenale.setBackground(holder.scontoPenale.getContext().getResources().getDrawable(R.drawable.sconto_penale_selected));
                holder.scontoPenale.setTextColor(holder.scontoPenale.getContext().getResources().getColor(android.R.color.white));
            }
            holder.scontoPenale.setEnabled(true);
        }

        //MW nella if che segue controllo il piano Abbonamento START (idOfferta =  11 perhe questo piano non deve avere possibilita di fare sconti.
        if (!TextUtils.isEmpty(offertaMobile.getCanonePromo()) && offertaMobile.getIdOfferta()!=11) {
            holder.canonePromo.setText(String.format("%.2f", Float.valueOf(offertaMobile.getCanonePromo().replace(",", "."))) + "€");
            holder.canoneScontato.setText(String.format("%.2f",(Float.parseFloat(offertaMobile.getCanoneScontato().replace(",", ".")))) + "€");
            holder.numSimArrowUp.setActivated(true);
            holder.numSimArrowUp.setEnabled(true);
            holder.numSimArrowDown.setActivated(true);
            holder.numSimArrowDown.setEnabled(true);
            holder.scontoArrowUp.setActivated(true);
            holder.scontoArrowUp.setEnabled(true);
            holder.scontoArrowDown.setActivated(true);
            holder.scontoArrowDown.setEnabled(true);
            holder.numSim.setActivated(true);
            holder.sconto.setActivated(true);
        } else {
            holder.canonePromo.setText(String.format("%.2f", Float.valueOf(offertaMobile.getCanoneMensile().replace(",", "."))) + "€");
            holder.canoneScontato.setText(String.format("%.2f", Float.valueOf(offertaMobile.getCanoneScontato().replace(",", "."))) + "€");
            holder.numSimArrowUp.setActivated(false);
            holder.numSimArrowUp.setEnabled(false);
            holder.numSimArrowDown.setActivated(false);
            holder.numSimArrowDown.setEnabled(false);
            holder.scontoArrowUp.setActivated(false);
            holder.scontoArrowUp.setEnabled(false);
            holder.scontoArrowDown.setActivated(false);
            holder.scontoArrowDown.setEnabled(false);
            holder.numSim.setActivated(false);
            holder.sconto.setActivated(false);
            holder.autorizzabilita.setVisibility(View.GONE);
        }

        holder.numSimArrowUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mobileInternetCreateOfferStep4Fragment!=null) {
                    offertaMobile.setSelectedNumSimStep4(mobileInternetCreateOfferStep4Fragment.incrementOfferNumSim(offertaMobile));
                    offertaMobile.setAutorizzabilita(mobileInternetCreateOfferStep4Fragment.checkAutorization(offertaMobile));
                }else{
                    offertaMobile.setSelectedNumSimStep4(mobileInternetSelectOfferStep4Fragment.incrementOfferNumSim(offertaMobile));
                    offertaMobile.setAutorizzabilita(mobileInternetSelectOfferStep4Fragment.checkAutorization(offertaMobile));
                }
                holder.autorizzabilita.setText(offertaMobile.getAutorizzabilita());
                if(mobileInternetCreateOfferStep4Fragment!=null) {
                    mobileInternetCreateOfferStep4Fragment.checkFattibilita();
                }else{
                    mobileInternetSelectOfferStep4Fragment.checkFattibilita();
                }
                notifyDataSetChanged();
            }
        });

        holder.numSimArrowDown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mobileInternetCreateOfferStep4Fragment!=null) {
                    offertaMobile.setSelectedNumSimStep4(mobileInternetCreateOfferStep4Fragment.decrementOfferNumSim(offertaMobile));
                }else{
                    offertaMobile.setSelectedNumSimStep4(mobileInternetSelectOfferStep4Fragment.decrementOfferNumSim(offertaMobile));
                }
                if (offertaMobile.getSelectedNumSimStep4() == 0){
                    offertaMobile.setScontoApplicato(offertaMobile.getPercentualeScontoArrayList().get(0).getPercentualeSconto());
                }
                if(mobileInternetCreateOfferStep4Fragment!=null) {
                    offertaMobile.setAutorizzabilita(mobileInternetCreateOfferStep4Fragment.checkAutorization(offertaMobile));
                }else{
                    offertaMobile.setAutorizzabilita(mobileInternetSelectOfferStep4Fragment.checkAutorization(offertaMobile));
                }
                holder.autorizzabilita.setText(offertaMobile.getAutorizzabilita());
                if(mobileInternetCreateOfferStep4Fragment!=null) {
                    mobileInternetCreateOfferStep4Fragment.checkFattibilita();
                }else{
                    mobileInternetSelectOfferStep4Fragment.checkFattibilita();
                }
                notifyDataSetChanged();
            }
        });

        holder.scontoArrowUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mobileInternetCreateOfferStep4Fragment!=null) {
                    offertaMobile.setScontoApplicato(mobileInternetCreateOfferStep4Fragment.incrementSconto(offertaMobile));
                    offertaMobile.setAutorizzabilita(mobileInternetCreateOfferStep4Fragment.checkAutorization(offertaMobile));
                }else{
                    offertaMobile.setScontoApplicato(mobileInternetSelectOfferStep4Fragment.incrementSconto(offertaMobile));
                    offertaMobile.setAutorizzabilita(mobileInternetSelectOfferStep4Fragment.checkAutorization(offertaMobile));
                }
                holder.autorizzabilita.setText(offertaMobile.getAutorizzabilita());
                if(mobileInternetCreateOfferStep4Fragment!=null) {
                    mobileInternetCreateOfferStep4Fragment.checkFattibilita();
                }else{
                    mobileInternetSelectOfferStep4Fragment.checkFattibilita();
                }
                notifyDataSetChanged();
            }
        });

        holder.scontoArrowDown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mobileInternetCreateOfferStep4Fragment!=null) {
                    offertaMobile.setScontoApplicato(mobileInternetCreateOfferStep4Fragment.decrementSconto(offertaMobile));
                    offertaMobile.setAutorizzabilita(mobileInternetCreateOfferStep4Fragment.checkAutorization(offertaMobile));
                }else{
                    offertaMobile.setScontoApplicato(mobileInternetSelectOfferStep4Fragment.decrementSconto(offertaMobile));
                    offertaMobile.setAutorizzabilita(mobileInternetSelectOfferStep4Fragment.checkAutorization(offertaMobile));
                }
                holder.autorizzabilita.setText(offertaMobile.getAutorizzabilita());
                if(mobileInternetCreateOfferStep4Fragment!=null) {
                    mobileInternetCreateOfferStep4Fragment.checkFattibilita();
                }else{
                    mobileInternetSelectOfferStep4Fragment.checkFattibilita();
                }
                notifyDataSetChanged();
            }
        });

        holder.scontoPenale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mobileInternetCreateOfferStep4Fragment!=null) {
                    offertaMobile.setScontoPenaleApplicato(mobileInternetCreateOfferStep4Fragment.calcolaScontoPenale(offertaMobile));
                }else{
                    offertaMobile.setScontoPenaleApplicato(mobileInternetSelectOfferStep4Fragment.calcolaScontoPenale(offertaMobile));
                }
                if(mobileInternetCreateOfferStep4Fragment!=null) {
                    mobileInternetCreateOfferStep4Fragment.checkFattibilita();
                    mobileInternetCreateOfferStep4Fragment.saveObjects();
                }else{
                    mobileInternetSelectOfferStep4Fragment.checkFattibilita();
                    mobileInternetSelectOfferStep4Fragment.saveObjects();
                }
                notifyDataSetChanged();
            }
        });

    }

    private void offertamobilevalori1(final ViewHolder2 holder, int position) {

        final OffertaMobile offertaMobile = offerteMobile.get(position);
        if(mobileInternetCreateOfferStep4Fragment!=null) {
            ric_tot = mobileInternetCreateOfferStep4Fragment.spesapromoRicTot;
        }else {
            ric_tot = mobileInternetSelectOfferStep4Fragment.spesapromoRicTot;
        }
        holder.canonePromo.setText(String.format("%.2f", Float.valueOf(String.valueOf(ric_tot).replace(",", "."))) + "€");
        holder.offerDescription.setText("Piani Voce");
        holder.offerDescriptionUpper.setText("Ricaricabili");
        holder.ricList.setAdapter(list_Ricaricabili);

    }
    private void offertamobilevalori2(final ViewHolder3 holder, int position) {
        final OffertaMobile offertaMobile = offerteMobile.get(position);
        if(mobileInternetCreateOfferStep4Fragment!=null) {
            giga_tot = mobileInternetCreateOfferStep4Fragment.gigaAcquistati;
        }else{
            giga_tot = mobileInternetSelectOfferStep4Fragment.gigaAcquistati;
        }
        holder.canonePromo.setText(String.format("%.2f", Float.valueOf(String.valueOf(giga_tot).replace(",", "."))) + "€");
        holder.offerDescription.setText("Pacchetti");
        holder.offerDescriptionUpper.setText("GIGA SHARE");
        holder.gigaList.setAdapter(list_Giga);
    }


    public void updateOfferte(ArrayList<String> offerteSelezionate) {
        this.offerteSelezionate = offerteSelezionate;
    }

    @Override
    public int getItemCount() {
        int i = offerteMobile.size();
        i=i++;
        return i;
    }

    @Override
    public int getItemViewType(int i){

       return i;

    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public class ViewHolder1 extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView offerDescription;
        public TextView offerDescriptionUpper;
        public TextView canonePromo;
        public TextView numSim;
        public TextView sconto;
        public TextView canoneScontato;
        public AspectRatioView numSimArrowUp;
        public AspectRatioView numSimArrowDown;
        public AspectRatioView scontoArrowUp;
        public AspectRatioView scontoArrowDown;
        public AspectRatioView ok_ko;
        public TextView autorizzabilita;
        public TextView scontoPenale;


        public ViewHolder1(View v) {
            super(v);
            offerDescription = (TextView) v.findViewById(R.id.offer_description);
            offerDescriptionUpper = (TextView) v.findViewById(R.id.offer_description_upper);
            canonePromo = (TextView) v.findViewById(R.id.canone_promo);
            numSim = (TextView) v.findViewById(R.id.num_sim);
            sconto = (TextView) v.findViewById(R.id.sconto);
            canoneScontato = (TextView) v.findViewById(R.id.canone_scontato);
            numSimArrowUp = (AspectRatioView) v.findViewById(R.id.num_sim_arrow_up);
            numSimArrowDown = (AspectRatioView) v.findViewById(R.id.num_sim_arrow_down);
            scontoArrowUp = (AspectRatioView) v.findViewById(R.id.sconto_arrow_up);
            scontoArrowDown = (AspectRatioView) v.findViewById(R.id.sconto_arrow_down);
            autorizzabilita = (TextView) v.findViewById(R.id.autorizzabilita);
            scontoPenale = (TextView) v.findViewById(R.id.sconto_penale);
            ok_ko = (AspectRatioView) v.findViewById(R.id.icon_ok_ko);
        }
    }
    public class ViewHolder2 extends RecyclerView.ViewHolder {
            // each data item is just a string in this case
            public TextView offerDescription;
            public TextView offerDescriptionUpper;
            public TextView canonePromo;
            public RecyclerView ricList;
            //public TextView numSim;
            //public TextView sconto;
            //public TextView canoneScontato;
            //public AspectRatioView numSimArrowUp;
            //public AspectRatioView numSimArrowDown;
            //public AspectRatioView scontoArrowUp;
            //public AspectRatioView scontoArrowDown;
            //public TextView autorizzabilita;
            //public TextView scontoPenale;

            public ViewHolder2(View v) {
                super(v);

                offerDescription = (TextView) v.findViewById(R.id.offer_description);
                offerDescriptionUpper = (TextView) v.findViewById(R.id.offer_description_upper);
                canonePromo = (TextView) v.findViewById(R.id.canone_promo);
                ricList = (RecyclerView) v.findViewById(R.id.offerta_ricaricabili_sim_label_recycler);
                RecyclerView.LayoutManager l = new LinearLayoutManager(null,LinearLayoutManager.VERTICAL,false);
                ricList.setLayoutManager(l);
                ricList.setItemAnimator(new DefaultItemAnimator());
                ricList.addItemDecoration(new VerticalSpaceItemDecoration(4));

                //numSim = (TextView) v.findViewById(R.id.num_sim);
                //sconto = (TextView) v.findViewById(R.id.sconto);
                //canoneScontato = (TextView) v.findViewById(R.id.canone_scontato);
                //numSimArrowUp = (AspectRatioView) v.findViewById(R.id.num_sim_arrow_up);
                //numSimArrowDown = (AspectRatioView) v.findViewById(R.id.num_sim_arrow_down);
                //scontoArrowUp = (AspectRatioView) v.findViewById(R.id.sconto_arrow_up);
                //scontoArrowDown = (AspectRatioView) v.findViewById(R.id.sconto_arrow_down);
                //autorizzabilita = (TextView) v.findViewById(R.id.autorizzabilita);
                //scontoPenale = (TextView) v.findViewById(R.id.sconto_penale);
            }
        }
    public class ViewHolder3 extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView offerDescription;
        public TextView offerDescriptionUpper;
        public TextView canonePromo;
        public RecyclerView gigaList;
        //public TextView numSim;
        //public TextView sconto;
        //public TextView canoneScontato;
        //public AspectRatioView numSimArrowUp;
        //public AspectRatioView numSimArrowDown;
        //public AspectRatioView scontoArrowUp;
        //public AspectRatioView scontoArrowDown;
        //public TextView autorizzabilita;
        //public TextView scontoPenale;

        public ViewHolder3(View v) {
            super(v);

            offerDescription = (TextView) v.findViewById(R.id.offer_description);
            offerDescriptionUpper = (TextView) v.findViewById(R.id.offer_description_upper);
            canonePromo = (TextView) v.findViewById(R.id.canone_promo);
            gigaList = (RecyclerView) v.findViewById(R.id.offerta_ricaricabili_giga_label_recycler);
            RecyclerView.LayoutManager l = new LinearLayoutManager(null,LinearLayoutManager.VERTICAL,false);
            gigaList.setLayoutManager(l);
            gigaList.setItemAnimator(new DefaultItemAnimator());
            gigaList.addItemDecoration(new VerticalSpaceItemDecoration(4));
            //numSim = (TextView) v.findViewById(R.id.num_sim);
            //sconto = (TextView) v.findViewById(R.id.sconto);
            //canoneScontato = (TextView) v.findViewById(R.id.canone_scontato);
            //numSimArrowUp = (AspectRatioView) v.findViewById(R.id.num_sim_arrow_up);
            //numSimArrowDown = (AspectRatioView) v.findViewById(R.id.num_sim_arrow_down);
            //scontoArrowUp = (AspectRatioView) v.findViewById(R.id.sconto_arrow_up);
            //scontoArrowDown = (AspectRatioView) v.findViewById(R.id.sconto_arrow_down);
            //autorizzabilita = (TextView) v.findViewById(R.id.autorizzabilita);
            //scontoPenale = (TextView) v.findViewById(R.id.sconto_penale);
        }
    }

}
