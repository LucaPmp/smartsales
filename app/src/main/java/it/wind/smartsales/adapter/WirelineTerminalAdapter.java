package it.wind.smartsales.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;

import java.util.ArrayList;
import java.util.Locale;

import it.wind.smartsales.R;
import it.wind.smartsales.SmartSalesApplication;
import it.wind.smartsales.commons.SelectedOnClickListener;
import it.wind.smartsales.commons.Utils;
import it.wind.smartsales.entities.Terminale;
import it.wind.smartsales.fragments.TerminalClickedFragment;

/**
 * Created by a.finocchiaro on 20/07/2016.
 * Wind smartsales
 */
public class WirelineTerminalAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements TerminalClickedFragment.OnUpdateDeviceListener {
    private static final int MIN = 0;
    private static final int MAX = 60;
    private final static int NORMAL = 0;
    private final static int EMPTY = 1;
    private final WirelineTerminalListener listener;
    private ArrayList<Terminale> listTerminali;

    public WirelineTerminalAdapter(WirelineTerminalListener listener, ArrayList<Terminale> listTerminali) {
        this.listTerminali = listTerminali;
        this.listener = listener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v;
        if (viewType == NORMAL) {
            v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_wireline_terminal, parent, false);
            return new TerminalViewHolder(v);
        } else {
            v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_wireline_terminal_empty, parent, false);
            return new EmptyViewHolder(v);
        }
    }

    @Override
    public int getItemViewType(int position) {
        return listTerminali != null && listTerminali.size() > 0 ? NORMAL : EMPTY;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder extHolder, int position) {
        if (extHolder instanceof EmptyViewHolder)
            return;
        final TerminalViewHolder holder = (TerminalViewHolder) extHolder;
        final Terminale terminal = listTerminali.get(position);
        holder.brand.setText(terminal.getTerminaleBrand());
        holder.modello.setText(terminal.getShortDesc());

        holder.itemView.setSelected(terminal.getSelectedTerminals() > 0);
        holder.count.setText(String.format(Locale.getDefault(), "%03d", terminal.getSelectedTerminals()));
        holder.leftArrow.setOnClickListener(new SelectedOnClickListener() {
            @Override
            public void performClick(View view) {
                int count = Integer.valueOf(holder.count.getText().toString()) - 1;
                count = count > MIN ? count <= MAX ? count : MAX : MIN;
                terminal.setSelectedTerminals(count);
                listener.updateTerminaleNumber(terminal);
                notifyDataSetChanged();
            }
        });
        holder.rightArrow.setOnClickListener(new SelectedOnClickListener() {
            @Override
            public void performClick(View view) {
                int count = Integer.valueOf(holder.count.getText().toString()) + 1;
                count = count > MIN ? count <= MAX ? count : MAX : MIN;
                terminal.setSelectedTerminals(count);
                listener.updateTerminaleNumber(terminal);
                notifyDataSetChanged();
            }
        });

        ImageLoader imgLoader = SmartSalesApplication.getInstance().getImageLoader();
        imgLoader.get(terminal.getImageUrl(), ImageLoader.getImageListener(holder.image,
                Utils.getPlaceholderBasedOnOem(terminal.getOem()), R.drawable.placeholder_altro));
        holder.image.setImageUrl(terminal.getImageUrl(), imgLoader);
    }

    @Override
    public int getItemCount() {
        return listTerminali != null && listTerminali.size() > 0 ? listTerminali.size() : 1;
    }

    @Override
    public void onUpdateDevice(Terminale terminal) {

    }

    public ArrayList<Terminale> getTerminals() {
        ArrayList<Terminale> result = new ArrayList<>();
        for (Terminale terminale : listTerminali) {
            if (terminale.getSelectedTerminals() > 0)
                result.add(terminale);
        }
        return result;
    }

    public static class TerminalViewHolder extends RecyclerView.ViewHolder {
        public TextView brand;
        public TextView modello;
        public NetworkImageView image;
        public TextView count;
        public View leftArrow;
        public View rightArrow;

        public TerminalViewHolder(View v) {
            super(v);
            brand = (TextView) v.findViewById(R.id.brand_title);
            modello = (TextView) v.findViewById(R.id.type_title);
            image = (NetworkImageView) v.findViewById(R.id.image_product);
            count = (TextView) v.findViewById(R.id.wireline_number);
            leftArrow = v.findViewById(R.id.wireline_number_arrow_left);
            rightArrow = v.findViewById(R.id.wireline_number_arrow_right);
        }
    }

    private class EmptyViewHolder extends RecyclerView.ViewHolder {
        public EmptyViewHolder(View v) {
            super(v);
        }
    }


    public interface WirelineTerminalListener {

        void updateTerminaleNumber(Terminale terminale);

    }
}