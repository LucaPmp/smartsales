package it.wind.smartsales.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.ArrayList;

import it.wind.smartsales.R;
import it.wind.smartsales.customviews.AspectRatioView;
import it.wind.smartsales.entities.ExtraGBDetail;
import it.wind.smartsales.entities.Session;
import it.wind.smartsales.fragments.MobileInternetCreateOfferStep8Fragment;

/**
 * Created by luca.quaranta on 13/11/2015.
 */
public class ExtraGigaDetailAdapter extends RecyclerView.Adapter<ExtraGigaDetailAdapter.ViewHolder> {

    private ArrayList<ExtraGBDetail> extraGigaDetailList;
//    private ArrayList<ExtraGBDetail> extraGigaDetailListSelected;
    private int valuePosition;
    private ExtraGBValueAdapter valueAdapter;

    private MobileInternetCreateOfferStep8Fragment mobileInternetCreateOfferStep8Fragment;

    public ExtraGigaDetailAdapter(MobileInternetCreateOfferStep8Fragment mobileInternetCreateOfferStep8Fragment, ArrayList<ExtraGBDetail> list, int position, ExtraGBValueAdapter valueAdapter) {
        this.mobileInternetCreateOfferStep8Fragment = mobileInternetCreateOfferStep8Fragment;
        this.extraGigaDetailList = list;
        this.valuePosition=position;
        this.valueAdapter = valueAdapter;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_extra_giga_detail_altern, parent, false);
        // set the view's size, margins, paddings and layout parameters

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    private int getTotNumberSelected()
    {
        int totCount=0;
        for (ExtraGBDetail extraGBDetail : extraGigaDetailList) {
            totCount+=extraGBDetail.getValue();
        }
        return totCount;
    }


    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        final ExtraGBDetail currElem = extraGigaDetailList.get(position);


//        for (AddOnMobile addOnMobile1 : addOnMobileSelezionati) {
//            if (addOnMobile1.getDescFascia().compareToIgnoreCase(addOnMobile.getDescFascia()) == 0) {
//                if (addOnMobile1.getSelectedNumSimStep3() > 0){
//                    holder.addOn.setSelected(true);
//                } else {
//                    holder.addOn.setSelected(false);
//                }
//                holder.addOnNum.setText(new DecimalFormat("000").format(addOnMobile1.getSelectedNumSimStep3()));
//                break;
//            }
//        }

        holder.addOnNum.setText(new DecimalFormat("00").format(currElem.getValue()));
//        holder.addOnNum.setText(new DecimalFormat("00").format(0));

        if (currElem.getValue() > 0){
                    holder.addOn.setSelected(true);
                } else {
                    holder.addOn.setSelected(false);
                }

//        if (addOnMobileSelezionati.isEmpty()){
//            holder.addOn.setSelected(false);
//            holder.addOnNum.setText(new DecimalFormat("000").format(0));
//        }

        holder.letterText.setText(currElem.getLetter());

//        holder.addOnNum.setText(new DecimalFormat("000").format(addOnMobile.getSelectedNumSimStep3()));

        holder.arrowUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (currElem.getValue() < currElem.getMaxValue()) {

                    currElem.setValue((currElem.getValue() + 1));

                    updateValues(position, currElem.getValue());
                }
            }
        });

        holder.arrowDown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(currElem.getValue()>=1)
                {
                    currElem.setValue((currElem.getValue() - 1));

                    updateValues(position, currElem.getValue());
                }

            }
        });

    }

    private void updateValues(int detailPosition, int value)
    {
        if (getTotNumberSelected() > 0) {
            valueAdapter.turnOn(valuePosition);
        } else {
            valueAdapter.turnOff(valuePosition);
        }

//        mobileInternetCreateOfferStep8Fragment.replaceExtraGigaDetail(valuePosition,extraGigaDetailList);
        Session.getInstance().updateExtraGiga(valuePosition,detailPosition,value);

        notifyDataSetChanged();
    }

    public void updateAddOnMobileSelezionati(ArrayList<ExtraGBDetail> selezionati) {
//        this.extraGigaDetailListSelected= selezionati;
    }

    @Override
    public int getItemCount() {
        return extraGigaDetailList.size();
    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public LinearLayout addOn;
        public TextView letterText;

        public TextView addOnNum;
        public AspectRatioView arrowUp;
        public AspectRatioView arrowDown;

        public ViewHolder(View v) {
            super(v);
            addOn = (LinearLayout) v.findViewById(R.id.extra_detail);
            letterText = (TextView) v.findViewById(R.id.extra_detail_desc_fascia);

            addOnNum = (TextView) v.findViewById(R.id.extra_detail_num);
            arrowUp = (AspectRatioView) v.findViewById(R.id.extra_detail_arrow_up);
            arrowDown = (AspectRatioView) v.findViewById(R.id.extra_detail_arrow_down);
        }
    }
}
