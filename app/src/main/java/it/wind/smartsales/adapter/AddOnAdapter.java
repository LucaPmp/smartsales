package it.wind.smartsales.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.ArrayList;

import it.wind.smartsales.R;
import it.wind.smartsales.customviews.AspectRatioView;
import it.wind.smartsales.entities.AddOnMobile;
import it.wind.smartsales.fragments.MobileInternetCreateOfferStep3Fragment;

/**
 * Created by luca.quaranta on 13/11/2015.
 */
public class AddOnAdapter extends RecyclerView.Adapter<AddOnAdapter.ViewHolder> {

    private ArrayList<AddOnMobile> addOnMobileList;
    private ArrayList<AddOnMobile> addOnMobileSelezionati;
    private MobileInternetCreateOfferStep3Fragment mobileInternetCreateOfferStep3Fragment;

    public AddOnAdapter(MobileInternetCreateOfferStep3Fragment mobileInternetCreateOfferStep3Fragment, ArrayList<AddOnMobile> addOnMobileList, ArrayList<AddOnMobile> addOnMobileSelezionati) {
        this.mobileInternetCreateOfferStep3Fragment = mobileInternetCreateOfferStep3Fragment;
        this.addOnMobileList = addOnMobileList;
        this.addOnMobileSelezionati = addOnMobileSelezionati;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_mobile_add_on, parent, false);
        // set the view's size, margins, paddings and layout parameters

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        final AddOnMobile addOnMobile = addOnMobileList.get(position);


        for (AddOnMobile addOnMobile1 : addOnMobileSelezionati) {
            if (addOnMobile1.getDescFascia().compareToIgnoreCase(addOnMobile.getDescFascia()) == 0) {
                if (addOnMobile1.getSelectedNumSimStep3() > 0){
                    holder.addOn.setSelected(true);
                } else {
                    holder.addOn.setSelected(false);
                }
                holder.addOnNum.setText(new DecimalFormat("000").format(addOnMobile1.getSelectedNumSimStep3()));
                break;
            }
        }

        if (addOnMobileSelezionati.isEmpty()){
            holder.addOn.setSelected(false);
            holder.addOnNum.setText(new DecimalFormat("000").format(0));
        }

        holder.addOnDescFascia.setText(addOnMobile.getDescFascia());
        holder.addOnPrezzoFascia.setText(addOnMobile.getPrezzoFascia() + "€");
//        holder.addOnNum.setText(new DecimalFormat("000").format(addOnMobile.getSelectedNumSimStep3()));

        holder.addOnArrowUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addOnMobile.setSelectedNumSimStep3(mobileInternetCreateOfferStep3Fragment.incrementAddOnMobileNumSim(addOnMobile));
                notifyDataSetChanged();
            }
        });

        holder.addOnArrowDown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addOnMobile.setSelectedNumSimStep3(mobileInternetCreateOfferStep3Fragment.decrementAddOnMobileNumSim(addOnMobile));
                notifyDataSetChanged();
            }
        });

    }

    public void updateAddOnMobileSelezionati(ArrayList<AddOnMobile> addOnMobileSelezionati) {
        this.addOnMobileSelezionati = addOnMobileSelezionati;
    }

    @Override
    public int getItemCount() {
        return addOnMobileList.size();
    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public LinearLayout addOn;
        public TextView addOnDescFascia;
        public TextView addOnPrezzoFascia;
        public TextView addOnNum;
        public AspectRatioView addOnArrowUp;
        public AspectRatioView addOnArrowDown;

        public ViewHolder(View v) {
            super(v);
            addOn = (LinearLayout) v.findViewById(R.id.add_on);
            addOnDescFascia = (TextView) v.findViewById(R.id.add_on_desc_fascia);
            addOnPrezzoFascia = (TextView) v.findViewById(R.id.add_on_prezzo_fascia);
            addOnNum = (TextView) v.findViewById(R.id.add_on_num);
            addOnArrowUp = (AspectRatioView) v.findViewById(R.id.add_on_arrow_up);
            addOnArrowDown = (AspectRatioView) v.findViewById(R.id.add_on_arrow_down);
        }
    }
}
