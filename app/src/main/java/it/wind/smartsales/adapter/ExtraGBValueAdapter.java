package it.wind.smartsales.adapter;

/**
 * Created by luca.pompa on 11/05/2016.
 */

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import it.wind.smartsales.R;
import it.wind.smartsales.entities.ExtraGBDetail;
import it.wind.smartsales.entities.ExtraGBValue;
import it.wind.smartsales.entities.Session;
import it.wind.smartsales.fragments.ExtraGbDialogFragment;

public class ExtraGBValueAdapter extends RecyclerView.Adapter<ExtraGBValueAdapter.MyViewHolder> {

    private final ExtraGbDialogFragment parentFragment;



    private List<ExtraGBValue> valueList;
    private Context currentContext;
    private ArrayList<ArrayList<ExtraGBDetail>> extraGigaDetail;
    private boolean noGbSelected;

    public ExtraGBValueAdapter(List<ExtraGBValue> valueList, ExtraGbDialogFragment parentFragment,ArrayList<ArrayList<ExtraGBDetail>> extraGigaDetail,boolean noGbSelected) {
        this.valueList = valueList;
        this.parentFragment = parentFragment;
        this.extraGigaDetail = extraGigaDetail;
        this.noGbSelected = noGbSelected;

        for (int i = 0; i < this.valueList.size(); i++) {

            int totNumValues=0;
            for (ExtraGBDetail extraGBDetail : this.extraGigaDetail.get(i)) {
                totNumValues+=extraGBDetail.getValue();
            }

            this.valueList.get(i).setTotDetail(totNumValues);
        }


    }

    public List<ExtraGBValue> getValueList() {
        return valueList;
    }

    @Override
    public int getItemViewType(int position) {



        if(extraGigaDetail.get(position).size()!=0) {
            return R.layout.extra_bundle_giga_row;
        }
        else
        {
            return 0;
        }

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.extra_bundle_giga_row, parent, false);

        this.currentContext = parent.getContext();
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        final ExtraGBValue extraGBValue = valueList.get(position);

        if(extraGigaDetail.get(position).size()!=0) {


            holder.extraGBValueText.setText(String.valueOf(extraGBValue.getValue()) + " GB");

            holder.extraGBValueButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                currElem.setValue((currElem.getValue() + 1));
                    noGbSelected=false;
                    parentFragment.swapDetailAdapter(position, String.valueOf(extraGBValue.getValue()) + " GB");
                    notifyDataSetChanged();

//                notifyDataSetChanged();
                }
            });
            if(noGbSelected){
                holder.extraGBValueButton.setSelected(true);
                noGbSelected=false;
            }else {
                if (extraGBValue.getTotDetail() > 0) {
                    holder.extraGBValueButton.setSelected(true);
                } else {
                    holder.extraGBValueButton.setSelected(false);
                }
            }
        }
        else
        {

            ViewGroup.LayoutParams params = holder.extraGBValueButton.getLayoutParams();
            // Changes the height and width to the specified *pixels*
            params.height = 0;
            params.width = params.width;
            holder.extraGBValueButton.setLayoutParams(params);
            holder.extraGBValueButton.setVisibility(View.GONE);
        }

    }

    public void turnOn(int position)
    {
        valueList.get(position).setTotDetail(1);
        Session.getInstance().turnOnExtraGigaValues(position);
        notifyDataSetChanged();
    }

    public void turnOff(int position)
    {
        valueList.get(position).setTotDetail(0);
        Session.getInstance().turnOffExtraGigaValues(position);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return valueList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView extraGBValueText;
        public LinearLayout extraGBValueButton;


        public MyViewHolder(View view) {
            super(view);

            extraGBValueText = (TextView) view.findViewById(R.id.dialog_extra_giga_label);
            extraGBValueButton = ((LinearLayout) view.findViewById(R.id.dialog_extra_giga_button));
            extraGBValueButton.setActivated(true);

        }
    }
}