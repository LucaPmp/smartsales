package it.wind.smartsales.adapter;

import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.ArrayList;

import it.wind.smartsales.R;
import it.wind.smartsales.customviews.AspectRatioView;
import it.wind.smartsales.entities.OffertaMobile;
import it.wind.smartsales.fragments.MobileInternetCreateOfferStep4Fragment;

/**
 * Created by luca.quaranta on 13/11/2015.
 */
public class MobileOfferStep4Adapter extends RecyclerView.Adapter<MobileOfferStep4Adapter.ViewHolder> {

    private ArrayList<OffertaMobile> offerteMobile;
    private ArrayList<String> offerteSelezionate;
    private MobileInternetCreateOfferStep4Fragment mobileInternetCreateOfferStep4Fragment;

    public MobileOfferStep4Adapter(MobileInternetCreateOfferStep4Fragment mobileInternetCreateOfferStep4Fragment, ArrayList<OffertaMobile> offerteMobile, ArrayList<String> offerteSelezionate) {
        this.mobileInternetCreateOfferStep4Fragment = mobileInternetCreateOfferStep4Fragment;
        this.offerteMobile = offerteMobile;
        this.offerteSelezionate = offerteSelezionate;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view

            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_mobile_offer_step_4, parent, false);


            ViewHolder vh = new ViewHolder(v);

            return vh;

    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        final OffertaMobile offertaMobile = offerteMobile.get(position);

        holder.offerDescription.setText(offertaMobile.getOfferDescription());
        holder.offerDescriptionUpper.setText(offertaMobile.getOfferDescriptionUpper());

        holder.numSim.setText(new DecimalFormat("000").format(offertaMobile.getSelectedNumSimStep4()));
        if (!offertaMobile.getPercentualeScontoArrayList().isEmpty()) {
            holder.sconto.setText(String.format("%.2f", Float.valueOf(offertaMobile.getScontoApplicato().replace(",", "."))) + "%");
            holder.autorizzabilita.setText(mobileInternetCreateOfferStep4Fragment.checkAutorization(offertaMobile));
        }

        if (TextUtils.isEmpty(offertaMobile.getPercentualeScontoPenale())){
            holder.scontoPenale.setBackground(holder.scontoPenale.getContext().getResources().getDrawable(R.drawable.sconto_penale_disabled));
            holder.scontoPenale.setTextColor(holder.scontoPenale.getContext().getResources().getColor(android.R.color.darker_gray));
            holder.scontoPenale.setEnabled(false);
        } else {
            if (!offertaMobile.isScontoPenaleApplicato()) {
                holder.scontoPenale.setBackground(holder.scontoPenale.getContext().getResources().getDrawable(R.drawable.sconto_penale_not_selected));
                holder.scontoPenale.setTextColor(holder.scontoPenale.getContext().getResources().getColor(R.color.colorAccent));
            } else {
                holder.scontoPenale.setBackground(holder.scontoPenale.getContext().getResources().getDrawable(R.drawable.sconto_penale_selected));
                holder.scontoPenale.setTextColor(holder.scontoPenale.getContext().getResources().getColor(android.R.color.white));
            }
            holder.scontoPenale.setEnabled(true);
        }

        if (!TextUtils.isEmpty(offertaMobile.getCanonePromo())) {
            holder.canonePromo.setText(String.format("%.2f", Float.valueOf(offertaMobile.getCanonePromo().replace(",", "."))) + "€");
            holder.canoneScontato.setText(String.format("%.2f",(Float.parseFloat(offertaMobile.getCanoneScontato().replace(",", ".")))) + "€");
            holder.numSimArrowUp.setActivated(true);
            holder.numSimArrowUp.setEnabled(true);
            holder.numSimArrowDown.setActivated(true);
            holder.numSimArrowDown.setEnabled(true);
            holder.scontoArrowUp.setActivated(true);
            holder.scontoArrowUp.setEnabled(true);
            holder.scontoArrowDown.setActivated(true);
            holder.scontoArrowDown.setEnabled(true);
            holder.numSim.setActivated(true);
            holder.sconto.setActivated(true);
        } else {
            holder.canonePromo.setText(String.format("%.2f", Float.valueOf(offertaMobile.getCanoneMensile().replace(",", "."))) + "€");
            holder.canoneScontato.setText(String.format("%.2f", Float.valueOf(offertaMobile.getCanoneScontato().replace(",", "."))) + "€");
            holder.numSimArrowUp.setActivated(false);
            holder.numSimArrowUp.setEnabled(false);
            holder.numSimArrowDown.setActivated(false);
            holder.numSimArrowDown.setEnabled(false);
            holder.scontoArrowUp.setActivated(false);
            holder.scontoArrowUp.setEnabled(false);
            holder.scontoArrowDown.setActivated(false);
            holder.scontoArrowDown.setEnabled(false);
            holder.numSim.setActivated(false);
            holder.sconto.setActivated(false);
            holder.autorizzabilita.setVisibility(View.GONE);
        }

        holder.numSimArrowUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                offertaMobile.setSelectedNumSimStep4(mobileInternetCreateOfferStep4Fragment.incrementOfferNumSim(offertaMobile));
                offertaMobile.setAutorizzabilita(mobileInternetCreateOfferStep4Fragment.checkAutorization(offertaMobile));
                holder.autorizzabilita.setText(offertaMobile.getAutorizzabilita());
                mobileInternetCreateOfferStep4Fragment.checkFattibilita();
                notifyDataSetChanged();
            }
        });

        holder.numSimArrowDown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                offertaMobile.setSelectedNumSimStep4(mobileInternetCreateOfferStep4Fragment.decrementOfferNumSim(offertaMobile));
                if (offertaMobile.getSelectedNumSimStep4() == 0){
                    offertaMobile.setScontoApplicato(offertaMobile.getPercentualeScontoArrayList().get(0).getPercentualeSconto());
                }
                offertaMobile.setAutorizzabilita(mobileInternetCreateOfferStep4Fragment.checkAutorization(offertaMobile));
                holder.autorizzabilita.setText(offertaMobile.getAutorizzabilita());
                mobileInternetCreateOfferStep4Fragment.checkFattibilita();
                notifyDataSetChanged();
            }
        });

        holder.scontoArrowUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                offertaMobile.setScontoApplicato(mobileInternetCreateOfferStep4Fragment.incrementSconto(offertaMobile));
                offertaMobile.setAutorizzabilita(mobileInternetCreateOfferStep4Fragment.checkAutorization(offertaMobile));
                holder.autorizzabilita.setText(offertaMobile.getAutorizzabilita());
                mobileInternetCreateOfferStep4Fragment.checkFattibilita();
                notifyDataSetChanged();
            }
        });

        holder.scontoArrowDown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                offertaMobile.setScontoApplicato(mobileInternetCreateOfferStep4Fragment.decrementSconto(offertaMobile));
                offertaMobile.setAutorizzabilita(mobileInternetCreateOfferStep4Fragment.checkAutorization(offertaMobile));
                holder.autorizzabilita.setText(offertaMobile.getAutorizzabilita());
                mobileInternetCreateOfferStep4Fragment.checkFattibilita();
                notifyDataSetChanged();
            }
        });

        holder.scontoPenale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                offertaMobile.setScontoPenaleApplicato(mobileInternetCreateOfferStep4Fragment.calcolaScontoPenale(offertaMobile));
                mobileInternetCreateOfferStep4Fragment.checkFattibilita();
                mobileInternetCreateOfferStep4Fragment.saveObjects();
                notifyDataSetChanged();
            }
        });

    }

    public void updateOfferte(ArrayList<String> offerteSelezionate) {
        this.offerteSelezionate = offerteSelezionate;
    }

    @Override
    public int getItemCount() {
        return offerteMobile.size();
    }


    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView offerDescription;
        public TextView offerDescriptionUpper;
        public TextView canonePromo;
        public TextView numSim;
        public TextView sconto;
        public TextView canoneScontato;
        public AspectRatioView numSimArrowUp;
        public AspectRatioView numSimArrowDown;
        public AspectRatioView scontoArrowUp;
        public AspectRatioView scontoArrowDown;
        public TextView autorizzabilita;
        public TextView scontoPenale;

        public ViewHolder(View v) {
            super(v);
            offerDescription = (TextView) v.findViewById(R.id.offer_description);
            offerDescriptionUpper = (TextView) v.findViewById(R.id.offer_description_upper);
            canonePromo = (TextView) v.findViewById(R.id.canone_promo);
            numSim = (TextView) v.findViewById(R.id.num_sim);
            sconto = (TextView) v.findViewById(R.id.sconto);
            canoneScontato = (TextView) v.findViewById(R.id.canone_scontato);
            numSimArrowUp = (AspectRatioView) v.findViewById(R.id.num_sim_arrow_up);
            numSimArrowDown = (AspectRatioView) v.findViewById(R.id.num_sim_arrow_down);
            scontoArrowUp = (AspectRatioView) v.findViewById(R.id.sconto_arrow_up);
            scontoArrowDown = (AspectRatioView) v.findViewById(R.id.sconto_arrow_down);
            autorizzabilita = (TextView) v.findViewById(R.id.autorizzabilita);
            scontoPenale = (TextView) v.findViewById(R.id.sconto_penale);
        }
    }

}
