package it.wind.smartsales.fragments;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;

import it.wind.smartsales.R;
import it.wind.smartsales.activity.MainActivity;
import it.wind.smartsales.commons.Constants;
import it.wind.smartsales.entities.Session;

//import it.wind.smartsales.Fragments.MobileInternetCreateOfferStep4Fragment;

/**
 * Created by luca.quaranta on 03/12/2015.
 */
public class GoHomeDialogFragment extends DialogFragment {

    public static final String KEY = "MESSAGE_KEY";
    private String note=null;


    private Button indietroButton, continuaButton;
//    private MobileInternetCreateOfferStep4Fragment mobileInternetCreateOfferStep4Fragment;
    private EditText noteText;
    private SharedPreferences prefs;
    private CheckBox rememberMeCheck;


    public GoHomeDialogFragment() {
    }

    private void initializeComponents(View view) {

        prefs = getActivity().getSharedPreferences("SmartSalesSharedPref", Context.MODE_PRIVATE);
        indietroButton = (Button) view.findViewById(R.id.indietro_button_note);
        continuaButton = (Button) view.findViewById(R.id.continua_button_note);
        noteText = (EditText) view.findViewById(R.id.note_text);
        rememberMeCheck = (CheckBox) view.findViewById(R.id.remember_check_go_home);
        this.setCancelable(false);
    }

    /*public GoHomeDialogFragment(String note) {
        this.note=note;
    }*/

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    @Override
    public void onResume() {
        super.onResume();
        int width = getResources().getDimensionPixelSize(R.dimen.fragment_dialog_mail_width);
        getDialog().getWindow().setLayout(width, ViewGroup.LayoutParams.WRAP_CONTENT);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_dialog_go_home, container, false);

        initializeComponents(view);

        indietroButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        continuaButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (rememberMeCheck.isChecked()) {
                    prefs.edit().putBoolean(Session.getInstance().getDealer().getDealerCode(), true).apply();
                } else {
                    prefs.edit().putBoolean(Session.getInstance().getDealer().getDealerCode(), false).apply();
                }

                ((MainActivity)getActivity()).goToPage(Constants.Pages.PAGE_HOME_ADMIN);


                dismiss();
            }
        });



        return view;
    }


}
