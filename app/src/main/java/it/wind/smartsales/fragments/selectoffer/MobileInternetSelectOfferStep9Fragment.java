package it.wind.smartsales.fragments.selectoffer;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.hardware.SensorManager;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.OrientationEventListener;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import it.wind.smartsales.customviews.AspectRatioView;
import it.wind.smartsales.entities.OffertaMobile;
import it.wind.smartsales.entities.OpzioneMobile;
import it.wind.smartsales.entities.Session;
import it.wind.smartsales.R;
import it.wind.smartsales.task.PreviewTask;
import it.wind.smartsales.commons.Constants;
import it.wind.smartsales.commons.FileExplore;
import it.wind.smartsales.commons.Log;
import it.wind.smartsales.commons.Utils;

/**
 * Created by luca.quaranta on 10/02/2016.
 */
public class MobileInternetSelectOfferStep9Fragment extends Fragment implements Handler.Callback {

    private MobileInternetSelectOfferStep9Fragment mobileInternetCreateOfferStep9Fragment = this;
    private LinearLayout documentoRappresentanteLegale, delegaRappresentanteLegale, documentoDelegato, fatturaOperatore, aggiungiDocumento;
    private Button previewButton, invioButton, procediButton;
    private AspectRatioView fatturaOperatoreImage;
    private TextView fatturaOperatoreText;

    private View.OnClickListener documentClickListener;

    private View viewClicked;

    private ArrayList<OffertaMobile> offertaMobileArrayList;

    boolean isPreview, isSendMail, isFirma;

    private void initializeComponents(View view) {
        documentoRappresentanteLegale = (LinearLayout) view.findViewById(R.id.documento_rappresentante_legale);
        delegaRappresentanteLegale = (LinearLayout) view.findViewById(R.id.delega_rappresentante_legale);
        documentoDelegato = (LinearLayout) view.findViewById(R.id.documento_delegato);
        fatturaOperatore = (LinearLayout) view.findViewById(R.id.fattura_operatore);
        aggiungiDocumento = (LinearLayout) view.findViewById(R.id.aggiungi_documento);

        fatturaOperatoreImage = (AspectRatioView) view.findViewById(R.id.fattura_operatore_image);
        fatturaOperatoreText = (TextView) view.findViewById(R.id.fattura_operatore_text);

        previewButton = (Button) view.findViewById(R.id.preview_button);
        invioButton = (Button) view.findViewById(R.id.invio_button);
        procediButton = (Button) view.findViewById(R.id.procedi_button);

        documentClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewClicked = v;
                showGenericMessage(getActivity(), "Allega Documento", "Scegli come allegare una foto.");
            }
        };

        documentoRappresentanteLegale.setOnClickListener(documentClickListener);
        delegaRappresentanteLegale.setOnClickListener(documentClickListener);
        documentoDelegato.setOnClickListener(documentClickListener);
        fatturaOperatore.setOnClickListener(documentClickListener);
        aggiungiDocumento.setOnClickListener(documentClickListener);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_mobile_internet_create_offer_step_9, container, false);
        initializeComponents(view);

        try {
            JSONArray offerteRaggruppate = (JSONArray) Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONArray("offerteConfigurate");
            offertaMobileArrayList = new ArrayList<>();
            for (int i = 0; i < offerteRaggruppate.length(); i++) {
                offertaMobileArrayList.add(Utils.createOffertaMobile(offerteRaggruppate.getJSONObject(i)));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (!hasMnp()) {
            fatturaOperatore.setEnabled(false);
            fatturaOperatoreImage.setEnabled(false);
            fatturaOperatoreText.setEnabled(false);
        }

        previewButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isPreview = true;
                isSendMail = false;
                isFirma = false;
                String xml = generateXML();
                PreviewTask.callTask(getActivity(), mobileInternetCreateOfferStep9Fragment, xml);
            }
        });

        invioButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isPreview = false;
                isSendMail = true;
                isFirma = false;
            }
        });

        procediButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isPreview = false;
                isSendMail = false;
                isFirma = true;
            }
        });

        return view;
    }

    int rotation = 0;

    private void changeRot(int r) {
        rotation = r;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == 5) {
            if (resultCode == Activity.RESULT_OK) {
                OrientationEventListener myOrientationEventListener = new OrientationEventListener(getActivity(), SensorManager.SENSOR_DELAY_NORMAL) {

                    @Override
                    public void onOrientationChanged(int r) {
                        // TODO Auto-generated method stub
                        changeRot(r);
                    }
                };

                if (myOrientationEventListener.canDetectOrientation()) {
                    myOrientationEventListener.enable();
                } else {
                    myOrientationEventListener.disable();
                }


                Bitmap bitmapToConvert = BitmapFactory.decodeFile(path);
                ExifInterface exif;
                try {
                    exif = new ExifInterface(path);

                    int exifOrientation = exif.getAttributeInt(
                            ExifInterface.TAG_ORIENTATION,
                            ExifInterface.ORIENTATION_NORMAL);

                    int rotate = 0;

                    switch (exifOrientation) {
                        case ExifInterface.ORIENTATION_ROTATE_90:
                            rotate = 90;
                            break;
                        case ExifInterface.ORIENTATION_ROTATE_180:
                            rotate = 180;
                            break;
                        case ExifInterface.ORIENTATION_ROTATE_270:
                            rotate = 270;
                            break;
                    }

                    if (rotate != 0) {
                        int w = bitmapToConvert.getWidth();
                        int h = bitmapToConvert.getHeight();

                        // Setting pre rotate
                        Matrix mtx = new Matrix();
                        mtx.preRotate(rotate);

                        // Rotating Bitmap & convert to ARGB_8888
                        bitmapToConvert = Bitmap.createBitmap(bitmapToConvert, 0, 0, w, h, mtx, false);
                        bitmapToConvert = bitmapToConvert.copy(Bitmap.Config.ARGB_8888, true);
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                }

                Bitmap convertedBitmap = convertColorImageBlackAndWhite(bitmapToConvert);


                FileOutputStream fos;
                try {
                    fos = new FileOutputStream(path);
                    if (convertedBitmap != null) {
                        convertedBitmap.compress(Bitmap.CompressFormat.JPEG, 70, fos);
                    }


                    fos.flush();
                    fos.close();

//                    dbHelper.saveAttachment(javaInterface.getAttachmentCustomerId(),javaInterface.getAttachmentPath(),javaInterface.getAttachmentType(),javaInterface.getAttachmentCarrelloId());
//                    javaInterface.loadAttachment(javaInterface.getAttachmentCustomerId(), javaInterface.getAttachmentType(),javaInterface.getAttachmentCarrelloId());

                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                viewClicked.setSelected(true);
                viewClicked = null;
            }
        } else if (requestCode == 6) {
            //--- ISCTF00114392 start
            if (data != null) {
                String path = data.getStringExtra("path");
                Log.d("ATTACHMENTCONVERSION", "path:" + path);
                File fileToCopy = new File(path);
                Date curDate = new Date();
                long curMillis = curDate.getTime();

                String nameFile = path.substring(path.lastIndexOf("/"));
                Log.d("ATTACHMENTCONVERSION", nameFile);
                String extension = nameFile.substring(nameFile.lastIndexOf("."));
                Log.d("ATTACHMENTCONVERSION", extension);

                //ISCTF00109842   starts
                if (fileToCopy.getAbsolutePath().toLowerCase().endsWith(".jpg")
                        || fileToCopy.getAbsolutePath().toLowerCase().endsWith(".jpeg")
                        || fileToCopy.getAbsolutePath().toLowerCase().endsWith(".bmp")
                        || fileToCopy.getAbsolutePath().toLowerCase().endsWith(".gif")
                        ) {

                    File fileCopied;

                    if (path.contains("/VDFN/Pictures")) {
                        fileCopied = fileToCopy;
                    } else {
                        fileCopied = new File(Environment.getExternalStorageDirectory(), "/VDFN/Pictures/" + curMillis + extension);

                        copyFile(fileToCopy, fileCopied);

                        Log.d("ATTACHMENTCONVERSION", "fileCopied.getAbsolutePath() [" + fileCopied.getAbsolutePath() + "]");
                        //ISCTF00113223
                        Bitmap bitmapToConvert = null;
                        if ((fileToCopy.getAbsolutePath().toLowerCase().endsWith(".jpg")
                                || fileToCopy.getAbsolutePath().toLowerCase().endsWith(".jpeg")) && fileToCopy.length() > 1024 * 1024) {
                            bitmapToConvert = loadResizedBitmap(fileCopied.getAbsolutePath(), 960, 720, true);
                        } else {
                            bitmapToConvert = BitmapFactory.decodeFile(fileCopied.getAbsolutePath());
                        }

                        Bitmap convertedBitmap = convertColorImageBlackAndWhiteFromGallery(bitmapToConvert);

                        FileOutputStream fos;
                        try {
                            fos = new FileOutputStream(fileCopied.getAbsolutePath());
                            if (convertedBitmap != null) {
                                convertedBitmap.compress(Bitmap.CompressFormat.JPEG, 70, fos);
                            }


                            fos.flush();
                            fos.close();

                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                    Log.d("ATTACHMENTCONVERSION", fileCopied.getAbsolutePath());
                    Log.d("ATTACHMENTCONVERSION", fileCopied.getName());

//                    dbHelper.saveAttachment(javaInterface.getAttachmentCustomerId(),"/sdcard/VDFN/Pictures/" + fileCopied.getName(),javaInterface.getAttachmentType(),javaInterface.getAttachmentCarrelloId());
//                    javaInterface.loadAttachment(javaInterface.getAttachmentCustomerId(), javaInterface.getAttachmentType(),javaInterface.getAttachmentCarrelloId());

                } else if (fileToCopy.length() < 1024 * 1024) {

                    File fileCopied;

                    if (path.contains("/VDFN/Pictures")) {
                        fileCopied = fileToCopy;
                    } else {
                        fileCopied = new File(Environment.getExternalStorageDirectory(), "/VDFN/Pictures/" + curMillis + extension);
                        copyFile(fileToCopy, fileCopied);
                        Log.d("ATTACHMENTCONVERSION", "fileCopied.getAbsolutePath() [" + fileCopied.getAbsolutePath() + "]");
                    }

//                    dbHelper.saveAttachment(javaInterface.getAttachmentCustomerId(),"/sdcard/VDFN/Pictures/" + fileCopied.getName(),javaInterface.getAttachmentType(),javaInterface.getAttachmentCarrelloId());
//                    javaInterface.loadAttachment(javaInterface.getAttachmentCustomerId(), javaInterface.getAttachmentType(),javaInterface.getAttachmentCarrelloId());

                }    // ISCTF00109842 ends
                else {
//                    mWebView.loadUrl("javascript:$(\"#checkAttachmentSize\").popup(\"open\");");
                }
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    private Bitmap convertColorImageBlackAndWhiteFromGallery(Bitmap origBitmap) {
        ColorMatrix colorMatrix = new ColorMatrix();
        colorMatrix.setSaturation(0);

        ColorMatrixColorFilter colorMatrixFilter = new ColorMatrixColorFilter(
                colorMatrix);
        Bitmap blackAndWhiteBitmap = null;

        if (origBitmap != null) {
            int dstWidth = 960;
            int dstHeight = 720;

            //Toast.makeText(this, "Can DetectOrientation " + rotation +" height:"+ origBitmap.getHeight() + " width:"+origBitmap.getWidth(), Toast.LENGTH_LONG).show();
            //ISCTF00110214
            if (origBitmap.getHeight() > origBitmap.getWidth()) {
                dstWidth = 720;
                dstHeight = 960;
            }


            blackAndWhiteBitmap = origBitmap.copy(Bitmap.Config.ARGB_8888, true);
            blackAndWhiteBitmap = Bitmap.createScaledBitmap(blackAndWhiteBitmap, dstWidth, dstHeight, true);


            Paint paint = new Paint();
            paint.setColorFilter(colorMatrixFilter);

            Canvas canvas = new Canvas(blackAndWhiteBitmap);
            canvas.drawBitmap(blackAndWhiteBitmap, 0, 0, paint);
        }


        return blackAndWhiteBitmap;
    }

    public Bitmap loadResizedBitmap(String filename, int width, int height, boolean exact) {
        Bitmap bitmap = null;
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(filename, options);

        if (options.outHeight > 0 && options.outWidth > 0) {
            options.inJustDecodeBounds = false;
            options.inSampleSize = 2;
            Log.d("ATTACHMENTCONVERSION", "ATTACHMENTCONVERSION options [" + options.outHeight + "; " + options.outWidth + "]");
            if (options.outHeight > options.outWidth) {
                width = 720;
                height = 960;
            }
            while (options.outWidth / options.inSampleSize > width
                    && options.outHeight / options.inSampleSize > height) {
                options.inSampleSize++;
            }
            options.inSampleSize--;

            bitmap = BitmapFactory.decodeFile(filename, options);
            if (bitmap != null && exact) {
                //ISCTF00110214
                Log.d("ATTACHMENTCONVERSION", "ATTACHMENTCONVERSION [" + bitmap.getHeight() + "; " + bitmap.getWidth() + "]");
                Log.d("ATTACHMENTCONVERSION", "ATTACHMENTCONVERSION2 [" + bitmap.getHeight() + "; " + bitmap.getWidth() + "]");
                bitmap = Bitmap.createScaledBitmap(bitmap, width, height, false);
            }
        }
        return bitmap;
    }

    public static boolean copyFile(File source, File dest) {
        BufferedInputStream bis = null;
        BufferedOutputStream bos = null;

        try {
            bis = new BufferedInputStream(new FileInputStream(source));
            bos = new BufferedOutputStream(new FileOutputStream(dest, false));

            byte[] buf = new byte[1024];
            bis.read(buf);

            do {
                bos.write(buf);
            } while (bis.read(buf) != -1);
        } catch (IOException e) {
            return false;
        } finally {
            try {
                if (bis != null) bis.close();
                if (bos != null) bos.close();
            } catch (IOException e) {
                return false;
            }
        }

        return true;
    }

    private Bitmap convertColorImageBlackAndWhite(Bitmap origBitmap) {
        ColorMatrix colorMatrix = new ColorMatrix();
        colorMatrix.setSaturation(0);

        ColorMatrixColorFilter colorMatrixFilter = new ColorMatrixColorFilter(
                colorMatrix);
        Bitmap blackAndWhiteBitmap = null;

        if (origBitmap != null) {
            getResources().getConfiguration();

            int dstWidth = 960;
            int dstHeight = 720;

            //Toast.makeText(this, "Can DetectOrientation " + rotation +" height:"+ origBitmap.getHeight() + " width:"+origBitmap.getWidth(), Toast.LENGTH_LONG).show();
            //ISCTF00110214
            if (origBitmap.getHeight() > origBitmap.getWidth()) {
                dstWidth = 720;
                dstHeight = 960;
            }


            blackAndWhiteBitmap = origBitmap.copy(Bitmap.Config.ARGB_8888, true);
            blackAndWhiteBitmap = Bitmap.createScaledBitmap(blackAndWhiteBitmap, dstWidth, dstHeight, true);


            Paint paint = new Paint();
            paint.setColorFilter(colorMatrixFilter);

            Canvas canvas = new Canvas(blackAndWhiteBitmap);
            canvas.drawBitmap(blackAndWhiteBitmap, 0, 0, paint);
        }


        return blackAndWhiteBitmap;
    }

    private void dispatchCamera() {
        takePhoto();
    }

    private boolean hasMnp() {
        for (OffertaMobile offertaMobile : offertaMobileArrayList) {
            if (offertaMobile.isMnp()) {
                return true;
            }
        }
        return false;
    }

    String path;

    public void takePhoto() {
        // start camera
        Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
        Date curDate = new Date();
        long curMillis = curDate.getTime();
        File photo = new File(Environment.getExternalStorageDirectory(), "/Pictures/" + curMillis + ".jpg");
        path = photo.getAbsolutePath();
        Log.d("JS", "path:" + photo.getAbsolutePath());
        intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photo));
        startActivityForResult(intent, 5);
    }

    public void takeFile() {

        Intent intent = new Intent(getActivity(), FileExplore.class);
        startActivityForResult(intent, 6);

    }

    public void showGenericMessage(Context context, String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton("Fotocamera", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                takePhoto();
                dialog.dismiss();
            }
        });
        builder.setNegativeButton("Galleria", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                takeFile();
                dialog.dismiss();
            }
        });
        builder.show();
    }

    private String generateXML() {
        try {

            String xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                    "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ins=\"http://insertpda.ws.wind.sfa.accenture.com/\">\n" +
                    "<soapenv:Header/>\n" +
                    "<soapenv:Body>" +
                    "<ins:InsertPdaRequestReq>";

//        InsertPdaAnagrafica_Input [INIZIO]
            xml += "<InsertPdaAnagrafica_Input>";

            xml += "<VariazioneTecnica>" + "false" + "</VariazioneTecnica>";

            xml += "<Dealer>";
            xml += "<DealerName>" + Session.getInstance().getDealer().getDealerName() + " " + Session.getInstance().getDealer().getDealerSurname() + "</DealerName>";
            xml += "<DealerUsername>" + Session.getInstance().getDealer().getUsername() + "</DealerUsername>";
            xml += "<DealerPwd>" + Session.getInstance().getDealer().getPassword() + "</DealerPwd>";
            xml += "<ClientManagerName>" + Session.getInstance().getDealer().getNomeClientManager() + "</ClientManagerName>";
            xml += "</Dealer>";

            xml += "<Customer>";

//            xml += "<CustomerCode>"+ "</CustomerCode>";

            xml += "<CustomerName>" + Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("inserimentoDati").getJSONObject("datiCliente").getString("nome") + " " +
                    Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("inserimentoDati").getJSONObject("datiCliente").getString("cognome") + " " +
                    Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("inserimentoDati").getJSONObject("datiCliente").getString("ragioneSociale") + "</CustomerName>";
            xml += "<CompanyType>" + Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("inserimentoDati").getJSONObject("datiCliente").getString("firmaSocietaria") + "</CompanyType>";
            xml += "<ProvinceCCIA>" + Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("inserimentoDati").getJSONObject("datiCliente").getString("provinciaCCIAA") + "</ProvinceCCIA>";
            xml += "<PartitaIVA>" + Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("inserimentoDati").getJSONObject("datiCliente").getString("p_iva") + "</PartitaIVA>";

            xml += "<Delegate>";
            xml += "<FirstName>" + Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("inserimentoDati").getJSONObject("datiRappresentanteLegale").getString("nome") + "</FirstName>";
            xml += "<LastName>" + Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("inserimentoDati").getJSONObject("datiRappresentanteLegale").getString("cognome") + "</LastName>";
            xml += "<PhoneNumber>" + Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("inserimentoDati").getJSONObject("datiRappresentanteLegale").getString("telefono") + "</PhoneNumber>";
            xml += "<FaxNumber>" + Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("inserimentoDati").getJSONObject("datiRappresentanteLegale").optString("fax") + "</FaxNumber>";
            xml += "<EmailAddress>" + Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("inserimentoDati").getJSONObject("datiRappresentanteLegale").optString("email") + "</EmailAddress>";
            xml += "<CodiceFiscale>" + Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("inserimentoDati").getJSONObject("datiRappresentanteLegale").optString("cf") + "</CodiceFiscale>";
            xml += "<BirthCity>" + Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("inserimentoDati").getJSONObject("datiRappresentanteLegale").optString("luogoNascita") + "</BirthCity>";
            xml += "<BirthProvince>" + Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("inserimentoDati").getJSONObject("datiRappresentanteLegale").optString("provincia") + "</BirthProvince>";

//            TODO: gestire data
            String data = Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("inserimentoDati").getJSONObject("datiRappresentanteLegale").optString("dataNascita");
            xml += "<BirthDate>" + (new SimpleDateFormat("yyyy-MM-dd").format(new SimpleDateFormat("dd/MM/yyyy").parse(data))) + "</BirthDate>";
            xml += "<Citizenship>" + Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("inserimentoDati").getJSONObject("datiRappresentanteLegale").optString("nazionalita") + "</Citizenship>";
            xml += "<Gender>" + (Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("inserimentoDati").getJSONObject("datiRappresentanteLegale").optString("gender").compareToIgnoreCase("Maschio") == 0 ? "M" : "F") + "</Gender>";
            xml += "<IDType>" + Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("inserimentoDati").getJSONObject("datiRappresentanteLegale").optString("docIdentita") + "</IDType>";
            xml += "<IDNumber>" + Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("inserimentoDati").getJSONObject("datiRappresentanteLegale").optString("numDocIdentita") + "</IDNumber>";
            xml += "<IDIssuedBy>" + Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("inserimentoDati").getJSONObject("datiRappresentanteLegale").optString("rilasciatoDa") + "</IDIssuedBy>";

            String data2 = Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("inserimentoDati").getJSONObject("datiRappresentanteLegale").optString("dataDoc");
            xml += "<IDIssueDate>" + (new SimpleDateFormat("yyyy-MM-dd").format(new SimpleDateFormat("dd/MM/yyyy").parse(data2))) + "</IDIssueDate>";
//        xml += "<Address>";
//        xml += "</Address>";
            xml += "</Delegate>";

            xml += "<Address>";
//            xml += "<AddressDescription>" + Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("inserimentoDati").getJSONObject("datiSede").optString("riferimentoSede") + "</AddressDescription>";
//            xml += "<AddressType>"++ "</AddressType>";
            xml += "<AddressName>" + Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("inserimentoDati").getJSONObject("datiSede").optString("indirizzo") + "</AddressName>";
            xml += "<StreetNbr>" + Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("inserimentoDati").getJSONObject("datiSede").optString("civico") + "</StreetNbr>";
            xml += "<City>" + Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("inserimentoDati").getJSONObject("datiSede").optString("citta") + "</City>";
            xml += "<Province>" + Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("inserimentoDati").getJSONObject("datiSede").optString("provincia") + "</Province>";
//            xml += "<Country>"++ "</Country>";
            xml += "<PostalCode>" + Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("inserimentoDati").getJSONObject("datiSede").optString("cap") + "</PostalCode>";
            xml += "<ReprFirstName>" + Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("inserimentoDati").getJSONObject("referenteContratto").optString("nome") + "</ReprFirstName>";
            xml += "<ReprLastName>" + Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("inserimentoDati").getJSONObject("referenteContratto").optString("cognome") + "</ReprLastName>";
            xml += "<ReprPhoneNumber>" + Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("inserimentoDati").getJSONObject("referenteContratto").optString("telefono") + "</ReprPhoneNumber>";
            xml += "<ReprFaxNumber>" + Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("inserimentoDati").getJSONObject("referenteContratto").optString("fax") + "</ReprFaxNumber>";
            xml += "<ReprEmailAddress>" + Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("inserimentoDati").getJSONObject("referenteContratto").optString("email") + "</ReprEmailAddress>";
            xml += "</Address>";

            xml += "</Customer>";

            xml += "<InvoiceCallDetail>" + (Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("inserimentoDati").getJSONObject("referenteContratto").getString("dettaglioChiamate").compareToIgnoreCase("1") == 0 ? "true" : "false") + "</InvoiceCallDetail>";
            xml += "<InvoiceUnique>" + (Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("inserimentoDati").getJSONObject("referenteContratto").getString("modFatturazione").compareToIgnoreCase("Fattura unica") == 0 ? "true" : "false") + "</InvoiceUnique>";
            xml += "<InvoiceByCostCenter>" + (Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("inserimentoDati").getJSONObject("referenteContratto").getString("modFatturazione").compareToIgnoreCase("Fattura unica") != 0 ? "true" : "false") + "</InvoiceByCostCenter>";
            xml += "<TaxBenefit>" + (Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("inserimentoDati").getJSONObject("referenteContratto").getString("agevolazioniFiscali").compareToIgnoreCase("1") == 0 ? "true" : "false") + "</TaxBenefit>";
            xml += "<FlagIPA>" + "false</FlagIPA>";
            xml += "<IPAIDNumber>" + Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("inserimentoDati").getJSONObject("pubblicaAmministrazione").optString("codiceIdentificativoUfficio") + "</IPAIDNumber>";
            xml += "<SplitPayment>" + (Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("inserimentoDati").getJSONObject("pubblicaAmministrazione").getString("splitPayment").compareToIgnoreCase("1") == 0 ? "true" : "false") + "</SplitPayment>";
            xml += "<EmailInvoice>" + (Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("inserimentoDati").getJSONObject("modInvioFattura").getString("elettronico").compareToIgnoreCase("1") == 0 ? "true" : "false") + "</EmailInvoice>";
            xml += "<PrintedInvoice>" + (Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("inserimentoDati").getJSONObject("modInvioFattura").getString("cartaceo").compareToIgnoreCase("1") == 0 ? "true" : "false") + "</PrintedInvoice>";
            xml += "<PrintedCostDetails>" + (Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("inserimentoDati").getJSONObject("documentazioneCosti").getString("supportoCartaceo").compareToIgnoreCase("1") == 0 ? "true" : "false") + "</PrintedCostDetails>";
            xml += "<CdromCostDetails>" + (Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("inserimentoDati").getJSONObject("documentazioneCosti").getString("supportoCD").compareToIgnoreCase("1") == 0 ? "true" : "false") + "</CdromCostDetails>";

            xml += "<PaymentProfile>";

            xml += "<PaymentMethod>" + (Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("infoPagamento").getString("pagamentoTramiteRid").compareToIgnoreCase("1") == 0 ? "RID" : "CC") + "</PaymentMethod>";
            if (Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("infoPagamento").getString("pagamentoTramiteRid").compareToIgnoreCase("1") == 0) {


                xml += "<IBAN>" + Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("infoPagamento").getJSONObject("estremiContoCorrente").optString("iban") + "</IBAN>";
                xml += "<BankName>" + Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("infoPagamento").getJSONObject("estremiContoCorrente").optString("nomeBancaPosta") + "</BankName>";
                xml += "<BankBranch>" + Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("infoPagamento").getJSONObject("estremiContoCorrente").optString("agenziaUfficioFiliale") + "</BankBranch>";
                xml += "<BankAuthorizationflag>" + "false" + "</BankAuthorizationflag>";

                xml += "<BankSubscriber>";

                xml += "<FirstName>" + Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("infoPagamento").getJSONObject("sottoscrittoreDelModulo").optString("nome") + "</FirstName>";
                xml += "<LastName>" + Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("infoPagamento").getJSONObject("sottoscrittoreDelModulo").optString("cognome") + "</LastName>";
//            xml += "<PhoneNumber>"+Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("infoPagamento").getJSONObject("sottoscrittoreDelModulo").optString("nome")+ "</PhoneNumber>";
//            xml += "<FaxNumber>"+Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("infoPagamento").getJSONObject("sottoscrittoreDelModulo").optString("nome")+ "</FaxNumber>";
//            xml += "<EmailAddress>"+Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("infoPagamento").getJSONObject("sottoscrittoreDelModulo").optString("nome")+ "</EmailAddress>";
                xml += "<CodiceFiscale>" + Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("infoPagamento").getJSONObject("sottoscrittoreDelModulo").optString("cf") + "</CodiceFiscale>";
                xml += "<BirthCity>" + Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("infoPagamento").getJSONObject("sottoscrittoreDelModulo").optString("luogoDiNascita") + "</BirthCity>";
                xml += "<BirthProvince>" + Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("infoPagamento").getJSONObject("sottoscrittoreDelModulo").optString("provincia") + "</BirthProvince>";

                String data3 = Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("infoPagamento").getJSONObject("sottoscrittoreDelModulo").optString("dataDiNascita");
                xml += "<BirthDate>" + (new SimpleDateFormat("yyyy-MM-dd").format(new SimpleDateFormat("dd/MM/yyyy").parse(data3))) + "</BirthDate>";
//            xml += "<Citizenship>"+Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("infoPagamento").getJSONObject("sottoscrittoreDelModulo").optString("nome")+ "</Citizenship>";
                xml += "<Gender>" + (Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("infoPagamento").getJSONObject("sottoscrittoreDelModulo").optString("gender").compareToIgnoreCase("Maschio") == 0 ? "M" : "F") + "</Gender>";
//            xml += "<IDType>"+Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("infoPagamento").getJSONObject("sottoscrittoreDelModulo").optString("nome")+ "</IDType>";
//            xml += "<IDNumber>"+Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("infoPagamento").getJSONObject("sottoscrittoreDelModulo").optString("nome")+ "</IDNumber>";
//            xml += "<IDIssuedBy>"+Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("infoPagamento").getJSONObject("sottoscrittoreDelModulo").optString("nome")+ "</IDIssuedBy>";
//            xml += "<IDIssueDate>"+Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("infoPagamento").getJSONObject("sottoscrittoreDelModulo").optString("nome")+ "</IDIssueDate>";

                xml += "<Address>";
//            xml += "<AddressDescription>"++ "</AddressDescription>";
//            xml += "<AddressType>"++ "</AddressType>";
                xml += "<AddressName>" + Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("infoPagamento").getJSONObject("sottoscrittoreDelModulo").optString("indirizzo") + "</AddressName>";
                xml += "<StreetNbr>" + Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("infoPagamento").getJSONObject("sottoscrittoreDelModulo").optString("civico") + "</StreetNbr>";
                xml += "<City>" + Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("infoPagamento").getJSONObject("sottoscrittoreDelModulo").optString("citta") + "</City>";
                xml += "<Province>" + Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("infoPagamento").getJSONObject("sottoscrittoreDelModulo").optString("provincia") + "</Province>";
//            xml += "<Country>"++ "</Country>";
                xml += "<PostalCode>" + Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("infoPagamento").getJSONObject("sottoscrittoreDelModulo").optString("cap") + "</PostalCode>";
//            xml += "<ReprFirstName>"++ "</ReprFirstName>";
//            xml += "<ReprLastName>"++ "</ReprLastName>";
//            xml += "<ReprPhoneNumber>"++ "</ReprPhoneNumber>";
//            xml += "<ReprFaxNumber>"++ "</ReprFaxNumber>";
//            xml += "<ReprEmailAddress>"++ "</ReprEmailAddress>";
                xml += "</Address>";

                xml += "</BankSubscriber>";

                xml += "<BankAccount>";

                xml += "<FirstName>" + Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("infoPagamento").getJSONObject("intestatarioConto").optString("nome") + "</FirstName>";
                xml += "<LastName>" + Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("infoPagamento").getJSONObject("intestatarioConto").optString("cognome") + "</LastName>";
//            xml += "<PhoneNumber>"++ "</PhoneNumber>";
//            xml += "<FaxNumber>"++ "</FaxNumber>";
//            xml += "<EmailAddress>"++ "</EmailAddress>";
                xml += "<CodiceFiscale>" + Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("infoPagamento").getJSONObject("intestatarioConto").optString("cf") + "</CodiceFiscale>";
                xml += "<BirthCity>" + Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("infoPagamento").getJSONObject("intestatarioConto").optString("citta") + "</BirthCity>";
                xml += "<BirthProvince>" + Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("infoPagamento").getJSONObject("intestatarioConto").optString("provincia") + "</BirthProvince>";
//            xml += "<BirthDate>"++ "</BirthDate>";
//            xml += "<Citizenship>"++ "</Citizenship>";
//            xml += "<Gender>"++ "</Gender>";
//            xml += "<IDType>"++ "</IDType>";
//            xml += "<IDNumber>"++ "</IDNumber>";
//            xml += "<IDIssuedBy>"++ "</IDIssuedBy>";
//            xml += "<IDIssueDate>"++ "</IDIssueDate>";

                xml += "<Address>";
//            xml += "<AddressDescription>"++ "</AddressDescription>";
//            xml += "<AddressType>"++ "</AddressType>";
                xml += "<AddressName>" + Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("infoPagamento").getJSONObject("intestatarioConto").optString("indirizzo") + "</AddressName>";
                xml += "<StreetNbr>" + Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("infoPagamento").getJSONObject("intestatarioConto").optString("civico") + "</StreetNbr>";
                xml += "<City>" + Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("infoPagamento").getJSONObject("intestatarioConto").optString("citta") + "</City>";
                xml += "<Province>" + Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("infoPagamento").getJSONObject("intestatarioConto").optString("provincia") + "</Province>";
//            xml += "<Country>"++ "</Country>";
                xml += "<PostalCode>" + Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("infoPagamento").getJSONObject("intestatarioConto").optString("cap") + "</PostalCode>";
//            xml += "<ReprFirstName>"++ "</ReprFirstName>";
//            xml += "<ReprLastName>"++ "</ReprLastName>";
//            xml += "<ReprPhoneNumber>"++ "</ReprPhoneNumber>";
//            xml += "<ReprFaxNumber>"++ "</ReprFaxNumber>";
//            xml += "<ReprEmailAddress>"++ "</ReprEmailAddress>";
                xml += "</Address>";

                xml += "</BankAccount>";

                xml += "<DirectDebitID>" + Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("infoPagamento").getJSONObject("addebitoDiretto").optString("identificativoMandato") + "</DirectDebitID>";
//            xml += "<DirectDebitDate>"++ "</DirectDebitDate>";
//            xml += "<DirectDebitPlace>"++ "</DirectDebitPlace>";
//            xml += "<BillFrequency>"++ "</BillFrequency>";
//            xml += "<BillType>"++ "</BillType>";
//            xml += "<BillVendorId>"++ "</BillVendorId>";


            } else {

                String creditCardType = "";
                if (Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("infoPagamento").getJSONObject("autorizzazionePermanenteDiAddebito").getString("visaMastercard").compareToIgnoreCase("1") == 0) {
                    creditCardType = "VISA/MASTERCARD";
                } else if (Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("infoPagamento").getJSONObject("autorizzazionePermanenteDiAddebito").getString("cartaSi").compareToIgnoreCase("1") == 0) {
                    creditCardType = "CARTASI";
                } else if (Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("infoPagamento").getJSONObject("autorizzazionePermanenteDiAddebito").getString("americanExpress").compareToIgnoreCase("1") == 0) {
                    creditCardType = "AMERICAN EXPRESS";
                } else if (Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("infoPagamento").getJSONObject("autorizzazionePermanenteDiAddebito").getString("diners").compareToIgnoreCase("1") == 0) {
                    creditCardType = "DINERS";
                } else if (Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("infoPagamento").getJSONObject("autorizzazionePermanenteDiAddebito").getString("deutscheCreditCard").compareToIgnoreCase("1") == 0) {
                    creditCardType = "DEUTSCHE CREDIT CARD";
                }


                xml += "<CreditCardType>" + creditCardType + "</CreditCardType>";
                xml += "<CreditCardNumber>" + Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("infoPagamento").getJSONObject("autorizzazionePermanenteDiAddebito").optString("numero") + "</CreditCardNumber>";

                String data4 = Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("infoPagamento").getJSONObject("autorizzazionePermanenteDiAddebito").optString("scadenza");
                xml += "<CreditCardExpirationDate>" + (new SimpleDateFormat("yyyy-MM-dd").format(new SimpleDateFormat("dd/MM/yyyy").parse(data4))) + "</CreditCardExpirationDate>";
            }

            xml += "</PaymentProfile>";


            xml += "</InsertPdaAnagrafica_Input>";
//        InsertPdaAnagrafica_Input [FINE]

//        InsertPdaOrder_Input [INIZIO]
            xml += "<InsertPdaOrder_Input>";

            xml += "<Order>";

//            xml += "<Status/>";
//            xml += "<OrderType/>";
//            xml += "<SystemId/>";

            boolean hasAbbonamento = false;
            ArrayList<OffertaMobile> offerteAbbonamento = new ArrayList<>();
            for (OffertaMobile offertaMobile : offertaMobileArrayList) {
                if (offertaMobile.getOfferType().compareToIgnoreCase("Abbonamento") == 0) {
                    hasAbbonamento = true;
                    offerteAbbonamento.add(offertaMobile);
                }
            }
            if (hasAbbonamento) {
                xml += "<MobileOffer>";

                xml += "<MobileOfferType>abbonamento</MobileOfferType>";
                xml += "<TransferCreditFlag>" + "false" + "</TransferCreditFlag>";
                xml += "<TCGFlag>" + "false" + "</TCGFlag>";
                xml += "<SubscriberName>" + Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("inserimentoDati").getJSONObject("datiCliente").getString("nome") + "</SubscriberName>";
                xml += "<SubscriberSurname>" + Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("inserimentoDati").getJSONObject("datiCliente").getString("cognome") + "</SubscriberSurname>";
                xml += "<CompanyName>" + Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("inserimentoDati").getJSONObject("datiCliente").getString("ragioneSociale") + "</CompanyName>";
                xml += "<PartitaIVA>" + Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("inserimentoDati").getJSONObject("datiCliente").getString("p_iva") + "</PartitaIVA>";
                xml += "<Place>" + Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("inserimentoDati").getJSONObject("datiCliente").getString("citta") + "</Place>";

                for (OffertaMobile offertaMobile : offerteAbbonamento) {
                    xml += "<SIM>";

                    xml += "<ICCID>" + offertaMobile.getNewICCID() + "</ICCID>";
                    if (offertaMobile.isMnp()) {
                        xml += "<MSISDN>" + offertaMobile.getOldMSISDN() + "</MSISDN>";
                        xml += "<MNPICCID>" + offertaMobile.getOldICCID() + "</MNPICCID>";
                        xml += "<MNPDonor>" + offertaMobile.getMnpStep8() + "</MNPDonor>";
                    }
                    xml += "<TariffPlan>" + (offertaMobile.getOfferDescription() + " " + offertaMobile.getOfferDescriptionUpper()).trim() + "</TariffPlan>";
//                    xml += "<InternetTariffPlan>"++ "</InternetTariffPlan>";

                    for (OpzioneMobile opzioneMobile : offertaMobile.getOpzioniMobileSelectedArrayListStep8()) {
                        if (!Boolean.parseBoolean(opzioneMobile.getIsDefault()) || TextUtils.isEmpty(opzioneMobile.getScontoConvergenza())) {
                            xml += "<Options>" + opzioneMobile.getOpzioneDesc() + "</Options>";
                        }
                    }

                    if (offertaMobile.getAddOnMobileStep8() != null) {
                        xml += "<Options>" + offertaMobile.getAddOnMobileStep8().getDescFascia() + " - " + offertaMobile.getAddOnMobileStep8().getSelectedLabelStep8() + "</Options>";
                    }

                    boolean hasPromoSuper = false;
                    for (OpzioneMobile opzioneMobile : offertaMobile.getOpzioniMobileSelectedArrayListStep8()) {
                        if (!TextUtils.isEmpty(opzioneMobile.getScontoConvergenza())) {
                            hasPromoSuper = true;
                            break;
                        }
                    }
                    xml += "<PromoSuper>" + String.valueOf(hasPromoSuper) + "</PromoSuper>";

                    boolean hasInternoMobile = false;
                    for (OpzioneMobile opzioneMobile : offertaMobile.getOpzioniMobileSelectedArrayListStep8()) {
                        if (opzioneMobile.getOpzioneDesc().compareToIgnoreCase("Interno Mobile") == 0) {
                            hasInternoMobile = true;
                            break;
                        }
                    }
                    xml += "<InternoMobile>" + String.valueOf(hasInternoMobile) + "</InternoMobile>";

                    boolean hasPromoTerminale = false;
                    for (OpzioneMobile opzioneMobile : offertaMobile.getOpzioniMobileSelectedArrayListStep8()) {
                        if (opzioneMobile.getOpzioneDesc().compareToIgnoreCase("Promo Terminale") == 0) {
                            hasPromoTerminale = true;
                            break;
                        }
                    }
                    xml += "<PromoTerminale>" + String.valueOf(hasPromoTerminale) + "</PromoTerminale>";

                    boolean hasAssistenzaTecnicaEvolutiva = false;
                    for (OpzioneMobile opzioneMobile : offertaMobile.getOpzioniMobileSelectedArrayListStep8()) {
                        if (opzioneMobile.getOpzioneDesc().compareToIgnoreCase("Assistenza Tecnica Evolutiva") == 0) {
                            hasAssistenzaTecnicaEvolutiva = true;
                            break;
                        }
                    }
                    xml += "<AssistenzaTecnicaEvolutiva>" + String.valueOf(hasAssistenzaTecnicaEvolutiva) + "</AssistenzaTecnicaEvolutiva>";

                    boolean hasActive = false;
                    for (OpzioneMobile opzioneMobile : offertaMobile.getOpzioniMobileSelectedArrayListStep8()) {
                        if (opzioneMobile.getOpzioneDesc().compareToIgnoreCase("Sim Già Attiva") == 0) {
                            hasActive = true;
                            break;
                        }
                    }
                    xml += "<Active>" + String.valueOf(hasActive) + "</Active>";

                    if (offertaMobile.getTerminale1() != null) {
                        xml += "<DeviceModel>" + offertaMobile.getTerminale1().getTerminaleDescription() + "</DeviceModel>";
                    }
                    if (offertaMobile.getTerminale2() != null) {
                        xml += "<DeviceModel>" + offertaMobile.getTerminale2().getTerminaleDescription() + "</DeviceModel>";
                    }

                    xml += "</SIM>";
                }

                xml += "</MobileOffer>";
            }

            boolean hasRicaricabile = false;
            ArrayList<OffertaMobile> offerteRicaricabile = new ArrayList<>();
            for (OffertaMobile offertaMobile : offertaMobileArrayList) {
                if (offertaMobile.getOfferType().compareToIgnoreCase("Ricaricabile") == 0) {
                    hasRicaricabile = true;
                    offerteRicaricabile.add(offertaMobile);
                }
            }
            if (hasRicaricabile) {
                xml += "<MobileOffer>";

                xml += "<MobileOfferType>abbonamento</MobileOfferType>";
                xml += "<TransferCreditFlag>" + "false" + "</TransferCreditFlag>";
                xml += "<TCGFlag>" + "false" + "</TCGFlag>";
                xml += "<SubscriberName>" + Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("inserimentoDati").getJSONObject("datiCliente").getString("nome") + "</SubscriberName>";
                xml += "<SubscriberSurname>" + Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("inserimentoDati").getJSONObject("datiCliente").getString("cognome") + "</SubscriberSurname>";
                xml += "<CompanyName>" + Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("inserimentoDati").getJSONObject("datiCliente").getString("ragioneSociale") + "</CompanyName>";
                xml += "<PartitaIVA>" + Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("inserimentoDati").getJSONObject("datiCliente").getString("p_iva") + "</PartitaIVA>";
                xml += "<Place>" + Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("inserimentoDati").getJSONObject("datiCliente").getString("citta") + "</Place>";

                for (OffertaMobile offertaMobile : offerteAbbonamento) {
                    xml += "<SIM>";

                    xml += "<ICCID>" + offertaMobile.getNewICCID() + "</ICCID>";
                    if (offertaMobile.isMnp()) {
                        xml += "<MSISDN>" + offertaMobile.getOldMSISDN() + "</MSISDN>";
                        xml += "<MNPICCID>" + offertaMobile.getOldICCID() + "</MNPICCID>";
                        xml += "<MNPDonor>" + offertaMobile.getMnpStep8() + "</MNPDonor>";
                    }
                    xml += "<TariffPlan>" + offertaMobile.getOfferDescription() + " " + offertaMobile.getOfferDescriptionUpper() + "</TariffPlan>";
//                    xml += "<InternetTariffPlan>"++ "</InternetTariffPlan>";

                    for (OpzioneMobile opzioneMobile : offertaMobile.getOpzioniMobileSelectedArrayListStep8()) {
                        if (!Boolean.parseBoolean(opzioneMobile.getIsDefault()) || TextUtils.isEmpty(opzioneMobile.getScontoConvergenza())) {
                            xml += "<Options>" + opzioneMobile.getOpzioneDesc() + " " + opzioneMobile.getOpzioneDescUpper() + "</Options>";
                        }
                    }

                    if (offertaMobile.getAddOnMobileStep8() != null) {
                        xml += "<Options>" + offertaMobile.getAddOnMobileStep8().getDescFascia() + " " + offertaMobile.getAddOnMobileStep8().getSelectedLabelStep8() + "</Options>";
                    }

                    String automaticRefill = "";
                    for (OpzioneMobile opzioneMobile : offertaMobile.getOpzioniMobileSelectedArrayListStep8()) {
                        if (!TextUtils.isEmpty(opzioneMobile.getValue())) {
                            automaticRefill = opzioneMobile.getValue();
                            break;
                        }
                    }
                    xml += "<AutomaticRefill>" + automaticRefill + "</AutomaticRefill>";

                    boolean hasPromoSuper = false;
                    for (OpzioneMobile opzioneMobile : offertaMobile.getOpzioniMobileSelectedArrayListStep8()) {
                        if (!TextUtils.isEmpty(opzioneMobile.getScontoConvergenza())) {
                            hasPromoSuper = true;
                            break;
                        }
                    }
                    xml += "<PromoSuper>" + String.valueOf(hasPromoSuper) + "</PromoSuper>";

                    boolean hasInternoMobile = false;
                    for (OpzioneMobile opzioneMobile : offertaMobile.getOpzioniMobileSelectedArrayListStep8()) {
                        if (opzioneMobile.getOpzioneDesc().compareToIgnoreCase("Interno Mobile") == 0) {
                            hasInternoMobile = true;
                            break;
                        }
                    }
                    xml += "<InternoMobile>" + String.valueOf(hasInternoMobile) + "</InternoMobile>";

                    boolean hasPromoTerminale = false;
                    for (OpzioneMobile opzioneMobile : offertaMobile.getOpzioniMobileSelectedArrayListStep8()) {
                        if (opzioneMobile.getOpzioneDesc().compareToIgnoreCase("Promo Terminale") == 0) {
                            hasPromoTerminale = true;
                            break;
                        }
                    }
                    xml += "<PromoTerminale>" + String.valueOf(hasPromoTerminale) + "</PromoTerminale>";

                    boolean hasAssistenzaTecnicaEvolutiva = false;
                    for (OpzioneMobile opzioneMobile : offertaMobile.getOpzioniMobileSelectedArrayListStep8()) {
                        if (opzioneMobile.getOpzioneDesc().compareToIgnoreCase("Assistenza Tecnica Evolutiva") == 0) {
                            hasAssistenzaTecnicaEvolutiva = true;
                            break;
                        }
                    }
                    xml += "<AssistenzaTecnicaEvolutiva>" + String.valueOf(hasAssistenzaTecnicaEvolutiva) + "</AssistenzaTecnicaEvolutiva>";

                    boolean hasActive = false;
                    for (OpzioneMobile opzioneMobile : offertaMobile.getOpzioniMobileSelectedArrayListStep8()) {
                        if (opzioneMobile.getOpzioneDesc().compareToIgnoreCase("Sim Già Attiva") == 0) {
                            hasActive = true;
                            break;
                        }
                    }
                    xml += "<Active>" + String.valueOf(hasActive) + "</Active>";

                    if (offertaMobile.getTerminale1() != null) {
                        xml += "<DeviceModel>" + offertaMobile.getTerminale1().getTerminaleDescription() + "</DeviceModel>";
                    }
                    if (offertaMobile.getTerminale2() != null) {
                        xml += "<DeviceModel>" + offertaMobile.getTerminale2().getTerminaleDescription() + "</DeviceModel>";
                    }

                    xml += "</SIM>";
                }

                xml += "</MobileOffer>";
            }

            xml += "</Order>";

            xml += "</InsertPdaOrder_Input>";
//        InsertPdaOrder_Input [FINE]

//        ServiceRequest [INIZIO]
            xml += "<ServiceRequest>";

            xml += "<RequestId>" + Session.getInstance().getPratica().getRequest_id() + "</RequestId>";

            String requestType = "";
            if (isPreview) {
                requestType = "ViewPdf";
            } else if (isSendMail) {
                requestType = "SendEmail";
            } else if (isFirma) {
                requestType = "SendXyzmo";
            }
            xml += "<RequestType>" + requestType + "</RequestType>";

            xml += "<GeneratePdfFlag>" + "true" + "</GeneratePdfFlag>";

            xml += "<BiometricPdaFlag>" + "true" + "</BiometricPdaFlag>";
            xml += "<SystemId>" + "12" + "</SystemId>";
//            xml += "<Login>"++ "</Login>";
            xml += "<CodiceManager>" + Session.getInstance().getDealer().getDealerCode() + "</CodiceManager>";
//            xml += "<OfferType/>";

//           TODO: Se esistono allegati

//            xml += "<Attachment>";
//            xml += "<ActivityFileExt>" + "jpeg" + "</ActivityFileExt>";
//            xml += "<ActivityFileName>" + "nome" + "</ActivityFileName>";
//            xml += "<AttachmentType>" + "nome" + "</AttachmentType>";
//            xml += "<ActivityFileBuffer>" + "nome" + "</ActivityFileBuffer>";
//            xml += "</Attachment>";

//            xml += "<TBLUserProfile/>";
            xml += "<ContractPlace>" + "Roma" + "</ContractPlace>";
            xml += "<ContractDate>" + (new SimpleDateFormat("yyyy-MM-dd").format(new Date())) + "</ContractDate>";

            if (isSendMail) {
//                xml += "<ContractDate>"++ "</ContractDate>";
            }


            xml += "</ServiceRequest>";
//        ServiceRequest [FINE]


            xml += "</ins:InsertPdaRequestReq>\n" +
                    "</soapenv:Body>\n" +
                    "</soapenv:Envelope>";

            return xml;

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "";
    }

    private void openPdf() {
        File pdfFile = new File(Environment.getExternalStorageDirectory() + "/windpdf.pdf");  // -> filename = maven.pdf
        Uri path = Uri.fromFile(pdfFile);
        Intent pdfIntent = new Intent(Intent.ACTION_VIEW);
        pdfIntent.setDataAndType(path, "application/pdf");
        pdfIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        try {
            startActivity(pdfIntent);
        } catch (ActivityNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean handleMessage(Message msg) {
        switch (msg.what) {
            case Constants.HandlerWhat.WHAT_PREVIEW:
                openPdf();
                break;
        }
        return false;
    }
}
