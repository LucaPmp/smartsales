package it.wind.smartsales.fragments.wireline;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Locale;

import it.wind.smartsales.R;
import it.wind.smartsales.commons.SelectedOnClickListener;
import it.wind.smartsales.dao.CatalogoQuestionarioDAO;
import it.wind.smartsales.entities.Question;

public class WirelineInternetOfferStep1Fragment extends WirelineInnerFragment {

    private static final String BUNDLE = "bundle";
    private View offer;
    private View offerAndNumber;
    private TextView numberCount;
    private View abroadYes;
    private View abroadNo;
    private Args args;

    public WirelineInternetOfferStep1Fragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        args = new Args();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_create_offer_wireline_internet_step_1, container, false);
    }

    private void setVisibility(View view, View adsl, View cable, View questionAdslCable, View otherOffer) {
        adsl.setActivated(view.isSelected());
        cable.setActivated(view.isSelected());
        adsl.setSelected(false);
        cable.setSelected(false);
        questionAdslCable.setActivated(view.isSelected());

        if (view.isSelected())
            otherOffer.setSelected(false);
    }

    private Bundle savedState = null;

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        savedState = saveState(); /* vstup defined here for sure */
    }


    private Bundle saveState() {
        Bundle outState = new Bundle();
        outState.putBoolean("isOfferSelected", args.isOfferSelected);
        outState.putBoolean("isOfferAndNumberSelected", args.isOfferAndNumberSelected);
        outState.putBoolean("isAdslSelected", args.isAdslSelected);
        outState.putBoolean("isCableSelected", args.isCableSelected);
        outState.putBoolean("isAbroadYesSelected", args.isAbroadYesSelected);
        outState.putBoolean("isAbroadNoSelected", args.isAbroadNoSelected);
        outState.putString("numberCountText", args.numberCountText);
        outState.putInt("topSpinnerSelectionId", args.topSpinnerSelectionId);
        outState.putInt("bottomSpinnerSelectionId", args.bottomSpinnerSelectionId);
        return outState;
    }

    private class Args {
        boolean isOfferSelected;
        boolean isOfferAndNumberSelected;
        boolean isAdslSelected;
        boolean isCableSelected;
        boolean isAbroadYesSelected;
        boolean isAbroadNoSelected;
        String numberCountText;
        int topSpinnerSelectionId;
        int bottomSpinnerSelectionId;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        offer = view.findViewById(R.id.wireline_offer);
        offerAndNumber = view.findViewById(R.id.wireline_offer_and_number);
        final View adsl = view.findViewById(R.id.wireline_adsl);
        final View cable = view.findViewById(R.id.wireline_cable);
        abroadYes = view.findViewById(R.id.wireline_abroad_yes);
        abroadNo = view.findViewById(R.id.wireline_abroad_no);
        final View geographicArea = view.findViewById(R.id.wireline_geographic_area);
        final View questionAdslCable = view.findViewById(R.id.question_1);
        final View questionNumberCount = view.findViewById(R.id.question_2);
        numberCount = (TextView) view.findViewById(R.id.wireline_number_count);
        final View leftArrow = view.findViewById(R.id.wireline_arrow_left);
        final View rightArrow = view.findViewById(R.id.wireline_arrow_right);
        final Spinner topSpinner = (Spinner) view.findViewById(R.id.spinner_1);
        final Spinner bottomSpinner = (Spinner) view.findViewById(R.id.spinner_2);

        numberCount.setText(String.format(Locale.getDefault(), "%03d", 0));

        if (savedState != null) {
            System.out.println("SAVED STATE");
            offer.setSelected(savedState.getBoolean("isOfferSelected", args.isOfferSelected));
            offerAndNumber.setSelected(savedState.getBoolean("isOfferAndNumberSelected", args.isOfferAndNumberSelected));
            adsl.setSelected(savedState.getBoolean("isAdslSelected", args.isAdslSelected));
            cable.setSelected(savedState.getBoolean("isCableSelected", args.isCableSelected));
            abroadYes.setSelected(savedState.getBoolean("isAbroadYesSelected", args.isAbroadYesSelected));

            abroadNo.setVisibility(args.isAbroadYesSelected ? View.GONE : View.VISIBLE);
            geographicArea.setVisibility(args.isAbroadYesSelected ? View.VISIBLE : View.GONE);

            abroadNo.setSelected(savedState.getBoolean("isAbroadNoSelected", args.isAbroadNoSelected));
            numberCount.setText(savedState.getString("numberCountText", args.numberCountText));
            topSpinner.setSelection(savedState.getInt("topSpinnerSelectionId", args.topSpinnerSelectionId));
            topSpinner.setSelection(savedState.getInt("bottomSpinnerSelectionId", args.bottomSpinnerSelectionId));
        }

        ArrayList<Question> questions = CatalogoQuestionarioDAO.retrieveQuestions(getActivity(), "fisso");
        for (int i = 0; i < questions.size(); i++) {
            ((TextView) view.findViewById(getResources().getIdentifier("question_" + i, "id", getActivity().getPackageName()))).setText(questions.get(i).getQuestionDescription());
        }
        final ArrayList<String> topSpinnerArray = new ArrayList<>(Arrays.asList(getResources().getStringArray(R.array.wireline_spinner)));
        final ArrayList<String> bottomSpinnerArray = new ArrayList<>(Arrays.asList(getResources().getStringArray(R.array.wireline_spinner)));

        ArrayAdapter<String> adapter1 = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, topSpinnerArray);
        adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        topSpinner.setAdapter(adapter1);
        ArrayAdapter<String> adapter2 = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, bottomSpinnerArray);
        adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        bottomSpinner.setAdapter(adapter2);


        offer.setOnClickListener(new SelectedOnClickListener() {
            @Override
            public void performClick(View view) {
                setVisibility(view, adsl, cable, questionAdslCable, offerAndNumber);
                enableContinue();
                args.isOfferSelected = view.isSelected();
                if (view.isSelected()) {
                    questionNumberCount.setActivated(false);
                    leftArrow.setActivated(false);
                    rightArrow.setActivated(false);
                    leftArrow.setEnabled(false);
                    rightArrow.setEnabled(false);
                    numberCount.setActivated(false);
                    numberCount.setText(String.format(Locale.getDefault(), "%03d", 0));
                }
            }
        });
        offerAndNumber.setOnClickListener(new SelectedOnClickListener() {
            @Override
            public void performClick(View view) {
                setVisibility(view, adsl, cable, questionAdslCable, offer);
                args.isOfferAndNumberSelected = view.isSelected();

                questionNumberCount.setActivated(view.isSelected());
                leftArrow.setActivated(view.isSelected());
                rightArrow.setActivated(view.isSelected());
                leftArrow.setEnabled(view.isSelected());
                rightArrow.setEnabled(view.isSelected());
                numberCount.setActivated(view.isSelected());
                if (!view.isSelected())
                    numberCount.setText(String.format(Locale.getDefault(), "%03d", 0));

                enableContinue();
            }
        });
        adsl.setOnClickListener(new SelectedOnClickListener() {
            @Override
            public void performClick(View view) {
                args.isAdslSelected = view.isSelected();

            }
        });
        cable.setOnClickListener(new SelectedOnClickListener() {
            @Override
            public void performClick(View view) {
                args.isCableSelected = view.isSelected();

            }
        });
        leftArrow.setOnClickListener(new SelectedOnClickListener() {
            @Override
            public void performClick(View view) {
                int count = Integer.valueOf(numberCount.getText().toString()) - 1;
                String text = String.format(Locale.getDefault(), "%03d", count > 0 ? count : 0);
                numberCount.setText(text);
                args.numberCountText = text;
                enableContinue();
            }
        });
        rightArrow.setOnClickListener(new SelectedOnClickListener() {
            @Override
            public void performClick(View view) {
                int count = Integer.valueOf(numberCount.getText().toString()) + 1;
                String text = String.format(Locale.getDefault(), "%03d", count);
                numberCount.setText(text);
                args.numberCountText = text;
                enableContinue();
            }
        });
        abroadYes.setOnClickListener(new SelectedOnClickListener() {
            @Override
            public void performClick(View view) {
                abroadNo.setSelected(false);
                abroadNo.setVisibility(view.isSelected() ? View.GONE : View.VISIBLE);
                geographicArea.setVisibility(view.isSelected() ? View.VISIBLE : View.GONE);
                args.isAbroadYesSelected = view.isSelected();

                enableContinue();
            }
        });
        abroadNo.setOnClickListener(new SelectedOnClickListener() {
            @Override
            public void performClick(View view) {
                abroadYes.setSelected(false);
                args.isAbroadNoSelected = view.isSelected();
                enableContinue();
            }
        });

        topSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                // your code here
                updateSpinner(topSpinnerArray.get(position), bottomSpinnerArray);
                args.topSpinnerSelectionId = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here

            }

        });

        bottomSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                // your code here
                updateSpinner(bottomSpinnerArray.get(position), topSpinnerArray);
                args.bottomSpinnerSelectionId = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });
    }


    private void updateSpinner(String selected, ArrayList<String> arrayList) {
        arrayList.clear();
        for (String string : new ArrayList<>(Arrays.asList(getResources().getStringArray(R.array.wireline_spinner)))) {
            if (selected.equalsIgnoreCase(string))
                arrayList.remove(selected);
            else
                arrayList.add(string);
        }
    }

    @Override
    public void enableContinue() {
        if (listener != null)
            listener.setEnabled((offer.isSelected()  // offerta
                    || (offerAndNumber.isSelected() && (Integer.valueOf((numberCount).getText().toString()) > 0))) // offerta+interno con almeno un interno
                    && (abroadYes.isSelected() || abroadNo.isSelected()));

    }

    @Override
    public int getPosition() {
        return 1;
    }


}