package it.wind.smartsales.fragments;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import it.wind.smartsales.R;

/**
 * Created by giuseppe.mangino on 14/07/2016.
 */
@SuppressLint("ValidFragment")
public class SubHeaderFragment extends Fragment {
    private final String title;
    private final String backTitle;
    private final boolean hasSearchBar;
    private final boolean hasNoteIcon;
    private SubHeaderItemClickListener onItemsListener;

    private LinearLayout searchContainer;
    private EditText searchEditText;
    private TextView titleTextView;
    private TextView backTitleTextView;
    private ImageView icNote;
    private ImageView icBack;
    private ImageView icSearch;

    public SubHeaderFragment(String title, String backTitle, boolean hasSearchBar,
                             boolean hasNoteIcon, SubHeaderItemClickListener onItemsListener) {
        this.title = title;
        this.backTitle = backTitle;
        this.hasSearchBar = hasSearchBar;
        this.hasNoteIcon = hasNoteIcon;
        this.onItemsListener = onItemsListener;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_sub_header, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        titleTextView = (TextView) view.findViewById(R.id.subheader_title);
        backTitleTextView = (TextView) view.findViewById(R.id.back_page_title);
        searchContainer = (LinearLayout) view.findViewById(R.id.search_container);
        searchEditText = (EditText) view.findViewById(R.id.search_edit_text);
        icNote = (ImageView) view.findViewById(R.id.note_icon);
        icBack = (ImageView) view.findViewById(R.id.ic_back);
        icSearch = (ImageView) view.findViewById(R.id.ic_search);

        titleTextView.setText(title);
        backTitleTextView.setText(backTitle);
        searchContainer.setVisibility(hasSearchBar ? View.VISIBLE : View.GONE);
        icNote.setVisibility(hasNoteIcon ? View.VISIBLE : View.GONE);
        icBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onItemsListener.onBackListener();
            }
        });
        icSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!TextUtils.isEmpty(searchEditText.getText()))
                    onItemsListener.onSearchListener(searchEditText.getText().toString());
            }
        });
        icNote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onItemsListener.onNoteListener();
            }
        });

    }

    public interface SubHeaderItemClickListener {
        void onBackListener();

        void onSearchListener(String text);

        void onNoteListener();
    }
}