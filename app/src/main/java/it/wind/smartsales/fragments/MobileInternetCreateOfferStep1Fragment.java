package it.wind.smartsales.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;

import it.wind.smartsales.customviews.AspectRatioView;
import it.wind.smartsales.customviews.VerticalTextView;
import it.wind.smartsales.dao.CatalogoQuestionarioDAO;
import it.wind.smartsales.dao.PraticheDAO;
import it.wind.smartsales.entities.Question;
import it.wind.smartsales.entities.Session;
import it.wind.smartsales.R;
import it.wind.smartsales.activity.MainActivity;
import it.wind.smartsales.commons.Constants;
import it.wind.smartsales.commons.Log;
import it.wind.smartsales.commons.Utils;

/**
 * Created by luca.quaranta on 10/02/2016.
 */
public class MobileInternetCreateOfferStep1Fragment extends Fragment {

    private LinearLayout abbonamentoButton, ricaricabileButton, smartphoneButton, tabletButton, appleButton, windowsButton, samsungButton, altroButton, dealButton, dealDeviceButton, transportYesButton, transportNoButton;
    private AspectRatioView abbonamentoArrowUp, abbonamentoArrowDown, ricaricabileArrowUp, ricaricabileArrowDown;
    private TextView abbonamentoNumSim, ricaricabileNumSim;
    private TextView phoneValue, webValue;
    private TextView question1, question2, question3, question4, question5, question6, question7;
    private ArrayList<TextView> questionsText;
    private View phoneStep1, phoneStep2, phoneStep3, phoneStep4, phoneStep5, webStep1, webStep2, webStep3, webStep4, webStep5;
    private View phoneDivider2, phoneDivider3, phoneDivider4, phoneDivider5, webDivider2, webDivider3, webDivider4, webDivider5;
    private VerticalTextView pacchetto1Text, pacchetto2Text, pacchetto3Text, pacchetto4Text;
    private RelativeLayout plusButton, pacchetto1, pacchetto2, pacchetto3, pacchetto4, deletePacchetto2, deletePacchetto3, deletePacchetto4;
    private LinearLayout question7Layout;
    private Spinner spinner1, spinner2;
    private Button continuaButton;
    private int pacchettoVisibile = 1;
    private AspectRatioView pacchetto1Tick, pacchetto2Tick, pacchetto3Tick, pacchetto4Tick;
    private ArrayList<String> listaPaesi1, listaPaesi2;
    private ArrayList<ArrayList<String>> listePaesi;
    private boolean dontCheck;

    private String OPZ1;
    private String OPZ2;

    private void initializeComponents(View view) {
        abbonamentoButton = (LinearLayout) view.findViewById(R.id.abbonamento_button);
        ricaricabileButton = (LinearLayout) view.findViewById(R.id.ricaricabile_button);
        smartphoneButton = (LinearLayout) view.findViewById(R.id.smartphone_button);
        tabletButton = (LinearLayout) view.findViewById(R.id.tablet_button);
        appleButton = (LinearLayout) view.findViewById(R.id.apple_button);
        windowsButton = (LinearLayout) view.findViewById(R.id.windows_button);
        samsungButton = (LinearLayout) view.findViewById(R.id.samsung_button);
        altroButton = (LinearLayout) view.findViewById(R.id.altro_button);
        dealButton = (LinearLayout) view.findViewById(R.id.deal_button);
        dealDeviceButton = (LinearLayout) view.findViewById(R.id.deal_device_button);
        transportYesButton = (LinearLayout) view.findViewById(R.id.transport_yes_button);
        transportNoButton = (LinearLayout) view.findViewById(R.id.transport_no_button);
        question7Layout = (LinearLayout) view.findViewById(R.id.question_7_layout);
        continuaButton = (Button) view.findViewById(R.id.continua_button);

        abbonamentoArrowUp = (AspectRatioView) view.findViewById(R.id.abbonamento_arrow_up);
        abbonamentoArrowDown = (AspectRatioView) view.findViewById(R.id.abbonamento_arrow_down);
        ricaricabileArrowUp = (AspectRatioView) view.findViewById(R.id.ricaricabile_arrow_up);
        ricaricabileArrowDown = (AspectRatioView) view.findViewById(R.id.ricaricabile_arrow_down);
        abbonamentoNumSim = (TextView) view.findViewById(R.id.abbonamento_num_sim);
        ricaricabileNumSim = (TextView) view.findViewById(R.id.ricaricabile_num_sim);

        questionsText = new ArrayList<>();
        questionsText.add(question1 = (TextView) view.findViewById(R.id.question_1));
        questionsText.add(question2 = (TextView) view.findViewById(R.id.question_2));
        questionsText.add(question3 = (TextView) view.findViewById(R.id.question_3));
        questionsText.add(question4 = (TextView) view.findViewById(R.id.question_4));
        questionsText.add(question5 = (TextView) view.findViewById(R.id.question_5));
        questionsText.add(question6 = (TextView) view.findViewById(R.id.question_6));
        questionsText.add(question7 = (TextView) view.findViewById(R.id.question_7));

//        dealButton.setActivated(true);
//        dealDeviceButton.setActivated(true);
//        abbonamentoButton.setActivated(true);
//        ricaricabileButton.setActivated(true);
//        transportYesButton.setActivated(true);
//        transportNoButton.setActivated(true);
//        abbonamentoArrowUp.setEnabled(false);
//        abbonamentoArrowDown.setEnabled(false);
//        ricaricabileArrowUp.setEnabled(false);
//        ricaricabileArrowDown.setEnabled(false);

        phoneStep1 = view.findViewById(R.id.phone_step_1);
        phoneStep2 = view.findViewById(R.id.phone_step_2);
        phoneStep3 = view.findViewById(R.id.phone_step_3);
        phoneStep4 = view.findViewById(R.id.phone_step_4);
        phoneStep5 = view.findViewById(R.id.phone_step_5);

        phoneDivider2 = view.findViewById(R.id.phone_divider_2);
        phoneDivider3 = view.findViewById(R.id.phone_divider_3);
        phoneDivider4 = view.findViewById(R.id.phone_divider_4);
        phoneDivider5 = view.findViewById(R.id.phone_divider_5);

        webStep1 = view.findViewById(R.id.web_step_1);
        webStep2 = view.findViewById(R.id.web_step_2);
        webStep3 = view.findViewById(R.id.web_step_3);
        webStep4 = view.findViewById(R.id.web_step_4);
        webStep5 = view.findViewById(R.id.web_step_5);

        webDivider2 = view.findViewById(R.id.web_divider_2);
        webDivider3 = view.findViewById(R.id.web_divider_3);
        webDivider4 = view.findViewById(R.id.web_divider_4);
        webDivider5 = view.findViewById(R.id.web_divider_5);

        phoneValue = (TextView) view.findViewById(R.id.phone_value);
        webValue = (TextView) view.findViewById(R.id.web_value);

        plusButton = (RelativeLayout) view.findViewById(R.id.plus_button);
        pacchetto1 = (RelativeLayout) view.findViewById(R.id.sede_1);
        pacchetto2 = (RelativeLayout) view.findViewById(R.id.pacchetto_2);
        pacchetto3 = (RelativeLayout) view.findViewById(R.id.pacchetto_3);
        pacchetto4 = (RelativeLayout) view.findViewById(R.id.pacchetto_4);
        deletePacchetto2 = (RelativeLayout) view.findViewById(R.id.delete_pacchetto_2);
        deletePacchetto3 = (RelativeLayout) view.findViewById(R.id.delete_pacchetto_3);
        deletePacchetto4 = (RelativeLayout) view.findViewById(R.id.delete_pacchetto_4);

        pacchetto1Tick = (AspectRatioView) view.findViewById(R.id.pacchetto_1_tick);
        pacchetto2Tick = (AspectRatioView) view.findViewById(R.id.pacchetto_2_tick);
        pacchetto3Tick = (AspectRatioView) view.findViewById(R.id.pacchetto_3_tick);
        pacchetto4Tick = (AspectRatioView) view.findViewById(R.id.pacchetto_4_tick);

        pacchetto1Text = (VerticalTextView) view.findViewById(R.id.pacchetto_1_text);
        pacchetto2Text = (VerticalTextView) view.findViewById(R.id.pacchetto_2_text);
        pacchetto3Text = (VerticalTextView) view.findViewById(R.id.pacchetto_3_text);
        pacchetto4Text = (VerticalTextView) view.findViewById(R.id.pacchetto_4_text);

        listePaesi = new ArrayList<ArrayList<String>>();
        listePaesi.add(Utils.getListaPaesiByTag(getActivity(), Constants.PaesiTag.MOBILE_ZONA_UE_TAG));
        listePaesi.add(Utils.getListaPaesiByTag(getActivity(), Constants.PaesiTag.MOBILE_AREA_1_TAG));
        listePaesi.add(Utils.getListaPaesiByTag(getActivity(), Constants.PaesiTag.MOBILE_USA_TAG));
        listePaesi.add(Utils.getListaPaesiByTag(getActivity(), Constants.PaesiTag.MOBILE_PRINCIPATO_DI_MONACO_TAG));
        listePaesi.add(Utils.getListaPaesiByTag(getActivity(), Constants.PaesiTag.MOBILE_SAN_MARINO_TAG));

        listaPaesi1 = new ArrayList<>();
        listaPaesi1.add(Constants.PaesiName.MOBILE_ZONA_UE_TAG);
        listaPaesi1.add(Constants.PaesiName.MOBILE_AREA_1_TAG);
        listaPaesi1.add(Constants.PaesiName.MOBILE_USA_TAG);
        listaPaesi1.add(Constants.PaesiName.MOBILE_PRINCIPATO_DI_MONACO_TAG);
        listaPaesi1.add(Constants.PaesiName.MOBILE_SAN_MARINO_TAG);

//        for (ArrayList<String> lista : listePaesi) {
//            listaPaesi1.addAll(lista);
//        }

//        Collections.sort(listaPaesi1, new Comparator<String>() {
//            @Override
//            public int compare(String text1, String text2) {
//                return text1.compareToIgnoreCase(text2);
//            }
//        });

        listaPaesi2 = new ArrayList<>(listaPaesi1);



//        listaPaesi1.add(0, "Opzione 1");
//        listaPaesi2.add(0, "Opzione 2");

        OPZ1 = getActivity().getResources().getString(R.string.step1_opzione_1);
        OPZ2 = getActivity().getResources().getString(R.string.step1_opzione_2);


        listaPaesi1.add(0, OPZ1);
        listaPaesi2.add(0, OPZ2);



        spinner1 = (Spinner) view.findViewById(R.id.spinner_1);
        ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, listaPaesi1);
        adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner1.setAdapter(adapter1);

        spinner2 = (Spinner) view.findViewById(R.id.spinner_2);
        ArrayAdapter<String> adapter2 = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, listaPaesi2);
        adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner2.setAdapter(adapter2);


    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_mobile_internet_create_offer_step_1, container, false);
        initializeComponents(view);

        ArrayList<Question> questions = CatalogoQuestionarioDAO.retrieveQuestions(getActivity(), "Mobile");
        for (Question question : questions) {
            questionsText.get(Integer.parseInt(question.getQuestionNumber()) - 1).setText(question.getQuestionDescription());
        }

        populatePage(1);
        checkValidation();


//        abbonamentoButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (!v.isSelected()) {
//                    v.setSelected(true);
//                    abbonamentoArrowUp.setActivated(true);
//                    abbonamentoArrowUp.setEnabled(true);
//                    abbonamentoArrowDown.setActivated(true);
//                    abbonamentoArrowDown.setEnabled(true);
//                    abbonamentoNumSim.setActivated(true);
//                    try {
//                        ((JSONObject) Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONArray("pacchetti").get(pacchettoVisibile - 1)).put("question4Abbonamento", true);
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                    }
//                } else {
//                    v.setSelected(false);
//                    abbonamentoArrowUp.setActivated(false);
//                    abbonamentoArrowUp.setEnabled(false);
//                    abbonamentoArrowDown.setActivated(false);
//                    abbonamentoArrowDown.setEnabled(false);
//                    abbonamentoNumSim.setActivated(false);
//                    abbonamentoNumSim.setText(new DecimalFormat("000").format(0));
//                    try {
//                        ((JSONObject) Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONArray("pacchetti").get(pacchettoVisibile - 1)).put("question4Abbonamento", false);
//                        ((JSONObject) Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONArray("pacchetti").get(pacchettoVisibile - 1)).put("question4AbbonamentoNumSim", 0);
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                    }
//                }
//                checkValidationPacchetto();
//            }
//        });

//        ricaricabileButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (!v.isSelected()) {
//                    v.setSelected(true);
//                    ricaricabileArrowUp.setActivated(true);
//                    ricaricabileArrowUp.setEnabled(true);
//                    ricaricabileArrowDown.setActivated(true);
//                    ricaricabileArrowDown.setEnabled(true);
//                    ricaricabileNumSim.setActivated(true);
//                    try {
//                        ((JSONObject) Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONArray("pacchetti").get(pacchettoVisibile - 1)).put("question4Ricaricabile", true);
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                    }
//                } else {
//                    v.setSelected(false);
//                    ricaricabileArrowUp.setActivated(false);
//                    ricaricabileArrowUp.setEnabled(false);
//                    ricaricabileArrowDown.setActivated(false);
//                    ricaricabileArrowDown.setEnabled(false);
//                    ricaricabileNumSim.setActivated(false);
//                    ricaricabileNumSim.setText(new DecimalFormat("000").format(0));
//                    try {
//                        ((JSONObject) Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONArray("pacchetti").get(pacchettoVisibile - 1)).put("question4Ricaricabile", false);
//                        ((JSONObject) Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONArray("pacchetti").get(pacchettoVisibile - 1)).put("question4RicaricabileNumSim", 0);
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                    }
//                }
//                checkValidationPacchetto();
//            }
//        });

        smartphoneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.setSelected(!v.isSelected());
                try {
                    ((JSONObject) Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONArray("pacchetti").get(pacchettoVisibile - 1)).put("question2Smartphone", v.isSelected());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                checkValidationPacchetto();
            }
        });

        tabletButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.setSelected(!v.isSelected());
                try {
                    ((JSONObject) Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONArray("pacchetti").get(pacchettoVisibile - 1)).put("question2Tablet", v.isSelected());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                checkValidationPacchetto();
            }
        });

        appleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.setSelected(!v.isSelected());
                try {
                    ((JSONObject) Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONArray("pacchetti").get(pacchettoVisibile - 1)).put("question3Apple", v.isSelected());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                checkValidationPacchetto();
            }
        });

        windowsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.setSelected(!v.isSelected());
                try {
                    ((JSONObject) Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONArray("pacchetti").get(pacchettoVisibile - 1)).put("question3Windows", v.isSelected());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                checkValidationPacchetto();
            }
        });

        samsungButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.setSelected(!v.isSelected());
                try {
                    ((JSONObject) Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONArray("pacchetti").get(pacchettoVisibile - 1)).put("question3Samsung", v.isSelected());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                checkValidationPacchetto();
            }
        });

        altroButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.setSelected(!v.isSelected());
                try {
                    ((JSONObject) Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONArray("pacchetti").get(pacchettoVisibile - 1)).put("question3Altro", v.isSelected());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                checkValidationPacchetto();
            }
        });

        dealButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!v.isSelected()) {
                    v.setSelected(true);
                    try {
                        ((JSONObject) Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONArray("pacchetti").get(pacchettoVisibile - 1)).put("question1Offerta", true);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    if (dealDeviceButton.isSelected()) {
                        dealDeviceButton.setSelected(false);
                        try {
                            ((JSONObject) Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONArray("pacchetti").get(pacchettoVisibile - 1)).put("question1OffertaDevice", false);
                            ((JSONObject) Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONArray("pacchetti").get(pacchettoVisibile - 1)).put("question2Smartphone", false);
                            ((JSONObject) Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONArray("pacchetti").get(pacchettoVisibile - 1)).put("question2Tablet", false);
                            ((JSONObject) Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONArray("pacchetti").get(pacchettoVisibile - 1)).put("question3Apple", false);
                            ((JSONObject) Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONArray("pacchetti").get(pacchettoVisibile - 1)).put("question3Windows", false);
                            ((JSONObject) Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONArray("pacchetti").get(pacchettoVisibile - 1)).put("question3Samsung", false);
                            ((JSONObject) Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONArray("pacchetti").get(pacchettoVisibile - 1)).put("question3Altro", false);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    if (smartphoneButton.isActivated()) {
                        smartphoneButton.setActivated(false);
                        smartphoneButton.setEnabled(false);
                        smartphoneButton.setSelected(false);
                        tabletButton.setActivated(false);
                        tabletButton.setEnabled(false);
                        tabletButton.setSelected(false);

                        appleButton.setActivated(false);
                        appleButton.setEnabled(false);
                        appleButton.setSelected(false);
                        windowsButton.setActivated(false);
                        windowsButton.setEnabled(false);
                        windowsButton.setSelected(false);
                        samsungButton.setActivated(false);
                        samsungButton.setEnabled(false);
                        samsungButton.setSelected(false);
                        altroButton.setActivated(false);
                        altroButton.setEnabled(false);
                        altroButton.setSelected(false);

                        question2.setActivated(false);
                        question3.setActivated(false);
                    }
                }
                checkValidationPacchetto();
            }
        });

        dealDeviceButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!v.isSelected()) {
                    v.setSelected(true);
                    try {
                        ((JSONObject) Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONArray("pacchetti").get(pacchettoVisibile - 1)).put("question1OffertaDevice", true);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    if (dealButton.isSelected()) {
                        dealButton.setSelected(false);
                        try {
                            ((JSONObject) Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONArray("pacchetti").get(pacchettoVisibile - 1)).put("question1Offerta", false);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    if (!smartphoneButton.isActivated()) {
                        smartphoneButton.setActivated(true);
                        smartphoneButton.setEnabled(true);
                        smartphoneButton.setSelected(false);
                        tabletButton.setActivated(true);
                        tabletButton.setEnabled(true);
                        tabletButton.setSelected(false);

                        appleButton.setActivated(true);
                        appleButton.setEnabled(true);
                        appleButton.setSelected(false);
                        windowsButton.setActivated(true);
                        windowsButton.setEnabled(true);
                        windowsButton.setSelected(false);
                        samsungButton.setActivated(true);
                        samsungButton.setEnabled(true);
                        samsungButton.setSelected(false);
                        altroButton.setActivated(true);
                        altroButton.setEnabled(true);
                        altroButton.setSelected(false);

                        question2.setActivated(true);
                        question3.setActivated(true);
                    }
                }
                checkValidationPacchetto();
            }
        });

        transportYesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.setSelected(!v.isSelected());
                try {
                    ((JSONObject) Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONArray("pacchetti").get(pacchettoVisibile - 1)).put("question6Si", v.isSelected());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (transportNoButton.isSelected()) {
                    transportNoButton.setSelected(false);
                    try {
                        ((JSONObject) Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONArray("pacchetti").get(pacchettoVisibile - 1)).put("question6No", false);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                if (v.isSelected()) {
                    transportNoButton.setVisibility(View.GONE);
                    question7Layout.setVisibility(View.VISIBLE);
                } else {
                    transportNoButton.setVisibility(View.VISIBLE);
                    question7Layout.setVisibility(View.GONE);
                    try {
                        ((JSONObject) Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONArray("pacchetti").get(pacchettoVisibile - 1)).put("question7Scelta1", listaPaesi1.get(0));
                        ((JSONObject) Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONArray("pacchetti").get(pacchettoVisibile - 1)).put("question7Scelta2", listaPaesi1.get(0));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                checkValidationPacchetto();
            }
        });

        transportNoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.setSelected(!v.isSelected());
                try {
                    ((JSONObject) Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONArray("pacchetti").get(pacchettoVisibile - 1)).put("question6No", v.isSelected());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (transportYesButton.isSelected()) {
                    transportYesButton.setSelected(false);
                    try {
                        ((JSONObject) Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONArray("pacchetti").get(pacchettoVisibile - 1)).put("question6Si", false);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                checkValidationPacchetto();
            }
        });

        abbonamentoArrowUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                incrementAbbonamentoNumSim();
            }
        });

        abbonamentoArrowDown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                decrementAbbonamentoNumSim();
            }
        });

        ricaricabileArrowUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                incrementRicaricabileNumSim();
            }
        });

        ricaricabileArrowDown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                decrementRicaricabileNumSim();
            }
        });

        phoneStep1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setPhoneStep(1);
            }
        });

        phoneStep2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setPhoneStep(2);
            }
        });

        phoneStep3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setPhoneStep(3);
            }
        });

        phoneStep4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setPhoneStep(4);
            }
        });

        phoneStep5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setPhoneStep(5);
            }
        });

        webStep1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setWebStep(1);
            }
        });

        webStep2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setWebStep(2);
            }
        });

        webStep3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setWebStep(3);
            }
        });

        webStep4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setWebStep(4);
            }
        });

        webStep5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setWebStep(5);
            }
        });

        plusButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONArray("pacchetti").put(Utils.createPacchetto());

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (pacchetto3.getVisibility() == View.VISIBLE) {
                    pacchetto4.setVisibility(View.VISIBLE);
                    openPacchetto(4);
                } else if (pacchetto2.getVisibility() == View.VISIBLE) {
                    pacchetto3.setVisibility(View.VISIBLE);
                    openPacchetto(3);
                } else if (pacchetto2.getVisibility() == View.GONE) {
                    pacchetto2.setVisibility(View.VISIBLE);
                    openPacchetto(2);
                }
                checkValidation();
                checkValidationPacchetto();
            }
        });

        deletePacchetto2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                removePacchetto(2);
            }
        });

        deletePacchetto3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                removePacchetto(3);
            }
        });

        deletePacchetto4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                removePacchetto(3);
            }
        });

        pacchetto1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openPacchetto(1);
            }
        });

        pacchetto2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openPacchetto(2);
            }
        });

        pacchetto3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openPacchetto(3);
            }
        });

        pacchetto4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openPacchetto(4);
            }
        });

        continuaButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Session.getInstance().getPratica().setStep(PraticheDAO.STEP2);
                Log.d("JSON PRATICA)", Session.getInstance().getJsonPratica().toString());
                PraticheDAO.updatePratica(getActivity().getBaseContext());

                ((MainActivity) getActivity()).goForward();
            }
        });

        abbonamentoArrowUp.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                mAutoIncrementAbbonamento = true;
                repeatUpdateHandlerAbbonamento.post(new RptUpdaterAbbonamento());
                return false;
            }
        });

        abbonamentoArrowUp.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if ((event.getAction() == MotionEvent.ACTION_UP || event.getAction() == MotionEvent.ACTION_CANCEL) && mAutoIncrementAbbonamento) {
                    mAutoIncrementAbbonamento = false;
                }
                return false;
            }
        });

        abbonamentoArrowDown.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                mAutoDecrementAbbonamento = true;
                repeatUpdateHandlerAbbonamento.post(new RptUpdaterAbbonamento());
                return false;
            }
        });

        abbonamentoArrowDown.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if ((event.getAction() == MotionEvent.ACTION_UP || event.getAction() == MotionEvent.ACTION_CANCEL) && mAutoDecrementAbbonamento) {
                    mAutoDecrementAbbonamento = false;
                }
                return false;
            }
        });

        ricaricabileArrowUp.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                mAutoIncrementRicaricabile = true;
                repeatUpdateHandlerRicaricabile.post(new RptUpdaterRicaricabile());
                return false;
            }
        });

        ricaricabileArrowUp.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if ((event.getAction() == MotionEvent.ACTION_UP || event.getAction() == MotionEvent.ACTION_CANCEL) && mAutoIncrementRicaricabile) {
                    mAutoIncrementRicaricabile = false;
                }
                return false;
            }
        });

        ricaricabileArrowDown.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                mAutoDecrementRicaricabile = true;
                repeatUpdateHandlerRicaricabile.post(new RptUpdaterRicaricabile());
                return false;
            }
        });

        ricaricabileArrowDown.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if ((event.getAction() == MotionEvent.ACTION_UP || event.getAction() == MotionEvent.ACTION_CANCEL) && mAutoDecrementAbbonamento) {
                    mAutoDecrementRicaricabile = false;
                }
                return false;
            }
        });

        spinner1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                try {
//                    String zona = "";
//                    for (int i = 0; i<listePaesi.size(); i++){
//                        if (listePaesi.get(i).contains(listaPaesi1.get(position))){
//                            switch (i){
//                                case 0:
//                                    zona = "Zona UE";
//                                    break;
//                                case 1:
//                                    zona = "Area1";
//                                    break;
//                                case 2:
//                                    zona = "USA";
//                                    break;
//                                case 3:
//                                    zona = "Principato di Monaco";
//                                    break;
//                                case 4:
//                                    zona = "San Marino";
//                                    break;
//                            }
//                        }
//                    }
//                    ((JSONObject) Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONArray("pacchetti").get(pacchettoVisibile - 1)).put("question7Scelta1", zona);
                    ((JSONObject) Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONArray("pacchetti").get(pacchettoVisibile - 1)).put("question7Scelta1", listaPaesi1.get(position));

                    if (listaPaesi1.get(position).compareToIgnoreCase(OPZ1) == 0) {
                        spinner2.setSelection(0);
                        spinner2.setEnabled(false);
                    } else {

                        //elimino quanto ho selezionato nel primo spinner dal secondo
                        ArrayList<String> listaPaesi2Filtered = new ArrayList<>(listaPaesi1);
                        listaPaesi2Filtered.remove(listaPaesi1.get(position));
                        listaPaesi2Filtered.set(0,OPZ2);

                        ((ArrayAdapter)spinner2.getAdapter()).clear();
                        ((ArrayAdapter)spinner2.getAdapter()).addAll(listaPaesi2Filtered);
                        ((ArrayAdapter)spinner2.getAdapter()).notifyDataSetChanged();
                        //-----------------------


                        spinner2.setEnabled(true);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (!dontCheck) {
                    checkValidationPacchetto();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinner2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                try {
//                    String zona = "";
//                    for (int i = 0; i<listePaesi.size(); i++){
//                        if (listePaesi.get(i).contains(listaPaesi1.get(position))){
//                            switch (i){
//                                case 0:
//                                    zona = "Zona UE";
//                                    break;
//                                case 1:
//                                    zona = "Area1";
//                                    break;
//                                case 2:
//                                    zona = "USA";
//                                    break;
//                                case 3:
//                                    zona = "Principato di Monaco";
//                                    break;
//                                case 4:
//                                    zona = "San Marino";
//                                    break;
//                            }
//                        }
//                    }
//                    ((JSONObject) Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONArray("pacchetti").get(pacchettoVisibile - 1)).put("question7Scelta2", zona);
                    ((JSONObject) Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONArray("pacchetti").get(pacchettoVisibile - 1)).put("question7Scelta2", listaPaesi2.get(position));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (!dontCheck) {
                    checkValidationPacchetto();
                }
                dontCheck = false;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        return view;
    }

    private static final int REP_DELAY = 100;
    private boolean mAutoIncrementAbbonamento = false;
    private boolean mAutoDecrementAbbonamento = false;
    private Handler repeatUpdateHandlerAbbonamento = new Handler();
    private boolean mAutoIncrementRicaricabile = false;
    private boolean mAutoDecrementRicaricabile = false;
    private Handler repeatUpdateHandlerRicaricabile = new Handler();

    class RptUpdaterAbbonamento implements Runnable {
        public void run() {
            if (mAutoIncrementAbbonamento) {
                incrementAbbonamentoNumSim();
                repeatUpdateHandlerAbbonamento.postDelayed(new RptUpdaterAbbonamento(), REP_DELAY);
            } else if (mAutoDecrementAbbonamento) {
                decrementAbbonamentoNumSim();
                repeatUpdateHandlerAbbonamento.postDelayed(new RptUpdaterAbbonamento(), REP_DELAY);
            }
        }
    }

    class RptUpdaterRicaricabile implements Runnable {
        public void run() {
            if (mAutoIncrementRicaricabile) {
                incrementRicaricabileNumSim();
                repeatUpdateHandlerRicaricabile.postDelayed(new RptUpdaterRicaricabile(), REP_DELAY);
            } else if (mAutoDecrementRicaricabile) {
                decrementRicaricabileNumSim();
                repeatUpdateHandlerRicaricabile.postDelayed(new RptUpdaterRicaricabile(), REP_DELAY);
            }
        }
    }

    private void incrementAbbonamentoNumSim() {
        int i = Integer.parseInt(abbonamentoNumSim.getText().toString());
        if (i < 999) {
            abbonamentoNumSim.setText(new DecimalFormat("000").format(++i));
            try {
                ((JSONObject) Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONArray("pacchetti").get(pacchettoVisibile - 1)).put("question4AbbonamentoNumSim", i);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        checkValidationPacchetto();
    }

    private void decrementAbbonamentoNumSim() {
        int i = Integer.parseInt(abbonamentoNumSim.getText().toString());
        if (i > 0) {
            abbonamentoNumSim.setText(new DecimalFormat("000").format(--i));
            try {
                ((JSONObject) Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONArray("pacchetti").get(pacchettoVisibile - 1)).put("question4AbbonamentoNumSim", i);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        checkValidationPacchetto();
    }

    private void incrementRicaricabileNumSim() {
        int i = Integer.parseInt(ricaricabileNumSim.getText().toString());
        if (i < 999) {
            ricaricabileNumSim.setText(new DecimalFormat("000").format(++i));
            try {
                ((JSONObject) Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONArray("pacchetti").get(pacchettoVisibile - 1)).put("question4RicaricabileNumSim", i);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        checkValidationPacchetto();
    }

    private void decrementRicaricabileNumSim() {
        int i = Integer.parseInt(ricaricabileNumSim.getText().toString());
        if (i > 0) {
            ricaricabileNumSim.setText(new DecimalFormat("000").format(--i));
            try {
                ((JSONObject) Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONArray("pacchetti").get(pacchettoVisibile - 1)).put("question4RicaricabileNumSim", i);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        checkValidationPacchetto();
    }

    private void removePacchetto(int numPacchetto) {
        try {
            openPacchetto(1);
            Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONArray("pacchetti").remove(numPacchetto - 1);
            switch (numPacchetto) {
                case 2:
                    if (pacchetto4.getVisibility() == View.VISIBLE) {
                        pacchetto4.setVisibility(View.GONE);
                    } else if (pacchetto3.getVisibility() == View.VISIBLE) {
                        pacchetto3.setVisibility(View.GONE);
                    } else if (pacchetto2.getVisibility() == View.VISIBLE) {
                        pacchetto2.setVisibility(View.GONE);
                    }
                    break;
                case 3:
                    if (pacchetto4.getVisibility() == View.VISIBLE) {
                        pacchetto4.setVisibility(View.GONE);
                    } else if (pacchetto3.getVisibility() == View.VISIBLE) {
                        pacchetto3.setVisibility(View.GONE);
                    }
                    break;
                case 4:
                    if (pacchetto4.getVisibility() == View.VISIBLE) {
                        pacchetto4.setVisibility(View.GONE);
                    }
                    break;
            }
            checkValidation();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void openPacchetto(int numPacchetto) {
        switch (numPacchetto) {
            case 1:
                pacchetto1.setBackgroundColor(getActivity().getResources().getColor(android.R.color.white));
                pacchetto2.setBackgroundColor(getActivity().getResources().getColor(R.color.wind_color_transparent_light_gray));
                pacchetto3.setBackgroundColor(getActivity().getResources().getColor(R.color.wind_color_transparent_light_gray));
                pacchetto4.setBackgroundColor(getActivity().getResources().getColor(R.color.wind_color_transparent_light_gray));
                pacchetto1Text.setTextColor(getActivity().getResources().getColor(R.color.colorAccent));
                pacchetto2Text.setTextColor(getActivity().getResources().getColor(android.R.color.black));
                pacchetto3Text.setTextColor(getActivity().getResources().getColor(android.R.color.black));
                pacchetto4Text.setTextColor(getActivity().getResources().getColor(android.R.color.black));

                pacchettoVisibile = 1;
                break;

            case 2:
                pacchetto1.setBackgroundColor(getActivity().getResources().getColor(R.color.wind_color_transparent_light_gray));
                pacchetto2.setBackgroundColor(getActivity().getResources().getColor(android.R.color.white));
                pacchetto3.setBackgroundColor(getActivity().getResources().getColor(R.color.wind_color_transparent_light_gray));
                pacchetto4.setBackgroundColor(getActivity().getResources().getColor(R.color.wind_color_transparent_light_gray));
                pacchetto1Text.setTextColor(getActivity().getResources().getColor(android.R.color.black));
                pacchetto2Text.setTextColor(getActivity().getResources().getColor(R.color.colorAccent));
                pacchetto3Text.setTextColor(getActivity().getResources().getColor(android.R.color.black));
                pacchetto4Text.setTextColor(getActivity().getResources().getColor(android.R.color.black));

                pacchettoVisibile = 2;
                break;

            case 3:
                pacchetto1.setBackgroundColor(getActivity().getResources().getColor(R.color.wind_color_transparent_light_gray));
                pacchetto2.setBackgroundColor(getActivity().getResources().getColor(R.color.wind_color_transparent_light_gray));
                pacchetto3.setBackgroundColor(getActivity().getResources().getColor(android.R.color.white));
                pacchetto4.setBackgroundColor(getActivity().getResources().getColor(R.color.wind_color_transparent_light_gray));
                pacchetto1Text.setTextColor(getActivity().getResources().getColor(android.R.color.black));
                pacchetto2Text.setTextColor(getActivity().getResources().getColor(android.R.color.black));
                pacchetto3Text.setTextColor(getActivity().getResources().getColor(R.color.colorAccent));
                pacchetto4Text.setTextColor(getActivity().getResources().getColor(android.R.color.black));

                pacchettoVisibile = 3;
                break;

            case 4:
                pacchetto1.setBackgroundColor(getActivity().getResources().getColor(R.color.wind_color_transparent_light_gray));
                pacchetto2.setBackgroundColor(getActivity().getResources().getColor(R.color.wind_color_transparent_light_gray));
                pacchetto3.setBackgroundColor(getActivity().getResources().getColor(R.color.wind_color_transparent_light_gray));
                pacchetto1Text.setTextColor(getActivity().getResources().getColor(android.R.color.black));
                pacchetto2Text.setTextColor(getActivity().getResources().getColor(android.R.color.black));
                pacchetto3Text.setTextColor(getActivity().getResources().getColor(android.R.color.black));
                pacchetto4Text.setTextColor(getActivity().getResources().getColor(R.color.colorAccent));

                pacchetto4.setBackgroundColor(getActivity().getResources().getColor(android.R.color.white));
                pacchettoVisibile = 4;
                break;
        }
        populatePage(pacchettoVisibile);
    }

    private void setPhoneStep(int step) {
        try {
            ((JSONObject) Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONArray("pacchetti").get(pacchettoVisibile - 1)).put("question5Phone", step);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        switch (step) {
            case 1:
                phoneDivider2.setBackgroundColor(getActivity().getResources().getColor(R.color.wind_color_light_gray));
                phoneDivider3.setBackgroundColor(getActivity().getResources().getColor(R.color.wind_color_light_gray));
                phoneDivider4.setBackgroundColor(getActivity().getResources().getColor(R.color.wind_color_light_gray));
                phoneDivider5.setBackgroundColor(getActivity().getResources().getColor(R.color.wind_color_light_gray));
                phoneValue.setText("Solo Dati");
                break;
            case 2:
                phoneDivider2.setBackgroundColor(getActivity().getResources().getColor(R.color.colorAccent));
                phoneDivider3.setBackgroundColor(getActivity().getResources().getColor(R.color.wind_color_light_gray));
                phoneDivider4.setBackgroundColor(getActivity().getResources().getColor(R.color.wind_color_light_gray));
                phoneDivider5.setBackgroundColor(getActivity().getResources().getColor(R.color.wind_color_light_gray));
                phoneValue.setText("200");
                break;
            case 3:
                phoneDivider2.setBackgroundColor(getActivity().getResources().getColor(R.color.colorAccent));
                phoneDivider3.setBackgroundColor(getActivity().getResources().getColor(R.color.colorAccent));
                phoneDivider4.setBackgroundColor(getActivity().getResources().getColor(R.color.wind_color_light_gray));
                phoneDivider5.setBackgroundColor(getActivity().getResources().getColor(R.color.wind_color_light_gray));
                phoneValue.setText("400");
                break;
            case 4:
                phoneDivider2.setBackgroundColor(getActivity().getResources().getColor(R.color.colorAccent));
                phoneDivider3.setBackgroundColor(getActivity().getResources().getColor(R.color.colorAccent));
                phoneDivider4.setBackgroundColor(getActivity().getResources().getColor(R.color.colorAccent));
                phoneDivider5.setBackgroundColor(getActivity().getResources().getColor(R.color.wind_color_light_gray));
                phoneValue.setText("600");
                break;
            case 5:
                phoneDivider2.setBackgroundColor(getActivity().getResources().getColor(R.color.colorAccent));
                phoneDivider3.setBackgroundColor(getActivity().getResources().getColor(R.color.colorAccent));
                phoneDivider4.setBackgroundColor(getActivity().getResources().getColor(R.color.colorAccent));
                phoneDivider5.setBackgroundColor(getActivity().getResources().getColor(R.color.colorAccent));
                phoneValue.setText("Illimitati");
                break;
        }

        //store data on sqlLite
        PraticheDAO.updatePratica(this.getActivity().getBaseContext());
//        checkValidationPacchetto();
    }

    private void setWebStep(int step) {
        try {
            ((JSONObject) Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONArray("pacchetti").get(pacchettoVisibile - 1)).put("question5Web", step);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        switch (step) {
            case 1:
                webDivider2.setBackgroundColor(getActivity().getResources().getColor(R.color.wind_color_light_gray));
                webDivider3.setBackgroundColor(getActivity().getResources().getColor(R.color.wind_color_light_gray));
                webDivider4.setBackgroundColor(getActivity().getResources().getColor(R.color.wind_color_light_gray));
                webDivider5.setBackgroundColor(getActivity().getResources().getColor(R.color.wind_color_light_gray));
                webValue.setText("2");
                break;
            case 2:
                webDivider2.setBackgroundColor(getActivity().getResources().getColor(R.color.colorAccent));
                webDivider3.setBackgroundColor(getActivity().getResources().getColor(R.color.wind_color_light_gray));
                webDivider4.setBackgroundColor(getActivity().getResources().getColor(R.color.wind_color_light_gray));
                webDivider5.setBackgroundColor(getActivity().getResources().getColor(R.color.wind_color_light_gray));
                webValue.setText("3");
                break;
            case 3:
                webDivider2.setBackgroundColor(getActivity().getResources().getColor(R.color.colorAccent));
                webDivider3.setBackgroundColor(getActivity().getResources().getColor(R.color.colorAccent));
                webDivider4.setBackgroundColor(getActivity().getResources().getColor(R.color.wind_color_light_gray));
                webDivider5.setBackgroundColor(getActivity().getResources().getColor(R.color.wind_color_light_gray));
                webValue.setText("5");
                break;
            case 4:
                webDivider2.setBackgroundColor(getActivity().getResources().getColor(R.color.colorAccent));
                webDivider3.setBackgroundColor(getActivity().getResources().getColor(R.color.colorAccent));
                webDivider4.setBackgroundColor(getActivity().getResources().getColor(R.color.colorAccent));
                webDivider5.setBackgroundColor(getActivity().getResources().getColor(R.color.wind_color_light_gray));
                webValue.setText("10");
                break;
            case 5:
                webDivider2.setBackgroundColor(getActivity().getResources().getColor(R.color.colorAccent));
                webDivider3.setBackgroundColor(getActivity().getResources().getColor(R.color.colorAccent));
                webDivider4.setBackgroundColor(getActivity().getResources().getColor(R.color.colorAccent));
                webDivider5.setBackgroundColor(getActivity().getResources().getColor(R.color.colorAccent));
                webValue.setText("25+");
                break;
        }

        //store data on sqlLite
        PraticheDAO.updatePratica(this.getActivity().getBaseContext());
//        checkValidationPacchetto();
    }

    private void populatePage(int pacchettoIndex) {
        try {
            JSONObject jsonPratica = Session.getInstance().getJsonPratica().getJSONObject("pratica");

            JSONArray pacchetti = jsonPratica.getJSONArray("pacchetti");
            switch (pacchetti.length()) {
                case 4:
                    pacchetto4.setVisibility(View.VISIBLE);
                case 3:
                    pacchetto3.setVisibility(View.VISIBLE);
                case 2:
                    pacchetto2.setVisibility(View.VISIBLE);
            }

            JSONObject pacchetto = pacchetti.getJSONObject(pacchettoIndex - 1);

            dealButton.setActivated(true);
            dealDeviceButton.setActivated(true);
            if (pacchetto.getBoolean("question1Offerta")) {
                dealButton.setSelected(true);
                dealDeviceButton.setSelected(false);
                smartphoneButton.setActivated(false);
                smartphoneButton.setEnabled(false);
                smartphoneButton.setSelected(false);
                tabletButton.setActivated(false);
                tabletButton.setEnabled(false);
                tabletButton.setSelected(false);

                appleButton.setActivated(false);
                appleButton.setEnabled(false);
                appleButton.setSelected(false);
                windowsButton.setActivated(false);
                windowsButton.setEnabled(false);
                windowsButton.setSelected(false);
                samsungButton.setActivated(false);
                samsungButton.setEnabled(false);
                samsungButton.setSelected(false);
                altroButton.setActivated(false);
                altroButton.setEnabled(false);
                altroButton.setSelected(false);

                question2.setActivated(false);
                question3.setActivated(false);
            } else if (pacchetto.getBoolean("question1OffertaDevice")) {
                dealButton.setSelected(false);
                dealDeviceButton.setSelected(true);
                smartphoneButton.setActivated(true);
                smartphoneButton.setEnabled(true);
                smartphoneButton.setSelected(false);
                tabletButton.setActivated(true);
                tabletButton.setEnabled(true);
                tabletButton.setSelected(false);

                appleButton.setActivated(true);
                appleButton.setEnabled(true);
                appleButton.setSelected(false);
                windowsButton.setActivated(true);
                windowsButton.setEnabled(true);
                windowsButton.setSelected(false);
                samsungButton.setActivated(true);
                samsungButton.setEnabled(true);
                samsungButton.setSelected(false);
                altroButton.setActivated(true);
                altroButton.setEnabled(true);
                altroButton.setSelected(false);

                question2.setActivated(true);
                question3.setActivated(true);
            } else {
                dealButton.setSelected(false);
                dealDeviceButton.setSelected(false);
                smartphoneButton.setActivated(false);
                smartphoneButton.setEnabled(false);
                smartphoneButton.setSelected(false);
                tabletButton.setActivated(false);
                tabletButton.setEnabled(false);
                tabletButton.setSelected(false);

                appleButton.setActivated(false);
                appleButton.setEnabled(false);
                appleButton.setSelected(false);
                windowsButton.setActivated(false);
                windowsButton.setEnabled(false);
                windowsButton.setSelected(false);
                samsungButton.setActivated(false);
                samsungButton.setEnabled(false);
                samsungButton.setSelected(false);
                altroButton.setActivated(false);
                altroButton.setEnabled(false);
                altroButton.setSelected(false);

                question2.setActivated(false);
                question3.setActivated(false);
            }

            if (pacchetto.getBoolean("question2Smartphone")) {
                smartphoneButton.setSelected(true);
            } else {
                smartphoneButton.setSelected(false);
            }

            if (pacchetto.getBoolean("question2Tablet")) {
                tabletButton.setSelected(true);
            } else {
                tabletButton.setSelected(false);
            }

            if (pacchetto.getBoolean("question3Apple")) {
                appleButton.setSelected(true);
            } else {
                appleButton.setSelected(false);
            }

            if (pacchetto.getBoolean("question3Windows")) {
                windowsButton.setSelected(true);
            } else {
                windowsButton.setSelected(false);
            }

            if (pacchetto.getBoolean("question3Samsung")) {
                samsungButton.setSelected(true);
            } else {
                samsungButton.setSelected(false);
            }

            if (pacchetto.getBoolean("question3Altro")) {
                altroButton.setSelected(true);
            } else {
                altroButton.setSelected(false);
            }

//            abbonamentoButton.setActivated(true);
//            if (pacchetto.getBoolean("question4Abbonamento")) {
//                abbonamentoButton.setSelected(true);
//                abbonamentoArrowUp.setActivated(true);
//                abbonamentoArrowUp.setEnabled(true);
//                abbonamentoArrowDown.setActivated(true);
//                abbonamentoArrowDown.setEnabled(true);
//                abbonamentoNumSim.setActivated(true);
//            } else {
//                abbonamentoButton.setSelected(false);
//                abbonamentoArrowUp.setActivated(false);
//                abbonamentoArrowUp.setEnabled(false);
//                abbonamentoArrowDown.setActivated(false);
//                abbonamentoArrowDown.setEnabled(false);
//                abbonamentoNumSim.setActivated(false);
//            }
            abbonamentoNumSim.setText(new DecimalFormat("000").format(pacchetto.getInt("question4AbbonamentoNumSim")));

//            ricaricabileButton.setActivated(true);
//            if (pacchetto.getBoolean("question4Ricaricabile")) {
//                ricaricabileButton.setSelected(true);
//                ricaricabileArrowUp.setActivated(true);
//                ricaricabileArrowUp.setEnabled(true);
//                ricaricabileArrowDown.setActivated(true);
//                ricaricabileArrowDown.setEnabled(true);
//                ricaricabileNumSim.setActivated(true);
//            } else {
//                ricaricabileButton.setSelected(false);
//                ricaricabileArrowUp.setActivated(false);
//                ricaricabileArrowUp.setEnabled(false);
//                ricaricabileArrowDown.setActivated(false);
//                ricaricabileArrowDown.setEnabled(false);
//                ricaricabileNumSim.setActivated(false);
//            }
            ricaricabileNumSim.setText(new DecimalFormat("000").format(pacchetto.getInt("question4RicaricabileNumSim")));

            setPhoneStep(pacchetto.getInt("question5Phone"));
            setWebStep(pacchetto.getInt("question5Web"));

            transportYesButton.setActivated(true);
            transportYesButton.setSelected(pacchetto.getBoolean("question6Si"));
            if (transportYesButton.isSelected()) {
                if (transportNoButton.isSelected()) {
                    transportNoButton.setSelected(false);
                }
                transportNoButton.setVisibility(View.GONE);
                question7Layout.setVisibility(View.VISIBLE);
            } else {
                transportNoButton.setVisibility(View.VISIBLE);
                question7Layout.setVisibility(View.GONE);
            }

            dontCheck = true;
            spinner1.setSelection(listaPaesi1.indexOf(((JSONObject) Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONArray("pacchetti").get(pacchettoVisibile - 1)).getString("question7Scelta1")));
            spinner2.setSelection(listaPaesi1.indexOf(((JSONObject) Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONArray("pacchetti").get(pacchettoVisibile - 1)).getString("question7Scelta2")));

            transportNoButton.setActivated(true);
            transportNoButton.setSelected(pacchetto.getBoolean("question6No"));
            if (transportNoButton.isSelected()) {
                if (transportYesButton.isSelected()) {
                    transportYesButton.setSelected(false);
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void changeTickVisibility(int pacchetto, int visibility) {
        switch (pacchetto) {
            case 1:
                pacchetto1Tick.setVisibility(visibility);
                break;
            case 2:
                pacchetto2Tick.setVisibility(visibility);
                break;
            case 3:
                pacchetto3Tick.setVisibility(visibility);
                break;
            case 4:
                pacchetto4Tick.setVisibility(visibility);
                break;
        }
    }

    private void checkValidationPacchetto() {
        try {

            JSONArray pacchetti = (JSONArray) Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONArray("pacchetti");
            JSONObject pacchetto = pacchetti.getJSONObject(pacchettoVisibile - 1);
            JSONObject pratica = (JSONObject) Session.getInstance().getJsonPratica().getJSONObject("pratica");

            pacchetto.put("offerte", new JSONArray());
            pacchetto.put("offerteRaggruppate", new JSONArray());
            //MW svuoto l'array offerte configurate per far si che in modifica lui elimina i dati precedentemente inseriti allo step 8.
            //pratica.remove("offerteConfigurate");
            pratica.put("offerteConfigurate", new JSONArray());

            pacchetto.put("completedStep2", false);
            Log.d("JSON PRATICA)", Session.getInstance().getJsonPratica().toString());

            boolean goOn = true;

            if (pacchetto.getBoolean("question1Offerta") || pacchetto.getBoolean("question1OffertaDevice")) {
                if (pacchetto.getBoolean("question1OffertaDevice")) {
                    if (!pacchetto.getBoolean("question2Smartphone") && !pacchetto.getBoolean("question2Tablet")) {
                        goOn = false;
                    }
                }
            } else {
                goOn = false;
            }

            if (goOn) {
//                if (pacchetto.getBoolean("question4Abbonamento") || pacchetto.getBoolean("question4Ricaricabile")) {
//                    if (pacchetto.getBoolean("question4Abbonamento")) {
                if (pacchetto.getInt("question4AbbonamentoNumSim") == 0 && pacchetto.getInt("question4RicaricabileNumSim") == 0) {
                    goOn = false;
                }
//                    }
//                    if (pacchetto.getBoolean("question4Ricaricabile")) {
//                        if (pacchetto.getInt("question4RicaricabileNumSim") == 0) {
//                            goOn = false;
//                        }
//                    }
//                } else {
//                    goOn = false;
//                }
            }

            if (goOn) {
                if (pacchetto.getBoolean("question6Si") || pacchetto.getBoolean("question6No")) {
                    if (pacchetto.getBoolean("question6Si")) {
                        if (pacchetto.getString("question7Scelta1").compareToIgnoreCase(OPZ1) == 0 && pacchetto.getString("question7Scelta2").compareToIgnoreCase(OPZ2) == 0) {
                            goOn = false;
                        }
                    }
                } else {
                    goOn = false;
                }
            }

            pacchetto.put("completedStep1", goOn);

            checkValidation();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void checkValidation() {
        try {

            //store data on sqlLite
            PraticheDAO.updatePratica(this.getActivity().getBaseContext());

            JSONArray pacchetti = (JSONArray) Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONArray("pacchetti");
            for (int i = 0; i < pacchetti.length(); i++) {
                changeTickVisibility(i + 1, pacchetti.getJSONObject(i).getBoolean("completedStep1") ? View.VISIBLE : View.GONE);
            }

            switch (pacchetti.length()) {
                case 1:
                    if (pacchetto1Tick.getVisibility() == View.VISIBLE) {
                        continuaButton.setEnabled(true);
                    } else {
                        continuaButton.setEnabled(false);
                    }
                    pacchetto2Tick.setVisibility(View.GONE);
                    pacchetto3Tick.setVisibility(View.GONE);
                    pacchetto4Tick.setVisibility(View.GONE);
                    break;
                case 2:
                    if (pacchetto1Tick.getVisibility() == View.VISIBLE && pacchetto2Tick.getVisibility() == View.VISIBLE) {
                        continuaButton.setEnabled(true);
                    } else {
                        continuaButton.setEnabled(false);
                    }
                    pacchetto3Tick.setVisibility(View.GONE);
                    pacchetto4Tick.setVisibility(View.GONE);
                    break;
                case 3:
                    if (pacchetto1Tick.getVisibility() == View.VISIBLE && pacchetto2Tick.getVisibility() == View.VISIBLE && pacchetto3Tick.getVisibility() == View.VISIBLE) {
                        continuaButton.setEnabled(true);
                    } else {
                        continuaButton.setEnabled(false);
                    }
                    pacchetto4Tick.setVisibility(View.GONE);
                    break;
                case 4:
                    if (pacchetto1Tick.getVisibility() == View.VISIBLE && pacchetto2Tick.getVisibility() == View.VISIBLE && pacchetto3Tick.getVisibility() == View.VISIBLE && pacchetto4Tick.getVisibility() == View.VISIBLE) {
                        continuaButton.setEnabled(true);
                    } else {
                        continuaButton.setEnabled(false);
                    }
                    break;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
