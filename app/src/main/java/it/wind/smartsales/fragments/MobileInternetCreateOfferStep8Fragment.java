package it.wind.smartsales.fragments;

import android.app.Fragment;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.nineoldandroids.animation.Animator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import it.wind.smartsales.R;
import it.wind.smartsales.activity.MainActivity;
import it.wind.smartsales.adapter.OfferteRicaricabiliAdapter;
import it.wind.smartsales.commons.Log;
import it.wind.smartsales.commons.Utils;
import it.wind.smartsales.customviews.AspectRatioView;
import it.wind.smartsales.customviews.VerticalTextView;
import it.wind.smartsales.dao.CatalogoOpzioniMobileDAO;
import it.wind.smartsales.dao.CatalogoTerminaliDAO;
import it.wind.smartsales.dao.PraticheDAO;
import it.wind.smartsales.entities.AddOnMobile;
import it.wind.smartsales.entities.AddOnPackage;
import it.wind.smartsales.entities.ExtraGBDetail;
import it.wind.smartsales.entities.OffertaMobile;
import it.wind.smartsales.entities.OpzioneMobile;
import it.wind.smartsales.entities.Session;
import it.wind.smartsales.entities.Terminale;

/**
 * Created by luca.quaranta on 10/02/2016.
 */
public class MobileInternetCreateOfferStep8Fragment extends Fragment {

    static final int REQUEST_IMAGE_CAPTURE = 1;
    private static final String TAG = "MobileInternetCreateOfferStep8Fragment";
    //private ArrayList<OffertaMobile> offerteSalvate;
    public ArrayList<AddOnMobile> allAddOnSelecedStep3;
    private RecyclerView recyclerViewRicaricabile, recyclerViewAbbonamento;
    private RecyclerView.LayoutManager mLayoutManagerRicaricabile, mLayoutManagerAbbonamento;
    private RecyclerView.Adapter adapterRicaricabile, adapterAbbonamento;
    private OffertaMobile pendingOffer;
    private Button continuaButton;
    private RelativeLayout headerRicaricabile, headerAbbonamento;
    private LinearLayout contentRicaricabile, contentAbbonamento;
    private AspectRatioView arrowRicaricabile, arrowAbbonamento;
    private LinearLayout opzioniLayoutRicaricabile, opzioniLayoutAbbonamento;
    private TextView numSimRicaricabile, numMnpRicaricabile, numGbRicaricabile, numOpzioniRicaricabile, numDeviceRicaricabile;
    private TextView numSimAbbonamento, numMnpAbbonamento, numGbAbbonamento, numOpzioniAbbonamento, numDeviceAbbonamento;
    private ArrayList<OffertaMobile> offerteDaRaggruppareRicaricabile, offerteDaRaggruppareAbbonamento;
    private ArrayList<OpzioneMobile> opzioniRicaricabile, opzioniAbbonamento;
    private EditText luogoField, CCID_ric;
    private LinearLayout extraGigaBtn;
    private MobileInternetCreateOfferStep8Fragment current;
    private ArrayList<ArrayList<ExtraGBDetail>> extraGigaDetail;
    private boolean datiRecuperati = false;
    private List<String> alfabeto = Arrays.asList("A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z");

    private void initializeComponents(View view) {
        continuaButton = (Button) view.findViewById(R.id.continua_button);

        headerRicaricabile = (RelativeLayout) view.findViewById(R.id.header_ricaricabile);
        contentRicaricabile = (LinearLayout) view.findViewById(R.id.content_ricaricabile);
        arrowRicaricabile = (AspectRatioView) view.findViewById(R.id.arrow_ricaricabile);
        headerAbbonamento = (RelativeLayout) view.findViewById(R.id.header_abbonamento);
        contentAbbonamento = (LinearLayout) view.findViewById(R.id.content_abbonamento);
        arrowAbbonamento = (AspectRatioView) view.findViewById(R.id.arrow_abbonamento);

        opzioniLayoutRicaricabile = (LinearLayout) view.findViewById(R.id.opzioni_layout_ricaricabile);
        opzioniLayoutAbbonamento = (LinearLayout) view.findViewById(R.id.opzioni_layout_abbonamento);

        numSimRicaricabile = (TextView) view.findViewById(R.id.num_sim_ricaricabile);
        numMnpRicaricabile = (TextView) view.findViewById(R.id.num_mnp_ricaricabile);
        numGbRicaricabile = (TextView) view.findViewById(R.id.num_gb_ricaricabile);
        numOpzioniRicaricabile = (TextView) view.findViewById(R.id.num_opzioni_ricaricabile);
        numDeviceRicaricabile = (TextView) view.findViewById(R.id.num_device_ricaricabile);
        numSimAbbonamento = (TextView) view.findViewById(R.id.num_sim_abbonamento);
        numMnpAbbonamento = (TextView) view.findViewById(R.id.num_mnp_abbonamento);
        numGbAbbonamento = (TextView) view.findViewById(R.id.num_gb_abbonamento);
        numOpzioniAbbonamento = (TextView) view.findViewById(R.id.num_opzioni_abbonamento);
        numDeviceAbbonamento = (TextView) view.findViewById(R.id.num_device_abbonamento);

        recyclerViewRicaricabile = (RecyclerView) view.findViewById(R.id.ricaricabile_recycler_view);
        recyclerViewAbbonamento = (RecyclerView) view.findViewById(R.id.abbonamento_recycler_view);
        extraGigaBtn = (LinearLayout) view.findViewById(R.id.extra_giga__button);


        checkExtraGigaSelection();

        luogoField = (EditText) view.findViewById(R.id.luogoField);


        luogoField.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    checkValidation();
                }
                return false;
            }
        });


        initRicaricabileList();
        initAbbonamentoList();
    }

    public void checkExtraGigaSelection() {
        int totValues = 0;
        extraGigaDetail = Session.getInstance().getExtraGiga(Session.getInstance().getPratica().getId());
        for (ArrayList<ExtraGBDetail> extraGBDetails : extraGigaDetail) {
            for (ExtraGBDetail extraGBDetail : extraGBDetails) {
                if (extraGBDetail.getValue() > 0) {
                    totValues += extraGBDetail.getValue();
                }
            }
        }

        if (totValues > 0) {
            extraGigaBtn.setSelected(true);
        } else {
            extraGigaBtn.setSelected(false);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_mobile_internet_create_offer_step_8, container, false);
        initializeComponents(view);
        current = this;


        extraGigaBtn.setActivated(true);

        extraGigaBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                extraGigaDetail = Session.getInstance().getExtraGiga(Session.getInstance().getPratica().getId());

                Utils.showExtraGbDialog(getActivity(), current, extraGigaDetail);
            }
        });

        headerRicaricabile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (contentAbbonamento.getVisibility() == View.VISIBLE) {
                    manageContent(contentAbbonamento, arrowAbbonamento);
                }
                manageContent(contentRicaricabile, arrowRicaricabile);
            }
        });


        headerAbbonamento.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (contentRicaricabile.getVisibility() == View.VISIBLE) {
                    manageContent(contentRicaricabile, arrowRicaricabile);
                }
                manageContent(contentAbbonamento, arrowAbbonamento);
            }
        });

        offerteDaRaggruppareRicaricabile = new ArrayList<>();
        offerteDaRaggruppareAbbonamento = new ArrayList<>();


        opzioniRicaricabile = new ArrayList<>();
        opzioniAbbonamento = new ArrayList<>();

        try {

            //MW L'array addOnMobileAll mi server per gestire i GigaShare e gli Extra Giga
            JSONArray addOnMobileAll = Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONArray("addOnMobileAllPacchetti");

            allAddOnSelecedStep3 = new ArrayList<>();
            for (int i = 0; i < addOnMobileAll.length(); i++) {

                allAddOnSelecedStep3.add(Utils.createAddOnMobile(addOnMobileAll.getJSONObject(i)));

            }


            JSONArray offerteRaggruppateJA = new JSONArray();

            //MW Controllo se ho dati salvati nella pratica
            JSONArray offerteRaggruppateJA2 = (JSONArray) Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONArray("offerteConfigurate");

            if (offerteRaggruppateJA2.length() == 0) {
                offerteRaggruppateJA = (JSONArray) Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONArray("offerteRaggruppate");
            } else {
                offerteRaggruppateJA = (JSONArray) Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONArray("offerteConfigurate");
                datiRecuperati = true;
            }

            //JSONArray opzioneMobileJsonArray = new JSONArray();
            //for(int i = 0; i<offerteRaggruppateJA.length();i++) {
            //    opzioneMobileJsonArray.put(offerteRaggruppateJA.getJSONObject(i).getJSONArray("addOnMobileList"));
            //}

            ArrayList<OffertaMobile> offerteRaggruppate = new ArrayList<>();
            for (int i = 0; i < offerteRaggruppateJA.length(); i++) {
                offerteRaggruppate.add(Utils.createOffertaMobile(offerteRaggruppateJA.getJSONObject(i)));
            }


            //offerteSalvate = new ArrayList<>();
            //for (int i = 0; i < offerteRaggruppateJA2.length(); i++) {
            //    offerteSalvate.add(Utils.createOffertaMobile(offerteRaggruppateJA2.getJSONObject(i)));
            //}


/*            if(offerteSalvate.size()>=offerteRaggruppate.size()){
                offerteRaggruppate.clear();
                offerteRaggruppate.addAll(offerteSalvate);
                Session.getInstance().getJsonPratica().getJSONObject("pratica").remove("offerteConfigurate");
            }*/


            boolean daCalcolare = true;
            for (int i = 0; i < allAddOnSelecedStep3.size(); i++) {

                numGbRicaricabile.setText(new DecimalFormat("00").format(Integer.valueOf(numGbRicaricabile.getText().toString()) + allAddOnSelecedStep3.get(i).getSelectedNumSimStep3()));

            }

            for (OffertaMobile offertaMobile : offerteRaggruppate) {

                if (offertaMobile.getOfferType().compareToIgnoreCase("Ricaricabile") == 0) {

                    ArrayList<AddOnPackage> addOnPackageArrayList = new ArrayList<>();
                    for (AddOnMobile addOnMobile : allAddOnSelecedStep3) {
                        AddOnPackage addOnPackage = new AddOnPackage();
                        addOnPackage.setDesc(addOnMobile.getDescFascia());

                        ArrayList<AddOnMobile> addOnMobileArrayList = new ArrayList<>();
                        for (int l = 0; l < addOnMobile.getSelectedNumSimStep3(); l++) {
                            addOnMobile.setSelectedLabelStep8(createLabel(l));
                            addOnMobileArrayList.add(Utils.createAddOnMobile(Utils.createAddOnMobile(addOnMobile)));
                        }
                        addOnPackage.setAddOnMobileArrayList(addOnMobileArrayList);
                        addOnPackageArrayList.add(addOnPackage);


                        offertaMobile.setAddOnPackageArrayListStep8(addOnPackageArrayList);
                        daCalcolare = false;

                    }
                    int mnpRicaricabili = 0;
                    int nuoveLineeRicaricabili = 0;
                    ArrayList<Terminale> terminaliMnp = new ArrayList<>();
                    ArrayList<Terminale> terminaliNuoveLinee = new ArrayList<>();
                    for (Terminale terminale : offertaMobile.getTerminaleArrayList()) {
                        numDeviceRicaricabile.setText(new DecimalFormat("00").format(Integer.valueOf(numDeviceRicaricabile.getText().toString()) + terminale.getSelectedNumSimStep3()));
                        if (Boolean.parseBoolean(terminale.getMnp())) {
                            numMnpRicaricabile.setText(new DecimalFormat("00").format(Integer.valueOf(numMnpRicaricabile.getText().toString()) + terminale.getSelectedNumSimStep3()));
                            mnpRicaricabili += terminale.getSelectedNumSimStep3();
                            terminaliMnp.add(terminale);
                        } else {
                            nuoveLineeRicaricabili += terminale.getSelectedNumSimStep3();
                            terminaliNuoveLinee.add(terminale);
                        }
                    }

                    if (!datiRecuperati) {
                        for (int k = 0; k < offertaMobile.getSelectedNumSimStep3(); k++) {
                            OffertaMobile offertaMobile1 = Utils.createOffertaMobile(Utils.createOffertaMobile(offertaMobile));
                            offertaMobile1.setMnp(k < mnpRicaricabili);
                            offertaMobile1.setTerminaliToShow(k < mnpRicaricabili ? terminaliMnp : terminaliNuoveLinee);
                            offertaMobile1.setTerminaliToShow2(CatalogoTerminaliDAO.retrieveTerminaliForSIM(getActivity(), offertaMobile1));
                            offertaMobile1.getTerminaliDiAppoggio().addAll(offertaMobile1.getTerminaliToShow());
                            offerteDaRaggruppareRicaricabile.add(offertaMobile1);
                        }
                    } else {

                        OffertaMobile offertaMobile1 = Utils.createOffertaMobile(Utils.createOffertaMobile(offertaMobile));
                        offertaMobile1.setMnp(0 < mnpRicaricabili);
                        offertaMobile1.setTerminaliToShow(0 < mnpRicaricabili ? terminaliMnp : terminaliNuoveLinee);
                        offertaMobile1.setTerminaliToShow2(CatalogoTerminaliDAO.retrieveTerminaliForSIM(getActivity(), offertaMobile1));
                        offertaMobile1.getTerminaliDiAppoggio().addAll(offertaMobile1.getTerminaliToShow());
                        offerteDaRaggruppareRicaricabile.add(offertaMobile1);

                    }

                    for (OpzioneMobile opzioneMobile : offertaMobile.getOpzioneMobileArrayList()) {
                        boolean opzioneRicaricabileTrovata = false;
                        for (OpzioneMobile opzioneRicaricabile : opzioniRicaricabile) {
                            if (opzioneMobile.getIdOpzione().compareToIgnoreCase(opzioneRicaricabile.getIdOpzione()) == 0) {
                                opzioneRicaricabileTrovata = true;
                                break;
                            }
                        }
                        if (!opzioneRicaricabileTrovata) {
                            opzioniRicaricabile.add(opzioneMobile);
                        } else {
                            for (OpzioneMobile opzioneRicaricabile : opzioniRicaricabile) {
                                if (opzioneMobile.getIdOpzione().compareToIgnoreCase(opzioneRicaricabile.getIdOpzione()) == 0) {
                                    opzioneRicaricabile.setSelectedNumSimStep3(opzioneRicaricabile.getSelectedNumSimStep3() + opzioneMobile.getSelectedNumSimStep3());
                                    break;
                                }
                            }
                        }

                    }

                }

                if (offertaMobile.getOfferType().compareToIgnoreCase("Abbonamento") == 0) {
//                    for (int k = 0; k < offertaMobile.getSelectedNumSimStep3(); k++) {
//                        offerteDaRaggruppareAbbonamento.add(Utils.createOffertaMobile(Utils.createOffertaMobile(offertaMobile)));
//                    }

                    int mnpAbbonamento = 0;
                    int nuoveLineeAbbonamento = 0;
                    ArrayList<Terminale> terminaliMnp = new ArrayList<>();
                    ArrayList<Terminale> terminaliNuoveLinee = new ArrayList<>();
                    for (Terminale terminale : offertaMobile.getTerminaleArrayList()) {
                        numDeviceAbbonamento.setText(new DecimalFormat("00").format(Integer.valueOf(numDeviceAbbonamento.getText().toString()) + terminale.getSelectedNumSimStep3()));
                        if (Boolean.parseBoolean(terminale.getMnp())) {
                            numMnpAbbonamento.setText(new DecimalFormat("00").format(Integer.valueOf(numMnpAbbonamento.getText().toString()) + terminale.getSelectedNumSimStep3()));
                            mnpAbbonamento += terminale.getSelectedNumSimStep3();
                            terminaliMnp.add(terminale);
                        } else {
                            nuoveLineeAbbonamento += terminale.getSelectedNumSimStep3();
                            terminaliNuoveLinee.add(terminale);
                        }
                    }
                    if (!datiRecuperati) {
                        for (int k = 0; k < offertaMobile.getSelectedNumSimStep3(); k++) {
                            OffertaMobile offertaMobile1 = Utils.createOffertaMobile(Utils.createOffertaMobile(offertaMobile));
                            offertaMobile1.setMnp(k < mnpAbbonamento);
                            offertaMobile1.setTerminaliToShow(k < mnpAbbonamento ? terminaliMnp : terminaliNuoveLinee);
                            offertaMobile1.setTerminaliToShow2(CatalogoTerminaliDAO.retrieveTerminaliForSIM(getActivity(), offertaMobile1));
                            offertaMobile1.getTerminaliDiAppoggio().addAll(offertaMobile1.getTerminaliToShow());
                            offerteDaRaggruppareAbbonamento.add(offertaMobile1);
                        }
                    } else {
                        OffertaMobile offertaMobile1 = Utils.createOffertaMobile(Utils.createOffertaMobile(offertaMobile));
                        offertaMobile1.setMnp(0 < mnpAbbonamento);
                        offertaMobile1.setTerminaliToShow(0 < mnpAbbonamento ? terminaliMnp : terminaliNuoveLinee);
                        offertaMobile1.setTerminaliToShow2(CatalogoTerminaliDAO.retrieveTerminaliForSIM(getActivity(), offertaMobile1));
                        offertaMobile1.getTerminaliDiAppoggio().addAll(offertaMobile1.getTerminaliToShow());
                        offerteDaRaggruppareAbbonamento.add(offertaMobile1);
                    }

                    for (OpzioneMobile opzioneMobile : offertaMobile.getOpzioneMobileArrayList()) {
                        boolean opzioneAbbonamentoTrovata = false;
                        for (OpzioneMobile opzioneAbbonamento : opzioniAbbonamento) {
                            if (opzioneMobile.getIdOpzione().compareToIgnoreCase(opzioneAbbonamento.getIdOpzione()) == 0) {
                                opzioneAbbonamentoTrovata = true;
                                break;
                            }
                        }
                        if (!opzioneAbbonamentoTrovata) {
                            opzioniAbbonamento.add(opzioneMobile);
                        } else {
                            for (OpzioneMobile opzioneRicaricabile : opzioniAbbonamento) {
                                if (opzioneMobile.getIdOpzione().compareToIgnoreCase(opzioneRicaricabile.getIdOpzione()) == 0) {
                                    opzioneRicaricabile.setSelectedNumSimStep3(opzioneRicaricabile.getSelectedNumSimStep3() + opzioneMobile.getSelectedNumSimStep3());
                                    break;
                                }
                            }
                        }
                    }
                }
            }


            Log.i(TAG, String.valueOf(offerteDaRaggruppareRicaricabile.size()));
            Log.i(TAG, String.valueOf(opzioniRicaricabile.size()));


        } catch (JSONException e) {
            e.printStackTrace();
        }

        numSimRicaricabile.setText(new DecimalFormat("00").format(offerteDaRaggruppareRicaricabile.size()));
        numSimAbbonamento.setText(new DecimalFormat("00").format(offerteDaRaggruppareAbbonamento.size()));

        opzioniRicaricabile.addAll(CatalogoOpzioniMobileDAO.retrieveOpzioniMobileFisse(getActivity(), "Ricaricabile"));
        opzioniAbbonamento.addAll(CatalogoOpzioniMobileDAO.retrieveOpzioniMobileFisse(getActivity(), "Abbonamento"));

        int numOpzioniRic = 0;
        for (OpzioneMobile opzioneMobile : opzioniRicaricabile) {
            numOpzioniRic += opzioneMobile.getSelectedNumSimStep3();
        }
        numOpzioniRicaricabile.setText(new DecimalFormat("00").format(numOpzioniRic));

        int numOpzioniAbb = 0;
        for (OpzioneMobile opzioneMobile : opzioniAbbonamento) {
            numOpzioniAbb += opzioneMobile.getSelectedNumSimStep3();
        }
        numOpzioniAbbonamento.setText(new DecimalFormat("00").format(numOpzioniAbb));

        populatePage();

        if (offerteDaRaggruppareRicaricabile.size() > 0) {
            populateOpzioniRicaricabile();
            populateRicaricabileList(offerteDaRaggruppareRicaricabile);
        } else {
            headerRicaricabile.setVisibility(View.GONE);
        }

        if (offerteDaRaggruppareAbbonamento.size() > 0) {
            populateOpzioniAbbonamento();
            populateAbbonamentoList(offerteDaRaggruppareAbbonamento);
        } else {
            headerAbbonamento.setVisibility(View.GONE);
        }

        continuaButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Session.getInstance().getPratica().setStep(PraticheDAO.STEP9);
                PraticheDAO.updatePratica(getActivity().getBaseContext());

                Log.d("JSON PRATICA)", Session.getInstance().getJsonPratica().toString());
                if (Session.getInstance().getDealer().getDealerRole().compareTo("UserSales") != 0)
                    ((MainActivity) getActivity()).goForward();
                else
                    ((MainActivity) getActivity()).goForwardSelectOffer();
            }
        });


        checkValidation();

        return view;
    }

    private void populatePage() {

       /* boolean aggioranto = false;
        if(offerteSalvate.size()==offerteDaRaggruppareRicaricabile.size()+offerteDaRaggruppareAbbonamento.size()) {

            for (int i = 0; i <offerteSalvate.size() ; i++) {

                for (int r = 0; r < offerteDaRaggruppareRicaricabile.size(); r++) {
                    if(offerteDaRaggruppareRicaricabile.get(0).getIdOfferta()==offerteSalvate.get(i).getIdOfferta()){

                    }
                }

            }

        }

*/
    }

    public void updateAddOnList(OffertaMobile offertaMobile) {
        for (OffertaMobile offertaMobile1 : offerteDaRaggruppareRicaricabile) {
            //MW commentato la condizione cosi applica la modifica a tutte le ricaricabili in quanto i GB sono diventati in comune
            //if (offertaMobile1.getIdOfferta() == offertaMobile.getIdOfferta()) {
            offertaMobile1.setAddOnPackageArrayListStep8(offertaMobile.getAddOnPackageArrayListStep8());
            //}
        }

        checkValidation();
    }

    public void openDeviceList(OffertaMobile offertaMobile, int listNumber) {
        Utils.showDeviceDialog(getActivity(), this, offertaMobile, listNumber);
    }

    public void updateDeviceList(OffertaMobile offertaMobile) {
        if (offertaMobile.getOfferType().compareToIgnoreCase("Ricaricabile") == 0) {
            ArrayList<Terminale> terminaleArrayList = new ArrayList<>();
            for (Terminale terminale : offertaMobile.getTerminaliDiAppoggio()) {
                terminaleArrayList.add(Utils.createTerminale(Utils.createTerminale(terminale)));
            }

            for (OffertaMobile offertaMobile1 : offerteDaRaggruppareRicaricabile) {
                if (offertaMobile1.getIdOfferta() == offertaMobile.getIdOfferta() && offertaMobile.isMnp() == offertaMobile1.isMnp()) {
                    if (offertaMobile1.getTerminale1() != null) {
                        for (int i = 0; i < terminaleArrayList.size(); i++) {
                            if (terminaleArrayList.get(i).getIdTerminale().compareToIgnoreCase(offertaMobile1.getTerminale1().getIdTerminale()) == 0 &&
                                    terminaleArrayList.get(i).getIdOfferta().compareToIgnoreCase(offertaMobile1.getTerminale1().getIdOfferta()) == 0 &&
                                    terminaleArrayList.get(i).getMnp().compareToIgnoreCase(offertaMobile1.getTerminale1().getMnp()) == 0) {
                                if (terminaleArrayList.get(i).getSelectedNumSimStep3() > 1) {
                                    terminaleArrayList.get(i).setSelectedNumSimStep3(terminaleArrayList.get(i).getSelectedNumSimStep3() - 1);
                                    break;
                                } else {
                                    terminaleArrayList.remove(i);
                                    break;
                                }
                            }
                        }
                    }

                    offertaMobile1.setTerminaliToShow(terminaleArrayList);
                }
            }
            adapterRicaricabile.notifyDataSetChanged();
        } else if (offertaMobile.getOfferType().compareToIgnoreCase("Abbonamento") == 0) {
            ArrayList<Terminale> terminaleArrayList = new ArrayList<>();
            for (Terminale terminale : offertaMobile.getTerminaliDiAppoggio()) {
                terminaleArrayList.add(Utils.createTerminale(Utils.createTerminale(terminale)));
            }

            for (OffertaMobile offertaMobile1 : offerteDaRaggruppareAbbonamento) {
                if (offertaMobile1.getIdOfferta() == offertaMobile.getIdOfferta() && offertaMobile.isMnp() == offertaMobile1.isMnp()) {
                    if (offertaMobile1.getTerminale1() != null) {
                        for (int i = 0; i < terminaleArrayList.size(); i++) {
                            if (terminaleArrayList.get(i).getIdTerminale().compareToIgnoreCase(offertaMobile1.getTerminale1().getIdTerminale()) == 0 &&
                                    terminaleArrayList.get(i).getIdOfferta().compareToIgnoreCase(offertaMobile1.getTerminale1().getIdOfferta()) == 0 &&
                                    terminaleArrayList.get(i).getMnp().compareToIgnoreCase(offertaMobile1.getTerminale1().getMnp()) == 0) {
                                if (terminaleArrayList.get(i).getSelectedNumSimStep3() > 1) {
                                    terminaleArrayList.get(i).setSelectedNumSimStep3(terminaleArrayList.get(i).getSelectedNumSimStep3() - 1);
                                    break;
                                } else {
                                    terminaleArrayList.remove(i);
                                    break;
                                }
                            }
                        }
                    }

                    offertaMobile1.setTerminaliToShow(terminaleArrayList);
                }
            }
            adapterAbbonamento.notifyDataSetChanged();
        }

        checkValidation();
    }

    private String createLabel(int num) {
        int Base = alfabeto.size();
        String label = "";

        if (num < Base) {
            int resto = num % Base;
            label += alfabeto.get(resto);
        } else if (num < ((Base * Base) + Base)) {
            int quoziente = num / Base;
            int resto = num % Base;
            label += alfabeto.get(quoziente - 1) + alfabeto.get(resto);
        } else {
            int quoziente = (num / Base) / Base;
            int quoto = quoziente % Base;
            int resto = num % Base;
            label += alfabeto.get(quoziente - 1) + alfabeto.get(quoto - 1) + alfabeto.get(resto);
        }

        return label;
    }

    private void initRicaricabileList() {
        recyclerViewRicaricabile.setHasFixedSize(true);
        mLayoutManagerRicaricabile = new LinearLayoutManager(getActivity());
        recyclerViewRicaricabile.setLayoutManager(mLayoutManagerRicaricabile);
        recyclerViewRicaricabile.setItemAnimator(new DefaultItemAnimator());
    }

    private void initAbbonamentoList() {
        recyclerViewAbbonamento.setHasFixedSize(true);
        mLayoutManagerAbbonamento = new LinearLayoutManager(getActivity());
        recyclerViewAbbonamento.setLayoutManager(mLayoutManagerAbbonamento);
        recyclerViewAbbonamento.setItemAnimator(new DefaultItemAnimator());
    }

    private void populateRicaricabileList(ArrayList<OffertaMobile> offerteMobile) {

        JSONObject pratica2;
        try {
            pratica2 = Session.getInstance().getJsonPratica().getJSONObject("pratica");
            adapterRicaricabile = new OfferteRicaricabiliAdapter(this, offerteMobile, opzioniRicaricabile, allAddOnSelecedStep3);
            recyclerViewRicaricabile.setAdapter(adapterRicaricabile);
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    private void populateAbbonamentoList(ArrayList<OffertaMobile> offerteMobile) {
        adapterAbbonamento = new OfferteRicaricabiliAdapter(this, offerteMobile, opzioniAbbonamento, allAddOnSelecedStep3);
        recyclerViewAbbonamento.setAdapter(adapterAbbonamento);
    }

    private void manageContent(final LinearLayout contentLayout, final AspectRatioView arrow) {
        if (contentLayout.getVisibility() == View.GONE) {
            YoYo.with(Techniques.SlideInDown).duration(300).withListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animation) {
                    contentLayout.setVisibility(View.VISIBLE);
                }

                @Override
                public void onAnimationEnd(Animator animation) {

                }

                @Override
                public void onAnimationCancel(Animator animation) {

                }

                @Override
                public void onAnimationRepeat(Animator animation) {

                }
            }).playOn(contentLayout);

            arrow.setBackground(getActivity().getResources().getDrawable(R.drawable.arrow_up_inserimento));
        } else {
            YoYo.with(Techniques.SlideOutUp).duration(300).withListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animation) {

                }

                @Override
                public void onAnimationEnd(Animator animation) {
                    contentLayout.setVisibility(View.GONE);
                }

                @Override
                public void onAnimationCancel(Animator animation) {

                }

                @Override
                public void onAnimationRepeat(Animator animation) {

                }
            }).playOn(contentLayout);
            arrow.setBackground(getActivity().getResources().getDrawable(R.drawable.arrow_down_inserimento));
        }
    }

    private void populateOpzioniRicaricabile() {
        for (OpzioneMobile opzioneMobile : opzioniRicaricabile) {
            LinearLayout linearLayout = new LinearLayout(getActivity());
            LinearLayout.LayoutParams layoutParamsLL = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
            layoutParamsLL.weight = 1f;
            linearLayout.setGravity(Gravity.CENTER);
            linearLayout.setLayoutParams(layoutParamsLL);

            VerticalTextView verticalTextView = new VerticalTextView(getActivity());
            verticalTextView.setTextColor(getActivity().getResources().getColor(R.color.colorAccent));
            verticalTextView.setText(opzioneMobile.getOpzioneDesc() + " " + opzioneMobile.getOpzioneDescUpper());
            verticalTextView.setTypeface(null, Typeface.BOLD);
            verticalTextView.setGravity(Gravity.CENTER_HORIZONTAL);
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            verticalTextView.setLayoutParams(layoutParams);

            linearLayout.addView(verticalTextView);
            opzioniLayoutRicaricabile.addView(linearLayout);
        }
    }

    private void populateOpzioniAbbonamento() {
        for (OpzioneMobile opzioneMobile : opzioniAbbonamento) {
            LinearLayout linearLayout = new LinearLayout(getActivity());
            LinearLayout.LayoutParams layoutParamsLL = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
            layoutParamsLL.weight = 1f;
            linearLayout.setGravity(Gravity.CENTER);
            linearLayout.setLayoutParams(layoutParamsLL);

            VerticalTextView verticalTextView = new VerticalTextView(getActivity());
            verticalTextView.setTextColor(getActivity().getResources().getColor(R.color.colorAccent));
            verticalTextView.setText(opzioneMobile.getOpzioneDesc() + " " + opzioneMobile.getOpzioneDescUpper());
            verticalTextView.setTypeface(null, Typeface.BOLD);
            verticalTextView.setGravity(Gravity.CENTER_HORIZONTAL);
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            verticalTextView.setLayoutParams(layoutParams);

            linearLayout.addView(verticalTextView);
            opzioniLayoutAbbonamento.addView(linearLayout);
        }
    }

    public void scanBarcode() {
        IntentIntegrator.forFragment(this).initiateScan();

        IntentIntegrator integrator = IntentIntegrator.forFragment(this);
        integrator.setDesiredBarcodeFormats(IntentIntegrator.ALL_CODE_TYPES);
        integrator.setPrompt("Scan a barcode");
        integrator.setCameraId(0);  // Use a specific camera of the device
        integrator.setBeepEnabled(false);
        integrator.initiateScan();
    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        final IntentResult intentResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (intentResult != null) {
            if (intentResult.getContents() == null) {

            } else {
                String result = intentResult.getContents();
                if (result.trim().length() != 19) {
                    Utils.showWarningMessage(getActivity(), "", "Il numero di serie SIM ICCID deve essere composto da 19 cifre");
                } else {
                    pendingOffer.setNewICCID(result);
                    if (pendingOffer.getOfferType().compareToIgnoreCase("Ricaricabile") == 0) {
                        adapterRicaricabile.notifyDataSetChanged();
                    } else if (pendingOffer.getOfferType().compareToIgnoreCase("Abbonamento") == 0) {
                        adapterAbbonamento.notifyDataSetChanged();
                    }
                }

                checkValidation();
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    public void onCheckBoxChecked(boolean isChecked, OffertaMobile offertaMobile, OpzioneMobile opzioneMobile) {
        if (isChecked) {
            offertaMobile.getOpzioniMobileSelectedArrayListStep8().add(opzioneMobile);
        } else {
            offertaMobile.getOpzioniMobileSelectedArrayListStep8().remove(opzioneMobile);
        }

        checkValidation();
    }

    public void onSpinnerBoxChecked(String Value, OffertaMobile offertaMobile, OpzioneMobile opzioneMobile) {
        if (Value.compareTo("") != 0) {
            offertaMobile.getOpzioniMobileSelectedArrayListStep8().add(opzioneMobile);
        } else {
            offertaMobile.getOpzioniMobileSelectedArrayListStep8().remove(opzioneMobile);
        }

        checkValidation();
    }

    public void onOcrButtonClick(OffertaMobile offertaMobile) {
        pendingOffer = offertaMobile;
        scanBarcode();
//        dispatchTakePictureIntent();
    }

    public void onMnpButtonClick(OffertaMobile offertaMobile) {
        Utils.showMnpDialog(getActivity(), this, offertaMobile);

    }

    public void updateOffer() {
        if (adapterRicaricabile != null) {
            adapterRicaricabile.notifyDataSetChanged();
        }
        if (adapterAbbonamento != null) {
            adapterAbbonamento.notifyDataSetChanged();
        }
        checkValidation();
    }

    public void onGbButtonClick(OffertaMobile offertaMobile) {
        Utils.showGbDialog(getActivity(), this, offertaMobile);
    }

    private boolean checkPraticaValidation() {
        if (offerteDaRaggruppareRicaricabile.size() > 0) {

            for (OffertaMobile offertaMobile : offerteDaRaggruppareRicaricabile) {
                if (TextUtils.isEmpty(offertaMobile.getNewICCID()) || offertaMobile.getNewICCID().length() != 19) {
                    return false;
                }
            }

            int numMnp = 0;
            if (Integer.parseInt(numMnpRicaricabile.getText().toString().trim()) > 0) {
                for (OffertaMobile offertaMobile : offerteDaRaggruppareRicaricabile) {
                    if (!TextUtils.isEmpty(offertaMobile.getMnpStep8())) {
                        numMnp++;
                    }
                }
            }
            if (Integer.parseInt(numMnpRicaricabile.getText().toString().trim()) != numMnp) {
                return false;
            }

            int numGb = 0;
            if (Integer.parseInt(numGbRicaricabile.getText().toString().trim()) > 0) {
                for (OffertaMobile offertaMobile : offerteDaRaggruppareRicaricabile) {
                    if (offertaMobile.getAddOnMobileStep8() != null) {
                        numGb++;
                    }
                }
            }
            if (Integer.parseInt(numGbRicaricabile.getText().toString().trim()) != numGb) {
                return false;
            }

            int numOpz = 0;
            if (Integer.parseInt(numOpzioniRicaricabile.getText().toString().trim()) > 0) {
                for (OffertaMobile offertaMobile : offerteDaRaggruppareRicaricabile) {
                    for (OpzioneMobile opzioneMobile : offertaMobile.getOpzioniMobileSelectedArrayListStep8()) {
                        for (OpzioneMobile opzioneMobile1 : offertaMobile.getOpzioneMobileArrayList()) {
                            if (opzioneMobile.getIdOpzione().compareToIgnoreCase(opzioneMobile1.getIdOpzione()) == 0) {
                                numOpz++;
                            }
                        }
                    }
                }
            }
            if (Integer.parseInt(numOpzioniRicaricabile.getText().toString().trim()) != numOpz) {
                return false;
            }

            int numDvc = 0;
            if (Integer.parseInt(numDeviceRicaricabile.getText().toString().trim()) > 0) {
                for (OffertaMobile offertaMobile : offerteDaRaggruppareRicaricabile) {
                    if (offertaMobile.getTerminale1() != null) {
                        numDvc++;
                    }
                }
            }
            if (Integer.parseInt(numDeviceRicaricabile.getText().toString().trim()) != numDvc) {
                return false;
            }

        }

        if (offerteDaRaggruppareAbbonamento.size() > 0) {

            for (OffertaMobile offertaMobile : offerteDaRaggruppareAbbonamento) {
                if (TextUtils.isEmpty(offertaMobile.getNewICCID())) {
                    return false;
                }
            }

            int numMnp = 0;
            if (Integer.parseInt(numMnpAbbonamento.getText().toString().trim()) > 0) {
                for (OffertaMobile offertaMobile : offerteDaRaggruppareAbbonamento) {
                    if (!TextUtils.isEmpty(offertaMobile.getMnpStep8())) {
                        numMnp++;
                    }
                }
            }
            if (Integer.parseInt(numMnpAbbonamento.getText().toString().trim()) != numMnp) {
                return false;
            }

            int numGb = 0;
            if (Integer.parseInt(numGbAbbonamento.getText().toString().trim()) > 0) {
                for (OffertaMobile offertaMobile : offerteDaRaggruppareAbbonamento) {
                    if (offertaMobile.getAddOnMobileStep8() != null) {
                        numGb++;
                    }
                }
            }
            if (Integer.parseInt(numGbAbbonamento.getText().toString().trim()) != numGb) {
                return false;
            }

            int numOpz = 0;
            if (Integer.parseInt(numOpzioniAbbonamento.getText().toString().trim()) > 0) {
                for (OffertaMobile offertaMobile : offerteDaRaggruppareAbbonamento) {
                    for (OpzioneMobile opzioneMobile : offertaMobile.getOpzioniMobileSelectedArrayListStep8()) {
                        for (OpzioneMobile opzioneMobile1 : offertaMobile.getOpzioneMobileArrayList()) {
                            if (opzioneMobile.getIdOpzione().compareToIgnoreCase(opzioneMobile1.getIdOpzione()) == 0) {
                                numOpz++;
                            }
                        }
                    }
                }
            }
            if (Integer.parseInt(numOpzioniAbbonamento.getText().toString().trim()) != numOpz) {
                return false;
            }

            int numDvc = 0;
            if (Integer.parseInt(numDeviceAbbonamento.getText().toString().trim()) > 0) {
                for (OffertaMobile offertaMobile : offerteDaRaggruppareAbbonamento) {
                    if (offertaMobile.getTerminale1() != null) {
                        numDvc++;
                    }
                }
            }
            if (Integer.parseInt(numDeviceAbbonamento.getText().toString().trim()) != numDvc) {
                return false;
            }

        }

        return true;
    }

    public void checkValidation() {
        updateJson();

//        if (checkPraticaValidation() && !luogoField.getText().toString().trim().equalsIgnoreCase("")) {
            continuaButton.setEnabled(true);
//        } else {
//            continuaButton.setEnabled(false);
//        }
    }

    private void updateJson() {
        try {
            JSONArray offerteRaggruppate = new JSONArray();
            for (OffertaMobile offertaMobile : offerteDaRaggruppareRicaricabile) {
                offerteRaggruppate.put(Utils.createOffertaMobile(offertaMobile));
            }
            for (OffertaMobile offertaMobile : offerteDaRaggruppareAbbonamento) {
                offerteRaggruppate.put(Utils.createOffertaMobile(offertaMobile));
            }
            Session.getInstance().getJsonPratica().getJSONObject("pratica").put("offerteConfigurate", offerteRaggruppate);

            //store data on sqlLite
            PraticheDAO.updatePratica(this.getActivity().getBaseContext());

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public void replaceExtraGigaDetail(int position, ArrayList<ExtraGBDetail> values) {
        extraGigaDetail.set(position, values);
    }

}
