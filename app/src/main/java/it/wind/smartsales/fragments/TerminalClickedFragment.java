package it.wind.smartsales.fragments;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;

import java.util.ArrayList;
import java.util.Locale;

import it.wind.smartsales.R;
import it.wind.smartsales.SmartSalesApplication;
import it.wind.smartsales.commons.Utils;
import it.wind.smartsales.dao.CatalogoTerminaliDAO;
import it.wind.smartsales.entities.Terminale;

/**
 * Created by giuseppe.mangino on 18/07/2016.
 */
@SuppressLint("ValidFragment")
public class TerminalClickedFragment extends Fragment {
    private OnUpdateDeviceListener mListener;
    private Terminale terminal;
    private String currentCapacity;
    private String currentColor;
    private TextView priceMonthRateMnp;
    private TextView priceMonthRate;
    private boolean isContinueEnabled;

    public TerminalClickedFragment(Terminale terminal) {
        this.terminal = terminal;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_terminal_clicked, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mListener = (TerminalsFragment) getFragmentManager().findFragmentById(R.id.fragment_container);

        final TextView brand = (TextView) view.findViewById(R.id.brand_title);
        final TextView capacityLabel = (TextView) view.findViewById(R.id.capacity_radio);
        final TextView colorLabel = (TextView) view.findViewById(R.id.color_radio);
        final TextView modello = (TextView) view.findViewById(R.id.type_title);
        priceMonthRateMnp = (TextView) view.findViewById(R.id.price_month_rate_mnp);
        priceMonthRate = (TextView) view.findViewById(R.id.price_month_rate);
        final TextView labelRate = (TextView) view.findViewById(R.id.label_rate);
        final TextView labelRateMnp = (TextView) view.findViewById(R.id.label_rate_mnp);
        final NetworkImageView image = (NetworkImageView) view.findViewById(R.id.image_product);
        final ImageView minusRate = (ImageView) view.findViewById(R.id.ic_minus_rate);
        final ImageView plusRate = (ImageView) view.findViewById(R.id.ic_plus_rate);
        final ImageView minusRateMnp = (ImageView) view.findViewById(R.id.ic_minus_rate_mnp);
        final ImageView plusRateMnp = (ImageView) view.findViewById(R.id.ic_plus_rate_mnp);
        final RadioGroup groupCapacity = (RadioGroup) view.findViewById(R.id.capacity_radio_group);
        final RadioGroup groupColor = (RadioGroup) view.findViewById(R.id.color_radio_group);

        final ArrayList<String> capacities = CatalogoTerminaliDAO
                .getCapacitiesByShortDesc(view.getContext(), terminal.getShortDesc());

        final ArrayList<String> colors = CatalogoTerminaliDAO
                .getColorsByShortDesc(view.getContext(), terminal.getShortDesc());

        capacityLabel.setVisibility(capacities.size() > 0 ? View.VISIBLE : View.INVISIBLE);
        colorLabel.setVisibility(colors.size() > 0 ? View.VISIBLE : View.INVISIBLE);

        for (String s : capacities) {
            final RadioButton button = new RadioButton(view.getContext());
            button.setText(s);
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    currentCapacity = button.getText().toString();
                    terminal.setCapacity(currentCapacity);
                    if (colors.size() == 0 || !TextUtils.isEmpty(currentColor)) {
                        updateFragment(button.getContext());
                    }
                }
            });
            groupCapacity.addView(button);
        }

        for (String s : colors) {
            final RadioButton button = new RadioButton(view.getContext());
            button.setText(s);
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    currentColor = button.getText().toString();
                    terminal.setColor(currentColor);
                    if (capacities.size() == 0 || !TextUtils.isEmpty(currentCapacity)) {
                        updateFragment(button.getContext());
                    }
                }
            });
            groupColor.addView(button);
        }

        labelRate.setText(String.format(Locale.getDefault(), "%03d", terminal.getSelectedTerminals()));
        minusRate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if ((Integer.valueOf(labelRate.getText().toString()) - 1) >= 0) {
                    final int count = Integer.valueOf(labelRate.getText().toString()) - 1;
                    labelRate.setText(String.format(Locale.getDefault(), "%03d", count));
                    terminal.setSelectedTerminals(count);
                    onSelected(terminal);
                }
            }
        });

        plusRate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final int count = Integer.valueOf(labelRate.getText().toString()) + 1;
                labelRate.setText(String.format(Locale.getDefault(), "%03d", count));
                terminal.setSelectedTerminals(count);
                onSelected(terminal);
            }
        });

        labelRateMnp.setText(String.format(Locale.getDefault(), "%03d", terminal.getSelectedTerminalsMnp()));
        minusRateMnp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if ((Integer.valueOf(labelRateMnp.getText().toString()) - 1) >= 0) {
                    final int count = Integer.valueOf(labelRateMnp.getText().toString()) - 1;
                    labelRateMnp.setText(String.format(Locale.getDefault(), "%03d", count));
                    terminal.setSelectedTerminalsMnp(count);
                    onSelected(terminal);
                }
            }
        });

        plusRateMnp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final int count = Integer.valueOf(labelRateMnp.getText().toString()) + 1;
                labelRateMnp.setText(String.format(Locale.getDefault(), "%03d", count));
                terminal.setSelectedTerminalsMnp(count);
                onSelected(terminal);
            }
        });

        brand.setText(terminal.getTerminaleBrand());
        modello.setText(terminal.getShortDesc());
        priceMonthRateMnp.setText(terminal.getRataMensileMnp() + getString(R.string.euro_price_suffix));
        priceMonthRate.setText(terminal.getRataMensile() + getString(R.string.euro_price_suffix));

        ImageLoader imgLoader = SmartSalesApplication.getInstance().getImageLoader();
        imgLoader.get(terminal.getImageUrl(), ImageLoader.getImageListener(image,
                Utils.getPlaceholderBasedOnOem(terminal.getOem()), R.drawable.placeholder_altro));
        image.setImageUrl(terminal.getImageUrl(), imgLoader);
    }

    private void updateFragment(Context context) {
        CatalogoTerminaliDAO.updatePrices(context, terminal);
        priceMonthRateMnp.setText(terminal.getRataMensileMnp() + getString(R.string.euro_price_suffix));
        priceMonthRate.setText(terminal.getRataMensile() + getString(R.string.euro_price_suffix));
        isContinueEnabled = true;
        onSelected(terminal);
    }

    public void onSelected(Terminale terminal) {
        if (mListener != null && isContinueEnabled) {
            mListener.onUpdateDevice(terminal);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnUpdateDeviceListener {
        void onUpdateDevice(Terminale terminal);
    }
}