package it.wind.smartsales.fragments;

import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Date;

import it.wind.smartsales.R;
import it.wind.smartsales.activity.MainActivity;
import it.wind.smartsales.dao.PraticheDAO;
import it.wind.smartsales.entities.Session;

/**
 * Created by luca.quaranta on 03/12/2015.
 */
public class NoteDialogFragment extends DialogFragment {

    public static final String KEY = "MESSAGE_KEY";
    private String note=null;


    private Button indietroButton, continuaButton;
    private MobileInternetCreateOfferStep4Fragment mobileInternetCreateOfferStep4Fragment;
    private EditText noteText;


    public NoteDialogFragment() {
    }

    public NoteDialogFragment(String note) {
        this.note=note;
    }

    private void initializeComponents(View view) {

        indietroButton = (Button) view.findViewById(R.id.indietro_button_note);
        continuaButton = (Button) view.findViewById(R.id.continua_button_note);
        noteText = (EditText) view.findViewById(R.id.note_text);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    @Override
    public void onResume() {
        super.onResume();
        int width = getResources().getDimensionPixelSize(R.dimen.fragment_dialog_mail_width);
        getDialog().getWindow().setLayout(width, ViewGroup.LayoutParams.WRAP_CONTENT);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_dialog_note, container, false);

        initializeComponents(view);

        indietroButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        continuaButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                String note = noteText.getText().toString().trim();
                Session.getInstance().getPratica().setNote(note);
                Session.getInstance().getPratica().setLast_update_note(new Date());
                PraticheDAO.updatePratica(getActivity());
                ((MainActivity)getActivity()).updateNote();
                dismiss();
            }
        });

        ((TextView)view.findViewById(R.id.note_text)).setText(note);

        return view;
    }


}
