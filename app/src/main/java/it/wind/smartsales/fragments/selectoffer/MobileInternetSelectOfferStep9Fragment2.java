package it.wind.smartsales.fragments.selectoffer;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import it.wind.smartsales.customviews.AspectRatioView;
import it.wind.smartsales.entities.OffertaMobile;
import it.wind.smartsales.entities.Session;
import it.wind.smartsales.R;
import it.wind.smartsales.commons.Utils;

/**
 * Created by luca.quaranta on 10/02/2016.
 */
public class MobileInternetSelectOfferStep9Fragment2 extends Fragment {

    private LinearLayout documentoRappresentanteLegale, delegaRappresentanteLegale, documentoDelegato, fatturaOperatore, aggiungiDocumento;
    private Button previewButton, invioButton, procediButton;
    private AspectRatioView fatturaOperatoreImage;
    private TextView fatturaOperatoreText;

    private View.OnClickListener documentClickListener;

    private View viewClicked;

    private ArrayList<OffertaMobile> offertaMobileArrayList;

    private void initializeComponents(View view) {
        documentoRappresentanteLegale = (LinearLayout) view.findViewById(R.id.documento_rappresentante_legale);
        delegaRappresentanteLegale = (LinearLayout) view.findViewById(R.id.delega_rappresentante_legale);
        documentoDelegato = (LinearLayout) view.findViewById(R.id.documento_delegato);
        fatturaOperatore = (LinearLayout) view.findViewById(R.id.fattura_operatore);
        aggiungiDocumento = (LinearLayout) view.findViewById(R.id.aggiungi_documento);

        fatturaOperatoreImage = (AspectRatioView) view.findViewById(R.id.fattura_operatore_image);
        fatturaOperatoreText = (TextView) view.findViewById(R.id.fattura_operatore_text);

        previewButton = (Button) view.findViewById(R.id.preview_button);
        invioButton = (Button) view.findViewById(R.id.invio_button);
        procediButton = (Button) view.findViewById(R.id.procedi_button);

        documentClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewClicked = v;
                showGenericMessage(getActivity(), "Allega Documento", "Scegli come allegare una foto.");
            }
        };

        documentoRappresentanteLegale.setOnClickListener(documentClickListener);
        delegaRappresentanteLegale.setOnClickListener(documentClickListener);
        documentoDelegato.setOnClickListener(documentClickListener);
        fatturaOperatore.setOnClickListener(documentClickListener);
        aggiungiDocumento.setOnClickListener(documentClickListener);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_mobile_internet_create_offer_step_9, container, false);
        initializeComponents(view);

        try {
            JSONArray offerteRaggruppate = (JSONArray) Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONArray("offerteConfigurate");
            offertaMobileArrayList = new ArrayList<>();
            for (int i = 0; i < offerteRaggruppate.length(); i++) {
                offertaMobileArrayList.add(Utils.createOffertaMobile(offerteRaggruppate.getJSONObject(i)));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (!hasMnp()) {
            fatturaOperatore.setEnabled(false);
            fatturaOperatoreImage.setEnabled(false);
            fatturaOperatoreText.setEnabled(false);
        }


        return view;
    }

    private static final int TAKE_PHOTO = 1;
    private static final int SELECT_PHOTO = 2;

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == 1) {
            galleryAddPic();

            Bitmap bitmap = scalePic();



            viewClicked.setSelected(true);
            viewClicked = null;
        } else if (requestCode == SELECT_PHOTO) {
            Uri selectedImageUri = data.getData();
            String selectedImagePath = getPath(selectedImageUri);
            viewClicked.setSelected(true);
            viewClicked = null;
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void takePhoto() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                        Uri.fromFile(photoFile));
                startActivityForResult(takePictureIntent, TAKE_PHOTO);
            }
        }
    }

    private void selectPhoto() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), SELECT_PHOTO);
    }

    public String getPath(Uri uri) {
        // just some safety built in
        if( uri == null ) {
            // TODO perform some logging or show user feedback
            return null;
        }
        // try to retrieve the image from the media store first
        // this will only work for images selected from gallery
        String[] projection = { MediaStore.Images.Media.DATA };
        Cursor cursor = getActivity().managedQuery(uri, projection, null, null, null);
        if( cursor != null ){
            int column_index = cursor
                    .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        }
        // this is our fallback here
        return uri.getPath();
    }

    String mCurrentPhotoPath;
    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = "file:" + image.getAbsolutePath();
        return image;
    }

    private void galleryAddPic() {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        File f = new File(mCurrentPhotoPath);
        Uri contentUri = Uri.fromFile(f);
        mediaScanIntent.setData(contentUri);
        getActivity().sendBroadcast(mediaScanIntent);
    }

    private Bitmap scalePic() {
        // Get the dimensions of the View
//        int targetW = mImageView.getWidth();
//        int targetH = mImageView.getHeight();

        // Get the dimensions of the bitmap
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;

        // Determine how much to scale down the image
        int scaleFactor = 4;
//        int scaleFactor = Math.min(photoW/targetW, photoH/targetH);

        // Decode the image file into a Bitmap sized to fill the View
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inPurgeable = true;

        return BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
//        mImageView.setImageBitmap(bitmap);
    }

    private Bitmap convertColorImageBlackAndWhiteFromGallery(Bitmap origBitmap) {
        ColorMatrix colorMatrix = new ColorMatrix();
        colorMatrix.setSaturation(0);

        ColorMatrixColorFilter colorMatrixFilter = new ColorMatrixColorFilter(
                colorMatrix);
        Bitmap blackAndWhiteBitmap = null;

        if (origBitmap != null) {
            int dstWidth = 960;
            int dstHeight = 720;

            //Toast.makeText(this, "Can DetectOrientation " + rotation +" height:"+ origBitmap.getHeight() + " width:"+origBitmap.getWidth(), Toast.LENGTH_LONG).show();
            //ISCTF00110214
            if (origBitmap.getHeight() > origBitmap.getWidth()) {
                dstWidth = 720;
                dstHeight = 960;
            }


            blackAndWhiteBitmap = origBitmap.copy(Bitmap.Config.ARGB_8888, true);
            blackAndWhiteBitmap = Bitmap.createScaledBitmap(blackAndWhiteBitmap, dstWidth, dstHeight, true);


            Paint paint = new Paint();
            paint.setColorFilter(colorMatrixFilter);

            Canvas canvas = new Canvas(blackAndWhiteBitmap);
            canvas.drawBitmap(blackAndWhiteBitmap, 0, 0, paint);
        }


        return blackAndWhiteBitmap;
    }

    private boolean hasMnp() {
        for (OffertaMobile offertaMobile : offertaMobileArrayList) {
            if (offertaMobile.isMnp()) {
                return true;
            }
        }
        return false;
    }

    public void showGenericMessage(Context context, String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton("Fotocamera", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                takePhoto();
                dialog.dismiss();
            }
        });
        builder.setNegativeButton("Galleria", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                selectPhoto();
                dialog.dismiss();
            }
        });
        builder.show();
    }

//    private void generateXML() {
//        try {
//
//            String xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
//                    "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ins=\"http://insertpda.ws.wind.sfa.accenture.com/\">\n" +
//                    "<soapenv:Header/>\n" +
//                    "<soapenv:Body>" +
//                    "<ins:InsertPdaRequestReq>";
//
////        InsertPdaAnagrafica_Input [INIZIO]
//            xml += "<InsertPdaAnagrafica_Input>";
//
//            xml += "<VariazioneTecnica>" + "false" + "</VariazioneTecnica>";
//
//            xml += "<Dealer>";
//            xml += "<DealerName>" + Session.getInstance().getDealer().getDealerName() + " " + Session.getInstance().getDealer().getDealerSurname() + "</DealerName>";
//            xml += "<ClientManagerName>" + Session.getInstance().getDealer().getNomeClientManager() + "</ClientManagerName>";
//            xml += "</Dealer>";
//
//            xml += "<Customer>";
//
////            xml += "<CustomerCode>"+ "</CustomerCode>";
//            xml += "<CustomerName>" + Session.getInstance().getJsonPratica().getJSONObject("inserimentoDati").getJSONObject("datiCliente").getString("nome") + " " +
//                    Session.getInstance().getJsonPratica().getJSONObject("inserimentoDati").getJSONObject("datiCliente").getString("cognome") + " " +
//                    Session.getInstance().getJsonPratica().getJSONObject("inserimentoDati").getJSONObject("datiCliente").getString("ragioneSociale") + "</CustomerName>";
//            xml += "<CompanyType>" + Session.getInstance().getJsonPratica().getJSONObject("inserimentoDati").getJSONObject("datiCliente").getString("firmaSocietaria") + "</CompanyType>";
//            xml += "<ProvinceCCIA>" + Session.getInstance().getJsonPratica().getJSONObject("inserimentoDati").getJSONObject("datiCliente").getString("provinciaCCIAA") + "</ProvinceCCIA>";
//            xml += "<PartitaIVA>" + Session.getInstance().getJsonPratica().getJSONObject("inserimentoDati").getJSONObject("datiCliente").getString("p_iva") + "</PartitaIVA>";
//
//            xml += "<Delegate>";
//            xml += "<FirstName>" + Session.getInstance().getJsonPratica().getJSONObject("inserimentoDati").getJSONObject("datiRappresentanteLegale").getString("nome") + "</FirstName>";
//            xml += "<LastName>" + Session.getInstance().getJsonPratica().getJSONObject("inserimentoDati").getJSONObject("datiRappresentanteLegale").getString("cognome") + "</LastName>";
//            xml += "<PhoneNumber>" + Session.getInstance().getJsonPratica().getJSONObject("inserimentoDati").getJSONObject("datiRappresentanteLegale").getString("telefono") + "</PhoneNumber>";
//            xml += "<FaxNumber>" + Session.getInstance().getJsonPratica().getJSONObject("inserimentoDati").getJSONObject("datiRappresentanteLegale").optString("fax") + "</FaxNumber>";
//            xml += "<EmailAddress>" + Session.getInstance().getJsonPratica().getJSONObject("inserimentoDati").getJSONObject("datiRappresentanteLegale").optString("email") + "</EmailAddress>";
//            xml += "<CodiceFiscale>" + Session.getInstance().getJsonPratica().getJSONObject("inserimentoDati").getJSONObject("datiRappresentanteLegale").optString("cf") + "</CodiceFiscale>";
//            xml += "<BirthCity>" + Session.getInstance().getJsonPratica().getJSONObject("inserimentoDati").getJSONObject("datiRappresentanteLegale").optString("luogoNascita") + "</BirthCity>";
//            xml += "<BirthProvince>" + Session.getInstance().getJsonPratica().getJSONObject("inserimentoDati").getJSONObject("datiRappresentanteLegale").optString("provincia") + "</BirthProvince>";
////            TODO: gestire data
//            xml += "<BirthDate>" + Session.getInstance().getJsonPratica().getJSONObject("inserimentoDati").getJSONObject("datiRappresentanteLegale").optString("dataNascita") + "</BirthDate>";
//            xml += "<Citizenship>" + Session.getInstance().getJsonPratica().getJSONObject("inserimentoDati").getJSONObject("datiRappresentanteLegale").optString("nazionalita") + "</Citizenship>";
//            xml += "<Gender>" + Session.getInstance().getJsonPratica().getJSONObject("inserimentoDati").getJSONObject("datiRappresentanteLegale").optString("gender") + "</Gender>";
//            xml += "<IDType>" + Session.getInstance().getJsonPratica().getJSONObject("inserimentoDati").getJSONObject("datiRappresentanteLegale").optString("docIdentita") + "</IDType>";
//            xml += "<IDNumber>" + Session.getInstance().getJsonPratica().getJSONObject("inserimentoDati").getJSONObject("datiRappresentanteLegale").optString("numDocIdentita") + "</IDNumber>";
//            xml += "<IDIssuedBy>" + Session.getInstance().getJsonPratica().getJSONObject("inserimentoDati").getJSONObject("datiRappresentanteLegale").optString("rilasciatoDa") + "</IDIssuedBy>";
//            xml += "<IDIssueDate>" + Session.getInstance().getJsonPratica().getJSONObject("inserimentoDati").getJSONObject("datiRappresentanteLegale").optString("dataDoc") + "</IDIssueDate>";
////        xml += "<Address>";
////        xml += "</Address>";
//            xml += "</Delegate>";
//
//            xml += "<Address>";
//            xml += "<AddressDescription>" + Session.getInstance().getJsonPratica().getJSONObject("inserimentoDati").getJSONObject("datiSede").optString("riferimentoSede") + "</AddressDescription>";
////            xml += "<AddressType>"++ "</AddressType>";
//            xml += "<AddressName>" + Session.getInstance().getJsonPratica().getJSONObject("inserimentoDati").getJSONObject("datiSede").optString("indirizzo") + "</AddressName>";
//            xml += "<StreetNbr>" + Session.getInstance().getJsonPratica().getJSONObject("inserimentoDati").getJSONObject("datiSede").optString("civico") + "</StreetNbr>";
//            xml += "<City>" + Session.getInstance().getJsonPratica().getJSONObject("inserimentoDati").getJSONObject("datiSede").optString("citta") + "</City>";
//            xml += "<Province>" + Session.getInstance().getJsonPratica().getJSONObject("inserimentoDati").getJSONObject("datiSede").optString("provincia") + "</Province>";
////            xml += "<Country>"++ "</Country>";
//            xml += "<PostalCode>" + Session.getInstance().getJsonPratica().getJSONObject("inserimentoDati").getJSONObject("datiSede").optString("cap") + "</PostalCode>";
////            xml += "<ReprFirstName>"++ "</ReprFirstName>";
////            xml += "<ReprLastName>"++ "</ReprLastName>";
////            xml += "<ReprPhoneNumber>"++ "</ReprPhoneNumber>";
////            xml += "<ReprFaxNumber>"++ "</ReprFaxNumber>";
//            xml += "<ReprEmailAddress>" + Session.getInstance().getJsonPratica().getJSONObject("inserimentoDati").getJSONObject("datiRappresentanteLegale").optString("email") + "</ReprEmailAddress>";
//            xml += "</Address>";
//
//            xml += "</Customer>";
//
//            xml += "<InvoiceCallDetail>" + (Session.getInstance().getJsonPratica().getJSONObject("inserimentoDati").getJSONObject("referenteContratto").getString("dettaglioChiamate").compareToIgnoreCase("1") == 0 ? "true" : "false") + "</InvoiceCallDetail>";
//            xml += "<InvoiceUnique>" + (Session.getInstance().getJsonPratica().getJSONObject("inserimentoDati").getJSONObject("referenteContratto").getString("modFatturazione").compareToIgnoreCase("Fattura unica") == 0 ? "true" : "false") + "</InvoiceUnique>";
//            xml += "<InvoiceByCostCenter>" + (Session.getInstance().getJsonPratica().getJSONObject("inserimentoDati").getJSONObject("referenteContratto").getString("modFatturazione").compareToIgnoreCase("Fattura unica") != 0 ? "true" : "false") + "</InvoiceByCostCenter>";
//            xml += "<TaxBenefit>" + (Session.getInstance().getJsonPratica().getJSONObject("inserimentoDati").getJSONObject("referenteContratto").getString("agevolazioniFiscali").compareToIgnoreCase("1") == 0 ? "true" : "false") + "</TaxBenefit>";
//            xml += "<IPAIDNumber>" + Session.getInstance().getJsonPratica().getJSONObject("inserimentoDati").getJSONObject("pubblicaAmministrazione").optString("codiceIdentificativoUfficio") + "</IPAIDNumber>";
//            xml += "<SplitPayment>" + (Session.getInstance().getJsonPratica().getJSONObject("inserimentoDati").getJSONObject("pubblicaAmministrazione").getString("splitPayment").compareToIgnoreCase("1") == 0 ? "true" : "false") + "</SplitPayment>";
//            xml += "<EmailInvoice>" + (Session.getInstance().getJsonPratica().getJSONObject("inserimentoDati").getJSONObject("pubblicaAmministrazione").getString("modFattElettronica").compareToIgnoreCase("1") == 0 ? "true" : "false") + "</EmailInvoice>";
//
//            xml += "<PrintedInvoice>" + (Session.getInstance().getJsonPratica().getJSONObject("inserimentoDati").getJSONObject("documentazioneCosti").getString("areaClienti").compareToIgnoreCase("1") == 0 ? "true" : "false") + "</PrintedInvoice>";
//            xml += "<PrintedCostDetails>" + (Session.getInstance().getJsonPratica().getJSONObject("inserimentoDati").getJSONObject("documentazioneCosti").getString("supportoCartaceo").compareToIgnoreCase("1") == 0 ? "true" : "false") + "</PrintedCostDetails>";
//            xml += "<CdromCostDetails>" + (Session.getInstance().getJsonPratica().getJSONObject("inserimentoDati").getJSONObject("documentazioneCosti").getString("supportoCD").compareToIgnoreCase("1") == 0 ? "true" : "false") + "</CdromCostDetails>";
//
//            xml += "<PaymentProfile>";
//
//            xml += "<PaymentMethod>" + (Session.getInstance().getJsonPratica().getJSONObject("infoPagamento").getString("pagamentoTramiteRid").compareToIgnoreCase("1") == 0 ? "RID" : "Carta") + "</PaymentMethod>";
//            xml += "<IBAN>" + Session.getInstance().getJsonPratica().getJSONObject("infoPagamento").getJSONObject("estremiContoCorrente").optString("iban") + "</IBAN>";
//            xml += "<BankName>" + Session.getInstance().getJsonPratica().getJSONObject("infoPagamento").getJSONObject("estremiContoCorrente").optString("nomeBancaPosta") + "</BankName>";
//            xml += "<BankBranch>" + Session.getInstance().getJsonPratica().getJSONObject("infoPagamento").getJSONObject("estremiContoCorrente").optString("agenziaUfficioFiliale") + "</BankBranch>";
//            xml += "<BankAuthorizationflag>" + "false" + "</BankAuthorizationflag>";
//
//            xml += "<BankSubscriber>";
//
//            xml += "<FirstName>" + Session.getInstance().getJsonPratica().getJSONObject("infoPagamento").getJSONObject("sottoscrittoreDelModulo").optString("nome") + "</FirstName>";
//            xml += "<LastName>" + Session.getInstance().getJsonPratica().getJSONObject("infoPagamento").getJSONObject("sottoscrittoreDelModulo").optString("cognome") + "</LastName>";
////            xml += "<PhoneNumber>"+Session.getInstance().getJsonPratica().getJSONObject("infoPagamento").getJSONObject("sottoscrittoreDelModulo").optString("nome")+ "</PhoneNumber>";
////            xml += "<FaxNumber>"+Session.getInstance().getJsonPratica().getJSONObject("infoPagamento").getJSONObject("sottoscrittoreDelModulo").optString("nome")+ "</FaxNumber>";
////            xml += "<EmailAddress>"+Session.getInstance().getJsonPratica().getJSONObject("infoPagamento").getJSONObject("sottoscrittoreDelModulo").optString("nome")+ "</EmailAddress>";
//            xml += "<CodiceFiscale>" + Session.getInstance().getJsonPratica().getJSONObject("infoPagamento").getJSONObject("sottoscrittoreDelModulo").optString("cf") + "</CodiceFiscale>";
//            xml += "<BirthCity>" + Session.getInstance().getJsonPratica().getJSONObject("infoPagamento").getJSONObject("sottoscrittoreDelModulo").optString("luogoDiNascita") + "</BirthCity>";
//            xml += "<BirthProvince>" + Session.getInstance().getJsonPratica().getJSONObject("infoPagamento").getJSONObject("sottoscrittoreDelModulo").optString("provincia") + "</BirthProvince>";
//            xml += "<BirthDate>" + Session.getInstance().getJsonPratica().getJSONObject("infoPagamento").getJSONObject("sottoscrittoreDelModulo").optString("dataDiNascita") + "</BirthDate>";
////            xml += "<Citizenship>"+Session.getInstance().getJsonPratica().getJSONObject("infoPagamento").getJSONObject("sottoscrittoreDelModulo").optString("nome")+ "</Citizenship>";
//            xml += "<Gender>" + Session.getInstance().getJsonPratica().getJSONObject("infoPagamento").getJSONObject("sottoscrittoreDelModulo").optString("gender") + "</Gender>";
////            xml += "<IDType>"+Session.getInstance().getJsonPratica().getJSONObject("infoPagamento").getJSONObject("sottoscrittoreDelModulo").optString("nome")+ "</IDType>";
////            xml += "<IDNumber>"+Session.getInstance().getJsonPratica().getJSONObject("infoPagamento").getJSONObject("sottoscrittoreDelModulo").optString("nome")+ "</IDNumber>";
////            xml += "<IDIssuedBy>"+Session.getInstance().getJsonPratica().getJSONObject("infoPagamento").getJSONObject("sottoscrittoreDelModulo").optString("nome")+ "</IDIssuedBy>";
////            xml += "<IDIssueDate>"+Session.getInstance().getJsonPratica().getJSONObject("infoPagamento").getJSONObject("sottoscrittoreDelModulo").optString("nome")+ "</IDIssueDate>";
//
//            xml += "<Address>";
////            xml += "<AddressDescription>"++ "</AddressDescription>";
////            xml += "<AddressType>"++ "</AddressType>";
//            xml += "<AddressName>" + Session.getInstance().getJsonPratica().getJSONObject("infoPagamento").getJSONObject("sottoscrittoreDelModulo").optString("indirizzo") + "</AddressName>";
//            xml += "<StreetNbr>" + Session.getInstance().getJsonPratica().getJSONObject("infoPagamento").getJSONObject("sottoscrittoreDelModulo").optString("civico") + "</StreetNbr>";
//            xml += "<City>" + Session.getInstance().getJsonPratica().getJSONObject("infoPagamento").getJSONObject("sottoscrittoreDelModulo").optString("citta") + "</City>";
//            xml += "<Province>" + Session.getInstance().getJsonPratica().getJSONObject("infoPagamento").getJSONObject("sottoscrittoreDelModulo").optString("provincia") + "</Province>";
////            xml += "<Country>"++ "</Country>";
//            xml += "<PostalCode>" + Session.getInstance().getJsonPratica().getJSONObject("infoPagamento").getJSONObject("sottoscrittoreDelModulo").optString("cap") + "</PostalCode>";
////            xml += "<ReprFirstName>"++ "</ReprFirstName>";
////            xml += "<ReprLastName>"++ "</ReprLastName>";
////            xml += "<ReprPhoneNumber>"++ "</ReprPhoneNumber>";
////            xml += "<ReprFaxNumber>"++ "</ReprFaxNumber>";
////            xml += "<ReprEmailAddress>"++ "</ReprEmailAddress>";
//            xml += "</Address>";
//
//            xml += "</BankSubscriber>";
//
//            xml += "<BankAccount>";
//
//            xml += "<FirstName>" + Session.getInstance().getJsonPratica().getJSONObject("infoPagamento").getJSONObject("intestatarioConto").optString("nome") + "</FirstName>";
//            xml += "<LastName>" + Session.getInstance().getJsonPratica().getJSONObject("infoPagamento").getJSONObject("intestatarioConto").optString("cognome") + "</LastName>";
////            xml += "<PhoneNumber>"++ "</PhoneNumber>";
////            xml += "<FaxNumber>"++ "</FaxNumber>";
////            xml += "<EmailAddress>"++ "</EmailAddress>";
//            xml += "<CodiceFiscale>" + Session.getInstance().getJsonPratica().getJSONObject("infoPagamento").getJSONObject("intestatarioConto").optString("cf") + "</CodiceFiscale>";
//            xml += "<BirthCity>" + Session.getInstance().getJsonPratica().getJSONObject("infoPagamento").getJSONObject("intestatarioConto").optString("citta") + "</BirthCity>";
//            xml += "<BirthProvince>" + Session.getInstance().getJsonPratica().getJSONObject("infoPagamento").getJSONObject("intestatarioConto").optString("provincia") + "</BirthProvince>";
////            xml += "<BirthDate>"++ "</BirthDate>";
////            xml += "<Citizenship>"++ "</Citizenship>";
////            xml += "<Gender>"++ "</Gender>";
////            xml += "<IDType>"++ "</IDType>";
////            xml += "<IDNumber>"++ "</IDNumber>";
////            xml += "<IDIssuedBy>"++ "</IDIssuedBy>";
////            xml += "<IDIssueDate>"++ "</IDIssueDate>";
//
//            xml += "<Address>";
////            xml += "<AddressDescription>"++ "</AddressDescription>";
////            xml += "<AddressType>"++ "</AddressType>";
//            xml += "<AddressName>" + Session.getInstance().getJsonPratica().getJSONObject("infoPagamento").getJSONObject("intestatarioConto").optString("indirizzo") + "</AddressName>";
//            xml += "<StreetNbr>" + Session.getInstance().getJsonPratica().getJSONObject("infoPagamento").getJSONObject("intestatarioConto").optString("civico") + "</StreetNbr>";
//            xml += "<City>" + Session.getInstance().getJsonPratica().getJSONObject("infoPagamento").getJSONObject("intestatarioConto").optString("citta") + "</City>";
//            xml += "<Province>" + Session.getInstance().getJsonPratica().getJSONObject("infoPagamento").getJSONObject("intestatarioConto").optString("provincia") + "</Province>";
////            xml += "<Country>"++ "</Country>";
//            xml += "<PostalCode>" + Session.getInstance().getJsonPratica().getJSONObject("infoPagamento").getJSONObject("intestatarioConto").optString("cap") + "</PostalCode>";
////            xml += "<ReprFirstName>"++ "</ReprFirstName>";
////            xml += "<ReprLastName>"++ "</ReprLastName>";
////            xml += "<ReprPhoneNumber>"++ "</ReprPhoneNumber>";
////            xml += "<ReprFaxNumber>"++ "</ReprFaxNumber>";
////            xml += "<ReprEmailAddress>"++ "</ReprEmailAddress>";
//            xml += "</Address>";
//
//            xml += "</BankAccount>";
//
//            xml += "<DirectDebitID>"++ "</DirectDebitID>";
//            xml += "<DirectDebitDate>"++ "</DirectDebitDate>";
//            xml += "<DirectDebitPlace>"++ "</DirectDebitPlace>";
//            xml += "<BillFrequency>"++ "</BillFrequency>";
//            xml += "<BillType>"++ "</BillType>";
//            xml += "<BillVendorId>"++ "</BillVendorId>";
//
//            xml += "</PaymentProfile>";
//
//
//            xml += "</InsertPdaAnagrafica_Input>";
////        InsertPdaAnagrafica_Input [FINE]
//
////        InsertPdaOrder_Input [INIZIO]
//            xml += "<InsertPdaOrder_Input>";
//
//            xml += "<Order>";
//
//            xml += "<Status/>";
//            xml += "<OrderType/>";
//            xml += "<SystemId/>";
//
//            boolean hasAbbonamento = false;
//            ArrayList<OffertaMobile> offerteAbbonamento = new ArrayList<>();
//            for (OffertaMobile offertaMobile : offertaMobileArrayList) {
//                if (offertaMobile.getOfferType().compareToIgnoreCase("Abbonamento") == 0) {
//                    hasAbbonamento = true;
//                    offerteAbbonamento.add(offertaMobile);
//                }
//            }
//            if (hasAbbonamento) {
//                xml += "<MobileOffer>";
//
//                xml += "<MobileOfferType>abbonamento</MobileOfferType>";
//                xml += "<TransferCreditFlag>"++ "</TransferCreditFlag>";
//                xml += "<TCGFlag>"++ "</TCGFlag>";
//                xml += "<SubscriberName>"++ "</SubscriberName>";
//                xml += "<SubscriberSurname>"++ "</SubscriberSurname>";
//                xml += "<CompanyName>"++ "</CompanyName>";
//                xml += "<PartitaIVA>"++ "</PartitaIVA>";
//                xml += "<Place>"++ "</Place>";
//
//                for (OffertaMobile offertaMobile : offerteAbbonamento) {
//                    xml += "<SIM>";
//
//                    xml += "<ICCID>" + offertaMobile.getNewICCID() + "</ICCID>";
//                    xml += "<MSISDN>" + offertaMobile.getOldMSISDN() + "</MSISDN>";
//                    xml += "<MNPICCID>" + offertaMobile.getOldICCID() + "</MNPICCID>";
//                    xml += "<TariffPlan>" + offertaMobile.getOfferDescription() + " " + offertaMobile.getOfferDescriptionUpper() + "</TariffPlan>";
////                    xml += "<InternetTariffPlan>"++ "</InternetTariffPlan>";
//
//                    for (OpzioneMobile opzioneMobile : offertaMobile.getOpzioniMobileSelectedArrayListStep8()) {
//                        if (!Boolean.parseBoolean(opzioneMobile.getIsDefault())) {
//                            xml += "<Options>" + opzioneMobile.getOpzioneDesc() + " " + opzioneMobile.getOpzioneDescUpper() + "</Options>";
//                        }
//                    }
//
//                    if (offertaMobile.getAddOnMobileStep8() != null) {
//                        xml += "<Options>" + offertaMobile.getAddOnMobileStep8().getDescFascia() + " " + offertaMobile.getAddOnMobileStep8().getSelectedLabelStep8() + "</Options>";
//                    }
//
//                    boolean hasPromoSuper = false;
//                    for (OpzioneMobile opzioneMobile : offertaMobile.getOpzioniMobileSelectedArrayListStep8()) {
//                        if (!TextUtils.isEmpty(opzioneMobile.getScontoConvergenza())) {
//                            hasPromoSuper = true;
//                            break;
//                        }
//                    }
//                    xml += "<PromoSuper>" + String.valueOf(hasPromoSuper) + "</PromoSuper>";
//
//                    boolean hasInternoMobile = false;
//                    for (OpzioneMobile opzioneMobile : offertaMobile.getOpzioniMobileSelectedArrayListStep8()) {
//                        if (opzioneMobile.getOpzioneDesc().compareToIgnoreCase("Interno Mobile") == 0) {
//                            hasInternoMobile = true;
//                            break;
//                        }
//                    }
//                    xml += "<InternoMobile>" + String.valueOf(hasInternoMobile) + "</InternoMobile>";
//
//                    boolean hasPromoTerminale = false;
//                    for (OpzioneMobile opzioneMobile : offertaMobile.getOpzioniMobileSelectedArrayListStep8()) {
//                        if (opzioneMobile.getOpzioneDesc().compareToIgnoreCase("Promo Terminale") == 0) {
//                            hasPromoTerminale = true;
//                            break;
//                        }
//                    }
//                    xml += "<PromoTerminale>" + String.valueOf(hasPromoTerminale) + "</PromoTerminale>";
//
//                    boolean hasAssistenzaTecnicaEvolutiva = false;
//                    for (OpzioneMobile opzioneMobile : offertaMobile.getOpzioniMobileSelectedArrayListStep8()) {
//                        if (opzioneMobile.getOpzioneDesc().compareToIgnoreCase("Assistenza Tecnica Evolutiva") == 0) {
//                            hasAssistenzaTecnicaEvolutiva = true;
//                            break;
//                        }
//                    }
//                    xml += "<AssistenzaTecnicaEvolutiva>" + String.valueOf(hasAssistenzaTecnicaEvolutiva) + "</AssistenzaTecnicaEvolutiva>";
//
//                    if (offertaMobile.getTerminale1() != null) {
//                        xml += "<DeviceModel>" + offertaMobile.getTerminale1().getTerminaleDescription() + "</DeviceModel>";
//                    }
//                    if (offertaMobile.getTerminale2() != null) {
//                        xml += "<DeviceModel>" + offertaMobile.getTerminale2().getTerminaleDescription() + "</DeviceModel>";
//                    }
//
//                    if (offertaMobile.isMnp()) {
//                        xml += "<Active>" + String.valueOf(offertaMobile.isMnp()) + "</Active>";
//                        xml += "<MNPDonor>" + offertaMobile.getMnpStep8() + "</MNPDonor>";
//                    }
//
//                    xml += "</SIM>";
//                }
//
//                xml += "</MobileOffer>";
//            }
//
//            boolean hasRicaricabile = false;
//            ArrayList<OffertaMobile> offerteRicaricabile = new ArrayList<>();
//            for (OffertaMobile offertaMobile : offertaMobileArrayList) {
//                if (offertaMobile.getOfferType().compareToIgnoreCase("Ricaricabile") == 0) {
//                    hasRicaricabile = true;
//                    offerteRicaricabile.add(offertaMobile);
//                }
//            }
//            if (hasRicaricabile) {
//                xml += "<MobileOffer>";
//
//                xml += "<MobileOfferType>abbonamento</MobileOfferType>";
//                xml += "<TransferCreditFlag>"++ "</TransferCreditFlag>";
//                xml += "<TCGFlag>"++ "</TCGFlag>";
//                xml += "<SubscriberName>"++ "</SubscriberName>";
//                xml += "<SubscriberSurname>"++ "</SubscriberSurname>";
//                xml += "<CompanyName>"++ "</CompanyName>";
//                xml += "<PartitaIVA>"++ "</PartitaIVA>";
//                xml += "<Place>"++ "</Place>";
//
//                for (OffertaMobile offertaMobile : offerteAbbonamento) {
//                    xml += "<SIM>";
//
//                    xml += "<ICCID>" + offertaMobile.getNewICCID() + "</ICCID>";
//                    xml += "<MSISDN>" + offertaMobile.getOldMSISDN() + "</MSISDN>";
//                    xml += "<MNPICCID>" + offertaMobile.getOldICCID() + "</MNPICCID>";
//                    xml += "<TariffPlan>" + offertaMobile.getOfferDescription() + " " + offertaMobile.getOfferDescriptionUpper() + "</TariffPlan>";
////                    xml += "<InternetTariffPlan>"++ "</InternetTariffPlan>";
//
//                    for (OpzioneMobile opzioneMobile : offertaMobile.getOpzioniMobileSelectedArrayListStep8()) {
//                        if (!Boolean.parseBoolean(opzioneMobile.getIsDefault())) {
//                            xml += "<Options>" + opzioneMobile.getOpzioneDesc() + " " + opzioneMobile.getOpzioneDescUpper() + "</Options>";
//                        }
//                    }
//
//                    if (offertaMobile.getAddOnMobileStep8() != null) {
//                        xml += "<Options>" + offertaMobile.getAddOnMobileStep8().getDescFascia() + " " + offertaMobile.getAddOnMobileStep8().getSelectedLabelStep8() + "</Options>";
//                    }
//
//                    String automaticRefill = "";
//                    for (OpzioneMobile opzioneMobile : offertaMobile.getOpzioniMobileSelectedArrayListStep8()) {
//                        if (!TextUtils.isEmpty(opzioneMobile.getValue())) {
//                            automaticRefill = opzioneMobile.getValue();
//                            break;
//                        }
//                    }
//                    xml += "<AutomaticRefill>" + automaticRefill + "</AutomaticRefill>";
//
//                    boolean hasPromoSuper = false;
//                    for (OpzioneMobile opzioneMobile : offertaMobile.getOpzioniMobileSelectedArrayListStep8()) {
//                        if (!TextUtils.isEmpty(opzioneMobile.getScontoConvergenza())) {
//                            hasPromoSuper = true;
//                            break;
//                        }
//                    }
//                    xml += "<PromoSuper>" + String.valueOf(hasPromoSuper) + "</PromoSuper>";
//
//                    boolean hasInternoMobile = false;
//                    for (OpzioneMobile opzioneMobile : offertaMobile.getOpzioniMobileSelectedArrayListStep8()) {
//                        if (opzioneMobile.getOpzioneDesc().compareToIgnoreCase("Interno Mobile") == 0) {
//                            hasInternoMobile = true;
//                            break;
//                        }
//                    }
//                    xml += "<InternoMobile>" + String.valueOf(hasInternoMobile) + "</InternoMobile>";
//
//                    boolean hasPromoTerminale = false;
//                    for (OpzioneMobile opzioneMobile : offertaMobile.getOpzioniMobileSelectedArrayListStep8()) {
//                        if (opzioneMobile.getOpzioneDesc().compareToIgnoreCase("Promo Terminale") == 0) {
//                            hasPromoTerminale = true;
//                            break;
//                        }
//                    }
//                    xml += "<PromoTerminale>" + String.valueOf(hasPromoTerminale) + "</PromoTerminale>";
//
//                    boolean hasAssistenzaTecnicaEvolutiva = false;
//                    for (OpzioneMobile opzioneMobile : offertaMobile.getOpzioniMobileSelectedArrayListStep8()) {
//                        if (opzioneMobile.getOpzioneDesc().compareToIgnoreCase("Assistenza Tecnica Evolutiva") == 0) {
//                            hasAssistenzaTecnicaEvolutiva = true;
//                            break;
//                        }
//                    }
//                    xml += "<AssistenzaTecnicaEvolutiva>" + String.valueOf(hasAssistenzaTecnicaEvolutiva) + "</AssistenzaTecnicaEvolutiva>";
//
//                    if (offertaMobile.getTerminale1() != null) {
//                        xml += "<DeviceModel>" + offertaMobile.getTerminale1().getTerminaleDescription() + "</DeviceModel>";
//                    }
//                    if (offertaMobile.getTerminale2() != null) {
//                        xml += "<DeviceModel>" + offertaMobile.getTerminale2().getTerminaleDescription() + "</DeviceModel>";
//                    }
//
//                    if (offertaMobile.isMnp()) {
//                        xml += "<Active>" + String.valueOf(offertaMobile.isMnp()) + "</Active>";
//                        xml += "<MNPDonor>" + offertaMobile.getMnpStep8() + "</MNPDonor>";
//                    }
//
//                    xml += "</SIM>";
//                }
//
//                xml += "</MobileOffer>";
//            }
//
//            xml += "</Order>";
//
//            xml += "</InsertPdaOrder_Input>";
////        InsertPdaOrder_Input [FINE]
//
////        ServiceRequest [INIZIO]
//            xml += "<ServiceRequest>";
//
//            xml += "<RequestId>"++ "</RequestId>";
//            xml += "<RequestType>"++ "</RequestType>";
//            xml += "<GeneratePdfFlag>"++ "</GeneratePdfFlag>";
//            xml += "<BiometricPdaFlag>"++ "</BiometricPdaFlag>";
//            xml += "<SystemId>"++ "</SystemId>";
//            xml += "<Login>"++ "</Login>";
//            xml += "<CodiceManager>"++ "</CodiceManager>";
//            xml += "<OfferType/>";
//
//            xml += "<Attachment>";
//            xml += "<ActivityFileExt>"++ "</ActivityFileExt>";
//            xml += "<ActivityFileName>"++ "</ActivityFileName>";
//            xml += "<AttachmentType>"++ "</AttachmentType>";
//            xml += "<ActivityFileBuffer>"++ "</ActivityFileBuffer>";
//            xml += "</Attachment>";
//
//            xml += "<TBLUserProfile/>";
//            xml += "<ContractPlace>"++ "</ContractPlace>";
//            xml += "<ContractDate>"++ "</ContractDate>";
//
//            xml += "</ServiceRequest>";
////        ServiceRequest [FINE]
//
//
//            xml += "</ins:InsertPdaRequestReq>\n" +
//                    "</soapenv:Body>\n" +
//                    "</soapenv:Envelope>";
//
//        } catch (JSONException e) {
//
//        }
//    }
}
