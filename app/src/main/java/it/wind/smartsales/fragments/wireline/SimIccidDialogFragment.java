package it.wind.smartsales.fragments.wireline;

import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import it.wind.smartsales.R;

public class SimIccidDialogFragment extends DialogFragment {

    private Button indietroButton;
    private Button continuaButton;
    private SimIccdListener listener;

    public SimIccidDialogFragment() {
    }

    public static SimIccidDialogFragment newInstance(SimIccdListener listener) {
        SimIccidDialogFragment fragment = new SimIccidDialogFragment();
        fragment.listener = listener;
        return fragment;
    }

    private void initializeComponents(View view) {
        indietroButton = (Button) view.findViewById(R.id.indietro_button);
        continuaButton = (Button) view.findViewById(R.id.continua_button);
    }


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    @Override
    public void onResume() {
        super.onResume();
        int width = getResources().getDimensionPixelSize(R.dimen.dialog_fragment_sim_iccid);
        getDialog().getWindow().setLayout(width, ViewGroup.LayoutParams.WRAP_CONTENT);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.dialog_fragment_sim_iccid, container, false);
        final TextView simIccid = (TextView) view.findViewById(R.id.sim_iccid);
        initializeComponents(view);

        indietroButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onResponse(false, "");
                dismiss();
            }
        });
        continuaButton.setEnabled(true);
        continuaButton.setActivated(true);
        continuaButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onResponse(true, simIccid.getText().toString());
                dismiss();
            }
        });

        return view;
    }

    public interface SimIccdListener {
        void onResponse(boolean hasDone, String simIccid);
    }
}
