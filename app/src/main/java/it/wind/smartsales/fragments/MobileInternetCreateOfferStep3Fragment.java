package it.wind.smartsales.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import it.wind.smartsales.R;
import it.wind.smartsales.activity.MainActivity;
import it.wind.smartsales.adapter.AddOnAdapter;
import it.wind.smartsales.adapter.MobileOfferStep3Adapter;
import it.wind.smartsales.adapter.OpzioniMobileAdapter;
import it.wind.smartsales.adapter.TerminaliAdapter;
import it.wind.smartsales.commons.Log;
import it.wind.smartsales.commons.Utils;
import it.wind.smartsales.customviews.AspectRatioView;
import it.wind.smartsales.customviews.HorizontalSpaceItemDecoration;
import it.wind.smartsales.customviews.VerticalSpaceItemDecoration;
import it.wind.smartsales.customviews.VerticalTextView;
import it.wind.smartsales.dao.CatalogoAddOnMobileDAO;
import it.wind.smartsales.dao.CatalogoOpzioniMobileDAO;
import it.wind.smartsales.dao.CatalogoPianiTariffariDAO;
import it.wind.smartsales.dao.CatalogoTerminaliDAO;
import it.wind.smartsales.dao.PraticheDAO;
import it.wind.smartsales.entities.AddOnMobile;
import it.wind.smartsales.entities.OffertaMobile;
import it.wind.smartsales.entities.OpzioneMobile;
import it.wind.smartsales.entities.PercentualeSconto;
import it.wind.smartsales.entities.PianoTariffario;
import it.wind.smartsales.entities.Session;
import it.wind.smartsales.entities.Terminale;
import it.wind.smartsales.entities.TerminaleBean;

/**
 * Created by luca.quaranta on 10/02/2016.
 */
public class MobileInternetCreateOfferStep3Fragment extends Fragment {

    private RecyclerView addOnRecyclerView, terminaliRecyclerView, opzioniRecyclerView, mobileOfferRecyclerView;
    private RecyclerView.LayoutManager mLayoutManagerAddOn, mLayoutManagerTerminali, mLayoutManagerOpzioni, mLayoutManagerMobileOffer;
    private RecyclerView.Adapter addOnAdapter, terminaliAdapter, opzioniAdapter, mobileOfferAdapter;

    private RelativeLayout pacchetto1, pacchetto2, pacchetto3, pacchetto4;
    private AspectRatioView pacchetto1Tick, pacchetto2Tick, pacchetto3Tick, pacchetto4Tick;
    private Button continuaButton;
    private LinearLayout addOnLayout, addOnEmptyLayout, terminaliLayout, opzioniLayout;
    private int pacchettoVisibile = 1, offertaVisibile = 1;
    private VerticalTextView pacchetto1Text, pacchetto2Text, pacchetto3Text, pacchetto4Text;
    private boolean aperturaStep=true;

    private CheckBox fuoriStandardCheck;

    private void initializeComponents(View view) {

        fuoriStandardCheck = (CheckBox) view.findViewById(R.id.fuori_standard_check);

        addOnRecyclerView = (RecyclerView) view.findViewById(R.id.add_on_recycler_view);
        terminaliRecyclerView = (RecyclerView) view.findViewById(R.id.terminali_recycler_view);
        opzioniRecyclerView = (RecyclerView) view.findViewById(R.id.opzioni_recycler_view);
        mobileOfferRecyclerView = (RecyclerView) view.findViewById(R.id.mobile_offer_recycler_view);

        continuaButton = (Button) view.findViewById(R.id.continua_button);

        pacchetto1 = (RelativeLayout) view.findViewById(R.id.sede_1);
        pacchetto2 = (RelativeLayout) view.findViewById(R.id.pacchetto_2);
        pacchetto3 = (RelativeLayout) view.findViewById(R.id.pacchetto_3);
        pacchetto4 = (RelativeLayout) view.findViewById(R.id.pacchetto_4);

        pacchetto1Tick = (AspectRatioView) view.findViewById(R.id.pacchetto_1_tick);
        pacchetto2Tick = (AspectRatioView) view.findViewById(R.id.pacchetto_2_tick);
        pacchetto3Tick = (AspectRatioView) view.findViewById(R.id.pacchetto_3_tick);
        pacchetto4Tick = (AspectRatioView) view.findViewById(R.id.pacchetto_4_tick);

        pacchetto1Text = (VerticalTextView) view.findViewById(R.id.pacchetto_1_text);
        pacchetto2Text = (VerticalTextView) view.findViewById(R.id.pacchetto_2_text);
        pacchetto3Text = (VerticalTextView) view.findViewById(R.id.pacchetto_3_text);
        pacchetto4Text = (VerticalTextView) view.findViewById(R.id.pacchetto_4_text);

        addOnLayout = (LinearLayout) view.findViewById(R.id.add_on_layout);
        addOnEmptyLayout = (LinearLayout) view.findViewById(R.id.add_on_layout_empty);
        terminaliLayout = (LinearLayout) view.findViewById(R.id.terminali_layout);
        opzioniLayout = (LinearLayout) view.findViewById(R.id.opzioni_layout);

        try {
            JSONArray pacchetti = (JSONArray) Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONArray("pacchetti");
            switch (pacchetti.length()) {
                case 4:
                    pacchetto4.setVisibility(View.VISIBLE);
                case 3:
                    pacchetto3.setVisibility(View.VISIBLE);
                case 2:
                    pacchetto2.setVisibility(View.VISIBLE);
                case 1:
                    pacchetto1.setVisibility(View.VISIBLE);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        initMobileOfferList();
        initAddOnList();
        initTerminaliList();
        initOpzioniList();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_mobile_internet_create_offer_step_3, container, false);
        initializeComponents(view);
        try {
            JSONObject pratica = Session.getInstance().getJsonPratica().getJSONObject("pratica");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        populatePage();
        checkValidation();

        fuoriStandardCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {

                    Session.getInstance().getJsonPratica().put("isFuoriStandard", fuoriStandardCheck.isChecked());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        pacchetto1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openPacchetto(1);
            }
        });

        pacchetto2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openPacchetto(2);
            }
        });

        pacchetto3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openPacchetto(3);
            }
        });

        pacchetto4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openPacchetto(4);
            }
        });

        continuaButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Session.getInstance().getPratica().setStep(PraticheDAO.STEP4);
                PraticheDAO.updatePratica(getActivity().getBaseContext());

                Log.d("JSON PRATICA)", Session.getInstance().getJsonPratica().toString());
                if (!Session.getInstance().getJsonPratica().optBoolean("isFuoriStandard")) {

                    aggregateOffers();
                    Session.getInstance().getPratica().setStep(PraticheDAO.STEP5);
                    PraticheDAO.updatePratica(getActivity().getBaseContext());
                }
                if (Session.getInstance().getDealer().getDealerRole().compareTo("UserSales") != 0)
                    ((MainActivity) getActivity()).goForward();
                else
                    ((MainActivity) getActivity()).goForwardSelectOffer();
            }
        });
        checkInitialValidationPacchetto();
        //store data on sqlLite
        PraticheDAO.updatePratica(this.getActivity().getBaseContext());

        return view;
    }

    private void initAddOnList() {
        addOnRecyclerView.setHasFixedSize(true);
        mLayoutManagerAddOn = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        addOnRecyclerView.setLayoutManager(mLayoutManagerAddOn);
        addOnRecyclerView.setItemAnimator(new DefaultItemAnimator());
        addOnRecyclerView.addItemDecoration(new HorizontalSpaceItemDecoration(20));
    }

    private void initTerminaliList() {
        terminaliRecyclerView.setHasFixedSize(true);
        mLayoutManagerTerminali = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        terminaliRecyclerView.setLayoutManager(mLayoutManagerTerminali);
        terminaliRecyclerView.setItemAnimator(new DefaultItemAnimator());
        terminaliRecyclerView.addItemDecoration(new HorizontalSpaceItemDecoration(20));
    }

    private void initOpzioniList() {
        opzioniRecyclerView.setHasFixedSize(true);
        mLayoutManagerOpzioni = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        opzioniRecyclerView.setLayoutManager(mLayoutManagerOpzioni);
        opzioniRecyclerView.setItemAnimator(new DefaultItemAnimator());
        opzioniRecyclerView.addItemDecoration(new HorizontalSpaceItemDecoration(20));
    }

    private void initMobileOfferList() {
        mLayoutManagerMobileOffer = new LinearLayoutManager(getActivity());
        mobileOfferRecyclerView.setLayoutManager(mLayoutManagerMobileOffer);
        mobileOfferRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mobileOfferRecyclerView.addItemDecoration(new VerticalSpaceItemDecoration(30));
    }

    private void populateAddOnList(ArrayList<AddOnMobile> addOnMobileList, ArrayList<AddOnMobile> addOnSelezionati) {
        addOnAdapter = new AddOnAdapter(this, addOnMobileList, addOnSelezionati);
        addOnRecyclerView.setAdapter(addOnAdapter);
    }

    private void populateTerminaliList(ArrayList<Terminale> terminali, ArrayList<TerminaleBean> terminaliSelezionati) {
        terminaliAdapter = new TerminaliAdapter(this, terminali, terminaliSelezionati);
        terminaliRecyclerView.setAdapter(terminaliAdapter);
    }

    private void populateOpzioniList(ArrayList<OpzioneMobile> opzioniMobile, ArrayList<OpzioneMobile> opzioniSelezionate) {
        opzioniAdapter = new OpzioniMobileAdapter(this, opzioniMobile, opzioniSelezionate);
        opzioniRecyclerView.setAdapter(opzioniAdapter);
    }

    private void populateMobileOfferList(ArrayList<OffertaMobile> offerteMobile, ArrayList<String> offerteList) {
        mobileOfferAdapter = new MobileOfferStep3Adapter(this, offerteMobile, null);
        mobileOfferRecyclerView.setAdapter(mobileOfferAdapter);
    }

    private void openPacchetto(int numPacchetto) {
        switch (numPacchetto) {
            case 1:
                pacchetto1.setBackgroundColor(getActivity().getResources().getColor(android.R.color.white));
                pacchetto2.setBackgroundColor(getActivity().getResources().getColor(R.color.wind_color_transparent_light_gray));
                pacchetto3.setBackgroundColor(getActivity().getResources().getColor(R.color.wind_color_transparent_light_gray));
                pacchetto4.setBackgroundColor(getActivity().getResources().getColor(R.color.wind_color_transparent_light_gray));
                pacchetto1Text.setTextColor(getActivity().getResources().getColor(R.color.colorAccent));
                pacchetto2Text.setTextColor(getActivity().getResources().getColor(android.R.color.black));
                pacchetto3Text.setTextColor(getActivity().getResources().getColor(android.R.color.black));
                pacchetto4Text.setTextColor(getActivity().getResources().getColor(android.R.color.black));

                pacchettoVisibile = 1;
                break;

            case 2:
                pacchetto1.setBackgroundColor(getActivity().getResources().getColor(R.color.wind_color_transparent_light_gray));
                pacchetto2.setBackgroundColor(getActivity().getResources().getColor(android.R.color.white));
                pacchetto3.setBackgroundColor(getActivity().getResources().getColor(R.color.wind_color_transparent_light_gray));
                pacchetto4.setBackgroundColor(getActivity().getResources().getColor(R.color.wind_color_transparent_light_gray));
                pacchetto1Text.setTextColor(getActivity().getResources().getColor(android.R.color.black));
                pacchetto2Text.setTextColor(getActivity().getResources().getColor(R.color.colorAccent));
                pacchetto3Text.setTextColor(getActivity().getResources().getColor(android.R.color.black));
                pacchetto4Text.setTextColor(getActivity().getResources().getColor(android.R.color.black));

                pacchettoVisibile = 2;
                break;

            case 3:
                pacchetto1.setBackgroundColor(getActivity().getResources().getColor(R.color.wind_color_transparent_light_gray));
                pacchetto2.setBackgroundColor(getActivity().getResources().getColor(R.color.wind_color_transparent_light_gray));
                pacchetto3.setBackgroundColor(getActivity().getResources().getColor(android.R.color.white));
                pacchetto4.setBackgroundColor(getActivity().getResources().getColor(R.color.wind_color_transparent_light_gray));
                pacchetto1Text.setTextColor(getActivity().getResources().getColor(android.R.color.black));
                pacchetto2Text.setTextColor(getActivity().getResources().getColor(android.R.color.black));
                pacchetto3Text.setTextColor(getActivity().getResources().getColor(R.color.colorAccent));
                pacchetto4Text.setTextColor(getActivity().getResources().getColor(android.R.color.black));

                pacchettoVisibile = 3;
                break;

            case 4:
                pacchetto1.setBackgroundColor(getActivity().getResources().getColor(R.color.wind_color_transparent_light_gray));
                pacchetto2.setBackgroundColor(getActivity().getResources().getColor(R.color.wind_color_transparent_light_gray));
                pacchetto3.setBackgroundColor(getActivity().getResources().getColor(R.color.wind_color_transparent_light_gray));
                pacchetto4.setBackgroundColor(getActivity().getResources().getColor(android.R.color.white));
                pacchetto1Text.setTextColor(getActivity().getResources().getColor(android.R.color.black));
                pacchetto2Text.setTextColor(getActivity().getResources().getColor(android.R.color.black));
                pacchetto3Text.setTextColor(getActivity().getResources().getColor(android.R.color.black));
                pacchetto4Text.setTextColor(getActivity().getResources().getColor(R.color.colorAccent));

                pacchettoVisibile = 4;
                break;
        }
        offertaVisibile = 1;
        populatePage();
    }

    private void populatePage() {
        try {
            JSONArray pacchetti = (JSONArray) Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONArray("pacchetti");
            JSONObject pacchetto = pacchetti.getJSONObject(pacchettoVisibile - 1);
            JSONArray offerte = (JSONArray) pacchetto.getJSONArray("offerte");
            ArrayList<OffertaMobile> offerteList = new ArrayList<>();

            for (int i = 0; i < offerte.length(); i++) {
                JSONObject offertaJO = offerte.getJSONObject(i);
                OffertaMobile offertaMobile = Utils.createOffertaMobile(offertaJO);
                offerteList.add(offertaMobile);
            }

            Collections.sort(offerteList, new Comparator<OffertaMobile>() {
                @Override
                public int compare(OffertaMobile offertaMobile1, OffertaMobile offertaMobile2) {
                    return offertaMobile2.getOfferType().compareToIgnoreCase(offertaMobile1.getOfferType());
                }
            });

//            TODO: Popolare la lista a sinistra
            offerteList.get(0).setSelectedStep3(true);
            populateMobileOfferList(offerteList, null);

            populateRightSection(offerteList.get(offertaVisibile - 1));
            checkValidationPacchetto();

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void populateRightSection(OffertaMobile offertaMobile) {

        Log.d("JSON PRATICA)", Session.getInstance().getJsonPratica().toString());

        try {
            fuoriStandardCheck.setChecked(Session.getInstance().getJsonPratica().optBoolean("isFuoriStandard"));


            JSONArray pacchetti = Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONArray("pacchetti");
            JSONObject pacchetto = pacchetti.getJSONObject(pacchettoVisibile - 1);
            JSONArray offerte = pacchetto.getJSONArray("offerte");

            for (int i = 0; i < offerte.length(); i++) {
                if (offerte.getJSONObject(i).getString("idOfferta").compareToIgnoreCase(String.valueOf(offertaMobile.getIdOfferta())) == 0) {
                    offertaVisibile = i + 1;
                }
            }

            JSONObject offerta = offerte.getJSONObject(offertaVisibile - 1);

            int value = 0;
            switch (pacchetto.getInt("question5Web")) {
                case 1:
                    value = 0;
                    break;
                case 2:
                    value = 3;
                    break;
                case 3:
                    value = 5;
                    break;
                case 4:
                    value = 10;
                    break;
                case 5:
                    value = 25;
                    break;
            }

            ArrayList<AddOnMobile> addOnMobiles;

            if(pacchetto.getBoolean("question1Offerta")||pacchetto.getBoolean("question1Offerta")) {
                addOnMobiles = CatalogoAddOnMobileDAO.retrieveAddOnMobile(getActivity(), String.valueOf(offertaMobile.getIdOfferta()), String.valueOf(value));
            }else {
                addOnMobiles = CatalogoAddOnMobileDAO.retrieveAllAddOnMobile(getActivity(), String.valueOf(offertaMobile.getIdOfferta()), String.valueOf(value));
            }

            if (addOnMobiles.size() > 0) {
                addOnLayout.setVisibility(View.VISIBLE);
                addOnEmptyLayout.setVisibility(View.GONE);

                JSONArray addOnMobileList = offerta.getJSONArray("addOnMobileList");
                ArrayList<AddOnMobile> addOnSelezionati = new ArrayList<>();
                for (int i = 0; i < addOnMobileList.length(); i++) {
                    AddOnMobile addOnMobile1 = Utils.createAddOnMobile(addOnMobileList.getJSONObject(i));
                    addOnSelezionati.add(addOnMobile1);
                }

                populateAddOnList(addOnMobiles, addOnSelezionati);
            } else {
                addOnLayout.setVisibility(View.GONE);
                addOnEmptyLayout.setVisibility(View.VISIBLE);
            }


            ArrayList<OpzioneMobile> opzioneMobiles = CatalogoOpzioniMobileDAO.retrieveOpzioniMobile(getActivity(), offertaMobile);
            if (opzioneMobiles.size() > 0) {
                JSONArray opzioniMobileList = offerta.getJSONArray("opzioniMobileList");
                ArrayList<OpzioneMobile> opzioniMobileSelezionate = new ArrayList<>();
                for (int i = 0; i < opzioniMobileList.length(); i++) {
                    OpzioneMobile opzioneMobile1 = Utils.createOpzioneMobile(opzioniMobileList.getJSONObject(i));
                    opzioniMobileSelezionate.add(opzioneMobile1);
                }

                opzioniLayout.setVisibility(View.VISIBLE);
                populateOpzioniList(opzioneMobiles, opzioniMobileSelezionate);
            } else {
                opzioniLayout.setVisibility(View.INVISIBLE);
            }


            ArrayList<Terminale> terminali = CatalogoTerminaliDAO.retrieveTerminali(getActivity(), offertaMobile, pacchetto.getBoolean("question2Smartphone"), pacchetto.getBoolean("question2Tablet"), pacchetto.getBoolean("question3Apple"), pacchetto.getBoolean("question3Windows"), pacchetto.getBoolean("question3Samsung"), pacchetto.getBoolean("question3Altro"));
            if (terminali.size() > 0) {
                JSONArray terminaleBeanList = offerta.getJSONArray("terminaleBeanList");
                ArrayList<TerminaleBean> terminaliSelezionati = new ArrayList<>();
                for (int i = 0; i < terminaleBeanList.length(); i++) {
                    TerminaleBean terminaleBean1 = Utils.createTerminaleBean(terminaleBeanList.getJSONObject(i));
                    terminaliSelezionati.add(terminaleBean1);
                }

                terminaliLayout.setVisibility(View.VISIBLE);
                populateTerminaliList(terminali, terminaliSelezionati);
            } else {
                terminaliLayout.setVisibility(View.INVISIBLE);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    public int incrementOfferNumSim() {
        int selectedSim = 0;
        try {
            JSONArray pacchetti = Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONArray("pacchetti");
            JSONObject pacchetto = pacchetti.getJSONObject(pacchettoVisibile - 1);
            JSONArray offerte = pacchetto.getJSONArray("offerte");
            JSONObject offerta = offerte.getJSONObject(offertaVisibile - 1);

            if (offerta.getInt("selectedNumSimStep3") < 999) {
                offerta.put("selectedNumSimStep3", offerta.getInt("selectedNumSimStep3") + 1);
                Session.getInstance().getJsonPratica().getJSONObject("pratica").put("offerteRaggruppate", new JSONArray());
            }
            selectedSim = offerta.getInt("selectedNumSimStep3");

        } catch (JSONException e) {
            e.printStackTrace();
        }
        checkValidationPacchetto();
        return selectedSim;
    }

    public int decrementOfferNumSim() {
        int selectedSim = 0;
        int numTotSimRicaricabile = 0;

        try {
            JSONArray pacchetti = Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONArray("pacchetti");
            JSONObject pacchetto = pacchetti.getJSONObject(pacchettoVisibile - 1);
            JSONArray offerte = pacchetto.getJSONArray("offerte");
            JSONObject offerta = offerte.getJSONObject(offertaVisibile - 1);
            for(int i = 0 ; i< offerte.length(); i++){
                numTotSimRicaricabile += offerte.getJSONObject(i).getInt("selectedNumSimStep3");
            }

            boolean isDecrementPossible = true;

            int totAddOnMobile = 0;
            JSONArray addOnMobileList = offerta.getJSONArray("addOnMobileList");
            for (int i = 0; i < addOnMobileList.length(); i++) {
                totAddOnMobile += addOnMobileList.getJSONObject(i).getInt("selectedNumSimStep3");
            }
            if (totAddOnMobile == numTotSimRicaricabile){//offerta.getInt("selectedNumSimStep3")) {
                isDecrementPossible = false;
            }


            int totTerminali = 0;
            JSONArray terminaleBeanList = offerta.getJSONArray("terminaleBeanList");
            for (int i = 0; i < terminaleBeanList.length(); i++) {
                totTerminali += terminaleBeanList.getJSONObject(i).getInt("selectedNumSimStep3Mnp");
                totTerminali += terminaleBeanList.getJSONObject(i).getInt("selectedNumSimStep3NuovaLinea");
            }
            if (totTerminali == numTotSimRicaricabile){//offerta.getInt("selectedNumSimStep3")) {
                isDecrementPossible = false;
            }

            JSONArray opzioniMobileList = offerta.getJSONArray("opzioniMobileList");
            for (int i = 0; i < opzioniMobileList.length(); i++) {
                if (opzioniMobileList.getJSONObject(i).getInt("selectedNumSimStep3") == numTotSimRicaricabile){//offerta.getInt("selectedNumSimStep3")) {
                    isDecrementPossible = false;
                }
            }


            if (numTotSimRicaricabile > 1){//offerta.getInt("selectedNumSimStep3") > 1) {
                if (isDecrementPossible) {
                    offerta.put("selectedNumSimStep3", offerta.getInt("selectedNumSimStep3") - 1);
                    Session.getInstance().getJsonPratica().getJSONObject("pratica").put("offerteRaggruppate", new JSONArray());
                } else {
                    Utils.showWarningMessage(getActivity(), "", "Verificare gli elementi selezionati.");
                }
            }
            selectedSim = offerta.getInt("selectedNumSimStep3");

        } catch (JSONException e) {
            e.printStackTrace();
        }
        checkValidationPacchetto();
        return selectedSim;
    }

//    public void saveSelectedAddOnMobile(AddOnMobile addOnMobile) {
//        try {
//            JSONArray pacchetti = Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONArray("pacchetti");
//            JSONObject pacchetto = pacchetti.getJSONObject(pacchettoVisibile - 1);
//            JSONArray offerte = pacchetto.getJSONArray("offerte");
//            JSONObject offerta = offerte.getJSONObject(offertaVisibile - 1);
//            JSONArray addOnMobileList = offerta.getJSONArray("addOnMobileList");
//
//            ArrayList<AddOnMobile> addOnMobileArrayList = new ArrayList<AddOnMobile>();
//            for (int i = 0; i<addOnMobileList.length(); i++){
//                if (addOnMobileList.getJSONObject(i).getString("descFascia").compareToIgnoreCase(addOnMobile.getDescFascia()) == 0) {
//                    addOnMobileList.remove(i);
//                    i--;
//                } else {
//                    AddOnMobile addOnMobile1 = Utils.createAddOnMobile(addOnMobileList.getJSONObject(i));
//                    addOnMobileArrayList.add(addOnMobile1);
//                }
//            }
//            ((AddOnAdapter)addOnRecyclerView.getAdapter()).updateAddOnMobileSelezionati(addOnMobileArrayList);
//
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//    }
    //MW commentato ex gestione degli addOn ai piani ricaricabili in quanto ora i GB sono sherati
    /*public int incrementAddOnMobileNumSim(AddOnMobile addOnMobile) {
        int selectedSim = 1;
        try {
            JSONArray pacchetti = Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONArray("pacchetti");
            JSONObject pacchetto = pacchetti.getJSONObject(pacchettoVisibile - 1);
            JSONArray offerte = pacchetto.getJSONArray("offerte");
            JSONObject offerta = offerte.getJSONObject(offertaVisibile - 1);
            JSONArray addOnMobileList = offerta.getJSONArray("addOnMobileList");

            int totAddOnMobile = 0;
            for (int i = 0; i < addOnMobileList.length(); i++) {
                totAddOnMobile += addOnMobileList.getJSONObject(i).getInt("selectedNumSimStep3");
            }

            boolean trovato = false;
            for (int i = 0; i < addOnMobileList.length(); i++) {
                if (addOnMobileList.getJSONObject(i).getString("descFascia").compareToIgnoreCase(addOnMobile.getDescFascia()) == 0) {
                    trovato = true;
                    if (addOnMobileList.getJSONObject(i).getInt("selectedNumSimStep3") < 999 && totAddOnMobile < offerta.getInt("selectedNumSimStep3")) {
                        selectedSim = addOnMobileList.getJSONObject(i).getInt("selectedNumSimStep3") + 1;
                        addOnMobileList.getJSONObject(i).put("selectedNumSimStep3", selectedSim);
                        Session.getInstance().getJsonPratica().getJSONObject("pratica").put("offerteRaggruppate", new JSONArray());
                    } else {
                        Utils.showWarningMessage(getActivity(), "", "Superato il numero massimo di pacchetti selezionabili");
                    }
                }
            }

            if (!trovato && totAddOnMobile < offerta.getInt("selectedNumSimStep3")) {
                JSONObject addOnMobileJO = Utils.createAddOnMobile(addOnMobile);
                addOnMobileJO.put("selectedNumSimStep3", 1);
                addOnMobileList.put(addOnMobileJO);
                Session.getInstance().getJsonPratica().getJSONObject("pratica").put("offerteRaggruppate", new JSONArray());
            }

            ArrayList<AddOnMobile> addOnMobileArrayList = new ArrayList<AddOnMobile>();
            for (int i = 0; i < addOnMobileList.length(); i++) {
                AddOnMobile addOnMobile1 = Utils.createAddOnMobile(addOnMobileList.getJSONObject(i));
                addOnMobileArrayList.add(addOnMobile1);
            }
            ((AddOnAdapter) addOnRecyclerView.getAdapter()).updateAddOnMobileSelezionati(addOnMobileArrayList);

            checkValidationPacchetto();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        //store data on sqlLite
        PraticheDAO.updatePratica(this.getActivity().getBaseContext());


        return selectedSim;
    }*/

    public int incrementAddOnMobileNumSim(AddOnMobile addOnMobile) {
        int selectedSim = 1;
        int selSimRicaricabiliTotale = 0;
        int selSimRicaricabiliPerPacchetto = 0;
        boolean show=true;
        try {
            JSONArray pacchetti = Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONArray("pacchetti");
            JSONObject pacchetto = pacchetti.getJSONObject(pacchettoVisibile - 1);
            JSONArray offerte = pacchetto.getJSONArray("offerte");
            JSONArray addOnMobileAll = Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONArray("addOnMobileAllPacchetti");
            boolean addOnTrovato=false;
            for (int p = 0; p < pacchetti.length(); p++) {
//                for(pacchetti.getJSONObject(p).getJSONArray("offerte")){
               for (int i = 0; i < pacchetti.getJSONObject(p).getJSONArray("offerte").length();i++){//offerte.length(); i++) {
                    if (pacchetti.getJSONObject(p).getJSONArray("offerte").getJSONObject(i).getString("offerType").compareTo("Ricaricabile") == 0) {
                        selSimRicaricabiliTotale += pacchetti.getJSONObject(p).getJSONArray("offerte").getJSONObject(i).getInt("selectedNumSimStep3");
                    }
                }
            }
            for (int i = 0; i < offerte.length();i++){//offerte.length(); i++) {
                if (offerte.getJSONObject(i).getString("offerType").compareTo("Ricaricabile") == 0) {
                    selSimRicaricabiliPerPacchetto += offerte.getJSONObject(i).getInt("selectedNumSimStep3");
                }
            }


            int numeroGB_PacchettiAssociati=0;
            int numeroGB_PacchettiAssociatiperPacchetto=0;

            if(addOnMobileAll.length()>0) {
                //MW Controllo quanti pacchetti GB in totale tra i pacchetti ho gia assegnato rispetto al nr di SIM totali:
                for (int j = 0; j <addOnMobileAll.length() ; j++) {
                    numeroGB_PacchettiAssociati+=Integer.valueOf(addOnMobileAll.getJSONObject(j).getString("selectedNumSimStep3"));
                }
                //MW Controllo quanti pacchetti GB nel singolo  pacchetto ho gia assegnato rispetto al nr di SIM del pacchetto:
                //for (int j = 0; j <offerte.length() ; j++) {

                    for(int f = 0; f<offerte.getJSONObject(0).getJSONArray("addOnMobileList").length();f++){
                        //if(offerte.getJSONObject(0).getString("offerType").compareTo("Ricaricabile")==0) {
                            numeroGB_PacchettiAssociatiperPacchetto += Integer.valueOf(offerte.getJSONObject(0).getJSONArray("addOnMobileList").getJSONObject(f).getString("selectedNumSimStep3"));
                        //}
                    }

                //}

                if(numeroGB_PacchettiAssociatiperPacchetto<selSimRicaricabiliPerPacchetto) {
                    for (int j = 0; j < addOnMobileAll.length(); j++) {

                        if (addOnMobileAll.getJSONObject(j).getString("descFascia").compareTo(addOnMobile.getDescFascia()) == 0 && Integer.valueOf(addOnMobileAll.getJSONObject(j).getString("selectedNumSimStep3")) < selSimRicaricabiliTotale) {
                            int addon = Integer.valueOf(addOnMobileAll.getJSONObject(j).getString("selectedNumSimStep3")) + 1;
                            addOnMobileAll.getJSONObject(j).put("selectedNumSimStep3", addon);
                            addOnTrovato = true;
                        }

                    }
                    if (!addOnTrovato) {
                        addOnMobile.setSelectedNumSimStep3(1);
                        addOnMobileAll.put(Utils.createAddOnMobile(addOnMobile));
                        addOnMobile.setSelectedNumSimStep3(0);

                    }
                }

            }else{
                addOnMobile.setSelectedNumSimStep3(1);
                addOnMobileAll.put(Utils.createAddOnMobile(addOnMobile));
                addOnMobile.setSelectedNumSimStep3(0);

            }


            //JSONObject offerta = offerte.getJSONObject(offertaVisibile - 1);
            for (int k = 0; k < offerte.length(); k++) {
                JSONObject offerta = offerte.getJSONObject(k);
                JSONArray addOnMobileList = offerta.getJSONArray("addOnMobileList");

                int totAddOnMobile = 0;
                for (int i = 0; i < addOnMobileList.length(); i++) {
                    totAddOnMobile += addOnMobileList.getJSONObject(i).getInt("selectedNumSimStep3");
                }

                boolean trovato = false;
                for (int i = 0; i < addOnMobileList.length(); i++) {
                    if (addOnMobileList.getJSONObject(i).getString("descFascia").compareToIgnoreCase(addOnMobile.getDescFascia()) == 0) {
                        trovato = true;
                        if (addOnMobileList.getJSONObject(i).getInt("selectedNumSimStep3") < 999 && totAddOnMobile < selSimRicaricabiliPerPacchetto){//offerta.getInt("selectedNumSimStep3")) {
                            selectedSim = addOnMobileList.getJSONObject(i).getInt("selectedNumSimStep3") + 1;
                            addOnMobileList.getJSONObject(i).put("selectedNumSimStep3", selectedSim);
                            Session.getInstance().getJsonPratica().getJSONObject("pratica").put("offerteRaggruppate", new JSONArray());



                        } else {
                            if(show) {
                                Utils.showWarningMessage(getActivity(), "", "Superato il numero massimo di pacchetti selezionabili");
                                show=false;
                            }
                        }
                    }
                }

                if (!trovato && totAddOnMobile < selSimRicaricabiliTotale) {
                    JSONObject addOnMobileJO = Utils.createAddOnMobile(addOnMobile);
                    addOnMobileJO.put("selectedNumSimStep3", 1);
                    addOnMobileList.put(addOnMobileJO);
                    Session.getInstance().getJsonPratica().getJSONObject("pratica").put("offerteRaggruppate", new JSONArray());
                }

                ArrayList<AddOnMobile> addOnMobileArrayList = new ArrayList<AddOnMobile>();
                for (int i = 0; i < addOnMobileList.length(); i++) {
                    AddOnMobile addOnMobile1 = Utils.createAddOnMobile(addOnMobileList.getJSONObject(i));
                    addOnMobileArrayList.add(addOnMobile1);
                }
                ((AddOnAdapter) addOnRecyclerView.getAdapter()).updateAddOnMobileSelezionati(addOnMobileArrayList);

                checkValidationPacchetto();
            }
            }catch(JSONException e){
                e.printStackTrace();
            }

            //store data on sqlLite
            PraticheDAO.updatePratica(this.getActivity().getBaseContext());


        return selectedSim;
    }
    public int decrementAddOnMobileNumSim(AddOnMobile addOnMobile) {
        int selectedSim = 0;
        int selSimRicaricabiliTotale = 0;

        try {
            JSONArray pacchetti = Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONArray("pacchetti");
            JSONObject pacchetto = pacchetti.getJSONObject(pacchettoVisibile - 1);
            JSONArray offerte = pacchetto.getJSONArray("offerte");

            JSONArray addOnMobileAll = Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONArray("addOnMobileAllPacchetti");


            if(addOnMobileAll.length()>0) {
                for (int i = 0; i <addOnMobileAll.length() ; i++) {

                    if(addOnMobileAll.getJSONObject(i).getString("descFascia").compareTo(addOnMobile.getDescFascia())==0 && Integer.valueOf(addOnMobileAll.getJSONObject(i).getString("selectedNumSimStep3"))>0){
                        int addon = Integer.valueOf(addOnMobileAll.getJSONObject(i).getString("selectedNumSimStep3"))-1;
                        addOnMobileAll.getJSONObject(i).put("selectedNumSimStep3",addon);
                    }
                    if(Integer.valueOf(addOnMobileAll.getJSONObject(i).getString("selectedNumSimStep3"))==0){
                        addOnMobileAll.remove(i);
                    }
                }
            }



            for(int i = 0 ; i< offerte.length(); i++){
                selSimRicaricabiliTotale += offerte.getJSONObject(i).getInt("selectedNumSimStep3");
            }
            //JSONObject offerta = offerte.getJSONObject(offertaVisibile - 1);
            for (int k = 0; k < offerte.length(); k++) {
                JSONObject offerta = offerte.getJSONObject(k);
                JSONArray addOnMobileList = offerta.getJSONArray("addOnMobileList");

//            boolean trovato = false;

                for (int i = 0; i < addOnMobileList.length(); i++) {
                    if (addOnMobileList.getJSONObject(i).getString("descFascia").compareToIgnoreCase(addOnMobile.getDescFascia()) == 0) {
//                    trovato = true;
                        if (addOnMobileList.getJSONObject(i).getInt("selectedNumSimStep3") > 0) {
                            selectedSim = addOnMobileList.getJSONObject(i).getInt("selectedNumSimStep3") - 1;
                            addOnMobileList.getJSONObject(i).put("selectedNumSimStep3", selectedSim);
                            Session.getInstance().getJsonPratica().getJSONObject("pratica").put("offerteRaggruppate", new JSONArray());
                        }
                        //WM commenato perche fa macello qunado porti a zero un pacchetto gb
                        //if (addOnMobileList.getJSONObject(i).getInt("selectedNumSimStep3") == 0) {
                        //    addOnMobileList.remove(i);
                        //}
                    }
                }

//            if (!trovato){
//                JSONObject addOnMobileJO = Utils.createAddOnMobile(addOnMobile);
//                addOnMobileJO.put("selectedNumSimStep3", 1);
//                addOnMobileList.put(addOnMobileJO);
//            }

                ArrayList<AddOnMobile> addOnMobileArrayList = new ArrayList<AddOnMobile>();
                for (int i = 0; i < addOnMobileList.length(); i++) {
                    AddOnMobile addOnMobile1 = Utils.createAddOnMobile(addOnMobileList.getJSONObject(i));
                    addOnMobileArrayList.add(addOnMobile1);
                }
                ((AddOnAdapter) addOnRecyclerView.getAdapter()).updateAddOnMobileSelezionati(addOnMobileArrayList);

                checkValidationPacchetto();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return selectedSim;
    }
/*
    public int decrementAddOnMobileNumSim(AddOnMobile addOnMobile) {
        int selectedSim = 0;
        try {
            JSONArray pacchetti = Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONArray("pacchetti");
            JSONObject pacchetto = pacchetti.getJSONObject(pacchettoVisibile - 1);
            JSONArray offerte = pacchetto.getJSONArray("offerte");
            JSONObject offerta = offerte.getJSONObject(offertaVisibile - 1);
            JSONArray addOnMobileList = offerta.getJSONArray("addOnMobileList");

//            boolean trovato = false;
            for (int i = 0; i < addOnMobileList.length(); i++) {
                if (addOnMobileList.getJSONObject(i).getString("descFascia").compareToIgnoreCase(addOnMobile.getDescFascia()) == 0) {
//                    trovato = true;
                    if (addOnMobileList.getJSONObject(i).getInt("selectedNumSimStep3") > 0) {
                        selectedSim = addOnMobileList.getJSONObject(i).getInt("selectedNumSimStep3") - 1;
                        addOnMobileList.getJSONObject(i).put("selectedNumSimStep3", selectedSim);
                        Session.getInstance().getJsonPratica().getJSONObject("pratica").put("offerteRaggruppate", new JSONArray());
                    }
                   //WM commenato perche fa macello qunado porti a zero un pacchetto gb
                    //if (addOnMobileList.getJSONObject(i).getInt("selectedNumSimStep3") == 0) {
                    //    addOnMobileList.remove(i);
                    //}
                }
            }

//            if (!trovato){
//                JSONObject addOnMobileJO = Utils.createAddOnMobile(addOnMobile);
//                addOnMobileJO.put("selectedNumSimStep3", 1);
//                addOnMobileList.put(addOnMobileJO);
//            }

            ArrayList<AddOnMobile> addOnMobileArrayList = new ArrayList<AddOnMobile>();
            for (int i = 0; i < addOnMobileList.length(); i++) {
                AddOnMobile addOnMobile1 = Utils.createAddOnMobile(addOnMobileList.getJSONObject(i));
                addOnMobileArrayList.add(addOnMobile1);
            }
            ((AddOnAdapter) addOnRecyclerView.getAdapter()).updateAddOnMobileSelezionati(addOnMobileArrayList);

            checkValidationPacchetto();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return selectedSim;
    }
*/
    public int incrementMnpNumSim(TerminaleBean terminaleBean) {
        int selectedSim = 1;
        try {
            JSONArray pacchetti = Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONArray("pacchetti");
            JSONObject pacchetto = pacchetti.getJSONObject(pacchettoVisibile - 1);
            JSONArray offerte = pacchetto.getJSONArray("offerte");
            JSONObject offerta = offerte.getJSONObject(offertaVisibile - 1);
            JSONArray terminaleBeanList = offerta.getJSONArray("terminaleBeanList");

            int totTerminali = 0;
            for (int i = 0; i < terminaleBeanList.length(); i++) {
                totTerminali += terminaleBeanList.getJSONObject(i).getInt("selectedNumSimStep3Mnp");
                totTerminali += terminaleBeanList.getJSONObject(i).getInt("selectedNumSimStep3NuovaLinea");
            }

            boolean trovato = false;
            for (int i = 0; i < terminaleBeanList.length(); i++) {
                if (terminaleBeanList.getJSONObject(i).getString("idTerminale").compareToIgnoreCase(terminaleBean.getIdTerminale()) == 0) {
                    trovato = true;
                    if (terminaleBeanList.getJSONObject(i).getInt("selectedNumSimStep3Mnp") < 999 && totTerminali < offerta.getInt("selectedNumSimStep3")) {
                        selectedSim = terminaleBeanList.getJSONObject(i).getInt("selectedNumSimStep3Mnp") + 1;
                        terminaleBeanList.getJSONObject(i).put("selectedNumSimStep3Mnp", selectedSim);
                        Session.getInstance().getJsonPratica().getJSONObject("pratica").put("offerteRaggruppate", new JSONArray());
                    } else {
                        Utils.showWarningMessage(getActivity(), "", "Superato il numero massimo di device selezionabili");
                    }
                }
            }

            if (!trovato && totTerminali < offerta.getInt("selectedNumSimStep3")) {
                JSONObject terminaleBeanJO = Utils.createTerminaleBean(terminaleBean);
                terminaleBeanJO.put("selectedNumSimStep3Mnp", 1);
                terminaleBeanList.put(terminaleBeanJO);
                Session.getInstance().getJsonPratica().getJSONObject("pratica").put("offerteRaggruppate", new JSONArray());
            }

            ArrayList<TerminaleBean> terminaleBeanArrayList = new ArrayList<>();
            for (int i = 0; i < terminaleBeanList.length(); i++) {
                TerminaleBean terminaleBean1 = Utils.createTerminaleBean(terminaleBeanList.getJSONObject(i));
                terminaleBeanArrayList.add(terminaleBean1);
            }
            ((TerminaliAdapter) terminaliRecyclerView.getAdapter()).updateTerminaliSelezionati(terminaleBeanArrayList);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        //store data on sqlLite
        PraticheDAO.updatePratica(this.getActivity().getBaseContext());


        return selectedSim;
    }

    public int decrementMnpNumSim(TerminaleBean terminaleBean) {
        int selectedSim = 0;
        try {
            JSONArray pacchetti = Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONArray("pacchetti");
            JSONObject pacchetto = pacchetti.getJSONObject(pacchettoVisibile - 1);
            JSONArray offerte = pacchetto.getJSONArray("offerte");
            JSONObject offerta = offerte.getJSONObject(offertaVisibile - 1);
            JSONArray terminaleBeanList = offerta.getJSONArray("terminaleBeanList");

//            boolean trovato = false;
            for (int i = 0; i < terminaleBeanList.length(); i++) {
                if (terminaleBeanList.getJSONObject(i).getString("idTerminale").compareToIgnoreCase(terminaleBean.getIdTerminale()) == 0) {
//                    trovato = true;
                    if (terminaleBeanList.getJSONObject(i).getInt("selectedNumSimStep3Mnp") > 0) {
                        selectedSim = terminaleBeanList.getJSONObject(i).getInt("selectedNumSimStep3Mnp") - 1;
                        terminaleBeanList.getJSONObject(i).put("selectedNumSimStep3Mnp", selectedSim);
                        Session.getInstance().getJsonPratica().getJSONObject("pratica").put("offerteRaggruppate", new JSONArray());
                    }
                    if (terminaleBeanList.getJSONObject(i).getInt("selectedNumSimStep3Mnp") + terminaleBeanList.getJSONObject(i).getInt("selectedNumSimStep3NuovaLinea") == 0) {
                        terminaleBeanList.remove(i);
                    }
                }
            }

//            if (!trovato){
//                JSONObject addOnMobileJO = Utils.createAddOnMobile(addOnMobile);
//                addOnMobileJO.put("selectedNumSimStep3", 1);
//                addOnMobileList.put(addOnMobileJO);
//            }

            ArrayList<TerminaleBean> terminaleBeanArrayList = new ArrayList<>();
            for (int i = 0; i < terminaleBeanList.length(); i++) {
                TerminaleBean terminaleBean1 = Utils.createTerminaleBean(terminaleBeanList.getJSONObject(i));
                terminaleBeanArrayList.add(terminaleBean1);
            }
            ((TerminaliAdapter) terminaliRecyclerView.getAdapter()).updateTerminaliSelezionati(terminaleBeanArrayList);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        //store data on sqlLite
        PraticheDAO.updatePratica(this.getActivity().getBaseContext());

        return selectedSim;
    }

    public int incrementNuovaLineaNumSim(TerminaleBean terminaleBean) {
        int selectedSim = 1;
        try {
            JSONArray pacchetti = Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONArray("pacchetti");
            JSONObject pacchetto = pacchetti.getJSONObject(pacchettoVisibile - 1);
            JSONArray offerte = pacchetto.getJSONArray("offerte");
            JSONObject offerta = offerte.getJSONObject(offertaVisibile - 1);
            JSONArray terminaleBeanList = offerta.getJSONArray("terminaleBeanList");

            int totTerminali = 0;
            for (int i = 0; i < terminaleBeanList.length(); i++) {
                totTerminali += terminaleBeanList.getJSONObject(i).getInt("selectedNumSimStep3Mnp");
                totTerminali += terminaleBeanList.getJSONObject(i).getInt("selectedNumSimStep3NuovaLinea");
            }

            boolean trovato = false;
            for (int i = 0; i < terminaleBeanList.length(); i++) {
                if (terminaleBeanList.getJSONObject(i).getString("idTerminale").compareToIgnoreCase(terminaleBean.getIdTerminale()) == 0) {
                    trovato = true;
                    if (terminaleBeanList.getJSONObject(i).getInt("selectedNumSimStep3NuovaLinea") < 999 && totTerminali < offerta.getInt("selectedNumSimStep3")) {
                        selectedSim = terminaleBeanList.getJSONObject(i).getInt("selectedNumSimStep3NuovaLinea") + 1;
                        terminaleBeanList.getJSONObject(i).put("selectedNumSimStep3NuovaLinea", selectedSim);
                        Session.getInstance().getJsonPratica().getJSONObject("pratica").put("offerteRaggruppate", new JSONArray());
                    } else {
                        Utils.showWarningMessage(getActivity(), "", "Superato il numero massimo di device selezionabili");
                    }
                }
            }

            if (!trovato && totTerminali < offerta.getInt("selectedNumSimStep3")) {
                JSONObject terminaleBeanJO = Utils.createTerminaleBean(terminaleBean);
                terminaleBeanJO.put("selectedNumSimStep3NuovaLinea", 1);
                terminaleBeanList.put(terminaleBeanJO);
                Session.getInstance().getJsonPratica().getJSONObject("pratica").put("offerteRaggruppate", new JSONArray());
            }

            ArrayList<TerminaleBean> terminaleBeanArrayList = new ArrayList<>();
            for (int i = 0; i < terminaleBeanList.length(); i++) {
                TerminaleBean terminaleBean1 = Utils.createTerminaleBean(terminaleBeanList.getJSONObject(i));
                terminaleBeanArrayList.add(terminaleBean1);
            }
            ((TerminaliAdapter) terminaliRecyclerView.getAdapter()).updateTerminaliSelezionati(terminaleBeanArrayList);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        //store data on sqlLite
        PraticheDAO.updatePratica(this.getActivity().getBaseContext());


        return selectedSim;
    }

    public int decrementNuovaLineaNumSim(TerminaleBean terminaleBean) {
        int selectedSim = 0;
        try {
            JSONArray pacchetti = Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONArray("pacchetti");
            JSONObject pacchetto = pacchetti.getJSONObject(pacchettoVisibile - 1);
            JSONArray offerte = pacchetto.getJSONArray("offerte");
            JSONObject offerta = offerte.getJSONObject(offertaVisibile - 1);
            JSONArray terminaleBeanList = offerta.getJSONArray("terminaleBeanList");

//            boolean trovato = false;
            for (int i = 0; i < terminaleBeanList.length(); i++) {
                if (terminaleBeanList.getJSONObject(i).getString("idTerminale").compareToIgnoreCase(terminaleBean.getIdTerminale()) == 0) {
//                    trovato = true;
                    if (terminaleBeanList.getJSONObject(i).getInt("selectedNumSimStep3NuovaLinea") > 0) {
                        selectedSim = terminaleBeanList.getJSONObject(i).getInt("selectedNumSimStep3NuovaLinea") - 1;
                        terminaleBeanList.getJSONObject(i).put("selectedNumSimStep3NuovaLinea", selectedSim);
                        Session.getInstance().getJsonPratica().getJSONObject("pratica").put("offerteRaggruppate", new JSONArray());
                    }
                    if (terminaleBeanList.getJSONObject(i).getInt("selectedNumSimStep3Mnp") + terminaleBeanList.getJSONObject(i).getInt("selectedNumSimStep3NuovaLinea") == 0) {
                        terminaleBeanList.remove(i);
                    }
                }
            }

//            if (!trovato){
//                JSONObject addOnMobileJO = Utils.createAddOnMobile(addOnMobile);
//                addOnMobileJO.put("selectedNumSimStep3", 1);
//                addOnMobileList.put(addOnMobileJO);
//            }

            ArrayList<TerminaleBean> terminaleBeanArrayList = new ArrayList<>();
            for (int i = 0; i < terminaleBeanList.length(); i++) {
                TerminaleBean terminaleBean1 = Utils.createTerminaleBean(terminaleBeanList.getJSONObject(i));
                terminaleBeanArrayList.add(terminaleBean1);
            }
            ((TerminaliAdapter) terminaliRecyclerView.getAdapter()).updateTerminaliSelezionati(terminaleBeanArrayList);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        //store data on sqlLite
        PraticheDAO.updatePratica(this.getActivity().getBaseContext());


        return selectedSim;
    }

    public int incrementOpzioneMobileNumSim(OpzioneMobile opzioneMobile) {
        int selectedSim = 1;
        try {
            JSONArray pacchetti = Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONArray("pacchetti");
            JSONObject pacchetto = pacchetti.getJSONObject(pacchettoVisibile - 1);
            JSONArray offerte = pacchetto.getJSONArray("offerte");
            JSONObject offerta = offerte.getJSONObject(offertaVisibile - 1);
            JSONArray opzioniMobileList = offerta.getJSONArray("opzioniMobileList");

            int totOpzioniMobile = 0;
            for (int i = 0; i < opzioniMobileList.length(); i++) {
                totOpzioniMobile += opzioniMobileList.getJSONObject(i).getInt("selectedNumSimStep3");
            }

            boolean trovato = false;
            for (int i = 0; i < opzioniMobileList.length(); i++) {
                if (opzioniMobileList.getJSONObject(i).getString("idOpzione").compareToIgnoreCase(opzioneMobile.getIdOpzione()) == 0) {
                    trovato = true;
                    if (opzioniMobileList.getJSONObject(i).getInt("selectedNumSimStep3") < 999 && opzioniMobileList.getJSONObject(i).getInt("selectedNumSimStep3") < offerta.getInt("selectedNumSimStep3")) {
                        if (TextUtils.isEmpty(opzioniMobileList.getJSONObject(i).getString("scontoConvergenza"))) {
                            selectedSim = opzioniMobileList.getJSONObject(i).getInt("selectedNumSimStep3") + 1;
                            opzioniMobileList.getJSONObject(i).put("selectedNumSimStep3", selectedSim);
                            Session.getInstance().getJsonPratica().getJSONObject("pratica").put("offerteRaggruppate", new JSONArray());
                        } else {
                            if (calculateTotPromoOption() < 10) {
                                selectedSim = opzioniMobileList.getJSONObject(i).getInt("selectedNumSimStep3") + 1;
                                opzioniMobileList.getJSONObject(i).put("selectedNumSimStep3", selectedSim);
                                Session.getInstance().getJsonPratica().getJSONObject("pratica").put("offerteRaggruppate", new JSONArray());
                            } else {
                                Utils.showWarningMessage(getActivity(), "", "Superato il numero massimo di opzioni selezionabili");
                            }
                        }
                    } else {
                        Utils.showWarningMessage(getActivity(), "", "Superato il numero massimo di opzioni selezionabili");
                    }
                }
            }

            if (!trovato) {
                if (TextUtils.isEmpty(opzioneMobile.getScontoConvergenza())) {
                    JSONObject opzioneMobileJO = Utils.createOpzioneMobile(opzioneMobile);
                    opzioneMobileJO.put("selectedNumSimStep3", 1);
                    opzioniMobileList.put(opzioneMobileJO);
                    Session.getInstance().getJsonPratica().getJSONObject("pratica").put("offerteRaggruppate", new JSONArray());
                } else {
                    if (calculateTotPromoOption() < 10) {
                        JSONObject opzioneMobileJO = Utils.createOpzioneMobile(opzioneMobile);
                        opzioneMobileJO.put("selectedNumSimStep3", 1);
                        opzioniMobileList.put(opzioneMobileJO);
                        Session.getInstance().getJsonPratica().getJSONObject("pratica").put("offerteRaggruppate", new JSONArray());
                    }
                }
            }

            ArrayList<OpzioneMobile> opzioniMobileArrayList = new ArrayList<>();
            for (int i = 0; i < opzioniMobileList.length(); i++) {
                OpzioneMobile opzioneMobile1 = Utils.createOpzioneMobile(opzioniMobileList.getJSONObject(i));
                opzioniMobileArrayList.add(opzioneMobile1);
            }
            ((OpzioniMobileAdapter) opzioniRecyclerView.getAdapter()).updateOpzioniMobileSelezionate(opzioniMobileArrayList);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        //store data on sqlLite
        PraticheDAO.updatePratica(this.getActivity().getBaseContext());


        return selectedSim;
    }

    public int decrementOpzioneMobileNumSim(OpzioneMobile opzioneMobile) {
        int selectedSim = 0;
        try {
            JSONArray pacchetti = Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONArray("pacchetti");
            JSONObject pacchetto = pacchetti.getJSONObject(pacchettoVisibile - 1);
            JSONArray offerte = pacchetto.getJSONArray("offerte");
            JSONObject offerta = offerte.getJSONObject(offertaVisibile - 1);
            JSONArray opzioniMobileList = offerta.getJSONArray("opzioniMobileList");

//            boolean trovato = false;
            for (int i = 0; i < opzioniMobileList.length(); i++) {
                if (opzioniMobileList.getJSONObject(i).getString("idOpzione").compareToIgnoreCase(opzioneMobile.getIdOpzione()) == 0) {
//                    trovato = true;
                    if (opzioniMobileList.getJSONObject(i).getInt("selectedNumSimStep3") > 0) {
                        selectedSim = opzioniMobileList.getJSONObject(i).getInt("selectedNumSimStep3") - 1;
                        opzioniMobileList.getJSONObject(i).put("selectedNumSimStep3", selectedSim);
                        Session.getInstance().getJsonPratica().getJSONObject("pratica").put("offerteRaggruppate", new JSONArray());
                    }
                    if (opzioniMobileList.getJSONObject(i).getInt("selectedNumSimStep3") == 0) {
                        opzioniMobileList.remove(i);
                    }
                }
            }

//            if (!trovato){
//                JSONObject addOnMobileJO = Utils.createAddOnMobile(addOnMobile);
//                addOnMobileJO.put("selectedNumSimStep3", 1);
//                addOnMobileList.put(addOnMobileJO);
//            }

            ArrayList<OpzioneMobile> opzioniMobileArrayList = new ArrayList<>();
            for (int i = 0; i < opzioniMobileList.length(); i++) {
                OpzioneMobile opzioneMobile1 = Utils.createOpzioneMobile(opzioniMobileList.getJSONObject(i));
                opzioniMobileArrayList.add(opzioneMobile1);
            }
            ((OpzioniMobileAdapter) opzioniRecyclerView.getAdapter()).updateOpzioniMobileSelezionate(opzioniMobileArrayList);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        //store data on sqlLite
        PraticheDAO.updatePratica(this.getActivity().getBaseContext());


        return selectedSim;
    }

    private int calculateTotPromoOption() {
        int totOpzioniMobilePromo = 0;
        try {
            JSONArray pacchetti = Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONArray("pacchetti");

            for (int i = 0; i < pacchetti.length(); i++) {
                JSONObject pacchetto = pacchetti.getJSONObject(i);
                JSONArray offerte = pacchetto.getJSONArray("offerte");

                for (int j = 0; j < offerte.length(); j++) {
                    JSONObject offerta = offerte.getJSONObject(j);
                    JSONArray opzioniMobileList = offerta.getJSONArray("opzioniMobileList");

                    for (int k = 0; k < opzioniMobileList.length(); k++) {
                        if (!TextUtils.isEmpty(opzioniMobileList.getJSONObject(k).getString("scontoConvergenza"))) {
                            totOpzioniMobilePromo += opzioniMobileList.getJSONObject(k).getInt("selectedNumSimStep3");
                        }
                    }
                }

            }


        } catch (JSONException e) {
            e.printStackTrace();
        }
        return totOpzioniMobilePromo;
    }

    private void changeTickVisibility(int pacchetto, int visibility) {
        switch (pacchetto) {
            case 1:
                pacchetto1Tick.setVisibility(visibility);
                break;
            case 2:
                pacchetto2Tick.setVisibility(visibility);
                break;
            case 3:
                pacchetto3Tick.setVisibility(visibility);
                break;
            case 4:
                pacchetto4Tick.setVisibility(visibility);
                break;
        }
    }

    private void checkInitialValidationPacchetto() {
        int numTotSimRicaricabile=0;
        try {
            JSONObject pratica = (JSONObject) Session.getInstance().getJsonPratica().getJSONObject("pratica");

            JSONArray pacchetti = Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONArray("pacchetti");
            for (int k = 0; k < pacchetti.length(); k++) {


                JSONObject pacchetto = pacchetti.getJSONObject(k);
                JSONArray offerte = pacchetto.getJSONArray("offerte");
                for (int i = 0; i < offerte.length(); i++) {
                    numTotSimRicaricabile += offerte.getJSONObject(i).getInt("selectedNumSimStep3");
                }
                boolean goOn = true;
                for (int j = 0; j < offerte.length(); j++) {
                    JSONObject offerta = offerte.getJSONObject(j);
                    JSONArray addOnMobileList = offerta.getJSONArray("addOnMobileList");

                    int sum = 0;
                    for (int i = 0; i < addOnMobileList.length(); i++) {
                        sum += Integer.valueOf(addOnMobileList.getJSONObject(i).getString("maxSimVendibile")) * addOnMobileList.getJSONObject(i).getInt("selectedNumSimStep3");
                    }

                    int total = 0;
                    for (int i = 0; i < addOnMobileList.length(); i++) {
                        total += addOnMobileList.getJSONObject(i).getInt("selectedNumSimStep3");
                    }

                    if (offerta.getString("offerType").compareToIgnoreCase("Ricaricabile") == 0 && total > numTotSimRicaricabile) {//offerta.getInt("selectedNumSimStep3")) {
                        goOn = false;
                    }

                    if (offerta.getString("offerType").compareToIgnoreCase("Ricaricabile") == 0 && sum < offerta.getInt("selectedNumSimStep3")) {
                        goOn = false;
                    }
                }


                pacchetto.put("completedStep3", goOn);

                checkValidation();
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void checkValidationPacchetto() {
        int numTotSimRicaricabile=0;
        try {
            JSONObject pratica = (JSONObject) Session.getInstance().getJsonPratica().getJSONObject("pratica");
            if(!aperturaStep) {
                pratica.put("offerteConfigurate", new JSONArray());
            }
            aperturaStep=false;
            JSONArray pacchetti = Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONArray("pacchetti");
            JSONObject pacchetto = pacchetti.getJSONObject(pacchettoVisibile - 1);
            JSONArray offerte = pacchetto.getJSONArray("offerte");
            for(int i = 0 ; i< offerte.length(); i++){
                numTotSimRicaricabile += offerte.getJSONObject(i).getInt("selectedNumSimStep3");
            }
            boolean goOn = true;
            for (int j = 0; j < offerte.length(); j++) {
                JSONObject offerta = offerte.getJSONObject(j);
                JSONArray addOnMobileList = offerta.getJSONArray("addOnMobileList");

                int sum = 0;
                for (int i = 0; i < addOnMobileList.length(); i++) {
                    sum += Integer.valueOf(addOnMobileList.getJSONObject(i).getString("maxSimVendibile")) * addOnMobileList.getJSONObject(i).getInt("selectedNumSimStep3");
                }

                int total = 0;
                for (int i = 0; i < addOnMobileList.length(); i++) {
                    total += addOnMobileList.getJSONObject(i).getInt("selectedNumSimStep3");
                }

                if (offerta.getString("offerType").compareToIgnoreCase("Ricaricabile") == 0 && total > numTotSimRicaricabile){//offerta.getInt("selectedNumSimStep3")) {
                    goOn = false;
                }

                if (offerta.getString("offerType").compareToIgnoreCase("Ricaricabile") == 0 && sum < offerta.getInt("selectedNumSimStep3")) {
                    goOn = false;
                }
            }


            pacchetto.put("completedStep3", goOn);

            checkValidation();

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void checkValidation() {
        JSONArray pacchetti = null;
        try {

            //store data on sqlLite
            PraticheDAO.updatePratica(this.getActivity().getBaseContext());

            pacchetti = (JSONArray) Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONArray("pacchetti");
            for (int i = 0; i < pacchetti.length(); i++) {
                changeTickVisibility(i + 1, pacchetti.getJSONObject(i).getBoolean("completedStep3") ? View.VISIBLE : View.GONE);
            }

            switch (pacchetti.length()) {
                case 1:
                    if (pacchetto1Tick.getVisibility() == View.VISIBLE) {
                        continuaButton.setEnabled(true);
                    } else {
                        continuaButton.setEnabled(false);
                    }
                    pacchetto2Tick.setVisibility(View.GONE);
                    pacchetto3Tick.setVisibility(View.GONE);
                    pacchetto4Tick.setVisibility(View.GONE);
                    break;
                case 2:
                    if (pacchetto1Tick.getVisibility() == View.VISIBLE && pacchetto2Tick.getVisibility() == View.VISIBLE) {
                        continuaButton.setEnabled(true);
                    } else {
                        continuaButton.setEnabled(false);
                    }
                    pacchetto3Tick.setVisibility(View.GONE);
                    pacchetto4Tick.setVisibility(View.GONE);
                    break;
                case 3:
                    if (pacchetto1Tick.getVisibility() == View.VISIBLE && pacchetto2Tick.getVisibility() == View.VISIBLE && pacchetto3Tick.getVisibility() == View.VISIBLE) {
                        continuaButton.setEnabled(true);
                    } else {
                        continuaButton.setEnabled(false);
                    }
                    pacchetto4Tick.setVisibility(View.GONE);
                    break;
                case 4:
                    if (pacchetto1Tick.getVisibility() == View.VISIBLE && pacchetto2Tick.getVisibility() == View.VISIBLE && pacchetto3Tick.getVisibility() == View.VISIBLE && pacchetto4Tick.getVisibility() == View.VISIBLE) {
                        continuaButton.setEnabled(true);
                    } else {
                        continuaButton.setEnabled(false);
                    }
                    break;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void aggregateOffers() {
        try {

            JSONArray offerteRaggruppate = (JSONArray) Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONArray("offerteRaggruppate");
            ArrayList<OffertaMobile> offertaMobiles = new ArrayList<>();

            if (offerteRaggruppate.length() <= 0) {


                JSONArray pacchetti = Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONArray("pacchetti");


                for (int i = 0; i < pacchetti.length(); i++) {
                    JSONObject pacchetto = pacchetti.getJSONObject(i);
                    JSONArray offerte = pacchetto.getJSONArray("offerte");

                    for (int j = 0; j < offerte.length(); j++) {

                        boolean offertaTrovata = false;
                        int offertaDaAggiornare = 0;
                        for (int k = 0; k < offertaMobiles.size(); k++) {
                            if (offerte.getJSONObject(j).getInt("idOfferta") == offertaMobiles.get(k).getIdOfferta()) {
                                offertaTrovata = true;
                                offertaDaAggiornare = k;
                                break;
                            }
                        }

                        if (offertaTrovata) {
                            offertaMobiles.get(offertaDaAggiornare).setSelectedNumSimStep3(offertaMobiles.get(offertaDaAggiornare).getSelectedNumSimStep3() + offerte.getJSONObject(j).getInt("selectedNumSimStep3"));

                            ArrayList<AddOnMobile> addOnMobileArrayList = offertaMobiles.get(offertaDaAggiornare).getAddOnMobileArrayList();
                            JSONArray addOnMobileJsonArray = offerte.getJSONObject(j).getJSONArray("addOnMobileList");

                            for (int j2 = 0; j2 < addOnMobileJsonArray.length(); j2++) {

                                boolean addOnMobileTrovato = false;
                                int addOnMobileDaAggiornare = 0;
                                for (int k2 = 0; k2 < addOnMobileArrayList.size(); k2++) {
                                    if (addOnMobileJsonArray.getJSONObject(j2).getString("descFascia").compareToIgnoreCase(addOnMobileArrayList.get(k2).getDescFascia()) == 0) {
                                        addOnMobileTrovato = true;
                                        addOnMobileDaAggiornare = k2;
                                        break;
                                    }
                                }

                                if (addOnMobileTrovato) {
                                    addOnMobileArrayList.get(addOnMobileDaAggiornare).setSelectedNumSimStep3(addOnMobileArrayList.get(addOnMobileDaAggiornare).getSelectedNumSimStep3() + addOnMobileJsonArray.getJSONObject(j2).getInt("selectedNumSimStep3"));
                                } else {
                                    addOnMobileArrayList.add(Utils.createAddOnMobile(addOnMobileJsonArray.getJSONObject(j2)));
                                }
                            }

                            ArrayList<Terminale> terminaleArrayList = offertaMobiles.get(offertaDaAggiornare).getTerminaleArrayList();
                            JSONArray terminaleJsonArray = offerte.getJSONObject(j).getJSONArray("terminaliList");
                            JSONArray terminaleBeanJsonArray = offerte.getJSONObject(j).getJSONArray("terminaleBeanList");

                            for (int j2 = 0; j2 < terminaleJsonArray.length(); j2++) {
                                TerminaleBean terminaleBean = Utils.createTerminaleBean(terminaleBeanJsonArray.getJSONObject(j2));
                                if (terminaleBean.getSelectedNumSimStep3Mnp() > 0) {
                                    Terminale terminale = CatalogoTerminaliDAO.retrieveTerminale(getActivity(), terminaleBean.getIdTerminale(), "true", String.valueOf(offertaMobiles.get(offertaDaAggiornare).getIdOfferta()));
                                    terminale.setSelectedNumSimStep3(terminaleBean.getSelectedNumSimStep3Mnp());
                                    terminaleJsonArray.put(Utils.createTerminale(terminale));
                                }
                                if (terminaleBean.getSelectedNumSimStep3NuovaLinea() > 0) {
                                    Terminale terminale = CatalogoTerminaliDAO.retrieveTerminale(getActivity(), terminaleBean.getIdTerminale(), "false", String.valueOf(offertaMobiles.get(offertaDaAggiornare).getIdOfferta()));
                                    terminale.setSelectedNumSimStep3(terminaleBean.getSelectedNumSimStep3NuovaLinea());
                                    terminaleJsonArray.put(Utils.createTerminale(terminale));
                                }
                            }


                            for (int j2 = 0; j2 < terminaleJsonArray.length(); j2++) {

                                boolean terminaleTrovato = false;
                                int terminaleDaAggiornare = 0;
                                for (int k2 = 0; k2 < terminaleArrayList.size(); k2++) {
                                    if (terminaleJsonArray.getJSONObject(j2).getString("idTerminale").compareToIgnoreCase(terminaleArrayList.get(k2).getIdTerminale()) == 0 && terminaleJsonArray.getJSONObject(j2).getString("mnp").compareToIgnoreCase(terminaleArrayList.get(k2).getMnp()) == 0) {
                                        terminaleTrovato = true;
                                        terminaleDaAggiornare = k2;
                                        break;
                                    }
                                }

                                if (terminaleTrovato) {
                                    terminaleArrayList.get(terminaleDaAggiornare).setSelectedNumSimStep3(terminaleArrayList.get(terminaleDaAggiornare).getSelectedNumSimStep3() + terminaleJsonArray.getJSONObject(j2).getInt("selectedNumSimStep3"));
                                } else {
                                    terminaleArrayList.add(Utils.createTerminale(terminaleJsonArray.getJSONObject(j2)));
                                }
                            }

                            ArrayList<OpzioneMobile> opzioneMobileArrayList = offertaMobiles.get(offertaDaAggiornare).getOpzioneMobileArrayList();
                            JSONArray opzioneMobileJsonArray = offerte.getJSONObject(j).getJSONArray("opzioniMobileList");

                            for (int j2 = 0; j2 < opzioneMobileJsonArray.length(); j2++) {

                                boolean opzioneMobileTrovato = false;
                                int opzioneMobileDaAggiornare = 0;
                                for (int k2 = 0; k2 < opzioneMobileArrayList.size(); k2++) {
                                    if (opzioneMobileJsonArray.getJSONObject(j2).getString("idOpzione").compareToIgnoreCase(opzioneMobileArrayList.get(k2).getIdOpzione()) == 0) {
                                        opzioneMobileTrovato = true;
                                        opzioneMobileDaAggiornare = k2;
                                        break;
                                    }
                                }

                                if (opzioneMobileTrovato) {
                                    opzioneMobileArrayList.get(opzioneMobileDaAggiornare).setSelectedNumSimStep3(opzioneMobileArrayList.get(opzioneMobileDaAggiornare).getSelectedNumSimStep3() + opzioneMobileJsonArray.getJSONObject(j2).getInt("selectedNumSimStep3"));
                                } else {
                                    opzioneMobileArrayList.add(Utils.createOpzioneMobile(opzioneMobileJsonArray.getJSONObject(j2)));
                                }
                            }

                        } else {
                            OffertaMobile offertaMobile = Utils.createOffertaMobile(offerte.getJSONObject(j));
//                        offertaMobiles.add(Utils.createOffertaMobile(offerte.getJSONObject(j)));

                            ArrayList<Terminale> terminaleArrayList = offertaMobile.getTerminaleArrayList();
                            JSONArray terminaleJsonArray = offerte.getJSONObject(j).getJSONArray("terminaliList");
                            JSONArray terminaleBeanJsonArray = offerte.getJSONObject(j).getJSONArray("terminaleBeanList");

                            for (int j2 = 0; j2 < terminaleBeanJsonArray.length(); j2++) {
                                TerminaleBean terminaleBean = Utils.createTerminaleBean(terminaleBeanJsonArray.getJSONObject(j2));
                                if (terminaleBean.getSelectedNumSimStep3Mnp() > 0) {
                                    Terminale terminale = CatalogoTerminaliDAO.retrieveTerminale(getActivity(), terminaleBean.getIdTerminale(), "true", String.valueOf(offertaMobile.getIdOfferta()));
                                    terminale.setSelectedNumSimStep3(terminaleBean.getSelectedNumSimStep3Mnp());
                                    terminaleArrayList.add(terminale);
                                }
                                if (terminaleBean.getSelectedNumSimStep3NuovaLinea() > 0) {
                                    Terminale terminale = CatalogoTerminaliDAO.retrieveTerminale(getActivity(), terminaleBean.getIdTerminale(), "false", String.valueOf(offertaMobile.getIdOfferta()));
                                    terminale.setSelectedNumSimStep3(terminaleBean.getSelectedNumSimStep3NuovaLinea());
                                    terminaleArrayList.add(terminale);
                                }
                            }

                            offertaMobiles.add(offertaMobile);
                        }
                    }
                }

                for (OffertaMobile offertaMobile : offertaMobiles) {
                    ArrayList<PianoTariffario> pianoTariffarioArrayList = CatalogoPianiTariffariDAO.retrievePianiTariffari(getActivity(), String.valueOf(offertaMobile.getIdOfferta()));
                    for (PianoTariffario pianoTariffario : pianoTariffarioArrayList) {
                        offertaMobile.setPercentualeScontoPenale(pianoTariffario.getPercentualeScontoPenale());
                        offertaMobile.getPercentualeScontoArrayList().add(new PercentualeSconto(pianoTariffario.getPercentualeSconto()));
                    }
                    offertaMobile.setSelectedNumSimStep4(offertaMobile.getSelectedNumSimStep3());
                    if (!offertaMobile.getPercentualeScontoArrayList().isEmpty()) {
                        offertaMobile.setScontoApplicato(offertaMobile.getPercentualeScontoArrayList().get(0).getPercentualeSconto());
                    } else {
                        offertaMobile.setScontoApplicato("0");
                    }
                    if (!TextUtils.isEmpty(offertaMobile.getCanonePromo())) {
                        offertaMobile.setCanoneScontato(String.valueOf(Float.parseFloat(offertaMobile.getCanonePromo().replace(",", ".")) - Float.parseFloat(offertaMobile.getScontoApplicato().replace(",", ".")) / 100 * Float.parseFloat(offertaMobile.getCanonePromo().replace(",", "."))));
                    } else {
                        offertaMobile.setCanoneScontato(String.valueOf(Float.parseFloat(offertaMobile.getCanoneMensile().replace(",", ".")) - Float.parseFloat(offertaMobile.getScontoApplicato().replace(",", ".")) / 100 * Float.parseFloat(offertaMobile.getCanoneMensile().replace(",", "."))));
                    }
                }

            } else {
                for (int i = 0; i < offerteRaggruppate.length(); i++) {
                    offertaMobiles.add(Utils.createOffertaMobile(offerteRaggruppate.getJSONObject(i)));
                }
            }

            JSONArray offerteRaggruppateJA = new JSONArray();
            for (OffertaMobile offertaMobile : offertaMobiles) {
                offerteRaggruppateJA.put(Utils.createOffertaMobile(offertaMobile));
            }

            Session.getInstance().getJsonPratica().getJSONObject("pratica").put("offerteRaggruppate", offerteRaggruppateJA);

            //store data on sqlLite
            PraticheDAO.updatePratica(this.getActivity().getBaseContext());

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}
