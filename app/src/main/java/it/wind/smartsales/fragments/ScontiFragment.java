package it.wind.smartsales.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.json.JSONException;

import java.util.ArrayList;

import it.wind.smartsales.R;
import it.wind.smartsales.adapter.TariffPlanAdapter;
import it.wind.smartsales.commons.Constants;
import it.wind.smartsales.commons.Log;
import it.wind.smartsales.entities.PercentageDiscount;
import it.wind.smartsales.entities.Session;
import it.wind.smartsales.entities.TariffPlan;
import it.wind.smartsales.task.CatalogoOfferteFissoTask;
import it.wind.smartsales.task.CatalogoScontiTask;
import it.wind.smartsales.task.requests.DownloadCatalogoRequest;
import it.wind.smartsales.task.requests.Request;

/**
 * Created by luca.quaranta on 10/02/2016.
 */
public class ScontiFragment extends Fragment implements Handler.Callback{

    private RecyclerView tariffPlanRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private RecyclerView.Adapter tariffPlanAdapter;
    private TextView fattibilita, fattibilita2, totSim, spesaPromo, spesaSconti;

    private void initializeComponents(View view){
        tariffPlanRecyclerView = (RecyclerView) view.findViewById(R.id.tariff_plan_recycler_view);
        fattibilita = (TextView) view.findViewById(R.id.fattibilita);
        fattibilita2 = (TextView) view.findViewById(R.id.fattibilita2);
        totSim = (TextView) view.findViewById(R.id.tot_sim);
        spesaPromo = (TextView) view.findViewById(R.id.spesa_promo);
        spesaSconti = (TextView) view.findViewById(R.id.spesa_sconti);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_sconti, container, false);
        initializeComponents(view);

        callCatalogoOfferteFissoTask();

        return view;
    }

    private void callDownloadCatalogoScontiTask(){
        try {
            DownloadCatalogoRequest downloadCatalogoRequest = new DownloadCatalogoRequest();
            downloadCatalogoRequest.setCatalogVersion("1.0");
            downloadCatalogoRequest.setCatalogIdentifier(String.valueOf(Constants.CatalogIdentifiers.CATALOGO_SCONTI));
            downloadCatalogoRequest.setCatalogLastUpdate("20160310");
            CatalogoScontiTask.callTask(getActivity(), this, new Request(downloadCatalogoRequest));
        } catch (JSONException e){
            e.printStackTrace();
        }
    }

    private void callCatalogoOfferteFissoTask(){
        try {
            DownloadCatalogoRequest downloadCatalogoRequest = new DownloadCatalogoRequest();
            downloadCatalogoRequest.setCatalogVersion("1.0");
            downloadCatalogoRequest.setCatalogIdentifier(String.valueOf(Constants.CatalogIdentifiers.CATALOGO_OFFERTE_FISSO));
            downloadCatalogoRequest.setCatalogLastUpdate("20160310");
            CatalogoOfferteFissoTask.callTask(getActivity(), this, new Request(downloadCatalogoRequest));
        } catch (JSONException e){
            e.printStackTrace();
        }
    }

    private void populateArticles(ArrayList<TariffPlan> tariffPlans) {
        tariffPlanRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getActivity());
        tariffPlanRecyclerView.setLayoutManager(mLayoutManager);
        tariffPlanRecyclerView.setItemAnimator(new DefaultItemAnimator());
        tariffPlanAdapter = new TariffPlanAdapter(this, tariffPlans);
        tariffPlanRecyclerView.setAdapter(tariffPlanAdapter);
    }

    public void checkFattibilita(){
        int checkTotSim = checkTotSim();
        int checkNumSim = checkNumSim(checkTotSim);

        totSim.setText(String.valueOf(checkTotSim));

        float spesapromo = 0;
        float spesasconti = 0;
        for (TariffPlan tariffPlan : Session.getInstance().getDiscountsParameters().getTariffPlans()){
            if (tariffPlan.getSelectedNumSim() > 0){
//                spesapromo += tariffPlan.getPromoFee();
//                spesasconti += tariffPlan.getPromoFee() - (tariffPlan.getGrossFee() * tariffPlan.getSelectedDiscountPercentage() / 100);
            }
        }
        spesaPromo.setText(String.format("%.2f", spesapromo));
        spesaSconti.setText(String.format("%.2f", spesasconti));

        if (checkTotSim == 0){
            fattibilita.setText("");
            fattibilita2.setVisibility(View.INVISIBLE);
        } else if (checkNumSim < 0){
            fattibilita.setText("Superato numero max SIM complessivo");
            fattibilita2.setText("Superato numero max SIM complessivo");
            fattibilita2.setVisibility(View.VISIBLE);
        } else if (checkNumSim == 0){
            fattibilita.setText("Sottosoglia");
            fattibilita2.setText("Sottosoglia");
            fattibilita2.setVisibility(View.VISIBLE);
        } else {
            boolean isKOChecked = false;
            for (TariffPlan tariffPlan : Session.getInstance().getDiscountsParameters().getTariffPlans()){
                if (tariffPlan.getSelectedNumSim() > 0 && tariffPlan.getSelectedPreAutorizzazione().compareTo(Constants.TariffPlanPreAutirization.OK) != 0){
                    isKOChecked = true;
                }
            }
            if (isKOChecked){
                fattibilita.setText("KO");
                fattibilita2.setVisibility(View.INVISIBLE);
            } else {
                fattibilita.setText("OK");
                fattibilita2.setVisibility(View.INVISIBLE);
            }
        }
    }

    public String checkAutorization(int position) {
        int checkTotSim = checkTotSim();
        int checkNumSim = checkNumSim(checkTotSim);
        float checkValoreSpesaMensile = checkValoreSpesaMensile();
        int checkValoreSpesa = checkValoreSpesa(checkValoreSpesaMensile);
        int checkFascia = checkFascia(checkValoreSpesa, checkNumSim);
        Log.d("CHANGE TARIFF PLAN", "[" + "checkTotSim: " + checkTotSim + ", checkNumSim: " + checkNumSim + ", checkValoreSpesaMensile: " + checkValoreSpesaMensile + ", checkValoreSpesa: " + checkValoreSpesa + ", checkFascia: " + checkFascia + "]");

        TariffPlan tariffPlan = Session.getInstance().getDiscountsParameters().getTariffPlans().get(position);
        if (tariffPlan.getSelectedNumSim() == 0) {
            return Constants.TariffPlanPreAutirization.NULL;
        } else {
            if (checkC17(tariffPlan, checkValoreSpesa, checkFascia) == 0){
                return Constants.TariffPlanPreAutirization.OVER_MAX;
            } else {
                return Constants.TariffPlanPreAutirization.OK;
            }

        }
    }

    private int checkTotSim() {
        int sum = 0;
        for (TariffPlan tariffPlan : Session.getInstance().getDiscountsParameters().getTariffPlans()) {
            sum += tariffPlan.getSelectedNumSim();
        }
        return sum;
    }

    private int checkNumSim(int totSelectedSim) {
        if (totSelectedSim > Session.getInstance().getDiscountsParameters().getSimCategory3()) {
            return -1;
        } else if (totSelectedSim > Session.getInstance().getDiscountsParameters().getSimCategory2()) {
            return 2;
        } else if (totSelectedSim > Session.getInstance().getDiscountsParameters().getSimCategory1()) {
            return 1;
        } else {
            return 0;
        }
    }

    private float checkValoreSpesaMensile() {
        int valoreSpesaMensile = 0;
        for (TariffPlan tariffPlan : Session.getInstance().getDiscountsParameters().getTariffPlans()) {
//            valoreSpesaMensile += tariffPlan.getPromoFee() * tariffPlan.getSelectedNumSim();
        }
        return valoreSpesaMensile;
    }

    private int checkValoreSpesa(float valoreSpesaMensile) {
        if (valoreSpesaMensile >= Session.getInstance().getDiscountsParameters().getExpenseCategory2()) {
            return 2;
        } else if (valoreSpesaMensile >= Session.getInstance().getDiscountsParameters().getExpenseCategory1()) {
            return 1;
        } else {
            return 0;
        }
    }

    private int checkFascia(int checkValoreSpesa, int checkNumSim) {
        if (checkValoreSpesa < checkNumSim) {
            return checkValoreSpesa;
        } else {
            return checkNumSim;
        }
    }

    private int checkC17(TariffPlan tariffPlan, int checkValoreSpesa, int checkFascia) {
        if (tariffPlan.getSelectedDiscountPercentage() == 0){
            return 1;
        } else {
            if (checkValoreSpesa <= 0){
                return 0;
            } else {
//                if (!tariffPlan.isRepaymentPenalties()){
                    return checkB37(tariffPlan, checkFascia);
//                } else {
//                    return checkK37(tariffPlan);
//                }
            }
        }
    }

    private int checkB37(TariffPlan tariffPlan, int checkFascia) {
        if (tariffPlan.getSelectedDiscountPercentage() <= checkB36(tariffPlan, checkFascia)){
            return 1;
        } else {
            return 0;
        }
    }

    private float checkB36(TariffPlan tariffPlan, int checkFascia) {
        if (checkFascia == 2){
            float max = -1f;
            for (PercentageDiscount percentageDiscount : tariffPlan.getPercentageDiscounts()) {
                if (percentageDiscount.getDiscount() > max) {
                    max = percentageDiscount.getDiscount();
                }
            }
            return max;
        } else {
            float min = 1000000f;
            for (PercentageDiscount percentageDiscount : tariffPlan.getPercentageDiscounts()) {
                if (percentageDiscount.getDiscount() != 0 && percentageDiscount.getDiscount() < min) {
                    min = percentageDiscount.getDiscount();
                }
            }
            return min;
        }
    }

    @Override
    public boolean handleMessage(Message msg) {
        switch (msg.what){
            case Constants.HandlerWhat.WHAT_CATALOGO_SCONTI:
                populateArticles(Session.getInstance().getDiscountsParameters().getTariffPlans());
                break;

            case Constants.HandlerWhat.WHAT_CATALOGO_SCONTI_KO:
                break;
        }
        return false;
    }
}
