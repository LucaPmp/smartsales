package it.wind.smartsales.fragments.wireline;


import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import it.wind.smartsales.R;
import it.wind.smartsales.commons.Log;

/**
 * A simple {@link Fragment} subclass.
 */
public class WirelineInternetOfferFragment extends Fragment implements WirelineInnerFragment.ContinueButtonInteraction {


    private int actualStep;
    private View addButton;


    private class Sede {
        int position;
        View root;
        View label;
        View tick;
        View delete;

        public Sede(int pos, View root, View label, View tick, View delete) {
            this.position = pos;
            this.root = root;
            this.label = label;
            this.tick = tick;
            this.delete = delete;
        }
    }

    private View continueButton;
    private Map<Sede, List<WirelineInnerFragment>> sedi;

    public WirelineInternetOfferFragment() {
        sedi = new LinkedHashMap<>(4);
        actualStep = 1;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return LayoutInflater.from(getActivity()).inflate(R.layout.fragment_create_offer_wireline_internet, container, false);
    }

    private Sede getSedeById(int pos) {
        return (new ArrayList<>(sedi.keySet())).get(pos - 1);
    }

    private void setSelected(Sede s) {
        for (Map.Entry<Sede, List<WirelineInnerFragment>> entry : sedi.entrySet()) {
            Sede sede = entry.getKey();
            List<WirelineInnerFragment> fragments = entry.getValue();
            sede.label.setSelected(sede == s);
            sede.root.setSelected(sede == s);
            sede.tick.setSelected(sede == s);
            if (sede == s) {
                final FragmentManager childFragmentManager = getChildFragmentManager();
                WirelineInnerFragment wirelineInnerFragment = null;
                if (fragments.size() > actualStep - 1) {
                    wirelineInnerFragment = fragments.get(actualStep - 1);
                }
                if (wirelineInnerFragment == null) {
                    WirelineInnerFragment previous = null;
                    try {
                        previous = sedi.get(getSedeById(sede.position)).get(actualStep - 2);
                    } catch (Exception e) {
                        Log.e("WIRELINE", e.getLocalizedMessage());
                    }
                    wirelineInnerFragment = getFragmentForActualPosition(previous);
                    fragments.add(actualStep - 1, wirelineInnerFragment);
                }

                childFragmentManager.beginTransaction().replace(R.id.fragment_container, wirelineInnerFragment).addToBackStack(null).commit();

            }
        }

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        continueButton = view.findViewById(R.id.continua_button);
        continueButton.setOnClickListener(new ContinueOnClickListener());
//        nextFragment();


        for (int i = 1; i < 5; i++) {
            final int identifier = getResources().getIdentifier("sede_" + i, "id", getActivity().getPackageName());
            final int labelIdentifier = getResources().getIdentifier("sede_" + i + "_text", "id", getActivity().getPackageName());
            final int tickIdentifier = getResources().getIdentifier("sede_" + i + "_tick", "id", getActivity().getPackageName());
            final int deleteIdentifier = getResources().getIdentifier("delete_sede_" + i, "id", getActivity().getPackageName());
            View root = view.findViewById(identifier);
            View label = view.findViewById(labelIdentifier);
            View tick = view.findViewById(tickIdentifier);
            View delete = view.findViewById(deleteIdentifier);
            final Sede sede = new Sede(i, root, label, tick, delete);
            sede.root.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    setSelected(sede);
                }
            });

            delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                }
            });

            sedi.put(sede, new ArrayList<WirelineInnerFragment>());
        }

        addButton = view.findViewById(R.id.plus_button);
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                for (Sede sede : sedi.keySet()) {
                    if (sede.root.getVisibility() != View.VISIBLE) {
                        sede.root.setVisibility(View.VISIBLE);
                        setSelected(sede);
                        break;
                    }
                }
                checkContinueForAllLocation();
            }
        });

        setSelected(getSedeById(1));
        setEnabled(false);


    }

    private void nextFragment() {
        final FragmentManager childFragmentManager = getChildFragmentManager();
        WirelineInnerFragment inner = (WirelineInnerFragment) childFragmentManager.findFragmentById(R.id.fragment_container);
        actualStep = inner != null ? (inner.getPosition() + 1) : 1;
        addButton.setVisibility(View.GONE);
        setSelected(getActualLocation());
//        childFragmentManager.beginTransaction().replace(R.id.fragment_container, getFragmentForActualPosition()).addToBackStack(null).commit();
        resetButtons();
    }

    private WirelineInnerFragment getFragmentForActualPosition(WirelineInnerFragment inner) {
        switch (actualStep) {
            case 1: {
                return new WirelineInternetOfferStep1Fragment();
            }
            case 2: {
                return new WirelineInternetOfferStep2Fragment();
            }
            case 3: {
                return WirelineInternetOfferStep3Fragment.newInstance(inner.getOffer());
            }
            case 4: {
                return WirelineInternetOfferStep4Fragment.newInstance(inner.getOffer(), inner.getTerminals(), inner.getOptions());
            }
        }
        return null;
    }

    @Override
    public void setEnabled(boolean value) {

        for (Sede sede : sedi.keySet()) {
            if (sede.root.isSelected()) {
                sede.tick.setVisibility(value ? View.VISIBLE : View.GONE);
                break;
            }
        }
        checkContinueForAllLocation();

    }


    public Sede getActualLocation() {
        for (Sede sede : sedi.keySet())
            if (sede.root.isSelected())
                return sede;
        return null;
    }

    private void resetButtons() {
        for (Sede sede : sedi.keySet()) {
            sede.tick.setVisibility(View.GONE);
        }
        continueButton.setSelected(false);
        continueButton.setEnabled(false);

    }

    private boolean checkContinueForAllLocation() {
        boolean shouldEnable = true;
        for (Sede sede : sedi.keySet()) {
            if (sede.root.getVisibility() == View.VISIBLE) {
                if (sede.tick.getVisibility() != View.VISIBLE) {
                    shouldEnable = false;
                    break;
                }
            }
        }
        continueButton.setSelected(shouldEnable);
        continueButton.setEnabled(shouldEnable);
        return shouldEnable;
    }


    public class ContinueOnClickListener implements View.OnClickListener {
        private long lastClick = Long.MIN_VALUE;

        public ContinueOnClickListener() {
        }

        @Override
        public void onClick(View view) {
            final long now = System.currentTimeMillis();
            if (lastClick + 3000 < now) {
                lastClick = now;
                nextFragment();
            }
        }
    }


}
