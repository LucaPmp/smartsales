package it.wind.smartsales.fragments;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import it.wind.smartsales.R;
import it.wind.smartsales.adapter.GbAdapter;
import it.wind.smartsales.customviews.HorizontalSpaceItemDecoration;
import it.wind.smartsales.entities.AddOnMobile;
import it.wind.smartsales.entities.AddOnPackage;
import it.wind.smartsales.entities.OffertaMobile;

/**
 * Created by luca.quaranta on 03/12/2015.
 */
public class GbDialogFragment extends DialogFragment {

    public static final String KEY = "MESSAGE_KEY";

    private OffertaMobile offertaMobile;

    private RecyclerView gbRecyclerView;
    private RecyclerView.LayoutManager mLayoutManagerGb;
    private RecyclerView.Adapter gbAdapter;

    private Button indietroButton, continuaButton;
    private MobileInternetCreateOfferStep8Fragment mobileInternetCreateOfferStep8Fragment;
    private TextView newICCID, offerDescription, offerDescriptionUpper, totalePacchettiAcquistati;
    private LinearLayout layoutPacchetti, packageDetailLayout;
    private View.OnClickListener gbClickListener;

    private Button buttonPlus, gb1, gb5, gb15, gb30, gb100;

    public GbDialogFragment() {
    }

    @SuppressLint("ValidFragment")
    public GbDialogFragment(MobileInternetCreateOfferStep8Fragment mobileInternetCreateOfferStep8Fragment, OffertaMobile offertaMobile) {
        this.mobileInternetCreateOfferStep8Fragment = mobileInternetCreateOfferStep8Fragment;
        this.offertaMobile = offertaMobile;
    }

    private void initializeComponents(View view) {
        indietroButton = (Button) view.findViewById(R.id.indietro_button);
        continuaButton = (Button) view.findViewById(R.id.continua_button);
        buttonPlus = (Button) view.findViewById(R.id.button_plus);

        gbRecyclerView = (RecyclerView) view.findViewById(R.id.gb_recycler_view);

        layoutPacchetti = (LinearLayout) view.findViewById(R.id.layout_pacchetti);
        packageDetailLayout = (LinearLayout) view.findViewById(R.id.package_detail_layout);

        offerDescription = (TextView) view.findViewById(R.id.offer_description);
        offerDescriptionUpper = (TextView) view.findViewById(R.id.offer_description_upper);
        newICCID = (TextView) view.findViewById(R.id.new_iccid);

        gb1 = (Button) view.findViewById(R.id.gb_1);
        gb5 = (Button) view.findViewById(R.id.gb_5);
        gb15 = (Button) view.findViewById(R.id.gb_15);
        gb30 = (Button) view.findViewById(R.id.gb_30);
        gb100 = (Button) view.findViewById(R.id.gb_100);

        totalePacchettiAcquistati = (TextView) view.findViewById(R.id.totali_pacchetti_acquistati);

        gbClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                layoutPacchetti.setVisibility(View.GONE);
                buttonPlus.setBackground(getActivity().getResources().getDrawable(R.drawable.gb_selector));
                buttonPlus.setTextColor(getActivity().getResources().getColor(android.R.color.white));
                buttonPlus.setText(((Button) v).getText());
                buttonPlus.setSelected(true);

                packageDetailLayout.setVisibility(View.VISIBLE);
                populateGbList(((Button) v).getText().toString());
            }
        };

        initGbList();
    }

    private void initGbList() {
        gbRecyclerView.setHasFixedSize(true);
        mLayoutManagerGb = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        gbRecyclerView.setLayoutManager(mLayoutManagerGb);
        gbRecyclerView.setItemAnimator(new DefaultItemAnimator());
        gbRecyclerView.addItemDecoration(new HorizontalSpaceItemDecoration(20));
    }

    private void populateGbList(String text) {

        AddOnPackage addOnPackage = null;
        for (AddOnPackage addOnPackage1 : offertaMobile.getAddOnPackageArrayListStep8()) {
            if (addOnPackage1.getDesc().compareToIgnoreCase(text.replace("\n", "")) == 0) {
                addOnPackage = addOnPackage1;
            }
        }

        gbAdapter = new GbAdapter(this, offertaMobile, addOnPackage.getAddOnMobileArrayList());
        gbRecyclerView.setAdapter(gbAdapter);
        totalePacchettiAcquistati.setText(String.valueOf(addOnPackage.getAddOnMobileArrayList().size()));
    }

    public void updateAddOnList(){
        mobileInternetCreateOfferStep8Fragment.updateAddOnList(offertaMobile);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    @Override
    public void onResume() {
        super.onResume();
        int width = getResources().getDimensionPixelSize(R.dimen.fragment_dialog_mnp_width);
        getDialog().getWindow().setLayout(width, ViewGroup.LayoutParams.WRAP_CONTENT);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_dialog_gb, container, false);

        initializeComponents(view);

        populateFields();

        buttonPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                layoutPacchetti.setVisibility(View.VISIBLE);
            }
        });

        gb1.setOnClickListener(gbClickListener);
        gb5.setOnClickListener(gbClickListener);
        gb15.setOnClickListener(gbClickListener);
        gb30.setOnClickListener(gbClickListener);
        gb100.setOnClickListener(gbClickListener);

        indietroButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        continuaButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mobileInternetCreateOfferStep8Fragment.updateOffer();
                dismiss();
            }
        });


        return view;
    }

    private void populateFields() {
        newICCID.setText(offertaMobile.getNewICCID());
        offerDescription.setText(offertaMobile.getOfferDescription());
        offerDescriptionUpper.setText(offertaMobile.getOfferDescriptionUpper());

        for (AddOnMobile addOnMobile : mobileInternetCreateOfferStep8Fragment.allAddOnSelecedStep3){//offertaMobile.getAddOnMobileArrayList()) {
            switch (addOnMobile.getDescFascia()) {
                case "1GB":
                    gb1.setEnabled(true);
                    break;
                case "5GB":
                    gb5.setEnabled(true);
                    break;
                case "15GB":
                    gb15.setEnabled(true);
                    break;
                case "30GB":
                    gb30.setEnabled(true);
                    break;
                case "100GB":
                    gb100.setEnabled(true);
                    break;
            }
        }

        if (offertaMobile.getAddOnMobileStep8() != null){
            buttonPlus.setBackground(getActivity().getResources().getDrawable(R.drawable.gb_selector));
            buttonPlus.setTextColor(getActivity().getResources().getColor(android.R.color.white));
            buttonPlus.setText(offertaMobile.getAddOnMobileStep8().getDescFascia().replace("GB", "\nGB"));
            buttonPlus.setSelected(true);

            packageDetailLayout.setVisibility(View.VISIBLE);
            populateGbList(offertaMobile.getAddOnMobileStep8().getDescFascia());
        }
    }

}
