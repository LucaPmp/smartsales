package it.wind.smartsales.fragments;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import it.wind.smartsales.R;
import it.wind.smartsales.commons.Utils;
import it.wind.smartsales.customviews.AspectRatioView;
import it.wind.smartsales.entities.OffertaMobile;

/**
 * Created by luca.quaranta on 03/12/2015.
 */
public class MnpDialogFragment extends DialogFragment {

    public static final String KEY = "MESSAGE_KEY";

    private OffertaMobile offertaMobile;
    private String selectedMnp;

    private Button indietroButton, continuaButton;
    private MobileInternetCreateOfferStep8Fragment mobileInternetCreateOfferStep8Fragment;
    private AspectRatioView buttonPlus, buttonTim, buttonVodafone, buttonTre, buttonAltro, buttonClose, ocrButton, altroOperatoreClose;
    private LinearLayout layoutOperatori, layoutMnp, altroOperatoreLayout;
    private TextView newICCID, offerDescription, offerDescriptionUpper;
    private EditText oldICCID, oldMSISDN, altroOperatore;

    public MnpDialogFragment() {
    }

    @SuppressLint("ValidFragment")
    public MnpDialogFragment(MobileInternetCreateOfferStep8Fragment mobileInternetCreateOfferStep8Fragment, OffertaMobile offertaMobile) {
        this.mobileInternetCreateOfferStep8Fragment = mobileInternetCreateOfferStep8Fragment;
        this.offertaMobile = offertaMobile;
    }

    private void initializeComponents(View view) {
        indietroButton = (Button) view.findViewById(R.id.indietro_button);
        continuaButton = (Button) view.findViewById(R.id.continua_button);

        offerDescription = (TextView) view.findViewById(R.id.offer_description);
        offerDescriptionUpper = (TextView) view.findViewById(R.id.offer_description_upper);

        layoutMnp = (LinearLayout) view.findViewById(R.id.layout_mnp);
        altroOperatoreLayout = (LinearLayout) view.findViewById(R.id.altro_operatore_layout);
        layoutOperatori = (LinearLayout) view.findViewById(R.id.layout_operatori);
        buttonPlus = (AspectRatioView) view.findViewById(R.id.button_plus);
        buttonTim = (AspectRatioView) view.findViewById(R.id.button_tim);
        buttonVodafone = (AspectRatioView) view.findViewById(R.id.button_vodafone);
        buttonTre = (AspectRatioView) view.findViewById(R.id.button_tre);
        buttonAltro = (AspectRatioView) view.findViewById(R.id.button_altro);
        buttonClose = (AspectRatioView) view.findViewById(R.id.button_close);

        newICCID = (TextView) view.findViewById(R.id.new_iccid);
        oldICCID = (EditText) view.findViewById(R.id.old_iccid);
        oldMSISDN = (EditText) view.findViewById(R.id.old_msisdn);
        altroOperatore = (EditText) view.findViewById(R.id.altro_operatore);
        altroOperatoreClose = (AspectRatioView) view.findViewById(R.id.altro_operatore_close);

        ocrButton = (AspectRatioView) view.findViewById(R.id.ocr_button);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    @Override
    public void onResume() {
        super.onResume();
        int width = getResources().getDimensionPixelSize(R.dimen.fragment_dialog_mnp_width);
        getDialog().getWindow().setLayout(width, ViewGroup.LayoutParams.WRAP_CONTENT);
    }

    public void scanBarcode() {
        IntentIntegrator.forFragment(this).initiateScan();

        IntentIntegrator integrator = IntentIntegrator.forFragment(this);
        integrator.setDesiredBarcodeFormats(IntentIntegrator.ALL_CODE_TYPES);
        integrator.setPrompt("Scan a barcode");
        integrator.setCameraId(0);  // Use a specific camera of the device
        integrator.setBeepEnabled(false);
        integrator.initiateScan();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_dialog_mnp, container, false);

        initializeComponents(view);

        populateFields();

        buttonPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (layoutOperatori.getVisibility() == View.GONE) {
                    layoutOperatori.setVisibility(View.VISIBLE);
                }
            }
        });

        buttonClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                layoutOperatori.setVisibility(View.GONE);
            }
        });

        buttonTim.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonPlus.setBackground(v.getBackground());
                layoutOperatori.setVisibility(View.GONE);
                selectedMnp = "Tim";
//                TODO: Salvare la selezione
            }
        });

        buttonVodafone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonPlus.setBackground(v.getBackground());
                layoutOperatori.setVisibility(View.GONE);
                selectedMnp = "Vodafone";
//                TODO: Salvare la selezione
            }
        });

        buttonTre.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonPlus.setBackground(v.getBackground());
                layoutOperatori.setVisibility(View.GONE);
                selectedMnp = "Tre";
//                TODO: Salvare la selezione
            }
        });

        buttonAltro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonPlus.setBackground(v.getBackground());
                layoutMnp.setVisibility(View.GONE);
                altroOperatoreLayout.setVisibility(View.VISIBLE);
                selectedMnp = "";
//                TODO: Salvare la selezione
            }
        });

        altroOperatoreClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonPlus.setBackground(v.getContext().getResources().getDrawable(R.drawable.icon_plus));
                layoutMnp.setVisibility(View.VISIBLE);
                altroOperatoreLayout.setVisibility(View.GONE);
                selectedMnp = "";
            }
        });

        indietroButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        continuaButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                offertaMobile.setMnpStep8(selectedMnp);
                offertaMobile.setOldMSISDN(oldMSISDN.getText().toString().trim());
                offertaMobile.setOldICCID(oldICCID.getText().toString().trim());
                mobileInternetCreateOfferStep8Fragment.updateOffer();
                dismiss();
            }
        });

        oldMSISDN.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    if (oldMSISDN.getText().toString().trim().length() < 10) {
                        Utils.showWarningMessage(getActivity(), "", "Il numero di telefono deve essere composto da almeno 10 cifre");
                    }
                }
            }
        });

        ocrButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                scanBarcode();
            }
        });

        altroOperatore.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                selectedMnp = altroOperatore.getText().toString().trim();

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        return view;
    }

    private void populateFields() {
        selectedMnp = offertaMobile.getMnpStep8();
        switch (selectedMnp) {
            case "":
                break;
            case "Tim":
                buttonPlus.setBackground(getActivity().getResources().getDrawable(R.drawable.operatore_tim));
                break;
            case "Vodafone":
                buttonPlus.setBackground(getActivity().getResources().getDrawable(R.drawable.operatore_vodafone));
                break;
            case "Tre":
                buttonPlus.setBackground(getActivity().getResources().getDrawable(R.drawable.operatore_tre));
                break;
            default:
                buttonPlus.setBackground(getActivity().getResources().getDrawable(R.drawable.operatore_altro));
                layoutMnp.setVisibility(View.GONE);
                altroOperatoreLayout.setVisibility(View.VISIBLE);
                altroOperatore.setText(selectedMnp);
                break;
        }

        newICCID.setText(offertaMobile.getNewICCID());
        oldMSISDN.setText(offertaMobile.getOldMSISDN());
        oldICCID.setText(offertaMobile.getOldICCID());

        offerDescription.setText(offertaMobile.getOfferDescription());
        offerDescriptionUpper.setText(offertaMobile.getOfferDescriptionUpper());
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        final IntentResult intentResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (intentResult != null) {
            if (intentResult.getContents() == null) {

            } else {
                String result = intentResult.getContents();
                offertaMobile.setNewICCID(result);
                oldICCID.setText(result);
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }
}
