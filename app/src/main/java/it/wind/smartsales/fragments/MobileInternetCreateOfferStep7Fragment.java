package it.wind.smartsales.fragments;

import android.app.DatePickerDialog;
import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.InputType;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.nineoldandroids.animation.Animator;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import it.wind.smartsales.R;
import it.wind.smartsales.activity.MainActivity;
import it.wind.smartsales.commons.Log;
import it.wind.smartsales.commons.Utils;
import it.wind.smartsales.customviews.AspectRatioView;
import it.wind.smartsales.dao.PraticheDAO;
import it.wind.smartsales.entities.Session;

/**
 * Created by luca.quaranta on 10/02/2016.
 */
public class MobileInternetCreateOfferStep7Fragment extends Fragment {

    private static final String TAG = "MobileInternetCreateOfferStep7Fragment";
    private RelativeLayout header1, header2, header3, header4, header5, header6, header7;
    private LinearLayout content1, content2, content3, content4, content5, content6, content7;
    private AspectRatioView arrow1, arrow2, arrow3, arrow4, arrow5, arrow6, arrow7;
    private Button continuaButton;

    private CheckBox content1Check, content6Check;
    private AspectRatioView icon2, icon3, icon4, icon5, icon7;
    private TextView label2, label3, label4, label5, label7;

    private ArrayList<String> sessoList;
    private DatePickerDialog dataDiNascitaPicker;

    private DatePickerDialog scadenzaCartaPicker;

    private SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    private SimpleDateFormat monthYearDateFormat = new SimpleDateFormat("MM/yyyy");

    private EditText content2Iban, content2NomeBancaPoste, content2AgenziaFilialeUfficioPostale;

    private EditText
            content3Nome, content3Cognome,
            content3DataDiNascita, content3LuogoDiNascita, content3CodiceFiscale,
            content3Indirizzo, content3NumeroCivico,
            content3CittaLocalita, content3Provincia, content3Cap;

    private Spinner content3Sesso;

    private EditText
            content4Nome, content4Cognome, content4RagioneSociale,
            content4CodiceFiscalePartitaIva, content4Indirizzo, content4NumeroCivico,
            content4Citta, content4Provincia, content4Cap;

    private EditText content5IdentificativoMandato;

    private RadioButton
            content7CartaSi, content7VisaMastercard, content7Diners, content7AmericanExpress,
            content7DeutscheCreditCard;

    private EditText
            content7Numero, content7Scadenza;

    private View.OnFocusChangeListener myFocusChangeListener;
    private AdapterView.OnItemSelectedListener myItemSelectedListener;
    private View.OnClickListener myOnClickListener;

    private void initializeComponents(View view) {
        continuaButton = (Button) view.findViewById(R.id.continua_button);

        header1 = (RelativeLayout) view.findViewById(R.id.header_1);
        header2 = (RelativeLayout) view.findViewById(R.id.header_2);
        header3 = (RelativeLayout) view.findViewById(R.id.header_3);
        header4 = (RelativeLayout) view.findViewById(R.id.header_4);
        header5 = (RelativeLayout) view.findViewById(R.id.header_5);
        header6 = (RelativeLayout) view.findViewById(R.id.header_6);
        header7 = (RelativeLayout) view.findViewById(R.id.header_7);

        content2 = (LinearLayout) view.findViewById(R.id.content_2);
        content3 = (LinearLayout) view.findViewById(R.id.content_3);
        content4 = (LinearLayout) view.findViewById(R.id.content_4);
        content5 = (LinearLayout) view.findViewById(R.id.content_5);
        content7 = (LinearLayout) view.findViewById(R.id.content_7);

        arrow2 = (AspectRatioView) view.findViewById(R.id.arrow_2);
        arrow3 = (AspectRatioView) view.findViewById(R.id.arrow_3);
        arrow4 = (AspectRatioView) view.findViewById(R.id.arrow_4);
        arrow5 = (AspectRatioView) view.findViewById(R.id.arrow_5);
        arrow7 = (AspectRatioView) view.findViewById(R.id.arrow_7);

        icon2 = (AspectRatioView) view.findViewById(R.id.icon_2);
        icon3 = (AspectRatioView) view.findViewById(R.id.icon_3);
        icon4 = (AspectRatioView) view.findViewById(R.id.icon_4);
        icon5 = (AspectRatioView) view.findViewById(R.id.icon_5);
        icon7 = (AspectRatioView) view.findViewById(R.id.icon_7);

        label2 = (TextView) view.findViewById(R.id.label_2);
        label3 = (TextView) view.findViewById(R.id.label_3);
        label4 = (TextView) view.findViewById(R.id.label_4);
        label5 = (TextView) view.findViewById(R.id.label_5);
        label7 = (TextView) view.findViewById(R.id.label_7);

        content1Check = (CheckBox) view.findViewById(R.id.content_1_check);

        content2Iban = (EditText) view.findViewById(R.id.content_2_iban);
        content2NomeBancaPoste = (EditText) view.findViewById(R.id.content_2_nome_banca_poste);
        content2AgenziaFilialeUfficioPostale = (EditText) view.findViewById(R.id.content_2_agenzia_filiale_ufficio_postale);

        content3Nome = (EditText) view.findViewById(R.id.content_3_nome);
        content3Cognome = (EditText) view.findViewById(R.id.content_3_cognome);
        content3DataDiNascita = (EditText) view.findViewById(R.id.content_3_data_di_nascita);
        content3LuogoDiNascita = (EditText) view.findViewById(R.id.content_3_luogo_di_nascita);
        content3CodiceFiscale = (EditText) view.findViewById(R.id.content_3_codice_fiscale);
        content3Indirizzo = (EditText) view.findViewById(R.id.content_3_indirizzo);
        content3NumeroCivico = (EditText) view.findViewById(R.id.content_3_numero_civico);
        content3CittaLocalita = (EditText) view.findViewById(R.id.content_3_citta_localita);
        content3Provincia = (EditText) view.findViewById(R.id.content_3_provincia);
        content3Cap = (EditText) view.findViewById(R.id.content_3_cap);
        content3Sesso = (Spinner) view.findViewById(R.id.content_3_sesso);

        sessoList = new ArrayList<String>();
        sessoList.add("Selezionare...");
        sessoList.add("Maschio");
        sessoList.add("Femmina");
        ArrayAdapter<String> sessoAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, sessoList);
        sessoAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        content3Sesso.setAdapter(sessoAdapter);

        Calendar calendar = Calendar.getInstance();
        dataDiNascitaPicker = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                content3DataDiNascita.setText(dateFormat.format(newDate.getTime()));
                checkContent();
            }

        }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));


        content4Nome = (EditText) view.findViewById(R.id.content_4_nome);
        content4Cognome = (EditText) view.findViewById(R.id.content_4_cognome);
        content4RagioneSociale = (EditText) view.findViewById(R.id.content_4_ragione_sociale);
        content4CodiceFiscalePartitaIva = (EditText) view.findViewById(R.id.content_4_codice_fiscale_partita_iva);
        content4Indirizzo = (EditText) view.findViewById(R.id.content_4_indirizzo);
        content4NumeroCivico = (EditText) view.findViewById(R.id.content_4_numero_civico);
        content4Citta = (EditText) view.findViewById(R.id.content_4_citta);
        content4Provincia = (EditText) view.findViewById(R.id.content_4_provincia);
        content4Cap = (EditText) view.findViewById(R.id.content_4_cap);

        content5IdentificativoMandato = (EditText) view.findViewById(R.id.content_5_identificativo_mandato);

        content6Check = (CheckBox) view.findViewById(R.id.content_6_check);

        content7CartaSi = (RadioButton) view.findViewById(R.id.content_7_cartasi);
        content7VisaMastercard = (RadioButton) view.findViewById(R.id.content_7_visa_mastercard);
        content7Diners = (RadioButton) view.findViewById(R.id.content_7_diners);
        content7AmericanExpress = (RadioButton) view.findViewById(R.id.content_7_american_express);
        content7DeutscheCreditCard = (RadioButton) view.findViewById(R.id.content_7_deutsche_credit_card);

        content7Numero = (EditText) view.findViewById(R.id.content_7_numero);
        content7Scadenza = (EditText) view.findViewById(R.id.content_7_scadenza);
        //content7CodiceCV = (EditText) view.findViewById(R.id.content_7_codice_cv);

        myFocusChangeListener = new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    checkContent();

                }
            }
        };

        myItemSelectedListener = new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                checkContent();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        };

        myOnClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkContent();
            }
        };


    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_mobile_internet_create_offer_step_7, container, false);
        initializeComponents(view);


        header2.setEnabled(false);
        icon2.setBackground(getActivity().getResources().getDrawable(R.drawable.icon_pagamento_1_disabled));
        label2.setTextColor(getActivity().getResources().getColor(android.R.color.darker_gray));
        arrow2.setBackground(getActivity().getResources().getDrawable(R.drawable.arrow_down_inserimento_gray));

        header3.setEnabled(false);
        icon3.setBackground(getActivity().getResources().getDrawable(R.drawable.icon_pagamento_2_disabled));
        label3.setTextColor(getActivity().getResources().getColor(android.R.color.darker_gray));
        arrow3.setBackground(getActivity().getResources().getDrawable(R.drawable.arrow_down_inserimento_gray));

        header4.setEnabled(false);
        icon4.setBackground(getActivity().getResources().getDrawable(R.drawable.icon_pagamento_3_disabled));
        label4.setTextColor(getActivity().getResources().getColor(android.R.color.darker_gray));
        arrow4.setBackground(getActivity().getResources().getDrawable(R.drawable.arrow_down_inserimento_gray));

        header5.setEnabled(false);
        icon5.setBackground(getActivity().getResources().getDrawable(R.drawable.icon_pagamento_4_disabled));
        label5.setTextColor(getActivity().getResources().getColor(android.R.color.darker_gray));
        arrow5.setBackground(getActivity().getResources().getDrawable(R.drawable.arrow_down_inserimento_gray));

        header7.setEnabled(false);
        icon7.setBackground(getActivity().getResources().getDrawable(R.drawable.icon_pagamento_5_disabled));
        label7.setTextColor(getActivity().getResources().getColor(android.R.color.darker_gray));
        arrow7.setBackground(getActivity().getResources().getDrawable(R.drawable.arrow_down_inserimento_gray));


        header2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                manageContent(content2, arrow2);
            }
        });

        header3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                manageContent(content3, arrow3);
            }
        });

        header4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                manageContent(content4, arrow4);
            }
        });

        header5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                manageContent(content5, arrow5);
            }
        });

        header7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                manageContent(content7, arrow7);
            }
        });

        content1Check.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                content1Check.setChecked(true);
                header2.setEnabled(true);
                header3.setEnabled(true);
                header4.setEnabled(true);
                header5.setEnabled(true);

                header2.setEnabled(true);
                icon2.setBackground(getActivity().getResources().getDrawable(R.drawable.icon_pagamento_1_enabled));
                label2.setTextColor(getActivity().getResources().getColor(android.R.color.black));
                arrow2.setBackground(getActivity().getResources().getDrawable(R.drawable.arrow_down_inserimento));

                header3.setEnabled(true);
                icon3.setBackground(getActivity().getResources().getDrawable(R.drawable.icon_pagamento_2_enabled));
                label3.setTextColor(getActivity().getResources().getColor(android.R.color.black));
                arrow3.setBackground(getActivity().getResources().getDrawable(R.drawable.arrow_down_inserimento));

                header4.setEnabled(true);
                icon4.setBackground(getActivity().getResources().getDrawable(R.drawable.icon_pagamento_3_enabled));
                label4.setTextColor(getActivity().getResources().getColor(android.R.color.black));
                arrow4.setBackground(getActivity().getResources().getDrawable(R.drawable.arrow_down_inserimento));

                header5.setEnabled(true);
                icon5.setBackground(getActivity().getResources().getDrawable(R.drawable.icon_pagamento_4_enabled));
                label5.setTextColor(getActivity().getResources().getColor(android.R.color.black));
                arrow5.setBackground(getActivity().getResources().getDrawable(R.drawable.arrow_down_inserimento));

                content6Check.setChecked(false);

                header7.setEnabled(false);
                icon7.setBackground(getActivity().getResources().getDrawable(R.drawable.icon_pagamento_5_disabled));
                label7.setTextColor(getActivity().getResources().getColor(android.R.color.darker_gray));
                arrow7.setBackground(getActivity().getResources().getDrawable(R.drawable.arrow_down_inserimento_gray));
                if (content7.getVisibility() == View.VISIBLE) {
                    manageContent(content7, arrow7);
                }

                checkContent();
            }
        });

        content6Check.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                content1Check.setChecked(false);
                header2.setEnabled(false);
                header3.setEnabled(false);
                header4.setEnabled(false);
                header5.setEnabled(false);

                header2.setEnabled(false);
                icon2.setBackground(getActivity().getResources().getDrawable(R.drawable.icon_pagamento_1_disabled));
                label2.setTextColor(getActivity().getResources().getColor(android.R.color.darker_gray));
                arrow2.setBackground(getActivity().getResources().getDrawable(R.drawable.arrow_down_inserimento_gray));
                if (content2.getVisibility() == View.VISIBLE) {
                    manageContent(content2, arrow2);
                }

                header3.setEnabled(false);
                icon3.setBackground(getActivity().getResources().getDrawable(R.drawable.icon_pagamento_2_disabled));
                label3.setTextColor(getActivity().getResources().getColor(android.R.color.darker_gray));
                arrow3.setBackground(getActivity().getResources().getDrawable(R.drawable.arrow_down_inserimento_gray));
                if (content3.getVisibility() == View.VISIBLE) {
                    manageContent(content3, arrow3);
                }

                header4.setEnabled(false);
                icon4.setBackground(getActivity().getResources().getDrawable(R.drawable.icon_pagamento_3_disabled));
                label4.setTextColor(getActivity().getResources().getColor(android.R.color.darker_gray));
                arrow4.setBackground(getActivity().getResources().getDrawable(R.drawable.arrow_down_inserimento_gray));
                if (content4.getVisibility() == View.VISIBLE) {
                    manageContent(content4, arrow4);
                }

                header5.setEnabled(false);
                icon5.setBackground(getActivity().getResources().getDrawable(R.drawable.icon_pagamento_4_disabled));
                label5.setTextColor(getActivity().getResources().getColor(android.R.color.darker_gray));
                arrow5.setBackground(getActivity().getResources().getDrawable(R.drawable.arrow_down_inserimento_gray));
                if (content5.getVisibility() == View.VISIBLE) {
                    manageContent(content5, arrow5);
                }

                content6Check.setChecked(true);

                header7.setEnabled(true);
                icon7.setBackground(getActivity().getResources().getDrawable(R.drawable.icon_pagamento_5_enabled));
                label7.setTextColor(getActivity().getResources().getColor(android.R.color.black));
                arrow7.setBackground(getActivity().getResources().getDrawable(R.drawable.arrow_down_inserimento));

                checkContent();
            }
        });

        addFocusControl();

        populatePage();
        checkContent();

        continuaButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Session.getInstance().getPratica().setStep(PraticheDAO.STEP8);
                PraticheDAO.updatePratica(getActivity().getBaseContext());


                Log.d("JSON PRATICA)", Session.getInstance().getJsonPratica().toString());
                if(Session.getInstance().getDealer().getDealerRole().compareTo("UserSales")!=0)
                    ((MainActivity) getActivity()).goForward();
                else
                    ((MainActivity) getActivity()).goForwardSelectOffer();
            }
        });

        return view;
    }

    private void manageContent(final LinearLayout contentLayout, final AspectRatioView arrow) {
        if (contentLayout.getVisibility() == View.GONE) {
            YoYo.with(Techniques.SlideInDown).duration(300).withListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animation) {
                    contentLayout.setVisibility(View.VISIBLE);
                }

                @Override
                public void onAnimationEnd(Animator animation) {

                }

                @Override
                public void onAnimationCancel(Animator animation) {

                }

                @Override
                public void onAnimationRepeat(Animator animation) {

                }
            }).playOn(contentLayout);

            arrow.setBackground(getActivity().getResources().getDrawable(R.drawable.arrow_up_inserimento));
        } else {
            YoYo.with(Techniques.SlideOutUp).duration(300).withListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animation) {

                }

                @Override
                public void onAnimationEnd(Animator animation) {
                    contentLayout.setVisibility(View.GONE);
                }

                @Override
                public void onAnimationCancel(Animator animation) {

                }

                @Override
                public void onAnimationRepeat(Animator animation) {

                }
            }).playOn(contentLayout);
            arrow.setBackground(getActivity().getResources().getDrawable(R.drawable.arrow_down_inserimento));
        }
    }

    private void checkContent() {
        updateJson();
        if (content1Check.isChecked()) {
            if (checkContent2() && checkContent3() && checkContent4() && checkContent5()) {
                continuaButton.setEnabled(true);
            }
        } else if (content6Check.isChecked()) {
            if (checkContent7()) {
                continuaButton.setEnabled(true);
            }
        } else {
            continuaButton.setEnabled(false);
        }

//        continuaButton.setEnabled(true);
    }


    private boolean checkContent2() {

        if (TextUtils.isEmpty(content2Iban.getText().toString().trim()) || !Utils.checkIban(content2Iban.getText().toString().trim())) {
            return false;
        }

        if (TextUtils.isEmpty(content2NomeBancaPoste.getText().toString().trim())) {
            return false;
        }

        if (TextUtils.isEmpty(content2AgenziaFilialeUfficioPostale.getText().toString().trim())) {
            return false;
        }

        return true;
    }

    private boolean checkContent3() {

        if (TextUtils.isEmpty(content3Nome.getText().toString().trim())) {
            return false;
        }

        if (TextUtils.isEmpty(content3Cognome.getText().toString().trim())) {
            return false;
        }

        if (TextUtils.isEmpty(content3DataDiNascita.getText().toString().trim())) {
            return false;
        }

        if (TextUtils.isEmpty(content3LuogoDiNascita.getText().toString().trim())) {
            return false;
        }

        if (TextUtils.isEmpty(content3CodiceFiscale.getText().toString().trim()) || !Utils.checkCodiceFiscale(content3CodiceFiscale.getText().toString().trim())) {
            return false;
        }

        if (TextUtils.isEmpty(content3Indirizzo.getText().toString().trim())) {
            return false;
        }

        if (TextUtils.isEmpty(content3NumeroCivico.getText().toString().trim())) {
            return false;
        }

        if (TextUtils.isEmpty(content3CittaLocalita.getText().toString().trim())) {
            return false;
        }

        if (TextUtils.isEmpty(content3Provincia.getText().toString().trim())) {
            return false;
        }

        if (TextUtils.isEmpty(content3Cap.getText().toString().trim()) || content3Cap.getText().toString().trim().length() != 5) {
            return false;
        }

        if (content3Sesso.getSelectedItemPosition() == 0) {
            return false;
        }

        return true;
    }

    private boolean checkContent4() {

        if (TextUtils.isEmpty(content4Nome.getText().toString().trim())) {
            return false;
        }

        if (TextUtils.isEmpty(content4Cognome.getText().toString().trim())) {
            return false;
        }

        if (TextUtils.isEmpty(content4RagioneSociale.getText().toString().trim())) {
            return false;
        }

        if (TextUtils.isEmpty(content4CodiceFiscalePartitaIva.getText().toString().trim())) {
            return false;
        }

        if (TextUtils.isEmpty(content4Indirizzo.getText().toString().trim())) {
            return false;
        }

        if (TextUtils.isEmpty(content4NumeroCivico.getText().toString().trim())) {
            return false;
        }

        if (TextUtils.isEmpty(content4Citta.getText().toString().trim())) {
            return false;
        }

        if (TextUtils.isEmpty(content4Provincia.getText().toString().trim())) {
            return false;
        }

        if (TextUtils.isEmpty(content4Cap.getText().toString().trim()) || content4Cap.getText().toString().trim().length() != 5) {
            return false;
        }

        return true;
    }

    private boolean checkContent5() {

        if (TextUtils.isEmpty(content5IdentificativoMandato.getText().toString().trim())) {
            return false;
        }

        return true;
    }

    private boolean checkContent7() {

        if (!content7CartaSi.isChecked() &&
                !content7VisaMastercard.isChecked() &&
                !content7Diners.isChecked() &&
                !content7AmericanExpress.isChecked() &&
                !content7DeutscheCreditCard.isChecked()) {
            return false;
        }

//        if (!content7VisaMastercard.isChecked()) {
//            return false;
//        }
//
//        if (!content7Diners.isChecked()) {
//            return false;
//        }
//
//        if (!content7AmericanExpress.isChecked()) {
//            return false;
//        }
//
//        if (!content7DeutscheCreditCard.isChecked()) {
//            return false;
//        }

        if (TextUtils.isEmpty(content7Numero.getText().toString().trim())) {
            return false;
        }

        if (TextUtils.isEmpty(content7Scadenza.getText().toString().trim())) {
            return false;
        }

//        if (TextUtils.isEmpty(content7CodiceCV.getText().toString().trim())) {
//            return false;
//        }

        return true;
    }

    private void addFocusControl() {

        content2Iban.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    if (!TextUtils.isEmpty(content2Iban.getText().toString().trim()) && !Utils.checkIban(content2Iban.getText().toString().trim())) {
                        Utils.showWarningMessage(getActivity(), "", "Codice IBAN non valido");
                    } else {
                        checkContent();
                    }
                }
            }
        });
        content2NomeBancaPoste.setOnFocusChangeListener(myFocusChangeListener);
        content2AgenziaFilialeUfficioPostale.setOnFocusChangeListener(myFocusChangeListener);

        content3Nome.setOnFocusChangeListener(myFocusChangeListener);
        content3Cognome.setOnFocusChangeListener(myFocusChangeListener);
        content3DataDiNascita.setOnFocusChangeListener(myFocusChangeListener);
        content3LuogoDiNascita.setOnFocusChangeListener(myFocusChangeListener);
        content3CodiceFiscale.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    if (!TextUtils.isEmpty(content3CodiceFiscale.getText().toString().trim()) && !Utils.checkCodiceFiscale(content3CodiceFiscale.getText().toString().trim())) {
                        Utils.showWarningMessage(getActivity(), "", "Codice Fiscale non valido");
                    } else {
                        checkContent();
                    }
                }
            }
        });
        content3Indirizzo.setOnFocusChangeListener(myFocusChangeListener);
        content3NumeroCivico.setOnFocusChangeListener(myFocusChangeListener);
        content3CittaLocalita.setOnFocusChangeListener(myFocusChangeListener);
        content3Provincia.setOnFocusChangeListener(myFocusChangeListener);
        content3Cap.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    if (!TextUtils.isEmpty(content3Cap.getText().toString().trim()) && content3Cap.getText().toString().trim().length() != 5) {
                        Utils.showWarningMessage(getActivity(), "", "CAP non valido");
                    } else {
                        checkContent();
                    }
                }
            }
        });
        content3Sesso.setOnItemSelectedListener(myItemSelectedListener);

        content4Nome.setOnFocusChangeListener(myFocusChangeListener);
        content4Cognome.setOnFocusChangeListener(myFocusChangeListener);
        content4RagioneSociale.setOnFocusChangeListener(myFocusChangeListener);
        content4CodiceFiscalePartitaIva.setOnFocusChangeListener(myFocusChangeListener);
        content4Indirizzo.setOnFocusChangeListener(myFocusChangeListener);
        content4NumeroCivico.setOnFocusChangeListener(myFocusChangeListener);
        content4Citta.setOnFocusChangeListener(myFocusChangeListener);
        content4Provincia.setOnFocusChangeListener(myFocusChangeListener);
        content4Cap.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    if (!TextUtils.isEmpty(content4Cap.getText().toString().trim()) && content4Cap.getText().toString().trim().length() != 5) {
                        Utils.showWarningMessage(getActivity(), "", "CAP non valido");
                    } else {
                        checkContent();
                    }
                }
            }
        });

        content5IdentificativoMandato.setOnFocusChangeListener(myFocusChangeListener);

        content7CartaSi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                content7CartaSi.setChecked(true);
                content7VisaMastercard.setChecked(false);
                content7Diners.setChecked(false);
                content7AmericanExpress.setChecked(false);
                content7DeutscheCreditCard.setChecked(false);
                checkContent();
            }
        });
        content7VisaMastercard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                content7CartaSi.setChecked(false);
                content7VisaMastercard.setChecked(true);
                content7Diners.setChecked(false);
                content7AmericanExpress.setChecked(false);
                content7DeutscheCreditCard.setChecked(false);
                checkContent();
            }
        });
        content7Diners.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                content7CartaSi.setChecked(false);
                content7VisaMastercard.setChecked(false);
                content7Diners.setChecked(true);
                content7AmericanExpress.setChecked(false);
                content7DeutscheCreditCard.setChecked(false);
                checkContent();
            }
        });
        content7AmericanExpress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                content7CartaSi.setChecked(false);
                content7VisaMastercard.setChecked(false);
                content7Diners.setChecked(false);
                content7AmericanExpress.setChecked(true);
                content7DeutscheCreditCard.setChecked(false);
                checkContent();
            }
        });
        content7DeutscheCreditCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                content7CartaSi.setChecked(false);
                content7VisaMastercard.setChecked(false);
                content7Diners.setChecked(false);
                content7AmericanExpress.setChecked(false);
                content7DeutscheCreditCard.setChecked(true);
                checkContent();
            }
        });
        content7Numero.setOnFocusChangeListener(myFocusChangeListener);


//        View.OnFocusChangeListener scadenzaFocusChangeListener = new View.OnFocusChangeListener() {
//            @Override
//            public void onFocusChange(View v, boolean hasFocus) {
//                if (!hasFocus) {
//                    validateScadenza();
//                    checkContent();
//                }
//            }
//        };
//
//        content7Scadenza.setOnFocusChangeListener(scadenzaFocusChangeListener);


        //content7CodiceCV.setOnFocusChangeListener(myFocusChangeListener);
        Calendar calendar = Calendar.getInstance();
        scadenzaCartaPicker= new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                content7Scadenza.setText(monthYearDateFormat.format(newDate.getTime()));
                checkContent();
            }

        }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));



        content7Scadenza.setInputType(InputType.TYPE_NULL);


        content7Scadenza.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               scadenzaCartaPicker.show();

           }
        });


    }

    private void validateScadenza() {

        DateFormat df = new SimpleDateFormat("mm/yyyy");
        df.setLenient(false);
        try {
            Date result =  df.parse(content7Scadenza.getText().toString().trim());
            Log.d(TAG, String.valueOf(result.getTime()));
        } catch (ParseException e) {
            Utils.showWarningMessage(getActivity(), "", "Data scadenza carta non valida");
        }
    }

    private void updateJson() {
        JSONObject pagamento = new JSONObject();
        try {
            pagamento.put("pagamentoTramiteRid", content1Check.isChecked() ? "1" : "0");

            JSONObject estremiContoCorrente = new JSONObject();
            estremiContoCorrente.put("iban", content2Iban.getText().toString().trim());
            estremiContoCorrente.put("nomeBancaPosta", content2NomeBancaPoste.getText().toString().trim());
            estremiContoCorrente.put("agenziaUfficioFiliale", content2AgenziaFilialeUfficioPostale.getText().toString().trim());
            pagamento.put("estremiContoCorrente", estremiContoCorrente);

            JSONObject sottoscrittoreDelModulo = new JSONObject();
            sottoscrittoreDelModulo.put("nome", content3Nome.getText().toString().trim());
            sottoscrittoreDelModulo.put("cognome", content3Cognome.getText().toString().trim());
            sottoscrittoreDelModulo.put("gender", sessoList.get(content3Sesso.getSelectedItemPosition()));
            sottoscrittoreDelModulo.put("dataDiNascita", content3DataDiNascita.getText().toString().trim());
            sottoscrittoreDelModulo.put("luogoDiNascita", content3LuogoDiNascita.getText().toString().trim());
            sottoscrittoreDelModulo.put("cf", content3CodiceFiscale.getText().toString().trim());
            sottoscrittoreDelModulo.put("indirizzo", content3Indirizzo.getText().toString().trim());
            sottoscrittoreDelModulo.put("civico", content3NumeroCivico.getText().toString().trim());
            sottoscrittoreDelModulo.put("citta", content3CittaLocalita.getText().toString().trim());
            sottoscrittoreDelModulo.put("provincia", content3Provincia.getText().toString().trim());
            sottoscrittoreDelModulo.put("cap", content3Cap.getText().toString().trim());
            pagamento.put("sottoscrittoreDelModulo", sottoscrittoreDelModulo);

            JSONObject intestatarioConto = new JSONObject();
            intestatarioConto.put("nome", content4Nome.getText().toString().trim());
            intestatarioConto.put("cognome", content4Cognome.getText().toString().trim());
            intestatarioConto.put("ragioneSociale", content4RagioneSociale.getText().toString().trim());
            intestatarioConto.put("cf", content4CodiceFiscalePartitaIva.getText().toString().trim());
            intestatarioConto.put("indirizzo", content4Indirizzo.getText().toString().trim());
            intestatarioConto.put("civico", content4NumeroCivico.getText().toString().trim());
            intestatarioConto.put("citta", content4Citta.getText().toString().trim());
            intestatarioConto.put("provincia", content4Provincia.getText().toString().trim());
            intestatarioConto.put("cap", content4Cap.getText().toString().trim());
            pagamento.put("intestatarioConto", intestatarioConto);

            JSONObject addebitoDiretto = new JSONObject();
            addebitoDiretto.put("identificativoMandato", content5IdentificativoMandato.getText().toString().trim());
            pagamento.put("addebitoDiretto", addebitoDiretto);

            pagamento.put("pagamentoTramiteCarta", content6Check.isChecked() ? "1" : "0");

            JSONObject autorizzazionePermanenteDiAddebito = new JSONObject();
            autorizzazionePermanenteDiAddebito.put("cartaSi", content7CartaSi.isChecked() ? "1" : "0");
            autorizzazionePermanenteDiAddebito.put("visaMastercard", content7VisaMastercard.isChecked() ? "1" : "0");
            autorizzazionePermanenteDiAddebito.put("diners", content7Diners.isChecked() ? "1" : "0");
            autorizzazionePermanenteDiAddebito.put("americanExpress", content7AmericanExpress.isChecked() ? "1" : "0");
            autorizzazionePermanenteDiAddebito.put("deutscheCreditCard", content7DeutscheCreditCard.isChecked() ? "1" : "0");
            autorizzazionePermanenteDiAddebito.put("numero", content7Numero.getText().toString().trim());
            autorizzazionePermanenteDiAddebito.put("scadenza", content7Scadenza.getText().toString().trim());
            //autorizzazionePermanenteDiAddebito.put("codiceCV", content7CodiceCV.getText().toString().trim());
            pagamento.put("autorizzazionePermanenteDiAddebito", autorizzazionePermanenteDiAddebito);

//            return inserimentoDati;
            Session.getInstance().getJsonPratica().getJSONObject("pratica").put("infoPagamento", pagamento);

            //store data on sqlLite
            PraticheDAO.updatePratica(this.getActivity().getBaseContext());
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void populatePage() {
        try {
            JSONObject infoPagamento = Session.getInstance().getJsonPratica().getJSONObject("pratica").optJSONObject("infoPagamento");

            if (infoPagamento != null) {

                content1Check.setChecked(infoPagamento.optString("pagamentoTramiteRid").compareTo("1") == 0 ? true : false);

                if (content1Check.isChecked()){
                    header2.setEnabled(true);
                    header3.setEnabled(true);
                    header4.setEnabled(true);
                    header5.setEnabled(true);

                    header2.setEnabled(true);
                    icon2.setBackground(getActivity().getResources().getDrawable(R.drawable.icon_pagamento_1_enabled));
                    label2.setTextColor(getActivity().getResources().getColor(android.R.color.black));
                    arrow2.setBackground(getActivity().getResources().getDrawable(R.drawable.arrow_down_inserimento));

                    header3.setEnabled(true);
                    icon3.setBackground(getActivity().getResources().getDrawable(R.drawable.icon_pagamento_2_enabled));
                    label3.setTextColor(getActivity().getResources().getColor(android.R.color.black));
                    arrow3.setBackground(getActivity().getResources().getDrawable(R.drawable.arrow_down_inserimento));

                    header4.setEnabled(true);
                    icon4.setBackground(getActivity().getResources().getDrawable(R.drawable.icon_pagamento_3_enabled));
                    label4.setTextColor(getActivity().getResources().getColor(android.R.color.black));
                    arrow4.setBackground(getActivity().getResources().getDrawable(R.drawable.arrow_down_inserimento));

                    header5.setEnabled(true);
                    icon5.setBackground(getActivity().getResources().getDrawable(R.drawable.icon_pagamento_4_enabled));
                    label5.setTextColor(getActivity().getResources().getColor(android.R.color.black));
                    arrow5.setBackground(getActivity().getResources().getDrawable(R.drawable.arrow_down_inserimento));

                    content6Check.setChecked(false);

                    header7.setEnabled(false);
                    icon7.setBackground(getActivity().getResources().getDrawable(R.drawable.icon_pagamento_5_disabled));
                    label7.setTextColor(getActivity().getResources().getColor(android.R.color.darker_gray));
                    arrow7.setBackground(getActivity().getResources().getDrawable(R.drawable.arrow_down_inserimento_gray));
                    if (content7.getVisibility() == View.VISIBLE) {
                        manageContent(content7, arrow7);
                    }
                }

                JSONObject estremiContoCorrente = infoPagamento.getJSONObject("estremiContoCorrente");
                content2Iban.setText(estremiContoCorrente.optString("iban"));
                content2NomeBancaPoste.setText(estremiContoCorrente.optString("nomeBancaPosta"));
                content2AgenziaFilialeUfficioPostale.setText(estremiContoCorrente.optString("agenziaUfficioFiliale"));

                JSONObject sottoscrittoreDelModulo = infoPagamento.getJSONObject("sottoscrittoreDelModulo");
                content3Nome.setText(sottoscrittoreDelModulo.optString("nome"));
                content3Cognome.setText(sottoscrittoreDelModulo.optString("cognome"));
                for (int i = 0; i < sessoList.size(); i++) {
                    if (sessoList.get(i).compareToIgnoreCase(sottoscrittoreDelModulo.optString("gender")) == 0) {
                        content3Sesso.setSelection(i);
                    }
                }
                content3DataDiNascita.setText(sottoscrittoreDelModulo.optString("dataDiNascita"));
                content3LuogoDiNascita.setText(sottoscrittoreDelModulo.optString("luogoDiNascita"));
                content3CodiceFiscale.setText(sottoscrittoreDelModulo.optString("cf"));
                content3Indirizzo.setText(sottoscrittoreDelModulo.optString("indirizzo"));
                content3NumeroCivico.setText(sottoscrittoreDelModulo.optString("civico"));
                content3CittaLocalita.setText(sottoscrittoreDelModulo.optString("citta"));
                content3Provincia.setText(sottoscrittoreDelModulo.optString("provincia"));
                content3Cap.setText(sottoscrittoreDelModulo.optString("cap"));

                JSONObject intestatarioConto = infoPagamento.getJSONObject("intestatarioConto");
                content4Nome.setText(intestatarioConto.optString("nome"));
                content4Cognome.setText(intestatarioConto.optString("cognome"));
                content4RagioneSociale.setText(intestatarioConto.optString("ragioneSociale"));
                content4CodiceFiscalePartitaIva.setText(intestatarioConto.optString("cf"));
                content4Indirizzo.setText(intestatarioConto.optString("indirizzo"));
                content4NumeroCivico.setText(intestatarioConto.optString("civico"));
                content4Citta.setText(intestatarioConto.optString("citta"));
                content4Provincia.setText(intestatarioConto.optString("provincia"));
                content4Cap.setText(intestatarioConto.optString("cap"));

                JSONObject addebitoDiretto = infoPagamento.getJSONObject("addebitoDiretto");
                content5IdentificativoMandato.setText(addebitoDiretto.optString("identificativoMandato"));

                content6Check.setChecked(infoPagamento.optString("pagamentoTramiteCarta").compareTo("1") == 0 ? true : false);

                if (content6Check.isChecked()){
                    header2.setEnabled(false);
                    header3.setEnabled(false);
                    header4.setEnabled(false);
                    header5.setEnabled(false);

                    header2.setEnabled(false);
                    icon2.setBackground(getActivity().getResources().getDrawable(R.drawable.icon_pagamento_1_disabled));
                    label2.setTextColor(getActivity().getResources().getColor(android.R.color.darker_gray));
                    arrow2.setBackground(getActivity().getResources().getDrawable(R.drawable.arrow_down_inserimento_gray));
                    if (content2.getVisibility() == View.VISIBLE) {
                        manageContent(content2, arrow2);
                    }

                    header3.setEnabled(false);
                    icon3.setBackground(getActivity().getResources().getDrawable(R.drawable.icon_pagamento_2_disabled));
                    label3.setTextColor(getActivity().getResources().getColor(android.R.color.darker_gray));
                    arrow3.setBackground(getActivity().getResources().getDrawable(R.drawable.arrow_down_inserimento_gray));
                    if (content3.getVisibility() == View.VISIBLE) {
                        manageContent(content3, arrow3);
                    }

                    header4.setEnabled(false);
                    icon4.setBackground(getActivity().getResources().getDrawable(R.drawable.icon_pagamento_3_disabled));
                    label4.setTextColor(getActivity().getResources().getColor(android.R.color.darker_gray));
                    arrow4.setBackground(getActivity().getResources().getDrawable(R.drawable.arrow_down_inserimento_gray));
                    if (content4.getVisibility() == View.VISIBLE) {
                        manageContent(content4, arrow4);
                    }

                    header5.setEnabled(false);
                    icon5.setBackground(getActivity().getResources().getDrawable(R.drawable.icon_pagamento_4_disabled));
                    label5.setTextColor(getActivity().getResources().getColor(android.R.color.darker_gray));
                    arrow5.setBackground(getActivity().getResources().getDrawable(R.drawable.arrow_down_inserimento_gray));
                    if (content5.getVisibility() == View.VISIBLE) {
                        manageContent(content5, arrow5);
                    }

                    content6Check.setChecked(true);

                    header7.setEnabled(true);
                    icon7.setBackground(getActivity().getResources().getDrawable(R.drawable.icon_pagamento_5_enabled));
                    label7.setTextColor(getActivity().getResources().getColor(android.R.color.black));
                    arrow7.setBackground(getActivity().getResources().getDrawable(R.drawable.arrow_down_inserimento));
                }

                JSONObject autorizzazionePermanenteDiAddebito = infoPagamento.getJSONObject("autorizzazionePermanenteDiAddebito");
                content7CartaSi.setChecked(autorizzazionePermanenteDiAddebito.optString("cartaSi").compareTo("1") == 0 ? true : false);
                content7VisaMastercard.setChecked(autorizzazionePermanenteDiAddebito.optString("visaMastercard").compareTo("1") == 0 ? true : false);
                content7Diners.setChecked(autorizzazionePermanenteDiAddebito.optString("diners").compareTo("1") == 0 ? true : false);
                content7AmericanExpress.setChecked(autorizzazionePermanenteDiAddebito.optString("americanExpress").compareTo("1") == 0 ? true : false);
                content7DeutscheCreditCard.setChecked(autorizzazionePermanenteDiAddebito.optString("deutscheCreditCard").compareTo("1") == 0 ? true : false);
                content7Numero.setText(autorizzazionePermanenteDiAddebito.optString("numero"));
                content7Scadenza.setText(autorizzazionePermanenteDiAddebito.optString("scadenza"));
                //content7CodiceCV.setText(autorizzazionePermanenteDiAddebito.optString("codiceCV"));


            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
