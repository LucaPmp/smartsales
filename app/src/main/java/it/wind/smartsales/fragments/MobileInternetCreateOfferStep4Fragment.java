package it.wind.smartsales.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import it.wind.smartsales.customviews.HorizontalSpaceItemDecoration;
import it.wind.smartsales.dao.CatalogoFasceScontiDAO;
import it.wind.smartsales.dao.CatalogoFasceScontiRicaricabiliDAO;
import it.wind.smartsales.dao.CatalogoPianiTariffariDAO;
import it.wind.smartsales.dao.CatalogoTerminaliDAO;
import it.wind.smartsales.dao.PraticheDAO;
import it.wind.smartsales.entities.AddOnMobile;
import it.wind.smartsales.entities.FasceSconti;
import it.wind.smartsales.entities.FasceScontiRicaricabili;
import it.wind.smartsales.entities.OffertaMobile;
import it.wind.smartsales.entities.OpzioneMobile;
import it.wind.smartsales.entities.PercentualeSconto;
import it.wind.smartsales.entities.PianoTariffario;
import it.wind.smartsales.entities.Session;
import it.wind.smartsales.entities.Terminale;
import it.wind.smartsales.entities.TerminaleBean;
import it.wind.smartsales.R;
import it.wind.smartsales.activity.MainActivity;
import it.wind.smartsales.adapter.MobileOfferGigaListStep4Adapter;
import it.wind.smartsales.adapter.MobileOfferRicListStep4Adapter;
import it.wind.smartsales.adapter.MobileOfferRicStep4Adapter;
import it.wind.smartsales.adapter.MobileOfferStep4Adapter;
import it.wind.smartsales.commons.Constants;
import it.wind.smartsales.commons.Log;
import it.wind.smartsales.commons.Utils;

/**
 * Created by luca.quaranta on 10/02/2016.
 */
public class MobileInternetCreateOfferStep4Fragment extends Fragment {

    private RecyclerView mobileOfferRecyclerView;
    private RecyclerView mobileOfferRicListRecyclerView;
    private RecyclerView mobileOfferGigaListRecyclerView;
    private RecyclerView mobileOfferRicRecyclerView;
    private RecyclerView.LayoutManager mLayoutManagerMobileOffer;
    private RecyclerView mobileOfferRicRicaricabiliRecyclerView;
    private RecyclerView.LayoutManager mLayoutManagerRicaricabiliMobileOffer;
    private RecyclerView.Adapter mobileOfferAdapter;
    private RecyclerView.Adapter mobileRicOfferAdapter;
    private RecyclerView.Adapter mobileRicListOfferAdapter;
    private RecyclerView.Adapter mobileGigaListOfferAdapter;

    public float gigaAcquistati;
    public float spesapromoRicTot;
    private float spesapromoRic;
    private Button continuaButton;

    private boolean mostraBoxRicaricabili;

    private ArrayList<OffertaMobile> offertaMobiles;
    public FasceSconti fasceSconti;
    public FasceScontiRicaricabili fasceScontiRicaricabili;
    private TextView numSim, spesaPromo, spesaScontata, numSimRic, numSimAbb, ricaricabile_totale, abbonamento_totale, abbonamento_totale_sconto, ricaricabile_totale_sconto;
    private MobileInternetCreateOfferStep4Fragment mobileInternetCreateOfferStep4Fragment = this;
    private ArrayList<OffertaMobile> RicaricabiliArray;
    private ArrayList<AddOnMobile> gigaArray;

    boolean isKOChecked = false;
    boolean isGigaKOChecked = false;
    int isFirstOpen = 0;

    private void initializeComponents(View view) {
        mobileOfferRicRecyclerView = (RecyclerView) view.findViewById(R.id.mobile_offer_recycler_view);
        mobileOfferRicListRecyclerView = (RecyclerView) view.findViewById(R.id.offerta_ricaricabili_sim_label_recycler);
        mobileOfferGigaListRecyclerView = (RecyclerView) view.findViewById(R.id.offerta_ricaricabili_giga_label_recycler);
        continuaButton = (Button) view.findViewById(R.id.continua_button);
        numSim = (TextView) view.findViewById(R.id.num_sim);
        numSimRic = (TextView) view.findViewById(R.id.num_sim_ricaricabile_SIM_sconto);
        numSimAbb = (TextView) view.findViewById(R.id.num_sim_abbonamento_sconti);
        spesaPromo = (TextView) view.findViewById(R.id.spesa_promo);
        spesaScontata = (TextView) view.findViewById(R.id.spesa_scontata);
        ricaricabile_totale = (TextView) view.findViewById(R.id.ricaricabile_totale_SIM_title);
        abbonamento_totale = (TextView) view.findViewById(R.id.abbonamento_totale_SIM_title);
        abbonamento_totale_sconto = (TextView) view.findViewById(R.id.abbonamento_totale_SIM_sconto_title);
        ricaricabile_totale_sconto = (TextView) view.findViewById(R.id.ricaricabile_totale_SIM_sconto_title);

        initMobileOfferList();

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
//        populateListaRicaricabiliPaginaSconto(RicaricabiliArray, null);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.fragment_mobile_internet_create_offer_step_4, container, false);
        initializeComponents(view);

        try {
            JSONArray offerteRaggruppate = (JSONArray) Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONArray("offerteRaggruppate");
            JSONObject pratica = Session.getInstance().getJsonPratica().getJSONObject("pratica");
            offertaMobiles = new ArrayList<>();
            fasceSconti = CatalogoFasceScontiDAO.retrieveFasceSconti(getActivity());
            fasceScontiRicaricabili = CatalogoFasceScontiRicaricabiliDAO.retrieveFasceScontiRicaricabili(getActivity());

            if (offerteRaggruppate.length() <= 0) {


                JSONArray pacchetti = Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONArray("pacchetti");


                for (int i = 0; i < pacchetti.length(); i++) {
                    JSONObject pacchetto = pacchetti.getJSONObject(i);
                    JSONArray offerte = pacchetto.getJSONArray("offerte");

                    for (int j = 0; j < offerte.length(); j++) {

                        boolean offertaTrovata = false;
                        int offertaDaAggiornare = 0;
                        for (int k = 0; k < offertaMobiles.size(); k++) {
                            if (offerte.getJSONObject(j).getInt("idOfferta") == offertaMobiles.get(k).getIdOfferta()) {
                                offertaTrovata = true;
                                offertaDaAggiornare = k;
                                break;
                            }
                        }

                        if (offertaTrovata) {
                            offertaMobiles.get(offertaDaAggiornare).setSelectedNumSimStep3(offertaMobiles.get(offertaDaAggiornare).getSelectedNumSimStep3() + offerte.getJSONObject(j).getInt("selectedNumSimStep3"));

                            ArrayList<AddOnMobile> addOnMobileArrayList = offertaMobiles.get(offertaDaAggiornare).getAddOnMobileArrayList();
                            JSONArray addOnMobileJsonArray = offerte.getJSONObject(j).getJSONArray("addOnMobileList");

                            for (int j2 = 0; j2 < addOnMobileJsonArray.length(); j2++) {

                                boolean addOnMobileTrovato = false;
                                int addOnMobileDaAggiornare = 0;
                                for (int k2 = 0; k2 < addOnMobileArrayList.size(); k2++) {
                                    if (addOnMobileJsonArray.getJSONObject(j2).getString("descFascia").compareToIgnoreCase(addOnMobileArrayList.get(k2).getDescFascia()) == 0) {
                                        addOnMobileTrovato = true;
                                        addOnMobileDaAggiornare = k2;
                                        break;
                                    }
                                }

                                if (addOnMobileTrovato) {
                                    addOnMobileArrayList.get(addOnMobileDaAggiornare).setSelectedNumSimStep3(addOnMobileArrayList.get(addOnMobileDaAggiornare).getSelectedNumSimStep3() + addOnMobileJsonArray.getJSONObject(j2).getInt("selectedNumSimStep3"));
                                } else {
                                    addOnMobileArrayList.add(Utils.createAddOnMobile(addOnMobileJsonArray.getJSONObject(j2)));
                                }
                            }

                            ArrayList<Terminale> terminaleArrayList = offertaMobiles.get(offertaDaAggiornare).getTerminaleArrayList();
                            JSONArray terminaleJsonArray = offerte.getJSONObject(j).getJSONArray("terminaliList");
                            JSONArray terminaleBeanJsonArray = offerte.getJSONObject(j).getJSONArray("terminaleBeanList");

                            for (int j2 = 0; j2 < terminaleJsonArray.length(); j2++) {
                                TerminaleBean terminaleBean = Utils.createTerminaleBean(terminaleBeanJsonArray.getJSONObject(j2));
                                if (terminaleBean.getSelectedNumSimStep3Mnp() > 0) {
                                    Terminale terminale = CatalogoTerminaliDAO.retrieveTerminale(getActivity(), terminaleBean.getIdTerminale(), "true", String.valueOf(offertaMobiles.get(offertaDaAggiornare).getIdOfferta()));
                                    terminale.setSelectedNumSimStep3(terminaleBean.getSelectedNumSimStep3Mnp());
                                    terminaleJsonArray.put(Utils.createTerminale(terminale));
                                }
                                if (terminaleBean.getSelectedNumSimStep3NuovaLinea() > 0) {
                                    Terminale terminale = CatalogoTerminaliDAO.retrieveTerminale(getActivity(), terminaleBean.getIdTerminale(), "false", String.valueOf(offertaMobiles.get(offertaDaAggiornare).getIdOfferta()));
                                    terminale.setSelectedNumSimStep3(terminaleBean.getSelectedNumSimStep3NuovaLinea());
                                    terminaleJsonArray.put(Utils.createTerminale(terminale));
                                }
                            }


                            for (int j2 = 0; j2 < terminaleJsonArray.length(); j2++) {

                                boolean terminaleTrovato = false;
                                int terminaleDaAggiornare = 0;
                                for (int k2 = 0; k2 < terminaleArrayList.size(); k2++) {
                                    if (terminaleJsonArray.getJSONObject(j2).getString("idTerminale").compareToIgnoreCase(terminaleArrayList.get(k2).getIdTerminale()) == 0 && terminaleJsonArray.getJSONObject(j2).getString("mnp").compareToIgnoreCase(terminaleArrayList.get(k2).getMnp()) == 0) {
                                        terminaleTrovato = true;
                                        terminaleDaAggiornare = k2;
                                        break;
                                    }
                                }

                                if (terminaleTrovato) {
                                    terminaleArrayList.get(terminaleDaAggiornare).setSelectedNumSimStep3(terminaleArrayList.get(terminaleDaAggiornare).getSelectedNumSimStep3() + terminaleJsonArray.getJSONObject(j2).getInt("selectedNumSimStep3"));
                                } else {
                                    terminaleArrayList.add(Utils.createTerminale(terminaleJsonArray.getJSONObject(j2)));
                                }
                            }

                            ArrayList<OpzioneMobile> opzioneMobileArrayList = offertaMobiles.get(offertaDaAggiornare).getOpzioneMobileArrayList();
                            JSONArray opzioneMobileJsonArray = offerte.getJSONObject(j).getJSONArray("opzioniMobileList");

                            for (int j2 = 0; j2 < opzioneMobileJsonArray.length(); j2++) {

                                boolean opzioneMobileTrovato = false;
                                int opzioneMobileDaAggiornare = 0;
                                for (int k2 = 0; k2 < opzioneMobileArrayList.size(); k2++) {
                                    if (opzioneMobileJsonArray.getJSONObject(j2).getString("idOpzione").compareToIgnoreCase(opzioneMobileArrayList.get(k2).getIdOpzione()) == 0) {
                                        opzioneMobileTrovato = true;
                                        opzioneMobileDaAggiornare = k2;
                                        break;
                                    }
                                }

                                if (opzioneMobileTrovato) {
                                    opzioneMobileArrayList.get(opzioneMobileDaAggiornare).setSelectedNumSimStep3(opzioneMobileArrayList.get(opzioneMobileDaAggiornare).getSelectedNumSimStep3() + opzioneMobileJsonArray.getJSONObject(j2).getInt("selectedNumSimStep3"));
                                } else {
                                    opzioneMobileArrayList.add(Utils.createOpzioneMobile(opzioneMobileJsonArray.getJSONObject(j2)));
                                }
                            }

                        } else {
                            OffertaMobile offertaMobile = Utils.createOffertaMobile(offerte.getJSONObject(j));
//                        offertaMobiles.add(Utils.createOffertaMobile(offerte.getJSONObject(j)));

                            ArrayList<Terminale> terminaleArrayList = offertaMobile.getTerminaleArrayList();
                            JSONArray terminaleJsonArray = offerte.getJSONObject(j).getJSONArray("terminaliList");
                            JSONArray terminaleBeanJsonArray = offerte.getJSONObject(j).getJSONArray("terminaleBeanList");

                            for (int j2 = 0; j2 < terminaleBeanJsonArray.length(); j2++) {
                                TerminaleBean terminaleBean = Utils.createTerminaleBean(terminaleBeanJsonArray.getJSONObject(j2));
                                if (terminaleBean.getSelectedNumSimStep3Mnp() > 0) {
                                    Terminale terminale = CatalogoTerminaliDAO.retrieveTerminale(getActivity(), terminaleBean.getIdTerminale(), "true", String.valueOf(offertaMobile.getIdOfferta()));
                                    terminale.setSelectedNumSimStep3(terminaleBean.getSelectedNumSimStep3Mnp());
                                    terminaleArrayList.add(terminale);
                                }
                                if (terminaleBean.getSelectedNumSimStep3NuovaLinea() > 0) {
                                    Terminale terminale = CatalogoTerminaliDAO.retrieveTerminale(getActivity(), terminaleBean.getIdTerminale(), "false", String.valueOf(offertaMobile.getIdOfferta()));
                                    terminale.setSelectedNumSimStep3(terminaleBean.getSelectedNumSimStep3NuovaLinea());
                                    terminaleArrayList.add(terminale);
                                }
                            }

                            offertaMobiles.add(offertaMobile);
                        }
                    }
                }

                for (OffertaMobile offertaMobile : offertaMobiles) {
                    ArrayList<PianoTariffario> pianoTariffarioArrayList = CatalogoPianiTariffariDAO.retrievePianiTariffari(getActivity(), String.valueOf(offertaMobile.getIdOfferta()));
                    for (PianoTariffario pianoTariffario : pianoTariffarioArrayList) {
                        offertaMobile.setPercentualeScontoPenale(pianoTariffario.getPercentualeScontoPenale());
                        offertaMobile.getPercentualeScontoArrayList().add(new PercentualeSconto(pianoTariffario.getPercentualeSconto()));
                    }
                    offertaMobile.setSelectedNumSimStep4(offertaMobile.getSelectedNumSimStep3());
                    if (!offertaMobile.getPercentualeScontoArrayList().isEmpty()) {
                        offertaMobile.setScontoApplicato(offertaMobile.getPercentualeScontoArrayList().get(0).getPercentualeSconto());
                    } else {
                        offertaMobile.setScontoApplicato("0");
                    }
                    if (!TextUtils.isEmpty(offertaMobile.getCanonePromo())) {
                        offertaMobile.setCanoneScontato(String.valueOf(Float.parseFloat(offertaMobile.getCanonePromo().replace(",", ".")) - Float.parseFloat(offertaMobile.getScontoApplicato().replace(",", ".")) / 100 * Float.parseFloat(offertaMobile.getCanonePromo().replace(",", "."))));
                    } else {
                        offertaMobile.setCanoneScontato(String.valueOf(Float.parseFloat(offertaMobile.getCanoneMensile().replace(",", ".")) - Float.parseFloat(offertaMobile.getScontoApplicato().replace(",", ".")) / 100 * Float.parseFloat(offertaMobile.getCanoneMensile().replace(",", "."))));
                    }
                    offertaMobile.setAutorizzabilita(checkAutorization(offertaMobile));
                }

            } else {
                for (int i = 0; i < offerteRaggruppate.length(); i++) {
                    offertaMobiles.add(Utils.createOffertaMobile(offerteRaggruppate.getJSONObject(i)));
                }
            }
            ArrayList<OffertaMobile> AbbonamentiArray = new ArrayList<>();
            RicaricabiliArray = new ArrayList<>();
            gigaArray = new ArrayList<>();


            while (gigaArray.size() < 5) {
                AddOnMobile a = new AddOnMobile();
                gigaArray.add(gigaArray.size(), a);
            }

            //L'array mi recupera divide i piani abbonamento dai ricaricabili e mi somma tra i Ricaricabili
            //anche le Opzioni TopMondo e Premium

            int k = 0;
            int k1 = 0;
            for (int i = 0; i < offertaMobiles.size(); i++) {

                if (offertaMobiles.get(i).getOfferType().equals("Abbonamento")) {
                    AbbonamentiArray.add(k, offertaMobiles.get(i));
                    k++;

                }
                if (offertaMobiles.get(i).getOfferType().equals("Ricaricabile")) {
                    RicaricabiliArray.add(k1, offertaMobiles.get(i));
                    k1++;

                }

            }

            mostraBoxRicaricabili = true;

            if (RicaricabiliArray.size() == 0) {
                mostraBoxRicaricabili = false;
            }

            OffertaMobile o = new OffertaMobile();

            if (mostraBoxRicaricabili) {
                //MW aggiungo questi due oggetti all'Array per far apparire i due Box Riepilogo ricariche e Riepilogo Giga
                o.setOfferDescriptionUpper("null");
                AbbonamentiArray.add(AbbonamentiArray.size(), o);
                AbbonamentiArray.add(AbbonamentiArray.size(), o);
            }

            OffertaMobile premiumAdd = new OffertaMobile();
            premiumAdd.setSelectedNumSimStep3(0);
            premiumAdd.setOfferDescriptionUpper("Premium");
            OffertaMobile mondoAdd = new OffertaMobile();
            mondoAdd.setOfferDescriptionUpper("Top Mondo");
            mondoAdd.setSelectedNumSimStep3(0);

            RicaricabiliArray.add(RicaricabiliArray.size(), premiumAdd);
            RicaricabiliArray.add(RicaricabiliArray.size(), mondoAdd);

            boolean checkTopMondo = false;
            boolean checkpremium = false;
            //MW conrtollo se ci sono opzioni TopMondo o Premium associate a una ricaricabile
            for (int i = 0; i < offertaMobiles.size(); i++) {

                if (offertaMobiles.get(i).getOfferType().equals("Ricaricabile")) {

                    if (offertaMobiles.get(i).getOpzioneMobileArrayList().size() > 0) {

                        for (int k2 = 0; k2 < offertaMobiles.get(i).getOpzioneMobileArrayList().size(); k2++) {

                            if (offertaMobiles.get(i).getOpzioneMobileArrayList().get(k2).getOpzioneDesc().equals("Top Mondo")) {
                                RicaricabiliArray.get(RicaricabiliArray.size() - 1).setSelectedNumSimStep3(RicaricabiliArray.get(RicaricabiliArray.size() - 1).getSelectedNumSimStep3() + offertaMobiles.get(i).getOpzioneMobileArrayList().get(k2).getSelectedNumSimStep3());
                                checkTopMondo = true;
                            } else if (offertaMobiles.get(i).getOpzioneMobileArrayList().get(k2).getOpzioneDesc().equals("Premium")) {
                                RicaricabiliArray.get(RicaricabiliArray.size() - 2).setSelectedNumSimStep3(RicaricabiliArray.get(RicaricabiliArray.size() - 2).getSelectedNumSimStep3() + offertaMobiles.get(i).getOpzioneMobileArrayList().get(k2).getSelectedNumSimStep3());
                                checkpremium = true;
                            }
                        }

                    }

                }

            }

            if (!checkTopMondo && !checkpremium) {
                RicaricabiliArray.remove(RicaricabiliArray.size() - 1);
                RicaricabiliArray.remove(RicaricabiliArray.size() - 1);
            } else if (!checkTopMondo) {
                RicaricabiliArray.remove(RicaricabiliArray.size() - 1);
            } else if (!checkpremium) {
                RicaricabiliArray.remove(RicaricabiliArray.size() - 2);
            }
            while (RicaricabiliArray.size() < 5) {
                RicaricabiliArray.add(RicaricabiliArray.size(), o);
            }

            //MW recupero gli AddOn Giga associati ai piani ricaricabili da mostrare nella Recycler.
            // e mi calcolo il valore totale dei Giaga Acquistati
            int k2 = 0;
            int sim1GB = 0;
            int sim5GB = 0;
            int sim15GB = 0;
            int sim30GB = 0;
            int sim100GB = 0;
            boolean unoGb = true;
            boolean cinqueGb = true;
            boolean quindiciGb = true;
            boolean trentaGb = true;
            boolean centoGb = true;

            gigaAcquistati = 0;

            for (int i = 0; i < offertaMobiles.size(); i++) {
                for (int k3 = 0; k3 < offertaMobiles.get(i).getAddOnMobileArrayList().size(); k3++) {
                    if (offertaMobiles.get(i).getAddOnMobileArrayList().get(k3).getDescFascia().equals("1GB")) {
                        if (unoGb) {
                            sim1GB = sim1GB + offertaMobiles.get(i).getAddOnMobileArrayList().get(k3).getSelectedNumSimStep3();
                            gigaArray.get(0).setPrezzoFascia(offertaMobiles.get(i).getAddOnMobileArrayList().get(k3).getPrezzoFascia());
                            gigaArray.get(0).setSelectedNumSimStep3(sim1GB);
                            gigaArray.get(0).setDescFascia("1 GB");
                            gigaArray.get(0).setScontoGigaApplicato(offertaMobiles.get(i).getAddOnMobileArrayList().get(k3).getScontoGigaApplicato());
                            gigaAcquistati = gigaAcquistati + ((Float.valueOf(offertaMobiles.get(i).getAddOnMobileArrayList().get(k3).getPrezzoFascia())) * Integer.valueOf(offertaMobiles.get(i).getAddOnMobileArrayList().get(k3).getSelectedNumSimStep3()));
                            unoGb = false;
                        }
                    }
                    if (offertaMobiles.get(i).getAddOnMobileArrayList().get(k3).getDescFascia().equals("5GB")) {
                        if (cinqueGb) {
                            sim5GB = sim5GB + offertaMobiles.get(i).getAddOnMobileArrayList().get(k3).getSelectedNumSimStep3();
                            gigaArray.get(1).setSelectedNumSimStep3(sim5GB);
                            gigaArray.get(1).setPrezzoFascia(offertaMobiles.get(i).getAddOnMobileArrayList().get(k3).getPrezzoFascia());
                            gigaArray.get(1).setDescFascia("5 GB");
                            gigaArray.get(1).setScontoGigaApplicato(offertaMobiles.get(i).getAddOnMobileArrayList().get(k3).getScontoGigaApplicato());
                            gigaAcquistati = gigaAcquistati + ((Float.valueOf(offertaMobiles.get(i).getAddOnMobileArrayList().get(k3).getPrezzoFascia())) * Integer.valueOf(offertaMobiles.get(i).getAddOnMobileArrayList().get(k3).getSelectedNumSimStep3()));
                            cinqueGb = false;
                        }
                    }
                    if (offertaMobiles.get(i).getAddOnMobileArrayList().get(k3).getDescFascia().equals("15GB")) {
                        if (quindiciGb) {
                            sim15GB = sim1GB + offertaMobiles.get(i).getAddOnMobileArrayList().get(k3).getSelectedNumSimStep3();
                            gigaArray.get(2).setSelectedNumSimStep3(sim15GB);
                            gigaArray.get(2).setPrezzoFascia(offertaMobiles.get(i).getAddOnMobileArrayList().get(k3).getPrezzoFascia());
                            gigaArray.get(2).setDescFascia("15 GB");
                            gigaArray.get(2).setScontoGigaApplicato(offertaMobiles.get(i).getAddOnMobileArrayList().get(k3).getScontoGigaApplicato());
                            gigaAcquistati = gigaAcquistati + ((Float.valueOf(offertaMobiles.get(i).getAddOnMobileArrayList().get(k3).getPrezzoFascia())) * Integer.valueOf(offertaMobiles.get(i).getAddOnMobileArrayList().get(k3).getSelectedNumSimStep3()));
                            quindiciGb = false;
                        }
                    }
                    if (offertaMobiles.get(i).getAddOnMobileArrayList().get(k3).getDescFascia().equals("30GB")) {
                        if (trentaGb) {
                            sim30GB = sim30GB + offertaMobiles.get(i).getAddOnMobileArrayList().get(k3).getSelectedNumSimStep3();
                            gigaArray.get(3).setSelectedNumSimStep3(sim30GB);
                            gigaArray.get(3).setPrezzoFascia(offertaMobiles.get(i).getAddOnMobileArrayList().get(k3).getPrezzoFascia());
                            gigaArray.get(3).setDescFascia("30 GB");
                            gigaArray.get(3).setScontoGigaApplicato(offertaMobiles.get(i).getAddOnMobileArrayList().get(k3).getScontoGigaApplicato());
                            gigaAcquistati = gigaAcquistati + ((Float.valueOf(offertaMobiles.get(i).getAddOnMobileArrayList().get(k3).getPrezzoFascia())) * Integer.valueOf(offertaMobiles.get(i).getAddOnMobileArrayList().get(k3).getSelectedNumSimStep3()));
                            trentaGb = false;
                        }
                    }
                    if (offertaMobiles.get(i).getAddOnMobileArrayList().get(k3).getDescFascia().equals("100GB")) {
                        if (centoGb) {
                            sim100GB = sim100GB + offertaMobiles.get(i).getAddOnMobileArrayList().get(k3).getSelectedNumSimStep3();
                            gigaArray.get(4).setSelectedNumSimStep3(sim100GB);
                            gigaArray.get(4).setPrezzoFascia(offertaMobiles.get(i).getAddOnMobileArrayList().get(k3).getPrezzoFascia());
                            gigaArray.get(4).setDescFascia("100 GB");
                            gigaArray.get(4).setScontoGigaApplicato(offertaMobiles.get(i).getAddOnMobileArrayList().get(k3).getScontoGigaApplicato());
                            gigaAcquistati = gigaAcquistati + ((Float.valueOf(offertaMobiles.get(i).getAddOnMobileArrayList().get(k3).getPrezzoFascia())) * Integer.valueOf(offertaMobiles.get(i).getAddOnMobileArrayList().get(k3).getSelectedNumSimStep3()));
                            centoGb = false;
                        }
                    }
                }
            }
            preparogigaArray();


            //MW commentata vecchia pagina scont fatta da Luca Quaranta
//            populateMobileOfferList(offertaMobiles, null);
            //MW Popola la Recycler View con i piani abbinamento e i riepiloghi Ricarica e Giga
            populateListaRicaricabiliPaginaSconto(RicaricabiliArray, null);
            populateListaGigaPaginaSconto(RicaricabiliArray, null);
            populateRicaricabileMobileOfferList(AbbonamentiArray, null);

            checkFattibilita();


            if (isFirstOpen == 1 || isFirstOpen == 2) {
                Utils.showScontiDialog(getActivity(), isFirstOpen, mobileInternetCreateOfferStep4Fragment);
            }
            isFirstOpen = 0;

        } catch (JSONException e) {
            e.printStackTrace();
        }


        continuaButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Session.getInstance().getPratica().setStep(PraticheDAO.STEP5);
                PraticheDAO.updatePratica(getActivity().getBaseContext());


                Log.d("JSON PRATICA)", Session.getInstance().getJsonPratica().toString());
                if (isKOChecked || isGigaKOChecked) {
                    Utils.showScontiDialog(getActivity(), 0, mobileInternetCreateOfferStep4Fragment);
                } else {
                    goForward();
                }
            }
        });

        return view;
    }

    private void preparogigaArray() {
        for (int i = 4; i >= 0; i--) {

            if (gigaArray.get(i).getSelectedNumSimStep3() == 0) {
                gigaArray.remove(i);
            }

        }
        while (gigaArray.size() < 5) {

            AddOnMobile a = new AddOnMobile();
            gigaArray.add(gigaArray.size(), a);

        }
    }

    public void saveObjects() {
        try {
            JSONArray offerteRaggruppate = new JSONArray();
            for (OffertaMobile offertaMobile : offertaMobiles) {
                offerteRaggruppate.put(Utils.createOffertaMobile(offertaMobile));
            }
            Session.getInstance().getJsonPratica().getJSONObject("pratica").put("offerteRaggruppate", offerteRaggruppate);

            //store data on sqlLite
            PraticheDAO.updatePratica(this.getActivity().getBaseContext());


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void goForward() {

        saveObjects();
        if (Session.getInstance().getDealer().getDealerRole().compareTo("UserSales") != 0)
            ((MainActivity) getActivity()).goForward();
        else
            ((MainActivity) getActivity()).goForwardSelectOffer();
        //((MainActivity) getActivity()).goForward();
    }

    private void initMobileOfferList() {
        mLayoutManagerMobileOffer = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        mobileOfferRicRecyclerView.setLayoutManager(mLayoutManagerMobileOffer);
        mobileOfferRicRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mobileOfferRicRecyclerView.addItemDecoration(new HorizontalSpaceItemDecoration(12));
    }

    //MW vecchia pagina sconti fatta da Luca Quaranta
    private void populateMobileOfferList(ArrayList<OffertaMobile> offerteMobile, ArrayList<String> offerteList) {
        mobileOfferAdapter = new MobileOfferStep4Adapter(this, offerteMobile, null);
        mobileOfferRicRecyclerView.setAdapter(mobileOfferAdapter);
    }

    //Popolamento della Recycle della Pagina Sconti
    private void populateRicaricabileMobileOfferList(ArrayList<OffertaMobile> offerteMobile, ArrayList<String> offerteList) {
        mobileRicOfferAdapter = new MobileOfferRicStep4Adapter(this, offerteMobile, null, mobileRicListOfferAdapter, mobileGigaListOfferAdapter, mostraBoxRicaricabili);
        mobileOfferRicRecyclerView.setAdapter(mobileRicOfferAdapter);

    }

    //MW Popolamento della Recycler del riepilogo ricaricabili
    private void populateListaRicaricabiliPaginaSconto(ArrayList<OffertaMobile> offerteMobile, ArrayList<String> offerteList) {
        mobileRicListOfferAdapter = new MobileOfferRicListStep4Adapter(this, offerteMobile, null);
        //mobileOfferRicListRecyclerView.swapAdapter(mobileRicListOfferAdapter, false);
    }

    //MW Popolamento della Recycler del riepilogo Giga
    private void populateListaGigaPaginaSconto(ArrayList<OffertaMobile> offerteMobile, ArrayList<String> offerteList) {
        mobileGigaListOfferAdapter = new MobileOfferGigaListStep4Adapter(this, offerteMobile, null, gigaArray);
        //mobileOfferRicListRecyclerView.swapAdapter(mobileRicListOfferAdapter, false);
    }


    public int incrementOfferNumSim(OffertaMobile offertaMobile) {
        int selectedSim = 0;
//        try {
//            JSONArray pacchetti = Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONArray("pacchetti");
//            JSONObject pacchetto = pacchetti.getJSONObject(pacchettoVisibile - 1);
//            JSONArray offerte = pacchetto.getJSONArray("offerte");
//            JSONObject offerta = offerte.getJSONObject(offertaVisibile - 1);

        if (offertaMobile.getSelectedNumSimStep4() < offertaMobile.getSelectedNumSimStep3()) {
            offertaMobile.setSelectedNumSimStep4(offertaMobile.getSelectedNumSimStep4() + 1);
        }
        selectedSim = offertaMobile.getSelectedNumSimStep4();

//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//        checkValidationPacchetto();
        saveObjects();
        return selectedSim;
    }

    public int decrementOfferNumSim(OffertaMobile offertaMobile) {
        int selectedSim = 0;
//        try {
//            JSONArray pacchetti = Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONArray("pacchetti");
//            JSONObject pacchetto = pacchetti.getJSONObject(pacchettoVisibile - 1);
//            JSONArray offerte = pacchetto.getJSONArray("offerte");
//            JSONObject offerta = offerte.getJSONObject(offertaVisibile - 1);

        if (offertaMobile.getSelectedNumSimStep4() > 0) {
            offertaMobile.setSelectedNumSimStep4(offertaMobile.getSelectedNumSimStep4() - 1);
        }
        selectedSim = offertaMobile.getSelectedNumSimStep4();

//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//        checkValidationPacchetto();
        saveObjects();
        return selectedSim;
    }

    public String incrementScontoGiga(AddOnMobile gigaAddOn) {
        String sconto = gigaAddOn.getScontoGigaApplicato();
        Double oldsconto;
        String fasciaAddOn = gigaAddOn.getDescFascia().replace(" ", "");
        if (Double.valueOf(sconto.replace(",", ".")) < Double.valueOf(gigaAddOn.getPrezzoFascia().replace(",", "."))) {
            if (gigaAddOn.getScontoGigaApplicato().equals("")) {
                sconto = "0,50";
            } else {
                oldsconto = Double.valueOf(sconto.replace(",", "."));
                sconto = (String.valueOf(oldsconto + 0.50) + "0").replace(".", ",");

            }
        }
        //MW Mi memorizzo lo sconto aggiornato nella Pratica per mantenerlo in memoria
        for (int i = 0; i < offertaMobiles.size(); i++) {

            if (offertaMobiles.get(i).getOfferType().equals("Ricaricabile")) {
                for (int k = 0; k < offertaMobiles.get(i).getAddOnMobileArrayList().size(); k++) {

                    if (offertaMobiles.get(i).getAddOnMobileArrayList().get(k).getDescFascia().equals(fasciaAddOn)) {
                        offertaMobiles.get(i).getAddOnMobileArrayList().get(k).setScontoGigaApplicato(sconto);
                    }

                }
            }
        }

        saveObjects();
        isGigaKOChecked = true;
        return sconto;
    }

    public String decrementScontoGiga(AddOnMobile gigaAddOn) {
        String sconto = gigaAddOn.getScontoGigaApplicato();
        Double oldsconto;
        String fasciaAddOn = gigaAddOn.getDescFascia().replace(" ", "");
        if (gigaAddOn.getScontoGigaApplicato().equals("") || gigaAddOn.getScontoGigaApplicato().equals("0,00")) {
            sconto = "0,00";
        } else {
            oldsconto = Double.valueOf(gigaAddOn.getScontoGigaApplicato().replace(",", "."));
            sconto = (String.valueOf(oldsconto - 0.50) + "0").replace(".", ",");

        }

        //MW Mi memorizzo lo sconto aggiornato nella Pratica per mantenerlo in memoria
        for (int i = 0; i < offertaMobiles.size(); i++) {

            if (offertaMobiles.get(i).getOfferType().equals("Ricaricabile")) {
                for (int k = 0; k < offertaMobiles.get(i).getAddOnMobileArrayList().size(); k++) {

                    if (offertaMobiles.get(i).getAddOnMobileArrayList().get(k).getDescFascia().equals(fasciaAddOn)) {
                        offertaMobiles.get(i).getAddOnMobileArrayList().get(k).setScontoGigaApplicato(sconto);
                    }

                }
            }
        }

        saveObjects();
        if (sconto.equals("0,00")) {
            isGigaKOChecked = false;
        }
        return sconto;
    }

    public String incrementScontoRicaricabile(OffertaMobile offertaMobile) {
        String sconto = offertaMobile.getScontoApplicato();

        if (sconto.equals(String.valueOf(fasceScontiRicaricabili.getFasciaSconto1())))
            offertaMobile.setScontoApplicato(String.valueOf(fasceScontiRicaricabili.getFasciaSconto2()));
        else if (sconto.equals(String.valueOf(fasceScontiRicaricabili.getFasciaSconto2())))
            offertaMobile.setScontoApplicato(String.valueOf(fasceScontiRicaricabili.getFasciaSconto3()));
        else if (sconto.equals(String.valueOf(fasceScontiRicaricabili.getFasciaSconto3())))
            offertaMobile.setScontoApplicato(String.valueOf(fasceScontiRicaricabili.getFasciaSconto4()));
        else if (sconto.equals(String.valueOf(fasceScontiRicaricabili.getFasciaSconto4())))
            offertaMobile.setScontoApplicato(String.valueOf(fasceScontiRicaricabili.getFasciaSconto4()));


        saveObjects();
        return sconto;
    }

    public String decrementScontoRicaricabile(OffertaMobile offertaMobile) {
        String sconto = offertaMobile.getScontoApplicato();

        if (sconto.equals(String.valueOf(fasceScontiRicaricabili.getFasciaSconto1()))) {
            offertaMobile.setScontoApplicato(String.valueOf(fasceScontiRicaricabili.getFasciaSconto1()));
        } else if (sconto.equals(String.valueOf(fasceScontiRicaricabili.getFasciaSconto2()))) {
            offertaMobile.setScontoApplicato(String.valueOf(fasceScontiRicaricabili.getFasciaSconto1()));
        } else if (sconto.equals(String.valueOf(fasceScontiRicaricabili.getFasciaSconto3()))) {
            offertaMobile.setScontoApplicato(String.valueOf(fasceScontiRicaricabili.getFasciaSconto2()));
        } else if (sconto.equals(String.valueOf(fasceScontiRicaricabili.getFasciaSconto4())))
            offertaMobile.setScontoApplicato(String.valueOf(fasceScontiRicaricabili.getFasciaSconto3()));


        saveObjects();
        return sconto;
    }

    public String incrementSconto(OffertaMobile offertaMobile) {
        String sconto = offertaMobile.getScontoApplicato();

        for (int i = 0; i < offertaMobile.getPercentualeScontoArrayList().size(); i++) {
            if (offertaMobile.getPercentualeScontoArrayList().get(i).getPercentualeSconto().compareToIgnoreCase(sconto) == 0 && i < offertaMobile.getPercentualeScontoArrayList().size() - 1) {
                sconto = offertaMobile.getPercentualeScontoArrayList().get(i + 1).getPercentualeSconto();
                offertaMobile.setScontoApplicato(sconto);
                if (!offertaMobile.isScontoPenaleApplicato()) {
                    offertaMobile.setCanoneScontato(String.valueOf(Float.parseFloat(offertaMobile.getCanonePromo().replace(",", ".")) - Float.parseFloat(sconto.replace(",", ".")) / 100 * Float.parseFloat(offertaMobile.getCanonePromo().replace(",", "."))));
                } else {
                    offertaMobile.setCanoneScontato(String.valueOf(Float.parseFloat(offertaMobile.getCanonePromo().replace(",", ".")) - Float.parseFloat(offertaMobile.getPercentualeScontoPenale().replace(",", ".")) / 100 * Float.parseFloat(offertaMobile.getCanonePromo().replace(",", ".")) - Float.parseFloat(sconto.replace(",", ".")) / 100 * Float.parseFloat(offertaMobile.getCanonePromo().replace(",", "."))));
                }
                break;
            }
        }
        saveObjects();
        return sconto;
    }


    public String decrementSconto(OffertaMobile offertaMobile) {
        String sconto = offertaMobile.getScontoApplicato();

        for (int i = 0; i < offertaMobile.getPercentualeScontoArrayList().size(); i++) {
            if (offertaMobile.getPercentualeScontoArrayList().get(i).getPercentualeSconto().compareToIgnoreCase(sconto) == 0 && i > 0) {
                sconto = offertaMobile.getPercentualeScontoArrayList().get(i - 1).getPercentualeSconto();
                offertaMobile.setScontoApplicato(sconto);

                if (!offertaMobile.isScontoPenaleApplicato()) {
                    offertaMobile.setCanoneScontato(String.valueOf(Float.parseFloat(offertaMobile.getCanonePromo().replace(",", ".")) - Float.parseFloat(sconto.replace(",", ".")) / 100 * Float.parseFloat(offertaMobile.getCanonePromo().replace(",", "."))));
                } else {
                    offertaMobile.setCanoneScontato(String.valueOf(Float.parseFloat(offertaMobile.getCanonePromo().replace(",", ".")) - Float.parseFloat(offertaMobile.getPercentualeScontoPenale().replace(",", ".")) / 100 * Float.parseFloat(offertaMobile.getCanonePromo().replace(",", ".")) - Float.parseFloat(sconto.replace(",", ".")) / 100 * Float.parseFloat(offertaMobile.getCanonePromo().replace(",", "."))));
                }

                break;
            }
        }
        saveObjects();
        return sconto;
    }

    public boolean calcolaScontoPenale(OffertaMobile offertaMobile) {
        boolean isScontoPenaleApplicato = !offertaMobile.isScontoPenaleApplicato();

        if (!isScontoPenaleApplicato) {
            if (!TextUtils.isEmpty(offertaMobile.getCanonePromo())) {
                offertaMobile.setCanoneScontato(String.valueOf(Float.parseFloat(offertaMobile.getCanonePromo().replace(",", ".")) - Float.parseFloat(offertaMobile.getScontoApplicato().replace(",", ".")) / 100 * Float.parseFloat(offertaMobile.getCanonePromo().replace(",", "."))));
            } else {
                offertaMobile.setCanoneScontato(String.valueOf(Float.parseFloat(offertaMobile.getCanoneMensile().replace(",", ".")) - Float.parseFloat(offertaMobile.getScontoApplicato().replace(",", ".")) / 100 * Float.parseFloat(offertaMobile.getCanoneMensile().replace(",", "."))));
            }
        } else {
            if (!TextUtils.isEmpty(offertaMobile.getCanonePromo())) {
                offertaMobile.setCanoneScontato(String.valueOf(Float.parseFloat(offertaMobile.getCanonePromo().replace(",", ".")) - Float.parseFloat(offertaMobile.getPercentualeScontoPenale().replace(",", ".")) / 100 * Float.parseFloat(offertaMobile.getCanonePromo().replace(",", ".")) - Float.parseFloat(offertaMobile.getScontoApplicato().replace(",", ".")) / 100 * Float.parseFloat(offertaMobile.getCanonePromo().replace(",", "."))));
            } else {
                offertaMobile.setCanoneScontato(String.valueOf(Float.parseFloat(offertaMobile.getCanoneMensile().replace(",", ".")) - Float.parseFloat(offertaMobile.getPercentualeScontoPenale().replace(",", ".")) / 100 * Float.parseFloat(offertaMobile.getCanoneMensile().replace(",", ".")) - Float.parseFloat(offertaMobile.getScontoApplicato().replace(",", ".")) / 100 * Float.parseFloat(offertaMobile.getCanoneMensile().replace(",", "."))));
            }
        }

        return isScontoPenaleApplicato;
    }

    public void checkFattibilita() {

        int checkTotSim = checkTotSim();
        int checkNumSim = checkNumSim(checkTotSim);

        numSim.setText(String.valueOf(checkTotSim));
        numSimRic.setText(String.valueOf(checkTotSimRicaricabileStep3()));
        numSimAbb.setText(String.valueOf(checkTotSimAbbonamentoStep3()));

        float spesapromo = 0;
        float spesasconti = 0;
        spesapromoRic = 0;
        float spesascontiRic = 0;
        float spesapromoAbb = 0;
        float spesascontiAbb = 0;
        float scontoSuRicaricabile = 0;
        float scontoSuGiga = 0;
        float spesaRicaricabileOpzioni = 0;
        boolean daCalcolare = true;
        for (OffertaMobile offertaMobile : offertaMobiles) {

            if (offertaMobile.getOfferType().equals("Ricaricabile")) {
                //MW Calcolo lo sconto applicato a ogni piano ricaricabile
                if (!TextUtils.isEmpty(offertaMobile.getScontoApplicato())) {
                    scontoSuRicaricabile = scontoSuRicaricabile + ((Float.valueOf(offertaMobile.getScontoApplicato())) * offertaMobile.getSelectedNumSimStep3());
                }
                //MW Calcolo lo sconto applicato a ogni pacchetto Giga associato a RIcaricabile

                for (int i = 0; i < offertaMobile.getAddOnMobileArrayList().size(); i++) {
                    if (!TextUtils.isEmpty(offertaMobile.getAddOnMobileArrayList().get(i).getScontoGigaApplicato())) {
                        if (daCalcolare) {
                            scontoSuGiga = scontoSuGiga + (Float.valueOf(offertaMobile.getAddOnMobileArrayList().get(i).getScontoGigaApplicato().replace(",", "."))) * offertaMobile.getAddOnMobileArrayList().get(i).getSelectedNumSimStep3();

                        }
                    }
                }

                if (offertaMobile.getSelectedNumSimStep4() > 0) {
                    if (!TextUtils.isEmpty(offertaMobile.getCanonePromo())) {
                        spesapromoRic += Float.valueOf(offertaMobile.getCanonePromo().replace(",", ".")) * offertaMobile.getSelectedNumSimStep4();
                        if (!offertaMobile.isScontoPenaleApplicato()) {
                            spesascontiRic += (Float.valueOf(offertaMobile.getCanonePromo().replace(",", ".")) - (Float.valueOf(offertaMobile.getCanonePromo().replace(",", ".")) * Float.valueOf(offertaMobile.getScontoApplicato().replace(",", ".")) / 100)) * offertaMobile.getSelectedNumSimStep4();
                        } else {
                            spesascontiRic += (Float.valueOf(offertaMobile.getCanonePromo().replace(",", ".")) - Float.parseFloat(offertaMobile.getPercentualeScontoPenale().replace(",", ".")) / 100 * Float.parseFloat(offertaMobile.getCanonePromo().replace(",", ".")) - (Float.valueOf(offertaMobile.getCanonePromo().replace(",", ".")) * Float.valueOf(offertaMobile.getScontoApplicato().replace(",", ".")) / 100)) * offertaMobile.getSelectedNumSimStep4();
                        }
                    } else {
                        spesapromoRic += Float.valueOf(offertaMobile.getCanoneMensile().replace(",", ".")) * offertaMobile.getSelectedNumSimStep4();
                        if (!offertaMobile.isScontoPenaleApplicato()) {
                            spesascontiRic += (Float.valueOf(offertaMobile.getCanoneMensile().replace(",", ".")) - (Float.valueOf(offertaMobile.getCanoneMensile().replace(",", ".")) * Float.valueOf(offertaMobile.getScontoApplicato().replace(",", ".")) / 100)) * offertaMobile.getSelectedNumSimStep4();
                        } else {
                            spesascontiRic += (Float.valueOf(offertaMobile.getCanoneMensile().replace(",", ".")) - Float.parseFloat(offertaMobile.getPercentualeScontoPenale().replace(",", ".")) / 100 * Float.parseFloat(offertaMobile.getCanoneMensile().replace(",", ".")) - (Float.valueOf(offertaMobile.getCanoneMensile().replace(",", ".")) * Float.valueOf(offertaMobile.getScontoApplicato().replace(",", ".")) / 100)) * offertaMobile.getSelectedNumSimStep4();
                        }

                    }
                }
                //MW mi calcolo il valore delle Opzioni TopMondo e Premium per la Pagina Sconti
                if (offertaMobile.getOpzioneMobileArrayList().size() > 0) {

                    for (OpzioneMobile opzioneMobile : offertaMobile.getOpzioneMobileArrayList()) {
                        if (!opzioneMobile.getCanoneMensile().equals("")) {
                            if (opzioneMobile.getOpzioneDesc().equals("Top Mondo") || opzioneMobile.getOpzioneDesc().equals("Premium"))
                                spesaRicaricabileOpzioni += Float.valueOf(opzioneMobile.getCanoneMensile().replace(",", ".")) * opzioneMobile.getSelectedNumSimStep3();

                        }
                    }

                }
                daCalcolare = false;
            } else {
                if (offertaMobile.getSelectedNumSimStep4() > 0) {
                    if (!TextUtils.isEmpty(offertaMobile.getCanonePromo())) {
                        spesapromoAbb += Float.valueOf(offertaMobile.getCanonePromo().replace(",", ".")) * offertaMobile.getSelectedNumSimStep4();
                        if (!offertaMobile.isScontoPenaleApplicato()) {
                            spesascontiAbb += (Float.valueOf(offertaMobile.getCanonePromo().replace(",", ".")) - (Float.valueOf(offertaMobile.getCanonePromo().replace(",", ".")) * Float.valueOf(offertaMobile.getScontoApplicato().replace(",", ".")) / 100)) * offertaMobile.getSelectedNumSimStep4();
                        } else {
                            spesascontiAbb += (Float.valueOf(offertaMobile.getCanonePromo().replace(",", ".")) - Float.parseFloat(offertaMobile.getPercentualeScontoPenale().replace(",", ".")) / 100 * Float.parseFloat(offertaMobile.getCanonePromo().replace(",", ".")) - (Float.valueOf(offertaMobile.getCanonePromo().replace(",", ".")) * Float.valueOf(offertaMobile.getScontoApplicato().replace(",", ".")) / 100)) * offertaMobile.getSelectedNumSimStep4();
                        }
                    } else {
                        spesapromoAbb += Float.valueOf(offertaMobile.getCanoneMensile().replace(",", ".")) * offertaMobile.getSelectedNumSimStep4();
                        if (!offertaMobile.isScontoPenaleApplicato()) {
                            spesascontiAbb += (Float.valueOf(offertaMobile.getCanoneMensile().replace(",", ".")) - (Float.valueOf(offertaMobile.getCanoneMensile().replace(",", ".")) * Float.valueOf(offertaMobile.getScontoApplicato().replace(",", ".")) / 100)) * offertaMobile.getSelectedNumSimStep4();
                        } else {
                            spesascontiAbb += (Float.valueOf(offertaMobile.getCanoneMensile().replace(",", ".")) - Float.parseFloat(offertaMobile.getPercentualeScontoPenale().replace(",", ".")) / 100 * Float.parseFloat(offertaMobile.getCanoneMensile().replace(",", ".")) - (Float.valueOf(offertaMobile.getCanoneMensile().replace(",", ".")) * Float.valueOf(offertaMobile.getScontoApplicato().replace(",", ".")) / 100)) * offertaMobile.getSelectedNumSimStep4();
                        }

                    }
                }
            }

        }
        Float spesaScontoRic = spesapromoRic + gigaAcquistati + spesaRicaricabileOpzioni - scontoSuRicaricabile - scontoSuGiga;
        Float spesaPromoRic = spesapromoRic + gigaAcquistati + spesaRicaricabileOpzioni;

        spesapromoRicTot = spesapromoRic + spesaRicaricabileOpzioni;
        spesapromoRic = spesapromoRic + gigaAcquistati + spesaRicaricabileOpzioni - scontoSuRicaricabile;
        spesascontiRic = spesascontiRic + gigaAcquistati + spesaRicaricabileOpzioni;
        spesapromo = spesapromoAbb + spesapromoRic;

        //spesasconti = spesascontiAbb+spesascontiRic;
        spesasconti = spesascontiAbb + spesaScontoRic;

        spesaPromo.setText(String.format("%.2f", spesapromo) + "€");


        abbonamento_totale.setText(String.format("%.2f", spesapromoAbb) + "€");


        abbonamento_totale_sconto.setText(String.format("%.2f", spesascontiAbb) + "€");
        ricaricabile_totale.setText(String.format("%.2f", spesaPromoRic) + "€");
        ricaricabile_totale_sconto.setText(String.format("%.2f", spesaScontoRic) + "€");
        spesaScontata.setText(String.format("%.2f", spesasconti) + "€");
        //ricaricabile_totale_sconto.setText(String.format("%.2f", spesascontiRic) + "€");

        if (checkTotSim == 0) {
//            fattibilita.setText("");
//            fattibilita2.setVisibility(View.INVISIBLE);
        } else if (checkNumSim < 0) {
            isFirstOpen = 2;
            isKOChecked = true;
//            fattibilita.setText("Superato numero max SIM complessivo");
//
//            fattibilita2.setVisibility(View.VISIBLE);
        } else if (checkNumSim == 0) {
            isFirstOpen = 1;

//            isKOChecked = true;
//            fattibilita.setText("Sottosoglia");
//            fattibilita2.setText("Sottosoglia");
//            fattibilita2.setVisibility(View.VISIBLE);
        }

        {
            isKOChecked = false;
            for (OffertaMobile offertaMobile : offertaMobiles) {
                if (offertaMobile.getSelectedNumSimStep4() > 0 && offertaMobile.getAutorizzabilita().compareTo(Constants.TariffPlanPreAutirization.OVER_MAX) == 0) {
                    isKOChecked = true;
                }
            }
            for (OffertaMobile offertaMobile : offertaMobiles) {
                if (offertaMobile.getAutorizzabilita().equals("Non Pre-Autorizzato")) {
                    isKOChecked = true;
                }
            }
        }
    }

    public Boolean checkAutorizationRicaricabili(OffertaMobile offertaMobile) {
        int checkTotSim = checkTotSimRicaricabili();
        int sconto = Integer.valueOf(offertaMobile.getScontoApplicato());
        int scontoApplicato = sconto * checkTotSim;
        int fascia;
        int boa;

        boolean autorizzato = true;

        if (fasceScontiRicaricabili.getFasciaSpesa1() > spesapromoRic) {
            fascia = 1;
            boa = 1;
        } else if (fasceScontiRicaricabili.getFasciaSpesa2() > spesapromoRic) {
            fascia = 2;
            boa = 2;
        } else if (fasceScontiRicaricabili.getFasciaSpesa3() > spesapromoRic) {
            fascia = 3;
            boa = 3;
        } else {
            fascia = 4;
            boa = 4;
        }

        if (fascia == 1 && boa == 1 && sconto == 0 || fascia == 2 && boa == 2 && sconto < 3 || fascia == 3 && boa == 3 && sconto < 5 || fascia == 4 && boa == 4 && sconto < 7) {
            autorizzato = true;
            for (OffertaMobile of : offertaMobiles) {
                if (of.getOfferType().equals("Ricaricabile")) {
                    if (of.getOfferDescriptionUpper().equals(offertaMobile.getOfferDescriptionUpper())) {

                        of.setAutorizzabilita("Pre-Autorizzato");

                    }
                }
            }
        } else {
            autorizzato = false;
            for (OffertaMobile of : offertaMobiles) {
                if (of.getOfferType().equals("Ricaricabile")) {
                    if (of.getOfferDescriptionUpper().equals(offertaMobile.getOfferDescriptionUpper())) {

                        of.setAutorizzabilita("Non Pre-Autorizzato");

                    }
                }
            }
        }

        /*    if(fasceScontiRicaricabili.getFasciaSpesa1()>spesapromoRic && fasceScontiRicaricabili.getFasciaSconto1()<sconto){
                autorizzato=false;
            } else if(fasceScontiRicaricabili.getFasciaSpesa2()>spesapromoRic && fasceScontiRicaricabili.getFasciaSconto2()<sconto){
                autorizzato=false;
        } else if(fasceScontiRicaricabili.getFasciaSpesa3()>spesapromoRic && fasceScontiRicaricabili.getFasciaSconto3()<sconto){
            autorizzato=false;
        } else if(sconto==0){
            autorizzato=true;
        }*/

        //int checkNumSim = checkNumSim(checkTotSim);
        float checkValoreSpesaMensile = checkValoreSpesaMensile();
        int checkValoreSpesa = checkValoreSpesa(checkValoreSpesaMensile);
        int checkFascia = checkFascia(checkValoreSpesa, checkTotSim);
        saveObjects();
        isKOChecked = autorizzato;
        return autorizzato;
    }


    public String checkAutorization(OffertaMobile offertaMobile) {
        int checkTotSim = checkTotSim();
        int checkNumSim = checkNumSim(checkTotSim);
        float checkValoreSpesaMensile = checkValoreSpesaMensile();
        int checkValoreSpesa = checkValoreSpesa(checkValoreSpesaMensile);
        int checkFascia = checkFascia(checkValoreSpesa, checkNumSim);

        if (offertaMobile.getSelectedNumSimStep4() == 0) {
            return Constants.TariffPlanPreAutirization.NULL;
        } else {
            if (checkC17(offertaMobile, checkValoreSpesa, checkFascia) == 0) {
                return Constants.TariffPlanPreAutirization.OVER_MAX;
            } else {
                return Constants.TariffPlanPreAutirization.OK;
            }

        }
    }

    private int checkC17(OffertaMobile offertaMobile, int checkValoreSpesa, int checkFascia) {
        if (Float.valueOf(offertaMobile.getScontoApplicato().replace(",", ".")) == 0) {
            return 1;
        } else {
            if (checkValoreSpesa <= 0) {
                return 0;
            } else {
//                if (!tariffPlan.isRepaymentPenalties()){
                return checkB37(offertaMobile, checkFascia);
//                } else {
//                    return checkK37(tariffPlan);
//                }
            }
        }
    }

    private int checkB37(OffertaMobile offertaMobile, int checkFascia) {
        if (Float.valueOf(offertaMobile.getScontoApplicato().replace(",", ".")) <= checkB36(offertaMobile, checkFascia)) {
            return 1;
        } else {
            return 0;
        }
    }

    private float checkB36(OffertaMobile offertaMobile, int checkFascia) {
        if (checkFascia == 2) {
            float max = -1f;
            for (PercentualeSconto percentualeSconto : offertaMobile.getPercentualeScontoArrayList()) {
                if (Float.valueOf(percentualeSconto.getPercentualeSconto().replace(",", ".")) > max) {
                    max = Float.valueOf(percentualeSconto.getPercentualeSconto().replace(",", "."));
                }
            }
            return max;
        } else {
            float min = 1000000f;
            for (PercentualeSconto percentualeSconto : offertaMobile.getPercentualeScontoArrayList()) {
                if (Float.valueOf(percentualeSconto.getPercentualeSconto().replace(",", ".")) != 0 && Float.valueOf(percentualeSconto.getPercentualeSconto().replace(",", ".")) < min) {
                    min = Float.valueOf(percentualeSconto.getPercentualeSconto().replace(",", "."));
                }
            }
            return min;
        }
    }

    private int checkTotSim() {
        int sum = 0;
        for (OffertaMobile offertaMobile : offertaMobiles) {
            sum += offertaMobile.getSelectedNumSimStep4();
        }
        return sum;
    }

    private int checkTotSimRicaricabili() {
        int sum = 0;
        for (OffertaMobile offertaMobile : offertaMobiles) {
            if (offertaMobile.getOfferType().equals("Ricaricabile"))
                sum += offertaMobile.getSelectedNumSimStep4();
        }
        return sum;
    }

    private int checkTotSimRicaricabileStep3() {
        int sum = 0;
        for (OffertaMobile offertaMobile : offertaMobiles) {
            if (offertaMobile.getOfferType().equals("Ricaricabile")) {
                sum += offertaMobile.getSelectedNumSimStep3();
            }
        }
        return sum;
    }

    private int checkTotSimAbbonamentoStep3() {
        int sum = 0;
        for (OffertaMobile offertaMobile : offertaMobiles) {
            if (offertaMobile.getOfferType().equals("Abbonamento")) {
                sum += offertaMobile.getSelectedNumSimStep3();
            }
        }
        return sum;
    }

    private int checkNumSim(int totSelectedSim) {
        if (totSelectedSim > fasceSconti.getFasciaSim3()) {
            return -1;
        } else if (totSelectedSim > fasceSconti.getFasciaSim2()) {
            return 2;
        } else if (totSelectedSim > fasceSconti.getFasciaSim1()) {
            return 1;
        } else {
            return 0;
        }
    }

    private float checkValoreSpesaMensile() {
        int valoreSpesaMensile = 0;
        for (OffertaMobile offertaMobile : offertaMobiles) {
            valoreSpesaMensile += (!TextUtils.isEmpty(offertaMobile.getCanonePromo()) ? Float.valueOf(offertaMobile.getCanonePromo().replace(",", ".")) : Float.valueOf(offertaMobile.getCanoneMensile().replace(",", "."))) * offertaMobile.getSelectedNumSimStep4();
        }
        return valoreSpesaMensile;
    }

    private int checkValoreSpesa(float valoreSpesaMensile) {
        if (valoreSpesaMensile >= fasceSconti.getFasciaSpesa2()) {
            return 2;
        } else if (valoreSpesaMensile >= fasceSconti.getFasciaSpesa1()) {
            return 1;
        } else {
            return 0;
        }
    }

    private int checkFascia(int checkValoreSpesa, int checkNumSim) {
        if (checkValoreSpesa < checkNumSim) {
            return checkValoreSpesa;
        } else {
            return checkNumSim;
        }
    }

}
