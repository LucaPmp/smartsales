package it.wind.smartsales.fragments.wireline;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.NonNull;

import java.util.ArrayList;

import it.wind.smartsales.entities.OffertaFisso;
import it.wind.smartsales.entities.OpzioneFisso;
import it.wind.smartsales.entities.Terminale;

public abstract class WirelineInnerFragment extends Fragment {

    public static final String OFFER = "offer";
    public static final String TERMINALS = "terminals";
    public static final String OPTIONS = "options";

    protected WirelineInternetOfferFragment listener;
    protected OffertaFisso offer;
    protected ArrayList<Terminale> terminals;
    private ArrayList<OpzioneFisso> options;

    abstract void enableContinue();

    @Override

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        terminals = new ArrayList<>();
        options = new ArrayList<>();
        if (getArguments() != null) {
            this.offer = (OffertaFisso) getArguments().get(OFFER);
            ArrayList<Terminale> terminales = (ArrayList<Terminale>) getArguments().get(TERMINALS);
            if (terminales != null)
                this.terminals = terminales;
            ArrayList<OpzioneFisso> opzioneFissos = (ArrayList<OpzioneFisso>) getArguments().get(OPTIONS);
            if (opzioneFissos != null)
                this.options = opzioneFissos;
        }
    }

    @NonNull
    protected static Bundle createArguments(OffertaFisso offer) {
        return createArguments(offer, null, null);
    }

    @NonNull
    protected static Bundle createArguments(OffertaFisso offer, ArrayList<Terminale> terminals, ArrayList<OpzioneFisso> options) {
        Bundle args = new Bundle();
        if (offer != null)
            args.putSerializable(OFFER, offer);
        if (terminals != null)
            args.putSerializable(TERMINALS, terminals);
        if (options != null)
            args.putSerializable(OPTIONS, options);
        return args;
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        listener = (WirelineInternetOfferFragment) getParentFragment();
    }

    public abstract int getPosition();

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    public String getOfferType() {
        return offer != null ? offer.getOfferType() : "";
    }

    public int getOfferId() {
        return offer != null ? offer.getIdOfferta() : -1;
    }

    public OffertaFisso getOffer() {
        return offer;
    }

    public ArrayList<Terminale> getTerminals() {
        return terminals;
    }

    public ArrayList<OpzioneFisso> getOptions() {
        return options;
    }

    public void addTerminal(Terminale t) {
        terminals.add(t);
    }

    public interface ContinueButtonInteraction {
        void setEnabled(boolean isSelected);
    }

}