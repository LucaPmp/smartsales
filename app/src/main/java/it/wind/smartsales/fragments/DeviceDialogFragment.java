package it.wind.smartsales.fragments;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;

import it.wind.smartsales.R;
import it.wind.smartsales.adapter.DeviceAdapter;
import it.wind.smartsales.entities.OffertaMobile;
import it.wind.smartsales.entities.Terminale;

/**
 * Created by luca.quaranta on 03/12/2015.
 */
public class DeviceDialogFragment extends DialogFragment {

    public static final String KEY = "LIST_NUMBER_KEY";

    int listNumber;
    private OffertaMobile offertaMobile;

    private RecyclerView deviceRecyclerView;
    private RecyclerView.LayoutManager mLayoutManagerDevice;
    private RecyclerView.Adapter deviceAdapter;

    private Button indietroButton;
    private MobileInternetCreateOfferStep8Fragment mobileInternetCreateOfferStep8Fragment;

    public DeviceDialogFragment() {
    }

    @SuppressLint("ValidFragment")
    public DeviceDialogFragment(MobileInternetCreateOfferStep8Fragment mobileInternetCreateOfferStep8Fragment, OffertaMobile offertaMobile, int listNumber) {
        this.mobileInternetCreateOfferStep8Fragment = mobileInternetCreateOfferStep8Fragment;
        this.offertaMobile = offertaMobile;
        this.listNumber = listNumber;
    }

    private void initializeComponents(View view) {
        indietroButton = (Button) view.findViewById(R.id.indietro_button);

        deviceRecyclerView = (RecyclerView) view.findViewById(R.id.device_recycler_view);


        initDeviceList();
    }

    private void initDeviceList() {
        deviceRecyclerView.setHasFixedSize(true);
        mLayoutManagerDevice = new LinearLayoutManager(getActivity());
        deviceRecyclerView.setLayoutManager(mLayoutManagerDevice);
        deviceRecyclerView.setItemAnimator(new DefaultItemAnimator());
    }

    private void populateDeviceList() {

        if (listNumber == 1){
            deviceAdapter = new DeviceAdapter(this, offertaMobile, offertaMobile.getTerminaliToShow());
        } else if (listNumber == 2){
            deviceAdapter = new DeviceAdapter(this, offertaMobile, offertaMobile.getTerminaliToShow2());
        }
        deviceRecyclerView.setAdapter(deviceAdapter);
    }

    public void onDeviceClicked(Terminale terminale){
        if (listNumber == 1){
            offertaMobile.setTerminale1(terminale);
        } else if (listNumber == 2){
            offertaMobile.setTerminale2(terminale);
        }
        mobileInternetCreateOfferStep8Fragment.updateDeviceList(offertaMobile);
        dismiss();
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    @Override
    public void onResume() {
        super.onResume();
        int width = getResources().getDimensionPixelSize(R.dimen.fragment_dialog_device_width);
        getDialog().getWindow().setLayout(width, ViewGroup.LayoutParams.WRAP_CONTENT);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_dialog_device, container, false);

        initializeComponents(view);

        populateDeviceList();

        indietroButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        return view;
    }

}
