package it.wind.smartsales.fragments.selectoffer;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import it.wind.smartsales.R;
import it.wind.smartsales.activity.MainActivity;
import it.wind.smartsales.commons.Constants;
import it.wind.smartsales.commons.Utils;
import it.wind.smartsales.customviews.AspectRatioView;
import it.wind.smartsales.entities.Session;
import it.wind.smartsales.fragments.MobileInternetCreateOfferStep3Fragment;
import it.wind.smartsales.fragments.MobileInternetCreateOfferStep4Fragment;
import it.wind.smartsales.fragments.MobileInternetCreateOfferStep5Fragment;
import it.wind.smartsales.fragments.MobileInternetCreateOfferStep6Fragment;
import it.wind.smartsales.fragments.MobileInternetCreateOfferStep7Fragment;
import it.wind.smartsales.fragments.MobileInternetCreateOfferStep8Fragment;
import it.wind.smartsales.fragments.MobileInternetCreateOfferStep9Fragment;

/**
 * Created by luca.quaranta on 10/02/2016.
 */
public class MobileInternetSelectOffer extends Fragment {

    private static final String STEP_1 = "Configura la tua Offerta";
    private static final String STEP_1_BACK = "Home";
    private static final String STEP_2 = "Scegli la tua Tariffa";
    private static final String STEP_2_BACK = "Configura";
    private static final String STEP_3 = "Device e Opzioni";
    private static final String STEP_3_BACK = "Tariffa";
    private static final String STEP_4 = "Applica Sconto";
    private static final String STEP_4_BACK = "Device e Opzioni";
    private static final String STEP_5 = "Riepilogo Ordine";
    private static final String STEP_5_BACK = "Sconti";
    private static final String STEP_6 = "Inserimento Dati";
    private static final String STEP_6_BACK = "Riepilogo";
    private static final String STEP_7 = "Pagamento";
    private static final String STEP_7_BACK = "Informazioni";
    private static final String STEP_8 = "Configura le tue SIM";
    private static final String STEP_8_BACK = "Pagamento";
    private static final String STEP_9 = "Invio";
    private static final String STEP_9_BACK = "Configura le tue SIM";
    private static final String STEP_DRAFT = "Bozze";


    private MobileInternetSelectOfferStep2Fragment mobileInternetCreateOfferStep2Fragment;
    private MobileInternetCreateOfferStep3Fragment mobileInternetCreateOfferStep3Fragment;
    private MobileInternetCreateOfferStep4Fragment mobileInternetCreateOfferStep4Fragment;
    private MobileInternetCreateOfferStep5Fragment mobileInternetCreateOfferStep5Fragment;
    private MobileInternetCreateOfferStep6Fragment mobileInternetCreateOfferStep6Fragment;
    private MobileInternetCreateOfferStep7Fragment mobileInternetCreateOfferStep7Fragment;
    private MobileInternetCreateOfferStep8Fragment mobileInternetCreateOfferStep8Fragment;
    private MobileInternetCreateOfferStep9Fragment mobileInternetCreateOfferStep9Fragment;

    private LinearLayout backButton;
    private TextView backPage, subheaderTitle;
    private AspectRatioView noteIcon, mailIcon;



    private void initializeComponents(View view) {
        backButton = (LinearLayout) view.findViewById(R.id.backButton);
        backPage = (TextView) view.findViewById(R.id.back_page);
        subheaderTitle = (TextView) view.findViewById(R.id.subheader_title);
        noteIcon = (AspectRatioView) view.findViewById(R.id.note_icon);
        mailIcon = (AspectRatioView) view.findViewById(R.id.mail_icon);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_mobile_internet_create_offer, container, false);
        initializeComponents(view);

        goToMobileInternetSelectOfferStep2Fragment();

        updateNote();

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goBack();
            }
        });

        mailIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.showMailDialog(getActivity(),mobileInternetCreateOfferStep5Fragment);
            }
        });

        noteIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.showNoteDialog(getActivity(), Session.getInstance().getPratica().getNote());
            }
        });

        return view;
    }

    public void updateNote(){
        if (!TextUtils.isEmpty(Session.getInstance().getPratica().getNote())){
            noteIcon.setBackground(getActivity().getResources().getDrawable(R.drawable.icon_note_full));
        } else {
            noteIcon.setBackground(getActivity().getResources().getDrawable(R.drawable.icon_note));
        }
    }

    public void goBack() {
        switch (subheaderTitle.getText().toString()) {

            case STEP_DRAFT:

            case STEP_1:
                ((MainActivity) getActivity()).goToPage(Constants.Pages.PAGE_HOME_ADMIN);
                ((MainActivity) getActivity()).setNavIcon(0);
                break;

            case STEP_2:
                ((MainActivity) getActivity()).goToPage(Constants.Pages.PAGE_HOME_ADMIN);
                ((MainActivity) getActivity()).setNavIcon(0);
                break;

            case STEP_3:
                goToMobileInternetSelectOfferStep2Fragment();
                ((MainActivity) getActivity()).setNavIcon(1);
                subheaderTitle.setText(STEP_2);
                backPage.setText(STEP_2_BACK);
                break;

            case STEP_4:
                goToMobileInternetSelectOfferStep3Fragment();
                ((MainActivity) getActivity()).setNavIcon(1);
                subheaderTitle.setText(STEP_3);
                backPage.setText(STEP_3_BACK);
                break;

            case STEP_5:

                if (!Session.getInstance().getJsonPratica().optBoolean("isFuoriStandard")) {
                    goToMobileInternetSelectOfferStep3Fragment();
                    ((MainActivity) getActivity()).setNavIcon(1);
                    mailIcon.setVisibility(View.GONE);
                    subheaderTitle.setText(STEP_3);
                    backPage.setText(STEP_3_BACK);
                    break;
                } else {
                    goToMobileInternetSelectOfferStep4Fragment();
                    ((MainActivity) getActivity()).setNavIcon(1);
                    mailIcon.setVisibility(View.GONE);
                    subheaderTitle.setText(STEP_4);
                    backPage.setText(STEP_4_BACK);
                    break;
                }

            case STEP_6:
                goToMobileInternetSelectOfferStep5Fragment();
                ((MainActivity) getActivity()).setNavIcon(2);
                mailIcon.setVisibility(View.VISIBLE);
                subheaderTitle.setText(STEP_5);
                backPage.setText(STEP_5_BACK);
                break;

            case STEP_7:
                goToMobileInternetSelectOfferStep6Fragment();
                ((MainActivity) getActivity()).setNavIcon(3);
                mailIcon.setVisibility(View.GONE);
                subheaderTitle.setText(STEP_6);
                backPage.setText(STEP_6_BACK);
                break;

            case STEP_8:
                goToMobileInternetSelectOfferStep7Fragment();
                ((MainActivity) getActivity()).setNavIcon(4);
                mailIcon.setVisibility(View.GONE);
                subheaderTitle.setText(STEP_7);
                backPage.setText(STEP_7_BACK);
                break;

            case STEP_9:
                goToMobileInternetSelectOfferStep8Fragment();
                ((MainActivity) getActivity()).setNavIcon(5);
                mailIcon.setVisibility(View.GONE);
                subheaderTitle.setText(STEP_8);
                backPage.setText(STEP_8_BACK);
                break;
        }
    }

    public void goForward() {
        switch (subheaderTitle.getText().toString()) {

            case STEP_1:
                goToMobileInternetSelectOfferStep2Fragment();
                ((MainActivity) getActivity()).setNavIcon(1);
                subheaderTitle.setText(STEP_2);
                backPage.setText(STEP_2_BACK);
                break;

            case STEP_2:
                goToMobileInternetSelectOfferStep3Fragment();
                ((MainActivity) getActivity()).setNavIcon(1);
                subheaderTitle.setText(STEP_3);
                backPage.setText(STEP_3_BACK);
                break;

            case STEP_3:
                if (!Session.getInstance().getJsonPratica().optBoolean("isFuoriStandard")) {
                    goToMobileInternetSelectOfferStep5Fragment();
                    ((MainActivity) getActivity()).setNavIcon(2);
                    mailIcon.setVisibility(View.VISIBLE);
                    subheaderTitle.setText(STEP_5);
                    backPage.setText(STEP_4_BACK);
                    break;
                } else {
                    goToMobileInternetSelectOfferStep4Fragment();
                    ((MainActivity) getActivity()).setNavIcon(1);
                    mailIcon.setVisibility(View.GONE);
                    subheaderTitle.setText(STEP_4);
                    backPage.setText(STEP_4_BACK);
                    break;
                }

            case STEP_4:
                goToMobileInternetSelectOfferStep5Fragment();
                ((MainActivity) getActivity()).setNavIcon(2);
                mailIcon.setVisibility(View.VISIBLE);
                subheaderTitle.setText(STEP_5);
                backPage.setText(STEP_5_BACK);
                break;

            case STEP_5:
                goToMobileInternetSelectOfferStep6Fragment();
                ((MainActivity) getActivity()).setNavIcon(3);
                mailIcon.setVisibility(View.GONE);
                subheaderTitle.setText(STEP_6);
                backPage.setText(STEP_6_BACK);
                break;

            case STEP_6:
                goToMobileInternetSelectOfferStep7Fragment();
                ((MainActivity) getActivity()).setNavIcon(4);
                mailIcon.setVisibility(View.GONE);
                subheaderTitle.setText(STEP_7);
                backPage.setText(STEP_7_BACK);
                break;

            case STEP_7:
                goToMobileInternetSelectOfferStep8Fragment();
                ((MainActivity) getActivity()).setNavIcon(5);
                mailIcon.setVisibility(View.GONE);
                subheaderTitle.setText(STEP_8);
                backPage.setText(STEP_8_BACK);
                break;

            case STEP_8:
                goToMobileInternetSelectOfferStep9Fragment();
                ((MainActivity) getActivity()).setNavIcon(6);
                mailIcon.setVisibility(View.GONE);
                subheaderTitle.setText(STEP_9);
                backPage.setText(STEP_9_BACK);
                break;

        }
    }


    private void goToMobileInternetSelectOfferStep2Fragment() {
        getFragmentManager().beginTransaction().replace(R.id.fragment_container_mobile_internet_create_offer, mobileInternetCreateOfferStep2Fragment = new MobileInternetSelectOfferStep2Fragment()).commit();
        subheaderTitle.setText(STEP_2);
        backPage.setText(STEP_1_BACK);
    }

    private void goToMobileInternetSelectOfferStep3Fragment() {
        getFragmentManager().beginTransaction().replace(R.id.fragment_container_mobile_internet_create_offer, mobileInternetCreateOfferStep3Fragment = new MobileInternetCreateOfferStep3Fragment()).commit();
    }


    private void goToMobileInternetSelectOfferStep4Fragment() {
        getFragmentManager().beginTransaction().replace(R.id.fragment_container_mobile_internet_create_offer, mobileInternetCreateOfferStep4Fragment = new MobileInternetCreateOfferStep4Fragment()).commit();
    }

    private void goToMobileInternetSelectOfferStep5Fragment() {
        getFragmentManager().beginTransaction().replace(R.id.fragment_container_mobile_internet_create_offer, mobileInternetCreateOfferStep5Fragment = new MobileInternetCreateOfferStep5Fragment()).commit();
    }

    private void goToMobileInternetSelectOfferStep6Fragment() {
        getFragmentManager().beginTransaction().replace(R.id.fragment_container_mobile_internet_create_offer, mobileInternetCreateOfferStep6Fragment = new MobileInternetCreateOfferStep6Fragment()).commit();
    }

    private void goToMobileInternetSelectOfferStep7Fragment() {
        getFragmentManager().beginTransaction().replace(R.id.fragment_container_mobile_internet_create_offer, mobileInternetCreateOfferStep7Fragment = new MobileInternetCreateOfferStep7Fragment()).commit();
    }

    private void goToMobileInternetSelectOfferStep8Fragment() {
        getFragmentManager().beginTransaction().replace(R.id.fragment_container_mobile_internet_create_offer, mobileInternetCreateOfferStep8Fragment = new MobileInternetCreateOfferStep8Fragment()).commit();
    }


    private void goToMobileInternetSelectOfferStep9Fragment() {
        getFragmentManager().beginTransaction().replace(R.id.fragment_container_mobile_internet_create_offer, mobileInternetCreateOfferStep9Fragment = new MobileInternetCreateOfferStep9Fragment()).commit();
    }
}
