package it.wind.smartsales.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.nineoldandroids.animation.Animator;

import org.json.JSONObject;

import it.wind.smartsales.R;
import it.wind.smartsales.activity.MainActivity;
import it.wind.smartsales.commons.Constants;
import it.wind.smartsales.commons.Utils;
import it.wind.smartsales.dao.PraticheDAO;
import it.wind.smartsales.entities.Pratica;
import it.wind.smartsales.entities.Session;

/**
 * Created by luca.quaranta on 10/02/2016.
 */
public class HomeAdminFragment extends Fragment {

    private final static int ANIMATION_DURATION = 300;
    private LinearLayout creationOfferLayout, selectionOfferLayout, creazioneOffertaButton, scegliOffertaButton;

    private void initializeComponents(View view) {
        creazioneOffertaButton = (LinearLayout) view.findViewById(R.id.creazione_offerta_button);
        creationOfferLayout = (LinearLayout) view.findViewById(R.id.creation_offer_layout);
        scegliOffertaButton = (LinearLayout) view.findViewById(R.id.scegli_offerta_button);
        selectionOfferLayout = (LinearLayout) view.findViewById(R.id.selection_offer_layout);
        View mobileInternetCreateOffer = view.findViewById(R.id.mobile_internet_create_offer);
        View mobileInternetSelectOffer = view.findViewById(R.id.mobile_internet_select_offer);
        View terminalsButton = view.findViewById(R.id.terminals_container);

        creazioneOffertaButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openCreationOption();
                if (scegliOffertaButton.getVisibility() == View.GONE) {
                    closeSelectionOption();
                }
            }
        });

        scegliOffertaButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openSelectionOption();
                if (creazioneOffertaButton.getVisibility() == View.GONE) {
                    closeCreationOption();
                }
            }
        });

        terminalsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity) getActivity()).increaseDraftNumber();
                ((MainActivity) getActivity()).goToPage(Constants.Pages.PAGE_MOBILE_TERMINALS_SELECT);
            }
        });

        view.findViewById(R.id.wireline_internet_create_offer).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JSONObject jsonPratica = Utils.createPratica();
                Pratica pratica = PraticheDAO.createPratica(v.getContext(), jsonPratica, Session.getInstance().getDealer().getDealerCode());
                Session.getInstance().setJsonPratica(jsonPratica, pratica);

                Session.getInstance().getPratica().setStep(PraticheDAO.STEP1);
                PraticheDAO.updatePratica(getActivity().getBaseContext());

                ((MainActivity) v.getContext()).increaseDraftNumber();
                ((MainActivity) v.getContext()).goToPage(Constants.Pages.PAGE_WIRELINE_INTERNET_CREATE_OFFER);

            }
        });

        mobileInternetCreateOffer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JSONObject jsonPratica = Utils.createPratica();
                Pratica pratica = PraticheDAO.createPratica(v.getContext(), jsonPratica, Session.getInstance().getDealer().getDealerCode());
                Session.getInstance().setJsonPratica(jsonPratica, pratica);

                Session.getInstance().getPratica().setStep(PraticheDAO.STEP1);
                PraticheDAO.updatePratica(getActivity().getBaseContext());

                ((MainActivity) v.getContext()).increaseDraftNumber();
                ((MainActivity) v.getContext()).goToPage(Constants.Pages.PAGE_MOBILE_INTERNET_CREATE_OFFER);
            }
        });


        mobileInternetSelectOffer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JSONObject jsonPratica = Utils.createPratica();
                Pratica pratica = PraticheDAO.createPratica(v.getContext(), jsonPratica, Session.getInstance().getDealer().getDealerCode());
                Session.getInstance().setJsonPratica(jsonPratica, pratica);

                Session.getInstance().getPratica().setStep(PraticheDAO.STEP1);
                PraticheDAO.updatePratica(getActivity().getBaseContext());

                ((MainActivity) v.getContext()).increaseDraftNumber();
                ((MainActivity) v.getContext()).goToPage(Constants.Pages.PAGE_MOBILE_INTERNET_SELECT_OFFER);
            }
        });
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_home_admin, container, false);
        initializeComponents(view);

        return view;
    }

    private void openCreationOption() {
        YoYo.with(Techniques.FadeIn).duration(ANIMATION_DURATION).withListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
                creationOfferLayout.setVisibility(View.VISIBLE);
                YoYo.with(Techniques.FadeOut).duration(ANIMATION_DURATION).withListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        creazioneOffertaButton.setVisibility(View.GONE);
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {

                    }
                }).playOn(creazioneOffertaButton);
            }

            @Override
            public void onAnimationEnd(Animator animation) {

            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        }).playOn(creationOfferLayout);
    }

    private void openSelectionOption() {
        YoYo.with(Techniques.FadeIn).duration(ANIMATION_DURATION).withListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
                selectionOfferLayout.setVisibility(View.VISIBLE);
                YoYo.with(Techniques.FadeOut).duration(ANIMATION_DURATION).withListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        scegliOffertaButton.setVisibility(View.GONE);
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {

                    }
                }).playOn(scegliOffertaButton);
            }

            @Override
            public void onAnimationEnd(Animator animation) {

            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        }).playOn(selectionOfferLayout);
    }

    private void closeCreationOption() {
        YoYo.with(Techniques.FadeIn).duration(ANIMATION_DURATION).withListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
                creazioneOffertaButton.setVisibility(View.VISIBLE);
                YoYo.with(Techniques.FadeOut).duration(ANIMATION_DURATION).withListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        creationOfferLayout.setVisibility(View.GONE);
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {

                    }
                }).playOn(creationOfferLayout);
            }

            @Override
            public void onAnimationEnd(Animator animation) {

            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        }).playOn(creazioneOffertaButton);
    }

    private void closeSelectionOption() {
        YoYo.with(Techniques.FadeIn).duration(ANIMATION_DURATION).withListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
                scegliOffertaButton.setVisibility(View.VISIBLE);
                YoYo.with(Techniques.FadeOut).duration(ANIMATION_DURATION).withListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        selectionOfferLayout.setVisibility(View.GONE);
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {

                    }
                }).playOn(selectionOfferLayout);
            }

            @Override
            public void onAnimationEnd(Animator animation) {

            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        }).playOn(scegliOffertaButton);
    }
}