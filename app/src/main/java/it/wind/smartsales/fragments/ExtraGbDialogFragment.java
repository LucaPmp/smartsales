package it.wind.smartsales.fragments;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import it.wind.smartsales.R;
import it.wind.smartsales.adapter.ExtraGBValueAdapter;
import it.wind.smartsales.adapter.ExtraGbAdapter;
import it.wind.smartsales.adapter.ExtraGigaDetailAdapter;
import it.wind.smartsales.customviews.HorizontalSpaceItemDecoration;
import it.wind.smartsales.customviews.VerticalSpaceItemDecoration;
import it.wind.smartsales.dao.ExtraGigaValueDAO;
import it.wind.smartsales.entities.AddOnPackage;
import it.wind.smartsales.entities.ExtraGBDetail;
import it.wind.smartsales.entities.OffertaMobile;
import it.wind.smartsales.entities.Session;

/**
 * Created by luca.quaranta on 03/12/2015.
 */
public class ExtraGbDialogFragment extends DialogFragment {

    public static final String KEY = "MESSAGE_KEY";
    private static final String TAG = "ExtraGbDialogFragment";
    private ArrayList<ArrayList<ExtraGBDetail>> extraGigaDetail, extraGigaDetailOriginal;

    private OffertaMobile offertaMobile;

    private RecyclerView gbRecyclerView;
    private RecyclerView.LayoutManager mLayoutManagerGb;
    private RecyclerView.Adapter gbAdapter;

    private Button indietroButton, continuaButton;
    private MobileInternetCreateOfferStep8Fragment mobileInternetCreateOfferStep8Fragment;
    private TextView newICCID, offerDescription, offerDescriptionUpper, totalePacchettiAcquistati;
    private LinearLayout layoutPacchetti, packageDetailLayout;
    private View.OnClickListener gbClickListener;

    private Button buttonPlus, gb1, gb5, gb15, gb30, gb100;
    private RecyclerView extraGBValueRecycler;

    private ExtraGBValueAdapter valueAdapter;
    private ExtraGigaDetailAdapter extraGigaDetailAdapter;
    private RecyclerView extraGigaDetailRecyclerView;
    private TextView detailLabelHeaderText;

    public ExtraGbDialogFragment() {
    }

    @SuppressLint("ValidFragment")
    public ExtraGbDialogFragment(MobileInternetCreateOfferStep8Fragment mobileInternetCreateOfferStep8Fragment, ArrayList<ArrayList<ExtraGBDetail>> extraGigaDetail) {
        this.mobileInternetCreateOfferStep8Fragment = mobileInternetCreateOfferStep8Fragment;
        this.extraGigaDetail = extraGigaDetail;
        this.extraGigaDetailOriginal = new ArrayList<ArrayList<ExtraGBDetail>>();

        int i = 0;
        for (ArrayList<ExtraGBDetail> extraGBDetails : extraGigaDetail) {
            ArrayList<ExtraGBDetail> detailsCopied = new ArrayList<ExtraGBDetail>();
            for (ExtraGBDetail extraGBDetail : extraGBDetails) {
                ExtraGBDetail extraGBDetailCopied = new ExtraGBDetail();
                extraGBDetailCopied.setLetter(extraGBDetail.getLetter());
                extraGBDetailCopied.setMaxValue(extraGBDetail.getMaxValue());
                extraGBDetailCopied.setValue(extraGBDetail.getValue());
                detailsCopied.add(extraGBDetailCopied);

            }
            this.extraGigaDetailOriginal.add(detailsCopied);
            i++;
        }


    }

    private void initializeComponents(View view) {
        indietroButton = (Button) view.findViewById(R.id.indietro_button);
        continuaButton = (Button) view.findViewById(R.id.continua_button);
        buttonPlus = (Button) view.findViewById(R.id.button_plus);

        gbRecyclerView = (RecyclerView) view.findViewById(R.id.extra_gb_detail_recycler_view);

        layoutPacchetti = (LinearLayout) view.findViewById(R.id.layout_pacchetti);
        packageDetailLayout = (LinearLayout) view.findViewById(R.id.package_detail_layout);

        offerDescription = (TextView) view.findViewById(R.id.offer_description);
        offerDescriptionUpper = (TextView) view.findViewById(R.id.offer_description_upper);
        newICCID = (TextView) view.findViewById(R.id.new_iccid);

        gb1 = (Button) view.findViewById(R.id.gb_1);
        gb5 = (Button) view.findViewById(R.id.gb_5);
        gb15 = (Button) view.findViewById(R.id.gb_15);
        gb30 = (Button) view.findViewById(R.id.gb_30);
        gb100 = (Button) view.findViewById(R.id.gb_100);

        totalePacchettiAcquistati = (TextView) view.findViewById(R.id.totali_pacchetti_acquistati);

        detailLabelHeaderText = (TextView) view.findViewById(R.id.detailLabelHeader);


        gbClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                layoutPacchetti.setVisibility(View.GONE);
                buttonPlus.setBackground(getActivity().getResources().getDrawable(R.drawable.gb_selector));
                buttonPlus.setTextColor(getActivity().getResources().getColor(android.R.color.white));
                buttonPlus.setText(((Button) v).getText());
                buttonPlus.setSelected(true);

                packageDetailLayout.setVisibility(View.VISIBLE);
                populateGbList(((Button) v).getText().toString());
            }
        };



        initGbList();
    }

    private void initGbList() {
        gbRecyclerView.setHasFixedSize(true);
        mLayoutManagerGb = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        gbRecyclerView.setLayoutManager(mLayoutManagerGb);
        gbRecyclerView.setItemAnimator(new DefaultItemAnimator());
        gbRecyclerView.addItemDecoration(new HorizontalSpaceItemDecoration(20));
    }

    private void populateGbList(String text) {

        AddOnPackage addOnPackage = null;
        for (AddOnPackage addOnPackage1 : offertaMobile.getAddOnPackageArrayListStep8()) {
            if (addOnPackage1.getDesc().compareToIgnoreCase(text.replace("\n", "")) == 0) {
                addOnPackage = addOnPackage1;
            }
        }

        gbAdapter = new ExtraGbAdapter(this, offertaMobile, addOnPackage.getAddOnMobileArrayList());
        gbRecyclerView.setAdapter(gbAdapter);
        totalePacchettiAcquistati.setText(String.valueOf(addOnPackage.getAddOnMobileArrayList().size()));
    }

    public void updateAddOnList(){
        mobileInternetCreateOfferStep8Fragment.updateAddOnList(offertaMobile);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    @Override
    public void onResume() {
        super.onResume();
        int width = getResources().getDimensionPixelSize(R.dimen.fragment_dialog_mnp_width);
        getDialog().getWindow().setLayout(width, ViewGroup.LayoutParams.WRAP_CONTENT);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_dialog__extra_gb, container, false);

        initializeComponents(view);

        populateExtraGBValues(view);


        populateExtraGigaDetail(view);

        populateFields();

//        buttonPlus.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                layoutPacchetti.setVisibility(View.VISIBLE);
//            }
//        });

//        gb1.setOnClickListener(gbClickListener);
//        gb5.setOnClickListener(gbClickListener);
//        gb15.setOnClickListener(gbClickListener);
//        gb30.setOnClickListener(gbClickListener);
//        gb100.setOnClickListener(gbClickListener);

        indietroButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //reset all
                ExtraGigaValueDAO.insertOrUpdateDetails(getActivity(), String.valueOf(Session.getInstance().getPratica().getId()), Session.createJSONFromExtraGBDetail(ExtraGbDialogFragment.this.extraGigaDetailOriginal));


                mobileInternetCreateOfferStep8Fragment.checkExtraGigaSelection();
                dismiss();
            }
        });

        continuaButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mobileInternetCreateOfferStep8Fragment.checkExtraGigaSelection();
                mobileInternetCreateOfferStep8Fragment.updateOffer();

                dismiss();
            }
        });

        return view;
    }

    private void populateExtraGBDetail(View view) {

    }

    private void populateExtraGBValues(View view) {

        boolean noGbSelected = true;
        extraGBValueRecycler = (RecyclerView) view.findViewById(R.id.extra_gb_label_recycler);
        //MW implemantazione accensione 1^ Bottone in caso di nessuna selezione di ExtraGiga
        for(int i = 0; i<extraGigaDetail.size();i++){
            for(int k = 0; k<extraGigaDetail.get(i).size();k++) {
                if(extraGigaDetail.get(i).get(k).getValue()!=0){
                    noGbSelected=false;
                }
            }
        }
//        extraGBValueRecycler.setHasFixedSize(true);

        valueAdapter = new ExtraGBValueAdapter(Session.getInstance().getExtraGigaValues(Session.getInstance().getPratica().getId()),this,this.extraGigaDetail,noGbSelected);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this.getActivity());
        extraGBValueRecycler.setLayoutManager(mLayoutManager);
        extraGBValueRecycler.setItemAnimator(new DefaultItemAnimator());
        extraGBValueRecycler.setAdapter(valueAdapter);
        extraGBValueRecycler.addItemDecoration(new VerticalSpaceItemDecoration(-1));
    }

    private void populateFields() {
//        newICCID.setText(offertaMobile.getNewICCID());
        offerDescription.setText("Configurazione");

        offerDescriptionUpper.setText("Extra Giga");

//
//
//        for (AddOnMobile addOnMobile : offertaMobile.getAddOnMobileArrayList()) {
//            switch (addOnMobile.getDescFascia()) {
//                case "1GB":
//                    gb1.setEnabled(true);
//                    break;
//                case "5GB":
//                    gb5.setEnabled(true);
//                    break;
//                case "15GB":
//                    gb15.setEnabled(true);
//                    break;
//                case "30GB":
//                    gb30.setEnabled(true);
//                    break;
//                case "100GB":
//                    gb100.setEnabled(true);
//                    break;
//            }
//        }
//
//        if (offertaMobile.getAddOnMobileStep8() != null){
//            buttonPlus.setBackground(getActivity().getResources().getDrawable(R.drawable.gb_selector));
//            buttonPlus.setTextColor(getActivity().getResources().getColor(android.R.color.white));
//            buttonPlus.setText(offertaMobile.getAddOnMobileStep8().getDescFascia().replace("GB", "\nGB"));
//            buttonPlus.setSelected(true);
//
//            packageDetailLayout.setVisibility(View.VISIBLE);
//            populateGbList(offertaMobile.getAddOnMobileStep8().getDescFascia());
//        }
    }


    private void populateExtraGigaDetail(View view) {

        Log.i(TAG,String.valueOf(extraGigaDetail.size()));

        extraGigaDetailRecyclerView = (RecyclerView) view.findViewById(R.id.extra_gb_detail_recycler_view);

        int firstNotNullElem=0;
        for (ArrayList<ExtraGBDetail> extraGBDetails : extraGigaDetail) {
            if(extraGBDetails.size()>0)
            {
                break;
            }
            firstNotNullElem++;
        }


        setLabel(String.valueOf(valueAdapter.getValueList().get(firstNotNullElem).getValue())+ " GB");

        extraGigaDetailAdapter = new ExtraGigaDetailAdapter(mobileInternetCreateOfferStep8Fragment, extraGigaDetail.get(firstNotNullElem), firstNotNullElem,valueAdapter);

        extraGigaDetailRecyclerView.setAdapter(extraGigaDetailAdapter);
    }

    public void swapDetailAdapter(int position, String label) {

        setLabel(label);

        extraGigaDetailAdapter = new ExtraGigaDetailAdapter(mobileInternetCreateOfferStep8Fragment, extraGigaDetail.get(position), position,valueAdapter);
        extraGigaDetailRecyclerView.swapAdapter(extraGigaDetailAdapter, false);
    }

    private void setLabel(String label)
    {
        String prezzo = Session.getInstance().getExtraGigaValueData(label).getExtraBundlePrezzo();
        String desc = Session.getInstance().getExtraGigaValueData(label).getExtraBundleDescrizione();
        String descrizionePDF = Session.getInstance().getExtraGigaValueData(label).getExtraBundleDescrizionePDF();
        String labelHeader = "Per ogni pacchetto, seleziona il numero di rinnovi dell’opzione "+descrizionePDF+" "+desc+" da "+prezzo+"€ al mese";
        detailLabelHeaderText.setText(labelHeader);
    };


}
