package it.wind.smartsales.fragments;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.squareup.picasso.Picasso;
import com.viewpagerindicator.CirclePageIndicator;

import org.json.JSONObject;

import java.util.ArrayList;

import it.wind.smartsales.R;
import it.wind.smartsales.activity.MainActivity;
import it.wind.smartsales.commons.Constants;
import it.wind.smartsales.commons.Utils;
import it.wind.smartsales.customviews.AspectRatioView;
import it.wind.smartsales.dao.PraticheDAO;
import it.wind.smartsales.entities.Pratica;
import it.wind.smartsales.entities.Session;

/**
 * Created by luca.quaranta on 10/02/2016.
 */
public class HomeUserFragment extends Fragment {
    private ViewPager pagerTerminali, pagerMobileInternet, pagerFissoAdsl, pagerOffertaIntegrata, pagerServiziDigitali;
    private PagerAdapter pagerTerminaliAdapter, pagerMobileInternetAdapter, pagerFissoAdslAdapter, pagerOffertaIntegrataAdapter, pagerServiziDigitaliAdapter;
    private RelativeLayout mobile_internet_select_offer_user;

    private void initializeComponents(View view) {
        setTerminaliPager(view);
        setMobileInternetPager(view);
        setFissoAdslPager(view);
        setOffertaIntegrataPager(view);
        setServiziDigitaliPager(view);
        mobile_internet_select_offer_user = (RelativeLayout) view.findViewById(R.id.mobile_internet_select_offer_user);

        mobile_internet_select_offer_user.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JSONObject jsonPratica = Utils.createPratica();
                Pratica pratica = PraticheDAO.createPratica(v.getContext(), jsonPratica, Session.getInstance().getDealer().getDealerCode());
                Session.getInstance().setJsonPratica(jsonPratica, pratica);


                Session.getInstance().getPratica().setStep(PraticheDAO.STEP1);
                PraticheDAO.updatePratica(getActivity().getBaseContext());

                ((MainActivity) v.getContext()).increaseDraftNumber();
                ((MainActivity) v.getContext()).goToPage(Constants.Pages.PAGE_MOBILE_INTERNET_SELECT_OFFER);
            }
        });
    }

    private void setTerminaliPager(View view){
        pagerTerminali = (ViewPager) view.findViewById(R.id.pager_terminali);

        ArrayList<String> stringArrayList = new ArrayList<>();

        pagerTerminaliAdapter = new ScreenSlidePagerAdapter(getActivity(), stringArrayList);
        pagerTerminali.setAdapter(pagerTerminaliAdapter);

        CirclePageIndicator circlePageIndicator = (CirclePageIndicator) view.findViewById(R.id.circle_indicator_terminali);
        circlePageIndicator.setViewPager(pagerTerminali);
    }

    private void setMobileInternetPager(View view){
        pagerMobileInternet = (ViewPager) view.findViewById(R.id.pager_mobile_internet);

        ArrayList<String> stringArrayList = new ArrayList<>();

        pagerMobileInternetAdapter = new ScreenSlidePagerAdapter(getActivity(), stringArrayList);
        pagerMobileInternet.setAdapter(pagerMobileInternetAdapter);

        CirclePageIndicator circlePageIndicator = (CirclePageIndicator) view.findViewById(R.id.circle_indicator_mobile_internet);
        circlePageIndicator.setViewPager(pagerMobileInternet);
    }

    private void setFissoAdslPager(View view){
        pagerFissoAdsl = (ViewPager) view.findViewById(R.id.pager_fisso_adsl);

        ArrayList<String> stringArrayList = new ArrayList<>();

        pagerFissoAdslAdapter = new ScreenSlidePagerAdapter(getActivity(), stringArrayList);
        pagerFissoAdsl.setAdapter(pagerFissoAdslAdapter);

        CirclePageIndicator circlePageIndicator = (CirclePageIndicator) view.findViewById(R.id.circle_indicator_fisso_adsl);
        circlePageIndicator.setViewPager(pagerFissoAdsl);
    }

    private void setOffertaIntegrataPager(View view){
        pagerOffertaIntegrata = (ViewPager) view.findViewById(R.id.pager_offerta_integrata);

        ArrayList<String> stringArrayList = new ArrayList<>();

        pagerOffertaIntegrataAdapter = new ScreenSlidePagerAdapter(getActivity(), stringArrayList);
        pagerOffertaIntegrata.setAdapter(pagerOffertaIntegrataAdapter);

        CirclePageIndicator circlePageIndicator = (CirclePageIndicator) view.findViewById(R.id.circle_indicator_offerta_integrata);
        circlePageIndicator.setViewPager(pagerOffertaIntegrata);
    }

    private void setServiziDigitaliPager(View view){
        pagerServiziDigitali = (ViewPager) view.findViewById(R.id.pager_servizi_digitali);

        ArrayList<String> stringArrayList = new ArrayList<>();

        pagerServiziDigitaliAdapter = new ScreenSlidePagerAdapter(getActivity(), stringArrayList);
        pagerServiziDigitali.setAdapter(pagerServiziDigitaliAdapter);

        CirclePageIndicator circlePageIndicator = (CirclePageIndicator) view.findViewById(R.id.circle_indicator_servizi_digitali);
        circlePageIndicator.setViewPager(pagerServiziDigitali);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_home_user, container, false);
        initializeComponents(view);

        return view;
    }

    private class ScreenSlidePagerAdapter extends PagerAdapter {

        private ArrayList<String> stringArrayList;
        private Context context;

        public ScreenSlidePagerAdapter(Context context, ArrayList<String> stringArrayList){
            this.context = context;
            this.stringArrayList = stringArrayList;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            LayoutInflater inflater = LayoutInflater.from(context);
            String imageUrl = stringArrayList.get(position);
            ViewGroup layout = (ViewGroup) inflater.inflate(R.layout.slide_page, container, false);

            Picasso.with(context).load(imageUrl).into(((AspectRatioView) layout.findViewById(R.id.pager_image)));

            container.addView(layout);
            return layout;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }

        @Override
        public int getCount() {
            return stringArrayList.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }
    }

}