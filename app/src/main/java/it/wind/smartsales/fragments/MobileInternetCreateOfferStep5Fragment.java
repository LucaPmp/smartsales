package it.wind.smartsales.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.text.Html;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

import it.wind.smartsales.R;
import it.wind.smartsales.activity.MainActivity;
import it.wind.smartsales.commons.Constants;
import it.wind.smartsales.commons.Log;
import it.wind.smartsales.commons.Utils;
import it.wind.smartsales.dao.PraticheDAO;
import it.wind.smartsales.entities.AddOnMobile;
import it.wind.smartsales.entities.OffertaMobile;
import it.wind.smartsales.entities.OpzioneMobile;
import it.wind.smartsales.entities.Session;
import it.wind.smartsales.entities.Terminale;

/**
 * Created by luca.quaranta on 10/02/2016.
 */
public class MobileInternetCreateOfferStep5Fragment extends Fragment implements Handler.Callback {

    private static final String TAG = "MobileInternetCreateOfferStep5Fragment";

    private LinearLayout scrollView;
    private Button continuaButton;
    private TextView totaleCanoneMensile, contributoUnaTantum;

    private float scontoApplicatoNew = 0;

    private float totCanoneMensile = 0f, contributoUnaTan = 0f, scontoApplicato = 0f;

    private void initializeComponents(View view) {
        scrollView = (LinearLayout) view.findViewById(R.id.scrollview);
        continuaButton = (Button) view.findViewById(R.id.continua_button);
        totaleCanoneMensile = (TextView) view.findViewById(R.id.totale_canone_mensile);
        contributoUnaTantum = (TextView) view.findViewById(R.id.contributo_una_tantum);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_mobile_internet_create_offer_step_5, container, false);
        initializeComponents(view);


        try {
            JSONArray offerteRaggruppate = (JSONArray) Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONArray("offerteRaggruppate");
            ArrayList<OffertaMobile> offertaMobiles = new ArrayList<>();
            for (int i = 0; i < offerteRaggruppate.length(); i++) {
                offertaMobiles.add(Utils.createOffertaMobile(offerteRaggruppate.getJSONObject(i)));
            }
            boolean daCalcolare=true;
            for (OffertaMobile offertaMobile : offertaMobiles) {
                createOfferRow(offertaMobile);

                //Calcolo lo socnto fatto sul Pinao Ricaricabile
                if(offertaMobile.getOfferType().equals("Ricaricabile")&&!TextUtils.isEmpty(offertaMobile.getScontoApplicato())) {

                        scontoApplicatoNew += (Float.valueOf(offertaMobile.getScontoApplicato())) * offertaMobile.getSelectedNumSimStep3();


                }
                //Controllo se ho sconti sui piani Giga
                if(offertaMobile.getAddOnMobileArrayList().size()!=0){
                    for(int i =0;i<offertaMobile.getAddOnMobileArrayList().size();i++) {
                        if(!TextUtils.isEmpty(offertaMobile.getAddOnMobileArrayList().get(i).getScontoGigaApplicato())) {
                            if(daCalcolare) {
                                scontoApplicatoNew += (Float.valueOf(offertaMobile.getAddOnMobileArrayList().get(i).getScontoGigaApplicato().replace(",", "."))) * offertaMobile.getAddOnMobileArrayList().get(i).getSelectedNumSimStep3();
                            }
                        }
                    }
                    daCalcolare=false;
                }
            }


            if (contributoUnaTan != 0) {
                contributoUnaTantum.setText(String.format("%.2f", contributoUnaTan) + "€");
            } else {
                contributoUnaTantum.setText(String.format("%.2f", 0f) + "€");
            }
            //Mi prendo il valore dello sconto applicato ai piani abbonamento
            scontoApplicato +=scontoApplicatoNew;


            for (int i = 0; i<offertaMobiles.size();i++){
                if(offertaMobiles.get(i).getOfferType().equals("Ricaricabile")){
                    createAddOnRow(offertaMobiles.get(i));
                    break;
                }
            }
            totaleCanoneMensile.setText(String.format("%.2f", totCanoneMensile - scontoApplicato) + "€");

            if (scontoApplicato != 0f) {
                createScontoRow();
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        continuaButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Session.getInstance().getPratica().setStep(PraticheDAO.STEP6);
                PraticheDAO.updatePratica(getActivity().getBaseContext());

                Session.getInstance().getPratica().setTotCanoneMensile(totCanoneMensile);
                Session.getInstance().getPratica().setContributoUnaTan(contributoUnaTan);
                Session.getInstance().getPratica().setScontoApplicato(scontoApplicato);

                Log.d("JSON PRATICA)", Session.getInstance().getJsonPratica().toString());
                if(Session.getInstance().getDealer().getDealerRole().compareTo("UserSales")!=0)
                    ((MainActivity) getActivity()).goForward();
                else
                    ((MainActivity) getActivity()).goForwardSelectOffer();
            }
        });

        return view;
    }

    private void createOfferRow(OffertaMobile offertaMobile) {
        LinearLayout linearLayout = new LinearLayout(getActivity());
        linearLayout.setOrientation(LinearLayout.HORIZONTAL);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        linearLayout.setLayoutParams(layoutParams);

        LinearLayout linearLayout1 = new LinearLayout(getActivity());
        linearLayout1.setOrientation(LinearLayout.VERTICAL);
        linearLayout1.setBackgroundColor(getActivity().getResources().getColor(R.color.terminali_background_not_selected));
        LinearLayout.LayoutParams layoutParams1 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        layoutParams1.setMargins(0, 0, (int) getActivity().getResources().getDimension(R.dimen.spacing_xsmall), 0);
        layoutParams1.weight = 1f;
        linearLayout1.setPadding(0, (int) getActivity().getResources().getDimension(R.dimen.spacing_20dp), (int) getActivity().getResources().getDimension(R.dimen.spacing_xlarge), (int) getActivity().getResources().getDimension(R.dimen.spacing_20dp));
        linearLayout1.setLayoutParams(layoutParams1);

        LinearLayout linearLayout2 = new LinearLayout(getActivity());
        linearLayout2.setOrientation(LinearLayout.VERTICAL);
        linearLayout2.setBackgroundColor(getActivity().getResources().getColor(R.color.terminali_background_not_selected));
        LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        layoutParams2.setMargins((int) getActivity().getResources().getDimension(R.dimen.spacing_xsmall), 0, 0, 0);
        layoutParams2.weight = 1f;
        linearLayout2.setPadding((int) getActivity().getResources().getDimension(R.dimen.spacing_xlarge), (int) getActivity().getResources().getDimension(R.dimen.spacing_20dp), 0, (int) getActivity().getResources().getDimension(R.dimen.spacing_20dp));
        linearLayout2.setLayoutParams(layoutParams2);

        TextView textView1 = new TextView(getActivity());
        textView1.setTextColor(getActivity().getResources().getColor(android.R.color.black));
        textView1.setText(offertaMobile.getOfferDescription() + " " + offertaMobile.getOfferDescriptionUpper());
        textView1.setGravity(Gravity.END);
        textView1.setTextSize(18f);
        LinearLayout.LayoutParams layoutParams11 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        layoutParams11.setMargins(0, (int) getActivity().getResources().getDimension(R.dimen.spacing_small), 0, (int) getActivity().getResources().getDimension(R.dimen.spacing_small));
        layoutParams11.weight = 1f;
        textView1.setLayoutParams(layoutParams11);
        linearLayout1.addView(textView1);

        TextView textView2 = new TextView(getActivity());
        textView2.setTextColor(getActivity().getResources().getColor(android.R.color.black));
        textView2.setText(String.valueOf(offertaMobile.getSelectedNumSimStep3()) + " x " + String.format("%.2f", Float.valueOf(offertaMobile.getCanoneMensile().replace(",", "."))) + "€");
        textView2.setTextSize(18f);
        LinearLayout.LayoutParams layoutParams22 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        layoutParams22.setMargins(0, (int) getActivity().getResources().getDimension(R.dimen.spacing_small), 0, (int) getActivity().getResources().getDimension(R.dimen.spacing_small));
        layoutParams22.weight = 1f;
        textView2.setLayoutParams(layoutParams22);
        linearLayout2.addView(textView2);

        totCanoneMensile += Float.parseFloat(offertaMobile.getCanoneMensile().replace(",", ".")) * offertaMobile.getSelectedNumSimStep3();

        for (Terminale terminale : offertaMobile.getTerminaleArrayList()) {
            TextView textViewTerminaleSX = new TextView(getActivity());
            textViewTerminaleSX.setTextColor(getActivity().getResources().getColor(android.R.color.black));
            textViewTerminaleSX.setText(Html.fromHtml("<i>" + terminale.getTerminaleBrand() + " " + terminale.getTerminaleDescription() + (Boolean.parseBoolean(terminale.getMnp()) ? " - MNP" : " - Nuova Linea" + "</i>")));
            textViewTerminaleSX.setGravity(Gravity.END);
            textViewTerminaleSX.setTextSize(18f);
            LinearLayout.LayoutParams layoutParamsTerminaleSX = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
            layoutParamsTerminaleSX.setMargins(0, (int) getActivity().getResources().getDimension(R.dimen.spacing_small), 0, (int) getActivity().getResources().getDimension(R.dimen.spacing_small));
            layoutParamsTerminaleSX.weight = 1f;
            textViewTerminaleSX.setLayoutParams(layoutParamsTerminaleSX);
            linearLayout1.addView(textViewTerminaleSX);

            TextView textViewTerminaleDX = new TextView(getActivity());
            textViewTerminaleDX.setTextColor(getActivity().getResources().getColor(android.R.color.black));
            textViewTerminaleDX.setText(String.valueOf(terminale.getSelectedNumSimStep3()) + " x " + String.format("%.2f", Float.valueOf(terminale.getRataMensile().replace(",", "."))) + "€");
            textViewTerminaleDX.setTextSize(18f);
            LinearLayout.LayoutParams layoutParamsTerminaleDX = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
            layoutParamsTerminaleDX.setMargins(0, (int) getActivity().getResources().getDimension(R.dimen.spacing_small), 0, (int) getActivity().getResources().getDimension(R.dimen.spacing_small));
            layoutParamsTerminaleDX.weight = 1f;
            textViewTerminaleDX.setLayoutParams(layoutParamsTerminaleDX);
            linearLayout2.addView(textViewTerminaleDX);

            totCanoneMensile += Float.parseFloat(terminale.getRataMensile().replace(",", ".")) * terminale.getSelectedNumSimStep3();
            if (!TextUtils.isEmpty(terminale.getRataIniziale())) {
                contributoUnaTan += Float.parseFloat(terminale.getRataIniziale().replace(",", ".")) * terminale.getSelectedNumSimStep3();
            }
        }

        for (OpzioneMobile opzioneMobile : offertaMobile.getOpzioneMobileArrayList()) {
            if (TextUtils.isEmpty(opzioneMobile.getScontoConvergenza())) {
                TextView textViewOpzioneMobileSX = new TextView(getActivity());
                textViewOpzioneMobileSX.setTextColor(getActivity().getResources().getColor(android.R.color.black));
                textViewOpzioneMobileSX.setText(Html.fromHtml("<i>" + opzioneMobile.getOpzioneDesc() + "</i>"));
                textViewOpzioneMobileSX.setGravity(Gravity.END);
                textViewOpzioneMobileSX.setTextSize(18f);
                LinearLayout.LayoutParams layoutParamsOpzioneMobileSX = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                layoutParamsOpzioneMobileSX.setMargins(0, (int) getActivity().getResources().getDimension(R.dimen.spacing_small), 0, (int) getActivity().getResources().getDimension(R.dimen.spacing_small));
                layoutParamsOpzioneMobileSX.weight = 1f;
                textViewOpzioneMobileSX.setLayoutParams(layoutParamsOpzioneMobileSX);
                linearLayout1.addView(textViewOpzioneMobileSX);

                TextView textViewOpzioneMobileDX = new TextView(getActivity());
                textViewOpzioneMobileDX.setTextColor(getActivity().getResources().getColor(android.R.color.black));
                textViewOpzioneMobileDX.setText(String.valueOf(opzioneMobile.getSelectedNumSimStep3()) + " x " + String.format("%.2f", Float.valueOf(opzioneMobile.getCanoneMensile().replace(",", "."))) + "€");
                textViewOpzioneMobileDX.setTextSize(18f);
                LinearLayout.LayoutParams layoutParamsOpzioneMobileDX = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                layoutParamsOpzioneMobileDX.setMargins(0, (int) getActivity().getResources().getDimension(R.dimen.spacing_small), 0, (int) getActivity().getResources().getDimension(R.dimen.spacing_small));
                layoutParamsOpzioneMobileDX.weight = 1f;
                textViewOpzioneMobileDX.setLayoutParams(layoutParamsOpzioneMobileDX);
                linearLayout2.addView(textViewOpzioneMobileDX);

                totCanoneMensile += Float.parseFloat(opzioneMobile.getCanoneMensile().replace(",", ".")) * opzioneMobile.getSelectedNumSimStep3();
                if (!TextUtils.isEmpty(opzioneMobile.getCostoUnaTantum())) {
                    contributoUnaTan += Float.parseFloat(opzioneMobile.getCostoUnaTantum().replace(",", ".")) * opzioneMobile.getSelectedNumSimStep3();
                }
            }
        }

        for (OpzioneMobile opzioneMobile : offertaMobile.getOpzioneMobileArrayList()) {
            if (!TextUtils.isEmpty(opzioneMobile.getScontoConvergenza())) {
                TextView textViewOpzioneMobileSX = new TextView(getActivity());
                textViewOpzioneMobileSX.setTextColor(getActivity().getResources().getColor(android.R.color.black));
                textViewOpzioneMobileSX.setText(Html.fromHtml("<i>" + opzioneMobile.getOpzioneDesc() + "</i>"));
                textViewOpzioneMobileSX.setGravity(Gravity.END);
                textViewOpzioneMobileSX.setTextSize(18f);
                LinearLayout.LayoutParams layoutParamsOpzioneMobileSX = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                layoutParamsOpzioneMobileSX.setMargins(0, (int) getActivity().getResources().getDimension(R.dimen.spacing_small), 0, (int) getActivity().getResources().getDimension(R.dimen.spacing_small));
                layoutParamsOpzioneMobileSX.weight = 1f;
                textViewOpzioneMobileSX.setLayoutParams(layoutParamsOpzioneMobileSX);
                linearLayout1.addView(textViewOpzioneMobileSX);

                TextView textViewOpzioneMobileDX = new TextView(getActivity());
                textViewOpzioneMobileDX.setTextColor(getActivity().getResources().getColor(android.R.color.black));
                textViewOpzioneMobileDX.setText(String.valueOf(opzioneMobile.getSelectedNumSimStep3()) + " x -" + String.format("%.2f", Float.valueOf(opzioneMobile.getScontoConvergenza().replace(",", "."))) + "€");
                textViewOpzioneMobileDX.setTextSize(18f);
                LinearLayout.LayoutParams layoutParamsOpzioneMobileDX = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                layoutParamsOpzioneMobileDX.setMargins(0, (int) getActivity().getResources().getDimension(R.dimen.spacing_small), 0, (int) getActivity().getResources().getDimension(R.dimen.spacing_small));
                layoutParamsOpzioneMobileDX.weight = 1f;
                textViewOpzioneMobileDX.setLayoutParams(layoutParamsOpzioneMobileDX);
                linearLayout2.addView(textViewOpzioneMobileDX);

                totCanoneMensile -= Float.parseFloat(opzioneMobile.getScontoConvergenza().replace(",", ".")) * opzioneMobile.getSelectedNumSimStep3();
                if (!TextUtils.isEmpty(opzioneMobile.getCostoUnaTantum())) {
                    contributoUnaTan += Float.parseFloat(opzioneMobile.getCostoUnaTantum().replace(",", ".")) * opzioneMobile.getSelectedNumSimStep3();
                }
            }
        }


        if (!TextUtils.isEmpty(offertaMobile.getCanonePromo())) {
            scontoApplicato += (Float.parseFloat(offertaMobile.getCanonePromo().replace(",", ".")) - Float.parseFloat(offertaMobile.getCanoneScontato().replace(",", "."))) * offertaMobile.getSelectedNumSimStep4();
        } else {
            scontoApplicato += (Float.parseFloat(offertaMobile.getCanoneMensile().replace(",", ".")) - Float.parseFloat(offertaMobile.getCanoneScontato().replace(",", "."))) * offertaMobile.getSelectedNumSimStep4();
        }

        linearLayout.addView(linearLayout1);
        linearLayout.addView(linearLayout2);
        scrollView.addView(linearLayout);
    }

    private void createAddOnRow(OffertaMobile offertaMobile) {
        LinearLayout linearLayout = new LinearLayout(getActivity());
        linearLayout.setOrientation(LinearLayout.HORIZONTAL);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        linearLayout.setLayoutParams(layoutParams);

        LinearLayout linearLayout1 = new LinearLayout(getActivity());
        linearLayout1.setOrientation(LinearLayout.VERTICAL);
        linearLayout1.setBackgroundColor(getActivity().getResources().getColor(R.color.terminali_background_not_selected));
        LinearLayout.LayoutParams layoutParams1 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        layoutParams1.setMargins(0, 0, (int) getActivity().getResources().getDimension(R.dimen.spacing_xsmall), 0);
        layoutParams1.weight = 1f;
        linearLayout1.setPadding(0, (int) getActivity().getResources().getDimension(R.dimen.spacing_20dp), (int) getActivity().getResources().getDimension(R.dimen.spacing_xlarge), (int) getActivity().getResources().getDimension(R.dimen.spacing_20dp));
        linearLayout1.setLayoutParams(layoutParams1);

        LinearLayout linearLayout2 = new LinearLayout(getActivity());
        linearLayout2.setOrientation(LinearLayout.VERTICAL);
        linearLayout2.setBackgroundColor(getActivity().getResources().getColor(R.color.terminali_background_not_selected));
        LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        layoutParams2.setMargins((int) getActivity().getResources().getDimension(R.dimen.spacing_xsmall), 0, 0, 0);
        layoutParams2.weight = 1f;
        linearLayout2.setPadding((int) getActivity().getResources().getDimension(R.dimen.spacing_xlarge), (int) getActivity().getResources().getDimension(R.dimen.spacing_20dp), 0, (int) getActivity().getResources().getDimension(R.dimen.spacing_20dp));
        linearLayout2.setLayoutParams(layoutParams2);

        for (AddOnMobile addOnMobile : offertaMobile.getAddOnMobileArrayList()) {
            if(addOnMobile.getSelectedNumSimStep3() > 0) {
                TextView textViewAddOnMobileSX = new TextView(getActivity());
                textViewAddOnMobileSX.setTextColor(getActivity().getResources().getColor(android.R.color.black));
                textViewAddOnMobileSX.setText(Html.fromHtml(addOnMobile.getDescFascia()));
                textViewAddOnMobileSX.setGravity(Gravity.END);
                textViewAddOnMobileSX.setTextSize(18f);
                LinearLayout.LayoutParams layoutParamsAddOnMobileSX = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                layoutParamsAddOnMobileSX.setMargins(0, (int) getActivity().getResources().getDimension(R.dimen.spacing_small), 0, (int) getActivity().getResources().getDimension(R.dimen.spacing_small));
                layoutParamsAddOnMobileSX.weight = 1f;
                textViewAddOnMobileSX.setLayoutParams(layoutParamsAddOnMobileSX);
                linearLayout1.addView(textViewAddOnMobileSX);

                TextView textViewAddOnMobileDX = new TextView(getActivity());
                textViewAddOnMobileDX.setTextColor(getActivity().getResources().getColor(android.R.color.black));
                textViewAddOnMobileDX.setText(String.valueOf(addOnMobile.getSelectedNumSimStep3()) + " x " + String.format("%.2f", Float.valueOf(addOnMobile.getPrezzoFascia().replace(",", "."))) + "€");
                textViewAddOnMobileDX.setTextSize(18f);
                LinearLayout.LayoutParams layoutParamsAddOnMobileDX = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                layoutParamsAddOnMobileDX.setMargins(0, (int) getActivity().getResources().getDimension(R.dimen.spacing_small), 0, (int) getActivity().getResources().getDimension(R.dimen.spacing_small));
                layoutParamsAddOnMobileDX.weight = 1f;
                textViewAddOnMobileDX.setLayoutParams(layoutParamsAddOnMobileDX);
                linearLayout2.addView(textViewAddOnMobileDX);

                totCanoneMensile += Float.parseFloat(addOnMobile.getPrezzoFascia().replace(",", ".")) * addOnMobile.getSelectedNumSimStep3();
            }
        }

        linearLayout.addView(linearLayout1);
        linearLayout.addView(linearLayout2);
        scrollView.addView(linearLayout);
    }


    private void createScontoRow() {
        LinearLayout linearLayout = new LinearLayout(getActivity());
        linearLayout.setOrientation(LinearLayout.HORIZONTAL);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        linearLayout.setLayoutParams(layoutParams);

        LinearLayout linearLayout1 = new LinearLayout(getActivity());
        linearLayout1.setOrientation(LinearLayout.VERTICAL);
        linearLayout1.setBackgroundColor(getActivity().getResources().getColor(R.color.terminali_background_not_selected));
        LinearLayout.LayoutParams layoutParams1 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        layoutParams1.setMargins(0, 0, (int) getActivity().getResources().getDimension(R.dimen.spacing_xsmall), 0);
        layoutParams1.weight = 1f;
        linearLayout1.setPadding(0, (int) getActivity().getResources().getDimension(R.dimen.spacing_20dp), (int) getActivity().getResources().getDimension(R.dimen.spacing_xlarge), (int) getActivity().getResources().getDimension(R.dimen.spacing_20dp));
        linearLayout1.setLayoutParams(layoutParams1);

        LinearLayout linearLayout2 = new LinearLayout(getActivity());
        linearLayout2.setOrientation(LinearLayout.VERTICAL);
        linearLayout2.setBackgroundColor(getActivity().getResources().getColor(R.color.terminali_background_not_selected));
        LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        layoutParams2.setMargins((int) getActivity().getResources().getDimension(R.dimen.spacing_xsmall), 0, 0, 0);
        layoutParams2.weight = 1f;
        linearLayout2.setPadding((int) getActivity().getResources().getDimension(R.dimen.spacing_xlarge), (int) getActivity().getResources().getDimension(R.dimen.spacing_20dp), 0, (int) getActivity().getResources().getDimension(R.dimen.spacing_20dp));
        linearLayout2.setLayoutParams(layoutParams2);

        TextView textView1 = new TextView(getActivity());
        textView1.setTextColor(getActivity().getResources().getColor(android.R.color.black));
        textView1.setText("Sconto applicato");
        textView1.setGravity(Gravity.END);
        textView1.setTextSize(18f);
        LinearLayout.LayoutParams layoutParams11 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        layoutParams11.setMargins(0, (int) getActivity().getResources().getDimension(R.dimen.spacing_small), 0, (int) getActivity().getResources().getDimension(R.dimen.spacing_small));
        layoutParams11.weight = 1f;
        textView1.setLayoutParams(layoutParams11);
        linearLayout1.addView(textView1);

        TextView textView2 = new TextView(getActivity());
        textView2.setTextColor(getActivity().getResources().getColor(android.R.color.black));
//        Per far contento Baccus
        //textView2.setText((scontoApplicato != 0 ? "-" : "") + String.format("%.2f", scontoApplicato) + "€");
        textView2.setText("-" + String.format("%.2f", scontoApplicato) + "€");
        textView2.setTextSize(18f);
        LinearLayout.LayoutParams layoutParams22 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        layoutParams22.setMargins(0, (int) getActivity().getResources().getDimension(R.dimen.spacing_small), 0, (int) getActivity().getResources().getDimension(R.dimen.spacing_small));
        layoutParams22.weight = 1f;
        textView2.setLayoutParams(layoutParams22);
        linearLayout2.addView(textView2);

        linearLayout.addView(linearLayout1);
        linearLayout.addView(linearLayout2);
        scrollView.addView(linearLayout);
    }


    public float getTotCanoneMensile() {
        return totCanoneMensile;
    }

    public float getContributoUnaTan() {
        return contributoUnaTan;
    }

    public float getScontoApplicato() {
        return scontoApplicato;
    }

    @Override
    public boolean handleMessage(Message msg) {
        switch (msg.what) {
            case Constants.HandlerWhat.WHAT_MAIL:
                Utils.showGenericMessage(this.getActivity(), "INVIO MAIL", "Mail inviata con successo!");
                Log.i(TAG, "mail sent successfully!");
                break;

            case Constants.HandlerWhat.WHAT_MAIL_KO:
                Utils.showGenericMessage(getActivity(), "INVIO MAIL", "Errore nell'invio mail!");
                Log.i(TAG, "mail not sent!");
                break;
        }
        return false;
    }
}
