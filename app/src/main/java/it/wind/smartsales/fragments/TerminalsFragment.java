package it.wind.smartsales.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import it.wind.smartsales.R;
import it.wind.smartsales.activity.MainActivity;
import it.wind.smartsales.adapter.TerminalAdapter;
import it.wind.smartsales.adapter.TerminalPriceAdapter;
import it.wind.smartsales.commons.Utils;
import it.wind.smartsales.customviews.HorizontalSpaceItemDecoration;
import it.wind.smartsales.customviews.RecyclerItemClickListener;
import it.wind.smartsales.dao.CatalogoTerminaliDAO;
import it.wind.smartsales.dao.PraticheDAO;
import it.wind.smartsales.entities.Pratica;
import it.wind.smartsales.entities.Session;
import it.wind.smartsales.entities.Terminale;
import it.wind.smartsales.fragments.selectoffer.MobileInternetSelectOfferStep2Fragment;

/**
 * Created by giuseppe.mangino on 14/07/2016.
 */

public class TerminalsFragment extends Fragment implements SubHeaderFragment.SubHeaderItemClickListener,
        TerminalClickedFragment.OnUpdateDeviceListener {

    private RecyclerView devicesList;
    private RecyclerView devicesPriceList;
    private View selectedView;
    private TextView totalPriceLabel;
    private Button btnContinue;
    private Spinner spinnerBrand;
    private final ArrayList<Terminale> selectedDevices = new ArrayList<>();
    private ArrayList<Terminale> terminalList = new ArrayList<>();
    private final ArrayList<Terminale> originalList = CatalogoTerminaliDAO.getTerminalList(getActivity());
    private TextView labelDeviceDetail;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_terminals, container, false);
        final SubHeaderFragment subHeaderFragment =
                new SubHeaderFragment(getString(R.string.choice_device), "Home", true, false, this);
        getFragmentManager().beginTransaction().replace(R.id.subheader_fragment, subHeaderFragment).commit();
        return root;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        terminalList.addAll(originalList);

        labelDeviceDetail = (TextView) view.findViewById(R.id.device_detail_tv);
        totalPriceLabel = (TextView) view.findViewById(R.id.total_price_label);
        final ImageView icSmart = (ImageView) view.findViewById(R.id.ic_smart);
        final ImageView icTablet = (ImageView) view.findViewById(R.id.ic_tablet);
        final ImageView icMnp = (ImageView) view.findViewById(R.id.ic_mnp);
        final ImageView icNuovaLinea = (ImageView) view.findViewById(R.id.ic_nuova_linea);
        final ImageView icAcquista = (ImageView) view.findViewById(R.id.ic_acquista);
        final ImageView icOrderAsc = (ImageView) view.findViewById(R.id.ic_order_asc);
        final ImageView icOrderDesc = (ImageView) view.findViewById(R.id.ic_order_desc);
        final Spinner spinnerFilter = (Spinner) view.findViewById(R.id.spinner_filter);
        spinnerBrand = (Spinner) view.findViewById(R.id.spinner_brand);
        btnContinue = (Button) view.findViewById(R.id.btn_continue);
        devicesList = (RecyclerView) view.findViewById(R.id.devices_recycler_view);
        devicesPriceList = (RecyclerView) view.findViewById(R.id.list_device_price);

        spinnerBrand.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                final String label = ((TextView) view).getText().toString().toLowerCase();
                ArrayList<Terminale> tempList = new ArrayList<>();
                for (int x = 0; x < terminalList.size(); x++) {
                    if (TextUtils.equals(label, terminalList.get(x).getTerminaleBrand().toLowerCase())) {
                        tempList.add(terminalList.get(x));
                    }
                }
                terminalList.clear();
                terminalList.addAll(tempList);
                devicesList.setAdapter(new TerminalAdapter(terminalList));
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        spinnerFilter.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                final String label = ((TextView) view).getText().toString().toLowerCase();
                switch (label) {
                    case "nessun filtro":
                        updateSpinnerBrand(new ArrayList<Terminale>());
                        terminalList.clear();
                        terminalList.addAll(originalList);
                        break;
                    case "device selezionati":
                        updateSpinnerBrand(new ArrayList<Terminale>());
                        ArrayList<Terminale> tempList = new ArrayList<>();
                        for (int x = 0; x < terminalList.size(); x++) {
                            if (terminalList.get(x).isSelected()) {
                                tempList.add(terminalList.get(x));
                            }
                        }
                        terminalList.clear();
                        terminalList.addAll(tempList);
                        break;
                    case "marca":
                        updateSpinnerBrand(terminalList);
                        break;
                }
                devicesList.setAdapter(new TerminalAdapter(terminalList));
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        icSmart.setSelected(true);
        icSmart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!view.isSelected()) {
                    addDeviceByType(terminalList, "smartphone");
                } else {
                    removeDeviceByType(terminalList, "smartphone");
                }
                view.setSelected(!view.isSelected());
            }
        });

        icTablet.setSelected(true);
        icTablet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!view.isSelected()) {
                    addDeviceByType(terminalList, "tablet");
                } else {
                    removeDeviceByType(terminalList, "tablet");
                }
                devicesList.setAdapter(new TerminalAdapter(terminalList));
                view.setSelected(!view.isSelected());
            }
        });

        icMnp.setSelected(true);
        icMnp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                filterDeviceByRate(view, icNuovaLinea, icAcquista, false);
            }
        });

        icNuovaLinea.setSelected(true);
        icNuovaLinea.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                filterDeviceByRate(view, icMnp, icAcquista, true);
            }
        });

        icAcquista.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                icNuovaLinea.setSelected(false);
                icMnp.setSelected(false);

                for (int i = 0; i < terminalList.size(); i++) {
                    terminalList.get(i).setPrezzoListinoVisible(true);
                }
                final TerminalAdapter terminaliAdapter = new TerminalAdapter(terminalList);
                devicesList.setAdapter(terminaliAdapter);
                view.setSelected(!view.isSelected());
            }
        });

        icOrderAsc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!view.isSelected()) {
                    Collections.sort(terminalList, new RateComparatorAsc());
                    devicesList.setAdapter(new TerminalAdapter(terminalList));
                    view.setSelected(!view.isSelected());
                    icOrderDesc.setSelected(false);
                }
            }
        });

        icOrderDesc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!view.isSelected()) {
                    Collections.sort(terminalList, new RateComparatorDesc());
                    devicesList.setAdapter(new TerminalAdapter(terminalList));
                    view.setSelected(!view.isSelected());
                    icOrderAsc.setSelected(!view.isSelected());
                }
            }
        });

        btnContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (view.isSelected()) {
                    JSONObject jsonPratica = Utils.createPratica();
                    Pratica pratica = PraticheDAO.createPratica(getActivity(), jsonPratica, Session.getInstance().getDealer().getDealerCode());
                    Session.getInstance().setJsonPratica(jsonPratica, pratica);

                    Session.getInstance().getPratica().setStep(PraticheDAO.STEP1);
                    PraticheDAO.updatePratica(getActivity().getBaseContext());

                    ((MainActivity) getActivity()).increaseDraftNumber();

                    ArrayList<String> idDevices = new ArrayList<>();
                    for (int i = 0; i < selectedDevices.size(); i++) {
                        idDevices.add(selectedDevices.get(i).getIdTerminale());
                    }
                    Bundle b = new Bundle();
                    b.putSerializable("FROM_TERMINALS", idDevices);
                    MobileInternetSelectOfferStep2Fragment fragment = new MobileInternetSelectOfferStep2Fragment();
                    fragment.setArguments(b);
                    getFragmentManager().beginTransaction()
                            .replace(R.id.fragment_container, fragment)
                            .addToBackStack(fragment.getClass().getName()).commit();
                }
            }
        });

        initializeRecyclerView(devicesPriceList, LinearLayoutManager.VERTICAL);
        initializeRecyclerView(devicesList, LinearLayoutManager.HORIZONTAL);

        devicesList.setAdapter(new TerminalAdapter(terminalList));
        devicesList.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(),
                new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        ViewGroup parent = (ViewGroup) labelDeviceDetail.getParent();
                        if (parent != null) {
                            parent.removeView(labelDeviceDetail);
                        }
                        final Terminale current = terminalList.get(position);
                        selectedView = view;
                        TerminalClickedFragment fragment = new TerminalClickedFragment(current);
                        getFragmentManager().beginTransaction().replace(R.id.phone_container, fragment).commit();
                    }
                })
        );
    }

    private void initializeRecyclerView(RecyclerView list, int orientation) {
        list.setHasFixedSize(true);
        list.setLayoutManager(new LinearLayoutManager(getActivity(), orientation, false));
        list.setItemAnimator(new DefaultItemAnimator());
        list.addItemDecoration(new HorizontalSpaceItemDecoration(20));
    }

    private void addDeviceByType(ArrayList<Terminale> terminalList, String type) {
        for (int i = 0; i < originalList.size(); i++) {
            Terminale t = originalList.get(i);
            if (TextUtils.equals(type.toLowerCase(), t.getDeviceType().toLowerCase())) {
                terminalList.add(t);
            }
        }
        devicesList.setAdapter(new TerminalAdapter(terminalList));
    }

    private void removeDeviceByType(ArrayList<Terminale> terminalList, String type) {
        final ArrayList<Terminale> newList = new ArrayList<>();
        for (int i = 0; i < originalList.size(); i++) {
            Terminale t = originalList.get(i);
            if (TextUtils.equals(type.toLowerCase(), t.getDeviceType().toLowerCase())) {
                newList.add(t);
            }
        }
        terminalList.removeAll(newList);
        devicesList.setAdapter(new TerminalAdapter(terminalList));
    }

    private void filterDeviceByRate(View v, ImageView otherRate, ImageView icAcquista, boolean isNuovaLinea) {
        if (otherRate.isSelected() || icAcquista.isSelected()) {
            for (int i = 0; i < terminalList.size(); i++) {
                if (!isNuovaLinea) {
                    terminalList.get(i).setRataMnpVisible(!v.isSelected());
                    terminalList.get(i).setRataVisible(otherRate.isSelected());

                } else {
                    terminalList.get(i).setRataVisible(!v.isSelected());
                    terminalList.get(i).setRataMnpVisible(otherRate.isSelected());
                }
                terminalList.get(i).setPrezzoListinoVisible(false);
            }
            devicesList.setAdapter(new TerminalAdapter(terminalList));
            icAcquista.setSelected(false);
            v.setSelected(!v.isSelected());
        }
    }

    private void updateSpinnerBrand(ArrayList<Terminale> terminalList) {
        final ArrayList<String> arrayBrand = new ArrayList<>();
        for (int i = 0; i < terminalList.size(); i++) {
            if (!arrayBrand.contains(terminalList.get(i).getTerminaleBrand()))
                arrayBrand.add(terminalList.get(i).getTerminaleBrand());
        }
        final ArrayAdapter<String> spinnerArrayAdapter =
                new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, arrayBrand);
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerBrand.setAdapter(spinnerArrayAdapter);
    }

    @Override
    public void onBackListener() {
        getActivity().onBackPressed();
    }

    @Override
    public void onSearchListener(String text) {
        ArrayList<Terminale> newList = new ArrayList<>();
        for (int i = 0; i < originalList.size(); i++) {
            final String brand = (originalList.get(i).getTerminaleBrand().toLowerCase());
            final String shortDesc = (originalList.get(i).getShortDesc().toLowerCase());
            if (brand.contains(text.toLowerCase()) || shortDesc.contains(text.toLowerCase())) {
                newList.add(originalList.get(i));
            }
        }
        terminalList = newList;
        final TerminalAdapter terminaliAdapter = new TerminalAdapter(terminalList);
        devicesList.setAdapter(terminaliAdapter);
    }

    @Override
    public void onNoteListener() {

    }

    @Override
    public void onUpdateDevice(Terminale terminal) {
        updateSelectedDevicesList(terminal);

        final int totalCount = terminal.getSelectedTerminals() + terminal.getSelectedTerminalsMnp();
        selectedView.setSelected(terminal.isSelected());
        ((TextView) selectedView.findViewById(R.id.total_count)).setText(String.valueOf(totalCount));
        btnContinue.setSelected(totalCount > 0 ? true : false);

        totalPriceLabel.setText(String.valueOf(getTotalPriceSelectedDevices()) + getString(R.string.euro_price_suffix));
    }

    private void updateSelectedDevicesList(Terminale terminal) {
        if (selectedDevices.contains(terminal)) {
            final int index = selectedDevices.indexOf(terminal);
            selectedDevices.remove(index);
            selectedDevices.add(index, terminal);
        } else {
            selectedDevices.add(terminal);
        }
        devicesPriceList.setAdapter(new TerminalPriceAdapter(selectedDevices));
    }

    private int getTotalPriceSelectedDevices() {
        int totalPrice = 0;
        for (Terminale t : selectedDevices) {
            final String rataMensileMnp = t.getRataMensileMnp().replace(getString(R.string.euro_price_suffix), "");
            final int totalRataMensileMnp = t.getSelectedTerminalsMnp() * Integer.valueOf(rataMensileMnp);
            totalPrice += (totalRataMensileMnp + t.getSelectedTerminals() * Integer.valueOf(t.getRataMensile()));
        }
        return totalPrice;
    }

    private class RateComparatorDesc implements Comparator<Terminale> {
        @Override
        public int compare(Terminale e1, Terminale e2) {
            if (Integer.valueOf(e1.getRataMensile()) < Integer.valueOf(e2.getRataMensile())) {
                return 1;
            } else {
                return -1;
            }
        }
    }

    private class RateComparatorAsc implements Comparator<Terminale> {
        @Override
        public int compare(Terminale e1, Terminale e2) {
            if (Integer.valueOf(e1.getRataMensile()) > Integer.valueOf(e2.getRataMensile())) {
                return 1;
            } else {
                return -1;
            }
        }
    }
}