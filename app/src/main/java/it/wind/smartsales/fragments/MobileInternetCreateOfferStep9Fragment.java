package it.wind.smartsales.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.hardware.SensorManager;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.OrientationEventListener;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import it.wind.smartsales.R;
import it.wind.smartsales.commons.Constants;
import it.wind.smartsales.commons.FileExplore;
import it.wind.smartsales.commons.Log;
import it.wind.smartsales.commons.Utils;
import it.wind.smartsales.customviews.AspectRatioView;
import it.wind.smartsales.dao.PraticheDAO;
import it.wind.smartsales.entities.OffertaMobile;
import it.wind.smartsales.entities.OpzioneMobile;
import it.wind.smartsales.entities.Session;
import it.wind.smartsales.task.PreviewTask;
import it.wind.smartsales.task.SendContractTask;

/**
 * Created by luca.quaranta on 10/02/2016.
 */
public class MobileInternetCreateOfferStep9Fragment extends Fragment implements Handler.Callback {

    private static final String TAG = "MobileInternetCreateOfferStep9Fragment";
    public AlertDialog builder;
    boolean isPreview, isSendMail, isFirma;
    int rotation = 0;
    String path;
    private MobileInternetCreateOfferStep9Fragment mobileInternetCreateOfferStep9Fragment = this;
    private LinearLayout documentoRappresentanteLegale, delegaRappresentanteLegale, documentoDelegato, fatturaOperatore, aggiungiDocumento;
    private Button previewButton, invioButton, procediButton;
    private AspectRatioView fatturaOperatoreImage;
    private TextView fatturaOperatoreText;
    private View.OnClickListener documentClickListener;
    private View viewClicked;
    private ArrayList<OffertaMobile> offertaMobileArrayList;
    private EditText toaddressText;
    private EditText ccaddressText;
    private LinearLayout ccAddressLayout;
    private EditText bccAddressText;
    private LinearLayout bccAddressLayout;
    private ImageButton ccAddressCrossButton;
    private ImageButton bccAddressCrossButton;
    private LinearLayout addMailAddressLayout;
    private TextView emailAgenteText;
    private TextView emailAgenziaText;
    private AspectRatioView addMailAddressButton;
    private float numEmailVisible = 0;
    private String pathFotoRappresentanteLegale;
    private String pathFotoDelegaRappresentanteLegale;
    private String pathFotoDocumenteoDelegato;
    private String pathFotoFatturaOperatore;
    private String pathFotoAggiungiDoc;

    public static boolean copyFile(File source, File dest) {
        BufferedInputStream bis = null;
        BufferedOutputStream bos = null;

        try {
            bis = new BufferedInputStream(new FileInputStream(source));
            bos = new BufferedOutputStream(new FileOutputStream(dest, false));

            byte[] buf = new byte[1024];
            bis.read(buf);

            do {
                bos.write(buf);
            } while (bis.read(buf) != -1);
        } catch (IOException e) {
            return false;
        } finally {
            try {
                if (bis != null) bis.close();
                if (bos != null) bos.close();
            } catch (IOException e) {
                return false;
            }
        }

        return true;
    }

    private void initializeComponents(View view) {
        documentoRappresentanteLegale = (LinearLayout) view.findViewById(R.id.documento_rappresentante_legale);
        delegaRappresentanteLegale = (LinearLayout) view.findViewById(R.id.delega_rappresentante_legale);
        documentoDelegato = (LinearLayout) view.findViewById(R.id.documento_delegato);
        fatturaOperatore = (LinearLayout) view.findViewById(R.id.fattura_operatore);
        aggiungiDocumento = (LinearLayout) view.findViewById(R.id.aggiungi_documento);

        fatturaOperatoreImage = (AspectRatioView) view.findViewById(R.id.fattura_operatore_image);
        fatturaOperatoreText = (TextView) view.findViewById(R.id.fattura_operatore_text);

        previewButton = (Button) view.findViewById(R.id.preview_button);
        invioButton = (Button) view.findViewById(R.id.invio_button);
        procediButton = (Button) view.findViewById(R.id.procedi_button);

        documentClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewClicked = v;

                if (viewClicked.getId() == R.id.documento_rappresentante_legale) {
                    showGenericMessageRappresentanteLegale(getActivity(), "Allega Documento", "Scegli come allegare una foto.");
                } else if (viewClicked.getId() == R.id.delega_rappresentante_legale) {
                    showGenericMessageDelegaRappresentanteLegale(getActivity(), "Allega Documento", "Scegli come allegare una foto.");
                } else if (viewClicked.getId() == R.id.documento_delegato) {
                    showGenericMessageFotoDocDelegato(getActivity(), "Allega Documento", "Scegli come allegare una foto.");
                } else if (viewClicked.getId() == R.id.fattura_operatore) {
                    showGenericMessageFotoFattura(getActivity(), "Allega Documento", "Scegli come allegare una foto.");
                } else if (viewClicked.getId() == R.id.aggiungi_documento) {
                    showGenericMessageAggiungiDoc(getActivity(), "Allega Documento", "Scegli come allegare una foto.");
                }


            }
        };

        documentoRappresentanteLegale.setOnClickListener(documentClickListener);
        delegaRappresentanteLegale.setOnClickListener(documentClickListener);
        documentoDelegato.setOnClickListener(documentClickListener);
        fatturaOperatore.setOnClickListener(documentClickListener);
        aggiungiDocumento.setOnClickListener(documentClickListener);


        //sezione indirizzi email
        toaddressText = (EditText) view.findViewById(R.id.to_address);
        ccaddressText = (EditText) view.findViewById(R.id.cc_address);
        ccAddressLayout = (LinearLayout) view.findViewById(R.id.cc_address_layout);
        bccAddressText = (EditText) view.findViewById(R.id.bcc_address);
        bccAddressLayout = (LinearLayout) view.findViewById(R.id.bcc_address_layout);
        ccAddressCrossButton = (ImageButton) view.findViewById(R.id.cc_address_cross);
        bccAddressCrossButton = (ImageButton) view.findViewById(R.id.bcc_address_cross);
        addMailAddressLayout = (LinearLayout) view.findViewById(R.id.add_mail_address_layout);
        addMailAddressButton = (AspectRatioView) view.findViewById(R.id.add_mail_address);
        emailAgenteText = (TextView) view.findViewById(R.id.email_agente);
        emailAgenziaText = (TextView) view.findViewById(R.id.email_agenzia);


    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_mobile_internet_create_offer_step_9, container, false);
        initializeComponents(view);

        try {
            JSONArray offerteRaggruppate = (JSONArray) Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONArray("offerteConfigurate");
            JSONObject offerteRaggruppate1 =  Session.getInstance().getJsonPratica().getJSONObject("pratica");

            offertaMobileArrayList = new ArrayList<>();
            for (int i = 0; i < offerteRaggruppate.length(); i++) {
                offertaMobileArrayList.add(Utils.createOffertaMobile(offerteRaggruppate.getJSONObject(i)));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (!hasMnp()) {
            fatturaOperatore.setEnabled(false);
            fatturaOperatoreImage.setEnabled(false);
            fatturaOperatoreText.setEnabled(false);
        }

        toaddressText.setText(emailReferente());
        previewButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isPreview = true;
                isSendMail = false;
                isFirma = false;
                String xml = generateXML("");

                PreviewTask.callTask(getActivity(), mobileInternetCreateOfferStep9Fragment, xml);

                builder = Utils.showGenericMessageWithReturn(getActivity(), "GENERAZIONE PREVIEW", "Generazione preview in corso....");


            }
        });

        invioButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isPreview = false;
                isSendMail = true;
                isFirma = false;

                if ((toaddressText.getText().toString().trim() + ccaddressText.getText().toString().trim() + bccAddressText.getText().toString().trim()).equalsIgnoreCase("")) {
                    Utils.showGenericMessage(getActivity(), "ATTENZIONE", "Inserire almeno un indirizzo di destinazione!");
                } else {
                    String summary = Session.getInstance().getContractContent(toaddressText.getText().toString().trim(), ccaddressText.getText().toString().trim(), bccAddressText.getText().toString().trim());
                    SendContractTask.callTask(getActivity(), mobileInternetCreateOfferStep9Fragment, generateXML(summary));
//                SendMailTask.callTask(getActivity().getBaseContext(), mobileInternetCreateOfferStep9Fragment, Session.getInstance().getMailContent(toaddressText.getText().toString().trim(), ccaddressText.getText().toString().trim(), bccAddressText.getText().toString().trim()));
                    Session.getInstance().getPratica().setStep(PraticheDAO.STEP10);
                    PraticheDAO.updatePratica(getActivity().getBaseContext());
                }


            }
        });

        procediButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isPreview = false;
                isSendMail = false;
                isFirma = true;
            }
        });

        //sezione indirizzi email
        addMailAddressButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (ccAddressLayout.getVisibility() == View.GONE) {
                    ccAddressLayout.setVisibility(View.VISIBLE);
                    numEmailVisible++;
                } else if (bccAddressLayout.getVisibility() == View.GONE) {
                    bccAddressLayout.setVisibility(View.VISIBLE);
                    numEmailVisible++;
                }

                if (numEmailVisible == 2) {
                    addMailAddressLayout.setVisibility(View.GONE);
                }

            }
        });

        ccAddressCrossButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                numEmailVisible--;
                ccAddressLayout.setVisibility(View.GONE);
                ccaddressText.setText("");
                addMailAddressLayout.setVisibility(View.VISIBLE);
            }
        });

        bccAddressCrossButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                numEmailVisible--;
                bccAddressLayout.setVisibility(View.GONE);
                bccAddressText.setText("");
                addMailAddressLayout.setVisibility(View.VISIBLE);
            }
        });


        emailAgenteText.setText(Session.getInstance().getDealer().getDealerMail());
        emailAgenziaText.setText(Session.getInstance().getDealer().getEmailBackOffice());

        return view;
    }

    private String emailReferente() {
        String email="";
        try {
            JSONObject inserimentoDati = Session.getInstance().getJsonPratica().getJSONObject("pratica").optJSONObject("inserimentoDati");

                JSONObject referenteContratto = inserimentoDati.getJSONObject("referenteContratto");
                email=referenteContratto.optString("email");


        } catch (JSONException e) {
            e.printStackTrace();
        }

        return email;
    }

    private void changeRot(int r) {
        rotation = r;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == 5) {
            if (resultCode == Activity.RESULT_OK) {
                OrientationEventListener myOrientationEventListener = new OrientationEventListener(getActivity(), SensorManager.SENSOR_DELAY_NORMAL) {

                    @Override
                    public void onOrientationChanged(int r) {
                        // TODO Auto-generated method stub
                        changeRot(r);
                    }
                };

                if (myOrientationEventListener.canDetectOrientation()) {
                    myOrientationEventListener.enable();
                } else {
                    myOrientationEventListener.disable();
                }

                storePhotoPath(path);

                Bitmap bitmapToConvert = BitmapFactory.decodeFile(path);
                ExifInterface exif;
                try {
                    exif = new ExifInterface(path);

                    int exifOrientation = exif.getAttributeInt(
                            ExifInterface.TAG_ORIENTATION,
                            ExifInterface.ORIENTATION_NORMAL);

                    int rotate = 0;

                    switch (exifOrientation) {
                        case ExifInterface.ORIENTATION_ROTATE_90:
                            rotate = 90;
                            break;
                        case ExifInterface.ORIENTATION_ROTATE_180:
                            rotate = 180;
                            break;
                        case ExifInterface.ORIENTATION_ROTATE_270:
                            rotate = 270;
                            break;
                    }

                    if (rotate != 0) {
                        int w = bitmapToConvert.getWidth();
                        int h = bitmapToConvert.getHeight();

                        // Setting pre rotate
                        Matrix mtx = new Matrix();
                        mtx.preRotate(rotate);

                        // Rotating Bitmap & convert to ARGB_8888
                        bitmapToConvert = Bitmap.createBitmap(bitmapToConvert, 0, 0, w, h, mtx, false);
                        bitmapToConvert = bitmapToConvert.copy(Bitmap.Config.ARGB_8888, true);
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                }

                Bitmap convertedBitmap = convertColorImageBlackAndWhite(bitmapToConvert);


                FileOutputStream fos;
                try {
                    fos = new FileOutputStream(path);
                    if (convertedBitmap != null) {
                        convertedBitmap.compress(Bitmap.CompressFormat.JPEG, 70, fos);
                    }


                    fos.flush();
                    fos.close();

//                    dbHelper.saveAttachment(javaInterface.getAttachmentCustomerId(),javaInterface.getAttachmentPath(),javaInterface.getAttachmentType(),javaInterface.getAttachmentCarrelloId());
//                    javaInterface.loadAttachment(javaInterface.getAttachmentCustomerId(), javaInterface.getAttachmentType(),javaInterface.getAttachmentCarrelloId());

                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                viewClicked.setSelected(true);
                viewClicked = null;
            }
        } else if (requestCode == 6) {
            //--- ISCTF00114392 start
            if (data != null) {
                String path = data.getStringExtra("path");
                String ppp = data.getDataString();
                Log.d("ATTACHMENTCONVERSION", "path:" + path);
                File fileToCopy = new File(path);
                Date curDate = new Date();
                long curMillis = curDate.getTime();

                String nameFile = path.substring(path.lastIndexOf("/"));
                Log.d("ATTACHMENTCONVERSION", nameFile);
                String extension = nameFile.substring(nameFile.lastIndexOf("."));
                Log.d("ATTACHMENTCONVERSION", extension);

                //ISCTF00109842   starts
                if (fileToCopy.getAbsolutePath().toLowerCase().endsWith(".jpg")
                        || fileToCopy.getAbsolutePath().toLowerCase().endsWith(".jpeg")
                        || fileToCopy.getAbsolutePath().toLowerCase().endsWith(".bmp")
                        || fileToCopy.getAbsolutePath().toLowerCase().endsWith(".gif")
                        ) {

                    File fileCopied;

//                    if (path.contains("/VDFN/Pictures")) {
                    if (path.contains("/Pictures")) {
                        fileCopied = fileToCopy;
                    } else {
//                        fileCopied = new File(Environment.getExternalStorageDirectory(), "/VDFN/Pictures/" + curMillis + extension);
                        fileCopied = new File(Environment.getExternalStorageDirectory(), "/Pictures/CP" + curMillis + extension);

                        copyFile(fileToCopy, fileCopied);

                        Log.d("ATTACHMENTCONVERSION", "fileCopied.getAbsolutePath() [" + fileCopied.getAbsolutePath() + "]");
                        //ISCTF00113223
                        Bitmap bitmapToConvert = null;
                        if ((fileToCopy.getAbsolutePath().toLowerCase().endsWith(".jpg")
                                || fileToCopy.getAbsolutePath().toLowerCase().endsWith(".jpeg")) && fileToCopy.length() > 1024 * 1024) {
                            bitmapToConvert = loadResizedBitmap(fileCopied.getAbsolutePath(), 960, 720, true);
                        } else {
                            bitmapToConvert = BitmapFactory.decodeFile(fileCopied.getAbsolutePath());
                        }

                        Bitmap convertedBitmap = convertColorImageBlackAndWhiteFromGallery(bitmapToConvert);

                        FileOutputStream fos;
                        try {
                            fos = new FileOutputStream(fileCopied.getAbsolutePath());
                            if (convertedBitmap != null) {
                                convertedBitmap.compress(Bitmap.CompressFormat.JPEG, 70, fos);
                            }


                            fos.flush();
                            fos.close();

                        } catch (FileNotFoundException e) {
                            Log.i(TAG, e.getMessage());
                        } catch (IOException e) {
                            Log.i(TAG, e.getMessage());
                        }
                    }

                    Log.d("ATTACHMENTCONVERSION", fileCopied.getAbsolutePath());
                    Log.d("ATTACHMENTCONVERSION", fileCopied.getName());

                    storePhotoPath(fileCopied.getAbsolutePath());

//                    dbHelper.saveAttachment(javaInterface.getAttachmentCustomerId(),"/sdcard/VDFN/Pictures/" + fileCopied.getName(),javaInterface.getAttachmentType(),javaInterface.getAttachmentCarrelloId());
//                    javaInterface.loadAttachment(javaInterface.getAttachmentCustomerId(), javaInterface.getAttachmentType(),javaInterface.getAttachmentCarrelloId());

                } else if (fileToCopy.length() < 1024 * 1024) {

                    File fileCopied;

//                    if (path.contains("/VDFN/Pictures")) {
                    if (path.contains("/Pictures")) {
                        fileCopied = fileToCopy;
                    } else {
//                        fileCopied = new File(Environment.getExternalStorageDirectory(), "/VDFN/Pictures/" + curMillis + extension);
                        fileCopied = new File(Environment.getExternalStorageDirectory(), "/Pictures/CP" + curMillis + extension);
                        copyFile(fileToCopy, fileCopied);
                        Log.d("ATTACHMENTCONVERSION", "fileCopied.getAbsolutePath() [" + fileCopied.getAbsolutePath() + "]");
                    }

                    storePhotoPath(fileCopied.getAbsolutePath());

//                    dbHelper.saveAttachment(javaInterface.getAttachmentCustomerId(),"/sdcard/VDFN/Pictures/" + fileCopied.getName(),javaInterface.getAttachmentType(),javaInterface.getAttachmentCarrelloId());
//                    javaInterface.loadAttachment(javaInterface.getAttachmentCustomerId(), javaInterface.getAttachmentType(),javaInterface.getAttachmentCarrelloId());

                }    // ISCTF00109842 ends
                else {
//                    mWebView.loadUrl("javascript:$(\"#checkAttachmentSize\").popup(\"open\");");
                }
                viewClicked.setSelected(true);
                viewClicked = null;
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    private Bitmap convertColorImageBlackAndWhiteFromGallery(Bitmap origBitmap) {
        ColorMatrix colorMatrix = new ColorMatrix();
        colorMatrix.setSaturation(0);

        ColorMatrixColorFilter colorMatrixFilter = new ColorMatrixColorFilter(
                colorMatrix);
        Bitmap blackAndWhiteBitmap = null;

        if (origBitmap != null) {
            int dstWidth = 960;
            int dstHeight = 720;

            //Toast.makeText(this, "Can DetectOrientation " + rotation +" height:"+ origBitmap.getHeight() + " width:"+origBitmap.getWidth(), Toast.LENGTH_LONG).show();
            //ISCTF00110214
            if (origBitmap.getHeight() > origBitmap.getWidth()) {
                dstWidth = 720;
                dstHeight = 960;
            }


            blackAndWhiteBitmap = origBitmap.copy(Bitmap.Config.ARGB_8888, true);
            blackAndWhiteBitmap = Bitmap.createScaledBitmap(blackAndWhiteBitmap, dstWidth, dstHeight, true);


            Paint paint = new Paint();
            paint.setColorFilter(colorMatrixFilter);

            Canvas canvas = new Canvas(blackAndWhiteBitmap);
            canvas.drawBitmap(blackAndWhiteBitmap, 0, 0, paint);
        }


        return blackAndWhiteBitmap;
    }

    public Bitmap loadResizedBitmap(String filename, int width, int height, boolean exact) {
        Bitmap bitmap = null;
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(filename, options);

        if (options.outHeight > 0 && options.outWidth > 0) {
            options.inJustDecodeBounds = false;
            options.inSampleSize = 2;
            Log.d("ATTACHMENTCONVERSION", "ATTACHMENTCONVERSION options [" + options.outHeight + "; " + options.outWidth + "]");
            if (options.outHeight > options.outWidth) {
                width = 720;
                height = 960;
            }
            while (options.outWidth / options.inSampleSize > width
                    && options.outHeight / options.inSampleSize > height) {
                options.inSampleSize++;
            }
            options.inSampleSize--;

            bitmap = BitmapFactory.decodeFile(filename, options);
            if (bitmap != null && exact) {
                //ISCTF00110214
                Log.d("ATTACHMENTCONVERSION", "ATTACHMENTCONVERSION [" + bitmap.getHeight() + "; " + bitmap.getWidth() + "]");
                Log.d("ATTACHMENTCONVERSION", "ATTACHMENTCONVERSION2 [" + bitmap.getHeight() + "; " + bitmap.getWidth() + "]");
                bitmap = Bitmap.createScaledBitmap(bitmap, width, height, false);
            }
        }
        return bitmap;
    }

    private Bitmap convertColorImageBlackAndWhite(Bitmap origBitmap) {
        ColorMatrix colorMatrix = new ColorMatrix();
        colorMatrix.setSaturation(0);

        ColorMatrixColorFilter colorMatrixFilter = new ColorMatrixColorFilter(
                colorMatrix);
        Bitmap blackAndWhiteBitmap = null;

        if (origBitmap != null) {
            getResources().getConfiguration();

            int dstWidth = 960;
            int dstHeight = 720;

            //Toast.makeText(this, "Can DetectOrientation " + rotation +" height:"+ origBitmap.getHeight() + " width:"+origBitmap.getWidth(), Toast.LENGTH_LONG).show();
            //ISCTF00110214
            if (origBitmap.getHeight() > origBitmap.getWidth()) {
                dstWidth = 720;
                dstHeight = 960;
            }


            blackAndWhiteBitmap = origBitmap.copy(Bitmap.Config.ARGB_8888, true);
            blackAndWhiteBitmap = Bitmap.createScaledBitmap(blackAndWhiteBitmap, dstWidth, dstHeight, true);


            Paint paint = new Paint();
            paint.setColorFilter(colorMatrixFilter);

            Canvas canvas = new Canvas(blackAndWhiteBitmap);
            canvas.drawBitmap(blackAndWhiteBitmap, 0, 0, paint);
        }


        return blackAndWhiteBitmap;
    }

    private void dispatchCamera() {
        takePhoto();
    }

    private boolean hasMnp() {
        for (OffertaMobile offertaMobile : offertaMobileArrayList) {
            if (offertaMobile.isMnp()) {
                return true;
            }
        }
        return false;
    }

    public void takePhoto() {
        // start camera
        Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
        Date curDate = new Date();
        long curMillis = curDate.getTime();
        File photo = new File(Environment.getExternalStorageDirectory(), "/Pictures/" + curMillis + ".jpg");
        path = photo.getAbsolutePath();


        Log.d("JS", "path:" + photo.getAbsolutePath());


        intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photo));
        startActivityForResult(intent, 5);
    }

    public void pickImage() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");
        startActivityForResult(intent, 6);
    }
    public void takeFile() {

        Intent intent = new Intent(getActivity(), FileExplore.class);
        startActivityForResult(intent, 6);

    }

    private void storePhotoPath(String path)
    {
        if(viewClicked.getId()==R.id.documento_rappresentante_legale)
        {
            pathFotoRappresentanteLegale =path;
        }
        else if(viewClicked.getId()==R.id.delega_rappresentante_legale)
        {
            pathFotoDelegaRappresentanteLegale =path;
        }
        else if(viewClicked.getId()==R.id.documento_delegato)
        {
            pathFotoDocumenteoDelegato =path;
        }
        else if(viewClicked.getId()==R.id.fattura_operatore)
        {
            pathFotoFatturaOperatore =path;
        }
        else if(viewClicked.getId()==R.id.aggiungi_documento)
        {
            pathFotoAggiungiDoc =path;
        }
    }

    public void showGenericMessageRappresentanteLegale(final Context context, String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title);
        builder.setMessage(message);
        if(pathFotoRappresentanteLegale!=null) {
            builder.setNeutralButton("Elimina Foto", new android.content.DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                    try {
                        File file = new File(pathFotoRappresentanteLegale);
                        viewClicked.setSelected(false);
                        file.delete();
                        pathFotoRappresentanteLegale = null;
                    } catch (Exception e) {
                        Log.i(TAG, e.getMessage());
                    }

                    dialog.dismiss();
                }
            });
        }
        builder.setPositiveButton("Fotocamera", new android.content.DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                takePhoto();
                dialog.dismiss();
            }
        });
        builder.setNegativeButton("Galleria", new android.content.DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                takeFile();
                //pickImage();
                dialog.dismiss();
            }
        });
        builder.show();
    }

    public void showGenericMessageDelegaRappresentanteLegale(final Context context, String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title);
        builder.setMessage(message);
        if(pathFotoDelegaRappresentanteLegale!=null) {
            builder.setNeutralButton("Elimina Foto", new android.content.DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                    try {
                        File file = new File(pathFotoDelegaRappresentanteLegale);
                        viewClicked.setSelected(false);
                        file.delete();
                        pathFotoDelegaRappresentanteLegale = null;
                    } catch (Exception e) {
                        Log.i(TAG, e.getMessage());
                    }

                    dialog.dismiss();
                }
            });
        }
        builder.setPositiveButton("Fotocamera", new android.content.DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                takePhoto();
                dialog.dismiss();
            }
        });
        builder.setNegativeButton("Galleria", new android.content.DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                takeFile();
                //pickImage();
                dialog.dismiss();
            }
        });
        builder.show();
    }

    public void showGenericMessageFotoDocDelegato(final Context context, String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title);
        builder.setMessage(message);
        if(pathFotoDocumenteoDelegato!=null) {
            builder.setNeutralButton("Elimina Foto", new android.content.DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                    try {
                        File file = new File(pathFotoDocumenteoDelegato);
                        viewClicked.setSelected(false);
                        file.delete();
                        pathFotoDocumenteoDelegato = null;
                    } catch (Exception e) {
                        Log.i(TAG, e.getMessage());
                    }

                    dialog.dismiss();
                }
            });
        }
        builder.setPositiveButton("Fotocamera", new android.content.DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                takePhoto();
                dialog.dismiss();
            }
        });
        builder.setNegativeButton("Galleria", new android.content.DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                takeFile();
                //pickImage();
                dialog.dismiss();
            }
        });
        builder.show();
    }

    public void showGenericMessageFotoFattura(final Context context, String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title);
        builder.setMessage(message);
        if(pathFotoFatturaOperatore!=null) {
            builder.setNeutralButton("Elimina Foto", new android.content.DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                    try {
                        File file = new File(pathFotoFatturaOperatore);
                        viewClicked.setSelected(false);
                        file.delete();
                        pathFotoFatturaOperatore = null;
                    } catch (Exception e) {
                        Log.i(TAG, e.getMessage());
                    }

                    dialog.dismiss();
                }
            });
        }
        builder.setPositiveButton("Fotocamera", new android.content.DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                takePhoto();
                dialog.dismiss();
            }
        });
        builder.setNegativeButton("Galleria", new android.content.DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                takeFile();
                //pickImage();
                dialog.dismiss();
            }
        });
        builder.show();
    }

    public void showGenericMessageAggiungiDoc(final Context context, String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title);
        builder.setMessage(message);
        if(pathFotoAggiungiDoc!=null) {
            builder.setNeutralButton("Elimina Foto", new android.content.DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                    try {
                        File file = new File(pathFotoAggiungiDoc);
                        viewClicked.setSelected(false);
                        file.delete();
                        pathFotoAggiungiDoc = null;
                    } catch (Exception e) {
                        Log.i(TAG, e.getMessage());
                    }

                    dialog.dismiss();
                }
            });
        }
        builder.setPositiveButton("Fotocamera", new android.content.DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                takePhoto();
                dialog.dismiss();
            }
        });
        builder.setNegativeButton("Galleria", new android.content.DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                takeFile();
                //pickImage();
                dialog.dismiss();
            }
        });
        builder.show();
    }

    private String generateXML(String summary) {
        try {

            String xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                    "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ins=\"http://insertpda.ws.wind.sfa.accenture.com/\">\n" +
                    "<soapenv:Header/>\n" +
                    "<soapenv:Body>" +
                    "<ins:InsertPdaRequestReq>";

//        InsertPdaAnagrafica_Input [INIZIO]
            xml += "<InsertPdaAnagrafica_Input>";

            xml += "<VariazioneTecnica>" + "false" + "</VariazioneTecnica>";

            xml += "<Dealer>";
            xml += "<DealerName>" + Session.getInstance().getDealer().getDealerName() + " " + Session.getInstance().getDealer().getDealerSurname() + "</DealerName>";
            xml += "<DealerUsername>" + Session.getInstance().getDealer().getUsername() + "</DealerUsername>";
            xml += "<DealerPwd>" + Session.getInstance().getDealer().getPassword() + "</DealerPwd>";
            xml += "<ClientManagerName>" + Session.getInstance().getDealer().getNomeClientManager() + "</ClientManagerName>";
            xml += "</Dealer>";

            xml += "<Customer>";

//            xml += "<CustomerCode>"+ "</CustomerCode>";

            xml += "<CustomerName>" + Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("inserimentoDati").getJSONObject("datiCliente").getString("nome") + " " +
                    Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("inserimentoDati").getJSONObject("datiCliente").getString("cognome") + " " +
                    Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("inserimentoDati").getJSONObject("datiCliente").getString("ragioneSociale") + "</CustomerName>";
            xml += "<CompanyType>" + Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("inserimentoDati").getJSONObject("datiCliente").getString("firmaSocietaria") + "</CompanyType>";
            xml += "<ProvinceCCIA>" + Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("inserimentoDati").getJSONObject("datiCliente").getString("provinciaCCIAA") + "</ProvinceCCIA>";
            xml += "<PartitaIVA>" + Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("inserimentoDati").getJSONObject("datiCliente").getString("p_iva") + "</PartitaIVA>";

            xml += "<Delegate>";
            xml += "<FirstName>" + Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("inserimentoDati").getJSONObject("datiRappresentanteLegale").getString("nome") + "</FirstName>";
            xml += "<LastName>" + Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("inserimentoDati").getJSONObject("datiRappresentanteLegale").getString("cognome") + "</LastName>";
            xml += "<PhoneNumber>" + Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("inserimentoDati").getJSONObject("datiRappresentanteLegale").getString("telefono") + "</PhoneNumber>";
            xml += "<FaxNumber>" + Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("inserimentoDati").getJSONObject("datiRappresentanteLegale").optString("fax") + "</FaxNumber>";
            xml += "<EmailAddress>" + Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("inserimentoDati").getJSONObject("datiRappresentanteLegale").optString("email") + "</EmailAddress>";
            xml += "<CodiceFiscale>" + Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("inserimentoDati").getJSONObject("datiRappresentanteLegale").optString("cf") + "</CodiceFiscale>";
            xml += "<BirthCity>" + Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("inserimentoDati").getJSONObject("datiRappresentanteLegale").optString("luogoNascita") + "</BirthCity>";
            xml += "<BirthProvince>" + Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("inserimentoDati").getJSONObject("datiRappresentanteLegale").optString("provincia") + "</BirthProvince>";

//            TODO: gestire data
            String data = Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("inserimentoDati").getJSONObject("datiRappresentanteLegale").optString("dataNascita");
            xml += "<BirthDate>" + (new SimpleDateFormat("yyyy-MM-dd").format(new SimpleDateFormat("dd/MM/yyyy").parse(data))) + "</BirthDate>";
            xml += "<Citizenship>" + Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("inserimentoDati").getJSONObject("datiRappresentanteLegale").optString("nazionalita") + "</Citizenship>";
            xml += "<Gender>" + (Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("inserimentoDati").getJSONObject("datiRappresentanteLegale").optString("gender").compareToIgnoreCase("Maschio") == 0 ? "M" : "F") + "</Gender>";
            xml += "<IDType>" + Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("inserimentoDati").getJSONObject("datiRappresentanteLegale").optString("docIdentita") + "</IDType>";
            xml += "<IDNumber>" + Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("inserimentoDati").getJSONObject("datiRappresentanteLegale").optString("numDocIdentita") + "</IDNumber>";
            xml += "<IDIssuedBy>" + Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("inserimentoDati").getJSONObject("datiRappresentanteLegale").optString("rilasciatoDa") + "</IDIssuedBy>";

            String data2 = Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("inserimentoDati").getJSONObject("datiRappresentanteLegale").optString("dataDoc");
            xml += "<IDIssueDate>" + (new SimpleDateFormat("yyyy-MM-dd").format(new SimpleDateFormat("dd/MM/yyyy").parse(data2))) + "</IDIssueDate>";
//        xml += "<Address>";
//        xml += "</Address>";
            xml += "</Delegate>";

            xml += "<Address>";
            xml += "<AddressDescription>" + Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("inserimentoDati").getJSONObject("datiSede").optString("riferimentoSede") + "</AddressDescription>";
//            xml += "<AddressType>"++ "</AddressType>";
            xml += "<AddressName>" + Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("inserimentoDati").getJSONObject("datiSede").optString("indirizzo") + "</AddressName>";
            xml += "<StreetNbr>" + Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("inserimentoDati").getJSONObject("datiSede").optString("civico") + "</StreetNbr>";
            xml += "<City>" + Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("inserimentoDati").getJSONObject("datiSede").optString("citta") + "</City>";
            xml += "<Province>" + Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("inserimentoDati").getJSONObject("datiSede").optString("provincia") + "</Province>";
//            xml += "<Country>"++ "</Country>";
            xml += "<PostalCode>" + Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("inserimentoDati").getJSONObject("datiSede").optString("cap") + "</PostalCode>";
            xml += "<ReprFirstName>" + Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("inserimentoDati").getJSONObject("referenteContratto").optString("nome") + "</ReprFirstName>";
            xml += "<ReprLastName>" + Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("inserimentoDati").getJSONObject("referenteContratto").optString("cognome") + "</ReprLastName>";
            xml += "<ReprPhoneNumber>" + Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("inserimentoDati").getJSONObject("referenteContratto").optString("telefono") + "</ReprPhoneNumber>";
            xml += "<ReprFaxNumber>" + Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("inserimentoDati").getJSONObject("referenteContratto").optString("fax") + "</ReprFaxNumber>";
            xml += "<ReprEmailAddress>" + Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("inserimentoDati").getJSONObject("referenteContratto").optString("email") + "</ReprEmailAddress>";
            xml += "</Address>";

            xml += "</Customer>";

            xml += "<InvoiceCallDetail>" + (Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("inserimentoDati").getJSONObject("referenteContratto").getString("dettaglioChiamate").compareToIgnoreCase("1") == 0 ? "true" : "false") + "</InvoiceCallDetail>";
            xml += "<InvoiceUnique>" + (Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("inserimentoDati").getJSONObject("referenteContratto").getString("modFatturazione").compareToIgnoreCase("Fattura unica") == 0 ? "true" : "false") + "</InvoiceUnique>";
            xml += "<InvoiceByCostCenter>" + (Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("inserimentoDati").getJSONObject("referenteContratto").getString("modFatturazione").compareToIgnoreCase("Fattura unica") != 0 ? "true" : "false") + "</InvoiceByCostCenter>";
            xml += "<TaxBenefit>" + (Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("inserimentoDati").getJSONObject("referenteContratto").getString("agevolazioniFiscali").compareToIgnoreCase("1") == 0 ? "true" : "false") + "</TaxBenefit>";
            xml += "<FlagIPA>" +(Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("inserimentoDati").getJSONObject("pubblicaAmministrazione").getString("modFattElettronica").compareToIgnoreCase("1") == 0 ? "true" : "false")+ "</FlagIPA>";
            xml += "<IPAIDNumber>" + Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("inserimentoDati").getJSONObject("pubblicaAmministrazione").optString("codiceIdentificativoUfficio") + "</IPAIDNumber>";
            xml += "<SplitPayment>" + (Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("inserimentoDati").getJSONObject("pubblicaAmministrazione").getString("splitPayment").compareToIgnoreCase("1") == 0 ? "true" : "false") + "</SplitPayment>";
            xml += "<EmailInvoice>" + (Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("inserimentoDati").getJSONObject("modInvioFattura").getString("elettronico").compareToIgnoreCase("1") == 0 ? "true" : "false") + "</EmailInvoice>";
            xml += "<PrintedInvoice>" + (Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("inserimentoDati").getJSONObject("modInvioFattura").getString("cartaceo").compareToIgnoreCase("1") == 0 ? "true" : "false") + "</PrintedInvoice>";
            xml += "<PrintedCostDetails>" + (Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("inserimentoDati").getJSONObject("documentazioneCosti").getString("supportoCartaceo").compareToIgnoreCase("1") == 0 ? "true" : "false") + "</PrintedCostDetails>";
            xml += "<CdromCostDetails>" + (Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("inserimentoDati").getJSONObject("documentazioneCosti").getString("supportoCD").compareToIgnoreCase("1") == 0 ? "true" : "false") + "</CdromCostDetails>";

            xml += "<PaymentProfile>";

            xml += "<PaymentMethod>" + (Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("infoPagamento").getString("pagamentoTramiteRid").compareToIgnoreCase("1") == 0 ? "RID" : "CC") + "</PaymentMethod>";
            if (Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("infoPagamento").getString("pagamentoTramiteRid").compareToIgnoreCase("1") == 0) {


                xml += "<IBAN>" + Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("infoPagamento").getJSONObject("estremiContoCorrente").optString("iban") + "</IBAN>";
                xml += "<BankName>" + Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("infoPagamento").getJSONObject("estremiContoCorrente").optString("nomeBancaPosta") + "</BankName>";
                xml += "<BankBranch>" + Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("infoPagamento").getJSONObject("estremiContoCorrente").optString("agenziaUfficioFiliale") + "</BankBranch>";
                xml += "<BankAuthorizationflag>" + "false" + "</BankAuthorizationflag>";

                xml += "<BankSubscriber>";

                xml += "<FirstName>" + Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("infoPagamento").getJSONObject("sottoscrittoreDelModulo").optString("nome") + "</FirstName>";
                xml += "<LastName>" + Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("infoPagamento").getJSONObject("sottoscrittoreDelModulo").optString("cognome") + "</LastName>";
//            xml += "<PhoneNumber>"+Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("infoPagamento").getJSONObject("sottoscrittoreDelModulo").optString("nome")+ "</PhoneNumber>";
//            xml += "<FaxNumber>"+Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("infoPagamento").getJSONObject("sottoscrittoreDelModulo").optString("nome")+ "</FaxNumber>";
//            xml += "<EmailAddress>"+Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("infoPagamento").getJSONObject("sottoscrittoreDelModulo").optString("nome")+ "</EmailAddress>";
                xml += "<CodiceFiscale>" + Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("infoPagamento").getJSONObject("sottoscrittoreDelModulo").optString("cf") + "</CodiceFiscale>";
                xml += "<BirthCity>" + Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("infoPagamento").getJSONObject("sottoscrittoreDelModulo").optString("luogoDiNascita") + "</BirthCity>";
                xml += "<BirthProvince>" + Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("infoPagamento").getJSONObject("sottoscrittoreDelModulo").optString("provincia") + "</BirthProvince>";

                String data3 = Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("infoPagamento").getJSONObject("sottoscrittoreDelModulo").optString("dataDiNascita");
                xml += "<BirthDate>" + (new SimpleDateFormat("yyyy-MM-dd").format(new SimpleDateFormat("dd/MM/yyyy").parse(data3))) + "</BirthDate>";
//            xml += "<Citizenship>"+Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("infoPagamento").getJSONObject("sottoscrittoreDelModulo").optString("nome")+ "</Citizenship>";
                xml += "<Gender>" + (Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("infoPagamento").getJSONObject("sottoscrittoreDelModulo").optString("gender").compareToIgnoreCase("Maschio") == 0 ? "M" : "F") + "</Gender>";
//            xml += "<IDType>"+Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("infoPagamento").getJSONObject("sottoscrittoreDelModulo").optString("nome")+ "</IDType>";
//            xml += "<IDNumber>"+Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("infoPagamento").getJSONObject("sottoscrittoreDelModulo").optString("nome")+ "</IDNumber>";
//            xml += "<IDIssuedBy>"+Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("infoPagamento").getJSONObject("sottoscrittoreDelModulo").optString("nome")+ "</IDIssuedBy>";
//            xml += "<IDIssueDate>"+Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("infoPagamento").getJSONObject("sottoscrittoreDelModulo").optString("nome")+ "</IDIssueDate>";

                xml += "<Address>";
//            xml += "<AddressDescription>"++ "</AddressDescription>";
//            xml += "<AddressType>"++ "</AddressType>";
                xml += "<AddressName>" + Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("infoPagamento").getJSONObject("sottoscrittoreDelModulo").optString("indirizzo") + "</AddressName>";
                xml += "<StreetNbr>" + Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("infoPagamento").getJSONObject("sottoscrittoreDelModulo").optString("civico") + "</StreetNbr>";
                xml += "<City>" + Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("infoPagamento").getJSONObject("sottoscrittoreDelModulo").optString("citta") + "</City>";
                xml += "<Province>" + Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("infoPagamento").getJSONObject("sottoscrittoreDelModulo").optString("provincia") + "</Province>";
//            xml += "<Country>"++ "</Country>";
                xml += "<PostalCode>" + Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("infoPagamento").getJSONObject("sottoscrittoreDelModulo").optString("cap") + "</PostalCode>";
//            xml += "<ReprFirstName>"++ "</ReprFirstName>";
//            xml += "<ReprLastName>"++ "</ReprLastName>";
//            xml += "<ReprPhoneNumber>"++ "</ReprPhoneNumber>";
//            xml += "<ReprFaxNumber>"++ "</ReprFaxNumber>";
//            xml += "<ReprEmailAddress>"++ "</ReprEmailAddress>";
                xml += "</Address>";

                xml += "</BankSubscriber>";

                xml += "<BankAccount>";

                xml += "<FirstName>" + Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("infoPagamento").getJSONObject("intestatarioConto").optString("nome") + "</FirstName>";
                xml += "<LastName>" + Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("infoPagamento").getJSONObject("intestatarioConto").optString("cognome") + "</LastName>";
//            xml += "<PhoneNumber>"++ "</PhoneNumber>";
//            xml += "<FaxNumber>"++ "</FaxNumber>";
//            xml += "<EmailAddress>"++ "</EmailAddress>";
                xml += "<CodiceFiscale>" + Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("infoPagamento").getJSONObject("intestatarioConto").optString("cf") + "</CodiceFiscale>";
                xml += "<BirthCity>" + Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("infoPagamento").getJSONObject("intestatarioConto").optString("citta") + "</BirthCity>";
                xml += "<BirthProvince>" + Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("infoPagamento").getJSONObject("intestatarioConto").optString("provincia") + "</BirthProvince>";
//            xml += "<BirthDate>"++ "</BirthDate>";
//            xml += "<Citizenship>"++ "</Citizenship>";
//            xml += "<Gender>"++ "</Gender>";
//            xml += "<IDType>"++ "</IDType>";
//            xml += "<IDNumber>"++ "</IDNumber>";
//            xml += "<IDIssuedBy>"++ "</IDIssuedBy>";
//            xml += "<IDIssueDate>"++ "</IDIssueDate>";

                xml += "<Address>";
//            xml += "<AddressDescription>"++ "</AddressDescription>";
//            xml += "<AddressType>"++ "</AddressType>";
                xml += "<AddressName>" + Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("infoPagamento").getJSONObject("intestatarioConto").optString("indirizzo") + "</AddressName>";
                xml += "<StreetNbr>" + Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("infoPagamento").getJSONObject("intestatarioConto").optString("civico") + "</StreetNbr>";
                xml += "<City>" + Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("infoPagamento").getJSONObject("intestatarioConto").optString("citta") + "</City>";
                xml += "<Province>" + Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("infoPagamento").getJSONObject("intestatarioConto").optString("provincia") + "</Province>";
//            xml += "<Country>"++ "</Country>";
                xml += "<PostalCode>" + Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("infoPagamento").getJSONObject("intestatarioConto").optString("cap") + "</PostalCode>";
//            xml += "<ReprFirstName>"++ "</ReprFirstName>";
//            xml += "<ReprLastName>"++ "</ReprLastName>";
//            xml += "<ReprPhoneNumber>"++ "</ReprPhoneNumber>";
//            xml += "<ReprFaxNumber>"++ "</ReprFaxNumber>";
//            xml += "<ReprEmailAddress>"++ "</ReprEmailAddress>";
                xml += "</Address>";

                xml += "</BankAccount>";

                xml += "<DirectDebitID>" + Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("infoPagamento").getJSONObject("addebitoDiretto").optString("identificativoMandato") + "</DirectDebitID>";
//            xml += "<DirectDebitDate>"++ "</DirectDebitDate>";
//            xml += "<DirectDebitPlace>"++ "</DirectDebitPlace>";
//            xml += "<BillFrequency>"++ "</BillFrequency>";
//            xml += "<BillType>"++ "</BillType>";
//            xml += "<BillVendorId>"++ "</BillVendorId>";


            } else {

                String creditCardType = "";
                if (Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("infoPagamento").getJSONObject("autorizzazionePermanenteDiAddebito").getString("visaMastercard").compareToIgnoreCase("1") == 0) {
                    creditCardType = "VISA/MASTERCARD";
                } else if (Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("infoPagamento").getJSONObject("autorizzazionePermanenteDiAddebito").getString("cartaSi").compareToIgnoreCase("1") == 0) {
                    creditCardType = "CARTASI";
                } else if (Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("infoPagamento").getJSONObject("autorizzazionePermanenteDiAddebito").getString("americanExpress").compareToIgnoreCase("1") == 0) {
                    creditCardType = "AMERICAN EXPRESS";
                } else if (Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("infoPagamento").getJSONObject("autorizzazionePermanenteDiAddebito").getString("diners").compareToIgnoreCase("1") == 0) {
                    creditCardType = "DINERS";
                } else if (Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("infoPagamento").getJSONObject("autorizzazionePermanenteDiAddebito").getString("deutscheCreditCard").compareToIgnoreCase("1") == 0) {
                    creditCardType = "DEUTSCHE CREDIT CARD";
                }


                xml += "<CreditCardType>" + creditCardType + "</CreditCardType>";
                xml += "<CreditCardNumber>" + Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("infoPagamento").getJSONObject("autorizzazionePermanenteDiAddebito").optString("numero") + "</CreditCardNumber>";

                String data4 = Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("infoPagamento").getJSONObject("autorizzazionePermanenteDiAddebito").optString("scadenza");
//                xml += "<CreditCardExpirationDate>" + (new SimpleDateFormat("yyyy-MM-dd").format(new SimpleDateFormat("dd/MM/yyyy").parse(data4))) + "</CreditCardExpirationDate>";
                xml += "<CreditCardExpirationDate>" + data4 + "</CreditCardExpirationDate>";
            }

            xml += "</PaymentProfile>";


            xml += "</InsertPdaAnagrafica_Input>";
//        InsertPdaAnagrafica_Input [FINE]

//        InsertPdaOrder_Input [INIZIO]
            xml += "<InsertPdaOrder_Input>";

            xml += "<Order>";

//            xml += "<Status/>";
//            xml += "<OrderType/>";
//            xml += "<SystemId/>";

            boolean hasAbbonamento = false;
            ArrayList<OffertaMobile> offerteAbbonamento = new ArrayList<>();
            for (OffertaMobile offertaMobile : offertaMobileArrayList) {
                if (offertaMobile.getOfferType().compareToIgnoreCase("Abbonamento") == 0) {
                    hasAbbonamento = true;
                    offerteAbbonamento.add(offertaMobile);
                }
            }
            if (hasAbbonamento) {
                xml += "<MobileOffer>";

                xml += "<MobileOfferType>abbonamento</MobileOfferType>";
                xml += "<TransferCreditFlag>" + "false" + "</TransferCreditFlag>";
                xml += "<TCGFlag>" + "false" + "</TCGFlag>";
                xml += "<SubscriberName>" + Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("inserimentoDati").getJSONObject("datiCliente").getString("nome") + "</SubscriberName>";
                xml += "<SubscriberSurname>" + Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("inserimentoDati").getJSONObject("datiCliente").getString("cognome") + "</SubscriberSurname>";
                xml += "<CompanyName>" + Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("inserimentoDati").getJSONObject("datiCliente").getString("ragioneSociale") + "</CompanyName>";
                xml += "<PartitaIVA>" + Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("inserimentoDati").getJSONObject("datiCliente").getString("p_iva") + "</PartitaIVA>";
                xml += "<Place>" + Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("inserimentoDati").getJSONObject("datiCliente").getString("citta") + "</Place>";

                for (OffertaMobile offertaMobile : offerteAbbonamento) {
                    xml += "<SIM>";

                    xml += "<ICCID>" + offertaMobile.getNewICCID() + "</ICCID>";
                    if (offertaMobile.isMnp()) {
                        xml += "<MSISDN>" + offertaMobile.getOldMSISDN() + "</MSISDN>";
                        xml += "<MNPICCID>" + offertaMobile.getOldICCID() + "</MNPICCID>";
                        xml += "<MNPDonor>" + offertaMobile.getMnpStep8() + "</MNPDonor>";
                    }
                    xml += "<TariffPlan>" + (offertaMobile.getOfferDescription() + " " + offertaMobile.getOfferDescriptionUpper()).trim() + "</TariffPlan>";
//                    xml += "<InternetTariffPlan>"++ "</InternetTariffPlan>";

                    for (OpzioneMobile opzioneMobile : offertaMobile.getOpzioniMobileSelectedArrayListStep8()) {
                        if (!Boolean.parseBoolean(opzioneMobile.getIsDefault()) || TextUtils.isEmpty(opzioneMobile.getScontoConvergenza())) {
                            xml += "<Options>" + opzioneMobile.getOpzioneDesc() + "</Options>";
                        }
                    }

                    if (offertaMobile.getAddOnMobileStep8() != null) {
                        xml += "<Options>" + offertaMobile.getAddOnMobileStep8().getDescFascia() + " - " + offertaMobile.getAddOnMobileStep8().getSelectedLabelStep8() + "</Options>";
                    }

                    boolean hasPromoSuper = false;
                    for (OpzioneMobile opzioneMobile : offertaMobile.getOpzioniMobileSelectedArrayListStep8()) {
                        if (!TextUtils.isEmpty(opzioneMobile.getScontoConvergenza())) {
                            hasPromoSuper = true;
                            break;
                        }
                    }
                    xml += "<PromoSuper>" + String.valueOf(hasPromoSuper) + "</PromoSuper>";

                    boolean hasInternoMobile = false;
                    for (OpzioneMobile opzioneMobile : offertaMobile.getOpzioniMobileSelectedArrayListStep8()) {
                        if (opzioneMobile.getOpzioneDesc().compareToIgnoreCase("Interno Mobile") == 0) {
                            hasInternoMobile = true;
                            break;
                        }
                    }
                    xml += "<InternoMobile>" + String.valueOf(hasInternoMobile) + "</InternoMobile>";

                    boolean hasPromoTerminale = false;
                    for (OpzioneMobile opzioneMobile : offertaMobile.getOpzioniMobileSelectedArrayListStep8()) {
                        if (opzioneMobile.getOpzioneDesc().compareToIgnoreCase("Promo Terminale") == 0) {
                            hasPromoTerminale = true;
                            break;
                        }
                    }
                    xml += "<PromoTerminale>" + String.valueOf(hasPromoTerminale) + "</PromoTerminale>";

                    boolean hasAssistenzaTecnicaEvolutiva = false;
                    for (OpzioneMobile opzioneMobile : offertaMobile.getOpzioniMobileSelectedArrayListStep8()) {
                        if (opzioneMobile.getOpzioneDesc().compareToIgnoreCase("Assistenza Tecnica Evolutiva") == 0) {
                            hasAssistenzaTecnicaEvolutiva = true;
                            break;
                        }
                    }
                    xml += "<AssistenzaTecnicaEvolutiva>" + String.valueOf(hasAssistenzaTecnicaEvolutiva) + "</AssistenzaTecnicaEvolutiva>";

                    boolean hasActive = false;
                    for (OpzioneMobile opzioneMobile : offertaMobile.getOpzioniMobileSelectedArrayListStep8()) {
                        if (opzioneMobile.getOpzioneDesc().compareToIgnoreCase("Sim Già Attiva") == 0) {
                            hasActive = true;
                            break;
                        }
                    }
                    xml += "<Active>" + String.valueOf(hasActive) + "</Active>";

                    if (offertaMobile.getTerminale1() != null) {
                        xml += "<DeviceModel>" + offertaMobile.getTerminale1().getTerminaleDescription() + "</DeviceModel>";
                    }
                    if (offertaMobile.getTerminale2() != null) {
                        xml += "<DeviceModel>" + offertaMobile.getTerminale2().getTerminaleDescription() + "</DeviceModel>";
                    }

                    xml += "</SIM>";

                }


                xml += "</MobileOffer>";

                xml +="<Discount>";
                for (OffertaMobile offertaMobile : offerteAbbonamento) {
                    if(offertaMobile.getScontoApplicato().compareTo("0,00")!=0) {
                        xml += "<CompanyName>" + Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("inserimentoDati").getJSONObject("datiCliente").getString("ragioneSociale") + "</CompanyName>";
                        xml += "<MobileOfferName>" + offertaMobile.getOfferDescription() + " " + offertaMobile.getOfferDescriptionUpper() + "</MobileOfferName>";
                        xml += "<MobileOfferType>" + offertaMobile.getOfferType() + "</MobileOfferType>";
                        xml += "<RecurringFull>" + offertaMobile.getCanoneMensile().replace(".", ",") + "</RecurringFull>";
                        xml += "<RecurringGA>" + offertaMobile.getCanonePromo().replace(".", ",") + "</RecurringGA>";
                        xml += "<PercDiscount>" + offertaMobile.getScontoApplicato().replace(".", ",") + "</PercDiscount>";
                        xml += "<RecurringPromo>" + offertaMobile.getCanoneScontato().replace(".", ",") + "</RecurringPromo>";
                        xml += "<PromoSIMCount>" + offertaMobile.getSelectedNumSimStep4() + "</PromoSIMCount>";
                        if (TextUtils.isEmpty(offertaMobile.getSmsIta()) && TextUtils.isEmpty(offertaMobile.getCallIta()) && TextUtils.isEmpty(offertaMobile.getCallAzIta())) {
                            xml += "<OnlyData>" + "true" + "</OnlyData>";
                        } else {
                            xml += "<OnlyData>" + "false" + "</OnlyData>";
                        }
                    }
                }
                xml +="</Discount>";
            }

            boolean hasRicaricabile = false;
            ArrayList<OffertaMobile> offerteRicaricabile = new ArrayList<>();
            for (OffertaMobile offertaMobile : offertaMobileArrayList) {
                if (offertaMobile.getOfferType().compareToIgnoreCase("Ricaricabile") == 0) {
                    hasRicaricabile = true;
                    offerteRicaricabile.add(offertaMobile);
                }
            }
            if (hasRicaricabile) {
                xml += "<MobileOffer>";

                xml += "<MobileOfferType>ricaricabile</MobileOfferType>";
                xml += "<TransferCreditFlag>" + "false" + "</TransferCreditFlag>";
                xml += "<TCGFlag>" + "false" + "</TCGFlag>";
                xml += "<SubscriberName>" + Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("inserimentoDati").getJSONObject("datiCliente").getString("nome") + "</SubscriberName>";
                xml += "<SubscriberSurname>" + Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("inserimentoDati").getJSONObject("datiCliente").getString("cognome") + "</SubscriberSurname>";
                xml += "<CompanyName>" + Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("inserimentoDati").getJSONObject("datiCliente").getString("ragioneSociale") + "</CompanyName>";
                xml += "<PartitaIVA>" + Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("inserimentoDati").getJSONObject("datiCliente").getString("p_iva") + "</PartitaIVA>";
                xml += "<Place>" + Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("inserimentoDati").getJSONObject("datiCliente").getString("citta") + "</Place>";

                for (OffertaMobile offertaMobile : offerteRicaricabile) {
                    xml += "<SIM>";

                    xml += "<ICCID>" + offertaMobile.getNewICCID() + "</ICCID>";
                    if (offertaMobile.isMnp()) {
                        xml += "<MSISDN>" + offertaMobile.getOldMSISDN() + "</MSISDN>";
                        xml += "<MNPICCID>" + offertaMobile.getOldICCID() + "</MNPICCID>";
                        xml += "<MNPDonor>" + offertaMobile.getMnpStep8() + "</MNPDonor>";
                    }
                    xml += "<TariffPlan>" + offertaMobile.getOfferDescription() + " " + offertaMobile.getOfferDescriptionUpper() + "</TariffPlan>";
//                    xml += "<InternetTariffPlan>"++ "</InternetTariffPlan>";

                    for (OpzioneMobile opzioneMobile : offertaMobile.getOpzioniMobileSelectedArrayListStep8()) {
                        if (!Boolean.parseBoolean(opzioneMobile.getIsDefault()) || TextUtils.isEmpty(opzioneMobile.getScontoConvergenza())) {
                            xml += "<Options>" + opzioneMobile.getOpzioneDesc() + " " + opzioneMobile.getOpzioneDescUpper() + "</Options>";
                        }
                    }

                    if (offertaMobile.getAddOnMobileStep8() != null) {

                        String endLabel = Session.getInstance().getContractLabel(offertaMobile.getAddOnMobileStep8().getDescFascia(),offertaMobile.getAddOnMobileStep8().getSelectedLabelStep8());

                        xml += "<Options>" + offertaMobile.getAddOnMobileStep8().getDescFascia() + " " + offertaMobile.getAddOnMobileStep8().getSelectedLabelStep8() + " "+ endLabel+"</Options>";
                    }

                    String automaticRefill = "";
                    for (OpzioneMobile opzioneMobile : offertaMobile.getOpzioniMobileSelectedArrayListStep8()) {
                        if (!TextUtils.isEmpty(opzioneMobile.getValue())) {
                            automaticRefill = opzioneMobile.getValue();
                            break;
                        }
                    }
                    xml += "<AutomaticRefill>" + automaticRefill + "</AutomaticRefill>";

                    boolean hasPromoSuper = false;
                    for (OpzioneMobile opzioneMobile : offertaMobile.getOpzioniMobileSelectedArrayListStep8()) {
                        if (!TextUtils.isEmpty(opzioneMobile.getScontoConvergenza())) {
                            hasPromoSuper = true;
                            break;
                        }
                    }
                    xml += "<PromoSuper>" + String.valueOf(hasPromoSuper) + "</PromoSuper>";

                    boolean hasInternoMobile = false;
                    for (OpzioneMobile opzioneMobile : offertaMobile.getOpzioniMobileSelectedArrayListStep8()) {
                        if (opzioneMobile.getOpzioneDesc().compareToIgnoreCase("Interno Mobile") == 0) {
                            hasInternoMobile = true;
                            break;
                        }
                    }
                    xml += "<InternoMobile>" + String.valueOf(hasInternoMobile) + "</InternoMobile>";

                    boolean hasPromoTerminale = false;
                    for (OpzioneMobile opzioneMobile : offertaMobile.getOpzioniMobileSelectedArrayListStep8()) {
                        if (opzioneMobile.getOpzioneDesc().compareToIgnoreCase("Promo Terminale") == 0) {
                            hasPromoTerminale = true;
                            break;
                        }
                    }
                    xml += "<PromoTerminale>" + String.valueOf(hasPromoTerminale) + "</PromoTerminale>";

                    boolean hasAssistenzaTecnicaEvolutiva = false;
                    for (OpzioneMobile opzioneMobile : offertaMobile.getOpzioniMobileSelectedArrayListStep8()) {
                        if (opzioneMobile.getOpzioneDesc().compareToIgnoreCase("Assistenza Tecnica Evolutiva") == 0) {
                            hasAssistenzaTecnicaEvolutiva = true;
                            break;
                        }
                    }
                    xml += "<AssistenzaTecnicaEvolutiva>" + String.valueOf(hasAssistenzaTecnicaEvolutiva) + "</AssistenzaTecnicaEvolutiva>";

                    boolean hasActive = false;
                    for (OpzioneMobile opzioneMobile : offertaMobile.getOpzioniMobileSelectedArrayListStep8()) {
                        if (opzioneMobile.getOpzioneDesc().compareToIgnoreCase("Sim Già Attiva") == 0) {
                            hasActive = true;
                            break;
                        }
                    }
                    xml += "<Active>" + String.valueOf(hasActive) + "</Active>";

                    if (offertaMobile.getTerminale1() != null) {
                        xml += "<DeviceModel>" + offertaMobile.getTerminale1().getTerminaleDescription() + "</DeviceModel>";
                    }
                    if (offertaMobile.getTerminale2() != null) {
                        xml += "<DeviceModel>" + offertaMobile.getTerminale2().getTerminaleDescription() + "</DeviceModel>";
                    }

                    xml += "</SIM>";
                }
                /*
                xml +="<Discount>";
                for (OffertaMobile offertaMobile : offerteRicaricabile) {
                    if(offertaMobile.getScontoApplicato().compareTo("0,00")!=0) {
                        xml += "<CompanyName>" + Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("inserimentoDati").getJSONObject("datiCliente").getString("ragioneSociale") + "</CompanyName>";
                        xml += "<MobileOfferName>" + offertaMobile.getOfferDescription() + " " + offertaMobile.getOfferDescriptionUpper() + "</MobileOfferName>";
                        xml += "<MobileOfferType>" + offertaMobile.getOfferType() + "</MobileOfferType>";
                        xml += "<RecurringFull>" + offertaMobile.getCanoneMensile() + "</RecurringFull>";
                        xml += "<RecurringGA>" + offertaMobile.getCanonePromo() + "</RecurringGA>";
                        xml += "<PercDiscount>" + offertaMobile.getScontoApplicato() + "</PercDiscount>";
                        xml += "<RecurringPromo>" + offertaMobile.getCanoneScontato() + "</RecurringPromo>";
                        xml += "<PromoSIMCount>" + offertaMobile.getSelectedNumSimStep4() + "</PromoSIMCount>";
                        if(TextUtils.isEmpty(offertaMobile.getSmsIta()) && TextUtils.isEmpty(offertaMobile.getCallIta()) && TextUtils.isEmpty(offertaMobile.getCallEst())) {
                            xml += "<OnlyData>" + "true" + "</OnlyData>";
                        }else{
                            xml += "<OnlyData>" + "false" + "</OnlyData>";
                        }
                    }
                }
                xml +="</Discount>";
                */
                xml += "</MobileOffer>";

                xml +="<Discount>";
                for (OffertaMobile offertaMobile : offerteRicaricabile) {
                    if(offertaMobile.getScontoApplicato().compareTo("0,00")!=0) {
                        xml += "<CompanyName>" + Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONObject("inserimentoDati").getJSONObject("datiCliente").getString("ragioneSociale") + "</CompanyName>";
                        xml += "<MobileOfferName>" + offertaMobile.getOfferDescription() + " " + offertaMobile.getOfferDescriptionUpper() + "</MobileOfferName>";
                        xml += "<MobileOfferType>" + offertaMobile.getOfferType() + "</MobileOfferType>";
                        xml += "<RecurringFull>" + offertaMobile.getCanoneMensile().replace(".", ",") + "</RecurringFull>";
                        xml += "<RecurringGA>" + offertaMobile.getCanonePromo().replace(".", ",") + "</RecurringGA>";
                        xml += "<PercDiscount>" + offertaMobile.getScontoApplicato().replace(".", ",") + "</PercDiscount>";
                        xml += "<RecurringPromo>" + offertaMobile.getCanoneScontato().replace(".", ",") + "</RecurringPromo>";
                        xml += "<PromoSIMCount>" + offertaMobile.getSelectedNumSimStep4() + "</PromoSIMCount>";
                        if (TextUtils.isEmpty(offertaMobile.getSmsIta()) && TextUtils.isEmpty(offertaMobile.getCallIta()) && TextUtils.isEmpty(offertaMobile.getCallAzIta())) {
                            xml += "<OnlyData>" + "true" + "</OnlyData>";
                        }else{
                            xml += "<OnlyData>" + "false" + "</OnlyData>";
                        }
                    }
                }
                xml +="</Discount>";
            }

            xml += "</Order>";

            xml += "</InsertPdaOrder_Input>";
//        InsertPdaOrder_Input [FINE]

//        ServiceRequest [INIZIO]
            xml += "<ServiceRequest>";

            xml += "<RequestId>" + Session.getInstance().getPratica().getRequest_id() + "</RequestId>";

            String requestType = "";
            if (isPreview) {
                requestType = "ViewPdf";
            } else if (isSendMail) {
                requestType = "SendEmail";
            } else if (isFirma) {
                requestType = "SendXyzmo";
            }
            xml += "<RequestType>" + requestType + "</RequestType>";

            xml += "<GeneratePdfFlag>" + "true" + "</GeneratePdfFlag>";

            xml += "<BiometricPdaFlag>" + "true" + "</BiometricPdaFlag>";
            xml += "<SystemId>" + "12" + "</SystemId>";
//            xml += "<Login>"++ "</Login>";
            xml += "<CodiceManager>" + Session.getInstance().getDealer().getDealerCode() + "</CodiceManager>";
//            xml += "<OfferType/>";

//           TODO: Se esistono allegati

            if(pathFotoRappresentanteLegale!=null) {
                xml += "<Attachment>";
                xml += "<ActivityFileExt>" + "jpeg" + "</ActivityFileExt>";
                xml += "<ActivityFileName>" + pathFotoRappresentanteLegale + "</ActivityFileName>";
                xml += "<AttachmentType>" + "Iddocument" + "</AttachmentType>";

                xml += "<ActivityFileBuffer>" + convertToBase64(pathFotoRappresentanteLegale) + "</ActivityFileBuffer>";
                xml += "</Attachment>";
            }

            if(pathFotoDelegaRappresentanteLegale!=null) {
                xml += "<Attachment>";
                xml += "<ActivityFileExt>" + "jpeg" + "</ActivityFileExt>";
                xml += "<ActivityFileName>" + pathFotoDelegaRappresentanteLegale + "</ActivityFileName>";
                xml += "<AttachmentType>" + "Iddocument" + "</AttachmentType>";

                xml += "<ActivityFileBuffer>" + convertToBase64(pathFotoDelegaRappresentanteLegale) + "</ActivityFileBuffer>";
                xml += "</Attachment>";
            }

            if(pathFotoDocumenteoDelegato!=null) {
                xml += "<Attachment>";
                xml += "<ActivityFileExt>" + "jpeg" + "</ActivityFileExt>";
                xml += "<ActivityFileName>" + pathFotoDocumenteoDelegato + "</ActivityFileName>";
                xml += "<AttachmentType>" + "Iddocument" + "</AttachmentType>";

                xml += "<ActivityFileBuffer>" + convertToBase64(pathFotoDocumenteoDelegato) + "</ActivityFileBuffer>";
                xml += "</Attachment>";
            }

            if(pathFotoFatturaOperatore!=null) {
                xml += "<Attachment>";
                xml += "<ActivityFileExt>" + "jpeg" + "</ActivityFileExt>";
                xml += "<ActivityFileName>" + pathFotoFatturaOperatore + "</ActivityFileName>";
                xml += "<AttachmentType>" + "Iddocument" + "</AttachmentType>";

                xml += "<ActivityFileBuffer>" + convertToBase64(pathFotoFatturaOperatore) + "</ActivityFileBuffer>";
                xml += "</Attachment>";
            }

            if(pathFotoAggiungiDoc!=null) {
                xml += "<Attachment>";
                xml += "<ActivityFileExt>" + "jpeg" + "</ActivityFileExt>";
                xml += "<ActivityFileName>" + pathFotoAggiungiDoc + "</ActivityFileName>";
                xml += "<AttachmentType>" + "Iddocument" + "</AttachmentType>";

                xml += "<ActivityFileBuffer>" + convertToBase64(pathFotoAggiungiDoc) + "</ActivityFileBuffer>";
                xml += "</Attachment>";
            }

//            xml += "<TBLUserProfile/>";
            xml += "<ContractPlace>" + "Roma" + "</ContractPlace>";
            xml += "<ContractDate>" + (new SimpleDateFormat("yyyy-MM-dd").format(new Date())) + "</ContractDate>";

            if (isSendMail) {
//                xml += "<ContractDate>"++ "</ContractDate>";
            }
            xml+=summary;

            xml += "</ServiceRequest>";
//        ServiceRequest [FINE]


            xml += "</ins:InsertPdaRequestReq>\n" +
                    "</soapenv:Body>\n" +
                    "</soapenv:Envelope>";

            Log.d(TAG,"generateXML " + xml);

            return xml;

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (ParseException e) {

            Log.d(TAG,"ParseException " + e.getMessage());
        }
        return "";
    }

    private void openPdf() {
        File pdfFile = new File(Environment.getExternalStorageDirectory() + "/windpdf.pdf");  // -> filename = maven.pdf
        Uri path = Uri.fromFile(pdfFile);
        Intent pdfIntent = new Intent(Intent.ACTION_VIEW);
        pdfIntent.setDataAndType(path, "application/pdf");
        pdfIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        try {
            startActivity(pdfIntent);
        } catch (ActivityNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean handleMessage(Message msg) {
        switch (msg.what) {
            case Constants.HandlerWhat.WHAT_SEND_CONTRACT:
                Utils.showGenericMessage(this.getActivity(), "INVIO CONTRATTO", "Contratto inviato con successo!");
                break;
            case Constants.HandlerWhat.WHAT_SEND_CONTRACT_KO:
                Utils.showGenericMessage(this.getActivity(), "INVIO CONTRATTO", "Errore nell'invio del contratto!");
                break;
            case Constants.HandlerWhat.WHAT_PREVIEW:
                builder.dismiss();
                openPdf();
                break;
            case Constants.HandlerWhat.WHAT_PREVIEW_KO:
                builder.dismiss();
                break;
            case Constants.HandlerWhat.WHAT_MAIL:
                //mostra il messaggio di invio mail corretto
                Utils.showGenericMessage(this.getActivity(), "INVIO MAIL", "Mail inviata con successo!");
                break;
        }
        return false;
    }


    private String convertToBase64(String fileName)
    {
        String encodedString="";
        try {
            InputStream inputStream = new FileInputStream(fileName);//You can get an inputStream using any IO API
            byte[] bytes;
            byte[] buffer = new byte[8192];
            int bytesRead;
            ByteArrayOutputStream output = new ByteArrayOutputStream();
            try {
                while ((bytesRead = inputStream.read(buffer)) != -1) {
                    output.write(buffer, 0, bytesRead);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            bytes = output.toByteArray();
            encodedString = Base64.encodeToString(bytes, Base64.DEFAULT);
        }
        catch (Exception e)
        {
            Log.i(TAG,e.getMessage());
        }
        return encodedString;
    }
}
