package it.wind.smartsales.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import it.wind.smartsales.R;
import it.wind.smartsales.activity.MainActivity;
import it.wind.smartsales.adapter.OfferAdapter;
import it.wind.smartsales.commons.Log;
import it.wind.smartsales.commons.Utils;
import it.wind.smartsales.customviews.AspectRatioView;
import it.wind.smartsales.customviews.HorizontalSpaceItemDecoration;
import it.wind.smartsales.customviews.VerticalTextView;
import it.wind.smartsales.dao.DbHelper;
import it.wind.smartsales.dao.PraticheDAO;
import it.wind.smartsales.entities.OffertaMobile;
import it.wind.smartsales.entities.Session;

/**
 * Created by luca.quaranta on 10/02/2016.
 */
public class MobileInternetCreateOfferStep2Fragment extends Fragment {

    private RecyclerView abbonamentoRecyclerView, ricaricabileRecyclerView;
    private RecyclerView.LayoutManager mLayoutManagerAbbonamento, mLayoutManagerRiciclabile;
    private RecyclerView.Adapter abbonamentoAdapter, ricaricabileAdapter;
    private int pacchettoVisibile = 1;
    private RelativeLayout pacchetto1, pacchetto2, pacchetto3, pacchetto4;
    private AspectRatioView pacchetto1Tick, pacchetto2Tick, pacchetto3Tick, pacchetto4Tick;
    private LinearLayout abbonamentoListLayout, ricaricabileListLayout;
    private Button continuaButton;
    private VerticalTextView pacchetto1Text, pacchetto2Text, pacchetto3Text, pacchetto4Text;

    private void initializeComponents(View view) {
        abbonamentoRecyclerView = (RecyclerView) view.findViewById(R.id.abbonamento_recycler_view);
        ricaricabileRecyclerView = (RecyclerView) view.findViewById(R.id.ricaricabile_recycler_view);

        continuaButton = (Button) view.findViewById(R.id.continua_button);

        pacchetto1 = (RelativeLayout) view.findViewById(R.id.sede_1);
        pacchetto2 = (RelativeLayout) view.findViewById(R.id.pacchetto_2);
        pacchetto3 = (RelativeLayout) view.findViewById(R.id.pacchetto_3);
        pacchetto4 = (RelativeLayout) view.findViewById(R.id.pacchetto_4);

        pacchetto1Tick = (AspectRatioView) view.findViewById(R.id.pacchetto_1_tick);
        pacchetto2Tick = (AspectRatioView) view.findViewById(R.id.pacchetto_2_tick);
        pacchetto3Tick = (AspectRatioView) view.findViewById(R.id.pacchetto_3_tick);
        pacchetto4Tick = (AspectRatioView) view.findViewById(R.id.pacchetto_4_tick);

        pacchetto1Text = (VerticalTextView) view.findViewById(R.id.pacchetto_1_text);
        pacchetto2Text = (VerticalTextView) view.findViewById(R.id.pacchetto_2_text);
        pacchetto3Text = (VerticalTextView) view.findViewById(R.id.pacchetto_3_text);
        pacchetto4Text = (VerticalTextView) view.findViewById(R.id.pacchetto_4_text);

        abbonamentoListLayout = (LinearLayout) view.findViewById(R.id.abbonamento_list_layout);
        ricaricabileListLayout = (LinearLayout) view.findViewById(R.id.ricaricabile_list_layout);

        try {
            JSONArray pacchetti = (JSONArray) Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONArray("pacchetti");
            switch (pacchetti.length()) {
                case 4:
                    pacchetto4.setVisibility(View.VISIBLE);
                case 3:
                    pacchetto3.setVisibility(View.VISIBLE);
                case 2:
                    pacchetto2.setVisibility(View.VISIBLE);
                case 1:
                    pacchetto1.setVisibility(View.VISIBLE);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        initAbbonamentoList();
        initRicaricabileList();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_mobile_internet_create_offer_step_2, container, false);
        initializeComponents(view);
        try {
            JSONObject pratica = Session.getInstance().getJsonPratica().getJSONObject("pratica");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        populatePage();
        checkValidation();

        pacchetto1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openPacchetto(1);
            }
        });

        pacchetto2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openPacchetto(2);
            }
        });

        pacchetto3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openPacchetto(3);
            }
        });

        pacchetto4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openPacchetto(4);
            }
        });

        continuaButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                try {
//                    JSONArray pacchetti = (JSONArray) Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONArray("pacchetti");
//                    for (int j = 0; j < pacchetti.length(); j++) {
//                        JSONObject pacchetto = pacchetti.getJSONObject(j);
//                        JSONArray offerte = (JSONArray) pacchetto.getJSONArray("offerte");
//
//                        for (int i = 0; i < offerte.length(); i++) {
//                            JSONObject offertaJO = Utils.createOffertaMobile(CatalogoOfferteMobileDAO.retrieveOffertaMobile(getActivity(), offerte.getJSONObject(i).getString("idOfferta")));
//                            offertaJO.put("selectedNumSimStep3", offerte.getJSONObject(i).optInt("selectedNumSimStep3"));
//                            offerte.put(i, offertaJO);
//                        }
//                    }
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }

                Session.getInstance().getPratica().setStep(PraticheDAO.STEP3);
                PraticheDAO.updatePratica(getActivity().getBaseContext());

                Log.d("JSON PRATICA)", Session.getInstance().getJsonPratica().toString());

                ((MainActivity) getActivity()).goForward();
            }
        });

        return view;
    }

    private void initAbbonamentoList() {
        abbonamentoRecyclerView.setHasFixedSize(true);
        mLayoutManagerAbbonamento = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        abbonamentoRecyclerView.setLayoutManager(mLayoutManagerAbbonamento);
        abbonamentoRecyclerView.setItemAnimator(new DefaultItemAnimator());
        abbonamentoRecyclerView.addItemDecoration(new HorizontalSpaceItemDecoration(80));
    }

    private void initRicaricabileList() {
        ricaricabileRecyclerView.setHasFixedSize(true);
        mLayoutManagerRiciclabile = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        ricaricabileRecyclerView.setLayoutManager(mLayoutManagerRiciclabile);
        ricaricabileRecyclerView.setItemAnimator(new DefaultItemAnimator());
        ricaricabileRecyclerView.addItemDecoration(new HorizontalSpaceItemDecoration(80));
    }

    private void populateAbbonamentoList(ArrayList<OffertaMobile> offerteMobileAbbonamento, ArrayList<OffertaMobile> offerteList) {
        abbonamentoAdapter = new OfferAdapter(this, offerteMobileAbbonamento, offerteList);
        abbonamentoRecyclerView.setAdapter(abbonamentoAdapter);
    }

    private void populateRicaricabileList(ArrayList<OffertaMobile> offerteMobileRicaricabile, ArrayList<OffertaMobile> offerteList) {
        ricaricabileAdapter = new OfferAdapter(this, offerteMobileRicaricabile, offerteList);
        ricaricabileRecyclerView.setAdapter(ricaricabileAdapter);
    }

    private void openPacchetto(int numPacchetto) {
        switch (numPacchetto) {
            case 1:
                pacchetto1.setBackgroundColor(getActivity().getResources().getColor(android.R.color.white));
                pacchetto2.setBackgroundColor(getActivity().getResources().getColor(R.color.wind_color_transparent_light_gray));
                pacchetto3.setBackgroundColor(getActivity().getResources().getColor(R.color.wind_color_transparent_light_gray));
                pacchetto4.setBackgroundColor(getActivity().getResources().getColor(R.color.wind_color_transparent_light_gray));
                pacchetto1Text.setTextColor(getActivity().getResources().getColor(R.color.colorAccent));
                pacchetto2Text.setTextColor(getActivity().getResources().getColor(android.R.color.black));
                pacchetto3Text.setTextColor(getActivity().getResources().getColor(android.R.color.black));
                pacchetto4Text.setTextColor(getActivity().getResources().getColor(android.R.color.black));

                pacchettoVisibile = 1;
                break;

            case 2:
                pacchetto1.setBackgroundColor(getActivity().getResources().getColor(R.color.wind_color_transparent_light_gray));
                pacchetto2.setBackgroundColor(getActivity().getResources().getColor(android.R.color.white));
                pacchetto3.setBackgroundColor(getActivity().getResources().getColor(R.color.wind_color_transparent_light_gray));
                pacchetto4.setBackgroundColor(getActivity().getResources().getColor(R.color.wind_color_transparent_light_gray));
                pacchetto1Text.setTextColor(getActivity().getResources().getColor(android.R.color.black));
                pacchetto2Text.setTextColor(getActivity().getResources().getColor(R.color.colorAccent));
                pacchetto3Text.setTextColor(getActivity().getResources().getColor(android.R.color.black));
                pacchetto4Text.setTextColor(getActivity().getResources().getColor(android.R.color.black));

                pacchettoVisibile = 2;
                break;

            case 3:
                pacchetto1.setBackgroundColor(getActivity().getResources().getColor(R.color.wind_color_transparent_light_gray));
                pacchetto2.setBackgroundColor(getActivity().getResources().getColor(R.color.wind_color_transparent_light_gray));
                pacchetto3.setBackgroundColor(getActivity().getResources().getColor(android.R.color.white));
                pacchetto4.setBackgroundColor(getActivity().getResources().getColor(R.color.wind_color_transparent_light_gray));
                pacchetto1Text.setTextColor(getActivity().getResources().getColor(android.R.color.black));
                pacchetto2Text.setTextColor(getActivity().getResources().getColor(android.R.color.black));
                pacchetto3Text.setTextColor(getActivity().getResources().getColor(R.color.colorAccent));
                pacchetto4Text.setTextColor(getActivity().getResources().getColor(android.R.color.black));

                pacchettoVisibile = 3;
                break;

            case 4:
                pacchetto1.setBackgroundColor(getActivity().getResources().getColor(R.color.wind_color_transparent_light_gray));
                pacchetto2.setBackgroundColor(getActivity().getResources().getColor(R.color.wind_color_transparent_light_gray));
                pacchetto3.setBackgroundColor(getActivity().getResources().getColor(R.color.wind_color_transparent_light_gray));
                pacchetto4.setBackgroundColor(getActivity().getResources().getColor(android.R.color.white));
                pacchetto1Text.setTextColor(getActivity().getResources().getColor(android.R.color.black));
                pacchetto2Text.setTextColor(getActivity().getResources().getColor(android.R.color.black));
                pacchetto3Text.setTextColor(getActivity().getResources().getColor(android.R.color.black));
                pacchetto4Text.setTextColor(getActivity().getResources().getColor(R.color.colorAccent));

                pacchettoVisibile = 4;
                break;
        }
        populatePage();
    }

    private void populatePage() {
        try {
            JSONArray pacchetti = (JSONArray) Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONArray("pacchetti");
            JSONObject pacchetto = pacchetti.getJSONObject(pacchettoVisibile - 1);
            JSONArray offerte = (JSONArray) pacchetto.getJSONArray("offerte");
            ArrayList<OffertaMobile> offerteList = new ArrayList<>();
            for (int i = 0; i < offerte.length(); i++) {
                offerteList.add(Utils.createOffertaMobile(offerte.getJSONObject(i)));
            }

            if (pacchetto.getInt("question4AbbonamentoNumSim") > 0) {
                abbonamentoListLayout.setVisibility(View.VISIBLE);
                populateAbbonamentoList(DbHelper.retrieveOfferteMobile(getActivity(), pacchettoVisibile, "Abbonamento"), offerteList);
            } else {
                abbonamentoListLayout.setVisibility(View.GONE);
            }

            if (pacchetto.getInt("question4RicaricabileNumSim") > 0) {
                ricaricabileListLayout.setVisibility(View.VISIBLE);
                populateRicaricabileList(DbHelper.retrieveOfferteMobile(getActivity(), pacchettoVisibile, "Ricaricabile"), offerteList);
            } else {
                ricaricabileListLayout.setVisibility(View.GONE);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void openInfo(OffertaMobile offertaMobile) {
//        Utils.showGenericMessage(getActivity(), "Info", info);
        Utils.showInfoDialog(getActivity(), offertaMobile);
    }

    public boolean saveSelectedOffer(boolean toSave, OffertaMobile offertaMobile) {

        try {
            JSONArray pacchetti = (JSONArray) Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONArray("pacchetti");
            JSONObject pacchetto = pacchetti.getJSONObject(pacchettoVisibile - 1);
            JSONArray offerte = (JSONArray) pacchetto.getJSONArray("offerte");

            if (toSave) {

                int selezionati = 0;
                for (int i = 0; i < offerte.length(); i++) {
                    if (offerte.getJSONObject(i).getString("offerType").compareToIgnoreCase(offertaMobile.getOfferType()) == 0) {
                        selezionati++;
                    }
                }

                if (offertaMobile.getOfferType().compareToIgnoreCase("Abbonamento") == 0) {
                    if (selezionati < Integer.parseInt(pacchetto.getString("question4AbbonamentoNumSim"))){
                        offerte.put(Utils.createOffertaMobile(offertaMobile));
//                        Log.d("SAVE OFFER", String.valueOf(offertaMobile.getIdOfferta()));
//                        Log.d("SAVE OFFER", offerte.toString());

                        ArrayList<OffertaMobile> offerteList = new ArrayList<>();
                        for (int i = 0; i < offerte.length(); i++) {
                            offerteList.add(Utils.createOffertaMobile(offerte.getJSONObject(i)));
                        }

                        if (abbonamentoRecyclerView.getAdapter() != null) {
                            ((OfferAdapter) abbonamentoRecyclerView.getAdapter()).updateOfferteSelezionate(offerteList);
                        }

                        if (ricaricabileRecyclerView.getAdapter() != null) {
                            ((OfferAdapter) ricaricabileRecyclerView.getAdapter()).updateOfferteSelezionate(offerteList);
                        }

                        checkValidationPacchetto();
                        return toSave;
                    } else {
                        return !toSave;
                    }
                } else {
                    if (selezionati < Integer.parseInt(pacchetto.getString("question4RicaricabileNumSim"))){
                        offerte.put(Utils.createOffertaMobile(offertaMobile));
//                        Log.d("SAVE OFFER", String.valueOf(idOfferta));
//                        Log.d("SAVE OFFER", offerte.toString());

                        ArrayList<OffertaMobile> offerteList = new ArrayList<>();
                        for (int i = 0; i < offerte.length(); i++) {
                            offerteList.add(Utils.createOffertaMobile(offerte.getJSONObject(i)));
                        }

                        if (abbonamentoRecyclerView.getAdapter() != null) {
                            ((OfferAdapter) abbonamentoRecyclerView.getAdapter()).updateOfferteSelezionate(offerteList);
                        }

                        if (ricaricabileRecyclerView.getAdapter() != null) {
                            ((OfferAdapter) ricaricabileRecyclerView.getAdapter()).updateOfferteSelezionate(offerteList);
                        }

                        checkValidationPacchetto();
                        return toSave;
                    } else {
                        return !toSave;
                    }
                }
            } else {
                for (int i = 0; i < offerte.length(); i++) {
                    if (offerte.getJSONObject(i).getString("idOfferta").compareTo(String.valueOf(offertaMobile.getIdOfferta())) == 0) {
                        offerte.remove(i);
//                        Log.d("REMOVE OFFER", String.valueOf(idOfferta));
//                        Log.d("REMOVE OFFER", offerte.toString());
                        break;
                    }
                }

                ArrayList<OffertaMobile> offerteList = new ArrayList<>();
                for (int i = 0; i < offerte.length(); i++) {
                    offerteList.add(Utils.createOffertaMobile(offerte.getJSONObject(i)));
                }

                if (abbonamentoRecyclerView.getAdapter() != null) {
                    ((OfferAdapter) abbonamentoRecyclerView.getAdapter()).updateOfferteSelezionate(offerteList);
                }

                if (ricaricabileRecyclerView.getAdapter() != null) {
                    ((OfferAdapter) ricaricabileRecyclerView.getAdapter()).updateOfferteSelezionate(offerteList);
                }

                checkValidationPacchetto();
                return toSave;
            }


//            if (toSave) {
//                offerte.put(Utils.addOfferta(String.valueOf(idOfferta), offerType));
//                Log.d("SAVE OFFER", String.valueOf(idOfferta));
//                Log.d("SAVE OFFER", offerte.toString());
//            } else {
//                for (int i = 0; i < offerte.length(); i++) {
//                    if (offerte.getJSONObject(i).getString("idOfferta").compareTo(String.valueOf(idOfferta)) == 0) {
//                        offerte.remove(i);
//                        Log.d("REMOVE OFFER", String.valueOf(idOfferta));
//                        Log.d("REMOVE OFFER", offerte.toString());
//                        break;
//                    }
//                }
//            }

//            ArrayList<String> offerteList = new ArrayList<>();
//            for (int i = 0; i < offerte.length(); i++) {
//                offerteList.add(offerte.getJSONObject(i).getString("idOfferta"));
//            }
//
//            if (abbonamentoRecyclerView.getAdapter() != null) {
//                ((OfferAdapter) abbonamentoRecyclerView.getAdapter()).updateOfferteSelezionate(offerteList);
//            }
//
//            if (ricaricabileRecyclerView.getAdapter() != null) {
//                ((OfferAdapter) ricaricabileRecyclerView.getAdapter()).updateOfferteSelezionate(offerteList);
//            }
//
//            checkValidationPacchetto();

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return toSave;
    }

    private void changeTickVisibility(int pacchetto, int visibility) {
        switch (pacchetto) {
            case 1:
                pacchetto1Tick.setVisibility(visibility);
                break;
            case 2:
                pacchetto2Tick.setVisibility(visibility);
                break;
            case 3:
                pacchetto3Tick.setVisibility(visibility);
                break;
            case 4:
                pacchetto4Tick.setVisibility(visibility);
                break;
        }
    }

    private void checkValidationPacchetto() {
        try {
            JSONArray pacchetti = (JSONArray) Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONArray("pacchetti");
            JSONObject pacchetto = pacchetti.getJSONObject(pacchettoVisibile - 1);
            JSONArray offerte = (JSONArray) pacchetto.getJSONArray("offerte");
            JSONObject pratica = (JSONObject) Session.getInstance().getJsonPratica().getJSONObject("pratica");
            pacchetto.put("offerteRaggruppate", new JSONArray());
            //MW aggiunto l'array offerte configraate per far si che in modifica lui elimina i dati precedenti dello step 8.
            pratica.put("offerteConfigurate", new JSONArray());

            boolean goOn = true;

            if (offerte.length() <= 0) {
                goOn = false;
            } else {
                if (abbonamentoListLayout.getVisibility() == View.VISIBLE && ricaricabileListLayout.getVisibility() == View.VISIBLE) {
                    boolean abbonamentoSelezionato = false, ricaricabileSelezionato = false;
                    for (int i = 0; i < offerte.length(); i++) {
                        if (offerte.getJSONObject(i).getString("offerType").toUpperCase().compareTo("Abbonamento".toUpperCase()) == 0) {
                            abbonamentoSelezionato = true;
                        }

                        if (offerte.getJSONObject(i).getString("offerType").toUpperCase().compareTo("Ricaricabile".toUpperCase()) == 0) {
                            ricaricabileSelezionato = true;
                        }
                    }
                    if (!abbonamentoSelezionato || !ricaricabileSelezionato) {
                        goOn = false;
                    }
                }
            }

            pacchetto.put("completedStep2", goOn);

            checkValidation();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void checkValidation() {
        try {

            //store data on sqlLite
            PraticheDAO.updatePratica(this.getActivity().getBaseContext());


            JSONArray pacchetti = (JSONArray) Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONArray("pacchetti");
            for (int i = 0; i < pacchetti.length(); i++) {
                changeTickVisibility(i + 1, pacchetti.getJSONObject(i).getBoolean("completedStep2") ? View.VISIBLE : View.GONE);
            }

            switch (pacchetti.length()) {
                case 1:
                    if (pacchetto1Tick.getVisibility() == View.VISIBLE) {
                        continuaButton.setEnabled(true);
                    } else {
                        continuaButton.setEnabled(false);
                    }
                    pacchetto2Tick.setVisibility(View.GONE);
                    pacchetto3Tick.setVisibility(View.GONE);
                    pacchetto4Tick.setVisibility(View.GONE);
                    break;
                case 2:
                    if (pacchetto1Tick.getVisibility() == View.VISIBLE && pacchetto2Tick.getVisibility() == View.VISIBLE) {
                        continuaButton.setEnabled(true);
                    } else {
                        continuaButton.setEnabled(false);
                    }
                    pacchetto3Tick.setVisibility(View.GONE);
                    pacchetto4Tick.setVisibility(View.GONE);
                    break;
                case 3:
                    if (pacchetto1Tick.getVisibility() == View.VISIBLE && pacchetto2Tick.getVisibility() == View.VISIBLE && pacchetto3Tick.getVisibility() == View.VISIBLE) {
                        continuaButton.setEnabled(true);
                    } else {
                        continuaButton.setEnabled(false);
                    }
                    pacchetto4Tick.setVisibility(View.GONE);
                    break;
                case 4:
                    if (pacchetto1Tick.getVisibility() == View.VISIBLE && pacchetto2Tick.getVisibility() == View.VISIBLE && pacchetto3Tick.getVisibility() == View.VISIBLE && pacchetto4Tick.getVisibility() == View.VISIBLE) {
                        continuaButton.setEnabled(true);
                    } else {
                        continuaButton.setEnabled(false);
                    }
                    break;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
