package it.wind.smartsales.fragments;

import android.app.Fragment;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import it.wind.smartsales.R;
import it.wind.smartsales.activity.MainActivity;
import it.wind.smartsales.adapter.PraticaAdapter;
import it.wind.smartsales.commons.Constants;
import it.wind.smartsales.commons.Log;
import it.wind.smartsales.customviews.AspectRatioView;
import it.wind.smartsales.customviews.RecyclerItemClickListener;
import it.wind.smartsales.customviews.VerticalSpaceItemDecoration;
import it.wind.smartsales.dao.PraticheDAO;
import it.wind.smartsales.entities.Pratica;
import it.wind.smartsales.entities.Session;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link DraftListFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link DraftListFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class DraftListFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    LinearLayout backButton;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private List<Pratica> listaPratiche = new ArrayList<>();
    private RecyclerView listaPraticheRecycler;
    private PraticaAdapter mAdapter;
    private String TAG = "DraftListFragment";
    private OnFragmentInteractionListener mListener;
    private TextView nomePraticaDettaglio;
    private int currentPraticaID;
    private TextView percCompletamentoTextView;
    private AspectRatioView percCompletamentoWidget;
    private Pratica currentPratica;
    private RelativeLayout navIcon1, navIcon2, navIcon3, navIcon4, navIcon5, navIcon6;
    private ImageView editBtn;
    private Button continuaButton;

    public DraftListFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment DraftListFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static DraftListFragment newInstance(String param1, String param2) {
        DraftListFragment fragment = new DraftListFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_draft_list, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        listaPraticheRecycler = (RecyclerView) this.getView().findViewById(R.id.listaPratiche);

        prepareData();
        mAdapter = new PraticaAdapter(listaPratiche);


        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this.getActivity());
        listaPraticheRecycler.setLayoutManager(mLayoutManager);
        listaPraticheRecycler.setItemAnimator(new DefaultItemAnimator());
        listaPraticheRecycler.setAdapter(mAdapter);
        listaPraticheRecycler.addItemDecoration(new VerticalSpaceItemDecoration(2));

        final Spinner spinner = (Spinner) this.getView().findViewById(R.id.filterSpinner);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity().getBaseContext(), R.array.draft_filter, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        nomePraticaDettaglio = ((TextView) this.getView().findViewById(R.id.nomePraticaDettaglio));
        nomePraticaDettaglio.setInputType(InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS);


        nomePraticaDettaglio.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    View view = DraftListFragment.this.getActivity().getCurrentFocus();
                    if (view != null) {
                        updateNomePratica(view);
                        InputMethodManager imm = (InputMethodManager) DraftListFragment.this.getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                    }
                    return true;
                }
                return false;
            }
        });


        navIcon1 = (RelativeLayout) this.getView().findViewById(R.id.nav_icon_1);
        navIcon2 = (RelativeLayout) this.getView().findViewById(R.id.nav_icon_2);
        navIcon3 = (RelativeLayout) this.getView().findViewById(R.id.nav_icon_3);
        navIcon4 = (RelativeLayout) this.getView().findViewById(R.id.nav_icon_4);
        navIcon5 = (RelativeLayout) this.getView().findViewById(R.id.nav_icon_5);
        navIcon6 = (RelativeLayout) this.getView().findViewById(R.id.nav_icon_6);

        editBtn = (ImageView) this.getView().findViewById(R.id.editDraftBtn);


        editBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                nomePraticaDettaglio.requestFocus();
            }
        });


        spinner.setAdapter(adapter);

//        Button spinnerButton = (Button) this.getView().findViewById(R.id.spinnerButton);
//        spinnerButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                spinner.performClick();
//
//            }
//        });

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                switch (position) {
                    case 0:
                        PraticaAdapter a1 = new PraticaAdapter(sortByPIVA(listaPratiche));
                        listaPraticheRecycler.swapAdapter(a1, false);
                        Log.i(TAG, "sortedByPIVA");
                        break;
                    case 1:
                        PraticaAdapter a2 = new PraticaAdapter(sortByRecenti(listaPratiche));
                        listaPraticheRecycler.swapAdapter(a2, false);
                        Log.i(TAG, "sortByRecenti");
                        break;
                    case 2:
                        PraticaAdapter a3 = new PraticaAdapter(sortByCompletamento(listaPratiche));
                        listaPraticheRecycler.swapAdapter(a3, false);
                        Log.i(TAG, "sortByCompletamento");
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });


        listaPraticheRecycler.addOnItemTouchListener(
                new RecyclerItemClickListener(getActivity().getBaseContext(), new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        Log.i(TAG, "pressed item " + position);

                        currentPratica = listaPratiche.get(position);
                        setDetailPratica(listaPratiche.get(position));
                    }
                })
        );


        backButton = (LinearLayout) this.getView().findViewById(R.id.backButton);
        backButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
//                getFragmentManager().beginTransaction().replace(R.id.fragment_container, ((MainActivity) getActivity()).getHomeAdminFragment()).commit();
                ((MainActivity) getActivity()).goToPage(Constants.Pages.PAGE_HOME_ADMIN);
                ((MainActivity) getActivity()).setNavIcon(0);
            }
        });
//        int search_button_id = getResources().getIdentifier("android:id/search_button", null, null);
//        ImageView search_button = (ImageView) this.getView().findViewById(search_button_id);
//
//        int search_bar_id = getResources().getIdentifier("android:id/search_bar", null, null);
//        LinearLayout search_bar = (LinearLayout) this.getView().findViewById(search_bar_id);
//        search_bar.removeView(search_button);
//
//
//
//        LinearLayout search_layout = (LinearLayout) this.getView().findViewById(R.id.searchLayout);
//        search_layout.addView(search_button);

        EditText searchQuery = (EditText) this.getView().findViewById(R.id.searchQuery);
        searchQuery.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                sortDraftList(s);
            }

        });

        percCompletamentoTextView = (TextView) view.findViewById(R.id.percCompletamentoTextDet);
        percCompletamentoWidget = (AspectRatioView) view.findViewById(R.id.percCompletamentoWidgetDet);


        continuaButton = (Button) this.getView().findViewById(R.id.continua);
        continuaButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (currentPratica != null) {
                    Session.getInstance().setJsonPratica(currentPratica.getJson_data(), currentPratica);
                }
                ((MainActivity) v.getContext()).goToPage(Constants.Pages.PAGE_MOBILE_INTERNET_CREATE_OFFER);

            }
        });


        initDetailPratica();


    }

    private void updateNomePratica(View v) {
        String nomePratica = ((EditText) v).getText().toString();
        //update db
        PraticheDAO.updateNomePratica(getActivity().getBaseContext(), currentPraticaID, nomePratica);
        //update recycler view
        List<Pratica> newPratiche = new ArrayList<Pratica>();

        for (Pratica pratica : listaPratiche) {
            if (pratica.getId() == currentPraticaID) {
                pratica.setNome_pratica(nomePratica);

            }
            newPratiche.add(pratica);
        }

        PraticaAdapter newPraticheAdp = new PraticaAdapter(newPratiche);
        listaPraticheRecycler.swapAdapter(newPraticheAdp, false);
    }

    private void initDetailPratica() {
        Pratica firstPratica = listaPratiche.size() == 0 ? null : listaPratiche.get(0);

        currentPraticaID = firstPratica == null ? 0 : listaPratiche.get(0).getId();

        currentPratica = firstPratica;

        nomePraticaDettaglio.setText(firstPratica == null ? "" : firstPratica.getNome_pratica());
        nomePraticaDettaglio.setInputType(InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS);

        if (firstPratica == null ? true : !firstPratica.isNomeEditable()) {
            nomePraticaDettaglio.setFocusable(false);
        }

        if (firstPratica != null) {

            Drawable res = null;
            switch (firstPratica.getStep()) {
                case PraticheDAO.STEP1:
                    res = getResources().getDrawable(R.drawable.bg_10);

                    break;
                case PraticheDAO.STEP2:
                    res = getResources().getDrawable(R.drawable.bg_20);

                    break;
                case PraticheDAO.STEP3:
                    res = getResources().getDrawable(R.drawable.bg_30);

                    break;
                case PraticheDAO.STEP4:
                    res = getResources().getDrawable(R.drawable.bg_40);

                    break;
                case PraticheDAO.STEP5:
                    res = getResources().getDrawable(R.drawable.bg_50);

                    break;
                case PraticheDAO.STEP6:
                    res = getResources().getDrawable(R.drawable.bg_60);

                    break;
                case PraticheDAO.STEP7:
                    res = getResources().getDrawable(R.drawable.bg_70);
                    //continuaButton.setEnabled(true);
                    break;
                case PraticheDAO.STEP8:
                    res = getResources().getDrawable(R.drawable.bg_80);

                    break;
                case PraticheDAO.STEP9:
                    res = getResources().getDrawable(R.drawable.bg_90);

                    break;
                case PraticheDAO.STEP10:

                    res = getResources().getDrawable(R.drawable.bg_100);
                    break;
            }
            percCompletamentoWidget.setBackground(res);

            percCompletamentoTextView.setText(calculatePercentualeCompletamento(firstPratica));
        } else {
            percCompletamentoWidget.setImageResource(0);
            percCompletamentoTextView.setText("0%");
        }

        if (firstPratica != null) {
            highlightNavIcon(firstPratica);
        }


        ((TextView) this.getView().findViewById(R.id.pivaValue)).setText(firstPratica == null ? "" : firstPratica.getP_iva());

        if (firstPratica.getStep().compareToIgnoreCase(PraticheDAO.STEP10) != 0) {
            ((TextView) this.getView().findViewById(R.id.stato)).setText(firstPratica == null ? "" : "In fase di completamento");
        } else {
            ((TextView) this.getView().findViewById(R.id.stato)).setText(firstPratica == null ? "" : "Pratica completata");
        }

        ((TextView) this.getView().findViewById(R.id.testoNota)).setText(firstPratica == null ? "" : firstPratica.getNote());

        ((TextView) this.getView().findViewById(R.id.lastUpdateNota)).setText(firstPratica == null ? "" : new SimpleDateFormat("dd/MM/yyyy").format(firstPratica.getLast_update_note()));
        ((TextView) this.getView().findViewById(R.id.lastUpdateNotaHour)).setText(firstPratica == null ? "" : new SimpleDateFormat("HH:mm:ss").format(firstPratica.getLast_update_note()));
    }

    private String calculatePercentualeCompletamento(Pratica firstPratica) {
        String result = null;

        switch (firstPratica.getStep()) {
            case PraticheDAO.STEP1:
                result = "10%";
                break;
            case PraticheDAO.STEP2:
                result = "20%";
                break;
            case PraticheDAO.STEP3:
                result = "30%";
                break;
            case PraticheDAO.STEP4:
                result = "40%";
                break;
            case PraticheDAO.STEP5:
                result = "50%";
                break;
            case PraticheDAO.STEP6:
                result = "60%";
                break;
            case PraticheDAO.STEP7:
                result = "70%";
                break;
            case PraticheDAO.STEP8:
                result = "80%";
                break;
            case PraticheDAO.STEP9:
                result = "90%";
                break;
            case PraticheDAO.STEP10:
                result = "100%";
                break;
        }

        return result;
    }

    private void highlightNavIcon(Pratica firstPratica) {
        switch (firstPratica.getStep()) {
            case PraticheDAO.STEP1:
                //continuaButton.setEnabled(true);
            case PraticheDAO.STEP2:
                //continuaButton.setEnabled(true);
            case PraticheDAO.STEP3:
                //continuaButton.setEnabled(true);
            case PraticheDAO.STEP4:
                navIcon1.setSelected(true);
                navIcon2.setSelected(false);
                navIcon3.setSelected(false);
                navIcon4.setSelected(false);
                navIcon5.setSelected(false);
                navIcon6.setSelected(false);
                //continuaButton.setEnabled(true);
                break;
            case PraticheDAO.STEP5:
                navIcon1.setSelected(true);
                navIcon2.setSelected(true);
                navIcon3.setSelected(false);
                navIcon4.setSelected(false);
                navIcon5.setSelected(false);
                navIcon6.setSelected(false);
                //continuaButton.setEnabled(true);
                break;
            case PraticheDAO.STEP6:
                navIcon1.setSelected(true);
                navIcon2.setSelected(true);
                navIcon3.setSelected(true);
                navIcon4.setSelected(false);
                navIcon5.setSelected(false);
                navIcon6.setSelected(false);
                //continuaButton.setEnabled(true);
                break;
            case PraticheDAO.STEP7:
                navIcon1.setSelected(true);
                navIcon2.setSelected(true);
                navIcon3.setSelected(true);
                navIcon4.setSelected(true);
                navIcon5.setSelected(false);
                navIcon6.setSelected(false);
                //continuaButton.setEnabled(true);
                break;
            case PraticheDAO.STEP8:
                navIcon1.setSelected(true);
                navIcon2.setSelected(true);
                navIcon3.setSelected(true);
                navIcon4.setSelected(true);
                navIcon5.setSelected(true);
                navIcon6.setSelected(false);
                //continuaButton.setEnabled(true);
                break;
            case PraticheDAO.STEP9:
                navIcon1.setSelected(true);
                navIcon2.setSelected(true);
                navIcon3.setSelected(true);
                navIcon4.setSelected(true);
                navIcon5.setSelected(true);
                navIcon6.setSelected(true);
                //continuaButton.setEnabled(true);
                break;
            case PraticheDAO.STEP10:
                navIcon1.setSelected(true);
                navIcon2.setSelected(true);
                navIcon3.setSelected(true);
                navIcon4.setSelected(true);
                navIcon5.setSelected(true);
                navIcon6.setSelected(true);
                //continuaButton.setEnabled(false);
                break;

        }
    }

    private void sortDraftList(CharSequence s) {


        ArrayList<Pratica> filteredPratiche = new ArrayList<>();

        for (Pratica pratica : listaPratiche) {
            if (pratica.getNome_pratica() != null && (pratica.getNome_pratica()).toLowerCase().contains(s.toString().toLowerCase())) {
                filteredPratiche.add(pratica);
            }
        }

        PraticaAdapter mAdapter = new PraticaAdapter(filteredPratiche);
        listaPraticheRecycler.swapAdapter(mAdapter, false);

//        listaPraticheRecycler.getAdapter().notifyDataSetChanged();

    }

    private void prepareData() {
        listaPratiche = PraticheDAO.retrievePratiche(this.getActivity());

    }


    private List<Pratica> sortByPIVA(List<Pratica> pratiche) {
        Collections.sort(pratiche, new Comparator<Pratica>() {
            @Override
            public int compare(Pratica pratica1, Pratica pratica2) {


                Double pratica1PIVA = Double.parseDouble(pratica1.getP_iva().equals("") ? "0" : pratica1.getP_iva());
                Double pratica2PIVA = Double.parseDouble(pratica2.getP_iva().equals("") ? "0" : pratica2.getP_iva());

                return pratica1PIVA.compareTo(pratica2PIVA);
            }
        });

        return pratiche;
    }

    private List<Pratica> sortByRecenti(List<Pratica> pratiche) {
        Collections.sort(pratiche, new Comparator<Pratica>() {
            @Override
            public int compare(Pratica pratica1, Pratica pratica2) {
                return pratica1.getLast_update().compareTo(pratica2.getLast_update()) == 0 ? -1 : 0;
            }
        });

        return pratiche;
    }


    private List<Pratica> sortByCompletamento(List<Pratica> pratiche) {
        Collections.sort(pratiche, new Comparator<Pratica>() {
            @Override
            public int compare(Pratica pratica1, Pratica pratica2) {

                Integer pratica1Perc = Integer.parseInt(pratica1.getStep().substring(pratica1.getStep().length() - 1));
                Integer pratica2Perc = Integer.parseInt(pratica2.getStep().substring(pratica2.getStep().length() - 1));

                return pratica1Perc.compareTo(pratica2Perc);
            }
        });

        return pratiche;
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    private void setDetailPratica(Pratica pratica) {
        this.currentPraticaID = pratica.getId();
        nomePraticaDettaglio.setText(pratica.getNome_pratica());

        if (!pratica.isNomeEditable()) {
            nomePraticaDettaglio.setFocusableInTouchMode(false);
            nomePraticaDettaglio.setFocusable(false);
            nomePraticaDettaglio.setClickable(false);

        } else {
            nomePraticaDettaglio.setFocusableInTouchMode(true);
            nomePraticaDettaglio.setFocusable(true);
            nomePraticaDettaglio.setClickable(true);
        }
        nomePraticaDettaglio.setInputType(InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS);

        ((TextView) this.getView().findViewById(R.id.pivaValue)).setText(pratica.getP_iva());
        if (pratica.getStep().compareToIgnoreCase(PraticheDAO.STEP10) != 0) {
            ((TextView) this.getView().findViewById(R.id.stato)).setText(pratica == null ? "" : "In fase di completamento");
        } else {
            ((TextView) this.getView().findViewById(R.id.stato)).setText(pratica == null ? "" : "Pratica completata");
        }
        ((TextView) this.getView().findViewById(R.id.testoNota)).setText(pratica.getNote());

        ((TextView) this.getView().findViewById(R.id.lastUpdateNota)).setText(new SimpleDateFormat("dd/MM/yyyy").format(pratica.getLast_update_note()));
        ((TextView) this.getView().findViewById(R.id.lastUpdateNotaHour)).setText(new SimpleDateFormat("HH:mm:ss").format(pratica.getLast_update_note()));


        Drawable res = null;
        switch (pratica.getStep()) {
            case PraticheDAO.STEP1:
                res = getResources().getDrawable(R.drawable.bg_10);
                break;
            case PraticheDAO.STEP2:
                res = getResources().getDrawable(R.drawable.bg_20);
                break;
            case PraticheDAO.STEP3:
                res = getResources().getDrawable(R.drawable.bg_30);
                break;
            case PraticheDAO.STEP4:
                res = getResources().getDrawable(R.drawable.bg_40);
                break;
            case PraticheDAO.STEP5:
                res = getResources().getDrawable(R.drawable.bg_50);
                break;
            case PraticheDAO.STEP6:
                res = getResources().getDrawable(R.drawable.bg_60);
                break;
            case PraticheDAO.STEP7:
                res = getResources().getDrawable(R.drawable.bg_70);
                break;
            case PraticheDAO.STEP8:
                res = getResources().getDrawable(R.drawable.bg_80);
                break;
            case PraticheDAO.STEP9:
                res = getResources().getDrawable(R.drawable.bg_90);
                break;
            case PraticheDAO.STEP10:
                res = getResources().getDrawable(R.drawable.bg_100);
                break;
        }

        percCompletamentoWidget.setBackground(res);

        percCompletamentoTextView.setText(calculatePercentualeCompletamento(pratica));

        highlightNavIcon(pratica);
    }

//    @Override
//    public void onAttach(Context context) {
//        super.onAttach(context);
//        if (context instanceof OnFragmentInteractionListener) {
//            mListener = (OnFragmentInteractionListener) context;
//        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
//    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }


    static class DividerItemDecoration extends RecyclerView.ItemDecoration {

        private static final int[] ATTRS = new int[]{android.R.attr.listDivider};

        private Drawable mDivider;

        /**
         * Default divider will be used
         */
        public DividerItemDecoration(Context context) {
            final TypedArray styledAttributes = context.obtainStyledAttributes(ATTRS);
            mDivider = styledAttributes.getDrawable(0);
            styledAttributes.recycle();
        }

        /**
         * Custom divider will be used
         */
        public DividerItemDecoration(Context context, int resId) {
            mDivider = ContextCompat.getDrawable(context, resId);
        }

        @Override
        public void onDraw(Canvas c, RecyclerView parent, RecyclerView.State state) {
            int left = parent.getPaddingLeft();
            int right = parent.getWidth() - parent.getPaddingRight();

            int childCount = parent.getChildCount();
            for (int i = 0; i < childCount; i++) {
                View child = parent.getChildAt(i);

                RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) child.getLayoutParams();

                int top = child.getBottom() + params.bottomMargin;
                int bottom = top + mDivider.getIntrinsicHeight();

                mDivider.setBounds(left, top, right, bottom);
                mDivider.draw(c);
            }
        }


    }
}

