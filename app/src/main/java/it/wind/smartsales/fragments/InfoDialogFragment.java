package it.wind.smartsales.fragments;

import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import it.wind.smartsales.R;
import it.wind.smartsales.customviews.AspectRatioView;
import it.wind.smartsales.entities.OffertaMobile;
import it.wind.smartsales.entities.OpzioneMobile;

/**
 * Created by luca.quaranta on 03/12/2015.
 */
public class InfoDialogFragment extends DialogFragment {

    public static final String KEY = "OFFERTA_MOBILE_KEY";

    private TextView offerDescription, offerDescriptionUpper, offerInfo;
    private AspectRatioView closeButton;

    public InfoDialogFragment() {
    }

    private void initializeComponents(View view) {
        offerDescription = (TextView) view.findViewById(R.id.offer_description);
        offerDescriptionUpper = (TextView) view.findViewById(R.id.offer_description_upper);
        offerInfo = (TextView) view.findViewById(R.id.offer_info);
        closeButton = (AspectRatioView) view.findViewById(R.id.close_button);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
            Dialog dialog = super.onCreateDialog(savedInstanceState);
            dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
            return dialog;
    }

    @Override
    public void onResume() {
        super.onResume();
        int width = getResources().getDimensionPixelSize(R.dimen.fragment_dialog_info_width);
        getDialog().getWindow().setLayout(width, ViewGroup.LayoutParams.WRAP_CONTENT);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_dialog_info, container, false);

        initializeComponents(view);

        if (getArguments().getSerializable(KEY) instanceof OffertaMobile) {
            OffertaMobile offertaMobile = (OffertaMobile) getArguments().getSerializable(KEY);
            offerDescription.setText(offertaMobile.getOfferDescription());
            offerDescriptionUpper.setText(offertaMobile.getOfferDescriptionUpper());
            offerInfo.setText(offertaMobile.getOfferDescriptionIButton());
        } else if (getArguments().getSerializable(KEY) instanceof OpzioneMobile){
            OpzioneMobile opzioneMobile = (OpzioneMobile) getArguments().getSerializable(KEY);
            offerDescription.setText(opzioneMobile.getOpzioneDesc());
            offerDescriptionUpper.setText(opzioneMobile.getOpzioneDescUpper());
            offerInfo.setText(opzioneMobile.getOpzioneDescriptionIButton());
        }



        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        return view;
    }




}
