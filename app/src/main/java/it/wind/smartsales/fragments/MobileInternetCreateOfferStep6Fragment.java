package it.wind.smartsales.fragments;

import android.app.DatePickerDialog;
import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.nineoldandroids.animation.Animator;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import it.wind.smartsales.R;
import it.wind.smartsales.activity.MainActivity;
import it.wind.smartsales.commons.Log;
import it.wind.smartsales.commons.Utils;
import it.wind.smartsales.customviews.AspectRatioView;
import it.wind.smartsales.dao.PraticheDAO;
import it.wind.smartsales.entities.Session;

/**
 * Created by luca.quaranta on 10/02/2016.
 */
public class MobileInternetCreateOfferStep6Fragment extends Fragment {

    private RelativeLayout header1, header2, header3, header4, header5, header6;
    private LinearLayout content1, content2, content3, content4, content5, content6, content7;
    private AspectRatioView arrow1, arrow2, arrow3, arrow4, arrow5, arrow6, arrow7;
    private Button continuaButton;

    private ArrayList<String> sessoList, documentoIdentitaList, modalitaFatturazioneList;

    private EditText
            content1Nome, content1Cognome, content1RagioneSociale,
            content1FirmaSocietaria, content1IndirizzoSedeLegale, content1NumeroCivico,
            content1Citta, content1Provincia, content1CAP,
            content1ProvinciaIscrizione, content1PartitaIva, content1CodiceFiscale;

    private EditText
            content2Nome, content2Cognome, content2Telefono,
            content2Fax, content2Email, content2CodiceFiscale,
            content2LuogoDiNascita, content2DataDiNascita,
            content2Provincia, content2Nazionalita,
            content2NumeroDocumentoIdentita, content2RilasciatoDa, content2Data;

    private Spinner content2Sesso, content2DocumentoIdentita;
    private DatePickerDialog dataDiNascitaPicker, dataPicker;
    private CheckBox content2Si, content2No;

    private SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

    private EditText
            content3RiferimentoSede, content3Indirizzo, content3NumeroCivico,
            content3Citta, content3Provincia, content3Cap;

    private EditText
            content4Nome, content4Cognome, content4Email,
            content4Telefono, content4Fax, content4Cellulare;

    private Spinner content4ModalitaFatturazione;

    private CheckBox content4RichiestaDettaglioChiamate, content4AgevolazioniFiscali;

    private CheckBox content5FormatoElettronicoViaEmail, content5CartaceoPerPostaOrdinaria;

    private CheckBox content6AreaClienti, content6SupportoCartaceo, content6SupportoCd;

    private CheckBox content7check, content7modalitaFatturazioneElettronica, content7SplitPayment;
    private EditText content7CodiceIdentificativoUfficio;

    private View.OnFocusChangeListener myFocusChangeListener;
    private AdapterView.OnItemSelectedListener myItemSelectedListener;

    private void initializeComponents(View view) {
        continuaButton = (Button) view.findViewById(R.id.continua_button);

        header1 = (RelativeLayout) view.findViewById(R.id.header_1);
        header2 = (RelativeLayout) view.findViewById(R.id.header_2);
        header3 = (RelativeLayout) view.findViewById(R.id.header_3);
        header4 = (RelativeLayout) view.findViewById(R.id.header_4);
        header5 = (RelativeLayout) view.findViewById(R.id.header_5);
        header6 = (RelativeLayout) view.findViewById(R.id.header_6);

        content1 = (LinearLayout) view.findViewById(R.id.content_1);
        content2 = (LinearLayout) view.findViewById(R.id.content_2);
        content3 = (LinearLayout) view.findViewById(R.id.content_3);
        content4 = (LinearLayout) view.findViewById(R.id.content_4);
        content5 = (LinearLayout) view.findViewById(R.id.content_5);
        content6 = (LinearLayout) view.findViewById(R.id.content_6);
        content7 = (LinearLayout) view.findViewById(R.id.content_7);

        arrow1 = (AspectRatioView) view.findViewById(R.id.arrow_1);
        arrow2 = (AspectRatioView) view.findViewById(R.id.arrow_2);
        arrow3 = (AspectRatioView) view.findViewById(R.id.arrow_3);
        arrow4 = (AspectRatioView) view.findViewById(R.id.arrow_4);
        arrow5 = (AspectRatioView) view.findViewById(R.id.arrow_5);
        arrow6 = (AspectRatioView) view.findViewById(R.id.arrow_6);
        arrow7 = (AspectRatioView) view.findViewById(R.id.arrow_7);

        content1Nome = (EditText) view.findViewById(R.id.content_1_nome);
        content1Cognome = (EditText) view.findViewById(R.id.content_1_cognome);
        content1RagioneSociale = (EditText) view.findViewById(R.id.content_1_ragione_sociale);
        content1FirmaSocietaria = (EditText) view.findViewById(R.id.content_1_firma_societaria);
        content1IndirizzoSedeLegale = (EditText) view.findViewById(R.id.content_1_indirizzo_sede_legale);
        content1NumeroCivico = (EditText) view.findViewById(R.id.content_1_numero_civico);
        content1Citta = (EditText) view.findViewById(R.id.content_1_citta);
        content1Provincia = (EditText) view.findViewById(R.id.content_1_provincia);
        content1CAP = (EditText) view.findViewById(R.id.content_1_cap);
        content1ProvinciaIscrizione = (EditText) view.findViewById(R.id.content_1_provincia_iscrizione);
        content1PartitaIva = (EditText) view.findViewById(R.id.content_1_partita_iva);
        content1CodiceFiscale = (EditText) view.findViewById(R.id.content_1_codice_fiscale);

        content2Nome = (EditText) view.findViewById(R.id.content_2_nome);
        content2Cognome = (EditText) view.findViewById(R.id.content_2_cognome);
        content2Telefono = (EditText) view.findViewById(R.id.content_2_telefono);
        content2Fax = (EditText) view.findViewById(R.id.content_2_fax);
        content2Email = (EditText) view.findViewById(R.id.content_2_email);
        content2CodiceFiscale = (EditText) view.findViewById(R.id.content_2_codice_fiscale);
        content2Sesso = (Spinner) view.findViewById(R.id.content_2_sesso);
        content2LuogoDiNascita = (EditText) view.findViewById(R.id.content_2_luogo_di_nascita);
        content2DataDiNascita = (EditText) view.findViewById(R.id.content_2_data_di_nascita);
        content2Provincia = (EditText) view.findViewById(R.id.content_2_provincia);
        content2Nazionalita = (EditText) view.findViewById(R.id.content_2_nazionalita);
        content2DocumentoIdentita = (Spinner) view.findViewById(R.id.content_2_documento_identita);
        content2NumeroDocumentoIdentita = (EditText) view.findViewById(R.id.content_2_numero_documento_identita);
        content2RilasciatoDa = (EditText) view.findViewById(R.id.content_2_rilasciato_da);
        content2Data = (EditText) view.findViewById(R.id.content_2_data);
        content2Si = (CheckBox) view.findViewById(R.id.content_2_si);
        content2No = (CheckBox) view.findViewById(R.id.content_2_no);

        sessoList = new ArrayList<String>();
        sessoList.add("Selezionare...");
        sessoList.add("Maschio");
        sessoList.add("Femmina");
        ArrayAdapter<String> sessoAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, sessoList);
        sessoAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        content2Sesso.setAdapter(sessoAdapter);

        documentoIdentitaList = new ArrayList<String>();
        documentoIdentitaList.add("Scegli un documento...");
        documentoIdentitaList.add("Carta Identità");
        documentoIdentitaList.add("Patente");
        documentoIdentitaList.add("Passaporto");
        ArrayAdapter<String> documentoIdentitaAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, documentoIdentitaList);
        documentoIdentitaAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        content2DocumentoIdentita.setAdapter(documentoIdentitaAdapter);

        content2DataDiNascita.setInputType(InputType.TYPE_NULL);
        content2Data.setInputType(InputType.TYPE_NULL);

        Calendar calendar = Calendar.getInstance();
        dataDiNascitaPicker = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                content2DataDiNascita.setText(dateFormat.format(newDate.getTime()));
                checkContent();
            }

        }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));

        dataPicker = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                content2Data.setText(dateFormat.format(newDate.getTime()));
                checkContent();
            }

        }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));

        content2DataDiNascita.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dataDiNascitaPicker.show();
            }
        });

        content2Data.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dataPicker.show();
            }
        });

        content2Si.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (content2Si.isChecked()) {
                    content2Si.setChecked(true);
                    content2No.setChecked(false);

                    content2Nome.setEnabled(false);
                    content2Cognome.setEnabled(false);
                    content2CodiceFiscale.setEnabled(false);

                    content2Nome.setText(content1Nome.getText().toString());
                    content2Cognome.setText(content1Cognome.getText().toString());
                    content2CodiceFiscale.setText(content1CodiceFiscale.getText().toString());

                    content2Nome.setBackground(getActivity().getResources().getDrawable(R.drawable.gray_shape_edittext));
                    content2Cognome.setBackground(getActivity().getResources().getDrawable(R.drawable.gray_shape_edittext));
                    content2CodiceFiscale.setBackground(getActivity().getResources().getDrawable(R.drawable.gray_shape_edittext));

                } else {
                    content2Si.setChecked(true);
                }
            }
        });

        content2No.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (content2No.isChecked()) {
                    content2No.setChecked(true);
                    content2Si.setChecked(false);

                    content2Nome.setEnabled(true);
                    content2Cognome.setEnabled(true);
                    content2CodiceFiscale.setEnabled(true);

                    content2Nome.setText("");
                    content2Cognome.setText("");
                    content2CodiceFiscale.setText("");

                    content2Nome.setBackground(getActivity().getResources().getDrawable(R.drawable.white_shape_edittext));
                    content2Cognome.setBackground(getActivity().getResources().getDrawable(R.drawable.white_shape_edittext));
                    content2CodiceFiscale.setBackground(getActivity().getResources().getDrawable(R.drawable.white_shape_edittext));
                } else {
                    content2No.setChecked(true);
                }
            }
        });

        content1Nome.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (content2Si.isChecked()) {
                    content2Nome.setText(s);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        content1Cognome.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (content2Si.isChecked()) {
                    content2Cognome.setText(s);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        content1CodiceFiscale.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (content2Si.isChecked()) {
                    content2CodiceFiscale.setText(s);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        content3RiferimentoSede = (EditText) view.findViewById(R.id.content_3_riferimento_sede);
        content3Indirizzo = (EditText) view.findViewById(R.id.content_3_indirizzo);
        content3NumeroCivico = (EditText) view.findViewById(R.id.content_3_numero_civico);
        content3Citta = (EditText) view.findViewById(R.id.content_3_citta);
        content3Provincia = (EditText) view.findViewById(R.id.content_3_provincia);
        content3Cap = (EditText) view.findViewById(R.id.content_3_cap);

        content4Nome = (EditText) view.findViewById(R.id.content_4_nome);
        content4Cognome = (EditText) view.findViewById(R.id.content_4_cognome);
        content4Email = (EditText) view.findViewById(R.id.content_4_email);
        content4Telefono = (EditText) view.findViewById(R.id.content_4_telefono);
        content4Fax = (EditText) view.findViewById(R.id.content_4_fax);
        content4Cellulare = (EditText) view.findViewById(R.id.content_4_cellulare);
        content4ModalitaFatturazione = (Spinner) view.findViewById(R.id.content_4_modalita_fatturazione);
        content4RichiestaDettaglioChiamate = (CheckBox) view.findViewById(R.id.content_4_richiesta_dettaglo_chiamate);
        content4AgevolazioniFiscali = (CheckBox) view.findViewById(R.id.content_4_agevolazioni_fiscali);

        modalitaFatturazioneList = new ArrayList<String>();
        modalitaFatturazioneList.add("Fattura unica");
        modalitaFatturazioneList.add("Fattura per Centro di Fatturazione");
        ArrayAdapter<String> modalitaFatturazioneAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, modalitaFatturazioneList);
        modalitaFatturazioneAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        content4ModalitaFatturazione.setAdapter(modalitaFatturazioneAdapter);

        content5FormatoElettronicoViaEmail = (CheckBox) view.findViewById(R.id.content_5_formato_elettronico_via_email);
        content5CartaceoPerPostaOrdinaria = (CheckBox) view.findViewById(R.id.content_5_cartaceo_per_posta_ordinaria);

        content5FormatoElettronicoViaEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (content5FormatoElettronicoViaEmail.isChecked()) {
                    content5FormatoElettronicoViaEmail.setChecked(true);
                    content5CartaceoPerPostaOrdinaria.setChecked(false);
                    checkContent();
                } else {
                    content5FormatoElettronicoViaEmail.setChecked(true);
                }
            }
        });

        content5CartaceoPerPostaOrdinaria.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (content5CartaceoPerPostaOrdinaria.isChecked()) {
                    content5CartaceoPerPostaOrdinaria.setChecked(true);
                    content5FormatoElettronicoViaEmail.setChecked(false);
                    checkContent();
                } else {
                    content5CartaceoPerPostaOrdinaria.setChecked(true);
                }
            }
        });

        content6AreaClienti = (CheckBox) view.findViewById(R.id.content_6_area_clienti);
        content6SupportoCartaceo = (CheckBox) view.findViewById(R.id.content_6_supporto_cartaceo);
        content6SupportoCd = (CheckBox) view.findViewById(R.id.content_6_supporto_cd);

        content6AreaClienti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                content6AreaClienti.setChecked(true);
                content6SupportoCartaceo.setChecked(false);
                content6SupportoCd.setChecked(false);
            }
        });

        content6SupportoCartaceo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                content6AreaClienti.setChecked(false);
                content6SupportoCartaceo.setChecked(true);
                content6SupportoCd.setChecked(false);
            }
        });

        content6SupportoCd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                content6AreaClienti.setChecked(false);
                content6SupportoCartaceo.setChecked(false);
                content6SupportoCd.setChecked(true);
            }
        });

        content7check = (CheckBox) view.findViewById(R.id.content_7_check);
        content7modalitaFatturazioneElettronica = (CheckBox) view.findViewById(R.id.content_7_modalita_fatturazione_elettronica);
        content7SplitPayment = (CheckBox) view.findViewById(R.id.content_7_split_payment);
        content7CodiceIdentificativoUfficio = (EditText) view.findViewById(R.id.content_7_codice_identificativo_ufficio);

        myFocusChangeListener = new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    checkContent();
                }
            }
        };

        myItemSelectedListener = new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                checkContent();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        };

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_mobile_internet_create_offer_step_6, container, false);
        initializeComponents(view);

        JSONObject jsonPratica;
        try {
            jsonPratica = Session.getInstance().getJsonPratica().getJSONObject("pratica");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        header1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                manageContent(content1, arrow1);
            }
        });

        header2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                manageContent(content2, arrow2);
            }
        });

        header3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                manageContent(content3, arrow3);
            }
        });

        header4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                manageContent(content4, arrow4);
            }
        });

        header5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                manageContent(content5, arrow5);
            }
        });

        header6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                manageContent(content6, arrow6);
            }
        });

        content7check.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                manageContent(content7, arrow7);
                checkContent();
            }
        });

        addFocusControl();

        populatePage();
        checkContent();

        continuaButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Session.getInstance().getPratica().setStep(PraticheDAO.STEP7);
                PraticheDAO.updatePratica(getActivity());

                Log.d("JSON PRATICA)", Session.getInstance().getJsonPratica().toString());
                if(Session.getInstance().getDealer().getDealerRole().compareTo("UserSales")!=0)
                    ((MainActivity) getActivity()).goForward();
                else
                    ((MainActivity) getActivity()).goForwardSelectOffer();
            }
        });

        return view;
    }

    private void manageContent(final LinearLayout contentLayout, final AspectRatioView arrow) {
        if (contentLayout.getVisibility() == View.GONE) {
            YoYo.with(Techniques.SlideInDown).duration(300).withListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animation) {
                    contentLayout.setVisibility(View.VISIBLE);
                }

                @Override
                public void onAnimationEnd(Animator animation) {

                }

                @Override
                public void onAnimationCancel(Animator animation) {

                }

                @Override
                public void onAnimationRepeat(Animator animation) {

                }
            }).playOn(contentLayout);

            arrow.setBackground(getActivity().getResources().getDrawable(R.drawable.arrow_up_inserimento));
        } else {
            YoYo.with(Techniques.SlideOutUp).duration(300).withListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animation) {

                }

                @Override
                public void onAnimationEnd(Animator animation) {
                    contentLayout.setVisibility(View.GONE);
                }

                @Override
                public void onAnimationCancel(Animator animation) {

                }

                @Override
                public void onAnimationRepeat(Animator animation) {

                }
            }).playOn(contentLayout);
            arrow.setBackground(getActivity().getResources().getDrawable(R.drawable.arrow_down_inserimento));
        }
    }

    private boolean checkContent1() {
        if (TextUtils.isEmpty(content1Nome.getText().toString().trim())) {
            return false;
        }

        if (TextUtils.isEmpty(content1Cognome.getText().toString().trim())) {
            return false;
        }

        if (TextUtils.isEmpty(content1RagioneSociale.getText().toString().trim())) {
            return false;
        }

        if (TextUtils.isEmpty(content1FirmaSocietaria.getText().toString().trim())) {
            return false;
        }

        if (TextUtils.isEmpty(content1IndirizzoSedeLegale.getText().toString().trim())) {
            return false;
        }

        if (TextUtils.isEmpty(content1NumeroCivico.getText().toString().trim())) {
            return false;
        }

        if (TextUtils.isEmpty(content1Citta.getText().toString().trim())) {
            return false;
        }

        if (TextUtils.isEmpty(content1Provincia.getText().toString().trim())) {
            return false;
        }

        if (TextUtils.isEmpty(content1CAP.getText().toString().trim()) || content1CAP.getText().toString().trim().length() != 5) {
            return false;
        }

        if (TextUtils.isEmpty(content1ProvinciaIscrizione.getText().toString().trim())) {
            return false;
        }

        if (TextUtils.isEmpty(content1PartitaIva.getText().toString().trim()) || content1PartitaIva.getText().toString().trim().length() != 11) {
            return false;
        }

        if (TextUtils.isEmpty(content1CodiceFiscale.getText().toString().trim()) || !Utils.checkCodiceFiscale(content1CodiceFiscale.getText().toString().trim())) {
            return false;
        }

        return true;
    }

    private boolean checkContent2() {
        if (TextUtils.isEmpty(content2Nome.getText().toString().trim())) {
            return false;
        }

        if (TextUtils.isEmpty(content2Cognome.getText().toString().trim())) {
            return false;
        }

        if (TextUtils.isEmpty(content2Telefono.getText().toString().trim())) {
            return false;
        }

        if (TextUtils.isEmpty(content2Email.getText().toString().trim()) || !Utils.isEmailValid(content2Email.getText().toString().trim())) {
            return false;
        }

        if (TextUtils.isEmpty(content2CodiceFiscale.getText().toString().trim()) || !Utils.checkCodiceFiscale(content2CodiceFiscale.getText().toString().trim())) {
            return false;
        }

        if (content2Sesso.getSelectedItemPosition() == 0) {
            return false;
        }

        if (TextUtils.isEmpty(content2LuogoDiNascita.getText().toString().trim())) {
            return false;
        }

        if (TextUtils.isEmpty(content2DataDiNascita.getText().toString().trim())) {
            return false;
        }

        if (TextUtils.isEmpty(content2Provincia.getText().toString().trim())) {
            return false;
        }

        if (TextUtils.isEmpty(content2Nazionalita.getText().toString().trim())) {
            return false;
        }

        if (content2DocumentoIdentita.getSelectedItemPosition() == 0) {
            return false;
        }

        if (TextUtils.isEmpty(content2NumeroDocumentoIdentita.getText().toString().trim())) {
            return false;
        }

        if (TextUtils.isEmpty(content2RilasciatoDa.getText().toString().trim())) {
            return false;
        }

        if (TextUtils.isEmpty(content2Data.getText().toString().trim())) {
            return false;
        }

        return true;
    }

    private boolean checkContent3() {
        if (TextUtils.isEmpty(content3RiferimentoSede.getText().toString().trim())) {
            return false;
        }

        if (TextUtils.isEmpty(content3Indirizzo.getText().toString().trim())) {
            return false;
        }

        if (TextUtils.isEmpty(content3NumeroCivico.getText().toString().trim())) {
            return false;
        }

        if (TextUtils.isEmpty(content3Citta.getText().toString().trim())) {
            return false;
        }

        if (TextUtils.isEmpty(content3Provincia.getText().toString().trim())) {
            return false;
        }

        if (TextUtils.isEmpty(content3Cap.getText().toString().trim()) || content3Cap.getText().toString().trim().length() != 5) {
            return false;
        }

        return true;
    }

    private boolean checkContent4() {
        if (TextUtils.isEmpty(content4Nome.getText().toString().trim())) {
            return false;
        }

        if (TextUtils.isEmpty(content4Cognome.getText().toString().trim())) {
            return false;
        }

        if (TextUtils.isEmpty(content4Email.getText().toString().trim())) {
            return false;
        }

        if (TextUtils.isEmpty(content4Telefono.getText().toString().trim())) {
            return false;
        }

        if (TextUtils.isEmpty(content4Cellulare.getText().toString().trim()) || content4Cellulare.getText().toString().trim().length() < 9) {
            return false;
        }

        return true;
    }

    private boolean checkContent5() {
        if (!content5FormatoElettronicoViaEmail.isChecked() && !content5CartaceoPerPostaOrdinaria.isChecked()) {
            return false;
        }
        return true;
    }

    private boolean checkContent6() {
        if (!content6AreaClienti.isChecked() && !content6SupportoCartaceo.isChecked() && !content6SupportoCd.isChecked()) {
            return false;
        }

        return true;
    }

    private boolean checkContent7() {
        if (content7check.isChecked()) {
            if (!content7modalitaFatturazioneElettronica.isChecked()) {
                return false;
            }

            if (!TextUtils.isEmpty(content7CodiceIdentificativoUfficio.getText().toString().trim()) && content7CodiceIdentificativoUfficio.getText().toString().trim().length()!=6) {
                return false;
            }
            return true;
        } else {
            return true;
        }
    }

    private void checkContent() {
        updateJson();
        if (checkContent1() && checkContent2() && checkContent3() && checkContent4() && checkContent5() && checkContent6() && checkContent7()) {
            continuaButton.setEnabled(true);
        } else {
            continuaButton.setEnabled(false);
        }
        //scommentare per abilitare il controllo
        continuaButton.setEnabled(true);
    }

    private void addFocusControl() {
        content1Nome.setOnFocusChangeListener(myFocusChangeListener);
        content1Cognome.setOnFocusChangeListener(myFocusChangeListener);
        content1RagioneSociale.setOnFocusChangeListener(myFocusChangeListener);
        content1FirmaSocietaria.setOnFocusChangeListener(myFocusChangeListener);
        content1IndirizzoSedeLegale.setOnFocusChangeListener(myFocusChangeListener);
        content1NumeroCivico.setOnFocusChangeListener(myFocusChangeListener);
        content1Citta.setOnFocusChangeListener(myFocusChangeListener);
        content1Provincia.setOnFocusChangeListener(myFocusChangeListener);
        content1CAP.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    if (!TextUtils.isEmpty(content1CAP.getText().toString().trim()) && content1CAP.getText().toString().trim().length() != 5) {
                        Utils.showWarningMessage(getActivity(), "", "CAP non valido");
                    } else {
                        checkContent();
                    }
                }
            }
        });
        content1ProvinciaIscrizione.setOnFocusChangeListener(myFocusChangeListener);
        content1PartitaIva.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    if (!TextUtils.isEmpty(content1PartitaIva.getText().toString().trim()) && content1PartitaIva.getText().toString().trim().length() != 11) {
                        Utils.showWarningMessage(getActivity(), "", "Partita IVA non valida");
                    } else {
                        checkContent();
                    }
                }
            }
        });
        content1CodiceFiscale.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    if (!TextUtils.isEmpty(content1CodiceFiscale.getText().toString().trim()) && !Utils.checkCodiceFiscale(content1CodiceFiscale.getText().toString().trim())) {
                        Utils.showWarningMessage(getActivity(), "", "Codice Fiscale non valido");
                    } else {
                        checkContent();
                    }
                }
            }
        });

        content2Nome.setOnFocusChangeListener(myFocusChangeListener);
        content2Cognome.setOnFocusChangeListener(myFocusChangeListener);
        content2Telefono.setOnFocusChangeListener(myFocusChangeListener);
        content2Email.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    if (!TextUtils.isEmpty(content2Email.getText().toString().trim()) && !Utils.isEmailValid(content2Email.getText().toString().trim())) {
                        Utils.showWarningMessage(getActivity(), "", "Email non valida");
                    } else {
                        checkContent();
                    }
                }
            }
        });
        content2CodiceFiscale.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    if (!TextUtils.isEmpty(content2CodiceFiscale.getText().toString().trim()) && !Utils.checkCodiceFiscale(content2CodiceFiscale.getText().toString().trim())) {
                        Utils.showWarningMessage(getActivity(), "", "Codice Fiscale non valido");
                    } else {
                        checkContent();
                    }
                }
            }
        });
        content2Sesso.setOnItemSelectedListener(myItemSelectedListener);
        content2LuogoDiNascita.setOnFocusChangeListener(myFocusChangeListener);
        content2Provincia.setOnFocusChangeListener(myFocusChangeListener);
        content2Nazionalita.setOnFocusChangeListener(myFocusChangeListener);
        content2DocumentoIdentita.setOnItemSelectedListener(myItemSelectedListener);
        content2NumeroDocumentoIdentita.setOnFocusChangeListener(myFocusChangeListener);
        content2RilasciatoDa.setOnFocusChangeListener(myFocusChangeListener);

        content3RiferimentoSede.setOnFocusChangeListener(myFocusChangeListener);
        content3Indirizzo.setOnFocusChangeListener(myFocusChangeListener);
        content3NumeroCivico.setOnFocusChangeListener(myFocusChangeListener);
        content3Citta.setOnFocusChangeListener(myFocusChangeListener);
        content3Provincia.setOnFocusChangeListener(myFocusChangeListener);
        content3Cap.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    if (!TextUtils.isEmpty(content3Cap.getText().toString().trim()) && content3Cap.getText().toString().trim().length() != 5) {
                        Utils.showWarningMessage(getActivity(), "", "CAP non valido");
                    } else {
                        checkContent();
                    }
                }
            }
        });

        content4Nome.setOnFocusChangeListener(myFocusChangeListener);
        content4Cognome.setOnFocusChangeListener(myFocusChangeListener);
        content4Email.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    if (!TextUtils.isEmpty(content4Email.getText().toString().trim()) && !Utils.isEmailValid(content4Email.getText().toString().trim())) {
                        Utils.showWarningMessage(getActivity(), "", "Email non valida");
                    } else {
                        checkContent();
                    }
                }
            }
        });
        content4Telefono.setOnFocusChangeListener(myFocusChangeListener);
        content4Cellulare.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    if (!TextUtils.isEmpty(content4Cellulare.getText().toString().trim()) && content4Cellulare.getText().toString().trim().length()<9) {
                        Utils.showWarningMessage(getActivity(), "", "Il numero di cellulare deve essere composto da almeno 9 cifre");
                    } else {
                        checkContent();
                    }
                }
            }
        });

        content4Cellulare.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {

                    if (!TextUtils.isEmpty(content4Cellulare.getText().toString().trim()) && content4Cellulare.getText().toString().trim().length() < 9) {
                        Utils.showWarningMessage(getActivity(), "", "Il numero di cellulare deve essere composto da almeno 9 cifre");
                    } else {
                        checkContent();
//                        return true
                    }


                }
                return false;
            }
        });


        content4RichiestaDettaglioChiamate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkContent();
            }
        });

        content6AreaClienti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                content6AreaClienti.setChecked(true);
                content6SupportoCartaceo.setChecked(false);
                content6SupportoCd.setChecked(false);
                checkContent();
            }
        });
        content6SupportoCartaceo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                content6AreaClienti.setChecked(false);
                content6SupportoCartaceo.setChecked(true);
                content6SupportoCd.setChecked(false);
                checkContent();
            }
        });
        content6SupportoCd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                content6AreaClienti.setChecked(false);
                content6SupportoCartaceo.setChecked(false);
                content6SupportoCd.setChecked(true);
                checkContent();
            }
        });

        content7modalitaFatturazioneElettronica.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkContent();
            }
        });
        content7CodiceIdentificativoUfficio.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    if (!TextUtils.isEmpty(content7CodiceIdentificativoUfficio.getText().toString().trim()) && content7CodiceIdentificativoUfficio.getText().toString().trim().length() != 6) {
                        Utils.showWarningMessage(getActivity(), "", "Il Codice identificativo Ufficio deve essere composto da esattamente 6 caratteri");
                    } else {
                        checkContent();
                    }
                }
            }
        });


        content7CodiceIdentificativoUfficio.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {

                    if (!TextUtils.isEmpty(content7CodiceIdentificativoUfficio.getText().toString().trim()) && content7CodiceIdentificativoUfficio.getText().toString().trim().length() != 6) {
                        Utils.showWarningMessage(getActivity(), "", "Il Codice identificativo Ufficio deve essere composto da esattamente 6 caratteri");
                    } else {
                        checkContent();
//                        return true
                    }


                }
                return false;
            }
        });


        content7SplitPayment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkContent();
            }
        });
    }

    private void updateJson() {
        JSONObject inserimentoDati = new JSONObject();
        try {
            JSONObject datiCliente = new JSONObject();
            datiCliente.put("indirizzoSedeLegale", content1IndirizzoSedeLegale.getText().toString().trim());
            datiCliente.put("civico", content1NumeroCivico.getText().toString().trim());
            datiCliente.put("p_iva", content1PartitaIva.getText().toString().trim());
            datiCliente.put("provinciaCCIAA", content1ProvinciaIscrizione.getText().toString().trim());
            datiCliente.put("cognome", content1Cognome.getText().toString().trim());
            datiCliente.put("cf", content1CodiceFiscale.getText().toString().trim());
            datiCliente.put("provincia", content1Provincia.getText().toString().trim());
            datiCliente.put("ragioneSociale", content1RagioneSociale.getText().toString().trim());
            datiCliente.put("nome", content1Nome.getText().toString().trim());
            datiCliente.put("citta", content1Citta.getText().toString().trim());
            datiCliente.put("firmaSocietaria", content1FirmaSocietaria.getText().toString().trim());
            datiCliente.put("cap", content1CAP.getText().toString().trim());
            inserimentoDati.put("datiCliente", datiCliente);

            JSONObject datiRappresentanteLegale = new JSONObject();
            datiRappresentanteLegale.put("numDocIdentita", content2NumeroDocumentoIdentita.getText().toString().trim());
            datiRappresentanteLegale.put("docIdentita", documentoIdentitaList.get(content2DocumentoIdentita.getSelectedItemPosition()));
            datiRappresentanteLegale.put("cf", content2CodiceFiscale.getText().toString().trim());
            datiRappresentanteLegale.put("cognome", content2Cognome.getText().toString().trim());
            datiRappresentanteLegale.put("telefono", content2Telefono.getText().toString().trim());
            datiRappresentanteLegale.put("provincia", content2Provincia.getText().toString().trim());
            datiRappresentanteLegale.put("dataDoc", content2Data.getText().toString().trim());
            datiRappresentanteLegale.put("nome", content2Nome.getText().toString().trim());
            datiRappresentanteLegale.put("fax", content2Fax.getText().toString().trim());
            datiRappresentanteLegale.put("luogoNascita", content2LuogoDiNascita.getText().toString().trim());
            datiRappresentanteLegale.put("dataNascita", content2DataDiNascita.getText().toString().trim());
            datiRappresentanteLegale.put("isCopiaCliente", content2Si.isChecked() ? "1" : "0");
            datiRappresentanteLegale.put("nazionalita", content2Nazionalita.getText().toString().trim());
            datiRappresentanteLegale.put("email", content2Email.getText().toString().trim());
            datiRappresentanteLegale.put("rilasciatoDa", content2RilasciatoDa.getText().toString().trim());
            datiRappresentanteLegale.put("gender", sessoList.get(content2Sesso.getSelectedItemPosition()));
            inserimentoDati.put("datiRappresentanteLegale", datiRappresentanteLegale);

            JSONObject datiSede = new JSONObject();
            datiSede.put("indirizzo", content3Indirizzo.getText().toString().trim());
            datiSede.put("citta", content3Citta.getText().toString().trim());
            datiSede.put("riferimentoSede", content3RiferimentoSede.getText().toString().trim());
            datiSede.put("provincia", content3Provincia.getText().toString().trim());
            datiSede.put("civico", content3NumeroCivico.getText().toString().trim());
            datiSede.put("cap", content3Cap.getText().toString().trim());
            inserimentoDati.put("datiSede", datiSede);

            JSONObject referenteContratto = new JSONObject();
            referenteContratto.put("fax", content4Fax.getText().toString().trim());
            referenteContratto.put("cognome", content4Cognome.getText().toString().trim());
            referenteContratto.put("dettaglioChiamate", content4RichiestaDettaglioChiamate.isChecked() ? "1" : "0");
            referenteContratto.put("email", content4Email.getText().toString().trim());
            referenteContratto.put("nome", content4Nome.getText().toString().trim());
            referenteContratto.put("cellulare", content4Cellulare.getText().toString().trim());
            referenteContratto.put("modFatturazione", modalitaFatturazioneList.get(content4ModalitaFatturazione.getSelectedItemPosition()));
            referenteContratto.put("telefono", content4Telefono.getText().toString().trim());
            referenteContratto.put("agevolazioniFiscali", content4AgevolazioniFiscali.isChecked() ? "1" : "0");
            inserimentoDati.put("referenteContratto", referenteContratto);

            JSONObject modInvioFattura = new JSONObject();
            modInvioFattura.put("cartaceo", content5CartaceoPerPostaOrdinaria.isChecked() ? "1" : "0");
            modInvioFattura.put("elettronico", content5FormatoElettronicoViaEmail.isChecked() ? "1" : "0");
            inserimentoDati.put("modInvioFattura", modInvioFattura);

            JSONObject documentazioneCosti = new JSONObject();
            documentazioneCosti.put("areaClienti", content6AreaClienti.isChecked() ? "1" : "0");
            documentazioneCosti.put("supportoCartaceo", content6SupportoCartaceo.isChecked() ? "1" : "0");
            documentazioneCosti.put("supportoCD", content6SupportoCd.isChecked() ? "1" : "0");
            inserimentoDati.put("documentazioneCosti", documentazioneCosti);

            JSONObject pubblicaAmministrazione = new JSONObject();
            pubblicaAmministrazione.put("codiceIdentificativoUfficio", content7CodiceIdentificativoUfficio.getText().toString().trim());
            pubblicaAmministrazione.put("splitPayment", content7SplitPayment.isChecked() ? "1" : "0");
            pubblicaAmministrazione.put("modFattElettronica", content7modalitaFatturazioneElettronica.isChecked() ? "1" : "0");
            pubblicaAmministrazione.put("isSelected", content7check.isChecked() ? "1" : "0");
            inserimentoDati.put("pubblicaAmministrazione", pubblicaAmministrazione);

//            return inserimentoDati;
            Session.getInstance().getJsonPratica().getJSONObject("pratica").put("inserimentoDati", inserimentoDati);

            //update nome pratica basing on current nome+cognome

            String nomeCognome = content1Cognome.getText().toString().trim() + " " + content1Nome.getText().toString().trim();

            if (!nomeCognome.equalsIgnoreCase(" ")) {
//                PraticheDAO.updateNomePratica(this.getActivity().getBaseContext(),Session.getInstance().getPratica().getId(),nomeCognome);
//
//                PraticheDAO.setNomeNotEditable(this.getActivity().getBaseContext(), Session.getInstance().getPratica().getId());

                Session.getInstance().getPratica().setNome_pratica(nomeCognome);
                Session.getInstance().getPratica().setIsNomeEditable(false);
            }

            if (!content1PartitaIva.getText().toString().trim().equalsIgnoreCase("")) {
                Session.getInstance().getPratica().setP_iva(content1PartitaIva.getText().toString().trim());
            }

            //store data on sqlLite
            PraticheDAO.updatePratica(getActivity());
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void populatePage() {
        try {
            JSONObject inserimentoDati = Session.getInstance().getJsonPratica().getJSONObject("pratica").optJSONObject("inserimentoDati");

            if (inserimentoDati != null) {

                JSONObject datiCliente = inserimentoDati.getJSONObject("datiCliente");
                content1IndirizzoSedeLegale.setText(datiCliente.optString("indirizzoSedeLegale"));
                content1NumeroCivico.setText(datiCliente.optString("civico"));
                content1PartitaIva.setText(datiCliente.optString("p_iva"));
                content1ProvinciaIscrizione.setText(datiCliente.optString("provinciaCCIAA"));
                content1Cognome.setText(datiCliente.optString("cognome"));
                content1CodiceFiscale.setText(datiCliente.optString("cf"));
                content1Provincia.setText(datiCliente.optString("provincia"));
                content1RagioneSociale.setText(datiCliente.optString("ragioneSociale"));
                content1Nome.setText(datiCliente.optString("nome"));
                content1Citta.setText(datiCliente.optString("citta"));
                content1FirmaSocietaria.setText(datiCliente.optString("firmaSocietaria"));
                content1CAP.setText(datiCliente.optString("cap"));

                JSONObject datiRappresentanteLegale = inserimentoDati.getJSONObject("datiRappresentanteLegale");
                content2NumeroDocumentoIdentita.setText(datiRappresentanteLegale.optString("numDocIdentita"));
                for (int i = 0; i < documentoIdentitaList.size(); i++) {
                    if (documentoIdentitaList.get(i).compareToIgnoreCase(datiRappresentanteLegale.optString("docIdentita")) == 0) {
                        content2DocumentoIdentita.setSelection(i);
                    }
                }
                content2CodiceFiscale.setText(datiRappresentanteLegale.optString("cf"));
                content2Cognome.setText(datiRappresentanteLegale.optString("cognome"));
                content2Telefono.setText(datiRappresentanteLegale.optString("telefono"));
                content2Provincia.setText(datiRappresentanteLegale.optString("provincia"));
                content2Data.setText(datiRappresentanteLegale.optString("dataDoc"));
                content2Nome.setText(datiRappresentanteLegale.optString("nome"));
                content2Fax.setText(datiRappresentanteLegale.optString("fax"));
                content2LuogoDiNascita.setText(datiRappresentanteLegale.optString("luogoNascita"));
                content2DataDiNascita.setText(datiRappresentanteLegale.optString("dataNascita"));
                content2Si.setChecked(datiRappresentanteLegale.optString("isCopiaCliente").compareTo("1") == 0 ? true : false);

                if (content2Si.isChecked()) {
                    content2Nome.setEnabled(false);
                    content2Cognome.setEnabled(false);
                    content2CodiceFiscale.setEnabled(false);

                    content2Nome.setBackground(getActivity().getResources().getDrawable(R.drawable.gray_shape_edittext));
                    content2Cognome.setBackground(getActivity().getResources().getDrawable(R.drawable.gray_shape_edittext));
                    content2CodiceFiscale.setBackground(getActivity().getResources().getDrawable(R.drawable.gray_shape_edittext));
                }

                content2No.setChecked((!content2Si.isChecked() && !content2Si.isChecked()) ? true : false);
                content2Nazionalita.setText(datiRappresentanteLegale.optString("nazionalita"));
                content2Email.setText(datiRappresentanteLegale.optString("email"));
                content2RilasciatoDa.setText(datiRappresentanteLegale.optString("rilasciatoDa"));
                for (int i = 0; i < sessoList.size(); i++) {
                    if (sessoList.get(i).compareToIgnoreCase(datiRappresentanteLegale.optString("gender")) == 0) {
                        content2Sesso.setSelection(i);
                    }
                }

                JSONObject datiSede = inserimentoDati.getJSONObject("datiSede");
                content3Indirizzo.setText(datiSede.optString("indirizzo"));
                content3Citta.setText(datiSede.optString("citta"));
                content3RiferimentoSede.setText(datiSede.optString("riferimentoSede"));
                content3Provincia.setText(datiSede.optString("provincia"));
                content3NumeroCivico.setText(datiSede.optString("civico"));
                content3Cap.setText(datiSede.optString("cap"));

                JSONObject referenteContratto = inserimentoDati.getJSONObject("referenteContratto");
                content4Fax.setText(referenteContratto.optString("fax"));
                content4Cognome.setText(referenteContratto.optString("cognome"));
                content4RichiestaDettaglioChiamate.setChecked(referenteContratto.optString("dettaglioChiamate").compareTo("1") == 0 ? true : false);
                content4Email.setText(referenteContratto.optString("email"));
                content4Nome.setText(referenteContratto.optString("nome"));
                content4Cellulare.setText(referenteContratto.optString("cellulare"));
                for (int i = 0; i < modalitaFatturazioneList.size(); i++) {
                    if (modalitaFatturazioneList.get(i).compareToIgnoreCase(referenteContratto.optString("modFatturazione")) == 0) {
                        content4ModalitaFatturazione.setSelection(i);
                    }
                }
                content4Telefono.setText(referenteContratto.optString("telefono"));
                content4AgevolazioniFiscali.setChecked(referenteContratto.optString("agevolazioniFiscali").compareTo("1") == 0 ? true : false);

                JSONObject modInvioFattura = inserimentoDati.getJSONObject("modInvioFattura");
                content5CartaceoPerPostaOrdinaria.setChecked(modInvioFattura.optString("cartaceo").compareTo("1") == 0 ? true : false);
                content5FormatoElettronicoViaEmail.setChecked(modInvioFattura.optString("elettronico").compareTo("1") == 0 ? true : false);

                JSONObject documentazioneCosti = inserimentoDati.getJSONObject("documentazioneCosti");
                content6AreaClienti.setChecked(documentazioneCosti.optString("areaClienti").compareTo("1") == 0 ? true : false);
                content6SupportoCartaceo.setChecked(documentazioneCosti.optString("supportoCartaceo").compareTo("1") == 0 ? true : false);
                content6SupportoCd.setChecked(documentazioneCosti.optString("supportoCD").compareTo("1") == 0 ? true : false);

                JSONObject pubblicaAmministrazione = inserimentoDati.getJSONObject("pubblicaAmministrazione");
                content7CodiceIdentificativoUfficio.setText(pubblicaAmministrazione.optString("codiceIdentificativoUfficio"));
                content7SplitPayment.setChecked(pubblicaAmministrazione.optString("splitPayment").compareTo("1") == 0 ? true : false);
                content7modalitaFatturazioneElettronica.setChecked(pubblicaAmministrazione.optString("modFattElettronica").compareTo("1") == 0 ? true : false);
                content7check.setChecked(pubblicaAmministrazione.optString("isSelected").compareTo("1") == 0 ? true : false);

                if (content7check.isChecked()) {
                    content7.setVisibility(View.VISIBLE);
                    arrow7.setBackground(getActivity().getResources().getDrawable(R.drawable.arrow_up_inserimento));
                }

            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
