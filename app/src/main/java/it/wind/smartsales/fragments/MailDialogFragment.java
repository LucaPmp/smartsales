package it.wind.smartsales.fragments;

import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import it.wind.smartsales.R;
import it.wind.smartsales.commons.Utils;
import it.wind.smartsales.entities.AddOnMobile;
import it.wind.smartsales.entities.Dealer;
import it.wind.smartsales.entities.OffertaMobile;
import it.wind.smartsales.entities.OpzioneMobile;
import it.wind.smartsales.entities.Session;
import it.wind.smartsales.entities.Terminale;
import it.wind.smartsales.task.SendMailTask;

/**
 * Created by luca.quaranta on 03/12/2015.
 */
public class MailDialogFragment extends DialogFragment {

    public static final String KEY = "MESSAGE_KEY";
    private static final String TAG = "MailDialogFragment";

    private Button indietroButton, continuaButton;
    private MobileInternetCreateOfferStep5Fragment mobileInternetCreateOfferStep5Fragment;
    private EditText emailText;

    private String email;

    public MailDialogFragment() {
    }

    public MailDialogFragment(MobileInternetCreateOfferStep5Fragment mobileInternetCreateOfferStep5Fragment) {
        this.mobileInternetCreateOfferStep5Fragment = mobileInternetCreateOfferStep5Fragment;
    }

    private void initializeComponents(View view) {
        indietroButton = (Button) view.findViewById(R.id.indietro_button);
        continuaButton = (Button) view.findViewById(R.id.continua_button);
        emailText = (EditText) view.findViewById(R.id.email_text);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    @Override
    public void onResume() {
        super.onResume();
        int width = getResources().getDimensionPixelSize(R.dimen.fragment_dialog_mail_width);
        getDialog().getWindow().setLayout(width, ViewGroup.LayoutParams.WRAP_CONTENT);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_dialog_mail, container, false);

        initializeComponents(view);

        indietroButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        continuaButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                email = emailText.getText().toString().trim();
                if (!Utils.isEmailValid(email)) {
                    Utils.showWarningMessage(getActivity(), getString(R.string.login_warning_title), getString(R.string.login_warning_username_incorrect));
                } else {
                    SendMailTask.callTask(getActivity().getBaseContext(), mobileInternetCreateOfferStep5Fragment, getMailContent());

                    dismiss();
                }
            }
        });

        return view;
    }

    private String getMailContent() {

        String result = "";
        String mailtemplate = getXMLTemplate(R.raw.mailtemplate);

        Dealer dealer = Session.getInstance().getDealer();
        String dealerName = dealer.getDealerName()+" "+dealer.getDealerSurname();
        mailtemplate = mailtemplate.replace("<to>", "<to>" + email);


        mailtemplate = mailtemplate.replace("<username>", "<username>" + Session.getInstance().getDealer().getUsername());

        mailtemplate = mailtemplate.replace("<cc>", "<cc>" + Session.getInstance().getDealer().getDealerMail());
        mailtemplate = mailtemplate.replace("<bcc>", "<bcc>" + Session.getInstance().getDealer().getEmailBackOffice());

        mailtemplate = mailtemplate.replace("<pwd>", "<pwd>" + Session.getInstance().getDealer().getPassword());
        mailtemplate = mailtemplate.replace("<dealer>", "<dealer>" + dealerName);
        mailtemplate = mailtemplate.replace("<dealerRole>", "<dealerRole>" + Session.getInstance().getDealer().getDealerRole());
        mailtemplate = mailtemplate.replace("<dealerPhone>", "<dealerPhone>" + Session.getInstance().getDealer().getDealerPhone());
        mailtemplate = mailtemplate.replace("<dealerCountry>", "<dealerCountry>" + Session.getInstance().getDealer().getDealerCountry());
        mailtemplate = mailtemplate.replace("<dealerCity>", "<dealerCity>" + Session.getInstance().getDealer().getDealerCity());
        mailtemplate = mailtemplate.replace("<dealerAgency>", "<dealerAgency>" + Session.getInstance().getDealer().getDealerAgency());
        mailtemplate = mailtemplate.replace("<mailType>", "<mailType>" + "riepilogo");

        mailtemplate = mailtemplate.replace("<noStandard>", "<noStandard>" + Session.getInstance().getJsonPratica().optBoolean("isFuoriStandard"));
//        xml.replace("<dealer>", dealer.);

        String totOfferte = "";

        for (OffertaMobile offertaMobile : getOfferteMobile()) {

            String offerTemplate = getXMLTemplate(R.raw.offertemplate);
            offerTemplate = offerTemplate.replace("<offerId>", "<offerId>" + String.valueOf(offertaMobile.getIdOfferta()));
            offerTemplate = offerTemplate.replace("<offerName>", "<offerName>" + String.valueOf(offertaMobile.getOfferDescription() + " " + offertaMobile.getOfferDescriptionUpper()));
            offerTemplate = offerTemplate.replace("<quantity>", "<quantity>" + String.valueOf(offertaMobile.getSelectedNumSimStep3()));
            offerTemplate = offerTemplate.replace("<price>", "<price>" + offertaMobile.getCanoneMensile().replace(".",","));


            String totTerminali = "";
            for (Terminale terminale : offertaMobile.getTerminaleArrayList()) {
                String deviceTemplate = getXMLTemplate(R.raw.devicetemplate);
                deviceTemplate = deviceTemplate.replace("<deviceName>", "<deviceName>" + terminale.getTerminaleBrand() + " " + terminale.getTerminaleDescription());
                deviceTemplate = deviceTemplate.replace("<quantity>", "<quantity>" + String.valueOf(terminale.getSelectedNumSimStep3()));
                deviceTemplate = deviceTemplate.replace("<price>", "<price>" + terminale.getRataMensile().replace(".",","));
                totTerminali += deviceTemplate;
            }

            offerTemplate = offerTemplate.replace("<deviceplaceholder>", totTerminali);

            String totItem = "";

            for (AddOnMobile addOnMobile : offertaMobile.getAddOnMobileArrayList()) {
                String itemTemplate = getXMLTemplate(R.raw.itemtemplate);
                itemTemplate = itemTemplate.replace("<name>", "<name>" + addOnMobile.getDescFascia());
                itemTemplate = itemTemplate.replace("<quantity>", "<quantity>" + addOnMobile.getSelectedNumSimStep3());
                itemTemplate = itemTemplate.replace("<price>", "<price>" + format(addOnMobile.getPrezzoFascia()));
                totItem += itemTemplate;
            }

            Collections.sort(offertaMobile.getOpzioneMobileArrayList(), new Comparator<OpzioneMobile>() {
                @Override
                public int compare(OpzioneMobile opz1, OpzioneMobile opz2) {
                    return opz1.getScontoConvergenza().compareToIgnoreCase(opz2.getScontoConvergenza());
                }
            });

            for (OpzioneMobile opzioneMobile : offertaMobile.getOpzioneMobileArrayList()) {
                String itemTemplate = getXMLTemplate(R.raw.itemtemplate);
                itemTemplate = itemTemplate.replace("<name>", "<name>" + opzioneMobile.getOpzioneDesc() + " " + opzioneMobile.getOpzioneDescUpper());
                itemTemplate = itemTemplate.replace("<quantity>", "<quantity>" + opzioneMobile.getSelectedNumSimStep3());
                itemTemplate = itemTemplate.replace("<price>", "<price>" + (opzioneMobile.getScontoConvergenza().isEmpty() == true ? format(opzioneMobile.getCanoneMensile()) : "-" + format(opzioneMobile.getScontoConvergenza())));
                totItem += itemTemplate;
            }

            offerTemplate = offerTemplate.replace("<itemplaceholder>", totItem);

            totOfferte += offerTemplate;

        }

        mailtemplate = mailtemplate.replace("<offerplaceholder>", totOfferte);

        if (mobileInternetCreateOfferStep5Fragment.getScontoApplicato()!=0f) {
            mailtemplate = mailtemplate.replace("<appliedDiscount>", "<appliedDiscount>-" + format(mobileInternetCreateOfferStep5Fragment.getScontoApplicato()));
        }
        mailtemplate = mailtemplate.replace("<monthlyFeeAmount>", "<monthlyFeeAmount>" + format(mobileInternetCreateOfferStep5Fragment.getTotCanoneMensile()));
        mailtemplate = mailtemplate.replace("<oneOffCharge>", "<oneOffCharge>" + format(mobileInternetCreateOfferStep5Fragment.getContributoUnaTan()));

        Log.i(TAG, mailtemplate);
        return mailtemplate;
    }

    private String format(float input) {
        String output;
        output = String.format("%.2f", input).replace(".", ",");
        return output;
    }

    private String format(String input) {
        String output;
        output = String.format("%.2f", Float.parseFloat(input)).replace(".", ",");
        return output;
    }

    private String getXMLTemplate(int resId) {
        InputStream inputStream = getActivity().getResources().openRawResource(resId);

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

        int i;
        try {
            i = inputStream.read();
            while (i != -1) {
                byteArrayOutputStream.write(i);
                i = inputStream.read();
            }
            inputStream.close();
        } catch (IOException e) {
            return null;
        }
        String xml = byteArrayOutputStream.toString();
        return xml;
    }

    private ArrayList<OffertaMobile> getOfferteMobile() {
        JSONArray offerteRaggruppate = null;
        ArrayList<OffertaMobile> offertaMobiles = null;
        try {
            offerteRaggruppate = (JSONArray) Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONArray("offerteRaggruppate");
            offertaMobiles = new ArrayList<>();
            for (int i = 0; i < offerteRaggruppate.length(); i++) {
                offertaMobiles.add(Utils.createOffertaMobile(offerteRaggruppate.getJSONObject(i)));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }


        return offertaMobiles;
    }

}
