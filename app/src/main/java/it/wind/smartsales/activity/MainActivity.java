package it.wind.smartsales.activity;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.nineoldandroids.animation.Animator;

import it.wind.smartsales.R;
import it.wind.smartsales.commons.Constants;
import it.wind.smartsales.commons.Utils;
import it.wind.smartsales.dao.DbHelper;
import it.wind.smartsales.dao.PraticheDAO;
import it.wind.smartsales.entities.Session;
import it.wind.smartsales.fragments.DraftListFragment;
import it.wind.smartsales.fragments.GoHomeDialogFragment;
import it.wind.smartsales.fragments.HomeAdminFragment;
import it.wind.smartsales.fragments.HomeUserFragment;
import it.wind.smartsales.fragments.MobileInternetCreateOffer;
import it.wind.smartsales.fragments.TerminalsFragment;
import it.wind.smartsales.fragments.selectoffer.MobileInternetSelectOffer;
import it.wind.smartsales.fragments.wireline.WirelineInternetOfferFragment;

/**
 * Created by luca.quaranta on 09/02/2016.
 */
public class MainActivity extends Activity {
    private MobileInternetCreateOffer mobileInternetCreateOfferFragment;
    private MobileInternetSelectOffer mobileInternetSelectOfferFragment;

    private Button draftButton, logoutButton, goHomeButton;
    private RelativeLayout draftNumberLayout;
    private TextView draftNumber;
    private int numero = 0;

    private LinearLayout navHeader;
    private RelativeLayout navIcon1, navIcon2, navIcon3, navIcon4, navIcon5, navIcon6;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initializeComponents();

        Session.getInstance().setActivity(this);

        if (Session.getInstance().getDealer().isCreationOfferEnabled()) {
            openFragment(new HomeAdminFragment());
        } else {
            openFragment(new HomeUserFragment());
        }

        setNavIcon(0);

        draftButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                goToPage(Constants.Pages.PAGE_DRAFT);
            }
        });

        draftNumberLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToPage(Constants.Pages.PAGE_DRAFT);
            }
        });

        if (numero != 0) {
            draftNumberLayout.setVisibility(View.VISIBLE);
        }

        if (PraticheDAO.getCountBozze(getBaseContext()) != 0) {
            increaseDraftNumber();
        }


        logoutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        goHomeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences sharedPref = getSharedPreferences("SmartSalesSharedPref", Context.MODE_PRIVATE);
                if (!sharedPref.getBoolean(Session.getInstance().getDealer().getDealerCode(), false)) {
                    new GoHomeDialogFragment().show(getFragmentManager(), "info_fragment_dialog");
                    ;
                } else {
                    goToPage(Constants.Pages.PAGE_HOME_ADMIN);
                }
            }
        });

        logoutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    private void initializeComponents() {
        draftButton = (Button) findViewById(R.id.draft_button);

        logoutButton = (Button) findViewById(R.id.logout_button);
        goHomeButton = (Button) findViewById(R.id.go_home_button);
        goHomeButton.setVisibility(View.GONE);

        draftNumberLayout = (RelativeLayout) findViewById(R.id.draft_number_layout);
        draftNumber = (TextView) findViewById(R.id.draft_number);

        navHeader = (LinearLayout) findViewById(R.id.nav_header);
        navIcon1 = (RelativeLayout) findViewById(R.id.nav_icon_1);
        navIcon2 = (RelativeLayout) findViewById(R.id.nav_icon_2);
        navIcon3 = (RelativeLayout) findViewById(R.id.nav_icon_3);
        navIcon4 = (RelativeLayout) findViewById(R.id.nav_icon_4);
        navIcon5 = (RelativeLayout) findViewById(R.id.nav_icon_5);
        navIcon6 = (RelativeLayout) findViewById(R.id.nav_icon_6);

        mobileInternetCreateOfferFragment = new MobileInternetCreateOffer();
        mobileInternetSelectOfferFragment = new MobileInternetSelectOffer();
    }

    public void increaseDraftNumber() {
        draftNumber.setText(String.valueOf(PraticheDAO.getCountBozze(getBaseContext())));
        YoYo.with(Techniques.RollIn).duration(300).withListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
                draftNumberLayout.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animator animation) {

            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        }).playOn(draftNumberLayout);
    }

    public void goToPage(int page) {
        switch (page) {
            case Constants.Pages.PAGE_HOME_ADMIN:
                goToHomeAdminFragment();
                goHomeButton.setVisibility(View.GONE);
                setNavIcon(0);
                break;

            case Constants.Pages.PAGE_MOBILE_INTERNET_CREATE_OFFER:
                goHomeButton.setVisibility(View.VISIBLE);
                openFragment(mobileInternetCreateOfferFragment);
                setNavIcon(1);
                DbHelper.getInstance(this).extractDB(this);
                break;
            case Constants.Pages.PAGE_MOBILE_INTERNET_SELECT_OFFER:
                goHomeButton.setVisibility(View.VISIBLE);
                openFragment(mobileInternetSelectOfferFragment);
                setNavIcon(1);
                DbHelper.getInstance(this).extractDB(this);
                break;
            case Constants.Pages.PAGE_MOBILE_TERMINALS_SELECT:
                goHomeButton.setVisibility(View.VISIBLE);
                openFragment(new TerminalsFragment());
                setNavIcon(0);
//                DbHelper.getInstance(this).extractDB(this);
                break;
            case Constants.Pages.PAGE_WIRELINE_INTERNET_CREATE_OFFER:
                goHomeButton.setVisibility(View.VISIBLE);
                openFragment(new WirelineInternetOfferFragment());
                setNavIcon(1);
                break;
            case Constants.Pages.PAGE_DRAFT:
                setNavIcon(0);
                goHomeButton.setVisibility(View.GONE);
                if (PraticheDAO.getCountBozze(getBaseContext()) != 0) {
                    getFragmentManager().beginTransaction().replace(R.id.fragment_container, new DraftListFragment()).commit();
                } else {
                    Utils.showWarningMessage(this, "", "Nessuna pratica salvata.");
                }
                break;
        }
    }

    public void setNavIcon(int navIcon) {
        switch (navIcon) {
            case 0:
                navHeader.setVisibility(View.GONE);
                navIcon1.setSelected(false);
                navIcon2.setSelected(false);
                navIcon3.setSelected(false);
                navIcon4.setSelected(false);
                navIcon5.setSelected(false);
                navIcon6.setSelected(false);
                break;

            case 1:
                navHeader.setVisibility(View.VISIBLE);
                navIcon1.setSelected(true);
                navIcon2.setSelected(false);
                navIcon3.setSelected(false);
                navIcon4.setSelected(false);
                navIcon5.setSelected(false);
                navIcon6.setSelected(false);
                break;

            case 2:
                navHeader.setVisibility(View.VISIBLE);
                navIcon1.setSelected(true);
                navIcon2.setSelected(true);
                navIcon3.setSelected(false);
                navIcon4.setSelected(false);
                navIcon5.setSelected(false);
                navIcon6.setSelected(false);
                break;

            case 3:
                navHeader.setVisibility(View.VISIBLE);
                navIcon1.setSelected(true);
                navIcon2.setSelected(true);
                navIcon3.setSelected(true);
                navIcon4.setSelected(false);
                navIcon5.setSelected(false);
                navIcon6.setSelected(false);
                break;

            case 4:
                navHeader.setVisibility(View.VISIBLE);
                navIcon1.setSelected(true);
                navIcon2.setSelected(true);
                navIcon3.setSelected(true);
                navIcon4.setSelected(true);
                navIcon5.setSelected(false);
                navIcon6.setSelected(false);
                break;

            case 5:
                navHeader.setVisibility(View.VISIBLE);
                navIcon1.setSelected(true);
                navIcon2.setSelected(true);
                navIcon3.setSelected(true);
                navIcon4.setSelected(true);
                navIcon5.setSelected(true);
                navIcon6.setSelected(false);
                break;

            case 6:
                navHeader.setVisibility(View.VISIBLE);
                navIcon1.setSelected(true);
                navIcon2.setSelected(true);
                navIcon3.setSelected(true);
                navIcon4.setSelected(true);
                navIcon5.setSelected(true);
                navIcon6.setSelected(true);
                break;
        }
    }

    private void goToHomeAdminFragment() {
        if (Session.getInstance().getDealer().getDealerRole().compareTo("UserSales") != 0)
            getFragmentManager().beginTransaction().replace(R.id.fragment_container, new HomeAdminFragment()).commit();
        else
            getFragmentManager().beginTransaction().replace(R.id.fragment_container, new HomeUserFragment()).commit();

    }

    private void openFragment(Fragment fragment) {
        getFragmentManager().beginTransaction().replace(R.id.fragment_container, fragment)
                .addToBackStack(fragment.getClass().getName()).commit();
    }

    public void goForward() {
        mobileInternetCreateOfferFragment.goForward();
    }

    public void goForwardSelectOffer() {
        mobileInternetSelectOfferFragment.goForward();
    }

    public void updateNote() {
        Fragment frag = getFragmentManager().findFragmentById(R.id.fragment_container_mobile_internet_create_offer);

        if (frag.getClass().getName().contains("CreateOffer")) {
            mobileInternetCreateOfferFragment.updateNote();
        } else if (frag.getClass().getName().contains("SelectOffer")) {
            mobileInternetSelectOfferFragment.updateNote();
        }
    }

    @Override
    public void onBackPressed() {
        if (mobileInternetCreateOfferFragment != null && mobileInternetCreateOfferFragment.isVisible()) {
            mobileInternetCreateOfferFragment.goBack();
        } else {
            super.onBackPressed();
        }
    }
}