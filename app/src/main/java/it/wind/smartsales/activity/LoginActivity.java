package it.wind.smartsales.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;
import android.text.TextUtils;
import android.text.method.PasswordTransformationMethod;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;

import java.util.HashMap;
import java.util.Iterator;

import it.wind.smartsales.R;
import it.wind.smartsales.commons.Constants;
import it.wind.smartsales.commons.Log;
import it.wind.smartsales.commons.Utils;
import it.wind.smartsales.dao.CataloghiCheckVersionDAO;
import it.wind.smartsales.entities.InfoCatalog;
import it.wind.smartsales.entities.Session;
import it.wind.smartsales.task.CataloghiCheckVersionTask;
import it.wind.smartsales.task.CatalogoOfferteFissoTask;
import it.wind.smartsales.task.CatalogoOfferteMobileTask;
import it.wind.smartsales.task.CatalogoQuestionarioTask;
import it.wind.smartsales.task.CatalogoScontiRicaricabiliTask;
import it.wind.smartsales.task.CatalogoScontiTask;
import it.wind.smartsales.task.CatalogoTerminaliTask;
import it.wind.smartsales.task.CheckAppVersionTask;
import it.wind.smartsales.task.LoginTask;
import it.wind.smartsales.task.requests.CataloghiCheckVersionRequest;
import it.wind.smartsales.task.requests.CheckAppVersionRequest;
import it.wind.smartsales.task.requests.DownloadCatalogoRequest;
import it.wind.smartsales.task.requests.LoginRequest;
import it.wind.smartsales.task.requests.Request;

/**
 * Created by luca.quaranta on 09/02/2016.
 */
public class LoginActivity extends Activity implements Handler.Callback {

    private static final String ENVIRONMENT_PASSWORD = "asdfghjkl";

    private SharedPreferences prefs;
    private EditText usernameEditText, passwordEditText;
    private Button loginButton;
    private String appVer;
    private ImageView imageView;
    private CheckBox rememberMeCheck;
    private TextView forgotCredentials, appVerText, headerText;
    private RelativeLayout progress;
    private ProgressDialog progressDialog;
    private boolean isMainStarted = false;

    private void initializeComponents() {
        prefs = getSharedPreferences("SmartSalesSharedPref", Context.MODE_PRIVATE);
        usernameEditText = (EditText) findViewById(R.id.username);
        passwordEditText = (EditText) findViewById(R.id.password);


        loginButton = (Button) findViewById(R.id.login_button);
        imageView = (ImageView) findViewById(R.id.login_logo);
        rememberMeCheck = (CheckBox) findViewById(R.id.remember_check);
        forgotCredentials = (TextView) findViewById(R.id.forgot_credentials);
        appVerText = (TextView) findViewById(R.id.app_ver);
        progress = (RelativeLayout) findViewById(R.id.progress);
        headerText = (TextView) findViewById(R.id.header_text);

        passwordEditText.setTypeface(Typeface.DEFAULT);
        passwordEditText.setTransformationMethod(new PasswordTransformationMethod());
        progress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });

        loginButton.setTypeface(Typeface.createFromAsset(getAssets(), "font/Trade Gothic LT Light.ttf"));
        usernameEditText.setTypeface(Typeface.createFromAsset(getAssets(), "font/Trade Gothic LT Light.ttf"));
        passwordEditText.setTypeface(Typeface.createFromAsset(getAssets(), "font/Trade Gothic LT Light.ttf"));
//        rememberMeCheck.setTypeface(Typeface.createFromAsset(getAssets(), "font/Trade Gothic LT Light.ttf"));
//        forgotCredentials.setTypeface(Typeface.createFromAsset(getAssets(), "font/Trade Gothic LT Light.ttf"));
        headerText.setTypeface(Typeface.createFromAsset(getAssets(), "font/Trade Gothic LT Light.ttf"));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initializeComponents();
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        usernameEditText.setText(prefs.getBoolean(Constants.IS_REMEMBERME_CHECKED, false) ? prefs.getString(Constants.LOGIN_USERNAME, "") : "");
        passwordEditText.setText(prefs.getBoolean(Constants.IS_REMEMBERME_CHECKED, false) ? prefs.getString(Constants.LOGIN_PASSWORD, "") : "");
        rememberMeCheck.setChecked(prefs.getBoolean(Constants.IS_REMEMBERME_CHECKED, false));


        if (getResources().getBoolean(R.bool.check_misure)) {
            usernameEditText.setText("santo.piazza@email.it");
            passwordEditText.setText("$Santo.Piazza01");
            rememberMeCheck.setChecked(true);
        }

        try {
            appVer = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        usernameEditText.setNextFocusUpId(R.id.password);

        passwordEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    View view = getCurrentFocus();
                    if (view != null) {
                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                    }
//                    performLogin();
                    return true;
                }
                return false;
            }
        });

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                performLogin();
            }
        });

        forgotCredentials.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = Constants.URLS.URL_PWD_RECOVERY;  //v.getContext().getString(R.string.forgot_credentials_url);
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
            }
        });

        setAppVerLabel();
        appVerText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPasswordDialog();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        isMainStarted = false;
    }

    private void performLogin() {
        String username = usernameEditText.getText().toString().trim();
        String password = passwordEditText.getText().toString().trim();

        if (TextUtils.isEmpty(username) || TextUtils.isEmpty(password)) {
            Utils.showWarningMessage(this, getString(R.string.login_warning_title), getString(R.string.login_warning_empty_fields));
        } else if (!Utils.isEmailValid(username)) {
            Utils.showWarningMessage(this, getString(R.string.login_warning_title), getString(R.string.login_warning_username_incorrect));
        } else {
            if (Utils.checkConn(this)) {
                callLoginTask(username, password);
            } else {
                Utils.showWarningMessage(this, "", "Assenza di rete");
            }
        }
    }


    private void callLoginTask(String username, String password) {
        progress.setVisibility(View.VISIBLE);

        try {
            LoginRequest loginRequest = new LoginRequest();
            loginRequest.setUsername(username);
            loginRequest.setPassword(password);
            loginRequest.setTabletId(Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID));
//            loginRequest.setTabletId("567890");
            loginRequest.setOS("android");
            LoginTask.callTask(this, this, new Request(loginRequest));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void callCheckAppVersionTask() {
        try {
            CheckAppVersionRequest checkAppVersionRequest = new CheckAppVersionRequest();
            checkAppVersionRequest.setOS("Android");
            checkAppVersionRequest.setAppVersion(appVer);
            checkAppVersionRequest.setOSVersion(Build.VERSION.RELEASE);
            checkAppVersionRequest.setToken(Session.getInstance().getToken());
            CheckAppVersionTask.callTask(this, this, new Request(checkAppVersionRequest));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void callCataloghiCheckVersionTask() {
        try {
            CataloghiCheckVersionRequest cataloghiCheckVersionRequest = new CataloghiCheckVersionRequest();
            cataloghiCheckVersionRequest.setToken(Session.getInstance().getToken());
            CataloghiCheckVersionTask.callTask(this, this, new Request(cataloghiCheckVersionRequest));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void callCatalogoTask(InfoCatalog infoCatalog) {
        progress.setVisibility(View.GONE);
        if (progressDialog == null || !progressDialog.isShowing()) {
            progressDialog = ProgressDialog.show(this, "Easy Sales", "Aggiornamento catalogo in corso...", true);
        }

        try {
            DownloadCatalogoRequest downloadCatalogoRequest = new DownloadCatalogoRequest();
            downloadCatalogoRequest.setCatalogIdentifier(String.valueOf(infoCatalog.getCatalogIdentifier()));
            downloadCatalogoRequest.setCatalogVersion(infoCatalog.getCatalogVersion());
            downloadCatalogoRequest.setCatalogLastUpdate(infoCatalog.getCatalogLastUpdate());
            downloadCatalogoRequest.setToken(Session.getInstance().getToken());

            switch (infoCatalog.getCatalogIdentifier()) {
                case Constants.CatalogIdentifiers.CATALOGO_OFFERTE_FISSO:
                    CatalogoOfferteFissoTask.callTask(this, this, new Request(downloadCatalogoRequest));
                    break;
                case Constants.CatalogIdentifiers.CATALOGO_OFFERTE_MOBILE:
                    CatalogoOfferteMobileTask.callTask(this, this, new Request(downloadCatalogoRequest));
                    break;
                case Constants.CatalogIdentifiers.CATALOGO_SCONTI:
                    CatalogoScontiTask.callTask(this, this, new Request(downloadCatalogoRequest));
                    break;
                case Constants.CatalogIdentifiers.CATALOGO_TERMINALI:
                    CatalogoTerminaliTask.callTask(this, this, new Request(downloadCatalogoRequest));
                    break;
                case Constants.CatalogIdentifiers.CATALOGO_QUESTIONARIO:
                    CatalogoQuestionarioTask.callTask(this, this, new Request(downloadCatalogoRequest));
                    break;
                case Constants.CatalogIdentifiers.CATALOGO_6:
                    break;
                case Constants.CatalogIdentifiers.CATALOGO_SCONTI_RICARICABILI:
                    CatalogoScontiRicaricabiliTask.callTask(this, this, new Request(downloadCatalogoRequest));
                    break;
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void goToMain() {
        Intent intent = new Intent(this, MainActivity.class);

        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
        progress.setVisibility(View.GONE);

        if (!isMainStarted){
            isMainStarted = true;
            startActivity(intent);
        }
    }

    private void setAppVerLabel() {
        String appVerToShow = appVer;
        if (Constants.URLS.BASE_URL.compareToIgnoreCase(Constants.baseUrlTest) == 0) {
            appVerToShow += "_TEST";
        } else if (Constants.URLS.BASE_URL.compareToIgnoreCase(Constants.baseUrlStub) == 0) {
            appVerToShow += "_STUB";
        }
        appVerText.setText(appVerToShow);
    }

    private void showPasswordDialog() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle("Modifica Ambiente");
        alertDialog.setMessage("Inserisci Password:");

        final EditText input = new EditText(this);
        input.setInputType(129);
        input.setTypeface(Typeface.DEFAULT);
        input.setTransformationMethod(new PasswordTransformationMethod());
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        input.setLayoutParams(lp);
        alertDialog.setView(input);
        alertDialog.setIcon(R.drawable.ico_psw);

        alertDialog.setPositiveButton("Ok",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        String password = input.getText().toString();
                        if (ENVIRONMENT_PASSWORD.equals(password)) {
                            Toast.makeText(getApplicationContext(),
                                    "Password Corretta", Toast.LENGTH_SHORT).show();
                            dialog.cancel();
                            openEnvironmentsPopup();
                        } else {
                            Toast.makeText(getApplicationContext(),
                                    "Password Errata!", Toast.LENGTH_SHORT).show();
                        }
                    }
                });

        alertDialog.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

        alertDialog.show();
    }


    private void openEnvironmentsPopup() {
// Strings to Show In Dialog with Radio Buttons
        final CharSequence[] items = {" PROD ", " TEST ", " STUB "};
        final AlertDialog levelDialog;

        // Creating and Building the Dialog
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Ambienti:");
        builder.setSingleChoiceItems(items, -1, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {

                switch (item) {
                    case 0:
                        Constants.setEnvironment(Constants.Environments.PROD);
                        setAppVerLabel();
                        break;

                    case 1:
                        Constants.setEnvironment(Constants.Environments.TEST);
                        setAppVerLabel();
                        break;

                    case 2:
                        Constants.setEnvironment(Constants.Environments.STUB);
                        setAppVerLabel();
                        break;

                }
                dialog.dismiss();
            }
        });
        levelDialog = builder.create();
        levelDialog.show();
    }

    private boolean checkCatalogsUpdate() {
        Iterator it = Session.getInstance().getCatalogsUpdate().entrySet().iterator();
        while (it.hasNext()) {
            HashMap.Entry pair = (HashMap.Entry) it.next();
            if (((int) pair.getValue()) == 0) {
                it.remove();
                return false;
            }
            it.remove();
        }
        return true;
    }

    @Override
    public boolean handleMessage(Message msg) {
        switch (msg.what) {
            case Constants.HandlerWhat.WHAT_LOGIN:
                prefs.edit().putString(Constants.LOGIN_USERNAME, (String) usernameEditText.getText().toString().trim()).apply();
                prefs.edit().putString(Constants.LOGIN_PASSWORD, (String) passwordEditText.getText().toString().trim()).apply();

                Session.getInstance().getDealer().setUsername(usernameEditText.getText().toString().trim());
                Session.getInstance().getDealer().setPassword(passwordEditText.getText().toString().trim());

                if (rememberMeCheck.isChecked()) {
                    prefs.edit().putBoolean(Constants.IS_REMEMBERME_CHECKED, true).apply();
                } else {
                    prefs.edit().putBoolean(Constants.IS_REMEMBERME_CHECKED, false).apply();
                }
                if (Session.getInstance().getAppVersion().compareTo(appVer) == 0){
                    callCataloghiCheckVersionTask();
                } else {
                    Utils.showNewVersionMessage(this);
                }
                break;

            case Constants.HandlerWhat.WHAT_LOGIN_KO:
                progress.setVisibility(View.GONE);

                break;

            case Constants.HandlerWhat.WHAT_CHECK_APP_VERSION:
                prefs.edit().putString(Constants.LOGIN_USERNAME, (String) usernameEditText.getText().toString().trim()).apply();
                prefs.edit().putString(Constants.LOGIN_PASSWORD, (String) passwordEditText.getText().toString().trim()).apply();

                if (rememberMeCheck.isChecked()) {
                    prefs.edit().putBoolean(Constants.IS_REMEMBERME_CHECKED, true).apply();
                } else {
                    prefs.edit().putBoolean(Constants.IS_REMEMBERME_CHECKED, false).apply();
                }
                callCataloghiCheckVersionTask();
                break;

            case Constants.HandlerWhat.WHAT_CHECK_APP_VERSION_KO:
                Log.d("DOWNLOAD", Session.getInstance().getAppDownloadUrl());
                break;

            case Constants.HandlerWhat.WHAT_CATALOGHI_CHECK_VERSION:
                HashMap<Integer, Integer> hashMap = new HashMap<>();
                hashMap.put(Constants.CatalogIdentifiers.CATALOGO_OFFERTE_FISSO, 0);
                hashMap.put(Constants.CatalogIdentifiers.CATALOGO_OFFERTE_MOBILE, 0);
                hashMap.put(Constants.CatalogIdentifiers.CATALOGO_SCONTI, 0);
                hashMap.put(Constants.CatalogIdentifiers.CATALOGO_TERMINALI, 0);
                hashMap.put(Constants.CatalogIdentifiers.CATALOGO_QUESTIONARIO, 0);
                hashMap.put(Constants.CatalogIdentifiers.CATALOGO_6, 1);
                hashMap.put(Constants.CatalogIdentifiers.CATALOGO_SCONTI_RICARICABILI, 0);
                Session.getInstance().setCatalogsUpdate(hashMap);

                for (InfoCatalog infoCatalog : Session.getInstance().getInfoCatalogs()) {
                    if (Long.parseLong(infoCatalog.getCatalogLastUpdate()) <= Long.parseLong(CataloghiCheckVersionDAO.retrieveCatalogLastUpdateFromID(this, infoCatalog))) {
                        Session.getInstance().getCatalogsUpdate().put(infoCatalog.getCatalogIdentifier(), 1);
                    }
                }

                if (checkCatalogsUpdate()) {
                    goToMain();
                    break;
                }

                for (InfoCatalog infoCatalog : Session.getInstance().getInfoCatalogs()) {
                    if (Long.parseLong(infoCatalog.getCatalogLastUpdate()) > Long.parseLong(CataloghiCheckVersionDAO.retrieveCatalogLastUpdateFromID(this, infoCatalog))) {
                        callCatalogoTask(infoCatalog);
                    }
                }
                break;

            case Constants.HandlerWhat.WHAT_CATALOGHI_CHECK_VERSION_KO:

                break;

            case Constants.HandlerWhat.WHAT_CATALOGO_OFFERTE_FISSO:
                CataloghiCheckVersionDAO.updateCatalogCheckVersion(this, Session.getInstance().getInfoCatalogs().get(Constants.CatalogIdentifiers.CATALOGO_OFFERTE_FISSO - 1));
                Session.getInstance().getCatalogsUpdate().put(Constants.CatalogIdentifiers.CATALOGO_OFFERTE_FISSO, 1);
                if (checkCatalogsUpdate()) {
                    goToMain();
                    break;
                }
                break;

            case Constants.HandlerWhat.WHAT_CATALOGO_OFFERTE_FISSO_KO:
                Log.d("KO", "Offerte Fisso");
                break;

            case Constants.HandlerWhat.WHAT_CATALOGO_OFFERTE_MOBILE:
                CataloghiCheckVersionDAO.updateCatalogCheckVersion(this, Session.getInstance().getInfoCatalogs().get(Constants.CatalogIdentifiers.CATALOGO_OFFERTE_MOBILE - 1));
                Session.getInstance().getCatalogsUpdate().put(Constants.CatalogIdentifiers.CATALOGO_OFFERTE_MOBILE, 1);
                if (checkCatalogsUpdate()) {
                    goToMain();
                    break;
                }
                break;

            case Constants.HandlerWhat.WHAT_CATALOGO_OFFERTE_MOBILE_KO:
                Log.d("KO", "Offerte Mobile");
                break;

            case Constants.HandlerWhat.WHAT_CATALOGO_SCONTI:
                CataloghiCheckVersionDAO.updateCatalogCheckVersion(this, Session.getInstance().getInfoCatalogs().get(Constants.CatalogIdentifiers.CATALOGO_SCONTI - 1));
                Session.getInstance().getCatalogsUpdate().put(Constants.CatalogIdentifiers.CATALOGO_SCONTI, 1);
                if (checkCatalogsUpdate()) {
                    goToMain();
                    break;
                }
                break;

            case Constants.HandlerWhat.WHAT_CATALOGO_SCONTI_KO:
                Log.d("KO", "Sconti");
                break;

            case Constants.HandlerWhat.WHAT_CATALOGO_SCONTI_RICARICABILI:
                CataloghiCheckVersionDAO.updateCatalogCheckVersion(this, Session.getInstance().getInfoCatalogs().get(Constants.CatalogIdentifiers.CATALOGO_SCONTI_RICARICABILI - 1));
                Session.getInstance().getCatalogsUpdate().put(Constants.CatalogIdentifiers.CATALOGO_SCONTI_RICARICABILI, 1);
                if (checkCatalogsUpdate()) {
                    goToMain();
                    break;
                }
                break;

            case Constants.HandlerWhat.WHAT_CATALOGO_SCONTI_RICARICABILI_KO:
                Log.d("KO", "ScontiRicaricabili");
                break;

            case Constants.HandlerWhat.WHAT_CATALOGO_TERMINALI:
                CataloghiCheckVersionDAO.updateCatalogCheckVersion(this, Session.getInstance().getInfoCatalogs().get(Constants.CatalogIdentifiers.CATALOGO_TERMINALI - 1));
                Session.getInstance().getCatalogsUpdate().put(Constants.CatalogIdentifiers.CATALOGO_TERMINALI, 1);
                if (checkCatalogsUpdate()) {
                    goToMain();
                    break;
                }
                break;

            case Constants.HandlerWhat.WHAT_CATALOGO_TERMINALI_KO:
                Log.d("KO", "Terminali");
                break;

            case Constants.HandlerWhat.WHAT_CATALOGO_QUESTIONARIO:
                CataloghiCheckVersionDAO.updateCatalogCheckVersion(this, Session.getInstance().getInfoCatalogs().get(Constants.CatalogIdentifiers.CATALOGO_QUESTIONARIO - 1));
                Session.getInstance().getCatalogsUpdate().put(Constants.CatalogIdentifiers.CATALOGO_QUESTIONARIO, 1);
                if (checkCatalogsUpdate()) {
                    goToMain();
                    break;
                }
                break;

            case Constants.HandlerWhat.WHAT_CATALOGO_QUESTIONARIO_KO:
                Log.d("KO", "Questionario");
                break;
        }
        return false;
    }
}
