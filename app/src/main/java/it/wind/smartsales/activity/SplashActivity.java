package it.wind.smartsales.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;

import it.wind.smartsales.R;
import it.wind.smartsales.commons.Constants;
import it.wind.smartsales.commons.Utils;

/**
 * Created by luca.quaranta on 09/02/2016.
 */
public class SplashActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (checkDeviceDimension()) {
                    goToLogin();
                } else {
                    openErrorPopup();
                }
            }
        }, 1600);
    }

    private void openErrorPopup() {
        Utils.showNotCompatibilityMessage(this);
    }

    private boolean checkDeviceDimension() {
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        double screenInches = Math.sqrt(Math.pow((double) (dm.widthPixels) / (double) dm.xdpi, 2) + Math.pow((double) (dm.heightPixels) / (double) dm.ydpi, 2));

        boolean testScreen = getResources().getBoolean(R.bool.check_misure);
        return testScreen || (screenInches >= Constants.CompatibleDimensionScreen.MIN_DIAGONAL &&
                screenInches <= Constants.CompatibleDimensionScreen.MAX_DIAGONAL);
    }

    private void goToLogin() {
        Intent intent = new Intent(this, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }
}