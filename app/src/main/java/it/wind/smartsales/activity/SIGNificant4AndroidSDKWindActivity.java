package it.wind.smartsales.activity;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;

import com.xyzmo.enums.WebServiceCall;
import com.xyzmo.enums.WebServiceResult;
import com.xyzmo.sdk.ApplicationEventListener;
import com.xyzmo.sdk.DocumentEventListener;
import com.xyzmo.sdk.SdkManager;
import com.xyzmo.ui.DocumentImage;
import com.xyzmo.ui.dialog.GenericSimpleDialog;

import java.io.File;

import it.wind.smartsales.R;
import it.wind.smartsales.commons.Log;

/**
 * Created by cresh on 15/07/16.
 */
public class SIGNificant4AndroidSDKWindActivity extends DocumentImage implements DocumentEventListener, ApplicationEventListener {
    public static final String DEBUG_TAG = "SIGNificant4AndroidWindActivity";

    // Use negative numbers because the SDK uses numbers > 0
    protected static final int DIALOG_SDK_DEMO = -1;
    protected static final int DIALOG_SDK_DEMO_CLOSE_APP = -2;


    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState)
    {

        Log.i("Evviva","Entratooooooo!");

        // Register for application events
        SdkManager.sharedInstance().setApplicationEventListener(this);

        // Register for document events
        SdkManager.sharedInstance().setDocumentEventListener(this);

        // HOWTO: Set RequestHeaderProperties for TLS Webservice Calls als String key -> String value pairs
        SdkManager.sharedInstance().getRequestHeaderProperties().put("TestKey", "TestValue");

        // start the SDK app code
        super.onCreate(savedInstanceState);

        //Uri uri = Uri.parse("significant://http.151.1.80.170:57003/WorkstepController.Process.asmx?WorkstepId=19191727EE33121E45E6F5C9FF9ED981009D663DC7B40DA76B68DFF3041D6584FAAE498A671F31165FA9D821E5CCA0A3");

        String port = "";
        String xyzmoURL ="";
        String server ="";
        String protocol="";
        String workstepId ="";

        Bundle bundleExtra = getIntent().getExtras();
        if (bundleExtra!=null) {
            port = bundleExtra.getString("port");
            xyzmoURL = bundleExtra.getString("URL");
            server = bundleExtra.getString("server");
            protocol = bundleExtra.getString("protocol");
            workstepId = bundleExtra.getString("workstepId");
        }

        Uri uri = Uri.parse(xyzmoURL + "?"
                + "WorkstepId=" + workstepId
                + "&server=" + server
                + "&port=" + port
                + "&protocol=" + protocol);
        Log.i(DEBUG_TAG, uri.toString());
        SdkManager.sharedInstance().loadWorkstepDocumentFromUri(uri);

        // Now the app is loaded. you can do your stuff below

        // example: with this call you can load a workstep document from a URI:

		/*
		android.net.Uri uri = Uri.parse("significant://launch.xyzmo.com/SignificantAndroidAppLauncher.aspx?" +
				"WorkstepId=ACC1A5027314210068C20F5D296E467BE19DED547169F12AFCC211F43030CB715A91EA2C21B3BAB5483F59C52E73B96B" +
				"&server=beta2.testlab.xyzmo.com&port=57003&protocol=http");
		android.net.Uri uri2 = Uri.parse("significant://launch.xyzmo.com/SignificantAndroidAppLauncher.aspx?" +
				"WorkstepId=79A6A31F62742D7BE0FDCB0CA51251C46406CEB86F6666D9AE053849CEC9D1F9AF257AE645D3048279053D42BF6593AA&" +
				"server=sign043.xyzmo.com&port=40100&protocol=https");

		SdkManager.sharedInstance().loadWorkstepDocumentFromUri(uri);
		/* */


    }

    @SuppressLint("NewApi")
    @Override
    protected Dialog onCreateDialog(int id, Bundle bundle) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        Dialog dialog=null;

        switch (id) {
            case DIALOG_SDK_DEMO:
                builder.setTitle("Demo title dialog");
                builder.setMessage("Demo title dialog");
                builder.setNeutralButton(R.string.okText, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.dismiss();
                    }
                });
                dialog = builder.create();
                break;

            case DIALOG_SDK_DEMO_CLOSE_APP:
                builder.setTitle("Demo title dialog");
                builder.setMessage("Demo title message");
                builder.setPositiveButton(R.string.okText, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.dismiss();
                        final Intent intent = new Intent();
                        setResult(Activity.RESULT_OK, intent);
                        finish();
                    }
                });
                builder.setNegativeButton(R.string.cancelText, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.dismiss();
                    }
                });
                dialog = builder.create();
                break;

            default:
                return super.onCreateDialog(id, bundle);
        }

        return dialog;

    }

    @Override
    public boolean onSDKError(SDKError sdkError, Exception exceptionOccured)
    {
        it.wind.smartsales.commons.Log.d(DEBUG_TAG, "SDKErrorListener!");

        switch (sdkError)
        {
            case ActivityNotFound:
                Log.d(DEBUG_TAG, "SDKError ActivityNotFound occurred!");
                break;

            default:
                break;
        }

        return false;
    }

    @Override
    public boolean onWebServiceResultError(WebServiceResult webserviceResult, WebServiceCall webserviceCall, Exception exceptionOccured) {
        String localizedMessage = "";
        String webserviceResultError = "";
        String webServiceCallTried = "";

        if (exceptionOccured != null) {
            localizedMessage = exceptionOccured.getLocalizedMessage();
        }

        if (webserviceResult != null) {
            webserviceResultError = webserviceResult.toString();
        }

        if (webserviceCall != null) {
            webServiceCallTried = webserviceCall.toString();
        }

        it.wind.smartsales.commons.Log.d(DEBUG_TAG, "DemoSDKErrorListener, onWebServiceResultError, when trying to call: " + webServiceCallTried + " the following error happened: " + webserviceResultError + " and the following exception occured: " + localizedMessage + ".");

        return false;
    }

    @Override
    public String onStartOpenFileIntent()
    {
        Log.d(DEBUG_TAG, "StartOpenFileListener!");
		/*
		 * this callback is called, whenever the SDK App wants to open the built in file explorer. you can do anything you want in you own app here, as long
		 * as you return the absolute path to a pdf file. otherwise the builtin file explorer is used to select a file.
		 */
        return Environment.getExternalStorageDirectory().toString() + File.separator + "SIGNificant_Data/quickstart_guide_form.pdf";
    }

    @Override
    public boolean onDocumentInitiallyLoadedToScreen(String workstepId)
    {
		/*
		 * this is called when the document is loaded and showed for the first time
		 */
        Log.d(DEBUG_TAG, "onDocumentInitiallyLoadedToScreen, workstepId: " + workstepId);
        return false;
    }

    @Override
    public boolean onFinishDocument(String workstepId)
    {
        Log.d(DEBUG_TAG, "FinishDocumentListener!");
        return false;
    }

    @Override
    public boolean onRejectDocument(String workstepId)
    {
        Log.d(DEBUG_TAG, "RejectDocumentListener!");
        return false;
    }

    @Override
    public boolean onSendDocument(Intent sendIntent)
    {
        Log.d(DEBUG_TAG, "SendDocumentListener!");
		/*
		 * sendIntent.setPackage has to be called, so that the SDK knows that a specific Intent should be used
		 */
        sendIntent.setPackage("com.google.android.gm");
        sendIntent.setClassName("com.google.android.gm", "com.google.android.gm.ComposeActivityGmail");
        return false;
    }

    @Override
    public boolean onSaveDocument() {
        Log.d(DEBUG_TAG, "onSaveDocument");
        return false;
    }

    @Override
    public boolean onOpenDocumentWith(Intent openInIntent) {
        Log.d(DEBUG_TAG, "onOpenDocumentWith");
        return false;
    }

    @Override
    public boolean onViewDocumentIn(Intent viewInIntent) {
        Log.d(DEBUG_TAG, "onViewDocumentIn");
        return false;
    }

    @Override
    public boolean onSyncCanceled(String workstepId)
    {
        Log.d(DEBUG_TAG, "SyncCanceledListener!");

        return false;
		/*
		if (syncedWorkstepDocument!=null) {

			// get document index
			int index = mWorkstepDocumentListe.indexOf(syncedWorkstepDocument);

			// remove the document from the screen and from the list and from the file system (flash storage)
			removeWorkstepDocumentFromListAndFile(index);

		}

		// this will quit the app
		final Intent intent = new Intent();
		setResult(Activity.RESULT_OK, intent);
		finish();

		return true;
		/* */
    }

    @Override
    public boolean onShowDialog(GenericSimpleDialog.DialogType type) {
        Log.d(DEBUG_TAG, "onShowDialog, " + type.toString());
        return false;
    }

    @Override
    public boolean onWorkstepDocumentSynced(String workstepId, 	String workstepIdOnServer) {
        Log.d(DEBUG_TAG, "onWorkstepDocumentSynced workstepId: " + workstepId + ", workstepIdOnServer: " + workstepIdOnServer);
        return false;
    }

    @Override
    public boolean onWorkstepDocumentCreated(String workstepId) {
        Log.d(DEBUG_TAG, "onWorkstepDocumentCreated, workstepId: " + workstepId);
        return false;
    }

    @Override
    public boolean onWorkstepDocumentCompleteleyDownloaded(String workstepId) {
        Log.d(DEBUG_TAG, "onWorkstepDocumentCompleteleyDownloaded workstepId: " + workstepId);
        return false;
    }

    @Override
    public boolean onTemplateCompletelyDownloaded(String originalWorkstepId,
                                                  String templateName) {
        Log.d(DEBUG_TAG, "onTemplateCompletelyDownloaded originalWorkstepId: " + originalWorkstepId + ", templateName: " + templateName);
        return false;
    }

    @Override
    public boolean onCaptureSignature(String workstepId, String signatureId) {
        Log.d(DEBUG_TAG, "onCaptureSignature, workstepId: " + workstepId + ", signatureId: " + signatureId);
        return false;
    }

    @Override
    public boolean onCaptureSignatureResult(String workstepId, String signatureId, int activityResultCode) {
        Log.d(DEBUG_TAG, "onCaptureSignatureResult, workstepId: " + workstepId + ", signatureId: "
                + signatureId + ", activityResultCode: " + activityResultCode);
        return false;
    }

    @Override
    public boolean onSyncStateChange(Bundle syncStateBundle) {
        Log.d(DEBUG_TAG, "onSyncStateChange");
        return false;
    }

    @Override
    public boolean onCloseDocument() {
        Log.d(DEBUG_TAG, "onCloseDocument");
        return false;
    }

    @Override
    public boolean onWorkstepDocumentSaved2Disk(String workstepId, File savedFile) {
        Log.d(DEBUG_TAG, "onWorkstepDocumentSaved2Disk, workstepId: " + workstepId);
        return false;
    }

    @Override
    public boolean onDeviceNotAllowedToSign() {
        Log.d(DEBUG_TAG, "onDeviceNotAllowedToSign");
        return false;
    }

    @Override
    public boolean onWorkstepUndone(String workstepId, boolean online) {
        Log.d(DEBUG_TAG, "onWorkstepUndone, workstepId: " + workstepId + " online: " + online);
        return false;
    }

}
