package it.wind.smartsales.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.text.TextUtils;

import java.util.ArrayList;
import java.util.List;

import it.wind.smartsales.commons.Log;
import it.wind.smartsales.entities.OffertaMobile;
import it.wind.smartsales.entities.Terminale;

/**
 * Created by luca.quaranta on 22/03/2016.
 */
public class CatalogoTerminaliDAO {

    public static final String WIND_SMART_OFFICE = "WIND SMART OFFICE";
    public static final String ALL_INCLUSIVE = "ALL INCLUSIVE AZIENDE";

    public final static String WIND_CATALOGO_TERMINALI_TABLE_NAME = "CATALOGO_TERMINALI";

    public final static String ID_TERMINALE_FIELD = "ID_TERMINALE";
    public final static String OEM_FIELD = "OEM";
    public final static String TERMINALE_BRAND_FIELD = "TERMINALE_BRAND";
    public final static String TERMINALE_DESCRIPTION_FIELD = "TERMINALE_DESCRIPTION";
    public final static String MNP_FIELD = "MNP";
    public final static String OFFER_TYPE_FIELD = "OFFER_TYPE";
    public final static String DEVICE_TYPE_FIELD = "DEVICE_TYPE";
    public final static String DEVICE_SHORT_DESC_FIELD = "DEVICE_SHORT_DESC";
    public final static String DEVICE_CAPACITY_FIELD = "DEVICE_CAPACITY";
    public final static String DEVICE_COLOR_FIELD = "DEVICE_COLOR";
    public final static String PREZZO_LISTINO_FIELD = "PREZZO_LISTINO";
    public final static String RATA_INIZIALE_FIELD = "RATA_INIZIALE";
    public final static String RATA_MENSILE_FIELD = "RATA_MENSILE";
    public final static String RATA_FINALE_FIELD = "RATA_FINALE";
    public final static String IMAGE_URL_FIELD = "IMAGE_URL";
    public final static String ID_OFFERTA_FIELD = "ID_OFFERTA";

    public final static String ID_TERMINALE = ID_TERMINALE_FIELD + " VARCHAR(50)";
    public final static String OEM = OEM_FIELD + " VARCHAR(50)";
    public final static String TERMINALE_BRAND = TERMINALE_BRAND_FIELD + " VARCHAR(50)";
    public final static String TERMINALE_DESCRIPTION = TERMINALE_DESCRIPTION_FIELD + " VARCHAR(255)";
    public final static String MNP = MNP_FIELD + " VARCHAR(50)";
    public final static String DEVICE_TYPE = DEVICE_TYPE_FIELD + " VARCHAR(50)";
    public final static String DEVICE_SHORT_DESC = DEVICE_SHORT_DESC_FIELD + " VARCHAR(50)";
    public final static String DEVICE_CAPACITY = DEVICE_CAPACITY_FIELD + " VARCHAR(50)";
    public final static String DEVICE_COLOR = DEVICE_COLOR_FIELD + " VARCHAR(50)";
    public final static String OFFER_TYPE = OFFER_TYPE_FIELD + " VARCHAR(50)";
    public final static String PREZZO_LISTINO = PREZZO_LISTINO_FIELD + " VARCHAR(50)";
    public final static String RATA_INIZIALE = RATA_INIZIALE_FIELD + " VARCHAR(50)";
    public final static String RATA_MENSILE = RATA_MENSILE_FIELD + " VARCHAR(50)";
    public final static String RATA_FINALE = RATA_FINALE_FIELD + " VARCHAR(50)";
    public final static String IMAGE_URL = IMAGE_URL_FIELD + " VARCHAR(255)";
    public final static String ID_OFFERTA = ID_OFFERTA_FIELD + " VARCHAR(50)";

    public final static List<String> fields = new ArrayList<String>();

    static {
        fields.add(ID_TERMINALE);
        fields.add(OEM);
        fields.add(TERMINALE_BRAND);
        fields.add(TERMINALE_DESCRIPTION);
        fields.add(MNP);
        fields.add(OFFER_TYPE);
        fields.add(DEVICE_TYPE);
        fields.add(DEVICE_SHORT_DESC);
        fields.add(DEVICE_CAPACITY);
        fields.add(DEVICE_COLOR);
        fields.add(PREZZO_LISTINO);
        fields.add(RATA_INIZIALE);
        fields.add(RATA_MENSILE);
        fields.add(RATA_FINALE);
        fields.add(IMAGE_URL);
        fields.add(ID_OFFERTA);
    }

    public static void clearTerminaliTable(Context context) {
        SQLiteDatabase db = DbHelper.getInstance(context).getWritableDatabase();
        db.execSQL("DELETE FROM " + WIND_CATALOGO_TERMINALI_TABLE_NAME);
        db.close();
    }

    public static void insertTerminale(Context context, Terminale terminale) {
        SQLiteDatabase db = DbHelper.getInstance(context).getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(ID_TERMINALE_FIELD, terminale.getIdTerminale());
        values.put(OEM_FIELD, terminale.getOem());
        values.put(TERMINALE_BRAND_FIELD, terminale.getTerminaleBrand());
        values.put(TERMINALE_DESCRIPTION_FIELD, terminale.getTerminaleDescription());
        values.put(MNP_FIELD, terminale.getMnp());
        values.put(OFFER_TYPE_FIELD, terminale.getOfferType());
        values.put(DEVICE_TYPE_FIELD, terminale.getDeviceType());
        values.put(DEVICE_SHORT_DESC_FIELD, terminale.getShortDesc());
        values.put(DEVICE_CAPACITY_FIELD, terminale.getCapacity());
        values.put(DEVICE_COLOR_FIELD, terminale.getColor());
        values.put(PREZZO_LISTINO_FIELD, terminale.getPrezzoListino());
        values.put(RATA_INIZIALE_FIELD, terminale.getRataIniziale());
        values.put(RATA_MENSILE_FIELD, terminale.getRataMensile());
        values.put(RATA_FINALE_FIELD, terminale.getRataFinale());
        values.put(IMAGE_URL_FIELD, terminale.getImageUrl());
        values.put(ID_OFFERTA_FIELD, terminale.getIdOfferta());

        db.insert(WIND_CATALOGO_TERMINALI_TABLE_NAME, null, values);
        db.close();
    }

    public static ArrayList<String> getColorsByShortDesc(Context context, String shortDesc) {
        ArrayList<String> colors = new ArrayList<>();

        String SELECT = "SELECT Distinct(" + DEVICE_COLOR_FIELD + ")";
        String FROM = " FROM " + WIND_CATALOGO_TERMINALI_TABLE_NAME;
        String WHERE = " WHERE UPPER (" + DEVICE_SHORT_DESC_FIELD + ") = UPPER('" + shortDesc + "')";
        String ORDER_BY = " ORDER BY " + DEVICE_COLOR_FIELD + ";";
        String query = SELECT + FROM + WHERE + ORDER_BY;

        SQLiteDatabase db = DbHelper.getInstance(context).getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                final String color = cursor.getString(cursor.getColumnIndex(DEVICE_COLOR_FIELD));
                if (!TextUtils.isEmpty(color))
                    colors.add(color);
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();

        return colors;
    }

    public static ArrayList<String> getCapacitiesByShortDesc(Context context, String shortDesc) {
        ArrayList<String> capacities = new ArrayList<>();

        String SELECT = "SELECT Distinct(" + DEVICE_CAPACITY_FIELD + ")";
        String FROM = " FROM " + WIND_CATALOGO_TERMINALI_TABLE_NAME;
        String WHERE = " WHERE UPPER (" + DEVICE_SHORT_DESC_FIELD + ") = UPPER('" + shortDesc + "')";
        String ORDER_BY = " ORDER BY CAST(" + DEVICE_CAPACITY_FIELD + " AS INT);";
        String query = SELECT + FROM + WHERE + ORDER_BY;

        SQLiteDatabase db = DbHelper.getInstance(context).getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                final String capacity = cursor.getString(cursor.getColumnIndex(DEVICE_CAPACITY_FIELD));
                if (!TextUtils.isEmpty(capacity))
                    capacities.add(capacity);
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();

        return capacities;
    }

    public static void updatePrices(Context context, Terminale t) {
        final String shortDesc = t.getShortDesc();
        final String color = !TextUtils.isEmpty(t.getColor()) ? t.getColor() : "";
        final String capacity = !TextUtils.isEmpty(t.getCapacity()) ? t.getCapacity() : "";

        String SELECT = "SELECT " + ID_TERMINALE_FIELD + ", " + MNP_FIELD + ", " + RATA_MENSILE_FIELD + ", " +
                PREZZO_LISTINO_FIELD + ", " + DEVICE_SHORT_DESC_FIELD + ", " + DEVICE_COLOR_FIELD + ", " +
                DEVICE_CAPACITY_FIELD + ", " + TERMINALE_BRAND_FIELD;
        String FROM = " FROM " + WIND_CATALOGO_TERMINALI_TABLE_NAME;
        String WHERE = " WHERE UPPER (" + DEVICE_SHORT_DESC_FIELD + ") = UPPER('" + shortDesc + "') AND UPPER" +
                "( " + DEVICE_COLOR_FIELD + ") = UPPER('" + color + "') AND UPPER (" + DEVICE_CAPACITY_FIELD +
                ") = UPPER ('" + capacity + "')";
        String GROUP_BY = " GROUP BY " + MNP_FIELD + ", " + DEVICE_COLOR_FIELD + ", " + DEVICE_CAPACITY_FIELD + ";";
        String query = SELECT + FROM + WHERE + GROUP_BY;

        SQLiteDatabase db = DbHelper.getInstance(context).getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                t.setIdTerminale(cursor.getString(cursor.getColumnIndex(ID_TERMINALE_FIELD)));
                t.setPrezzoListino(cursor.getString(cursor.getColumnIndex(PREZZO_LISTINO_FIELD)));
                if (Boolean.valueOf(cursor.getString(cursor.getColumnIndex(MNP_FIELD)))) {
                    t.setRataMensileMnp(cursor.getString(cursor.getColumnIndex(RATA_MENSILE_FIELD)).replace(".00", ""));
                } else {
                    t.setRataMensile(cursor.getString(cursor.getColumnIndex(RATA_MENSILE_FIELD)).replace(".00", ""));
                }
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
    }

    public static ArrayList<Terminale> getTerminalList(Context context) {
        ArrayList<Terminale> terminalList = new ArrayList<>();

        SQLiteDatabase db = DbHelper.getInstance(context).getWritableDatabase();
        Cursor cursor = db.rawQuery(getQueryRowWithMinMonthRate(), null);
        if (cursor.moveToFirst()) {
            do {
                Terminale terminale = new Terminale();
                terminale.setShortDesc(cursor.getString(cursor.getColumnIndex(DEVICE_SHORT_DESC_FIELD)));
                terminale.setOem(cursor.getString(cursor.getColumnIndex(OEM_FIELD)));
                terminale.setTerminaleBrand(cursor.getString(cursor.getColumnIndex(TERMINALE_BRAND_FIELD)));
                terminale.setRataMensileMnp(cursor.getString(cursor.getColumnIndex("MIN(RATA_MNP)")));
                terminale.setRataMensile(cursor.getString(cursor.getColumnIndex("MIN(RATA)")));
                terminale.setPrezzoListino(cursor.getString(cursor.getColumnIndex(PREZZO_LISTINO_FIELD)));
                terminale.setImageUrl(cursor.getString(cursor.getColumnIndex(IMAGE_URL_FIELD)));
                terminale.setDeviceType(cursor.getString(cursor.getColumnIndex(DEVICE_TYPE_FIELD)));
                terminalList.add(terminale);
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();

        return terminalList;
    }

    public static String getQueryElencoShortDesc() {
        String SELECT = "SELECT " + "distinct UPPER (" + DEVICE_SHORT_DESC_FIELD + ")";
        String FROM = " FROM " + WIND_CATALOGO_TERMINALI_TABLE_NAME;
        String WHERE = " WHERE " + OFFER_TYPE_FIELD + " in ('Abbonamento','Ricaricabile')" +
                " AND " + DEVICE_SHORT_DESC_FIELD + "!=''";
        String ORDER_BY = " ORDER BY " + TERMINALE_BRAND_FIELD;

        return SELECT + FROM + WHERE + ORDER_BY;
    }

    public static String getQueryRowWithMinMonthRate() {
        String SELECT1 = "SELECT " + DEVICE_TYPE_FIELD + ", " + DEVICE_SHORT_DESC_FIELD + ", " + OEM_FIELD + ", "
                + TERMINALE_BRAND_FIELD + ", " + IMAGE_URL_FIELD + " ,MIN(RATA_MNP), MIN(RATA)," + PREZZO_LISTINO_FIELD;
        String SELECT2 = " SELECT " + DEVICE_TYPE_FIELD + ", " + DEVICE_SHORT_DESC_FIELD + ", " + OEM_FIELD + ", "
                + TERMINALE_BRAND_FIELD + ", " + PREZZO_LISTINO_FIELD
                + ", CASE UPPER(mnp) WHEN UPPER('true') THEN CAST(" + RATA_MENSILE_FIELD + " AS INT) ELSE "
                + "null END AS RATA_MNP, CASE UPPER(mnp) WHEN UPPER('false') THEN CAST( " + RATA_MENSILE_FIELD
                + " AS INT) ELSE null END AS RATA, " + IMAGE_URL_FIELD;
        String WHERE1 = " WHERE UPPER(" + DEVICE_SHORT_DESC_FIELD + ") in (" + getQueryElencoShortDesc() + ")";
        String FROM2 = " FROM " + WIND_CATALOGO_TERMINALI_TABLE_NAME;
        ;
        String FROM1 = " FROM (" + SELECT2 + " " + FROM2 + " " + WHERE1 + ")";
        String GROUP_BY = " AS INT GROUP BY " + DEVICE_SHORT_DESC_FIELD + ", " + OEM_FIELD + ", "
                + TERMINALE_BRAND_FIELD + ";";

        return SELECT1 + FROM1 + GROUP_BY;
    }

    public static String getQueryTerminaliFisso(int id, String type) {
//        String SELECT1 = "SELECT " + ID_TERMINALE_FIELD + ", " + OEM_FIELD + ", " + TERMINALE_BRAND_FIELD + ", "
//                + TERMINALE_DESCRIPTION_FIELD + ", MIN(CAST(" + RATA_MENSILE_FIELD + " AS INT)) RATA, " +
//                IMAGE_URL_FIELD;
        String SELECT = "SELECT *";
        String FROM = " FROM " + WIND_CATALOGO_TERMINALI_TABLE_NAME;
        String WHERE = " WHERE " + ID_OFFERTA_FIELD + " = " + id;

        WHERE += " AND UPPER(" + OFFER_TYPE_FIELD + ") in ('" + (type.equalsIgnoreCase(WIND_SMART_OFFICE) ? type.toUpperCase() : "ALL INCLUSIVE AZIENDE','ALL INCLUSIVE BUSINESS") + "')";
        String GROUP_BY = " GROUP BY " + ID_TERMINALE_FIELD + ", " + OEM_FIELD + ", " + TERMINALE_BRAND_FIELD + ", " + TERMINALE_DESCRIPTION_FIELD;
        String ORDER_BY = " ORDER BY CAST(" + ID_TERMINALE_FIELD + " AS INT);";
        return SELECT + FROM + WHERE + GROUP_BY + ORDER_BY;
    }

    public static String getQueryTerminaliFissoTappo(String type) {
//        String SELECT1 = "SELECT " + ID_TERMINALE_FIELD + ", " + OEM_FIELD + ", " + TERMINALE_BRAND_FIELD + ", "
//                + TERMINALE_DESCRIPTION_FIELD + ", MIN(CAST(" + RATA_MENSILE_FIELD + " AS INT)) RATA, " +
//                IMAGE_URL_FIELD;

        String SELECT = "SELECT *";
        String FROM = " FROM " + WIND_CATALOGO_TERMINALI_TABLE_NAME;
        String WHERE = " WHERE UPPER(" + OFFER_TYPE_FIELD + ") in ('" + (type.equalsIgnoreCase(WIND_SMART_OFFICE) ? type.toUpperCase() : "ALL INCLUSIVE AZIENDE','ALL INCLUSIVE BUSINESS") + "')";
        String GROUP_BY = " GROUP BY " + ID_TERMINALE_FIELD + ", " + OEM_FIELD + ", " + TERMINALE_BRAND_FIELD + ", " + TERMINALE_DESCRIPTION_FIELD;
        String ORDER_BY = " ORDER BY CAST(" + ID_TERMINALE_FIELD + " AS INT);";
        return SELECT + FROM + WHERE + GROUP_BY + ORDER_BY;
    }

    public static ArrayList<Terminale> getTerminaliFisso(Context context, int offerId, String type) {
        SQLiteDatabase db = DbHelper.getInstance(context).getWritableDatabase();
        Cursor cursor = db.rawQuery(getQueryTerminaliFisso(offerId, type), null);

        ArrayList<Terminale> terminali = new ArrayList<>();
        if (cursor.moveToFirst()) {
            terminali = setTerminali(cursor);
        } else {
            cursor = db.rawQuery(getQueryTerminaliFissoTappo(type), null);
            if (cursor.moveToFirst())
                terminali = setTerminali(cursor);
        }
        cursor.close();
        db.close();
        return terminali;
    }


    public static ArrayList<Terminale> retrieveTerminali(Context context, OffertaMobile offertaMobile, boolean isSmartphoneSelected, boolean isTabletSelected, boolean isAppleSelected, boolean isWindowsSelected, boolean isSamsungSelected, boolean isAltroSelected) {
        SQLiteDatabase db = DbHelper.getInstance(context).getWritableDatabase();

        String query = "SELECT " + ID_TERMINALE_FIELD + ", " +
                OEM_FIELD + ", " +
                TERMINALE_BRAND_FIELD + ", " +
                TERMINALE_DESCRIPTION_FIELD + ", " +
                MNP_FIELD + ", " +
                OFFER_TYPE_FIELD + ", " +
                DEVICE_TYPE_FIELD + ", " +
                PREZZO_LISTINO_FIELD + ", " +
                RATA_INIZIALE_FIELD + ", " +
                "MIN(CAST(" + RATA_MENSILE_FIELD + " AS INT)) AS " + RATA_MENSILE_FIELD + ", " +
                RATA_FINALE_FIELD + ", " +
                IMAGE_URL_FIELD + ", " +
                ID_OFFERTA_FIELD +
                " FROM " + WIND_CATALOGO_TERMINALI_TABLE_NAME + " WHERE " + ID_OFFERTA_FIELD + " = '" + offertaMobile.getIdOfferta() + "'" +
                " AND " + OFFER_TYPE_FIELD + " = '" + offertaMobile.getOfferType() + "'" +
                createWhereConditionOEMType(isAppleSelected, isWindowsSelected, isSamsungSelected, isAltroSelected, isSmartphoneSelected, isTabletSelected) +
                " GROUP BY " + ID_TERMINALE_FIELD + ", " + OEM_FIELD + ", " + TERMINALE_BRAND_FIELD + ", " + TERMINALE_DESCRIPTION_FIELD +
                " ORDER BY CAST(" + ID_TERMINALE_FIELD + " AS INT)";

        Log.d("QUERY", "retrieveTerminali: " + query);

        Cursor cursor = db.rawQuery(query, null);

        ArrayList<Terminale> terminali = new ArrayList<>();
        if (cursor.moveToFirst()) {
            terminali = setTerminali(cursor);
        }

        if (terminali.isEmpty()) {
            query = "SELECT " + ID_TERMINALE_FIELD + ", " +
                    OEM_FIELD + ", " +
                    TERMINALE_BRAND_FIELD + ", " +
                    TERMINALE_DESCRIPTION_FIELD + ", " +
                    MNP_FIELD + ", " +
                    OFFER_TYPE_FIELD + ", " +
                    DEVICE_TYPE_FIELD + ", " +
                    PREZZO_LISTINO_FIELD + ", " +
                    RATA_INIZIALE_FIELD + ", " +
                    "MIN(CAST(" + RATA_MENSILE_FIELD + " AS INT)) AS " + RATA_MENSILE_FIELD + ", " +
                    RATA_FINALE_FIELD + ", " +
                    IMAGE_URL_FIELD + ", " +
                    ID_OFFERTA_FIELD +
                    " FROM " + WIND_CATALOGO_TERMINALI_TABLE_NAME + " WHERE " + ID_OFFERTA_FIELD + " = '" + offertaMobile.getIdOfferta() + "'" +
                    " AND " + OFFER_TYPE_FIELD + " = '" + offertaMobile.getOfferType() + "'" +
                    " GROUP BY " + ID_TERMINALE_FIELD + ", " + OEM_FIELD + ", " + TERMINALE_BRAND_FIELD + ", " + TERMINALE_DESCRIPTION_FIELD +
                    " ORDER BY CAST(" + ID_TERMINALE_FIELD + " AS INT)";
            cursor = db.rawQuery(query, null);

            Log.d("QUERY TAPPO", "retrieveTerminali: " + query);

            if (cursor.moveToFirst()) {
                terminali = setTerminali(cursor);
            }
        }

        cursor.close();
        db.close();
        return terminali;
    }

    ;

    private static ArrayList<Terminale> setTerminali(Cursor cursor) {
        ArrayList<Terminale> terminali = new ArrayList<>();
        do {
            Terminale terminale = new Terminale();
            terminale.setIdTerminale(cursor.getString(cursor.getColumnIndex(ID_TERMINALE_FIELD)));
            terminale.setOem(cursor.getString(cursor.getColumnIndex(OEM_FIELD)));
            terminale.setTerminaleBrand(cursor.getString(cursor.getColumnIndex(TERMINALE_BRAND_FIELD)));
            terminale.setTerminaleDescription(cursor.getString(cursor.getColumnIndex(TERMINALE_DESCRIPTION_FIELD)));
            terminale.setMnp(cursor.getString(cursor.getColumnIndex(MNP_FIELD)));
            terminale.setOfferType(cursor.getString(cursor.getColumnIndex(OFFER_TYPE_FIELD)));
            terminale.setDeviceType(cursor.getString(cursor.getColumnIndex(DEVICE_TYPE_FIELD)));
            terminale.setPrezzoListino(cursor.getString(cursor.getColumnIndex(PREZZO_LISTINO_FIELD)));
            terminale.setRataIniziale(cursor.getString(cursor.getColumnIndex(RATA_INIZIALE_FIELD)));
            terminale.setRataMensile(cursor.getString(cursor.getColumnIndex(RATA_MENSILE_FIELD)));
            terminale.setRataFinale(cursor.getString(cursor.getColumnIndex(RATA_FINALE_FIELD)));
            terminale.setImageUrl(cursor.getString(cursor.getColumnIndex(IMAGE_URL_FIELD)));
            terminale.setIdOfferta(cursor.getString(cursor.getColumnIndex(ID_OFFERTA_FIELD)));
            terminali.add(terminale);
        } while (cursor.moveToNext());
        return terminali;
    }

    private static String createWhereConditionOEMType(boolean isApple, boolean isWindows, boolean isSamsung, boolean isAltro, boolean isSmartphone, boolean isTablet) {
        String whereCondition = " AND OEM IN (__REPLACE_ALTRO__,__REPLACE_APPLE__,__REPLACE_SAMSUNG__,__REPLACE_WINDOW__) AND DEVICE_TYPE IN (__REPLACE_SMARTPHONE__,__REPLACE_TABLET__)";

        whereCondition = whereCondition.replace("__REPLACE_ALTRO__", isAltro ? "'Altro'" : "");
        whereCondition = whereCondition.replace("__REPLACE_APPLE__", isApple ? "'Apple'" : "");
        whereCondition = whereCondition.replace("__REPLACE_SAMSUNG__", isSamsung ? "'Samsung'" : "");
        whereCondition = whereCondition.replace("__REPLACE_WINDOW__", isWindows ? "'Windows Phone'" : "");

        whereCondition = whereCondition.replace(",,", ",");
        whereCondition = whereCondition.replace("(,", "(");
        whereCondition = whereCondition.replace(",)", ")");

        whereCondition = whereCondition.replace("__REPLACE_SMARTPHONE__", isSmartphone ? "'Smartphone'" : "");
        whereCondition = whereCondition.replace("__REPLACE_TABLET__", isTablet ? "'Tablet'" : "");

        whereCondition = whereCondition.replace(",,", ",");
        whereCondition = whereCondition.replace("(,", "(");
        whereCondition = whereCondition.replace(",)", ")");

        whereCondition = whereCondition.replace("AND OEM IN ()", "");


        return whereCondition;
    }

    public static Terminale retrieveTerminale(Context context, String idTerminale, String mnp, String idOfferta) {
        SQLiteDatabase db = DbHelper.getInstance(context).getWritableDatabase();

        String query = "SELECT " +
                ID_TERMINALE_FIELD + ", " +
                OEM_FIELD + ", " +
                TERMINALE_BRAND_FIELD + ", " +
                TERMINALE_DESCRIPTION_FIELD + ", " +
                MNP_FIELD + ", " +
                OFFER_TYPE_FIELD + ", " +
                DEVICE_TYPE_FIELD + ", " +
                PREZZO_LISTINO_FIELD + ", " +
                RATA_INIZIALE_FIELD + ", " +
                RATA_MENSILE_FIELD + ", " +
                RATA_FINALE_FIELD + ", " +
                IMAGE_URL_FIELD + ", " +
                ID_OFFERTA_FIELD +
                " FROM " + WIND_CATALOGO_TERMINALI_TABLE_NAME +
                " WHERE " + ID_TERMINALE_FIELD + " = '" + idTerminale + "'" +
                " AND " + MNP_FIELD + " = '" + mnp + "'" +
                " AND " + ID_OFFERTA_FIELD + " = '" + idOfferta + "'";
        Cursor cursor = db.rawQuery(query, null);

        Log.d("QUERY", "retrieveTerminale: " + query);

        Terminale terminale = new Terminale();
        if (cursor.moveToFirst()) {
            do {
                terminale.setIdTerminale(cursor.getString(cursor.getColumnIndex(ID_TERMINALE_FIELD)));
                terminale.setOem(cursor.getString(cursor.getColumnIndex(OEM_FIELD)));
                terminale.setTerminaleBrand(cursor.getString(cursor.getColumnIndex(TERMINALE_BRAND_FIELD)));
                terminale.setTerminaleDescription(cursor.getString(cursor.getColumnIndex(TERMINALE_DESCRIPTION_FIELD)));
                terminale.setMnp(cursor.getString(cursor.getColumnIndex(MNP_FIELD)));
                terminale.setOfferType(cursor.getString(cursor.getColumnIndex(OFFER_TYPE_FIELD)));
                terminale.setDeviceType(cursor.getString(cursor.getColumnIndex(DEVICE_TYPE_FIELD)));
                terminale.setPrezzoListino(cursor.getString(cursor.getColumnIndex(PREZZO_LISTINO_FIELD)));
                terminale.setRataIniziale(cursor.getString(cursor.getColumnIndex(RATA_INIZIALE_FIELD)));
                terminale.setRataMensile(cursor.getString(cursor.getColumnIndex(RATA_MENSILE_FIELD)));
                terminale.setRataFinale(cursor.getString(cursor.getColumnIndex(RATA_FINALE_FIELD)));
                terminale.setImageUrl(cursor.getString(cursor.getColumnIndex(IMAGE_URL_FIELD)));
                terminale.setIdOfferta(cursor.getString(cursor.getColumnIndex(ID_OFFERTA_FIELD)));
            } while (cursor.moveToNext());
        }


        cursor.close();
        db.close();
        return terminale;
    }

    public static ArrayList<Terminale> retrieveTerminaliForSIM(Context context, OffertaMobile offertaMobile) {
        SQLiteDatabase db = DbHelper.getInstance(context).getWritableDatabase();

        ArrayList<Terminale> terminali = new ArrayList<>();

        String query = "SELECT " + ID_TERMINALE_FIELD + ", " +
                OEM_FIELD + ", " +
                TERMINALE_BRAND_FIELD + ", " +
                TERMINALE_DESCRIPTION_FIELD + ", " +
                MNP_FIELD + ", " +
                OFFER_TYPE_FIELD + ", " +
                DEVICE_TYPE_FIELD + ", " +
                PREZZO_LISTINO_FIELD + ", " +
                RATA_INIZIALE_FIELD + ", " +
                "MIN(CAST(" + RATA_MENSILE_FIELD + " AS INT)) AS " + RATA_MENSILE_FIELD + ", " +
                RATA_FINALE_FIELD + ", " +
                IMAGE_URL_FIELD + ", " +
                ID_OFFERTA_FIELD +
                " FROM " + WIND_CATALOGO_TERMINALI_TABLE_NAME + " WHERE " + ID_OFFERTA_FIELD + " = '" + offertaMobile.getIdOfferta() + "'" +
                " AND " + OFFER_TYPE_FIELD + " = '" + offertaMobile.getOfferType() + "'" +
                " AND " + MNP_FIELD + " = '" + String.valueOf(offertaMobile.isMnp()) + "'" +
                " GROUP BY " + ID_TERMINALE_FIELD + ", " + OEM_FIELD + ", " + TERMINALE_BRAND_FIELD + ", " + TERMINALE_DESCRIPTION_FIELD +
                " ORDER BY CAST(" + ID_TERMINALE_FIELD + " AS INT)";
        Cursor cursor = db.rawQuery(query, null);

        Log.d("QUERY TAPPO", "retrieveTerminali: " + query);

        if (cursor.moveToFirst()) {
            terminali = setTerminali(cursor);
        }

        cursor.close();
        db.close();
        return terminali;
    }
}