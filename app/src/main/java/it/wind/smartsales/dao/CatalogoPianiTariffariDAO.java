package it.wind.smartsales.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import it.wind.smartsales.entities.PianoTariffario;

/**
 * Created by luca.quaranta on 22/03/2016.
 */
public class CatalogoPianiTariffariDAO {

    public final static String WIND_CATALOGO_PIANI_TARIFFARI_TABLE_NAME = "CATALOGO_PIANI_TARIFFARI";

    public final static String ID_OFFERTA_FIELD = "ID_OFFERTA";
    public final static String PERCENTUALE_SCONTO_FIELD = "PERCENTUALE_SCONTO";
    public final static String PERCENTUALE_SCONTO_PENALE_FIELD = "PERCENTUALE_SCONTO_PENALE";

    public final static String ID_OFFERTA = ID_OFFERTA_FIELD + " VARCHAR(50)";
    public final static String PERCENTUALE_SCONTO = PERCENTUALE_SCONTO_FIELD + " VARCHAR(50)";
    public final static String PERCENTUALE_SCONTO_PENALE = PERCENTUALE_SCONTO_PENALE_FIELD + " VARCHAR(50)";

    public final static List<String> fields = new ArrayList<String>();

    static {
        fields.add(ID_OFFERTA);
        fields.add(PERCENTUALE_SCONTO);
        fields.add(PERCENTUALE_SCONTO_PENALE);
    }

    public static void clearPianiTariffariTable(Context context){
        SQLiteDatabase db = DbHelper.getInstance(context).getWritableDatabase();
        db.execSQL("DELETE FROM "+ WIND_CATALOGO_PIANI_TARIFFARI_TABLE_NAME);
        db.close();
    }

    public static void insertPianoTariffario(Context context, PianoTariffario pianoTariffario){
        SQLiteDatabase db = DbHelper.getInstance(context).getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(ID_OFFERTA_FIELD, pianoTariffario.getIdOfferta());
        values.put(PERCENTUALE_SCONTO_FIELD, pianoTariffario.getPercentualeSconto());
        values.put(PERCENTUALE_SCONTO_PENALE_FIELD, pianoTariffario.getPercentualeScontoPenale());

        db.insert(WIND_CATALOGO_PIANI_TARIFFARI_TABLE_NAME, null, values);
        db.close();
    }

    public static ArrayList<PianoTariffario> retrievePianiTariffari(Context context, String idOfferta) {
        SQLiteDatabase db = DbHelper.getInstance(context).getWritableDatabase();

        String query = "SELECT " + ID_OFFERTA_FIELD + ", " + PERCENTUALE_SCONTO_FIELD + ", " + PERCENTUALE_SCONTO_PENALE_FIELD + " FROM " + WIND_CATALOGO_PIANI_TARIFFARI_TABLE_NAME +
                " WHERE " + ID_OFFERTA_FIELD + " = '" + idOfferta + "'" +
                " ORDER BY cast(" + ID_OFFERTA_FIELD + " as int) ASC" +
                ", cast(" + PERCENTUALE_SCONTO_FIELD + " as int) ASC ";
        Cursor cursor = db.rawQuery(query, null);

        ArrayList<PianoTariffario> pianiTariffari = new ArrayList<>();
        if (cursor.moveToFirst()) {
            do {
                PianoTariffario pianoTariffario = new PianoTariffario();
                pianoTariffario.setIdOfferta(cursor.getString(cursor.getColumnIndex(ID_OFFERTA_FIELD)));
                pianoTariffario.setPercentualeSconto(cursor.getString(cursor.getColumnIndex(PERCENTUALE_SCONTO_FIELD)));
                pianoTariffario.setPercentualeScontoPenale(cursor.getString(cursor.getColumnIndex(PERCENTUALE_SCONTO_PENALE_FIELD)));
                pianiTariffari.add(pianoTariffario);
            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();
        return pianiTariffari;
    }

}
