package it.wind.smartsales.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import it.wind.smartsales.entities.FasceSconti;

/**
 * Created by luca.quaranta on 22/03/2016.
 */
public class CatalogoFasceScontiDAO {

    public final static String WIND_CATALOGO_FASCE_SCONTI_TABLE_NAME = "CATALOGO_FASCE_SCONTI";

    public final static String FASCIA_SIM_1_FIELD = "FASCIA_SIM_1";
    public final static String FASCIA_SIM_2_FIELD = "FASCIA_SIM_2";
    public final static String FASCIA_SIM_3_FIELD = "FASCIA_SIM_3";
    public final static String FASCIA_SPESA_1_FIELD = "FASCIA_SPESA_1";
    public final static String FASCIA_SPESA_2_FIELD = "FASCIA_SPESA_2";

    public final static String FASCIA_SIM_1 = FASCIA_SIM_1_FIELD + " VARCHAR(50)";
    public final static String FASCIA_SIM_2 = FASCIA_SIM_2_FIELD + " VARCHAR(50)";
    public final static String FASCIA_SIM_3 = FASCIA_SIM_3_FIELD + " VARCHAR(50)";
    public final static String FASCIA_SPESA_1 = FASCIA_SPESA_1_FIELD + " VARCHAR(50)";
    public final static String FASCIA_SPESA_2 = FASCIA_SPESA_2_FIELD + " VARCHAR(50)";

    public final static List<String> fields = new ArrayList<String>();

    static {
        fields.add(FASCIA_SIM_1);
        fields.add(FASCIA_SIM_2);
        fields.add(FASCIA_SIM_3);
        fields.add(FASCIA_SPESA_1);
        fields.add(FASCIA_SPESA_2);
    }

    public static void clearFasceScontiTable(Context context){
        SQLiteDatabase db = DbHelper.getInstance(context).getWritableDatabase();
        db.execSQL("DELETE FROM "+ WIND_CATALOGO_FASCE_SCONTI_TABLE_NAME);
        db.close();
    }

    public static void insertFasceSconti(Context context, FasceSconti fasceSconti){
        SQLiteDatabase db = DbHelper.getInstance(context).getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(FASCIA_SIM_1_FIELD, fasceSconti.getFasciaSim1());
        values.put(FASCIA_SIM_2_FIELD, fasceSconti.getFasciaSim2());
        values.put(FASCIA_SIM_3_FIELD, fasceSconti.getFasciaSim3());
        values.put(FASCIA_SPESA_1_FIELD, fasceSconti.getFasciaSpesa1());
        values.put(FASCIA_SPESA_2_FIELD, fasceSconti.getFasciaSpesa2());

        db.insert(WIND_CATALOGO_FASCE_SCONTI_TABLE_NAME, null, values);
        db.close();
    }

    public static FasceSconti retrieveFasceSconti(Context context) {
        SQLiteDatabase db = DbHelper.getInstance(context).getWritableDatabase();

        String query = "SELECT " +
                FASCIA_SIM_1_FIELD + ", " +
                FASCIA_SIM_2_FIELD + ", " +
                FASCIA_SIM_3_FIELD + ", " +
                FASCIA_SPESA_1_FIELD + ", " +
                FASCIA_SPESA_2_FIELD +
                " FROM " + WIND_CATALOGO_FASCE_SCONTI_TABLE_NAME;
        Cursor cursor = db.rawQuery(query, null);

        FasceSconti fasceSconti = new FasceSconti();
        if (cursor.moveToFirst()) {
            do {
                fasceSconti.setFasciaSim1(Integer.valueOf(cursor.getString(cursor.getColumnIndex(FASCIA_SIM_1_FIELD))));
                fasceSconti.setFasciaSim2(Integer.valueOf(cursor.getString(cursor.getColumnIndex(FASCIA_SIM_2_FIELD))));
                fasceSconti.setFasciaSim3(Integer.valueOf(cursor.getString(cursor.getColumnIndex(FASCIA_SIM_3_FIELD))));
                fasceSconti.setFasciaSpesa1(Integer.valueOf(cursor.getString(cursor.getColumnIndex(FASCIA_SPESA_1_FIELD))));
                fasceSconti.setFasciaSpesa2(Integer.valueOf(cursor.getString(cursor.getColumnIndex(FASCIA_SPESA_2_FIELD))));
            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();
        return fasceSconti;
    }

}
