package it.wind.smartsales.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import it.wind.smartsales.entities.Pratica;
import it.wind.smartsales.entities.Session;
import it.wind.smartsales.commons.Log;


public class PraticheDAO {

    public final static String WIND_PRATICHE_TABLE_NAME = "PRATICHE";

    public final static String ID_PRATICA_FIELD = "ID_PRATICA";
    public final static String REQUEST_ID_FIELD = "REQUEST_ID";
    public final static String NOME_PRATICA_FIELD = "NOME_PRATICA";
    public final static String JSON_DATA_FIELD = "DATA"; //json object containing pratica data
    public final static String STEP_FIELD = "STEP";
    public final static String STATO_FIELD = "STATO";
    public final static String CREATION_DATE_FIELD = "CREATION_DATE";
    public final static String LAST_UPDATE_FIELD = "LAST_UPDATE";
    public final static String CODICE_DEALER_FIELD = "CODICE_DEALER";
    public final static String NOTE_FIELD = "NOTE";
    public final static String LAST_UPDATE_NOTE_FIELD = "LAST_UPDATE_NOTE";

    public final static String PERCENTUALE_COMPLET_FIELD = "PERCENTUALE_COMPLETAMENTO";
    public final static String IS_NOME_EDITABLE_FIELD = "IS_NOME_EDITABLE";
    public final static String PIVA_FIELD = "PIVA";

    public static final String STEP1 = "STEP1";
    public static final String STEP2 = "STEP2";
    public static final String STEP3 = "STEP3";
    public static final String STEP4 = "STEP4";
    public static final String STEP5 = "STEP5";
    public static final String STEP6 = "STEP6";
    public static final String STEP7 = "STEP7";
    public static final String STEP8 = "STEP8";
    public static final String STEP9 = "STEP9";
    public static final String STEP10 = "STEP10";


    public final static String ID_PRATICA = ID_PRATICA_FIELD + " INTEGER PRIMARY KEY AUTOINCREMENT";
    public final static String REQUEST_ID = REQUEST_ID_FIELD + " VARCHAR(255)";
    public final static String NOME_PRATICA = NOME_PRATICA_FIELD + " VARCHAR(255)";
    public final static String JSON_DATA = JSON_DATA_FIELD + " TEXT";
    public final static String STEP = STEP_FIELD + " VARCHAR(50)";
    public final static String STATO = STATO_FIELD + " VARCHAR(50)";
    public final static String CREATION_DATE = CREATION_DATE_FIELD + " LAST_UPDATE BIGINT";
    public final static String LAST_UPDATE = LAST_UPDATE_FIELD + " LAST_UPDATE BIGINT";
    public final static String CODICE_DEALER = CODICE_DEALER_FIELD + " VARCHAR(50)";
    public final static String NOTE = NOTE_FIELD + " TEXT";
    public final static String LAST_UPDATE_NOTE = LAST_UPDATE_NOTE_FIELD + " LAST_UPDATE BIGINT";

    public final static String PERCENTUALE_COMPLET = PERCENTUALE_COMPLET_FIELD + " INTEGER";
    public final static String IS_NOME_EDITABLE = IS_NOME_EDITABLE_FIELD + " INTEGER";
    public final static String PIVA = PIVA_FIELD + " VARCHAR(255)";

    private static String TAG = "PraticheDAO";

    public final static List<String> fields = new ArrayList<String>();


    static {
        fields.add(ID_PRATICA);
        fields.add(REQUEST_ID);
        fields.add(NOME_PRATICA);
        fields.add(JSON_DATA);
        fields.add(STEP);
        fields.add(STATO);
        fields.add(CREATION_DATE);
        fields.add(LAST_UPDATE);
        fields.add(CODICE_DEALER);
        fields.add(NOTE);
        fields.add(LAST_UPDATE_NOTE);
        fields.add(PERCENTUALE_COMPLET);
        fields.add(IS_NOME_EDITABLE);
        fields.add(PIVA);
    }

    public static void clearPraticheTable(Context context) {
        SQLiteDatabase db = DbHelper.getInstance(context).getWritableDatabase();
        db.execSQL("DELETE FROM " + WIND_PRATICHE_TABLE_NAME);
        db.close();
    }

    public static void updatePratica(Context context) {
        SQLiteDatabase db = DbHelper.getInstance(context).getWritableDatabase();

        //update pratica JSON data
        Session.getInstance().getPratica().setJson_data(Session.getInstance().getJsonPratica());
        Pratica pratica = Session.getInstance().getPratica();

        DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

        ContentValues values = new ContentValues();
        values.put(ID_PRATICA_FIELD, pratica.getId());
        values.put(REQUEST_ID_FIELD, pratica.getRequest_id());
        values.put(NOME_PRATICA_FIELD, pratica.getNome_pratica());
        values.put(JSON_DATA_FIELD, pratica.getJson_data().toString());
        values.put(STEP_FIELD, pratica.getStep());
        values.put(STATO_FIELD, pratica.getStato());
        values.put(CREATION_DATE_FIELD, pratica.getCreation_date().getTime());
        values.put(LAST_UPDATE_FIELD, new Date().getTime());
        values.put(CODICE_DEALER_FIELD, pratica.getCodice_dealer());
        values.put(NOTE_FIELD, pratica.getNote());
        values.put(LAST_UPDATE_NOTE_FIELD, pratica.getLast_update_note().getTime());
        values.put(PERCENTUALE_COMPLET_FIELD, pratica.getPercentualeCompletamento());
        values.put(IS_NOME_EDITABLE_FIELD, (pratica.isNomeEditable() == true ? 1 : 0)); //1 = true
        values.put(PIVA_FIELD, pratica.getP_iva());


        db.replace(WIND_PRATICHE_TABLE_NAME, null, values);
        db.close();

    }

    public static void updateNomePratica(Context context, int idPratica, String nomePratica) {

        SQLiteDatabase db = DbHelper.getInstance(context).getWritableDatabase();

        try {
            String strSQL = "UPDATE " + WIND_PRATICHE_TABLE_NAME + " SET " + NOME_PRATICA_FIELD + " =\"" + nomePratica + "\" WHERE ID_PRATICA = " + idPratica;
            db.execSQL(strSQL);

            db.close();
        } catch (Exception e) {
            db.close();
            Log.i(TAG, "eccezione aggiornamento nome pratica " + e.getMessage());
        }
    }

    public static void setNomeNotEditable(Context context, int idPratica) {

        SQLiteDatabase db = DbHelper.getInstance(context).getWritableDatabase();

        try {
            String strSQL = "UPDATE " + WIND_PRATICHE_TABLE_NAME + " SET " + IS_NOME_EDITABLE_FIELD + " =0 WHERE ID_PRATICA = " + idPratica;
            db.execSQL(strSQL);

            db.close();
        } catch (Exception e) {
            db.close();
            Log.i(TAG, "eccezione aggiornamento campo editable pratica" + e.getMessage());
        }
    }

    public static Pratica createPratica(Context context, JSONObject jsonData, String dealer) {
        SQLiteDatabase db = DbHelper.getInstance(context).getWritableDatabase();

        DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

        Date nowDate = new Date();
        long now = nowDate.getTime();
        ContentValues values = new ContentValues();
//        values.put(ID_PRATICA, pratica.getId()); not set because autoincrement on pk
        values.put(REQUEST_ID_FIELD, "");
        values.put(NOME_PRATICA_FIELD, "Pratica N°: ");
        values.put(JSON_DATA_FIELD, jsonData.toString());
        values.put(STEP_FIELD, "");
        values.put(STATO_FIELD, "BOZZA");
        values.put(CREATION_DATE_FIELD, now);
        values.put(LAST_UPDATE_FIELD, now);
        values.put(CODICE_DEALER_FIELD, dealer);
        values.put(NOTE_FIELD, "");
        values.put(LAST_UPDATE_NOTE_FIELD, now);
        values.put(PIVA_FIELD, "");

        values.put(PERCENTUALE_COMPLET_FIELD, 10);
        values.put(IS_NOME_EDITABLE_FIELD, 1);


        Pratica result = new Pratica();

        result.setJson_data(jsonData);
        result.setLast_update_note(nowDate);
        result.setCodice_dealer(dealer);
        result.setLast_update(nowDate);
        result.setCreation_date(nowDate);
        result.setIsNomeEditable(true);
        result.setPercentualeCompletamento(10);
        result.setStato("BOZZA");
        result.setP_iva("");

        long index = db.insert(WIND_PRATICHE_TABLE_NAME, null, values);

        result.setId((int) index);
        result.setNome_pratica("Pratica N°: " + index);

        String strSQL = "UPDATE " + WIND_PRATICHE_TABLE_NAME + " SET " + NOME_PRATICA_FIELD + " = \"Pratica N°: " + index + "\" WHERE ID_PRATICA = " + index;
        db.execSQL(strSQL);

        db.close();

        Log.i(TAG, "creata pratica " + index);
        logPratica(result);

        return result;
    }

    public static int getCountBozze(Context context) {
        SQLiteDatabase db = DbHelper.getInstance(context).getWritableDatabase();
        String query = "SELECT count(*) FROM " + WIND_PRATICHE_TABLE_NAME;
        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();
        int count = cursor.getInt(0);
        cursor.close();
        return count;
    }

    public static ArrayList<Pratica> retrievePratiche(Context context) {
        SQLiteDatabase db = DbHelper.getInstance(context).getWritableDatabase();

        String query = "SELECT * FROM " + WIND_PRATICHE_TABLE_NAME + " ORDER BY LAST_UPDATE DESC";

        Cursor cursor = db.rawQuery(query, null);

        ArrayList<Pratica> listaPratiche = new ArrayList<>();

        Log.i(TAG, "lettura lista pratiche");

        while (cursor.moveToNext()) {

            Pratica pratica = new Pratica();
            try {
                pratica.setId(cursor.getInt(cursor.getColumnIndex(ID_PRATICA_FIELD)));
                pratica.setRequest_id(cursor.getString(cursor.getColumnIndex(REQUEST_ID_FIELD)));
                pratica.setNome_pratica(cursor.getString(cursor.getColumnIndex(NOME_PRATICA_FIELD)));
                pratica.setJson_data(new JSONObject(cursor.getString(cursor.getColumnIndex(JSON_DATA_FIELD))));
                pratica.setStep(cursor.getString(cursor.getColumnIndex(STEP_FIELD)));
                pratica.setStato(cursor.getString(cursor.getColumnIndex(STATO_FIELD)));
                DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
                pratica.setCreation_date(new Date(cursor.getLong(cursor.getColumnIndex(CREATION_DATE_FIELD))));
                pratica.setLast_update(new Date(cursor.getLong(cursor.getColumnIndex(LAST_UPDATE_FIELD))));
                pratica.setCodice_dealer(cursor.getString(cursor.getColumnIndex(CODICE_DEALER_FIELD)));
                pratica.setNote(cursor.getString(cursor.getColumnIndex(NOTE_FIELD)));
                pratica.setLast_update_note(new Date(cursor.getLong(cursor.getColumnIndex(LAST_UPDATE_NOTE_FIELD))));

                pratica.setIsNomeEditable((cursor.getInt(cursor.getColumnIndex(IS_NOME_EDITABLE_FIELD))) == 1 ? true : false);
                pratica.setPercentualeCompletamento((cursor.getInt(cursor.getColumnIndex(PERCENTUALE_COMPLET_FIELD))));
                pratica.setP_iva(cursor.getString(cursor.getColumnIndex(PIVA_FIELD)));


                logPratica(pratica);

                if (pratica.getId() != 0) {
                    listaPratiche.add(pratica);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        cursor.close();
        db.close();
        return listaPratiche;
    }

    private static void logPratica(Pratica pratica) {
        Log.i(TAG, "pratica " + pratica.getId());
        Log.i(TAG, "dealer " + pratica.getCodice_dealer());
        Log.i(TAG, "stato " + pratica.getStato());
        Log.i(TAG, "last update " + pratica.getLast_update());
        Log.i(TAG, "nome " + pratica.getNome_pratica());
        Log.i(TAG, "note " + pratica.getNote());
        Log.i(TAG, "req id " + pratica.getRequest_id());
        Log.i(TAG, "step " + pratica.getStep());
        Log.i(TAG, "creat date " + pratica.getCreation_date());
        Log.i(TAG, "last up note " + pratica.getLast_update_note());

        Log.i(TAG, "perc comple " + pratica.getPercentualeCompletamento());
        Log.i(TAG, "is nome edit " + (pratica.isNomeEditable() == true ? 1 : 0));
        Log.i(TAG, "piva " + pratica.getP_iva());
    }

}
