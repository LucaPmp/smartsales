package it.wind.smartsales.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import it.wind.smartsales.entities.OffertaMobile;

/**
 * Created by luca.quaranta on 22/03/2016.
 */
public class CatalogoOfferteMobileDAO {

    public final static String WIND_CATALOGO_OFFERTE_MOBILE_TABLE_NAME = "CATALOGO_OFFERTE_MOBILE";

    public final static String ID_OFFERTA_FIELD = "ID_OFFERTA";
    public final static String OFFER_DESCRIPTION_FIELD = "OFFER_DESCRIPTION";
    public final static String OFFER_DESCRIPTION_UPPER_FIELD = "OFFER_DESCRIPTION_UPPER";
    public final static String OFFER_DESCRIPTION_I_BUTTON_FIELD = "OFFER_DESCRIPTION_I_BUTTON";
    public final static String OFFER_TYPE_FIELD = "OFFER_TYPE";
    public final static String CANONE_MENSILE_FIELD = "CANONE_MENSILE";
    public final static String CANONE_MENSILE_CONV_FIELD = "CANONE_MENSILE_CONV";
    public final static String CANONE_PROMO_FIELD = "CANONE_PROMO";
    public final static String CALL_AZ_ITA_FIELD = "CALL_AZ_ITA";
    public final static String CALL_ITA_FIELD = "CALL_ITA";
    public final static String SMS_ITA_FIELD = "SMS_ITA";
    public final static String DATI_ITA_FIELD = "DATI_ITA";
    public final static String CALL_EST_FIELD = "CALL_EST";
    public final static String SMS_EST_FIELD = "SMS_EST";
    public final static String DATI_EST_FIELD = "DATI_EST";
    public final static String FASCIA_VOCE_FIELD = "FASCIA_VOCE";
    public final static String FASCIA_DATI_FIELD = "FASCIA_DATI";

    public final static String ID_OFFERTA = ID_OFFERTA_FIELD + " INTEGER PRIMARY KEY";
    public final static String OFFER_DESCRIPTION = OFFER_DESCRIPTION_FIELD + " VARCHAR(255)";
    public final static String OFFER_DESCRIPTION_UPPER = OFFER_DESCRIPTION_UPPER_FIELD + " VARCHAR(255)";
    public final static String OFFER_DESCRIPTION_I_BUTTON = OFFER_DESCRIPTION_I_BUTTON_FIELD + " VARCHAR(255)";
    public final static String OFFER_TYPE = OFFER_TYPE_FIELD + " VARCHAR(50)";
    public final static String CANONE_BIMESTRALE = CANONE_MENSILE_FIELD + " VARCHAR(50)";
    public final static String CANONE_BIMESTRALE_CONV = CANONE_MENSILE_CONV_FIELD + " VARCHAR(50)";
    public final static String CANONE_PROMO = CANONE_PROMO_FIELD + " VARCHAR(50)";
    public final static String CALL_AZ_ITA = CALL_AZ_ITA_FIELD + " VARCHAR(50)";
    public final static String CALL_ITA = CALL_ITA_FIELD + " VARCHAR(50)";
    public final static String SMS_ITA = SMS_ITA_FIELD + " VARCHAR(50)";
    public final static String DATI_ITA = DATI_ITA_FIELD + " VARCHAR(50)";
    public final static String CALL_EST = CALL_EST_FIELD + " VARCHAR(50)";
    public final static String SMS_EST = SMS_EST_FIELD + " VARCHAR(50)";
    public final static String DATI_EST = DATI_EST_FIELD + " VARCHAR(50)";
    public final static String FASCIA_VOCE = FASCIA_VOCE_FIELD + " VARCHAR(255)";
    public final static String FASCIA_DATI = FASCIA_DATI_FIELD + " VARCHAR(255)";

    public final static List<String> fields = new ArrayList<String>();

    static {
        fields.add(ID_OFFERTA);
        fields.add(OFFER_DESCRIPTION);
        fields.add(OFFER_DESCRIPTION_UPPER);
        fields.add(OFFER_DESCRIPTION_I_BUTTON);
        fields.add(OFFER_TYPE);
        fields.add(CANONE_BIMESTRALE);
        fields.add(CANONE_BIMESTRALE_CONV);
        fields.add(CANONE_PROMO);
        fields.add(CALL_AZ_ITA);
        fields.add(CALL_ITA);
        fields.add(SMS_ITA);
        fields.add(DATI_ITA);
        fields.add(CALL_EST);
        fields.add(SMS_EST);
        fields.add(DATI_EST);
        fields.add(FASCIA_VOCE);
        fields.add(FASCIA_DATI);
    }

    public static void clearOffertaMobileTable(Context context){
        SQLiteDatabase db = DbHelper.getInstance(context).getWritableDatabase();
        db.execSQL("DELETE FROM "+ WIND_CATALOGO_OFFERTE_MOBILE_TABLE_NAME);
        db.close();
    }

    public static void insertOffertaMobile(Context context, OffertaMobile offertaMobile){
        SQLiteDatabase db = DbHelper.getInstance(context).getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(ID_OFFERTA_FIELD, offertaMobile.getIdOfferta());
        values.put(OFFER_DESCRIPTION_FIELD, offertaMobile.getOfferDescription());
        values.put(OFFER_DESCRIPTION_UPPER_FIELD, offertaMobile.getOfferDescriptionUpper());
        values.put(OFFER_DESCRIPTION_I_BUTTON_FIELD, offertaMobile.getOfferDescriptionIButton());
        values.put(OFFER_TYPE_FIELD, offertaMobile.getOfferType());
        values.put(CANONE_MENSILE_FIELD, offertaMobile.getCanoneMensile());
        values.put(CANONE_MENSILE_CONV_FIELD, offertaMobile.getCanoneMensileConv());
        values.put(CANONE_PROMO_FIELD, offertaMobile.getCanonePromo());
        values.put(CALL_AZ_ITA_FIELD, offertaMobile.getCallAzIta());
        values.put(CALL_ITA_FIELD, offertaMobile.getCallIta());
        values.put(SMS_ITA_FIELD, offertaMobile.getSmsIta());
        values.put(DATI_ITA_FIELD, offertaMobile.getDatiIta());
        values.put(CALL_EST_FIELD, offertaMobile.getCallEst());
        values.put(SMS_EST_FIELD, offertaMobile.getSmsEst());
        values.put(DATI_EST_FIELD, offertaMobile.getDatiEst());
        values.put(FASCIA_VOCE_FIELD, offertaMobile.getFasciaVoce());
        values.put(FASCIA_DATI_FIELD, offertaMobile.getFasciaDati());

        db.insert(WIND_CATALOGO_OFFERTE_MOBILE_TABLE_NAME, null, values);
        db.close();
    }

    public static OffertaMobile retrieveOffertaMobile(Context context, String idOfferta){
        SQLiteDatabase db = DbHelper.getInstance(context).getWritableDatabase();

        String query = "SELECT " +
                ID_OFFERTA_FIELD + ", " +
                OFFER_DESCRIPTION_FIELD + ", " +
                OFFER_DESCRIPTION_UPPER_FIELD + ", " +
                OFFER_DESCRIPTION_I_BUTTON_FIELD + ", " +
                OFFER_TYPE_FIELD + ", " +
                CANONE_MENSILE_FIELD + ", " +
                CANONE_MENSILE_CONV_FIELD + ", " +
                CANONE_PROMO_FIELD + ", " +
                CALL_AZ_ITA_FIELD + ", " +
                CALL_ITA_FIELD + ", " +
                SMS_ITA_FIELD + ", " +
                DATI_ITA_FIELD + ", " +
                CALL_EST_FIELD + ", " +
                SMS_EST_FIELD + ", " +
                DATI_EST_FIELD + ", " +
                FASCIA_VOCE_FIELD + ", " +
                FASCIA_DATI_FIELD +
                " FROM " + WIND_CATALOGO_OFFERTE_MOBILE_TABLE_NAME + " WHERE " + ID_OFFERTA_FIELD + " = '" + idOfferta + "'";
        Cursor cursor = db.rawQuery(query, null);

        OffertaMobile offertaMobile = new OffertaMobile();
        if (cursor.moveToFirst()) {
            offertaMobile.setIdOfferta(cursor.getInt(cursor.getColumnIndex(ID_OFFERTA_FIELD)));
            offertaMobile.setOfferDescription(cursor.getString(cursor.getColumnIndex(OFFER_DESCRIPTION_FIELD)));
            offertaMobile.setOfferDescriptionUpper(cursor.getString(cursor.getColumnIndex(OFFER_DESCRIPTION_UPPER_FIELD)));
            offertaMobile.setOfferDescriptionIButton(cursor.getString(cursor.getColumnIndex(OFFER_DESCRIPTION_I_BUTTON_FIELD)));
            offertaMobile.setOfferType(cursor.getString(cursor.getColumnIndex(OFFER_TYPE_FIELD)));
            offertaMobile.setCanoneMensile(cursor.getString(cursor.getColumnIndex(CANONE_MENSILE_FIELD)));
            offertaMobile.setCanoneMensileConv(cursor.getString(cursor.getColumnIndex(CANONE_MENSILE_CONV_FIELD)));
            offertaMobile.setCanonePromo(cursor.getString(cursor.getColumnIndex(CANONE_PROMO_FIELD)));
            offertaMobile.setCallAzIta(cursor.getString(cursor.getColumnIndex(CALL_AZ_ITA_FIELD)));
            offertaMobile.setCallIta(cursor.getString(cursor.getColumnIndex(CALL_ITA_FIELD)));
            offertaMobile.setSmsIta(cursor.getString(cursor.getColumnIndex(SMS_ITA_FIELD)));
            offertaMobile.setDatiIta(cursor.getString(cursor.getColumnIndex(DATI_ITA_FIELD)));
            offertaMobile.setCallEst(cursor.getString(cursor.getColumnIndex(CALL_EST_FIELD)));
            offertaMobile.setSmsEst(cursor.getString(cursor.getColumnIndex(SMS_EST_FIELD)));
            offertaMobile.setDatiEst(cursor.getString(cursor.getColumnIndex(DATI_EST_FIELD)));
            offertaMobile.setFasciaVoce(cursor.getString(cursor.getColumnIndex(FASCIA_VOCE_FIELD)));
            offertaMobile.setFasciaDati(cursor.getString(cursor.getColumnIndex(FASCIA_DATI_FIELD)));
        }

        cursor.close();
        db.close();
        return offertaMobile;
    }

}
