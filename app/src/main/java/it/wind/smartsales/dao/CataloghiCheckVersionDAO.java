package it.wind.smartsales.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import it.wind.smartsales.entities.InfoCatalog;

/**
 * Created by luca.quaranta on 22/03/2016.
 */
public class CataloghiCheckVersionDAO {

    public final static String WIND_CATALOGHI_CHECK_VERSION_TABLE_NAME = "CATALOGHI_CHECK_VERSION";

    public final static String CATALOG_IDENTIFIER_FIELD = "CATALOG_IDENTIFIER";
    public final static String CATALOG_VERSION_FIELD = "CATALOG_VERSION";
    public final static String CATALOG_LAST_UPDATE_FIELD = "CATALOG_LAST_UPDATE";

    public final static String CATALOG_IDENTIFIER = CATALOG_IDENTIFIER_FIELD + " INTEGER PRIMARY KEY";
    public final static String CATALOG_VERSION = CATALOG_VERSION_FIELD + " VARCHAR(50)";
    public final static String CATALOG_LAST_UPDATE = CATALOG_LAST_UPDATE_FIELD + " VARCHAR(50)";

    public final static List<String> fields = new ArrayList<String>();

    static {
        fields.add(CATALOG_IDENTIFIER);
        fields.add(CATALOG_VERSION);
        fields.add(CATALOG_LAST_UPDATE);
    }

    public static String retrieveCatalogLastUpdateFromID(Context context, InfoCatalog infoCatalog) {
        SQLiteDatabase db = DbHelper.getInstance(context).getWritableDatabase();

        String query = "SELECT " + CATALOG_LAST_UPDATE_FIELD + " FROM " + WIND_CATALOGHI_CHECK_VERSION_TABLE_NAME + " WHERE " + CATALOG_IDENTIFIER_FIELD + " = " + infoCatalog.getCatalogIdentifier();
        Cursor cursor = db.rawQuery(query, null);
        String lastUpdate = "";

        if (cursor.moveToFirst()) {
            lastUpdate = cursor.getString(cursor.getColumnIndex(CATALOG_LAST_UPDATE_FIELD));
        }
        cursor.close();
        db.close();
        return lastUpdate;
    }

    public static void insertCatalogCheckVersion(Context context, int catalogIdentifier) {
        SQLiteDatabase db = DbHelper.getInstance(context).getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(CATALOG_IDENTIFIER_FIELD, catalogIdentifier);
        values.put(CATALOG_VERSION_FIELD, "");
        values.put(CATALOG_LAST_UPDATE_FIELD, "19700101");

        db.insert(WIND_CATALOGHI_CHECK_VERSION_TABLE_NAME, null, values);
        db.close();
    }

    public static void updateCatalogCheckVersion(Context context, InfoCatalog infoCatalog) {
        SQLiteDatabase db = DbHelper.getInstance(context).getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(CATALOG_VERSION_FIELD, infoCatalog.getCatalogVersion());
        values.put(CATALOG_LAST_UPDATE_FIELD, infoCatalog.getCatalogLastUpdate());

        db.update(WIND_CATALOGHI_CHECK_VERSION_TABLE_NAME, values, CATALOG_IDENTIFIER_FIELD + "=" + infoCatalog.getCatalogIdentifier(), null);
        db.close();
    }
}
