package it.wind.smartsales.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import it.wind.smartsales.entities.Question;

/**
 * Created by luca.quaranta on 22/03/2016.
 */
public class CatalogoQuestionarioDAO {

    public final static String WIND_CATALOGO_QUESTIONARIO_TABLE_NAME = "CATALOGO_QUESTIONARIO";

    public final static String QUESTION_CATEGORY_FIELD = "QUESTION_CATEGORY";
    public final static String QUESTION_NUMBER_FIELD = "QUESTION_NUMBER";
    public final static String QUESTION_DESCRIPTION_FIELD = "QUESTION_DESCRIPTION";

    public final static String QUESTION_CATEGORY = QUESTION_CATEGORY_FIELD + " VARCHAR(50)";
    public final static String QUESTION_NUMBER = QUESTION_NUMBER_FIELD + " VARCHAR(50)";
    public final static String QUESTION_DESCRIPTION = QUESTION_DESCRIPTION_FIELD + " VARCHAR(255)";

    public final static List<String> fields = new ArrayList<String>();

    static {
        fields.add(QUESTION_CATEGORY);
        fields.add(QUESTION_NUMBER);
        fields.add(QUESTION_DESCRIPTION);
    }

    public static void clearQuestionarioTable(Context context){
        SQLiteDatabase db = DbHelper.getInstance(context).getWritableDatabase();
        db.execSQL("DELETE FROM "+ WIND_CATALOGO_QUESTIONARIO_TABLE_NAME);
        db.close();
    }

    public static void insertQuestion(Context context, Question question){
        SQLiteDatabase db = DbHelper.getInstance(context).getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(QUESTION_CATEGORY_FIELD, question.getQuestionCategory());
        values.put(QUESTION_NUMBER_FIELD, question.getQuestionNumber());
        values.put(QUESTION_DESCRIPTION_FIELD, question.getQuestionDescription());

        db.insert(WIND_CATALOGO_QUESTIONARIO_TABLE_NAME, null, values);
        db.close();
    }

    public static ArrayList<Question> retrieveQuestions(Context context, String questionCategory){
        SQLiteDatabase db = DbHelper.getInstance(context).getWritableDatabase();

        String query = "SELECT " + QUESTION_NUMBER_FIELD + ", " + QUESTION_DESCRIPTION_FIELD + " FROM " + WIND_CATALOGO_QUESTIONARIO_TABLE_NAME + " WHERE UPPER(" + QUESTION_CATEGORY_FIELD + ") = '" + questionCategory.toUpperCase() + "'";
        Cursor cursor = db.rawQuery(query, null);
        String lastUpdate = "";

        ArrayList<Question> questions = new ArrayList<>();
        if (cursor.moveToFirst()) {
            do {
                Question question = new Question();
                question.setQuestionNumber(cursor.getString(cursor.getColumnIndex(QUESTION_NUMBER_FIELD)));
                question.setQuestionDescription(cursor.getString(cursor.getColumnIndex(QUESTION_DESCRIPTION_FIELD)));
                questions.add(question);
            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();
        return questions;
    }

}
