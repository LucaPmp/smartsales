package it.wind.smartsales.dao;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Environment;
import android.text.TextUtils;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import it.wind.smartsales.commons.Log;
import it.wind.smartsales.commons.Utils;
import it.wind.smartsales.entities.OffertaMobile;
import it.wind.smartsales.entities.Session;

/**
 * Created by luca.quaranta on 23/02/2016.
 */
public class DbHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 5;
    private static final String DATABASE_NAME = "SmartSalesDatabase";
    private static final String DATABASE_ENCRYPTION_KEY = "SecretKey";

    private static DbHelper mInstance;

    public static String dropTable(String table) {
        return "DROP TABLE IF EXISTS  " + table;
    }

    public static final String WIND_CATALOGO_OFFERTE_FISSO_TABLE_CREATE = "CREATE TABLE "
            + CatalogoOfferteFissoDAO.WIND_CATALOGO_OFFERTE_FISSO_TABLE_NAME + " ("
            + formatFields(CatalogoOfferteFissoDAO.fields) + ");";

    public static final String WIND_CATALOGO_OFFERTE_MOBILE_TABLE_CREATE = "CREATE TABLE "
            + CatalogoOfferteMobileDAO.WIND_CATALOGO_OFFERTE_MOBILE_TABLE_NAME + " ("
            + formatFields(CatalogoOfferteMobileDAO.fields) + ");";

    public static final String WIND_CATALOGO_OPZIONI_FISSO_TABLE_CREATE = "CREATE TABLE "
            + CatalogoOpzioniFissoDAO.WIND_CATALOGO_OPZIONI_FISSO_TABLE_NAME + " ("
            + formatFields(CatalogoOpzioniFissoDAO.fields) + ");";

    public static final String WIND_CATALOGO_OPZIONI_MOBILE_TABLE_CREATE = "CREATE TABLE "
            + CatalogoOpzioniMobileDAO.WIND_CATALOGO_OPZIONI_MOBILE_TABLE_NAME + " ("
            + formatFields(CatalogoOpzioniMobileDAO.fields) + ");";

    public static final String WIND_CATALOGO_ADD_ON_MOBILE_TABLE_CREATE = "CREATE TABLE "
            + CatalogoAddOnMobileDAO.WIND_CATALOGO_ADD_ON_MOBILE_TABLE_NAME + " ("
            + formatFields(CatalogoAddOnMobileDAO.fields) + ");";

    public static final String WIND_CATALOGHI_CHECK_VERSION_TABLE_CREATE = "CREATE TABLE "
            + CataloghiCheckVersionDAO.WIND_CATALOGHI_CHECK_VERSION_TABLE_NAME + " ("
            + formatFields(CataloghiCheckVersionDAO.fields) + ");";

    public static final String WIND_CATALOGO_FASCE_SCONTI_TABLE_CREATE = "CREATE TABLE "
            + CatalogoFasceScontiDAO.WIND_CATALOGO_FASCE_SCONTI_TABLE_NAME + " ("
            + formatFields(CatalogoFasceScontiDAO.fields) + ");";

    public static final String WIND_CATALOGO_PIANI_TARIFFARI_TABLE_CREATE = "CREATE TABLE "
            + CatalogoPianiTariffariDAO.WIND_CATALOGO_PIANI_TARIFFARI_TABLE_NAME + " ("
            + formatFields(CatalogoPianiTariffariDAO.fields) + ");";

    public static final String WIND_CATALOGO_FASCE_SCONTI_RICARICABILI_TABLE_CREATE = "CREATE TABLE "
            + CatalogoFasceScontiRicaricabiliDAO.WIND_CATALOGO_FASCE_SCONTI_RICARICABILI_TABLE_NAME + " ("
            + formatFields(CatalogoFasceScontiRicaricabiliDAO.fields) + ");";

    public static final String WIND_CATALOGO_TERMINALI_TABLE_CREATE = "CREATE TABLE "
            + CatalogoTerminaliDAO.WIND_CATALOGO_TERMINALI_TABLE_NAME + " ("
            + formatFields(CatalogoTerminaliDAO.fields) + ");";

    public static final String WIND_CATALOGO_QUESTIONARIO_TABLE_CREATE = "CREATE TABLE "
            + CatalogoQuestionarioDAO.WIND_CATALOGO_QUESTIONARIO_TABLE_NAME + " ("
            + formatFields(CatalogoQuestionarioDAO.fields) + ");";


    public static final String WIND_CATALOGO_PRATICA_TABLE_CREATE = "CREATE TABLE "
            + PraticheDAO.WIND_PRATICHE_TABLE_NAME + " ("
            + formatFields(PraticheDAO.fields) + ");";

    public static final String WIND_EXTRA_GIGA_TABLE_CREATE = "CREATE TABLE "
            + ExtraGigaValueDAO.WIND_EXTRA_GIGA_TABLE_NAME + " ("
            + formatFields(ExtraGigaValueDAO.fields) + ");";


    public static String formatFields(List<String> fields) {
        StringBuilder result = new StringBuilder();
        Iterator<String> it = fields.iterator();
        while (it.hasNext()) {
            result.append(it.next());
            if (it.hasNext()) {
                result.append(", ");
            }
        }
        return result.toString();
    }

    public static DbHelper getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new DbHelper(context);
        }
        return mInstance;
    }

//    @Override
//    public void onConfigure(SQLiteDatabase db) {
//        db.execSQL("PRAGMA key = '" + DATABASE_ENCRYPTION_KEY + "'");
//    }

    private DbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        System.out.println("****************" + context.getDatabasePath(DATABASE_NAME));
        System.out.println("****************" + context.getDatabasePath(DATABASE_NAME).getAbsolutePath());
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(WIND_CATALOGO_OFFERTE_FISSO_TABLE_CREATE);
        db.execSQL(WIND_CATALOGO_OFFERTE_MOBILE_TABLE_CREATE);
        db.execSQL(WIND_CATALOGO_OPZIONI_FISSO_TABLE_CREATE);
        db.execSQL(WIND_CATALOGO_OPZIONI_MOBILE_TABLE_CREATE);
        db.execSQL(WIND_CATALOGO_ADD_ON_MOBILE_TABLE_CREATE);
        db.execSQL(WIND_CATALOGHI_CHECK_VERSION_TABLE_CREATE);
        db.execSQL(WIND_CATALOGO_FASCE_SCONTI_TABLE_CREATE);
        db.execSQL(WIND_CATALOGO_FASCE_SCONTI_RICARICABILI_TABLE_CREATE);
        db.execSQL(WIND_CATALOGO_PIANI_TARIFFARI_TABLE_CREATE);
        db.execSQL(WIND_CATALOGO_TERMINALI_TABLE_CREATE);
        db.execSQL(WIND_CATALOGO_QUESTIONARIO_TABLE_CREATE);
        db.execSQL(WIND_CATALOGO_PRATICA_TABLE_CREATE);
        db.execSQL(WIND_EXTRA_GIGA_TABLE_CREATE);


        Log.d("CREATE SCRIPTS", WIND_CATALOGO_OFFERTE_FISSO_TABLE_CREATE);
        Log.d("CREATE SCRIPTS", WIND_CATALOGO_OFFERTE_MOBILE_TABLE_CREATE);
        Log.d("CREATE SCRIPTS", WIND_CATALOGO_OPZIONI_FISSO_TABLE_CREATE);
        Log.d("CREATE SCRIPTS", WIND_CATALOGO_OPZIONI_MOBILE_TABLE_CREATE);
        Log.d("CREATE SCRIPTS", WIND_CATALOGO_ADD_ON_MOBILE_TABLE_CREATE);
        Log.d("CREATE SCRIPTS", WIND_CATALOGHI_CHECK_VERSION_TABLE_CREATE);
        Log.d("CREATE SCRIPTS", WIND_CATALOGO_FASCE_SCONTI_TABLE_CREATE);
        Log.d("CREATE SCRIPTS", WIND_CATALOGO_FASCE_SCONTI_RICARICABILI_TABLE_CREATE);
        Log.d("CREATE SCRIPTS", WIND_CATALOGO_PIANI_TARIFFARI_TABLE_CREATE);
        Log.d("CREATE SCRIPTS", WIND_CATALOGO_TERMINALI_TABLE_CREATE);
        Log.d("CREATE SCRIPTS", WIND_CATALOGO_QUESTIONARIO_TABLE_CREATE);
        Log.d("CREATE SCRIPTS", WIND_CATALOGO_PRATICA_TABLE_CREATE);
        Log.d("CREATE SCRIPTS", WIND_EXTRA_GIGA_TABLE_CREATE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(dropTable(CatalogoOfferteFissoDAO.WIND_CATALOGO_OFFERTE_FISSO_TABLE_NAME));
        db.execSQL(dropTable(CatalogoOfferteMobileDAO.WIND_CATALOGO_OFFERTE_MOBILE_TABLE_NAME));
        db.execSQL(dropTable(CatalogoOpzioniFissoDAO.WIND_CATALOGO_OPZIONI_FISSO_TABLE_NAME));
        db.execSQL(dropTable(CatalogoOpzioniMobileDAO.WIND_CATALOGO_OPZIONI_MOBILE_TABLE_NAME));
        db.execSQL(dropTable(CatalogoAddOnMobileDAO.WIND_CATALOGO_ADD_ON_MOBILE_TABLE_NAME));
        db.execSQL(dropTable(CataloghiCheckVersionDAO.WIND_CATALOGHI_CHECK_VERSION_TABLE_NAME));
        db.execSQL(dropTable(CatalogoFasceScontiDAO.WIND_CATALOGO_FASCE_SCONTI_TABLE_NAME));
        db.execSQL(dropTable(CatalogoFasceScontiRicaricabiliDAO.WIND_CATALOGO_FASCE_SCONTI_RICARICABILI_TABLE_NAME));
        db.execSQL(dropTable(CatalogoPianiTariffariDAO.WIND_CATALOGO_PIANI_TARIFFARI_TABLE_NAME));
        db.execSQL(dropTable(CatalogoTerminaliDAO.WIND_CATALOGO_TERMINALI_TABLE_NAME));
        db.execSQL(dropTable(CatalogoQuestionarioDAO.WIND_CATALOGO_QUESTIONARIO_TABLE_NAME));
        db.execSQL(dropTable(PraticheDAO.WIND_PRATICHE_TABLE_NAME));
        db.execSQL(dropTable(ExtraGigaValueDAO.WIND_EXTRA_GIGA_TABLE_NAME));

        onCreate(db);

    }

    public void extractDB(Context context) {
        try {
            File sd = Environment.getExternalStorageDirectory();
            File data = context.getDatabasePath(DATABASE_NAME);

            if (sd.canWrite()) {
                String currentDBPath = "/data/" + context.getPackageName() + "/databases/SmartSalesDB";
                String backupDBPath = DATABASE_NAME;
                File currentDB = new File(data.getPath());
                Log.d("DB CPATH", currentDBPath);
                Log.d("DB PATH", data.getPath());
                File backupDB = new File(sd, backupDBPath);

                if (currentDB.exists()) {
                    FileChannel src = new FileInputStream(currentDB).getChannel();
                    FileChannel dst = new FileOutputStream(backupDB).getChannel();
                    dst.transferFrom(src, 0, src.size());
                    src.close();
                    dst.close();
                    Toast.makeText(context, "DB Exported!", Toast.LENGTH_LONG).show();
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static ArrayList<OffertaMobile> retrieveAllOfferteMobile(Context context, int numPacchetto, String offerType) {
        SQLiteDatabase db = DbHelper.getInstance(context).getWritableDatabase();

        JSONArray pacchetti = null;
        JSONObject pacchetto = null;
        try {
            pacchetti = (JSONArray) Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONArray("pacchetti");
            pacchetto = pacchetti.getJSONObject(numPacchetto - 1);

            String query = "";


            Cursor cursor = null;

            ArrayList<OffertaMobile> offerteMobile = new ArrayList<>();

            if (offerteMobile.isEmpty()) {
                query = "SELECT " +
                        CatalogoOfferteMobileDAO.ID_OFFERTA_FIELD + ", " +
                        CatalogoOfferteMobileDAO.OFFER_DESCRIPTION_FIELD + ", " +
                        CatalogoOfferteMobileDAO.OFFER_DESCRIPTION_UPPER_FIELD + ", " +
                        CatalogoOfferteMobileDAO.OFFER_DESCRIPTION_I_BUTTON_FIELD + ", " +
                        CatalogoOfferteMobileDAO.OFFER_TYPE_FIELD + ", " +
                        CatalogoOfferteMobileDAO.CANONE_MENSILE_FIELD + ", " +
                        CatalogoOfferteMobileDAO.CANONE_MENSILE_CONV_FIELD + ", " +
                        CatalogoOfferteMobileDAO.CANONE_PROMO_FIELD + ", " +
                        CatalogoOfferteMobileDAO.CALL_AZ_ITA_FIELD + ", " +
                        CatalogoOfferteMobileDAO.CALL_ITA_FIELD + ", " +
                        CatalogoOfferteMobileDAO.SMS_ITA_FIELD + ", " +
                        CatalogoOfferteMobileDAO.DATI_ITA_FIELD + ", " +
                        CatalogoOfferteMobileDAO.CALL_EST_FIELD + ", " +
                        CatalogoOfferteMobileDAO.SMS_EST_FIELD + ", " +
                        CatalogoOfferteMobileDAO.DATI_EST_FIELD + ", " +
                        CatalogoOfferteMobileDAO.FASCIA_VOCE_FIELD + ", " +
                        CatalogoOfferteMobileDAO.FASCIA_DATI_FIELD +
                        " FROM " + CatalogoOfferteMobileDAO.WIND_CATALOGO_OFFERTE_MOBILE_TABLE_NAME +
                        " WHERE " + "UPPER(" + CatalogoOfferteMobileDAO.OFFER_TYPE_FIELD + ") = '" + offerType.toUpperCase() + "'";

                query = query + " order by CAST(" + CatalogoOfferteMobileDAO.CANONE_MENSILE_FIELD + " as INT) desc ";

                Log.d("QUERY TAPPO", "retrieveOfferteMobile: " + query);
                cursor = db.rawQuery(query, null);

                if (cursor.moveToFirst()) {
                    do {
                        OffertaMobile offertaMobile = new OffertaMobile();
                        offertaMobile.setIdOfferta(cursor.getInt(cursor.getColumnIndex(CatalogoOfferteMobileDAO.ID_OFFERTA_FIELD)));
                        offertaMobile.setOfferDescription(cursor.getString(cursor.getColumnIndex(CatalogoOfferteMobileDAO.OFFER_DESCRIPTION_FIELD)));
                        offertaMobile.setOfferDescriptionUpper(cursor.getString(cursor.getColumnIndex(CatalogoOfferteMobileDAO.OFFER_DESCRIPTION_UPPER_FIELD)));
                        offertaMobile.setOfferDescriptionIButton(cursor.getString(cursor.getColumnIndex(CatalogoOfferteMobileDAO.OFFER_DESCRIPTION_I_BUTTON_FIELD)));
                        offertaMobile.setOfferType(cursor.getString(cursor.getColumnIndex(CatalogoOfferteMobileDAO.OFFER_TYPE_FIELD)));
                        offertaMobile.setCanoneMensile(cursor.getString(cursor.getColumnIndex(CatalogoOfferteMobileDAO.CANONE_MENSILE_FIELD)));
                        offertaMobile.setCanoneMensileConv(cursor.getString(cursor.getColumnIndex(CatalogoOfferteMobileDAO.CANONE_MENSILE_CONV_FIELD)));
                        offertaMobile.setCanonePromo(cursor.getString(cursor.getColumnIndex(CatalogoOfferteMobileDAO.CANONE_PROMO_FIELD)));
                        offertaMobile.setCallAzIta(cursor.getString(cursor.getColumnIndex(CatalogoOfferteMobileDAO.CALL_AZ_ITA_FIELD)));
                        offertaMobile.setCallIta(cursor.getString(cursor.getColumnIndex(CatalogoOfferteMobileDAO.CALL_ITA_FIELD)));
                        offertaMobile.setSmsIta(cursor.getString(cursor.getColumnIndex(CatalogoOfferteMobileDAO.SMS_ITA_FIELD)));
                        offertaMobile.setDatiIta(cursor.getString(cursor.getColumnIndex(CatalogoOfferteMobileDAO.DATI_ITA_FIELD)));
                        offertaMobile.setCallEst(cursor.getString(cursor.getColumnIndex(CatalogoOfferteMobileDAO.CALL_EST_FIELD)));
                        offertaMobile.setSmsEst(cursor.getString(cursor.getColumnIndex(CatalogoOfferteMobileDAO.SMS_EST_FIELD)));
                        offertaMobile.setDatiEst(cursor.getString(cursor.getColumnIndex(CatalogoOfferteMobileDAO.DATI_EST_FIELD)));
                        offertaMobile.setFasciaVoce(cursor.getString(cursor.getColumnIndex(CatalogoOfferteMobileDAO.FASCIA_VOCE_FIELD)));
                        offertaMobile.setFasciaDati(cursor.getString(cursor.getColumnIndex(CatalogoOfferteMobileDAO.FASCIA_DATI_FIELD)));
                        offerteMobile.add(offertaMobile);
                    } while (cursor.moveToNext());
                }
            }


            cursor.close();
            db.close();

            return offerteMobile;

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static ArrayList<OffertaMobile> retrieveOfferteMobileFromTerminal(Context context, String offerType) {
        SQLiteDatabase db = DbHelper.getInstance(context).getWritableDatabase();
        ArrayList<OffertaMobile> offerteMobile = new ArrayList<>();

        if (offerteMobile.isEmpty()) {
            String query = "SELECT " +
                    CatalogoOfferteMobileDAO.ID_OFFERTA_FIELD + ", " +
                    CatalogoOfferteMobileDAO.OFFER_DESCRIPTION_FIELD + ", " +
                    CatalogoOfferteMobileDAO.OFFER_DESCRIPTION_UPPER_FIELD + ", " +
                    CatalogoOfferteMobileDAO.OFFER_DESCRIPTION_I_BUTTON_FIELD + ", " +
                    CatalogoOfferteMobileDAO.OFFER_TYPE_FIELD + ", " +
                    CatalogoOfferteMobileDAO.CANONE_MENSILE_FIELD + ", " +
                    CatalogoOfferteMobileDAO.CANONE_MENSILE_CONV_FIELD + ", " +
                    CatalogoOfferteMobileDAO.CANONE_PROMO_FIELD + ", " +
                    CatalogoOfferteMobileDAO.CALL_AZ_ITA_FIELD + ", " +
                    CatalogoOfferteMobileDAO.CALL_ITA_FIELD + ", " +
                    CatalogoOfferteMobileDAO.SMS_ITA_FIELD + ", " +
                    CatalogoOfferteMobileDAO.DATI_ITA_FIELD + ", " +
                    CatalogoOfferteMobileDAO.CALL_EST_FIELD + ", " +
                    CatalogoOfferteMobileDAO.SMS_EST_FIELD + ", " +
                    CatalogoOfferteMobileDAO.DATI_EST_FIELD + ", " +
                    CatalogoOfferteMobileDAO.FASCIA_VOCE_FIELD + ", " +
                    CatalogoOfferteMobileDAO.FASCIA_DATI_FIELD +
                    " FROM " + CatalogoOfferteMobileDAO.WIND_CATALOGO_OFFERTE_MOBILE_TABLE_NAME +
                    " WHERE " + "UPPER(" + CatalogoOfferteMobileDAO.OFFER_TYPE_FIELD + ") = '" + offerType.toUpperCase() + "'";

            query = query + " order by CAST(" + CatalogoOfferteMobileDAO.CANONE_MENSILE_FIELD + " as INT) desc ";

            Cursor cursor = db.rawQuery(query, null);
            if (cursor.moveToFirst()) {
                do {
                    OffertaMobile offertaMobile = new OffertaMobile();
                    offertaMobile.setIdOfferta(cursor.getInt(cursor.getColumnIndex(CatalogoOfferteMobileDAO.ID_OFFERTA_FIELD)));
                    offertaMobile.setOfferDescription(cursor.getString(cursor.getColumnIndex(CatalogoOfferteMobileDAO.OFFER_DESCRIPTION_FIELD)));
                    offertaMobile.setOfferDescriptionUpper(cursor.getString(cursor.getColumnIndex(CatalogoOfferteMobileDAO.OFFER_DESCRIPTION_UPPER_FIELD)));
                    offertaMobile.setOfferDescriptionIButton(cursor.getString(cursor.getColumnIndex(CatalogoOfferteMobileDAO.OFFER_DESCRIPTION_I_BUTTON_FIELD)));
                    offertaMobile.setOfferType(cursor.getString(cursor.getColumnIndex(CatalogoOfferteMobileDAO.OFFER_TYPE_FIELD)));
                    offertaMobile.setCanoneMensile(cursor.getString(cursor.getColumnIndex(CatalogoOfferteMobileDAO.CANONE_MENSILE_FIELD)));
                    offertaMobile.setCanoneMensileConv(cursor.getString(cursor.getColumnIndex(CatalogoOfferteMobileDAO.CANONE_MENSILE_CONV_FIELD)));
                    offertaMobile.setCanonePromo(cursor.getString(cursor.getColumnIndex(CatalogoOfferteMobileDAO.CANONE_PROMO_FIELD)));
                    offertaMobile.setCallAzIta(cursor.getString(cursor.getColumnIndex(CatalogoOfferteMobileDAO.CALL_AZ_ITA_FIELD)));
                    offertaMobile.setCallIta(cursor.getString(cursor.getColumnIndex(CatalogoOfferteMobileDAO.CALL_ITA_FIELD)));
                    offertaMobile.setSmsIta(cursor.getString(cursor.getColumnIndex(CatalogoOfferteMobileDAO.SMS_ITA_FIELD)));
                    offertaMobile.setDatiIta(cursor.getString(cursor.getColumnIndex(CatalogoOfferteMobileDAO.DATI_ITA_FIELD)));
                    offertaMobile.setCallEst(cursor.getString(cursor.getColumnIndex(CatalogoOfferteMobileDAO.CALL_EST_FIELD)));
                    offertaMobile.setSmsEst(cursor.getString(cursor.getColumnIndex(CatalogoOfferteMobileDAO.SMS_EST_FIELD)));
                    offertaMobile.setDatiEst(cursor.getString(cursor.getColumnIndex(CatalogoOfferteMobileDAO.DATI_EST_FIELD)));
                    offertaMobile.setFasciaVoce(cursor.getString(cursor.getColumnIndex(CatalogoOfferteMobileDAO.FASCIA_VOCE_FIELD)));
                    offertaMobile.setFasciaDati(cursor.getString(cursor.getColumnIndex(CatalogoOfferteMobileDAO.FASCIA_DATI_FIELD)));
                    offerteMobile.add(offertaMobile);
                } while (cursor.moveToNext());
            }

            cursor.close();
            db.close();
        }
        return offerteMobile;
    }


    public static ArrayList<OffertaMobile> retrieveOfferteMobile(Context context, int numPacchetto, String offerType) {
        SQLiteDatabase db = DbHelper.getInstance(context).getWritableDatabase();

        JSONArray pacchetti = null;
        JSONObject pacchetto = null;
        try {
            pacchetti = (JSONArray) Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONArray("pacchetti");
            pacchetto = pacchetti.getJSONObject(numPacchetto - 1);

            String query = "SELECT " +
                    CatalogoOfferteMobileDAO.ID_OFFERTA_FIELD + ", " +
                    CatalogoOfferteMobileDAO.OFFER_DESCRIPTION_FIELD + ", " +
                    CatalogoOfferteMobileDAO.OFFER_DESCRIPTION_UPPER_FIELD + ", " +
                    CatalogoOfferteMobileDAO.OFFER_DESCRIPTION_I_BUTTON_FIELD + ", " +
                    CatalogoOfferteMobileDAO.OFFER_TYPE_FIELD + ", " +
                    CatalogoOfferteMobileDAO.CANONE_MENSILE_FIELD + ", " +
                    CatalogoOfferteMobileDAO.CANONE_MENSILE_CONV_FIELD + ", " +
                    CatalogoOfferteMobileDAO.CANONE_PROMO_FIELD + ", " +
                    CatalogoOfferteMobileDAO.CALL_AZ_ITA_FIELD + ", " +
                    CatalogoOfferteMobileDAO.CALL_ITA_FIELD + ", " +
                    CatalogoOfferteMobileDAO.SMS_ITA_FIELD + ", " +
                    CatalogoOfferteMobileDAO.DATI_ITA_FIELD + ", " +
                    CatalogoOfferteMobileDAO.CALL_EST_FIELD + ", " +
                    CatalogoOfferteMobileDAO.SMS_EST_FIELD + ", " +
                    CatalogoOfferteMobileDAO.DATI_EST_FIELD + ", " +
                    CatalogoOfferteMobileDAO.FASCIA_VOCE_FIELD + ", " +
                    CatalogoOfferteMobileDAO.FASCIA_DATI_FIELD +
                    " FROM " + CatalogoOfferteMobileDAO.WIND_CATALOGO_OFFERTE_MOBILE_TABLE_NAME +
                    " WHERE " + "UPPER(" + CatalogoOfferteMobileDAO.OFFER_TYPE_FIELD + ") = '" + offerType.toUpperCase() + "'";

            if (pacchetto != null) {
                if (pacchetto.getBoolean("question1OffertaDevice")) {
                    query = query +
                            " AND ID_OFFERTA IN (SELECT ID_OFFERTA FROM CATALOGO_TERMINALI WHERE" + createWhereConditionOEMType(pacchetto.getBoolean("question3Apple"), pacchetto.getBoolean("question3Windows"), pacchetto.getBoolean("question3Samsung"), pacchetto.getBoolean("question3Altro"), pacchetto.getBoolean("question2Smartphone"), pacchetto.getBoolean("question2Tablet"));
                }


                switch (pacchetto.getInt("question5Phone")) {
                    case 1:
                        query = query +
                                (offerType.compareToIgnoreCase("Abbonamento") == 0 ? " AND " + CatalogoOfferteMobileDAO.DATI_ITA_FIELD + " <> ''" : "") +
                                " AND " + CatalogoOfferteMobileDAO.CALL_ITA_FIELD + " = ''" +
                                " AND " + CatalogoOfferteMobileDAO.CALL_AZ_ITA_FIELD + " = ''" +
                                " AND " + CatalogoOfferteMobileDAO.SMS_ITA_FIELD + " = ''";
                        break;
                    case 2:
                        query = query +
                                " AND " + "CAST(" + CatalogoOfferteMobileDAO.CALL_ITA_FIELD + " as INT) <= 200" +
                                " AND " + CatalogoOfferteMobileDAO.CALL_ITA_FIELD + " <> ''" +
                                " AND " + CatalogoOfferteMobileDAO.CALL_ITA_FIELD + " <> 'Illimitate'";
                        break;
                    case 3:
                        query = query +
                                " AND " + "CAST(" + CatalogoOfferteMobileDAO.CALL_ITA_FIELD + " as INT) <= 400" +
                                " AND " + CatalogoOfferteMobileDAO.CALL_ITA_FIELD + " <> ''" +
                                " AND " + CatalogoOfferteMobileDAO.CALL_ITA_FIELD + " <> 'Illimitate'";
                        break;
                    case 4:
                        query = query +
                                " AND " + "CAST(" + CatalogoOfferteMobileDAO.CALL_ITA_FIELD + " as INT) <= 600" +
                                " AND " + CatalogoOfferteMobileDAO.CALL_ITA_FIELD + " <> ''" +
                                " AND " + CatalogoOfferteMobileDAO.CALL_ITA_FIELD + " <> 'Illimitate'";
                        break;
                    case 5:
                        query = query +
                                " AND " + CatalogoOfferteMobileDAO.CALL_ITA_FIELD + " <> ''";
                        break;
                }

                if (offerType.compareToIgnoreCase("Abbonamento") == 0) {
                    switch (pacchetto.getInt("question5Web")) {
                        case 1:
                            query = query +
                                    " AND " + "CAST(" + CatalogoOfferteMobileDAO.DATI_ITA_FIELD + " as INT) <= 2";
                            break;
                        case 2:
                            query = query +
                                    " AND " + "CAST(" + CatalogoOfferteMobileDAO.DATI_ITA_FIELD + " as INT) <= 3";
                            break;
                        case 3:
                            query = query +
                                    " AND " + "CAST(" + CatalogoOfferteMobileDAO.DATI_ITA_FIELD + " as INT) <= 5";
                            break;
                        case 4:
                            query = query +
                                    " AND " + "CAST(" + CatalogoOfferteMobileDAO.DATI_ITA_FIELD + " as INT) <= 10";
                            break;
                        case 5:
                            query = query +
                                    " AND " + CatalogoOfferteMobileDAO.DATI_ITA_FIELD + " <> ''";
                            break;
                    }
                }

                if (pacchetto.getBoolean("question6Si")) {

                    query = query + createWhereConditionState(Utils.getZonaByArea(context, pacchetto.getString("question7Scelta1")), Utils.getZonaByArea(context, pacchetto.getString("question7Scelta2")));

//                    Utils.getZonaByState(context, pacchetto.getString("question7Scelta1"));
//
////                    TODO: gestire i paesi esteri scelta 1
//                    if (!TextUtils.isEmpty(pacchetto.getString("question7Scelta2"))) {
////                        TODO: gestire i paesi esteri scelta 2
//                    }
                }

                query = query + " order by CAST(" + CatalogoOfferteMobileDAO.CANONE_MENSILE_FIELD + " as INT) desc ";

            }

            Log.d("QUERY", "retrieveOfferteMobile: " + query);
            Cursor cursor = db.rawQuery(query, null);

            ArrayList<OffertaMobile> offerteMobile = new ArrayList<>();
            if (cursor.moveToFirst()) {
                do {
                    OffertaMobile offertaMobile = new OffertaMobile();
                    offertaMobile.setIdOfferta(cursor.getInt(cursor.getColumnIndex(CatalogoOfferteMobileDAO.ID_OFFERTA_FIELD)));
                    offertaMobile.setOfferDescription(cursor.getString(cursor.getColumnIndex(CatalogoOfferteMobileDAO.OFFER_DESCRIPTION_FIELD)));
                    offertaMobile.setOfferDescriptionUpper(cursor.getString(cursor.getColumnIndex(CatalogoOfferteMobileDAO.OFFER_DESCRIPTION_UPPER_FIELD)));
                    offertaMobile.setOfferDescriptionIButton(cursor.getString(cursor.getColumnIndex(CatalogoOfferteMobileDAO.OFFER_DESCRIPTION_I_BUTTON_FIELD)));
                    offertaMobile.setOfferType(cursor.getString(cursor.getColumnIndex(CatalogoOfferteMobileDAO.OFFER_TYPE_FIELD)));
                    offertaMobile.setCanoneMensile(cursor.getString(cursor.getColumnIndex(CatalogoOfferteMobileDAO.CANONE_MENSILE_FIELD)));
                    offertaMobile.setCanoneMensileConv(cursor.getString(cursor.getColumnIndex(CatalogoOfferteMobileDAO.CANONE_MENSILE_CONV_FIELD)));
                    offertaMobile.setCanonePromo(cursor.getString(cursor.getColumnIndex(CatalogoOfferteMobileDAO.CANONE_PROMO_FIELD)));
                    offertaMobile.setCallAzIta(cursor.getString(cursor.getColumnIndex(CatalogoOfferteMobileDAO.CALL_AZ_ITA_FIELD)));
                    offertaMobile.setCallIta(cursor.getString(cursor.getColumnIndex(CatalogoOfferteMobileDAO.CALL_ITA_FIELD)));
                    offertaMobile.setSmsIta(cursor.getString(cursor.getColumnIndex(CatalogoOfferteMobileDAO.SMS_ITA_FIELD)));
                    offertaMobile.setDatiIta(cursor.getString(cursor.getColumnIndex(CatalogoOfferteMobileDAO.DATI_ITA_FIELD)));
                    offertaMobile.setCallEst(cursor.getString(cursor.getColumnIndex(CatalogoOfferteMobileDAO.CALL_EST_FIELD)));
                    offertaMobile.setSmsEst(cursor.getString(cursor.getColumnIndex(CatalogoOfferteMobileDAO.SMS_EST_FIELD)));
                    offertaMobile.setDatiEst(cursor.getString(cursor.getColumnIndex(CatalogoOfferteMobileDAO.DATI_EST_FIELD)));
                    offertaMobile.setFasciaVoce(cursor.getString(cursor.getColumnIndex(CatalogoOfferteMobileDAO.FASCIA_VOCE_FIELD)));
                    offertaMobile.setFasciaDati(cursor.getString(cursor.getColumnIndex(CatalogoOfferteMobileDAO.FASCIA_DATI_FIELD)));
                    offerteMobile.add(offertaMobile);
                } while (cursor.moveToNext());
            }

            if (offerteMobile.isEmpty()) {
                query = "SELECT " +
                        CatalogoOfferteMobileDAO.ID_OFFERTA_FIELD + ", " +
                        CatalogoOfferteMobileDAO.OFFER_DESCRIPTION_FIELD + ", " +
                        CatalogoOfferteMobileDAO.OFFER_DESCRIPTION_UPPER_FIELD + ", " +
                        CatalogoOfferteMobileDAO.OFFER_DESCRIPTION_I_BUTTON_FIELD + ", " +
                        CatalogoOfferteMobileDAO.OFFER_TYPE_FIELD + ", " +
                        CatalogoOfferteMobileDAO.CANONE_MENSILE_FIELD + ", " +
                        CatalogoOfferteMobileDAO.CANONE_MENSILE_CONV_FIELD + ", " +
                        CatalogoOfferteMobileDAO.CANONE_PROMO_FIELD + ", " +
                        CatalogoOfferteMobileDAO.CALL_AZ_ITA_FIELD + ", " +
                        CatalogoOfferteMobileDAO.CALL_ITA_FIELD + ", " +
                        CatalogoOfferteMobileDAO.SMS_ITA_FIELD + ", " +
                        CatalogoOfferteMobileDAO.DATI_ITA_FIELD + ", " +
                        CatalogoOfferteMobileDAO.CALL_EST_FIELD + ", " +
                        CatalogoOfferteMobileDAO.SMS_EST_FIELD + ", " +
                        CatalogoOfferteMobileDAO.DATI_EST_FIELD + ", " +
                        CatalogoOfferteMobileDAO.FASCIA_VOCE_FIELD + ", " +
                        CatalogoOfferteMobileDAO.FASCIA_DATI_FIELD +
                        " FROM " + CatalogoOfferteMobileDAO.WIND_CATALOGO_OFFERTE_MOBILE_TABLE_NAME +
                        " WHERE " + "UPPER(" + CatalogoOfferteMobileDAO.OFFER_TYPE_FIELD + ") = '" + offerType.toUpperCase() + "'";

                query = query + " order by CAST(" + CatalogoOfferteMobileDAO.CANONE_MENSILE_FIELD + " as INT) desc ";

                Log.d("QUERY TAPPO", "retrieveOfferteMobile: " + query);
                cursor = db.rawQuery(query, null);

                if (cursor.moveToFirst()) {
                    do {
                        OffertaMobile offertaMobile = new OffertaMobile();
                        offertaMobile.setIdOfferta(cursor.getInt(cursor.getColumnIndex(CatalogoOfferteMobileDAO.ID_OFFERTA_FIELD)));
                        offertaMobile.setOfferDescription(cursor.getString(cursor.getColumnIndex(CatalogoOfferteMobileDAO.OFFER_DESCRIPTION_FIELD)));
                        offertaMobile.setOfferDescriptionUpper(cursor.getString(cursor.getColumnIndex(CatalogoOfferteMobileDAO.OFFER_DESCRIPTION_UPPER_FIELD)));
                        offertaMobile.setOfferDescriptionIButton(cursor.getString(cursor.getColumnIndex(CatalogoOfferteMobileDAO.OFFER_DESCRIPTION_I_BUTTON_FIELD)));
                        offertaMobile.setOfferType(cursor.getString(cursor.getColumnIndex(CatalogoOfferteMobileDAO.OFFER_TYPE_FIELD)));
                        offertaMobile.setCanoneMensile(cursor.getString(cursor.getColumnIndex(CatalogoOfferteMobileDAO.CANONE_MENSILE_FIELD)));
                        offertaMobile.setCanoneMensileConv(cursor.getString(cursor.getColumnIndex(CatalogoOfferteMobileDAO.CANONE_MENSILE_CONV_FIELD)));
                        offertaMobile.setCanonePromo(cursor.getString(cursor.getColumnIndex(CatalogoOfferteMobileDAO.CANONE_PROMO_FIELD)));
                        offertaMobile.setCallAzIta(cursor.getString(cursor.getColumnIndex(CatalogoOfferteMobileDAO.CALL_AZ_ITA_FIELD)));
                        offertaMobile.setCallIta(cursor.getString(cursor.getColumnIndex(CatalogoOfferteMobileDAO.CALL_ITA_FIELD)));
                        offertaMobile.setSmsIta(cursor.getString(cursor.getColumnIndex(CatalogoOfferteMobileDAO.SMS_ITA_FIELD)));
                        offertaMobile.setDatiIta(cursor.getString(cursor.getColumnIndex(CatalogoOfferteMobileDAO.DATI_ITA_FIELD)));
                        offertaMobile.setCallEst(cursor.getString(cursor.getColumnIndex(CatalogoOfferteMobileDAO.CALL_EST_FIELD)));
                        offertaMobile.setSmsEst(cursor.getString(cursor.getColumnIndex(CatalogoOfferteMobileDAO.SMS_EST_FIELD)));
                        offertaMobile.setDatiEst(cursor.getString(cursor.getColumnIndex(CatalogoOfferteMobileDAO.DATI_EST_FIELD)));
                        offertaMobile.setFasciaVoce(cursor.getString(cursor.getColumnIndex(CatalogoOfferteMobileDAO.FASCIA_VOCE_FIELD)));
                        offertaMobile.setFasciaDati(cursor.getString(cursor.getColumnIndex(CatalogoOfferteMobileDAO.FASCIA_DATI_FIELD)));
                        offerteMobile.add(offertaMobile);
                    } while (cursor.moveToNext());
                }
            }


            cursor.close();
            db.close();

            return offerteMobile;

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    private static String createWhereConditionOEMType(boolean isApple, boolean isWindows, boolean isSamsung, boolean isAltro, boolean isSmartphone, boolean isTablet) {
        String whereCondition = " OEM IN (__REPLACE_ALTRO__,__REPLACE_APPLE__,__REPLACE_SAMSUNG__,__REPLACE_WINDOW__) AND DEVICE_TYPE IN (__REPLACE_SMARTPHONE__,__REPLACE_TABLET__))";

        whereCondition = whereCondition.replace("__REPLACE_ALTRO__", isAltro ? "'Altro'" : "");
        whereCondition = whereCondition.replace("__REPLACE_APPLE__", isApple ? "'Apple'" : "");
        whereCondition = whereCondition.replace("__REPLACE_SAMSUNG__", isSamsung ? "'Samsung'" : "");
        whereCondition = whereCondition.replace("__REPLACE_WINDOW__", isWindows ? "'Windows Phone'" : "");

        whereCondition = whereCondition.replace(",,", ",");
        whereCondition = whereCondition.replace("(,", "(");
        whereCondition = whereCondition.replace(",)", ")");

        whereCondition = whereCondition.replace("__REPLACE_SMARTPHONE__", isSmartphone ? "'Smartphone'" : "");
        whereCondition = whereCondition.replace("__REPLACE_TABLET__", isTablet ? "'Tablet'" : "");

        whereCondition = whereCondition.replace(",,", ",");
        whereCondition = whereCondition.replace("(,", "(");
        whereCondition = whereCondition.replace(",)", ")");
        whereCondition = whereCondition.replace(" OEM IN () AND", "");


        return whereCondition;
    }

    private static String createWhereConditionState(String fascia1, String fascia2) {
        String whereCondition = " AND (upper(fascia_voce) like '%__REPLACE_OPZ1__%'";
        ;

        whereCondition = whereCondition.replace("__REPLACE_OPZ1__", fascia1);

        if (!TextUtils.isEmpty(fascia2)) {
            whereCondition = whereCondition + "or upper(fascia_voce) like '%__REPLACE_OPZ2__%')";
            whereCondition = whereCondition.replace("__REPLACE_OPZ2__", fascia2);
        } else {
            whereCondition = whereCondition + ")";
        }

        return whereCondition;
    }


}
