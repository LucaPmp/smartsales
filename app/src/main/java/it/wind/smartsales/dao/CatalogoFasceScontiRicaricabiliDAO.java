package it.wind.smartsales.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import it.wind.smartsales.entities.FasceScontiRicaricabili;

/**
 * Created by luca.quaranta on 22/03/2016.
 */
public class CatalogoFasceScontiRicaricabiliDAO {

    public final static String WIND_CATALOGO_FASCE_SCONTI_RICARICABILI_TABLE_NAME = "CATALOGO_FASCE_SCONTI_RICARICABILI";

    public final static String FASCIA_SPESA_1_FIELD = "FASCIA_SPESA_1";
    public final static String FASCIA_SPESA_2_FIELD = "FASCIA_SPESA_2";
    public final static String FASCIA_SPESA_3_FIELD = "FASCIA_SPESA_3";
    public final static String FASCIA_SPESA_4_FIELD = "FASCIA_SPESA_4";
    public final static String FASCIA_SCONTO_1_FIELD = "FASCIA_SCONTO_1";
    public final static String FASCIA_SCONTO_2_FIELD = "FASCIA_SCONTO_2";
    public final static String FASCIA_SCONTO_3_FIELD = "FASCIA_SCONTO_3";
    public final static String FASCIA_SCONTO_4_FIELD = "FASCIA_SCONTO_4";

    public final static String FASCIA_SPESA_1 = FASCIA_SPESA_1_FIELD + " VARCHAR(50)";
    public final static String FASCIA_SPESA_2 = FASCIA_SPESA_2_FIELD + " VARCHAR(50)";
    public final static String FASCIA_SPESA_3 = FASCIA_SPESA_3_FIELD + " VARCHAR(50)";
    public final static String FASCIA_SPESA_4 = FASCIA_SPESA_4_FIELD + " VARCHAR(50)";
    public final static String FASCIA_SCONTO_1 = FASCIA_SCONTO_1_FIELD + " VARCHAR(50)";
    public final static String FASCIA_SCONTO_2 = FASCIA_SCONTO_2_FIELD + " VARCHAR(50)";
    public final static String FASCIA_SCONTO_3 = FASCIA_SCONTO_3_FIELD + " VARCHAR(50)";
    public final static String FASCIA_SCONTO_4 = FASCIA_SCONTO_4_FIELD + " VARCHAR(50)";


    public final static List<String> fields = new ArrayList<String>();

    static {
        fields.add(FASCIA_SPESA_1);
        fields.add(FASCIA_SPESA_2);
        fields.add(FASCIA_SPESA_3);
        fields.add(FASCIA_SPESA_4);
        fields.add(FASCIA_SCONTO_1);
        fields.add(FASCIA_SCONTO_2);
        fields.add(FASCIA_SCONTO_3);
        fields.add(FASCIA_SCONTO_4);
    }

    public static void clearFasceScontiRicaricabiliTable(Context context){
        SQLiteDatabase db = DbHelper.getInstance(context).getWritableDatabase();
        db.execSQL("DELETE FROM "+ WIND_CATALOGO_FASCE_SCONTI_RICARICABILI_TABLE_NAME);
        db.close();
    }

    public static void insertFasceScontiRicaricabili(Context context, FasceScontiRicaricabili fasceScontiRicaricabili){
        SQLiteDatabase db = DbHelper.getInstance(context).getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(FASCIA_SPESA_1_FIELD, fasceScontiRicaricabili.getFasciaSpesa1());
        values.put(FASCIA_SPESA_2_FIELD, fasceScontiRicaricabili.getFasciaSpesa2());
        values.put(FASCIA_SPESA_3_FIELD, fasceScontiRicaricabili.getFasciaSpesa3());
        values.put(FASCIA_SPESA_4_FIELD, fasceScontiRicaricabili.getFasciaSpesa4());
        values.put(FASCIA_SCONTO_1_FIELD, fasceScontiRicaricabili.getFasciaSconto1());
        values.put(FASCIA_SCONTO_2_FIELD, fasceScontiRicaricabili.getFasciaSconto2());
        values.put(FASCIA_SCONTO_3_FIELD, fasceScontiRicaricabili.getFasciaSconto3());
        values.put(FASCIA_SCONTO_4_FIELD, fasceScontiRicaricabili.getFasciaSconto4());


        db.insert(WIND_CATALOGO_FASCE_SCONTI_RICARICABILI_TABLE_NAME, null, values);
        db.close();
    }

    public static FasceScontiRicaricabili retrieveFasceScontiRicaricabili(Context context) {
        SQLiteDatabase db = DbHelper.getInstance(context).getWritableDatabase();

        String query = "SELECT " +
                FASCIA_SPESA_1_FIELD + ", " +
                FASCIA_SPESA_2_FIELD + ", " +
                FASCIA_SPESA_3_FIELD + ", " +
                FASCIA_SPESA_4_FIELD + ", " +
                FASCIA_SCONTO_1_FIELD + ", " +
                FASCIA_SCONTO_2_FIELD + ", " +
                FASCIA_SCONTO_3_FIELD + ", " +
                FASCIA_SCONTO_4_FIELD +
                " FROM " + WIND_CATALOGO_FASCE_SCONTI_RICARICABILI_TABLE_NAME;
        Cursor cursor = db.rawQuery(query, null);

        FasceScontiRicaricabili fasceScontiRicaricabili = new FasceScontiRicaricabili();
        if (cursor.moveToFirst()) {
            do {
                fasceScontiRicaricabili.setFasciaSpesa1(Integer.valueOf(cursor.getString(cursor.getColumnIndex(FASCIA_SPESA_1_FIELD))));
                fasceScontiRicaricabili.setFasciaSpesa2(Integer.valueOf(cursor.getString(cursor.getColumnIndex(FASCIA_SPESA_2_FIELD))));
                fasceScontiRicaricabili.setFasciaSpesa3(Integer.valueOf(cursor.getString(cursor.getColumnIndex(FASCIA_SPESA_3_FIELD))));
                fasceScontiRicaricabili.setFasciaSpesa4(Integer.valueOf(cursor.getString(cursor.getColumnIndex(FASCIA_SPESA_4_FIELD))));
                fasceScontiRicaricabili.setFasciaSconto1(Integer.valueOf(cursor.getString(cursor.getColumnIndex(FASCIA_SCONTO_1_FIELD))));
                fasceScontiRicaricabili.setFasciaSconto2(Integer.valueOf(cursor.getString(cursor.getColumnIndex(FASCIA_SCONTO_2_FIELD))));
                fasceScontiRicaricabili.setFasciaSconto3(Integer.valueOf(cursor.getString(cursor.getColumnIndex(FASCIA_SCONTO_3_FIELD))));
                fasceScontiRicaricabili.setFasciaSconto4(Integer.valueOf(cursor.getString(cursor.getColumnIndex(FASCIA_SCONTO_4_FIELD))));
            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();
        return fasceScontiRicaricabili;
    }

}
