package it.wind.smartsales.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import it.wind.smartsales.commons.Log;


public class ExtraGigaValueDAO {

    public final static String WIND_EXTRA_GIGA_TABLE_NAME = "EXTRA_GIGA_VALUE";

    public final static String ID_EXTRA_GIGA_FIELD = "ID_EXTRA_GIGA";
    public final static String ID_PRATICA_FIELD = "ID_PRATICA";
    public final static String JSON_DATA_FIELD = "DATA"; //json object containing data
//    public final static String JSON_DATA_VALUE_FIELD = "DATA_VALUE"; //json object containing data

    public final static String ID_EXTRA_GIGA = ID_EXTRA_GIGA_FIELD + " INTEGER PRIMARY KEY AUTOINCREMENT";
    public final static String ID_PRATICA = ID_PRATICA_FIELD + " VARCHAR(255)";
    public final static String JSON_DATA = JSON_DATA_FIELD + " TEXT";
//    public final static String JSON_DATA_VALUE = JSON_DATA_FIELD + " TEXT";



    private static String TAG = "ExtraGigaValueDAO";

    public final static List<String> fields = new ArrayList<String>();


    static {
        fields.add(ID_EXTRA_GIGA);
        fields.add(ID_PRATICA);
        fields.add(JSON_DATA);
//        fields.add(JSON_DATA_VALUE);
    }

    public static void clearPraticheTable(Context context) {
        SQLiteDatabase db = DbHelper.getInstance(context).getWritableDatabase();
        db.execSQL("DELETE FROM " + WIND_EXTRA_GIGA_TABLE_NAME);
        db.close();
    }

    public static void insertOrUpdateDetails(Context context, String praticaId, JSONObject jsonData) {
        try {
            String id = findIdByPratica(context, praticaId);
            if(id==null) {
                SQLiteDatabase db = DbHelper.getInstance(context).getWritableDatabase();
                ContentValues values = new ContentValues();
                values.put(ID_PRATICA_FIELD, praticaId);
                values.put(JSON_DATA_FIELD, jsonData.toString());
                db.insert(WIND_EXTRA_GIGA_TABLE_NAME, null, values);
                db.close();
            }
            else{
                SQLiteDatabase db = DbHelper.getInstance(context).getWritableDatabase();
                ContentValues values = new ContentValues();
                String whereClause = ID_EXTRA_GIGA_FIELD +" = "+id;
                values.put(ID_PRATICA_FIELD, praticaId);
                values.put(JSON_DATA_FIELD, jsonData.toString());
                db.update(WIND_EXTRA_GIGA_TABLE_NAME, values, whereClause, null);
                db.close();
            }

        }
        catch (Exception e )
        {
            Log.d(TAG,e.getMessage());
        }
    }

//    public static void insertOrUpdateValues(Context context, String praticaId, JSONObject jsonData) {
//        try {
//            String id = findIdByPratica(context, praticaId);
//            if(id==null) {
//                SQLiteDatabase db = DbHelper.getInstance(context).getWritableDatabase();
//                ContentValues values = new ContentValues();
//                values.put(ID_PRATICA_FIELD, praticaId);
//                values.put(JSON_DATA_VALUE_FIELD, jsonData.toString());
//                db.insert(WIND_EXTRA_GIGA_TABLE_NAME, null, values);
//                db.close();
//            }
//            else{
//                SQLiteDatabase db = DbHelper.getInstance(context).getWritableDatabase();
//                ContentValues values = new ContentValues();
//                values.put(ID_EXTRA_GIGA_FIELD, id);
//                values.put(ID_PRATICA_FIELD, praticaId);
//                values.put(JSON_DATA_VALUE_FIELD, jsonData.toString());
//                db.update(WIND_EXTRA_GIGA_TABLE_NAME,  values,null,null);
//                db.close();
//            }
//
//        }
//        catch (Exception e )
//        {
//            Log.d(TAG,e.getMessage());
//        }
//    }

    public static JSONObject findDetailsByIdPratica(Context context, String idPratica) {

        SQLiteDatabase db = DbHelper.getInstance(context).getWritableDatabase();
        JSONObject data=null;

        try {
            String query = "SELECT * FROM " + WIND_EXTRA_GIGA_TABLE_NAME+ " WHERE ID_PRATICA = " + idPratica;
            Cursor cursor = db.rawQuery(query, null);


            while (cursor.moveToNext()) {


                try {
                    String value = cursor.getString(cursor.getColumnIndex(JSON_DATA_FIELD));
                    data = new JSONObject(value);

                }
                catch (Exception e)
                {
                    Log.d(TAG,e.getMessage());
                }
            }

            cursor.close();
            db.close();
        } catch (Exception e) {
            db.close();
            Log.i(TAG, "eccezione aggiornamento nome pratica " + e.getMessage());
        }

        return data;
    }

    public static String findIdByPratica(Context context, String idPratica) {

        SQLiteDatabase db = DbHelper.getInstance(context).getWritableDatabase();
        String value=null;

        try {
            String query = "SELECT * FROM " + WIND_EXTRA_GIGA_TABLE_NAME+ " WHERE ID_PRATICA = " + idPratica;
            Cursor cursor = db.rawQuery(query, null);


            while (cursor.moveToNext()) {


                try {
                    value = cursor.getString(cursor.getColumnIndex(ID_EXTRA_GIGA_FIELD));
                }
                catch (Exception e)
                {
                    Log.d(TAG,e.getMessage());
                }
            }

            cursor.close();
            db.close();
        } catch (Exception e) {
            db.close();
            Log.i(TAG, "eccezione aggiornamento nome pratica " + e.getMessage());
        }

        return value;
    }

//    public static JSONObject findValuesByIdPratica(Context context, String idPratica) {
//
//        SQLiteDatabase db = DbHelper.getInstance(context).getWritableDatabase();
//        JSONObject data=null;
//
//        try {
//            String query = "SELECT * FROM " + WIND_EXTRA_GIGA_TABLE_NAME+ " WHERE ID_PRATICA = " + idPratica;
//            Cursor cursor = db.rawQuery(query, null);
//
//
//            while (cursor.moveToNext()) {
//
//
//                try {
//                    String value = cursor.getString(cursor.getColumnIndex(JSON_DATA_VALUE_FIELD));
//                    data = new JSONObject(value);
//
//                }
//                catch (Exception e)
//                {
//                    Log.d(TAG,e.getMessage());
//                }
//            }
//
//            cursor.close();
//            db.close();
//        } catch (Exception e) {
//            db.close();
//            Log.i(TAG, "eccezione aggiornamento nome pratica " + e.getMessage());
//        }
//
//        return data;
//    }

}
