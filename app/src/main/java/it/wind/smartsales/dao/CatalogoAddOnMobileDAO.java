package it.wind.smartsales.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import it.wind.smartsales.entities.AddOnMobile;

/**
 * Created by luca.quaranta on 22/03/2016.
 */
public class CatalogoAddOnMobileDAO {

    public final static String WIND_CATALOGO_ADD_ON_MOBILE_TABLE_NAME = "CATALOGO_ADD_ON_MOBILE";

    public final static String ID_OFFERTA_FIELD = "ID_OFFERTA";
    public final static String DESC_FASCIA_FIELD = "DESC_FASCIA";
    public final static String PREZZO_FASCIA_FIELD = "PREZZO_FASCIA";
    public final static String MAX_SIM_VENDIBILE_FIELD = "MAX_SIM_VENDIBILE";

    private static final String EXTRA_BUNDLE_PREZZO_FIELD = "EXTRA_BUNDLE_PREZZO";
    private static final String EXTRA_BUNDLE_DESCRIZIONE_FIELD ="EXTRA_BUNDLE_DESCRIZIONE";
    private static final String EXTRA_BUNDLE_MAX_NUMERO_RINNOVI_FIELD = "EXTRA_BUNDLE_MAX_NUMERO_RINNOVI";
    private static final String EXTRA_BUNDLE_DESCRIZIONE_PDF_FIELD = "EXTRA_BUNDLE_DESCRIZIONE_PDF";


    public final static String ID_OFFERTA = ID_OFFERTA_FIELD + " VARCHAR(50)";
    public final static String DESC_FASCIA = DESC_FASCIA_FIELD + " VARCHAR(255)";
    public final static String PREZZO_FASCIA = PREZZO_FASCIA_FIELD + " VARCHAR(50)";
    public final static String MAX_SIM_VENDIBILE = MAX_SIM_VENDIBILE_FIELD + " VARCHAR(50)";

    private static final String EXTRA_BUNDLE_PREZZO = EXTRA_BUNDLE_PREZZO_FIELD + " VARCHAR(50)";
    private static final String EXTRA_BUNDLE_DESCRIZIONE = EXTRA_BUNDLE_DESCRIZIONE_FIELD+ " VARCHAR(50)";
    private static final String EXTRA_BUNDLE_MAX_NUMERO_RINNOVI = EXTRA_BUNDLE_MAX_NUMERO_RINNOVI_FIELD+ " VARCHAR(50)";
    private static final String EXTRA_BUNDLE_DESCRIZIONE_PDF = EXTRA_BUNDLE_DESCRIZIONE_PDF_FIELD+ " VARCHAR(50)";


    public final static List<String> fields = new ArrayList<String>();



    static {
        fields.add(ID_OFFERTA);
        fields.add(DESC_FASCIA);
        fields.add(PREZZO_FASCIA);
        fields.add(MAX_SIM_VENDIBILE);

        fields.add(EXTRA_BUNDLE_PREZZO);
        fields.add(EXTRA_BUNDLE_DESCRIZIONE);
        fields.add(EXTRA_BUNDLE_MAX_NUMERO_RINNOVI);
        fields.add(EXTRA_BUNDLE_DESCRIZIONE_PDF);
    }

    public static void clearAddOnMobileTable(Context context) {
        SQLiteDatabase db = DbHelper.getInstance(context).getWritableDatabase();
        db.execSQL("DELETE FROM " + WIND_CATALOGO_ADD_ON_MOBILE_TABLE_NAME);
        db.close();
    }

    public static void insertAddOnMobile(Context context, AddOnMobile addOnMobile) {
        SQLiteDatabase db = DbHelper.getInstance(context).getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(ID_OFFERTA_FIELD, addOnMobile.getIdOfferta());
        values.put(DESC_FASCIA_FIELD, addOnMobile.getDescFascia());
        values.put(PREZZO_FASCIA_FIELD, addOnMobile.getPrezzoFascia());
        values.put(MAX_SIM_VENDIBILE_FIELD, addOnMobile.getMaxSimVendibile());

        values.put(EXTRA_BUNDLE_PREZZO_FIELD, addOnMobile.getExtraBundlePrezzo());
        values.put(EXTRA_BUNDLE_DESCRIZIONE_FIELD, addOnMobile.getExtraBundleDescrizione());
        values.put(EXTRA_BUNDLE_MAX_NUMERO_RINNOVI_FIELD, addOnMobile.getExtraBundleMaxNumeroRinnovi());
        values.put(EXTRA_BUNDLE_DESCRIZIONE_PDF_FIELD, addOnMobile.getExtraBundleDescrizionePDF());


        db.insert(WIND_CATALOGO_ADD_ON_MOBILE_TABLE_NAME, null, values);
        db.close();
    }

    public static ArrayList<AddOnMobile> retrieveAddOnMobile(Context context, String idOfferta, String minValue) {
        SQLiteDatabase db = DbHelper.getInstance(context).getWritableDatabase();

        String query = "SELECT " + ID_OFFERTA_FIELD + ", " + DESC_FASCIA_FIELD + ", " + PREZZO_FASCIA_FIELD + ", " + MAX_SIM_VENDIBILE_FIELD + " FROM " + WIND_CATALOGO_ADD_ON_MOBILE_TABLE_NAME +
                " WHERE " + ID_OFFERTA_FIELD + " = '" + idOfferta + "'" +
                " AND cast(substr(" + DESC_FASCIA_FIELD + ", 0, length(" + DESC_FASCIA_FIELD + ")-1) as int) >= " + minValue;
        Cursor cursor = db.rawQuery(query, null);

        ArrayList<AddOnMobile> addOnMobiles = new ArrayList<>();
        if (cursor.moveToFirst()) {
            do {
                AddOnMobile addOnMobile = new AddOnMobile();
                addOnMobile.setIdOfferta(cursor.getString(cursor.getColumnIndex(ID_OFFERTA_FIELD)));
                addOnMobile.setDescFascia(cursor.getString(cursor.getColumnIndex(DESC_FASCIA_FIELD)));
                addOnMobile.setPrezzoFascia(cursor.getString(cursor.getColumnIndex(PREZZO_FASCIA_FIELD)));
                addOnMobile.setMaxSimVendibile(cursor.getString(cursor.getColumnIndex(MAX_SIM_VENDIBILE_FIELD)));
                addOnMobiles.add(addOnMobile);
            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();
        return addOnMobiles;
    }

    public static ArrayList<AddOnMobile> retrieveAllAddOnMobile(Context context, String idOfferta, String minValue) {
        SQLiteDatabase db = DbHelper.getInstance(context).getWritableDatabase();

        String query = "SELECT " + ID_OFFERTA_FIELD + ", " + DESC_FASCIA_FIELD + ", " + PREZZO_FASCIA_FIELD + ", " + MAX_SIM_VENDIBILE_FIELD + " FROM " + WIND_CATALOGO_ADD_ON_MOBILE_TABLE_NAME +
                " WHERE " + ID_OFFERTA_FIELD + " = '" + idOfferta + "'"; //+ " AND cast(substr(" + DESC_FASCIA_FIELD + ", 0, length(" + DESC_FASCIA_FIELD + ")-1) as int) >= " + minValue;
        Cursor cursor = db.rawQuery(query, null);

        ArrayList<AddOnMobile> addOnMobiles = new ArrayList<>();
        if (cursor.moveToFirst()) {
            do {
                AddOnMobile addOnMobile = new AddOnMobile();
                addOnMobile.setIdOfferta(cursor.getString(cursor.getColumnIndex(ID_OFFERTA_FIELD)));
                addOnMobile.setDescFascia(cursor.getString(cursor.getColumnIndex(DESC_FASCIA_FIELD)));
                addOnMobile.setPrezzoFascia(cursor.getString(cursor.getColumnIndex(PREZZO_FASCIA_FIELD)));
                addOnMobile.setMaxSimVendibile(cursor.getString(cursor.getColumnIndex(MAX_SIM_VENDIBILE_FIELD)));
                addOnMobiles.add(addOnMobile);
            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();
        return addOnMobiles;
    }


    public static ArrayList<AddOnMobile> retrieveExtraBundles(Context context) {
        SQLiteDatabase db = DbHelper.getInstance(context).getWritableDatabase();

        String query = "SELECT distinct(DESC_FASCIA),ID_OFFERTA, PREZZO_FASCIA, MAX_SIM_VENDIBILE, EXTRA_BUNDLE_PREZZO, EXTRA_BUNDLE_DESCRIZIONE, EXTRA_BUNDLE_MAX_NUMERO_RINNOVI, EXTRA_BUNDLE_DESCRIZIONE_PDF FROM " + WIND_CATALOGO_ADD_ON_MOBILE_TABLE_NAME;

        Cursor cursor = db.rawQuery(query, null);

        ArrayList<AddOnMobile> addOnMobiles = new ArrayList<>();
        if (cursor.moveToFirst()) {
            do {
                AddOnMobile addOnMobile = new AddOnMobile();
                addOnMobile.setIdOfferta(cursor.getString(cursor.getColumnIndex(ID_OFFERTA_FIELD)));
                addOnMobile.setDescFascia(cursor.getString(cursor.getColumnIndex(DESC_FASCIA_FIELD)));
                addOnMobile.setPrezzoFascia(cursor.getString(cursor.getColumnIndex(PREZZO_FASCIA_FIELD)));
                addOnMobile.setMaxSimVendibile(cursor.getString(cursor.getColumnIndex(MAX_SIM_VENDIBILE_FIELD)));

                addOnMobile.setExtraBundlePrezzo(cursor.getString(cursor.getColumnIndex(EXTRA_BUNDLE_PREZZO_FIELD)));
                addOnMobile.setExtraBundleDescrizione(cursor.getString(cursor.getColumnIndex(EXTRA_BUNDLE_DESCRIZIONE_FIELD)));
                addOnMobile.setExtraBundleMaxNumeroRinnovi(cursor.getString(cursor.getColumnIndex(EXTRA_BUNDLE_MAX_NUMERO_RINNOVI_FIELD)));
                addOnMobile.setExtraBundleDescrizionePDF(cursor.getString(cursor.getColumnIndex(EXTRA_BUNDLE_DESCRIZIONE_PDF_FIELD)));


                addOnMobiles.add(addOnMobile);
            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();
        return addOnMobiles;
    }

}
