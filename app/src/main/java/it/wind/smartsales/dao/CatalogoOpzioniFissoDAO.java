package it.wind.smartsales.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import it.wind.smartsales.entities.OpzioneFisso;

/**
 * Created by luca.quaranta on 22/03/2016.
 * p
 */
public class CatalogoOpzioniFissoDAO {

    public final static String WIND_CATALOGO_OPZIONI_FISSO_TABLE_NAME = "CATALOGO_OPZIONI_FISSO";

    public final static String ID_OPZIONE_FIELD = "ID_OPZIONE";
    public final static String OPZIONE_DESC_FIELD = "OPZIONE_DESC";
    public final static String ID_OFFERTA_FIELD = "ID_OFFERTA";
    public final static String CANONE_MENSILE_FIELD = "CANONE_MENSILE";
    public final static String ATTIVAZIONE_UT_FIELD = "ATTIVAZIONE_UT";
    public final static String IS_DEFAULT_FIELD = "IS_DEFAULT";
    public final static String BUTTON_TYPE_FIELD = "BUTTON_TYPE";
    public final static String PDC_SECTION_FIELD = "PDC_SECTION";
    public final static String OPZIONE_DESC_UPPER_FIELD = "OPZIONE_DESC_UPPER";
    public final static String OPZIONE_DESC_I_BUTTON_FIELD = "OPZIONE_DESC_I_BUTTON";
    public final static String SCONTO_CONVERGENZA_FIELD = "SCONTO_CONVERGENZA";


    public final static String ID_OPZIONE = ID_OPZIONE_FIELD + " VARCHAR(50)";
    public final static String OPZIONE_DESC = OPZIONE_DESC_FIELD + " VARCHAR(255)";
    public final static String ID_OFFERTA = ID_OFFERTA_FIELD + " VARCHAR(50)";
    public final static String CANONE_MENSILE = CANONE_MENSILE_FIELD + " VARCHAR(50)";
    public final static String ATTIVAZIONE_UT = ATTIVAZIONE_UT_FIELD + " VARCHAR(50)";
    public final static String IS_DEFAULT = IS_DEFAULT_FIELD + " VARCHAR(50)";
    public final static String BUTTON_TYPE = BUTTON_TYPE_FIELD + " VARCHAR(50)";
    public final static String PDC_SECTION = PDC_SECTION_FIELD + " VARCHAR(50)";
    public final static String OPZIONE_DESC_UPPER = OPZIONE_DESC_UPPER_FIELD + " VARCHAR(50)";
    public final static String OPZIONE_DESC_I_BUTTON = OPZIONE_DESC_I_BUTTON_FIELD + " VARCHAR(255)";
    public final static String SCONTO_CONVERGENZA = SCONTO_CONVERGENZA_FIELD + " VARCHAR(50)";

    public final static List<String> fields = new ArrayList<>();

    static {
        fields.add(ID_OPZIONE);
        fields.add(OPZIONE_DESC);
        fields.add(ID_OFFERTA);
        fields.add(CANONE_MENSILE);
        fields.add(ATTIVAZIONE_UT);
        fields.add(IS_DEFAULT);
        fields.add(BUTTON_TYPE);
        fields.add(PDC_SECTION);
        fields.add(OPZIONE_DESC_UPPER);
        fields.add(OPZIONE_DESC_I_BUTTON);
        fields.add(SCONTO_CONVERGENZA);
    }

    public static void clearOpzioniFissoTable(Context context) {
        SQLiteDatabase db = DbHelper.getInstance(context).getWritableDatabase();
        db.execSQL("DELETE FROM " + WIND_CATALOGO_OPZIONI_FISSO_TABLE_NAME);
        db.close();
    }

    public static void insertOpzioneFisso(Context context, OpzioneFisso opzioneFisso) {
        SQLiteDatabase db = DbHelper.getInstance(context).getWritableDatabase();



        ContentValues values = new ContentValues();
        values.put(ID_OPZIONE_FIELD, opzioneFisso.getIdOpzione());
        values.put(IS_DEFAULT_FIELD, opzioneFisso.isDefault());
        values.put(BUTTON_TYPE_FIELD, opzioneFisso.getButtonType());
        values.put(OPZIONE_DESC_FIELD, opzioneFisso.getOpzioneDesc());
        values.put(PDC_SECTION_FIELD, opzioneFisso.getPdcSection());
        values.put(OPZIONE_DESC_UPPER_FIELD, opzioneFisso.getOpzioneDescUpper());
        values.put(OPZIONE_DESC_I_BUTTON_FIELD, opzioneFisso.getOpzioneDescriptionIButton());
        values.put(SCONTO_CONVERGENZA_FIELD, opzioneFisso.getScontoConvergenza());
        values.put(ATTIVAZIONE_UT_FIELD, opzioneFisso.getCostoUnaTantum());
        values.put(CANONE_MENSILE_FIELD, opzioneFisso.getCanoneMensile());
        values.put(ID_OFFERTA_FIELD, opzioneFisso.getIdOfferta());

        db.insert(WIND_CATALOGO_OPZIONI_FISSO_TABLE_NAME, null, values);
        db.close();
    }

    private static String getOpzioniByIdQuery(int id) {
        String select = "SELECT * ";
        String from = " FROM " + WIND_CATALOGO_OPZIONI_FISSO_TABLE_NAME;
        String where = " WHERE " + ID_OFFERTA_FIELD + " = " + id;
        String orderBy = " ORDER BY CAST(" + CANONE_MENSILE_FIELD + " as INT) desc";

        return select + from + where + orderBy;
    }

    public static ArrayList<OpzioneFisso> getOpzioniById(Context context, int offerId) {
        SQLiteDatabase db = DbHelper.getInstance(context).getWritableDatabase();
        Cursor cursor = db.rawQuery(getOpzioniByIdQuery(offerId), null);

        ArrayList<OpzioneFisso> opzioni = setOpzioni(cursor);
        cursor.close();
        db.close();
        return opzioni;
    }


    public static ArrayList<OpzioneFisso> setOpzioni(Cursor cursor) {
        ArrayList<OpzioneFisso> result = new ArrayList<>();
        while (cursor.moveToNext()) {
            OpzioneFisso opzione = new OpzioneFisso();
            opzione.setIdOpzione(getString(cursor, ID_OPZIONE_FIELD));
            opzione.setDefault(getBoolean(cursor, ID_OFFERTA_FIELD));
            opzione.setButtonType(getString(cursor, BUTTON_TYPE_FIELD));
            opzione.setOpzioneDesc(getString(cursor, OPZIONE_DESC_FIELD));
            opzione.setPdcSection(getString(cursor, PDC_SECTION_FIELD));
            opzione.setOpzioneDescUpper(getString(cursor, OPZIONE_DESC_UPPER_FIELD));
            opzione.setOpzioneDescriptionIButton(getString(cursor, OPZIONE_DESC_I_BUTTON_FIELD));
            opzione.setScontoConvergenza(getBoolean(cursor, SCONTO_CONVERGENZA_FIELD));
            opzione.setCostoUnaTantum(getString(cursor, ATTIVAZIONE_UT_FIELD));
            opzione.setCanoneMensile(getString(cursor, CANONE_MENSILE_FIELD));
            opzione.setIdOfferta(getString(cursor, ID_OFFERTA_FIELD));
            result.add(opzione);
        }
        return result;
    }

    private static String getString(Cursor cursor, String field) {
        return cursor.getString(cursor.getColumnIndex(field));
    }

    private static boolean getBoolean(Cursor cursor, String field) {
        return Boolean.valueOf(cursor.getString(cursor.getColumnIndex(field)));
    }
}
