package it.wind.smartsales.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import it.wind.smartsales.entities.OffertaFisso;

/**
 * Created by luca.quaranta on 22/03/2016.
 */
public class CatalogoOfferteFissoDAO {

    public static final String WIND_SMART_OFFICE = "WIND SMART OFFICE";
    public static final String ALL_INCLUSIVE = "ALL INCLUSIVE AZIENDE";

    public final static String WIND_CATALOGO_OFFERTE_FISSO_TABLE_NAME = "CATALOGO_OFFERTE_FISSO";
    public final static List<String> fields = new ArrayList<>();
    private final static String ID_OFFERTA_FIELD = "ID_OFFERTA";
    private final static String OFFER_DESCRIPTION_FIELD = "OFFER_DESCRIPTION";
    private final static String OFFER_TYPE_FIELD = "OFFER_TYPE";
    private final static String TECHNOLOGY_FIELD = "TECHNOLOGY";
    private final static String CONTRIBUTO_UT_NUOVA_LINEA_FIELD = "CONTRIBUTO_UT_NUOVA_LINEA";
    private final static String CONTRIBUTO_UT_LINEA_ATTIVA_FIELD = "CONTRIBUTO_UT_LINEA_ATTIVA";
    private final static String CANONE_MENSILE_FIELD = "CANONE_MENSILE";
    private final static String CANONE_BIMESTRALE_FIELD = "CANONE_BIMESTRALE";
    private final static String CANONE_BIMESTRALE_CONV_FIELD = "CANONE_BIMESTRALE_CONV";
    private final static String CANONE_PROMO_FIELD = "CANONE_PROMO";
    private final static String CALL_ITA_FISSO_WIND_FIELD = "CALL_ITA_FISSO_WIND";
    private final static String CALL_ITA_FISSO_FIELD = "CALL_ITA_FISSO";
    private final static String CALL_ITA_MOBILE_WIND_FIELD = "CALL_ITA_MOBILE_WIND";
    private final static String CALL_ITA_MOBILE_FIELD = "CALL_ITA_MOBILE";
    private final static String ADSL_ITA_FIELD = "ADSL_ITA";
    private final static String CALL_EST_FISSO_AREA_1_FIELD = "CALL_EST_FISSO_AREA_1";
    private final static String ID_OFFERTA = ID_OFFERTA_FIELD + " INTEGER PRIMARY KEY";
    private final static String OFFER_DESCRIPTION = OFFER_DESCRIPTION_FIELD + " VARCHAR(255)";
    private final static String OFFER_TYPE = OFFER_TYPE_FIELD + " VARCHAR(50)";
    private final static String TECHNOLOGY = TECHNOLOGY_FIELD + " VARCHAR(50)";
    private final static String CONTRIBUTO_UT_NUOVA_LINEA = CONTRIBUTO_UT_NUOVA_LINEA_FIELD + " VARCHAR(50)";
    private final static String CONTRIBUTO_UT_LINEA_ATTIVA = CONTRIBUTO_UT_LINEA_ATTIVA_FIELD + " VARCHAR(50)";
    private final static String CANONE_MENSILE = CANONE_MENSILE_FIELD + " VARCHAR(50)";
    private final static String CANONE_BIMESTRALE = CANONE_BIMESTRALE_FIELD + " VARCHAR(50)";
    private final static String CANONE_BIMESTRALE_CONV = CANONE_BIMESTRALE_CONV_FIELD + " VARCHAR(50)";
    private final static String CANONE_PROMO = CANONE_PROMO_FIELD + " VARCHAR(50)";
    private final static String CALL_ITA_FISSO_WIND = CALL_ITA_FISSO_WIND_FIELD + " VARCHAR(50)";
    private final static String CALL_ITA_FISSO = CALL_ITA_FISSO_FIELD + " VARCHAR(50)";
    private final static String CALL_ITA_MOBILE_WIND = CALL_ITA_MOBILE_WIND_FIELD + " VARCHAR(50)";
    private final static String CALL_ITA_MOBILE = CALL_ITA_MOBILE_FIELD + " VARCHAR(50)";
    private final static String ADSL_ITA = ADSL_ITA_FIELD + " VARCHAR(50)";
    private final static String CALL_EST_FISSO_AREA_1 = CALL_EST_FISSO_AREA_1_FIELD + " VARCHAR(50)";

    static {
        fields.add(ID_OFFERTA);
        fields.add(OFFER_DESCRIPTION);
        fields.add(OFFER_TYPE);
        fields.add(TECHNOLOGY);
        fields.add(CONTRIBUTO_UT_NUOVA_LINEA);
        fields.add(CONTRIBUTO_UT_LINEA_ATTIVA);
        fields.add(CANONE_MENSILE);
        fields.add(CANONE_BIMESTRALE);
        fields.add(CANONE_BIMESTRALE_CONV);
        fields.add(CANONE_PROMO);
        fields.add(CALL_ITA_FISSO_WIND);
        fields.add(CALL_ITA_FISSO);
        fields.add(CALL_ITA_MOBILE_WIND);
        fields.add(CALL_ITA_MOBILE);
        fields.add(ADSL_ITA);
        fields.add(CALL_EST_FISSO_AREA_1);
    }

    public static void clearOffertaFissoTable(Context context) {
        SQLiteDatabase db = DbHelper.getInstance(context).getWritableDatabase();
        db.execSQL("DELETE FROM " + WIND_CATALOGO_OFFERTE_FISSO_TABLE_NAME);
        db.close();
    }

    public static void insertOffertaFisso(Context context, OffertaFisso offertaFisso) {
        SQLiteDatabase db = DbHelper.getInstance(context).getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(ID_OFFERTA_FIELD, offertaFisso.getIdOfferta());
        values.put(OFFER_DESCRIPTION_FIELD, offertaFisso.getOfferDescription());
        values.put(OFFER_TYPE_FIELD, offertaFisso.getOfferType());
        values.put(TECHNOLOGY_FIELD, offertaFisso.getTechnology());
        values.put(CONTRIBUTO_UT_NUOVA_LINEA_FIELD, offertaFisso.getContributoUTNuovaLinea());
        values.put(CONTRIBUTO_UT_LINEA_ATTIVA_FIELD, offertaFisso.getContributoUTLineaAttiva());
        values.put(CANONE_MENSILE_FIELD, offertaFisso.getCanoneMensile());
        values.put(CANONE_BIMESTRALE_FIELD, offertaFisso.getCanoneBimestrale());
        values.put(CANONE_BIMESTRALE_CONV_FIELD, offertaFisso.getCanoneBimestraleConv());
        values.put(CANONE_PROMO_FIELD, offertaFisso.getCanonePromo());
        values.put(CALL_ITA_FISSO_WIND_FIELD, offertaFisso.getCallItaFissoWind());
        values.put(CALL_ITA_FISSO_FIELD, offertaFisso.getCallItaFisso());
        values.put(CALL_ITA_MOBILE_WIND_FIELD, offertaFisso.getCallItaMobileWind());
        values.put(CALL_ITA_MOBILE_FIELD, offertaFisso.getCallItaMobile());
        values.put(ADSL_ITA_FIELD, offertaFisso.getAdslIta());
        values.put(CALL_EST_FISSO_AREA_1_FIELD, offertaFisso.getCallEstFissoArea1());

        db.insert(WIND_CATALOGO_OFFERTE_FISSO_TABLE_NAME, null, values);
        db.close();
    }

    private static String createQueryOffers(String type) {

        String SELECT = "SELECT *";
        String FROM = " FROM " + WIND_CATALOGO_OFFERTE_FISSO_TABLE_NAME;
        String WHERE = " WHERE UPPER(" + OFFER_TYPE_FIELD + ") in ('" + (type.equals(WIND_SMART_OFFICE) ? type : "ALL INCLUSIVE AZIENDE','ALL INCLUSIVE BUSINESS") + "')";
        String ORDERBY = " ORDER BY CAST(" + CANONE_MENSILE_FIELD + " as INT) desc;";

        return SELECT + FROM + WHERE + ORDERBY;
    }

    public static ArrayList<OffertaFisso> getOfferte(Context context, String type) {
        ArrayList<OffertaFisso> offerte = new ArrayList<>();


        SQLiteDatabase db = DbHelper.getInstance(context).getWritableDatabase();
        Cursor cursor = db.rawQuery(createQueryOffers(type), null);
        if (cursor.moveToFirst()) {
            do {
                OffertaFisso offerta = new OffertaFisso();
                offerta.setIdOfferta(cursor.getInt(cursor.getColumnIndex(ID_OFFERTA_FIELD)));
                offerta.setOfferDescription(cursor.getString(cursor.getColumnIndex(OFFER_DESCRIPTION_FIELD)));
                offerta.setOfferType(cursor.getString(cursor.getColumnIndex(OFFER_TYPE_FIELD)));
                offerta.setTechnology(cursor.getString(cursor.getColumnIndex(TECHNOLOGY_FIELD)));
                offerta.setContributoUTNuovaLinea(cursor.getString(cursor.getColumnIndex(CONTRIBUTO_UT_NUOVA_LINEA_FIELD)));
                offerta.setCanoneMensile(cursor.getString(cursor.getColumnIndex(CANONE_MENSILE_FIELD)));
                offerta.setCanoneBimestrale(cursor.getString(cursor.getColumnIndex(CANONE_BIMESTRALE_FIELD)));
                offerta.setCanoneBimestraleConv(cursor.getString(cursor.getColumnIndex(CANONE_BIMESTRALE_CONV_FIELD)));
                offerta.setCanonePromo(cursor.getString(cursor.getColumnIndex(CANONE_PROMO_FIELD)));
                offerta.setCallItaFissoWind(cursor.getString(cursor.getColumnIndex(CALL_ITA_FISSO_WIND_FIELD)));
                offerta.setCallItaMobile(cursor.getString(cursor.getColumnIndex(CALL_ITA_MOBILE_FIELD)));
                offerta.setCallItaMobileWind(cursor.getString(cursor.getColumnIndex(CALL_ITA_MOBILE_WIND_FIELD)));
                offerta.setCallEstFissoArea1(cursor.getString(cursor.getColumnIndex(CALL_EST_FISSO_AREA_1_FIELD)));
                offerte.add(offerta);
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();

        return offerte;
    }

}
