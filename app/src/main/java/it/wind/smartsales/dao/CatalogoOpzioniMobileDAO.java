package it.wind.smartsales.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import it.wind.smartsales.commons.Log;
import it.wind.smartsales.entities.OffertaMobile;
import it.wind.smartsales.entities.OpzioneMobile;

/**
 * Created by luca.quaranta on 22/03/2016.
 */
public class CatalogoOpzioniMobileDAO {

    public final static String WIND_CATALOGO_OPZIONI_MOBILE_TABLE_NAME = "CATALOGO_OPZIONI_MOBILE";

    public final static String ID_OPZIONE_FIELD = "ID_OPZIONE";
    public final static String OFFER_TYPE_FIELD = "OFFER_TYPE";
    public final static String OPZIONE_DESC_FIELD = "OPZIONE_DESC";
    public final static String OPZIONE_DESC_UPPER_FIELD = "OPZIONE_DESC_UPPER";
    public final static String OPZIONE_DESCRIPTION_I_BUTTON_FIELD = "OPZIONE_DESCRIPTION_I_BUTTON";
    public final static String ID_OFFERTA_FIELD = "ID_OFFERTA";
    public final static String CANONE_MENSILE_FIELD = "CANONE_MENSILE";
    public final static String CANONE_SETTIMANALE_FIELD = "CANONE_SETTIMANALE";
    public final static String CANONE_BIMESTRALE_FIELD = "CANONE_BIMESTRALE";
    public final static String VOCE_FIELD = "VOCE";
    public final static String SMS_FIELD = "SMS";
    public final static String DATI_FIELD = "DATI";
    public final static String SCONTO_CONVERGENZA_FIELD = "SCONTO_CONVERGENZA";
    public final static String COSTO_UNA_TANTUM_FIELD = "COSTO_UNA_TANTUM";
    public final static String IS_DEFAULT_FIELD = "IS_DEFAULT";
    public final static String IS_DOUBLE_CHECKED_FIELD = "IS_DOUBLE_CHECKED";

    public final static String ID_OPZIONE = ID_OPZIONE_FIELD + " VARCHAR(50)";
    public final static String OFFER_TYPE = OFFER_TYPE_FIELD + " VARCHAR(50)";
    public final static String OPZIONE_DESC = OPZIONE_DESC_FIELD + " VARCHAR(255)";
    public final static String OPZIONE_DESC_UPPER = OPZIONE_DESC_UPPER_FIELD + " VARCHAR(255)";
    public final static String OPZIONE_DESCRIPTION_I_BUTTON = OPZIONE_DESCRIPTION_I_BUTTON_FIELD + " VARCHAR(255)";
    public final static String ID_OFFERTA = ID_OFFERTA_FIELD + " VARCHAR(50)";
    public final static String CANONE_MENSILE = CANONE_MENSILE_FIELD + " VARCHAR(50)";
    public final static String CANONE_SETTIMANALE = CANONE_SETTIMANALE_FIELD + " VARCHAR(50)";
    public final static String CANONE_BIMESTRALE = CANONE_BIMESTRALE_FIELD + " VARCHAR(50)";
    public final static String VOCE = VOCE_FIELD + " VARCHAR(50)";
    public final static String SMS = SMS_FIELD + " VARCHAR(50)";
    public final static String DATI = DATI_FIELD + " VARCHAR(50)";
    public final static String SCONTO_CONVERGENZA = SCONTO_CONVERGENZA_FIELD + " VARCHAR(50)";
    public final static String COSTO_UNA_TANTUM = COSTO_UNA_TANTUM_FIELD + " VARCHAR(50)";
    public final static String IS_DEFAULT = IS_DEFAULT_FIELD + " VARCHAR(50)";
    public final static String IS_DOUBLE_CHECKED = IS_DOUBLE_CHECKED_FIELD + " VARCHAR(50)";

    public final static List<String> fields = new ArrayList<String>();

    static {
        fields.add(ID_OPZIONE);
        fields.add(OFFER_TYPE);
        fields.add(OPZIONE_DESC);
        fields.add(OPZIONE_DESC_UPPER);
        fields.add(OPZIONE_DESCRIPTION_I_BUTTON);
        fields.add(ID_OFFERTA);
        fields.add(CANONE_MENSILE);
        fields.add(CANONE_SETTIMANALE);
        fields.add(CANONE_BIMESTRALE);
        fields.add(VOCE);
        fields.add(SMS);
        fields.add(DATI);
        fields.add(SCONTO_CONVERGENZA);
        fields.add(COSTO_UNA_TANTUM);
        fields.add(IS_DEFAULT);
        fields.add(IS_DOUBLE_CHECKED);
    }

    public static void clearOpzioniMobileTable(Context context){
        SQLiteDatabase db = DbHelper.getInstance(context).getWritableDatabase();
        db.execSQL("DELETE FROM "+ WIND_CATALOGO_OPZIONI_MOBILE_TABLE_NAME);
        db.close();
    }

    public static void insertOpzioneMobile(Context context, OpzioneMobile opzioneMobile){
        SQLiteDatabase db = DbHelper.getInstance(context).getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(ID_OPZIONE_FIELD, opzioneMobile.getIdOpzione());
        values.put(OFFER_TYPE_FIELD, opzioneMobile.getOfferType());
        values.put(OPZIONE_DESC_FIELD, opzioneMobile.getOpzioneDesc());
        values.put(OPZIONE_DESC_UPPER_FIELD, opzioneMobile.getOpzioneDescUpper());
        values.put(OPZIONE_DESCRIPTION_I_BUTTON_FIELD, opzioneMobile.getOpzioneDescriptionIButton());
        values.put(ID_OFFERTA_FIELD, opzioneMobile.getIdOfferta());
        values.put(CANONE_MENSILE_FIELD, opzioneMobile.getCanoneMensile());
        values.put(CANONE_SETTIMANALE_FIELD, opzioneMobile.getCanoneSettimanale());
        values.put(CANONE_BIMESTRALE_FIELD, opzioneMobile.getCanoneBimestrale());
        values.put(VOCE_FIELD, opzioneMobile.getVoce());
        values.put(SMS_FIELD, opzioneMobile.getSms());
        values.put(DATI_FIELD, opzioneMobile.getDati());
        values.put(SCONTO_CONVERGENZA_FIELD, opzioneMobile.getScontoConvergenza());
        values.put(COSTO_UNA_TANTUM_FIELD, opzioneMobile.getCostoUnaTantum());
        values.put(IS_DEFAULT_FIELD, opzioneMobile.getIsDefault());
        values.put(IS_DOUBLE_CHECKED_FIELD, opzioneMobile.getIsDoubleChecked());

        db.insert(WIND_CATALOGO_OPZIONI_MOBILE_TABLE_NAME, null, values);
        db.close();
    }

    public static ArrayList<OpzioneMobile> retrieveOpzioniMobile(Context context, OffertaMobile offertaMobile){
        SQLiteDatabase db = DbHelper.getInstance(context).getWritableDatabase();

        String query = "SELECT " + ID_OPZIONE_FIELD + ", " +
                OFFER_TYPE_FIELD + ", " +
                OPZIONE_DESC_FIELD + ", " +
                OPZIONE_DESC_UPPER_FIELD + ", " +
                OPZIONE_DESCRIPTION_I_BUTTON_FIELD + ", " +
                ID_OFFERTA_FIELD + ", " +
                CANONE_MENSILE_FIELD + ", " +
                CANONE_SETTIMANALE_FIELD + ", " +
                CANONE_BIMESTRALE_FIELD + ", " +
                VOCE_FIELD + ", " +
                SMS_FIELD + ", " +
                DATI_FIELD + ", " +
                SCONTO_CONVERGENZA_FIELD + ", " +
                COSTO_UNA_TANTUM_FIELD + ", " +
                IS_DEFAULT_FIELD + ", " +
                IS_DOUBLE_CHECKED_FIELD +
                " FROM " + WIND_CATALOGO_OPZIONI_MOBILE_TABLE_NAME + " WHERE " + ID_OFFERTA_FIELD + " = '" + offertaMobile.getIdOfferta() + "'" +
                " ORDER BY " + SCONTO_CONVERGENZA_FIELD + " DESC";
        Cursor cursor = db.rawQuery(query, null);

        ArrayList<OpzioneMobile> opzioneMobiles = new ArrayList<>();
        if (cursor.moveToFirst()) {
            do {
                OpzioneMobile opzioneMobile = new OpzioneMobile();
                opzioneMobile.setIdOpzione(cursor.getString(cursor.getColumnIndex(ID_OPZIONE_FIELD)));
                opzioneMobile.setOfferType(cursor.getString(cursor.getColumnIndex(OFFER_TYPE_FIELD)));
                opzioneMobile.setOpzioneDesc(cursor.getString(cursor.getColumnIndex(OPZIONE_DESC_FIELD)));
                opzioneMobile.setOpzioneDescUpper(cursor.getString(cursor.getColumnIndex(OPZIONE_DESC_UPPER_FIELD)));
                opzioneMobile.setOpzioneDescriptionIButton(cursor.getString(cursor.getColumnIndex(OPZIONE_DESCRIPTION_I_BUTTON_FIELD)));
                opzioneMobile.setIdOfferta(cursor.getString(cursor.getColumnIndex(ID_OFFERTA_FIELD)));
                opzioneMobile.setCanoneMensile(cursor.getString(cursor.getColumnIndex(CANONE_MENSILE_FIELD)));
                opzioneMobile.setCanoneSettimanale(cursor.getString(cursor.getColumnIndex(CANONE_SETTIMANALE_FIELD)));
                opzioneMobile.setCanoneBimestrale(cursor.getString(cursor.getColumnIndex(CANONE_BIMESTRALE_FIELD)));
                opzioneMobile.setVoce(cursor.getString(cursor.getColumnIndex(VOCE_FIELD)));
                opzioneMobile.setSms(cursor.getString(cursor.getColumnIndex(SMS_FIELD)));
                opzioneMobile.setDati(cursor.getString(cursor.getColumnIndex(DATI_FIELD)));
                opzioneMobile.setScontoConvergenza(cursor.getString(cursor.getColumnIndex(SCONTO_CONVERGENZA_FIELD)));
                opzioneMobile.setCostoUnaTantum(cursor.getString(cursor.getColumnIndex(COSTO_UNA_TANTUM_FIELD)));
                opzioneMobile.setIsDefault(cursor.getString(cursor.getColumnIndex(IS_DEFAULT_FIELD)));
                opzioneMobile.setIsDoubleChecked(cursor.getString(cursor.getColumnIndex(IS_DOUBLE_CHECKED_FIELD)));
                opzioneMobiles.add(opzioneMobile);
            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();
        return opzioneMobiles;
    }

    public static ArrayList<OpzioneMobile> retrieveOpzioniMobileFisse(Context context, String offerType){
        SQLiteDatabase db = DbHelper.getInstance(context).getWritableDatabase();

        String query = "SELECT " +
                ID_OPZIONE_FIELD + ", " +
                OFFER_TYPE_FIELD + ", " +
                OPZIONE_DESC_FIELD + ", " +
                OPZIONE_DESC_UPPER_FIELD + ", " +
                OPZIONE_DESCRIPTION_I_BUTTON_FIELD + ", " +
                ID_OFFERTA_FIELD + ", " +
                CANONE_MENSILE_FIELD + ", " +
                CANONE_SETTIMANALE_FIELD + ", " +
                CANONE_BIMESTRALE_FIELD + ", " +
                VOCE_FIELD + ", " +
                SMS_FIELD + ", " +
                DATI_FIELD + ", " +
                SCONTO_CONVERGENZA_FIELD + ", " +
                COSTO_UNA_TANTUM_FIELD + ", " +
                IS_DEFAULT_FIELD + ", " +
                IS_DOUBLE_CHECKED_FIELD +
                " FROM " + WIND_CATALOGO_OPZIONI_MOBILE_TABLE_NAME + " WHERE " + "UPPER(" + OFFER_TYPE_FIELD + ") = '" + offerType.toUpperCase() + "'" +
                " AND " + IS_DEFAULT_FIELD  + " = 'true'";

        Log.d("QUERY", "retrieveOpzioniMobileFisse: " + query);

        Cursor cursor = db.rawQuery(query, null);

        ArrayList<OpzioneMobile> opzioniMobileFisse = new ArrayList<>();
        if (cursor.moveToFirst()) {
            do {
                OpzioneMobile opzioneMobile = new OpzioneMobile();
                opzioneMobile.setIdOpzione(cursor.getString(cursor.getColumnIndex(ID_OPZIONE_FIELD)));
                opzioneMobile.setOfferType(cursor.getString(cursor.getColumnIndex(OFFER_TYPE_FIELD)));
                opzioneMobile.setOpzioneDesc(cursor.getString(cursor.getColumnIndex(OPZIONE_DESC_FIELD)));
                opzioneMobile.setOpzioneDescUpper(cursor.getString(cursor.getColumnIndex(OPZIONE_DESC_UPPER_FIELD)));
                opzioneMobile.setOpzioneDescriptionIButton(cursor.getString(cursor.getColumnIndex(OPZIONE_DESCRIPTION_I_BUTTON_FIELD)));
                opzioneMobile.setIdOfferta(cursor.getString(cursor.getColumnIndex(ID_OFFERTA_FIELD)));
                opzioneMobile.setCanoneMensile(cursor.getString(cursor.getColumnIndex(CANONE_MENSILE_FIELD)));
                opzioneMobile.setCanoneSettimanale(cursor.getString(cursor.getColumnIndex(CANONE_SETTIMANALE_FIELD)));
                opzioneMobile.setCanoneBimestrale(cursor.getString(cursor.getColumnIndex(CANONE_BIMESTRALE_FIELD)));
                opzioneMobile.setVoce(cursor.getString(cursor.getColumnIndex(VOCE_FIELD)));
                opzioneMobile.setSms(cursor.getString(cursor.getColumnIndex(SMS_FIELD)));
                opzioneMobile.setDati(cursor.getString(cursor.getColumnIndex(DATI_FIELD)));
                opzioneMobile.setScontoConvergenza(cursor.getString(cursor.getColumnIndex(SCONTO_CONVERGENZA_FIELD)));
                opzioneMobile.setCostoUnaTantum(cursor.getString(cursor.getColumnIndex(COSTO_UNA_TANTUM_FIELD)));
                opzioneMobile.setIsDefault(cursor.getString(cursor.getColumnIndex(IS_DEFAULT_FIELD)));
                opzioneMobile.setIsDoubleChecked(cursor.getString(cursor.getColumnIndex(IS_DOUBLE_CHECKED_FIELD)));
                opzioniMobileFisse.add(opzioneMobile);
            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();
        return opzioniMobileFisse;
    }

}
