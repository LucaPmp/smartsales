package it.wind.smartsales.customviews.selectoffer;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import it.wind.smartsales.R;
import it.wind.smartsales.entities.AddOnMobile;
import it.wind.smartsales.entities.OffertaMobile;

/**
 * Created by luca.quaranta on 13/11/2015.
 */
public class SelectGbAdapter extends RecyclerView.Adapter<SelectGbAdapter.ViewHolder> {

    private ArrayList<AddOnMobile> addOnMobileArrayList;
    //    private AddOnMobile addOnMobile;
    private OffertaMobile offertaMobile;
    private SelectGbDialogFragment gbDialogFragment;
//    private String text;

//    private List<String> alfabeto = Arrays.asList("A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z");

    public SelectGbAdapter(SelectGbDialogFragment gbDialogFragment, OffertaMobile offertaMobile, ArrayList<AddOnMobile> addOnMobileArrayList) {
        this.gbDialogFragment = gbDialogFragment;
//        this.addOnMobileList = offertaMobile.getAddOnMobileArrayList();
        this.addOnMobileArrayList = addOnMobileArrayList;
        this.offertaMobile = offertaMobile;
//        this.text = text;

//        for (AddOnMobile addOnMobile : offertaMobile.getAddOnMobileArrayList()) {
//            if (addOnMobile.getDescFascia().compareToIgnoreCase(text.replace("\n", "")) == 0) {
//                this.addOnMobile = addOnMobile;
//            }
//        }

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_mobile_gb, parent, false);
        // set the view's size, margins, paddings and layout parameters

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        final AddOnMobile addOnMobile = addOnMobileArrayList.get(position);

        holder.label.setText(addOnMobile.getSelectedLabelStep8());
        holder.number.setText(addOnMobile.getSelectedNumSimStep8() + "/" + addOnMobile.getMaxSimVendibile());

        if (addOnMobile.getSelectedNumSimStep8() == Integer.parseInt(addOnMobile.getMaxSimVendibile())) {
            holder.gb.setTextColor(holder.gb.getContext().getResources().getColor(android.R.color.white));
            holder.gb.setText(addOnMobile.getDescFascia().replace("GB", "\nGB"));
            holder.gb.setSelected(false);
            holder.gb.setEnabled(false);
        } else {
            holder.gb.setTextColor(holder.gb.getContext().getResources().getColor(R.color.colorAccent));
            holder.gb.setText(addOnMobile.getDescFascia().replace("GB", "\nGB"));
            holder.gb.setSelected(false);
            holder.gb.setEnabled(true);
        }

        if (offertaMobile.getAddOnMobileStep8() != null && addOnMobile.getDescFascia().compareToIgnoreCase(offertaMobile.getAddOnMobileStep8().getDescFascia()) == 0 && addOnMobile.getSelectedLabelStep8().compareToIgnoreCase(offertaMobile.getAddOnMobileStep8().getSelectedLabelStep8()) == 0) {
            holder.gb.setTextColor(holder.gb.getContext().getResources().getColor(android.R.color.white));
            holder.gb.setText(addOnMobile.getDescFascia().replace("GB", "\nGB"));
            holder.gb.setSelected(true);
            holder.gb.setEnabled(true);
        }

//        holder.gb.setTextColor(holder.gb.getContext().getResources().getColor(R.color.colorAccent));
//        holder.gb.setText(text);
//        holder.gb.setSelected(false);
//        holder.gb.setEnabled(true);


        if (Integer.parseInt(addOnMobile.getMaxSimVendibile()) > 500) {
            holder.number.setVisibility(View.GONE);
        }


    }

//    private String createLabel(int num) {
//        int Base = alfabeto.size();
//        String label = "";
//
//        if (num < Base) {
//            int resto = num % Base;
//            label += alfabeto.get(resto);
//        } else if (num < ((Base * Base) + Base)) {
//            int quoziente = num / Base;
//            int resto = num % Base;
//            label += alfabeto.get(quoziente - 1) + alfabeto.get(resto);
//        } else {
//            int quoziente = (num / Base) / Base;
//            int quoto = quoziente % Base;
//            int resto = num % Base;
//            label += alfabeto.get(quoziente - 1) + alfabeto.get(quoto - 1) + alfabeto.get(resto);
//        }
//
//        return label;
//    }

    @Override
    public int getItemCount() {
        return addOnMobileArrayList.size();
    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public LinearLayout itemLayout;
        public Button gb;
        public TextView label;
        public TextView number;

        public ViewHolder(View v) {
            super(v);
            itemLayout = (LinearLayout) v.findViewById(R.id.item_layout);
            gb = (Button) v.findViewById(R.id.gb);
            label = (TextView) v.findViewById(R.id.label);
            number = (TextView) v.findViewById(R.id.number);

            gb.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (!gb.isSelected()) {
                        if (offertaMobile.getAddOnMobileStep8() == null) {
                            gb.setSelected(true);
                            gb.setTextColor(gb.getContext().getResources().getColor(android.R.color.white));
                            offertaMobile.setAddOnMobileStep8(addOnMobileArrayList.get(getAdapterPosition()));
                            addOnMobileArrayList.get(getAdapterPosition()).setSelectedNumSimStep8(addOnMobileArrayList.get(getAdapterPosition()).getSelectedNumSimStep8() + 1);
                            offertaMobile.getAddOnMobileStep8().setSelectedLabelStep8(label.getText().toString());
                            gbDialogFragment.updateAddOnList();
                        }
                    } else {
                        if (offertaMobile.getAddOnMobileStep8() != null) {
                            gb.setSelected(false);
                            gb.setTextColor(gb.getContext().getResources().getColor(R.color.colorAccent));
                            offertaMobile.setAddOnMobileStep8(null);
                            addOnMobileArrayList.get(getAdapterPosition()).setSelectedNumSimStep8(addOnMobileArrayList.get(getAdapterPosition()).getSelectedNumSimStep8() - 1);
                            gbDialogFragment.updateAddOnList();
                        }
                    }
                    notifyDataSetChanged();
                }
            });
        }
    }
}
