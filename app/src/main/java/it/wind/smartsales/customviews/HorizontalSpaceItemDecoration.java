package it.wind.smartsales.customviews;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by luca.quaranta on 13/04/2016.
 */
public class HorizontalSpaceItemDecoration extends RecyclerView.ItemDecoration {

    private final int mHorizontalSpaceWidth;

    public HorizontalSpaceItemDecoration(int mHorizontalSpaceWidth) {
        this.mHorizontalSpaceWidth = mHorizontalSpaceWidth;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent,
                               RecyclerView.State state) {
        outRect.right = mHorizontalSpaceWidth;
    }
}
