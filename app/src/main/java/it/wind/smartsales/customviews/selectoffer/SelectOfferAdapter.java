package it.wind.smartsales.customviews.selectoffer;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import it.wind.smartsales.R;
import it.wind.smartsales.customviews.AspectRatioView;
import it.wind.smartsales.entities.OffertaMobile;
import it.wind.smartsales.fragments.selectoffer.MobileInternetSelectOfferStep2Fragment;

/**
 * Created by luca.quaranta on 13/11/2015.
 */
public class SelectOfferAdapter extends RecyclerView.Adapter<SelectOfferAdapter.ViewHolder> {

    private ArrayList<OffertaMobile> offerteMobile;
    private ArrayList<OffertaMobile> offerteSelezionate;
    private MobileInternetSelectOfferStep2Fragment mobileInternetCreateOfferStep2Fragment;

    public SelectOfferAdapter(MobileInternetSelectOfferStep2Fragment mobileInternetCreateOfferStep2Fragment, ArrayList<OffertaMobile> offerteMobile, ArrayList<OffertaMobile> offerteSelezionate) {
        this.mobileInternetCreateOfferStep2Fragment = mobileInternetCreateOfferStep2Fragment;
        this.offerteMobile = offerteMobile;
        this.offerteSelezionate = offerteSelezionate;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_mobile_offer, parent, false);
        // set the view's size, margins, paddings and layout parameters

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        final OffertaMobile offertaMobile = offerteMobile.get(position);

        for (OffertaMobile offertaMobile1 : offerteSelezionate){
            if (offertaMobile1.getIdOfferta() == offertaMobile.getIdOfferta()){
                holder.item.setSelected(true);
                break;
            }
            holder.item.setSelected(false);
        }
//        holder.item.setSelected(offerteSelezionate.contains(offertaMobile));

        holder.offerDescription1.setText(offertaMobile.getOfferDescription());
        holder.offerDescription2.setText(offertaMobile.getOfferDescriptionUpper());
        holder.canone.setText(offertaMobile.getCanoneMensile().replace(".00", "") + "€");

        holder.item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.setSelected(mobileInternetCreateOfferStep2Fragment.saveSelectedOffer(!v.isSelected(), offertaMobile));
//                mobileInternetCreateOfferStep2Fragment.saveSelectedOffer(v.isSelected(), offertaMobile.getIdOfferta(), offertaMobile.getOfferType());
                notifyDataSetChanged();
            }
        });

        holder.infoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mobileInternetCreateOfferStep2Fragment.openInfo(offertaMobile);
            }
        });

    }

    public void updateOfferteSelezionate(ArrayList<OffertaMobile> offerteSelezionate){
        this.offerteSelezionate = offerteSelezionate;
    }

    @Override
    public int getItemCount() {
        return offerteMobile.size();
    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public LinearLayout item;
        public TextView offerDescription1;
        public TextView offerDescription2;
        public TextView canone;
        public AspectRatioView infoButton;

        public ViewHolder(View v) {
            super(v);
            item = (LinearLayout) v.findViewById(R.id.item_layout);
            offerDescription1 = (TextView) v.findViewById(R.id.offer_description_1);
            offerDescription2 = (TextView) v.findViewById(R.id.offer_description_2);
            canone = (TextView) v.findViewById(R.id.canone);
            infoButton = (AspectRatioView) v.findViewById(R.id.info_button);
        }
    }
}
