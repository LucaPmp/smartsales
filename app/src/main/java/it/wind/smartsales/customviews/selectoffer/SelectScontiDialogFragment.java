package it.wind.smartsales.customviews.selectoffer;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import it.wind.smartsales.R;
import it.wind.smartsales.fragments.selectoffer.MobileInternetSelectOfferStep4Fragment;

/**
 * Created by luca.quaranta on 03/12/2015.
 */
public class SelectScontiDialogFragment extends DialogFragment {

    public static final String KEY = "MESSAGE_KEY";

    private TextView message;
    private Button indietroButton, continuaButton, closeButton;
    private MobileInternetSelectOfferStep4Fragment mobileInternetCreateOfferStep4Fragment;
    private LinearLayout buttonLayout;

    private void initializeComponents(View view) {
        message = (TextView) view.findViewById(R.id.message);
        indietroButton = (Button) view.findViewById(R.id.indietro_button);
        continuaButton = (Button) view.findViewById(R.id.continua_button);
        closeButton = (Button) view.findViewById(R.id.close_button);
        buttonLayout = (LinearLayout) view.findViewById(R.id.button_layout);
    }

    public SelectScontiDialogFragment() {
    }

    @SuppressLint("ValidFragment")
    public SelectScontiDialogFragment(MobileInternetSelectOfferStep4Fragment mobileInternetCreateOfferStep4Fragment) {
        this.mobileInternetCreateOfferStep4Fragment = mobileInternetCreateOfferStep4Fragment;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    @Override
    public void onResume() {
        super.onResume();
        int width = getResources().getDimensionPixelSize(R.dimen.fragment_dialog_sconti_width);
        getDialog().getWindow().setLayout(width, ViewGroup.LayoutParams.WRAP_CONTENT);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_dialog_sconti, container, false);

        initializeComponents(view);

        int type = getArguments().getInt(KEY);

        switch (type) {
            case 0:
                buttonLayout.setVisibility(View.VISIBLE);
                closeButton.setVisibility(View.GONE);
                break;
            case 1:
                message.setText("Il numero di SIM selezionate è inferiore alla soglia minima consentita per l'applicazione degli sconti.");
                buttonLayout.setVisibility(View.GONE);
                closeButton.setVisibility(View.VISIBLE);
                break;
            case 2:
                message.setText("Il numero di SIM selezionate è superiore alla soglia massima consentita per l'applicazione degli sconti.");
                buttonLayout.setVisibility(View.GONE);
                closeButton.setVisibility(View.VISIBLE);
                break;
        }

        indietroButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        continuaButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mobileInternetCreateOfferStep4Fragment.goForward();
                dismiss();
            }
        });

        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        return view;
    }


}
