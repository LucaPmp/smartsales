package it.wind.smartsales.customviews;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.ImageView;

import static android.view.View.MeasureSpec.makeMeasureSpec;
import static android.view.ViewGroup.LayoutParams.WRAP_CONTENT;

/**
 * An implementation of View that maintains the aspect ratio size of its
 * background image.
 *
 * @author luca.quaranta
 */
public class AspectRatioView extends ImageView {

    public AspectRatioView(final Context context, final AttributeSet attrs,
            final int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public AspectRatioView(final Context context, final AttributeSet attrs) {
        super(context, attrs);
    }

    public AspectRatioView(final Context context) {
        super(context);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        final Drawable drawable = getBackground();

        if (drawable != null) {

            final int intrinsictWidth = drawable.getIntrinsicWidth();
            final int intrinsictHeight = drawable.getIntrinsicHeight();

            if (getLayoutParams().height == WRAP_CONTENT) {
                final int width = MeasureSpec.getSize(widthMeasureSpec);
                final int height = width * intrinsictHeight / intrinsictWidth;

                widthMeasureSpec = makeMeasureSpec(width, MeasureSpec.EXACTLY);
                heightMeasureSpec = makeMeasureSpec(height, MeasureSpec.EXACTLY);
            } else if (getLayoutParams().width == WRAP_CONTENT) {
                final int height = MeasureSpec.getSize(heightMeasureSpec);
                final int width = height * intrinsictWidth / intrinsictHeight;

                widthMeasureSpec = makeMeasureSpec(width, MeasureSpec.EXACTLY);
                heightMeasureSpec = makeMeasureSpec(height, MeasureSpec.EXACTLY);
            }
        }

        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }
}
