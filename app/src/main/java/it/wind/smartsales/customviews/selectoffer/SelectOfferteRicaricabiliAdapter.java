package it.wind.smartsales.customviews.selectoffer;

import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;

import it.wind.smartsales.R;
import it.wind.smartsales.commons.Utils;
import it.wind.smartsales.customviews.AspectRatioView;
import it.wind.smartsales.entities.OffertaMobile;
import it.wind.smartsales.entities.OpzioneMobile;
import it.wind.smartsales.fragments.selectoffer.MobileInternetSelectOfferStep8Fragment;

/**
 * Created by luca.quaranta on 13/11/2015.
 */
public class SelectOfferteRicaricabiliAdapter extends RecyclerView.Adapter<SelectOfferteRicaricabiliAdapter.ViewHolder> {

    private ArrayList<OffertaMobile> offerte;
    private ArrayList<OpzioneMobile> opzioni;
    private MobileInternetSelectOfferStep8Fragment mobileInternetCreateOfferStep8Fragment;

    public SelectOfferteRicaricabiliAdapter(MobileInternetSelectOfferStep8Fragment mobileInternetCreateOfferStep8Fragment, ArrayList<OffertaMobile> offerte, ArrayList<OpzioneMobile> opzioni) {
        this.mobileInternetCreateOfferStep8Fragment = mobileInternetCreateOfferStep8Fragment;
        this.offerte = offerte;
        this.opzioni = opzioni;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_offerta_ricaricabile, parent, false);
        // set the view's size, margins, paddings and layout parameters

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        final OffertaMobile offertaMobile = offerte.get(position);

        holder.offertaDesc.setText(offertaMobile.getOfferDescription());
        holder.offertaDescUpper.setText(offertaMobile.getOfferDescriptionUpper());

        switch (offertaMobile.getMnpStep8()) {
            case "":
                holder.mnpButton.setBackground(holder.mnpButton.getContext().getResources().getDrawable(R.drawable.icon_plus));
                break;
            case "Tim":
                holder.mnpButton.setBackground(holder.mnpButton.getContext().getResources().getDrawable(R.drawable.operatore_tim));
                break;
            case "Vodafone":
                holder.mnpButton.setBackground(holder.mnpButton.getContext().getResources().getDrawable(R.drawable.operatore_vodafone));
                break;
            case "Tre":
                holder.mnpButton.setBackground(holder.mnpButton.getContext().getResources().getDrawable(R.drawable.operatore_tre));
                break;
            default:
                holder.mnpButton.setBackground(holder.mnpButton.getContext().getResources().getDrawable(R.drawable.operatore_altro));
                break;
        }

        if (offertaMobile.getAddOnMobileStep8() != null) {
            holder.gbButton.setBackground(holder.gbButton.getContext().getResources().getDrawable(R.drawable.gb_selector));
            holder.gbButton.setTextColor(holder.gbButton.getContext().getResources().getColor(android.R.color.white));
            holder.gbButton.setText(offertaMobile.getAddOnMobileStep8().getDescFascia().replace("GB", "\nGB"));
            holder.gbButton.setSelected(true);
        } else {
            holder.gbButton.setBackground(holder.gbButton.getContext().getResources().getDrawable(R.drawable.icon_plus));
            holder.gbButton.setTextColor(holder.gbButton.getContext().getResources().getColor(android.R.color.transparent));
            holder.gbButton.setText("");
            holder.gbButton.setSelected(false);
        }

        if (offertaMobile.isMnp()) {
            holder.mnpButton.setEnabled(true);
        } else {
            holder.mnpButton.setEnabled(false);
            holder.mnpButton.setBackground(holder.mnpButton.getContext().getResources().getDrawable(R.drawable.icon_plus_disabled));
        }

        if (offertaMobile.getOfferType().compareToIgnoreCase("Abbonamento") == 0) {
            holder.gbButton.setVisibility(View.GONE);
            holder.gbButton.setEnabled(false);
            holder.gbButton.setBackground(holder.mnpButton.getContext().getResources().getDrawable(R.drawable.icon_plus_disabled));
        }

        holder.iccid.setText(offertaMobile.getNewICCID());

        if (offertaMobile.getTerminale1() != null) {
            holder.device1.setText(offertaMobile.getTerminale1().getTerminaleBrand() + " " + offertaMobile.getTerminale1().getTerminaleDescription());
            holder.device1.setTextColor(holder.device1.getContext().getResources().getColor(android.R.color.black));
            holder.device1.setEnabled(true);
        } else {
            holder.device1.setText("Opzione 1");
            if (offertaMobile.getTerminaliToShow().size() == 0) {
                holder.device1.setTextColor(holder.device1.getContext().getResources().getColor(R.color.wind_color_light_gray));
                holder.device1.setEnabled(false);
            } else {
                holder.device1.setTextColor(holder.device1.getContext().getResources().getColor(android.R.color.black));
                holder.device1.setEnabled(true);
            }
        }

        for (int i = 0; i < opzioni.size(); i++) {
            for (OpzioneMobile opzioneMobile : offertaMobile.getOpzioniMobileSelectedArrayListStep8()) {
                if (((LinearLayout)holder.opzioniLayoutRicaricabile.getChildAt(i)).getChildAt(0) instanceof CheckBox) {
                    ((CheckBox) ((LinearLayout)holder.opzioniLayoutRicaricabile.getChildAt(i)).getChildAt(0)).setChecked(opzioneMobile.getIdOpzione().compareToIgnoreCase(opzioni.get(i).getIdOpzione()) == 0);
                }
            }
        }

        if (offertaMobile.getTerminale2() != null) {
            holder.device2.setText(offertaMobile.getTerminale2().getTerminaleBrand() + " " + offertaMobile.getTerminale2().getTerminaleDescription());
        } else {
            holder.device2.setText("Opzione 2");
        }
    }

//    public void updateTerminaliSelezionati(ArrayList<TerminaleBean> terminaliSelezionati){
//        this.terminaliSelezionati = terminaliSelezionati;
//    }

    @Override
    public int getItemCount() {
        return offerte.size();
    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public LinearLayout itemLayout;
        public LinearLayout opzioniLayoutRicaricabile;
        public TextView offertaDesc;
        public TextView offertaDescUpper;
        public AspectRatioView ocrButton;
        public AspectRatioView mnpButton;
        public Button gbButton;
        public EditText iccid;
        public TextView device1, device2;

        public ViewHolder(View v) {
            super(v);
            itemLayout = (LinearLayout) v.findViewById(R.id.item_layout);
            opzioniLayoutRicaricabile = (LinearLayout) v.findViewById(R.id.opzioni_layout_ricaricabile);
            offertaDesc = (TextView) v.findViewById(R.id.offerta_desc);
            offertaDescUpper = (TextView) v.findViewById(R.id.offerta_desc_upper);
            ocrButton = (AspectRatioView) v.findViewById(R.id.ocr_button);
            mnpButton = (AspectRatioView) v.findViewById(R.id.mnp_button);
            gbButton = (Button) v.findViewById(R.id.gb_button);
            iccid = (EditText) v.findViewById(R.id.iccid);
            device1 = (TextView) v.findViewById(R.id.device_1);
            device2 = (TextView) v.findViewById(R.id.device_2);

            ocrButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mobileInternetCreateOfferStep8Fragment.onOcrButtonClick(offerte.get(getAdapterPosition()));
                }
            });

            mnpButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mobileInternetCreateOfferStep8Fragment.onMnpButtonClick(offerte.get(getAdapterPosition()));
                }
            });

            gbButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mobileInternetCreateOfferStep8Fragment.onGbButtonClick(offerte.get(getAdapterPosition()));
                }
            });

            for (final OpzioneMobile opzioneMobile : opzioni) {
                LinearLayout linearLayout = new LinearLayout(v.getContext());
                LinearLayout.LayoutParams layoutParamsLL = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                layoutParamsLL.weight = 1f;
                linearLayout.setGravity(Gravity.CENTER);
                linearLayout.setLayoutParams(layoutParamsLL);

                if (!Boolean.parseBoolean(opzioneMobile.getIsDoubleChecked())) {
                    final CheckBox checkBox = new CheckBox(v.getContext());
                    checkBox.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mobileInternetCreateOfferStep8Fragment.onCheckBoxChecked(checkBox.isChecked(), offerte.get(getAdapterPosition()), opzioneMobile);
                        }
                    });
                    linearLayout.addView(checkBox);
                } else {
                    final Spinner spinner = new Spinner(v.getContext());
                    final ArrayList<String> lista = new ArrayList<>();
                    lista.add(" ");
                    lista.add("T");
                    lista.add("S");
                    LinearLayout.LayoutParams ll = new LinearLayout.LayoutParams(40, 40);
                    spinner.setBackground(v.getContext().getResources().getDrawable(R.drawable.white_shape_spinner));
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(v.getContext(), R.layout.spinner_textview_layout, lista);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spinner.setAdapter(adapter);
                    spinner.setLayoutParams(ll);

                    spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        int check = 0;

                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            check++;
                            if (check > 1) {
                                opzioneMobile.setValue(lista.get(position).trim());
                                if (!TextUtils.isEmpty(opzioneMobile.getValue())) {
                                    offerte.get(getAdapterPosition()).getOpzioniMobileSelectedArrayListStep8().add(opzioneMobile);
                                } else {
                                    offerte.get(getAdapterPosition()).getOpzioniMobileSelectedArrayListStep8().remove(opzioneMobile);
                                }
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });
                    linearLayout.addView(spinner);
                }


                opzioniLayoutRicaricabile.addView(linearLayout);
            }

            iccid.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    offerte.get(getAdapterPosition()).setNewICCID(iccid.getText().toString().trim());
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });

            iccid.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if (!hasFocus) {
                        if (iccid.getText().toString().trim().length() != 19) {
                            Utils.showWarningMessage(iccid.getContext(), "", "Il numero di serie SIM ICCID deve essere composto da 19 cifre");
                        }
                    }
                }
            });

            device1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mobileInternetCreateOfferStep8Fragment.openDeviceList(offerte.get(getAdapterPosition()), 1);
                }
            });

            device2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mobileInternetCreateOfferStep8Fragment.openDeviceList(offerte.get(getAdapterPosition()), 2);
                }
            });
        }
    }
}
