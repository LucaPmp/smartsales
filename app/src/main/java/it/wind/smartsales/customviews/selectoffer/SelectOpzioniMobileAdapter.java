package it.wind.smartsales.customviews.selectoffer;

import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.ArrayList;

import it.wind.smartsales.R;
import it.wind.smartsales.activity.MainActivity;
import it.wind.smartsales.commons.Utils;
import it.wind.smartsales.customviews.AspectRatioView;
import it.wind.smartsales.entities.OpzioneMobile;
import it.wind.smartsales.fragments.selectoffer.MobileInternetSelectOfferStep3Fragment;

/**
 * Created by luca.quaranta on 13/11/2015.
 */
public class SelectOpzioniMobileAdapter extends RecyclerView.Adapter<SelectOpzioniMobileAdapter.ViewHolder> {

    private ArrayList<OpzioneMobile> opzioniMobile;
    private ArrayList<OpzioneMobile> opzioniMobileSelezionate;
    private MobileInternetSelectOfferStep3Fragment mobileInternetCreateOfferStep3Fragment;

    public SelectOpzioniMobileAdapter(MobileInternetSelectOfferStep3Fragment mobileInternetCreateOfferStep3Fragment, ArrayList<OpzioneMobile> opzioniMobile, ArrayList<OpzioneMobile> opzioniMobileSelezionate) {
        this.mobileInternetCreateOfferStep3Fragment = mobileInternetCreateOfferStep3Fragment;
        this.opzioniMobile = opzioniMobile;
        this.opzioniMobileSelezionate = opzioniMobileSelezionate;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_mobile_opzioni, parent, false);
        // set the view's size, margins, paddings and layout parameters

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        final OpzioneMobile opzioneMobile = opzioniMobile.get(position);

        for (OpzioneMobile opzioneMobile1 : opzioniMobileSelezionate) {
            if (opzioneMobile1.getIdOpzione().compareToIgnoreCase(opzioneMobile.getIdOpzione()) == 0) {
                if (opzioneMobile1.getSelectedNumSimStep3() > 0){
                    holder.opzione.setSelected(true);
                } else {
                    holder.opzione.setSelected(false);
                }
                holder.opzioneNum.setText(new DecimalFormat("000").format(opzioneMobile1.getSelectedNumSimStep3()));
                break;
            } else {
                holder.opzione.setSelected(false);
                holder.opzioneNum.setText(new DecimalFormat("000").format(0));
            }
        }

        if (opzioniMobileSelezionate.isEmpty()){
            holder.opzione.setSelected(false);
            holder.opzioneNum.setText(new DecimalFormat("000").format(0));
        }

//        holder.addOn.setSelected(offerteSelezionate.contains(String.valueOf(offertaMobile.getIdOfferta())));

        holder.description.setText(opzioneMobile.getOpzioneDesc());
        holder.prezzo.setText(opzioneMobile.getCanoneMensile().replace(".00", "") + "€");

        if (!TextUtils.isEmpty(opzioneMobile.getScontoConvergenza())){
            holder.canoneLayout.setVisibility(View.GONE);
        } else {
            holder.canoneLayout.setVisibility(View.VISIBLE);
        }

//        holder.opzioneNum.setText(new DecimalFormat("000").format(opzioneMobile.getSelectedNumSimStep3()));

        holder.arrowUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                opzioneMobile.setSelectedNumSimStep3(mobileInternetCreateOfferStep3Fragment.incrementOpzioneMobileNumSim(opzioneMobile));
                notifyDataSetChanged();
            }
        });

        holder.arrowDown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                opzioneMobile.setSelectedNumSimStep3(mobileInternetCreateOfferStep3Fragment.decrementOpzioneMobileNumSim(opzioneMobile));
                notifyDataSetChanged();
            }
        });

        holder.info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.showInfoDialog((MainActivity)v.getContext(), opzioneMobile);
            }
        });

    }

    public void updateOpzioniMobileSelezionate(ArrayList<OpzioneMobile> opzioniMobileSelezionate){
        this.opzioniMobileSelezionate = opzioniMobileSelezionate;
    }

    @Override
    public int getItemCount() {
        return opzioniMobile.size();
    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public LinearLayout opzione;
        public TextView description;
        public TextView prezzo;
        public TextView opzioneNum;
        public AspectRatioView arrowUp;
        public AspectRatioView arrowDown;
        public AspectRatioView info;
        public LinearLayout canoneLayout;

        public ViewHolder(View v) {
            super(v);
            opzione = (LinearLayout) v.findViewById(R.id.opzione);
            canoneLayout = (LinearLayout) v.findViewById(R.id.canone_layout);
            description = (TextView) v.findViewById(R.id.opzione_desc);
            prezzo = (TextView) v.findViewById(R.id.opzione_prezzo);
            opzioneNum = (TextView) v.findViewById(R.id.opzione_num);
            arrowUp = (AspectRatioView) v.findViewById(R.id.opzione_arrow_up);
            arrowDown = (AspectRatioView) v.findViewById(R.id.opzione_arrow_down);
            info = (AspectRatioView) v.findViewById(R.id.info);
        }
    }
}
