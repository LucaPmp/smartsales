package it.wind.smartsales.customviews.selectoffer;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.ArrayList;

import it.wind.smartsales.customviews.AspectRatioView;
import it.wind.smartsales.entities.OffertaMobile;
import it.wind.smartsales.R;
import it.wind.smartsales.fragments.selectoffer.MobileInternetSelectOfferStep3Fragment;

/**
 * Created by luca.quaranta on 13/11/2015.
 */
public class SelectMobileOfferStep3Adapter extends RecyclerView.Adapter<SelectMobileOfferStep3Adapter.ViewHolder> {

    private ArrayList<OffertaMobile> offerteMobile;
    private ArrayList<String> offerteSelezionate;
    private MobileInternetSelectOfferStep3Fragment mobileInternetCreateOfferStep3Fragment;

    public SelectMobileOfferStep3Adapter(MobileInternetSelectOfferStep3Fragment mobileInternetCreateOfferStep3Fragment, ArrayList<OffertaMobile> offerteMobile, ArrayList<String> offerteSelezionate) {
        this.mobileInternetCreateOfferStep3Fragment = mobileInternetCreateOfferStep3Fragment;
        this.offerteMobile = offerteMobile;
        this.offerteSelezionate = offerteSelezionate;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view

        View v;

        if (viewType == 0) {
            v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_mobile_offer_step_3_selected, parent, false);
            return new ViewHolderSelected(v);
        } else {
            v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_mobile_offer_step_3_not_selected, parent, false);
            return new ViewHolderNotSelected(v);
        }
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        final OffertaMobile offertaMobile = offerteMobile.get(position);

        if (holder instanceof ViewHolderSelected) {
            holder.offerDescription1.setText(offertaMobile.getOfferDescription());
            holder.offerDescription2.setText(offertaMobile.getOfferDescriptionUpper());
            ((ViewHolderSelected) holder).canone.setText(offertaMobile.getCanoneMensile().replace(".00", "") + "€");
            ((ViewHolderSelected) holder).numSim.setText(new DecimalFormat("000").format(offertaMobile.getSelectedNumSimStep3()));

            ((ViewHolderSelected) holder).arrowUp.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    offertaMobile.setSelectedNumSimStep3(mobileInternetCreateOfferStep3Fragment.incrementOfferNumSim());
//                        ((ViewHolderSelected) holder).numSim.setText(new DecimalFormat("000").format(Integer.parseInt(((ViewHolderSelected) holder).numSim.getText().toString())+ 1));
                    notifyDataSetChanged();
                }
            });

            ((ViewHolderSelected) holder).arrowDown.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    offertaMobile.setSelectedNumSimStep3(mobileInternetCreateOfferStep3Fragment.decrementOfferNumSim());
//                        ((ViewHolderSelected) holder).numSim.setText(new DecimalFormat("000").format(Integer.parseInt(((ViewHolderSelected) holder).numSim.getText().toString()) - 1));
                    notifyDataSetChanged();
                }
            });

        } else {
            holder.offerDescription1.setText(offertaMobile.getOfferDescription());
            holder.offerDescription2.setText(offertaMobile.getOfferDescriptionUpper());
            ((ViewHolderNotSelected) holder).item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    for (OffertaMobile offertaMobile1 : offerteMobile) {
                        offertaMobile1.setSelectedStep3(false);
                    }
                    offertaMobile.setSelectedStep3(true);
                    mobileInternetCreateOfferStep3Fragment.populateRightSection(offertaMobile);
                    notifyDataSetChanged();
                }
            });
        }

//        holder.item.setSelected(offerteSelezionate.contains(String.valueOf(offertaMobile.getIdOfferta())));

//        holder.canone.setText(offertaMobile.getCanoneMensile() + "€");
//
//        holder.item.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                v.setSelected(!v.isSelected());
////                mobileInternetCreateOfferStep3Fragment.saveSelectedOffer(v.isSelected(), offertaMobile.getIdOfferta(), offertaMobile.getOfferType());
//                notifyDataSetChanged();
//            }
//        });
//
//        holder.infoButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
////                mobileInternetCreateOfferStep3Fragment.openInfo(offertaMobile.getOfferType());
//            }
//        });

    }

    public void updateOfferte(ArrayList<String> offerteSelezionate) {
        this.offerteSelezionate = offerteSelezionate;
    }

    @Override
    public int getItemCount() {
        return offerteMobile.size();
    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public LinearLayout item;
        public TextView offerDescription1;
        public TextView offerDescription2;
        public TextView canone;

        public ViewHolder(View v) {
            super(v);
            item = (LinearLayout) v.findViewById(R.id.item_layout);
            offerDescription1 = (TextView) v.findViewById(R.id.offer_description_1);
            offerDescription2 = (TextView) v.findViewById(R.id.offer_description_2);
            canone = (TextView) v.findViewById(R.id.canone);
        }
    }

    @Override
    public int getItemViewType(int position) {
        return offerteMobile.get(position).isSelectedStep3() ? 0 : 1;
    }

    class ViewHolderSelected extends ViewHolder {

        public LinearLayout item;
        public TextView offerDescription1;
        public TextView offerDescription2;
        public TextView canone;
        public TextView numSim;
        public AspectRatioView arrowUp;
        public AspectRatioView arrowDown;

        public ViewHolderSelected(View v) {
            super(v);
            item = (LinearLayout) v.findViewById(R.id.item_layout);
            offerDescription1 = (TextView) v.findViewById(R.id.offer_description_1);
            offerDescription2 = (TextView) v.findViewById(R.id.offer_description_2);
            canone = (TextView) v.findViewById(R.id.canone);
            numSim = (TextView) v.findViewById(R.id.offer_num);
            arrowUp = (AspectRatioView) v.findViewById(R.id.offer_arrow_right);
            arrowDown = (AspectRatioView) v.findViewById(R.id.offer_arrow_left);
        }
    }

    class ViewHolderNotSelected extends ViewHolder {

        public LinearLayout item;
        public TextView offerDescription1;
        public TextView offerDescription2;

        public ViewHolderNotSelected(View v) {
            super(v);
            item = (LinearLayout) v.findViewById(R.id.item_layout);
            offerDescription1 = (TextView) v.findViewById(R.id.offer_description_1);
            offerDescription2 = (TextView) v.findViewById(R.id.offer_description_2);
        }
    }
}
