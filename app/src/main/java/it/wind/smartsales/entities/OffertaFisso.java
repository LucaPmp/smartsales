package it.wind.smartsales.entities;

/**
 * Created by luca.quaranta on 22/03/2016.
 */
public class OffertaFisso extends Offerta {

    private int idOfferta;
    private String offerDescription;
    private String offerType;
    private String technology;
    private String contributoUTNuovaLinea;
    private String contributoUTLineaAttiva;
    private String canoneMensile;
    private String canoneBimestrale;
    private String canoneBimestraleConv;
    private String canonePromo;
    private String callItaFissoWind;
    private String callItaFisso;
    private String callItaMobileWind;
    private String callItaMobile;
    private String adslIta;
    private String callEstFissoArea1;
    private boolean selected;

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public int getIdOfferta() {
        return idOfferta;
    }

    public void setIdOfferta(int idOfferta) {
        this.idOfferta = idOfferta;
    }

    public String getOfferDescription() {
        return offerDescription;
    }

    public void setOfferDescription(String offerDescription) {
        this.offerDescription = offerDescription;
    }

    @Override
    public String getOfferDescriptionUpper() {
        return getOfferDescription().toUpperCase();
    }

    public String getOfferType() {
        return offerType;
    }

    public void setOfferType(String offerType) {
        this.offerType = offerType;
    }

    public String getTechnology() {
        return technology;
    }

    public void setTechnology(String technology) {
        this.technology = technology;
    }

    public String getContributoUTNuovaLinea() {
        return contributoUTNuovaLinea;
    }

    public void setContributoUTNuovaLinea(String contributoUTNuovaLinea) {
        this.contributoUTNuovaLinea = contributoUTNuovaLinea;
    }

    public String getContributoUTLineaAttiva() {
        return contributoUTLineaAttiva;
    }

    public void setContributoUTLineaAttiva(String contributoUTLineaAttiva) {
        this.contributoUTLineaAttiva = contributoUTLineaAttiva;
    }

    public String getCanoneMensile() {
        return canoneMensile;
    }

    public void setCanoneMensile(String canoneMensile) {
        this.canoneMensile = canoneMensile;
    }

    public String getCanoneBimestrale() {
        return canoneBimestrale;
    }

    public void setCanoneBimestrale(String canoneBimestrale) {
        this.canoneBimestrale = canoneBimestrale;
    }

    public String getCanoneBimestraleConv() {
        return canoneBimestraleConv;
    }

    public void setCanoneBimestraleConv(String canoneBimestraleConv) {
        this.canoneBimestraleConv = canoneBimestraleConv;
    }

    public String getCanonePromo() {
        return canonePromo;
    }

    public void setCanonePromo(String canonePromo) {
        this.canonePromo = canonePromo;
    }

    public String getCallItaFissoWind() {
        return callItaFissoWind;
    }

    public void setCallItaFissoWind(String callItaFissoWind) {
        this.callItaFissoWind = callItaFissoWind;
    }

    public String getCallItaFisso() {
        return callItaFisso;
    }

    public void setCallItaFisso(String callItaFisso) {
        this.callItaFisso = callItaFisso;
    }

    public String getCallItaMobileWind() {
        return callItaMobileWind;
    }

    public void setCallItaMobileWind(String callItaMobileWind) {
        this.callItaMobileWind = callItaMobileWind;
    }

    public String getCallItaMobile() {
        return callItaMobile;
    }

    public void setCallItaMobile(String callItaMobile) {
        this.callItaMobile = callItaMobile;
    }

    public String getAdslIta() {
        return adslIta;
    }

    public void setAdslIta(String adslIta) {
        this.adslIta = adslIta;
    }

    public String getCallEstFissoArea1() {
        return callEstFissoArea1;
    }

    public void setCallEstFissoArea1(String callEstFissoArea1) {
        this.callEstFissoArea1 = callEstFissoArea1;
    }
}
