package it.wind.smartsales.entities;

/**
 * Created by luca.quaranta on 26/04/2016.
 */
public class PercentualeSconto {

    private String percentualeSconto;

    public PercentualeSconto(){}

    public PercentualeSconto(String percentualeSconto){
        this.percentualeSconto = percentualeSconto;
    }

    public String getPercentualeSconto() {
        return percentualeSconto;
    }

    public void setPercentualeSconto(String percentualeSconto) {
        this.percentualeSconto = percentualeSconto;
    }
}
