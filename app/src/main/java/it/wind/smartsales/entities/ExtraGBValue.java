package it.wind.smartsales.entities;

/**
 * Created by luca.pompa on 10/05/2016.
 */
public class ExtraGBValue {

    private int value;
    private int totDetail;

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public int getTotDetail() {
        return totDetail;
    }

    public void setTotDetail(int totDetail) {
        this.totDetail = totDetail;
    }


}
