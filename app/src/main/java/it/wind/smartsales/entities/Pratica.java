package it.wind.smartsales.entities;

import org.json.JSONObject;

import java.util.Date;

/**
 * Created by luca.pompa on 10/05/2016.
 */
public class Pratica {

    private int id;
    private String request_id;
    private String nome_pratica;
    private JSONObject json_data;
    private String step;
    private String stato;
    private Date creation_date;
    private Date last_update;
    private String codice_dealer;
    private String note;
    private Date last_update_note;
    private int percentualeCompletamento=10;
    private boolean isNomeEditable=false;
    private String p_iva;
    private float scontoApplicato = 0f;
    private float totCanoneMensile = 0f;
    private float contributoUnaTan = 0f;

    public float getScontoApplicato() {
        return scontoApplicato;
    }

    public void setScontoApplicato(float scontoApplicato) {
        this.scontoApplicato = scontoApplicato;
    }

    public float getTotCanoneMensile() {
        return totCanoneMensile;
    }

    public void setTotCanoneMensile(float totCanoneMensile) {
        this.totCanoneMensile = totCanoneMensile;
    }

    public float getContributoUnaTan() {
        return contributoUnaTan;
    }

    public void setContributoUnaTan(float contributoUnaTan) {
        this.contributoUnaTan = contributoUnaTan;
    }



    public String getP_iva() {
        return p_iva;
    }

    public void setP_iva(String p_iva) {
        this.p_iva = p_iva;
    }



    public int getPercentualeCompletamento() {
        return percentualeCompletamento;
    }

    public void setPercentualeCompletamento(int percentualeCompletamento) {
        this.percentualeCompletamento = percentualeCompletamento;
    }





    public boolean isNomeEditable() {
        return isNomeEditable;
    }

    public void setIsNomeEditable(boolean isNomeEditable) {
        this.isNomeEditable = isNomeEditable;
    }



    public Pratica(int id, String request_id, String nome_pratica, JSONObject json_data, String step, String stato, Date creation_date, Date last_update, String codice_dealer, String note, Date last_update_note, boolean fuori_standard) {
        this.id = id;
        this.request_id = request_id;
        this.nome_pratica = nome_pratica;
        this.json_data = json_data;
        this.step = step;
        this.stato = stato;
        this.creation_date = creation_date;
        this.last_update = last_update;
        this.codice_dealer = codice_dealer;
        this.note = note;
        this.last_update_note = last_update_note;

    }

    public Pratica()
    {

    }

    public void setId(int id) {
        this.id = id;
    }

    public void setRequest_id(String request_id) {
        this.request_id = request_id;
    }

    public void setNome_pratica(String nome_pratica) {
        this.nome_pratica = nome_pratica;
    }

    public void setJson_data(JSONObject json_data) {
        this.json_data = json_data;
    }

    public void setStep(String step) {
        this.step = step;
    }

    public void setStato(String stato) {
        this.stato = stato;
    }

    public void setCreation_date(Date creation_date) {
        this.creation_date = creation_date;
    }

    public void setLast_update(Date last_update) {
        this.last_update = last_update;
    }

    public void setCodice_dealer(String codice_dealer) {
        this.codice_dealer = codice_dealer;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public void setLast_update_note(Date last_update_note) {
        this.last_update_note = last_update_note;
    }







    public int getId() {
        return id;
    }

    public String getRequest_id() {
        return request_id;
    }

    public String getNome_pratica() {
        return nome_pratica;
    }

    public JSONObject getJson_data() {
        return json_data;
    }

    public String getStep() {
        return step;
    }

    public String getStato() {
        return stato;
    }

    public Date getCreation_date() {
        return creation_date;
    }

    public Date getLast_update() {
        return last_update;
    }

    public String getCodice_dealer() {
        return codice_dealer;
    }

    public String getNote() {
        return note;
    }

    public Date getLast_update_note() {
        return last_update_note;
    }



}
