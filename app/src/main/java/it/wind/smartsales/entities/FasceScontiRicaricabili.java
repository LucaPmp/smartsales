package it.wind.smartsales.entities;

/**
 * Created by luca.quaranta on 04/04/2016.
 */
public class FasceScontiRicaricabili {


    private int fasciaSpesa1;
    private int fasciaSpesa2;
    private int fasciaSpesa3;
    private int fasciaSpesa4;
    private int fasciaSconto1;
    private int fasciaSconto2;
    private int fasciaSconto3;
    private int fasciaSconto4;

    public int getFasciaSpesa1() {
        return fasciaSpesa1;
    }

    public void setFasciaSpesa1(int fasciaSpesa1) {
        this.fasciaSpesa1 = fasciaSpesa1;
    }

    public int getFasciaSpesa2() {
        return fasciaSpesa2;
    }

    public void setFasciaSpesa2(int fasciaSpesa2) {
        this.fasciaSpesa2 = fasciaSpesa2;
    }

    public int getFasciaSpesa3() {
        return fasciaSpesa3;
    }

    public void setFasciaSpesa3(int fasciaSpesa3) {
        this.fasciaSpesa3 = fasciaSpesa3;
    }

    public int getFasciaSpesa4() {
        return fasciaSpesa4;
    }

    public void setFasciaSpesa4(int fasciaSpesa4) {
        this.fasciaSpesa4 = fasciaSpesa4;
    }

    public int getFasciaSconto1() {

        return fasciaSconto1;
    }

    public void setFasciaSconto1(int fasciaSconto1) {
        this.fasciaSconto1 = fasciaSconto1;
    }
    public int getFasciaSconto2() {

        return fasciaSconto2;
    }

    public void setFasciaSconto2(int fasciaSconto2) {
        this.fasciaSconto2 = fasciaSconto2;
    }
    public int getFasciaSconto3() {

        return fasciaSconto3;
    }

    public void setFasciaSconto3(int fasciaSconto3) {
        this.fasciaSconto3 = fasciaSconto3;
    }
    public int getFasciaSconto4() {

        return fasciaSconto4;
    }

    public void setFasciaSconto4(int fasciaSconto4)
    {
        this.fasciaSconto4 = fasciaSconto4;
    }
}
