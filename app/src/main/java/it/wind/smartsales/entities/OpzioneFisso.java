package it.wind.smartsales.entities;

/**
 * Created by luca.quaranta on 24/03/2016.
 */
public class OpzioneFisso {

    private String idOpzione;
    private boolean isDefault;
    private String buttonType;
    private String opzioneDesc;
    private String pdcSection;
    private String opzioneDescUpper;
    private String opzioneDescriptionIButton;
    private boolean scontoConvergenza;
    private String costoUnaTantum;
    private String canoneMensile;
    private String idOfferta;
    private int number;

    public String getIdOpzione() {
        return idOpzione;
    }

    public void setIdOpzione(String idOpzione) {
        this.idOpzione = idOpzione;
    }

    public boolean isDefault() {
        return isDefault;
    }

    public void setDefault(boolean aDefault) {
        isDefault = aDefault;
    }

    public String getButtonType() {
        return buttonType;
    }

    public void setButtonType(String buttonType) {
        this.buttonType = buttonType;
    }

    public String getOpzioneDesc() {
        return opzioneDesc;
    }

    public void setOpzioneDesc(String opzioneDesc) {
        this.opzioneDesc = opzioneDesc;
    }

    public String getPdcSection() {
        return pdcSection;
    }

    public void setPdcSection(String pdcSection) {
        this.pdcSection = pdcSection;
    }

    public String getOpzioneDescUpper() {
        return opzioneDescUpper;
    }

    public void setOpzioneDescUpper(String opzioneDescUpper) {
        this.opzioneDescUpper = opzioneDescUpper;
    }

    public String getOpzioneDescriptionIButton() {
        return opzioneDescriptionIButton;
    }

    public void setOpzioneDescriptionIButton(String opzioneDescriptionIButton) {
        this.opzioneDescriptionIButton = opzioneDescriptionIButton;
    }

    public boolean getScontoConvergenza() {
        return scontoConvergenza;
    }

    public void setScontoConvergenza(boolean scontoConvergenza) {
        this.scontoConvergenza = scontoConvergenza;
    }

    public String getCostoUnaTantum() {
        return costoUnaTantum;
    }

    public void setCostoUnaTantum(String costoUnaTantum) {
        this.costoUnaTantum = costoUnaTantum;
    }

    public String getCanoneMensile() {
        return canoneMensile;
    }

    public void setCanoneMensile(String canoneMensile) {
        this.canoneMensile = canoneMensile;
    }

    public String getIdOfferta() {
        return idOfferta;
    }

    public void setIdOfferta(String idOfferta) {
        this.idOfferta = idOfferta;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }
}
