package it.wind.smartsales.entities;

import java.util.ArrayList;

/**
 * Created by luca.quaranta on 17/05/2016.
 */
public class AddOnPackage {

    private String desc;
    private ArrayList<AddOnMobile> addOnMobileArrayList;

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public ArrayList<AddOnMobile> getAddOnMobileArrayList() {
        if (addOnMobileArrayList == null){
            return new ArrayList<>();
        }
        return addOnMobileArrayList;
    }

    public void setAddOnMobileArrayList(ArrayList<AddOnMobile> addOnMobileArrayList) {
        if (this.addOnMobileArrayList == null){
            this.addOnMobileArrayList = new ArrayList<>();
        }
        this.addOnMobileArrayList = addOnMobileArrayList;
    }
}
