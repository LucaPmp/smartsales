package it.wind.smartsales.entities;

/**
 * Created by luca.quaranta on 15/03/2016.
 */
public class Dealer {

    private String dealerCode;
    private String dealerName;
    private String dealerSurname;
    private String dealerMail;
    private String emailBackOffice;
    private boolean isCreationOfferEnabled;
    private String nomeAgenziaVenditore;
    private String nomeClientManager;
    private String cityAgenzia;
    private String username;
    private String password;
    private String dealerCity;
    private String dealerCountry;
    private String dealerAgency;
    private String dealerPhone;
    private String dealerRole;


    //MW 22/06/16 implementati nuovi campi delaer
    public String getDealerCity() {
        return dealerCity;
    }

    public void setDealerCity(String dealerCity) {
        this.dealerCity = dealerCity;
    }
    public String getDealerAgency() {
        return dealerAgency;
    }

    public void setDealerAgency(String dealerAgency) {
        this.dealerAgency = dealerAgency;
    }

    public String getDealerCountry() {
        return dealerCountry;
    }

    public void setDealerCountry(String dealerCountry) {
        this.dealerCountry = dealerCountry;
    }

    public String getDealerPhone() {
        return dealerPhone;
    }

    public void setDealerPhone(String dealerPhone) {
        this.dealerPhone = dealerPhone;
    }

    public String getDealerRole() {
        return dealerRole;
    }

    public void setDealerRole(String dealerRole) {
        this.dealerRole = dealerRole;
    }

    public String getDealerCode() {
        return dealerCode;
    }

    public void setDealerCode(String dealerCode) {
        this.dealerCode = dealerCode;
    }

    public String getDealerName() {
        return dealerName;
    }

    public void setDealerName(String dealerName) {
        this.dealerName = dealerName;
    }

    public String getDealerSurname() {
        return dealerSurname;
    }

    public void setDealerSurname(String dealerSurname) {
        this.dealerSurname = dealerSurname;
    }

    public String getDealerMail() {
        return dealerMail;
    }

    public void setDealerMail(String dealerMail) {
        this.dealerMail = dealerMail;
    }

    public String getEmailBackOffice() {
        return emailBackOffice;
    }

    public void setEmailBackOffice(String emailBackOffice) {
        this.emailBackOffice = emailBackOffice;
    }

    public boolean isCreationOfferEnabled() {
        return isCreationOfferEnabled;
    }

    public void setIsCreationOfferEnabled(boolean isCreationOfferEnabled) {
        this.isCreationOfferEnabled = isCreationOfferEnabled;
    }

    public String getNomeAgenziaVenditore() {
        return nomeAgenziaVenditore;
    }

    public void setNomeAgenziaVenditore(String nomeAgenziaVenditore) {
        this.nomeAgenziaVenditore = nomeAgenziaVenditore;
    }

    public String getNomeClientManager() {
        return nomeClientManager;
    }

    public void setNomeClientManager(String nomeClientManager) {
        this.nomeClientManager = nomeClientManager;
    }

    public String getCityAgenzia() {
        return cityAgenzia;
    }

    public void setCityAgenzia(String cityAgenzia) {
        this.cityAgenzia = cityAgenzia;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
