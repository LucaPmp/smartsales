package it.wind.smartsales.entities;

import android.content.Context;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

import it.wind.smartsales.dao.CatalogoAddOnMobileDAO;
import it.wind.smartsales.dao.ExtraGigaValueDAO;
import it.wind.smartsales.R;
import it.wind.smartsales.commons.Utils;

/**
 * Created by luca.quaranta on 15/03/2015.
 */
public class Session {

    private static Session instance = null;
    private String token, appDownloadUrl;
    private Dealer dealer;
    private ArrayList<InfoCatalog> infoCatalogs;
    private HashMap<Integer, Integer> catalogsUpdate;
    private DiscountsParameters discountsParameters;
    private String adUrl;
    private String appVersion;
    private JSONObject jsonPratica;
    private Context activity;
    private static String TAG = "Session";
    private ArrayList<AddOnMobile> addOnMobiles;
    private ArrayList<ArrayList<ExtraGBDetail>> detailList;
    private List<ExtraGBValue> extraGigaValues;

    public Pratica getPratica() {
        return pratica;
    }

    private Pratica pratica;

    private Session() {

    }

    public void turnOnExtraGigaValues(int position)
    {
        extraGigaValues.get(position).setTotDetail(1);
    }

    public void turnOffExtraGigaValues(int position)
    {
        extraGigaValues.get(position).setTotDetail(0);
    }

    public List<ExtraGBValue> getExtraGigaValues(int idPratica)
    {
        if(extraGigaValues!=null)
        {
            //se il campo già esiste lo ritorno altrimenti lo creo
            return  extraGigaValues;
        }
        extraGigaValues = new ArrayList<ExtraGBValue>();

//        int i=0;
//        String[] values = {"1","5","15","30","100"};
//        for (ArrayList<ExtraGBDetail> extraGBDetails : detailList) {
//
//            if(extraGBDetails.size()!=0)
//            {
//                ExtraGBValue value = new ExtraGBValue();
//                value.setValue(Integer.parseInt(values[i]));
//                extraGigaValues.add(value);
//            }
//            i++;

//        JSONObject extraGigaValueStored = ExtraGigaValueDAO.findValuesByIdPratica(activity, String.valueOf(idPratica));
//
//        if(extraGigaValueStored!=null)
//        {
//            detailList = createExtraGBValuesFromJSON(extraGigaValueStored);
//            return extraGigaValues;
//        }
//        }
        ExtraGBValue value = new ExtraGBValue();
        value.setValue(1);
        extraGigaValues.add(value);


        ExtraGBValue value2 = new ExtraGBValue();
        value2.setValue(5);

        extraGigaValues.add(value2);

        ExtraGBValue value3 = new ExtraGBValue();
        value3.setValue(15);
        extraGigaValues.add(value3);

        ExtraGBValue value4 = new ExtraGBValue();
        value4.setValue(30);
        extraGigaValues.add(value4);

        ExtraGBValue value5 = new ExtraGBValue();
        value5.setValue(100);
        extraGigaValues.add(value5);

        return extraGigaValues;
    }

    public static Session getInstance() {
        if (instance == null) {
            synchronized (Session.class) {
                if (instance == null) {
                    instance = new Session();
                }
            }
        }
        return instance;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Dealer getDealer() {
        return dealer;
    }

    public void setDealer(Dealer dealer) {
        this.dealer = dealer;
    }

    public String getAppDownloadUrl() {
        return appDownloadUrl;
    }

    public void setAppDownloadUrl(String appDownloadUrl) {
        this.appDownloadUrl = appDownloadUrl;
    }

    public ArrayList<InfoCatalog> getInfoCatalogs() {
        return infoCatalogs;
    }

    public void setInfoCatalogs(ArrayList<InfoCatalog> infoCatalogs) {
        this.infoCatalogs = infoCatalogs;
    }

    public HashMap<Integer, Integer> getCatalogsUpdate() {
        return catalogsUpdate;
    }

    public void setCatalogsUpdate(HashMap<Integer, Integer> catalogsUpdate) {
        this.catalogsUpdate = catalogsUpdate;
    }

    public DiscountsParameters getDiscountsParameters() {
        return discountsParameters;
    }

    public void setDiscountsParameters(DiscountsParameters discountsParameters) {
        this.discountsParameters = discountsParameters;
    }

    public String getAdUrl() {
        return adUrl;
    }

    public void setAdUrl(String adUrl) {
        this.adUrl = adUrl;
    }

    public String getAppVersion() {
        return appVersion;
    }

    public void setAppVersion(String appVersion) {
        this.appVersion = appVersion;
    }

    public JSONObject getJsonPratica() {
        return jsonPratica;
    }

    public void setJsonPratica(JSONObject jsonPratica, Pratica pratica) {
        this.jsonPratica = jsonPratica;
        this.pratica = pratica;
    }

    public void setActivity(Context activity) {
        this.activity = activity;
    }


    private String getXMLTemplate(int resId) {
        InputStream inputStream = activity.getResources().openRawResource(resId);

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

        int i;
        try {
            i = inputStream.read();
            while (i != -1) {
                byteArrayOutputStream.write(i);
                i = inputStream.read();
            }
            inputStream.close();
        } catch (IOException e) {
            return null;
        }
        String xml = byteArrayOutputStream.toString();
        return xml;
    }

    public String getContractContent(String emailTO, String emailTO2, String emailTO3) {

        String result = "";
        String mailtemplate = getXMLTemplate(R.raw.contracttemplate);

        Dealer dealer = Session.getInstance().getDealer();

        if (!emailTO2.equalsIgnoreCase("")) {
            emailTO2 = ";" + emailTO2;
        }

        if (!emailTO3.equalsIgnoreCase("")) {
            emailTO3 = ";" + emailTO3;
        }

        mailtemplate = mailtemplate.replace("<to>", "<to>" + emailTO + emailTO2 + emailTO3);


//        mailtemplate = mailtemplate.replace("<username>", "<username>" + Session.getInstance().getDealer().getUsername());

        mailtemplate = mailtemplate.replace("<cc>", "<cc>" + Session.getInstance().getDealer().getDealerMail());
        mailtemplate = mailtemplate.replace("<bcc>", "<bcc>" + Session.getInstance().getDealer().getEmailBackOffice());

//        mailtemplate = mailtemplate.replace("<pwd>", "<pwd>" + Session.getInstance().getDealer().getPassword());
//        mailtemplate = mailtemplate.replace("<dealer>", "<dealer>" + dealer.getDealerName());
//        mailtemplate = mailtemplate.replace("<mailType>", "<mailType>" + "riepilogo");

        mailtemplate = mailtemplate.replace("<noStandard>", "<noStandard>" + Session.getInstance().getJsonPratica().optBoolean("isFuoriStandard"));
//        xml.replace("<dealer>", dealer.);

        String totOfferte = "";

        for (OffertaMobile offertaMobile : getOfferteMobile()) {

            String offerTemplate = getXMLTemplate(R.raw.offertemplate);
            offerTemplate = offerTemplate.replace("<offerId>", "<offerId>" + String.valueOf(offertaMobile.getIdOfferta()));
            offerTemplate = offerTemplate.replace("<offerName>", "<offerName>" + String.valueOf(offertaMobile.getOfferDescription() + " " + offertaMobile.getOfferDescriptionUpper()));
            offerTemplate = offerTemplate.replace("<quantity>", "<quantity>" + String.valueOf(offertaMobile.getSelectedNumSimStep3()));
            offerTemplate = offerTemplate.replace("<price>", "<price>" + offertaMobile.getCanoneMensile().replace(".", ","));


            String totTerminali = "";
            for (Terminale terminale : offertaMobile.getTerminaleArrayList()) {
                String deviceTemplate = getXMLTemplate(R.raw.devicetemplate);
                deviceTemplate = deviceTemplate.replace("<deviceName>", "<deviceName>" + terminale.getTerminaleBrand() + " " + terminale.getTerminaleDescription());
                deviceTemplate = deviceTemplate.replace("<quantity>", "<quantity>" + String.valueOf(terminale.getSelectedNumSimStep3()));
                deviceTemplate = deviceTemplate.replace("<price>", "<price>" + terminale.getRataMensile().replace(".", ","));
                totTerminali += deviceTemplate;
            }

            offerTemplate = offerTemplate.replace("<deviceplaceholder>", totTerminali);

            String totItem = "";

            for (AddOnMobile addOnMobile : offertaMobile.getAddOnMobileArrayList()) {
                String itemTemplate = getXMLTemplate(R.raw.itemtemplate);
                itemTemplate = itemTemplate.replace("<name>", "<name>" + addOnMobile.getDescFascia());
                itemTemplate = itemTemplate.replace("<quantity>", "<quantity>" + addOnMobile.getSelectedNumSimStep3());
                itemTemplate = itemTemplate.replace("<price>", "<price>" + format(addOnMobile.getPrezzoFascia()));
                totItem += itemTemplate;
            }

            Collections.sort(offertaMobile.getOpzioneMobileArrayList(), new Comparator<OpzioneMobile>() {
                @Override
                public int compare(OpzioneMobile opz1, OpzioneMobile opz2) {
                    return opz1.getScontoConvergenza().compareToIgnoreCase(opz2.getScontoConvergenza());
                }
            });

            for (OpzioneMobile opzioneMobile : offertaMobile.getOpzioneMobileArrayList()) {
                String itemTemplate = getXMLTemplate(R.raw.itemtemplate);
                itemTemplate = itemTemplate.replace("<name>", "<name>" + opzioneMobile.getOpzioneDesc() + " " + opzioneMobile.getOpzioneDescUpper());
                itemTemplate = itemTemplate.replace("<quantity>", "<quantity>" + opzioneMobile.getSelectedNumSimStep3());
                itemTemplate = itemTemplate.replace("<price>", "<price>" + (opzioneMobile.getScontoConvergenza().isEmpty() == true ? format(opzioneMobile.getCanoneMensile()) : "-" + format(opzioneMobile.getScontoConvergenza())));
                totItem += itemTemplate;
            }

            offerTemplate = offerTemplate.replace("<itemplaceholder>", totItem);

            totOfferte += offerTemplate;

        }

        mailtemplate = mailtemplate.replace("<offerplaceholder>", totOfferte);

        if (this.getPratica().getScontoApplicato() != 0f) {
            mailtemplate = mailtemplate.replace("<appliedDiscount>", "<appliedDiscount>-" + format(this.getPratica().getScontoApplicato()));
        }
        mailtemplate = mailtemplate.replace("<monthlyFeeAmount>", "<monthlyFeeAmount>" + format(this.getPratica().getTotCanoneMensile()));
        mailtemplate = mailtemplate.replace("<oneOffCharge>", "<oneOffCharge>" + format(this.getPratica().getContributoUnaTan()));

        Log.i(TAG, mailtemplate);
        return mailtemplate;
    }

    public String getMailContent(String emailTO, String emailTO2, String emailTO3) {

        String result = "";
        String mailtemplate = getXMLTemplate(R.raw.mailtemplate);

        Dealer dealer = Session.getInstance().getDealer();

        if (!emailTO2.equalsIgnoreCase("")) {
            emailTO2 = ";" + emailTO2;
        }

        if (!emailTO3.equalsIgnoreCase("")) {
            emailTO3 = ";" + emailTO3;
        }

        mailtemplate = mailtemplate.replace("<to>", "<to>" + emailTO + emailTO2 + emailTO3);


        mailtemplate = mailtemplate.replace("<username>", "<username>" + Session.getInstance().getDealer().getUsername());

        mailtemplate = mailtemplate.replace("<cc>", "<cc>" + Session.getInstance().getDealer().getDealerMail());
        mailtemplate = mailtemplate.replace("<bcc>", "<bcc>" + Session.getInstance().getDealer().getEmailBackOffice());

        mailtemplate = mailtemplate.replace("<pwd>", "<pwd>" + Session.getInstance().getDealer().getPassword());
        mailtemplate = mailtemplate.replace("<dealer>", "<dealer>" + dealer.getDealerName());
        mailtemplate = mailtemplate.replace("<mailType>", "<mailType>" + "riepilogo");

        mailtemplate = mailtemplate.replace("<noStandard>", "<noStandard>" + Session.getInstance().getJsonPratica().optBoolean("isFuoriStandard"));
//        xml.replace("<dealer>", dealer.);

        String totOfferte = "";

        for (OffertaMobile offertaMobile : getOfferteMobile()) {

            String offerTemplate = getXMLTemplate(R.raw.offertemplate);
            offerTemplate = offerTemplate.replace("<offerId>", "<offerId>" + String.valueOf(offertaMobile.getIdOfferta()));
            offerTemplate = offerTemplate.replace("<offerName>", "<offerName>" + String.valueOf(offertaMobile.getOfferDescription() + " " + offertaMobile.getOfferDescriptionUpper()));
            offerTemplate = offerTemplate.replace("<quantity>", "<quantity>" + String.valueOf(offertaMobile.getSelectedNumSimStep3()));
            offerTemplate = offerTemplate.replace("<price>", "<price>" + offertaMobile.getCanoneMensile().replace(".", ","));


            String totTerminali = "";
            for (Terminale terminale : offertaMobile.getTerminaleArrayList()) {
                String deviceTemplate = getXMLTemplate(R.raw.devicetemplate);
                deviceTemplate = deviceTemplate.replace("<deviceName>", "<deviceName>" + terminale.getTerminaleBrand() + " " + terminale.getTerminaleDescription());
                deviceTemplate = deviceTemplate.replace("<quantity>", "<quantity>" + String.valueOf(terminale.getSelectedNumSimStep3()));
                deviceTemplate = deviceTemplate.replace("<price>", "<price>" + terminale.getRataMensile().replace(".", ","));
                totTerminali += deviceTemplate;
            }

            offerTemplate = offerTemplate.replace("<deviceplaceholder>", totTerminali);

            String totItem = "";

            for (AddOnMobile addOnMobile : offertaMobile.getAddOnMobileArrayList()) {
                String itemTemplate = getXMLTemplate(R.raw.itemtemplate);
                itemTemplate = itemTemplate.replace("<name>", "<name>" + addOnMobile.getDescFascia());
                itemTemplate = itemTemplate.replace("<quantity>", "<quantity>" + addOnMobile.getSelectedNumSimStep3());
                itemTemplate = itemTemplate.replace("<price>", "<price>" + format(addOnMobile.getPrezzoFascia()));
                totItem += itemTemplate;
            }

            Collections.sort(offertaMobile.getOpzioneMobileArrayList(), new Comparator<OpzioneMobile>() {
                @Override
                public int compare(OpzioneMobile opz1, OpzioneMobile opz2) {
                    return opz1.getScontoConvergenza().compareToIgnoreCase(opz2.getScontoConvergenza());
                }
            });

            for (OpzioneMobile opzioneMobile : offertaMobile.getOpzioneMobileArrayList()) {
                String itemTemplate = getXMLTemplate(R.raw.itemtemplate);
                itemTemplate = itemTemplate.replace("<name>", "<name>" + opzioneMobile.getOpzioneDesc() + " " + opzioneMobile.getOpzioneDescUpper());
                itemTemplate = itemTemplate.replace("<quantity>", "<quantity>" + opzioneMobile.getSelectedNumSimStep3());
                itemTemplate = itemTemplate.replace("<price>", "<price>" + (opzioneMobile.getScontoConvergenza().isEmpty() == true ? format(opzioneMobile.getCanoneMensile()) : "-" + format(opzioneMobile.getScontoConvergenza())));
                totItem += itemTemplate;
            }

            offerTemplate = offerTemplate.replace("<itemplaceholder>", totItem);

            totOfferte += offerTemplate;

        }

        mailtemplate = mailtemplate.replace("<offerplaceholder>", totOfferte);

        if (this.getPratica().getScontoApplicato() != 0f) {
            mailtemplate = mailtemplate.replace("<appliedDiscount>", "<appliedDiscount>-" + format(this.getPratica().getScontoApplicato()));
        }
        mailtemplate = mailtemplate.replace("<monthlyFeeAmount>", "<monthlyFeeAmount>" + format(this.getPratica().getTotCanoneMensile()));
        mailtemplate = mailtemplate.replace("<oneOffCharge>", "<oneOffCharge>" + format(this.getPratica().getContributoUnaTan()));

        Log.i(TAG, mailtemplate);
        return mailtemplate;
    }

    private ArrayList<OffertaMobile> getOfferteMobile() {
        JSONArray offerteRaggruppate = null;
        ArrayList<OffertaMobile> offertaMobiles = null;
        try {
            offerteRaggruppate = (JSONArray) Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONArray("offerteRaggruppate");
            offertaMobiles = new ArrayList<>();
            for (int i = 0; i < offerteRaggruppate.length(); i++) {
                offertaMobiles.add(Utils.createOffertaMobile(offerteRaggruppate.getJSONObject(i)));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }


        return offertaMobiles;
    }

    private String format(float input) {
        String output;
        output = String.format("%.2f", input).replace(".", ",");
        return output;
    }

    private String format(String input) {
        String output;
        output = String.format("%.2f", Float.parseFloat(input)).replace(".", ",");
        return output;
    }


    public void updateExtraGiga(int valuePosition, int detailPosition, int value){

        ExtraGBDetail currentDetail = detailList.get(valuePosition).get(detailPosition);
        currentDetail.setValue(value);
        detailList.get(valuePosition).set(detailPosition, currentDetail);

        ExtraGigaValueDAO.insertOrUpdateDetails(activity, String.valueOf(pratica.getId()), createJSONFromExtraGBDetail(detailList));

    }

    public ArrayList<ArrayList<ExtraGBDetail>> getExtraGiga(int idPratica)
    {

//        if(detailList!=null)
//        {
//            //se ho già letto dalla pratica la lista degli extragiga allora la ritorno, altrimenti la creo
//            return detailList;
//        }

        detailList = new ArrayList<ArrayList<ExtraGBDetail>>();

        ArrayList<ArrayList<ExtraGBDetail>> oldDetailList = new ArrayList<ArrayList<ExtraGBDetail>>();

        for(int i=0;i<5;i++)
        {
            detailList.add(new ArrayList<ExtraGBDetail>());
        };

        //cerco il valore sul DB, se è presente lo restituisco altrimenti lo creo dalla pratica
        JSONObject extraGigaStored = ExtraGigaValueDAO.findDetailsByIdPratica(activity, String.valueOf(idPratica));

        if(extraGigaStored!=null)
        {
            oldDetailList = createExtraGBDetailFromJSON(extraGigaStored);
//            return detailList;
        }

        int letterIndex1=0;
        int letterIndex5=0;
        int letterIndex15=0;
        int letterIndex30=0;
        int letterIndex100=0;
        String lettera;

        //se sul DB ho già presente la struttura extraGigaStored devo settare i valori di quella estratta dalla pratica a quelli che trovo sul DB
        ArrayList<Object> result = new ArrayList<>();
        try {



            JSONArray addOnMobileAll = Session.getInstance().getJsonPratica().getJSONObject("pratica").getJSONArray("addOnMobileAllPacchetti");
            for (int k = 0; k < addOnMobileAll.length(); k++) {
                JSONObject elem = (JSONObject) addOnMobileAll.get(k);
                String descFascia = (String) elem.get("descFascia");
                Log.i(TAG, "Fascia " + descFascia);
                //desc fascia da la posizione sull'array esterno di oldDetailList
                int selectedNumSimStep3 = (int) elem.get("selectedNumSimStep3");

                for(int t = 0;t< selectedNumSimStep3;t++) {
                    ExtraGBDetail detail = new ExtraGBDetail();

//                            String descFasciaSub = descFascia.substring(0, descFascia.length() - 2);
//                            detail.setValue(Integer.parseInt(descFasciaSub));



                    detail.setValue(0);
                    int maxValue = getExtraGigaValueData(descFascia) == null ? -1 : Integer.parseInt(getExtraGigaValueData(descFascia).getExtraBundleMaxNumeroRinnovi());
                    detail.setMaxValue(maxValue);

/*            JSONArray pacchetti = (JSONArray) ((JSONObject)jsonPratica.get("pratica")).get("pacchetti");
            for (int i = 0; i < pacchetti.length(); i++) {
                JSONArray offerte = (JSONArray) ((JSONObject) pacchetti.get(i)).get("offerte");

                for (int j = 0; j < offerte.length(); j++) {
                    JSONArray addOnMobList = (JSONArray) ((JSONObject)offerte.get(j)).get("addOnMobileList");

                    for (int k = 0; k < addOnMobList.length(); k++) {
                        JSONObject elem = (JSONObject) addOnMobList.get(k);
                        String descFascia = (String) elem.get("descFascia");
                        Log.i(TAG, "Fascia " + descFascia);
                        //desc fascia da la posizione sull'array esterno di oldDetailList
                        int selectedNumSimStep3 = (int) elem.get("selectedNumSimStep3");

                        for(int t = 0;t< selectedNumSimStep3;t++) {
                            ExtraGBDetail detail = new ExtraGBDetail();

//                            String descFasciaSub = descFascia.substring(0, descFascia.length() - 2);
//                            detail.setValue(Integer.parseInt(descFasciaSub));



                            detail.setValue(0);
                            int maxValue = getExtraGigaValueData(descFascia) == null ? -1 : Integer.parseInt(getExtraGigaValueData(descFascia).getExtraBundleMaxNumeroRinnovi());
                            detail.setMaxValue(maxValue);
*/
                            switch (descFascia) {
                                case "1GB":
                                    lettera = getCharForNumber(letterIndex1);
                                    detail.setLetter(lettera);


                                    if(oldDetailList.size()>0&&oldDetailList.get(0).size()>0){
                                        try {
                                            detail.setValue(oldDetailList.get(0).get(letterIndex1).getValue());
                                        }
                                        catch (Exception e)
                                        {}
                                    }

                                    letterIndex1++;

                                    detailList.get(0).add(detail);
                                    break;
                                case "5GB":
                                    lettera = getCharForNumber(letterIndex5);
                                    detail.setLetter(lettera);


                                    if(oldDetailList.size()>0&&oldDetailList.get(1).size()>0){
                                        try{
                                        detail.setValue(oldDetailList.get(1).get(letterIndex5).getValue());
                                        }
                                        catch (Exception e)
                                        {}
                                    }
                                    letterIndex5++;

                                    detailList.get(1).add(detail);
                                    break;
                                case "15GB":
                                    lettera = getCharForNumber(letterIndex15);
                                    detail.setLetter(lettera);


                                    if(oldDetailList.size()>0&&oldDetailList.get(2).size()>0){

                                        try{
                                        detail.setValue(oldDetailList.get(2).get(letterIndex15).getValue());
                                        }
                                        catch (Exception e)
                                        {}
                                    }

                                    letterIndex15++;
                                    detailList.get(2).add(detail);
                                    break;
                                case "30GB":
                                    lettera = getCharForNumber(letterIndex30);
                                    detail.setLetter(lettera);


                                    if(oldDetailList.size()>0&&oldDetailList.get(3).size()>0){
                                        try{
                                        detail.setValue(oldDetailList.get(3).get(letterIndex30).getValue());
                                        }
                                        catch (Exception e)
                                        {}
                                    }
                                    letterIndex30++;
                                    detailList.get(3).add(detail);
                                    break;
                                case "100GB":
                                    lettera = getCharForNumber(letterIndex100);
                                    detail.setLetter(lettera);


                                    if(oldDetailList.size()>0&&oldDetailList.get(4).size()>0){
                                        try{
                                        detail.setValue(oldDetailList.get(4).get(letterIndex100).getValue());
                                        }
                                        catch (Exception e)
                                        {}
                                    }

                                    detailList.get(4).add(detail);
                                    letterIndex100++;
                                    break;
                            }
//                        }


                  //  }

                }

            }
        }
        catch (Exception e)
        {
            Log.i(TAG, e.getMessage());
        }

//        for (ArrayList<ExtraGBDetail> extraGBDetails : detailList) {
//            if(extraGBDetails.size()==0)
//            {
//                detailList.remove(extraGBDetails);
//            }
//        }
        Log.i(TAG, String.valueOf(detailList.size()));
        return detailList;
    }




    private ArrayList<ArrayList<ExtraGBDetail>> createExtraGBValuesFromJSON(JSONObject extraGigaStored) {
        ArrayList<ArrayList<ExtraGBDetail>> result=new ArrayList<ArrayList<ExtraGBDetail>>();
        try {
            JSONArray listValues = ((JSONArray) extraGigaStored.get("data"));
            for (int i = 0; i < listValues.length(); i++) {
                JSONArray listDetail = ((JSONArray)listValues.get(i));
                ArrayList<ExtraGBDetail> detailList = new ArrayList<ExtraGBDetail>();
                for (int j = 0; j < listDetail.length(); j++) {

                    ExtraGBDetail detail = new ExtraGBDetail();
                    detail.setValue((Integer) ((JSONObject) listDetail.get(j)).get("value"));
                    detail.setMaxValue((Integer) ((JSONObject) listDetail.get(j)).get("maxValue"));
                    detail.setLetter((String) ((JSONObject) listDetail.get(j)).get("letter"));
                    detailList.add(detail);

                }
                result.add(detailList);
            }
        }
        catch (Exception e )
        {
            Log.d(TAG, e.getMessage());
        }
        return result;
    }

    private ArrayList<ArrayList<ExtraGBDetail>> createExtraGBDetailFromJSON(JSONObject extraGigaStored) {
        ArrayList<ArrayList<ExtraGBDetail>> result=new ArrayList<ArrayList<ExtraGBDetail>>();
        try {
            JSONArray listValues = ((JSONArray) extraGigaStored.get("data"));
            for (int i = 0; i < listValues.length(); i++) {
                JSONArray listDetail = ((JSONArray)listValues.get(i));
                ArrayList<ExtraGBDetail> detailList = new ArrayList<ExtraGBDetail>();
                for (int j = 0; j < listDetail.length(); j++) {

                    ExtraGBDetail detail = new ExtraGBDetail();
                    detail.setValue((Integer) ((JSONObject) listDetail.get(j)).get("value"));
                    detail.setMaxValue((Integer) ((JSONObject) listDetail.get(j)).get("maxValue"));
                    detail.setLetter((String) ((JSONObject) listDetail.get(j)).get("letter"));
                    detailList.add(detail);

                }
                result.add(detailList);
            }
        }
        catch (Exception e )
        {
            Log.d(TAG, e.getMessage());
        }
        return result;
    }

    public static JSONObject createJSONFromExtraGBDetail(ArrayList<ArrayList<ExtraGBDetail>> extraGigaDetail) {

        JSONObject data = new JSONObject();

        try {
            JSONArray alllist = new JSONArray();

            for (ArrayList<ExtraGBDetail> extraGBDetails : extraGigaDetail) {

                JSONArray listDetail = new JSONArray();
                for (ExtraGBDetail extraGBDetail : extraGBDetails) {

                    JSONObject detail = new JSONObject();
                    detail.put("value", extraGBDetail.getValue());
                    detail.put("maxValue", extraGBDetail.getMaxValue());
                    detail.put("letter", extraGBDetail.getLetter());
                    listDetail.put(detail);

                }
                alllist.put(listDetail);
            }
            data.put("data",alllist);
        }
        catch (Exception e)
        {
            Log.d(TAG,e.getMessage());
        }
        return data;
    }

    private String getCharForNumber(int i) {
        char[] alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray();
        if (i > 25) {
            return null;
        }
        return Character.toString(alphabet[i]);
    }

    private int getNumberFromChar(String c)
    {
        String alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        return alphabet.indexOf(c);
    }


    public AddOnMobile getExtraGigaValueData(String label)
    {
        //leggo il db solo una volta, per le chiamate successive il campp è già valorizzato
        if(addOnMobiles==null)
        {addOnMobiles = CatalogoAddOnMobileDAO.retrieveExtraBundles(activity);}

        ArrayList<AddOnMobile> addOnMobilesFiltered = new ArrayList<AddOnMobile>();

        //ritorno i dati dell'add on mobile in base alla label (1GB, 5GB, 15GB, ...)

        //FIXME: per ora filtro i doppioni
        for (AddOnMobile addOnMobile : addOnMobiles) {

            label = label.replace(" ", "");

            if(addOnMobile.getDescFascia().equalsIgnoreCase( label))
            {
                Log.d(TAG,"found value data for " +label);
                return  addOnMobile;
            }


        }
        Log.d(TAG,"not found value data for " +label);
        return null;
    }


    public String getContractLabel(String descFascia, String selectedLabelStep8) {

        String desc = getExtraGigaValueData(descFascia).getExtraBundleDescrizionePDF();

        switch (descFascia)
        {
            case "1GB":

                if(detailList.get(0).get(getNumberFromChar(selectedLabelStep8)).getValue()>0) {
                    return desc + " X " + (detailList.get(0).get(getNumberFromChar(selectedLabelStep8)).getValue());
                }
                else
                {
                    return "";
                }

            case "5GB":
                if(detailList.get(1).get(getNumberFromChar(selectedLabelStep8)).getValue()>0) {
                    return desc + " X " + (detailList.get(1).get(getNumberFromChar(selectedLabelStep8)).getValue());
                }
                else
                {
                    return "";
                }

            case "15GB":
                if(detailList.get(2).get(getNumberFromChar(selectedLabelStep8)).getValue()>0) {
                    return desc + " X " + (detailList.get(2).get(getNumberFromChar(selectedLabelStep8)).getValue());
                }
                else
                {
                    return "";
                }

            case "30GB":
                if(detailList.get(3).get(getNumberFromChar(selectedLabelStep8)).getValue()>0) {
                    return desc + " X " + (detailList.get(3).get(getNumberFromChar(selectedLabelStep8)).getValue());
                }
                else
                {
                    return "";
                }

            case "100GB":
                if(detailList.get(4).get(getNumberFromChar(selectedLabelStep8)).getValue()>0) {
                    return desc + " X " + (detailList.get(4).get(getNumberFromChar(selectedLabelStep8)).getValue());
                }
                else
                {
                    return "";
                }

        }

        return "";

    }


}
