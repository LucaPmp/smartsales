package it.wind.smartsales.entities;

/**
 * Created by luca.quaranta on 04/04/2016.
 */
public class TerminaleBean {

    private String idTerminale;
    private int selectedNumSimStep3Mnp;
    private int selectedNumSimStep3NuovaLinea;

    public String getIdTerminale() {
        return idTerminale;
    }

    public void setIdTerminale(String idTerminale) {
        this.idTerminale = idTerminale;
    }

    public int getSelectedNumSimStep3Mnp() {
        return selectedNumSimStep3Mnp;
    }

    public void setSelectedNumSimStep3Mnp(int selectedNumSimStep3Mnp) {
        this.selectedNumSimStep3Mnp = selectedNumSimStep3Mnp;
    }

    public int getSelectedNumSimStep3NuovaLinea() {
        return selectedNumSimStep3NuovaLinea;
    }

    public void setSelectedNumSimStep3NuovaLinea(int selectedNumSimStep3NuovaLinea) {
        this.selectedNumSimStep3NuovaLinea = selectedNumSimStep3NuovaLinea;
    }
}
