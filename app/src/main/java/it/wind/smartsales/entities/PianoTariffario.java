package it.wind.smartsales.entities;

/**
 * Created by luca.quaranta on 04/04/2016.
 */
public class PianoTariffario {

    private String idOfferta;
    private String percentualeSconto;
    private String percentualeScontoPenale;

    public String getIdOfferta() {
        return idOfferta;
    }

    public void setIdOfferta(String idOfferta) {
        this.idOfferta = idOfferta;
    }

    public String getPercentualeSconto() {
        return percentualeSconto;
    }

    public void setPercentualeSconto(String percentualeSconto) {
        this.percentualeSconto = percentualeSconto;
    }

    public String getPercentualeScontoPenale() {
        return percentualeScontoPenale;
    }

    public void setPercentualeScontoPenale(String percentualeScontoPenale) {
        this.percentualeScontoPenale = percentualeScontoPenale;
    }
}
