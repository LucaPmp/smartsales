package it.wind.smartsales.entities;

import java.io.Serializable;

/**
 * Created by luca.quaranta on 24/03/2016.
 */
public class OpzioneMobile implements Serializable{

    private String idOpzione;
    private String offerType;
    private String opzioneDesc;
    private String opzioneDescUpper;
    private String opzioneDescriptionIButton;
    private String idOfferta;
    private String canoneMensile;
    private String canoneSettimanale;
    private String canoneBimestrale;
    private String voce;
    private String sms;
    private String dati;
    private String scontoConvergenza;
    private String costoUnaTantum;
    private String isDefault;
    private String isDoubleChecked;
    private int selectedNumSimStep3;
    private String value;

    public String getIdOpzione() {
        return idOpzione;
    }

    public void setIdOpzione(String idOpzione) {
        this.idOpzione = idOpzione;
    }

    public String getOfferType() {
        return offerType;
    }

    public void setOfferType(String offerType) {
        this.offerType = offerType;
    }

    public String getOpzioneDesc() {
        return opzioneDesc;
    }

    public void setOpzioneDesc(String opzioneDesc) {
        this.opzioneDesc = opzioneDesc;
    }

    public String getOpzioneDescUpper() {
        return opzioneDescUpper;
    }

    public void setOpzioneDescUpper(String opzioneDescUpper) {
        this.opzioneDescUpper = opzioneDescUpper;
    }

    public String getOpzioneDescriptionIButton() {
        return opzioneDescriptionIButton;
    }

    public void setOpzioneDescriptionIButton(String opzioneDescriptionIButton) {
        this.opzioneDescriptionIButton = opzioneDescriptionIButton;
    }

    public String getIdOfferta() {
        return idOfferta;
    }

    public void setIdOfferta(String idOfferta) {
        this.idOfferta = idOfferta;
    }

    public String getCanoneMensile() {
        return canoneMensile;
    }

    public void setCanoneMensile(String canoneMensile) {
        this.canoneMensile = canoneMensile;
    }

    public String getCanoneSettimanale() {
        return canoneSettimanale;
    }

    public void setCanoneSettimanale(String canoneSettimanale) {
        this.canoneSettimanale = canoneSettimanale;
    }

    public String getCanoneBimestrale() {
        return canoneBimestrale;
    }

    public void setCanoneBimestrale(String canoneBimestrale) {
        this.canoneBimestrale = canoneBimestrale;
    }

    public String getVoce() {
        return voce;
    }

    public void setVoce(String voce) {
        this.voce = voce;
    }

    public String getSms() {
        return sms;
    }

    public void setSms(String sms) {
        this.sms = sms;
    }

    public String getDati() {
        return dati;
    }

    public void setDati(String dati) {
        this.dati = dati;
    }

    public String getScontoConvergenza() {
        return scontoConvergenza;
    }

    public void setScontoConvergenza(String scontoConvergenza) {
        this.scontoConvergenza = scontoConvergenza;
    }

    public String getCostoUnaTantum() {
        return costoUnaTantum;
    }

    public void setCostoUnaTantum(String costoUnaTantum) {
        this.costoUnaTantum = costoUnaTantum;
    }

    public String getIsDefault() {
        return isDefault;
    }

    public void setIsDefault(String isDefault) {
        this.isDefault = isDefault;
    }

    public String getIsDoubleChecked() {
        return isDoubleChecked;
    }

    public void setIsDoubleChecked(String isDoubleChecked) {
        this.isDoubleChecked = isDoubleChecked;
    }

    public int getSelectedNumSimStep3() {
        return selectedNumSimStep3;
    }

    public void setSelectedNumSimStep3(int selectedNumSimStep3) {
        this.selectedNumSimStep3 = selectedNumSimStep3;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
