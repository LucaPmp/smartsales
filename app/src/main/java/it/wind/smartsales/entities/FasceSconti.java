package it.wind.smartsales.entities;

/**
 * Created by luca.quaranta on 04/04/2016.
 */
public class FasceSconti {

    private int fasciaSim1;
    private int fasciaSim2;
    private int fasciaSim3;
    private int fasciaSpesa1;
    private int fasciaSpesa2;

    public int getFasciaSim1() {
        return fasciaSim1;
    }

    public void setFasciaSim1(int fasciaSim1) {
        this.fasciaSim1 = fasciaSim1;
    }

    public int getFasciaSim2() {
        return fasciaSim2;
    }

    public void setFasciaSim2(int fasciaSim2) {
        this.fasciaSim2 = fasciaSim2;
    }

    public int getFasciaSim3() {
        return fasciaSim3;
    }

    public void setFasciaSim3(int fasciaSim3) {
        this.fasciaSim3 = fasciaSim3;
    }

    public int getFasciaSpesa1() {
        return fasciaSpesa1;
    }

    public void setFasciaSpesa1(int fasciaSpesa1) { this.fasciaSpesa1 = fasciaSpesa1; }

    public int getFasciaSpesa2() {
        return fasciaSpesa2;
    }

    public void setFasciaSpesa2(int fasciaSpesa2) {
        this.fasciaSpesa2 = fasciaSpesa2;
    }
}
