package it.wind.smartsales.entities;

import java.util.ArrayList;

/**
 * Created by luca.quaranta on 17/03/2016.
 */
public class TariffPlan {

    private int idOfferta;
    private boolean repaymentPenalties;
    private ArrayList<PercentageDiscount> percentageDiscounts;

    private float selectedDiscountPercentage;
    private int selectedNumSim;
    private String selectedPreAutorizzazione;

    public TariffPlan(){
        percentageDiscounts = new ArrayList<>();
    }

    public int getIdOfferta() {
        return idOfferta;
    }

    public void setIdOfferta(int idOfferta) {
        this.idOfferta = idOfferta;
    }

    public boolean isRepaymentPenalties() {
        return repaymentPenalties;
    }

    public void setRepaymentPenalties(boolean repaymentPenalties) {
        this.repaymentPenalties = repaymentPenalties;
    }

    public ArrayList<PercentageDiscount> getPercentageDiscounts() {
        return percentageDiscounts;
    }

    public void setPercentageDiscounts(ArrayList<PercentageDiscount> percentageDiscounts) {
        this.percentageDiscounts = percentageDiscounts;
    }

    public float getSelectedDiscountPercentage() {
        return selectedDiscountPercentage;
    }

    public void setSelectedDiscountPercentage(float selectedDiscountPercentage) {
        this.selectedDiscountPercentage = selectedDiscountPercentage;
    }

    public int getSelectedNumSim() {
        return selectedNumSim;
    }

    public void setSelectedNumSim(int selectedNumSim) {
        this.selectedNumSim = selectedNumSim;
    }

    public String getSelectedPreAutorizzazione() {
        return selectedPreAutorizzazione;
    }

    public void setSelectedPreAutorizzazione(String selectedPreAutorizzazione) {
        this.selectedPreAutorizzazione = selectedPreAutorizzazione;
    }
}
