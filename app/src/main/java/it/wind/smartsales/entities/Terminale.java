package it.wind.smartsales.entities;

/**
 * Created by luca.quaranta on 04/04/2016.
 */
public class Terminale {
    private String idTerminale;
    private String oem;
    private String terminaleBrand;
    private String terminaleDescription;
    private String mnp;
    private String offerType;
    private String deviceType;
    private String prezzoListino;
    private String rataIniziale;
    private String rataMensile;
    private String rataMensileMnp;
    private String rataFinale;
    private String imageUrl;
    private String shortDesc;
    private String capacity;
    private String color;
    private String idOfferta;
    private int selectedNumSimStep3;
    private int selectedTerminalsMnp;
    private int selectedTerminals;
    private boolean isRataMnpVisible = true;
    private boolean isRataVisible = true;
    private boolean isPrezzoListinoVisible = false;

    public String getIdTerminale() {
        return idTerminale;
    }

    public void setIdTerminale(String idTerminale) {
        this.idTerminale = idTerminale;
    }

    public String getOem() {
        return oem;
    }

    public void setOem(String oem) {
        this.oem = oem;
    }

    public String getTerminaleBrand() {
        return terminaleBrand;
    }

    public void setTerminaleBrand(String terminaleBrand) {
        this.terminaleBrand = terminaleBrand;
    }

    public String getTerminaleDescription() {
        return terminaleDescription;
    }

    public void setTerminaleDescription(String terminaleDescription) {
        this.terminaleDescription = terminaleDescription;
    }

    public String getShortDesc() {
        return shortDesc;
    }

    public void setShortDesc(String shortDesc) {
        this.shortDesc = shortDesc;
    }

    public String getCapacity() {
        return capacity;
    }

    public void setCapacity(String capacity) {
        this.capacity = capacity;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getMnp() {
        return mnp;
    }

    public void setMnp(String mnp) {
        this.mnp = mnp;
    }

    public String getOfferType() {
        return offerType;
    }

    public void setOfferType(String offerType) {
        this.offerType = offerType;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    public String getPrezzoListino() {
        return prezzoListino;
    }

    public void setPrezzoListino(String prezzoListino) {
        this.prezzoListino = prezzoListino;
    }

    public String getRataIniziale() {
        return rataIniziale;
    }

    public void setRataIniziale(String rataIniziale) {
        this.rataIniziale = rataIniziale;
    }

    public String getRataMensile() {
        return rataMensile;
    }

    public void setRataMensile(String rataMensile) {
        this.rataMensile = rataMensile;
    }

    public void setRataMensileMnp(String rataMensileMnp) {
        this.rataMensileMnp = rataMensileMnp;
    }

    public String getRataMensileMnp() {
        return rataMensileMnp;
    }

    public String getRataFinale() {
        return rataFinale;
    }

    public void setRataFinale(String rataFinale) {
        this.rataFinale = rataFinale;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getIdOfferta() {
        return idOfferta;
    }

    public void setIdOfferta(String idOfferta) {
        this.idOfferta = idOfferta;
    }

    public int getSelectedNumSimStep3() {
        return selectedNumSimStep3;
    }

    public void setSelectedNumSimStep3(int selectedNumSimStep3) {
        this.selectedNumSimStep3 = selectedNumSimStep3;
    }

    public int getSelectedTerminalsMnp() {
        return selectedTerminalsMnp;
    }

    public void setSelectedTerminalsMnp(int selectedTerminalsMnp) {
        this.selectedTerminalsMnp = selectedTerminalsMnp;
    }

    public int getSelectedTerminals() {
        return selectedTerminals;
    }

    public void setSelectedTerminals(int selectedTerminals) {
        this.selectedTerminals = selectedTerminals;
    }

    public boolean isSelected() {
        return getSelectedTerminals() != 0 || getSelectedTerminalsMnp() != 0;
    }

    public boolean isRataMnpVisible() {
        return isRataMnpVisible;
    }

    public void setRataMnpVisible(boolean rataMnpVisible) {
        isRataMnpVisible = rataMnpVisible;
    }

    public boolean isRataVisible() {
        return isRataVisible;
    }

    public void setRataVisible(boolean isVisible) {
        isRataVisible = isVisible;
    }

    public boolean isPrezzoListinoVisible() {
        return isPrezzoListinoVisible;
    }

    public void setPrezzoListinoVisible(boolean isVisible) {
        isPrezzoListinoVisible = isVisible;
    }
}
