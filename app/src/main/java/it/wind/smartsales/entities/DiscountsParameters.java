package it.wind.smartsales.entities;

import java.util.ArrayList;

/**
 * Created by luca.quaranta on 17/03/2016.
 */
public class DiscountsParameters {

    private int simCategory1;
    private int simCategory2;
    private int simCategory3;
    private int expenseCategory1;
    private int expenseCategory2;
    private ArrayList<TariffPlan> tariffPlans;

    public DiscountsParameters(){
        tariffPlans = new ArrayList<>();
    }

    public int getSimCategory1() {
        return simCategory1;
    }

    public void setSimCategory1(int simCategory1) {
        this.simCategory1 = simCategory1;
    }

    public int getSimCategory2() {
        return simCategory2;
    }

    public void setSimCategory2(int simCategory2) {
        this.simCategory2 = simCategory2;
    }

    public int getSimCategory3() {
        return simCategory3;
    }

    public void setSimCategory3(int simCategory3) {
        this.simCategory3 = simCategory3;
    }

    public int getExpenseCategory1() {
        return expenseCategory1;
    }

    public void setExpenseCategory1(int expenseCategory1) {
        this.expenseCategory1 = expenseCategory1;
    }

    public int getExpenseCategory2() {
        return expenseCategory2;
    }

    public void setExpenseCategory2(int expenseCategory2) {
        this.expenseCategory2 = expenseCategory2;
    }

    public ArrayList<TariffPlan> getTariffPlans() {
        return tariffPlans;
    }

    public void setTariffPlans(ArrayList<TariffPlan> tariffPlans) {
        this.tariffPlans = tariffPlans;
    }
}
