package it.wind.smartsales.entities;

import java.util.ArrayList;

/**
 * Created by luca.quaranta on 22/03/2016.
 */
public class OffertaMobile extends Offerta {

    private int idOfferta;
    private String offerDescription;
    private String offerDescriptionUpper;
    private String offerDescriptionIButton;
    private String offerType;
    private String canoneMensile;
    private String canoneMensileConv;
    private String canonePromo;
    private String callAzIta;
    private String callIta;
    private String smsIta;
    private String datiIta;
    private String callEst;
    private String smsEst;
    private String datiEst;
    private String fasciaVoce;
    private String fasciaDati;
    private boolean isSelectedStep3;
    private int selectedNumSimStep3 = 1;
    private ArrayList<AddOnMobile> addOnMobileArrayList;
    private ArrayList<Terminale> terminaleArrayList;
    private ArrayList<OpzioneMobile> opzioneMobileArrayList;
    private ArrayList<TerminaleBean> terminaleBeanArrayList;
    private String percentualeScontoPenale;
    private ArrayList<PercentualeSconto> percentualeScontoArrayList;
    private int selectedNumSimStep4 = 1;
    private String scontoApplicato;
    private String canoneScontato;
    private String autorizzabilita;
    private boolean isScontoPenaleApplicato;
    private String mnpStep8;
    private AddOnMobile addOnMobileStep8;
    private ArrayList<OpzioneMobile> opzioneMobileArrayListStep8;
    private String oldICCID;
    private String newICCID;
    private String oldMSISDN;
    private ArrayList<AddOnPackage> addOnPackageArrayListStep8;
    private ArrayList<OpzioneMobile> opzioniMobileSelectedArrayListStep8;
    private boolean isMnp;
    private ArrayList<Terminale> terminaliToShow;
    private Terminale terminale1;
    private Terminale terminale2;
    private ArrayList<Terminale> terminaliToShow2;
    private ArrayList<Terminale> terminaliDiAppoggio;

    public int getIdOfferta() {
        return idOfferta;
    }

    public void setIdOfferta(int idOfferta) {
        this.idOfferta = idOfferta;
    }

    public String getOfferDescription() {
        return offerDescription;
    }

    public void setOfferDescription(String offerDescription) {
        this.offerDescription = offerDescription;
    }

    public String getOfferDescriptionUpper() {
        return offerDescriptionUpper;
    }

    public void setOfferDescriptionUpper(String offerDescriptionUpper) {
        this.offerDescriptionUpper = offerDescriptionUpper;
    }

    public String getOfferDescriptionIButton() {
        return offerDescriptionIButton;
    }

    public void setOfferDescriptionIButton(String offerDescriptionIButton) {
        this.offerDescriptionIButton = offerDescriptionIButton;
    }

    public String getOfferType() {
        return offerType;
    }

    public void setOfferType(String offerType) {
        this.offerType = offerType;
    }

    public String getCanoneMensile() {
        return canoneMensile;
    }

    public void setCanoneMensile(String canoneMensile) {
        this.canoneMensile = canoneMensile;
    }

    public String getCanoneMensileConv() {
        return canoneMensileConv;
    }

    public void setCanoneMensileConv(String canoneMensileConv) {
        this.canoneMensileConv = canoneMensileConv;
    }

    public String getCanonePromo() {
        return canonePromo;
    }

    public void setCanonePromo(String canonePromo) {
        this.canonePromo = canonePromo;
    }

    public String getCallAzIta() {
        return callAzIta;
    }

    public void setCallAzIta(String callAzIta) {
        this.callAzIta = callAzIta;
    }

    public String getCallIta() {
        return callIta;
    }

    public void setCallIta(String callIta) {
        this.callIta = callIta;
    }

    public String getSmsIta() {
        return smsIta;
    }

    public void setSmsIta(String smsIta) {
        this.smsIta = smsIta;
    }

    public String getDatiIta() {
        return datiIta;
    }

    public void setDatiIta(String datiIta) {
        this.datiIta = datiIta;
    }

    public String getCallEst() {
        return callEst;
    }

    public void setCallEst(String callEst) {
        this.callEst = callEst;
    }

    public String getSmsEst() {
        return smsEst;
    }

    public void setSmsEst(String smsEst) {
        this.smsEst = smsEst;
    }

    public String getDatiEst() {
        return datiEst;
    }

    public void setDatiEst(String datiEst) {
        this.datiEst = datiEst;
    }

    public String getFasciaVoce() {
        return fasciaVoce;
    }

    public void setFasciaVoce(String fasciaVoce) {
        this.fasciaVoce = fasciaVoce;
    }

    public String getFasciaDati() {
        return fasciaDati;
    }

    public void setFasciaDati(String fasciaDati) {
        this.fasciaDati = fasciaDati;
    }

    public boolean isSelectedStep3() {
        return isSelectedStep3;
    }

    public void setSelectedStep3(boolean selectedStep3) {
        isSelectedStep3 = selectedStep3;
    }

    public int getSelectedNumSimStep3() {
        return selectedNumSimStep3;
    }

    public void setSelectedNumSimStep3(int selectedNumSimStep3) {
        this.selectedNumSimStep3 = selectedNumSimStep3;
    }

    public ArrayList<AddOnMobile> getAddOnMobileArrayList() {
        if (addOnMobileArrayList == null){
            return new ArrayList<>();
        }
        return addOnMobileArrayList;
    }

    public void setAddOnMobileArrayList(ArrayList<AddOnMobile> addOnMobileArrayList) {
        if (this.addOnMobileArrayList == null){
            this.addOnMobileArrayList = new ArrayList<>();
        }
        this.addOnMobileArrayList = addOnMobileArrayList;
    }

    public ArrayList<Terminale> getTerminaleArrayList() {
        if (terminaleArrayList == null){
            return new ArrayList<>();
        }
        return terminaleArrayList;
    }

    public void setTerminaleArrayList(ArrayList<Terminale> terminaleArrayList) {
        if (this.terminaleArrayList == null){
            this.terminaleArrayList = new ArrayList<>();
        }
        this.terminaleArrayList = terminaleArrayList;
    }

    public ArrayList<OpzioneMobile> getOpzioneMobileArrayList() {
        if (opzioneMobileArrayList == null){
            return new ArrayList<>();
        }
        return opzioneMobileArrayList;
    }

    public void setOpzioneMobileArrayList(ArrayList<OpzioneMobile> opzioneMobileArrayList) {
        if (this.opzioneMobileArrayList == null){
            this.opzioneMobileArrayList = new ArrayList<>();
        }
        this.opzioneMobileArrayList = opzioneMobileArrayList;
    }

    public ArrayList<TerminaleBean> getTerminaleBeanArrayList() {
        if (terminaleBeanArrayList == null){
            return new ArrayList<>();
        }
        return terminaleBeanArrayList;
    }

    public void setTerminaleBeanArrayList(ArrayList<TerminaleBean> terminaleBeanArrayList) {
        if (this.terminaleBeanArrayList == null){
            this.terminaleBeanArrayList = new ArrayList<>();
        }
        this.terminaleBeanArrayList = terminaleBeanArrayList;
    }

    public String getPercentualeScontoPenale() {
        return percentualeScontoPenale;
    }

    public void setPercentualeScontoPenale(String percentualeScontoPenale) {
        this.percentualeScontoPenale = percentualeScontoPenale;
    }

    public ArrayList<PercentualeSconto> getPercentualeScontoArrayList() {
        if (percentualeScontoArrayList == null){
            return new ArrayList<>();
        }
        return percentualeScontoArrayList;
    }

    public void setPercentualeScontoArrayList(ArrayList<PercentualeSconto> percentualeScontoArrayList) {
        if (this.percentualeScontoArrayList == null){
            this.percentualeScontoArrayList = new ArrayList<>();
        }
        this.percentualeScontoArrayList = percentualeScontoArrayList;
    }

    public int getSelectedNumSimStep4() {
        return selectedNumSimStep4;
    }

    public void setSelectedNumSimStep4(int selectedNumSimStep4) {
        this.selectedNumSimStep4 = selectedNumSimStep4;
    }

    public String getScontoApplicato() {
        return scontoApplicato;
    }

    public void setScontoApplicato(String scontoApplicato) {
        this.scontoApplicato = scontoApplicato;
    }

    public String getCanoneScontato() {
        return canoneScontato;
    }

    public void setCanoneScontato(String canoneScontato) {
        this.canoneScontato = canoneScontato;
    }

    public String getAutorizzabilita() {
        return autorizzabilita;
    }

    public void setAutorizzabilita(String autorizzabilita) {
        this.autorizzabilita = autorizzabilita;
    }

    public boolean isScontoPenaleApplicato() {
        return isScontoPenaleApplicato;
    }

    public void setScontoPenaleApplicato(boolean scontoPenaleApplicato) {
        isScontoPenaleApplicato = scontoPenaleApplicato;
    }

    public String getMnpStep8() {
        return mnpStep8;
    }

    public void setMnpStep8(String mnpStep8) {
        this.mnpStep8 = mnpStep8;
    }

    public AddOnMobile getAddOnMobileStep8() {
        return addOnMobileStep8;
    }

    public void setAddOnMobileStep8(AddOnMobile addOnMobileStep8) {
        this.addOnMobileStep8 = addOnMobileStep8;
    }

    public ArrayList<OpzioneMobile> getOpzioneMobileArrayListStep8() {
        return opzioneMobileArrayListStep8;
    }

    public void setOpzioneMobileArrayListStep8(ArrayList<OpzioneMobile> opzioneMobileArrayListStep8) {
        this.opzioneMobileArrayListStep8 = opzioneMobileArrayListStep8;
    }

    public String getOldICCID() {
        return oldICCID;
    }

    public void setOldICCID(String oldICCID) {
        this.oldICCID = oldICCID;
    }

    public String getNewICCID() {
        return newICCID;
    }

    public void setNewICCID(String newICCID) {
        this.newICCID = newICCID;
    }

    public String getOldMSISDN() {
        return oldMSISDN;
    }

    public void setOldMSISDN(String oldMSISDN) {
        this.oldMSISDN = oldMSISDN;
    }

    public ArrayList<AddOnPackage> getAddOnPackageArrayListStep8() {
        if (addOnPackageArrayListStep8 == null){
            return new ArrayList<>();
        }
        return addOnPackageArrayListStep8;
    }

    public void setAddOnPackageArrayListStep8(ArrayList<AddOnPackage> addOnPackageArrayListStep8) {
        if (this.addOnPackageArrayListStep8 == null){
            this.addOnPackageArrayListStep8 = new ArrayList<>();
        }
        this.addOnPackageArrayListStep8 = addOnPackageArrayListStep8;
    }

    public ArrayList<OpzioneMobile> getOpzioniMobileSelectedArrayListStep8() {
        if (opzioniMobileSelectedArrayListStep8 == null){
            return new ArrayList<>();
        }
        return opzioniMobileSelectedArrayListStep8;
    }

    public void setOpzioniMobileSelectedArrayListStep8(ArrayList<OpzioneMobile> opzioniMobileSelectedArrayListStep8) {
        if (this.opzioniMobileSelectedArrayListStep8 == null){
            this.opzioniMobileSelectedArrayListStep8 = new ArrayList<>();
        }
        this.opzioniMobileSelectedArrayListStep8 = opzioniMobileSelectedArrayListStep8;
    }

    public boolean isMnp() {
        return isMnp;
    }

    public void setMnp(boolean mnp) {
        isMnp = mnp;
    }

    public ArrayList<Terminale> getTerminaliToShow() {
        if (terminaliToShow == null){
            return new ArrayList<>();
        }
        return terminaliToShow;
    }

    public void setTerminaliToShow(ArrayList<Terminale> terminaliToShow) {
        if (this.terminaliToShow == null){
            this.terminaliToShow = new ArrayList<>();
        }
        this.terminaliToShow = terminaliToShow;
    }

    public Terminale getTerminale1() {
        return terminale1;
    }

    public void setTerminale1(Terminale terminale1) {
        this.terminale1 = terminale1;
    }

    public Terminale getTerminale2() {
        return terminale2;
    }

    public void setTerminale2(Terminale terminale2) {
        this.terminale2 = terminale2;
    }

    public ArrayList<Terminale> getTerminaliToShow2() {
        if (terminaliToShow2 == null){
            return new ArrayList<>();
        }
        return terminaliToShow2;
    }

    public void setTerminaliToShow2(ArrayList<Terminale> terminaliToShow2) {
        if (this.terminaliToShow2 == null){
            this.terminaliToShow2 = new ArrayList<>();
        }
        this.terminaliToShow2 = terminaliToShow2;
    }

    public ArrayList<Terminale> getTerminaliDiAppoggio() {
        if (terminaliDiAppoggio == null){
            return new ArrayList<>();
        }
        return terminaliDiAppoggio;
    }

    public void setTerminaliDiAppoggio(ArrayList<Terminale> terminaliDiAppoggio) {
        if (this.terminaliDiAppoggio == null){
            this.terminaliDiAppoggio = new ArrayList<>();
        }
        this.terminaliDiAppoggio = terminaliDiAppoggio;
    }
}
