package it.wind.smartsales.entities;

/**
 * Created by luca.quaranta on 24/03/2016.
 */
public class AddOnMobile {

    private String idOfferta;
    private String descFascia;
    private String prezzoFascia;
    private String maxSimVendibile;
    private int selectedNumSimStep3;
    private int selectedNumSimStep8;
    private String selectedLabelStep8;
    //MW Aggiunto campo per memorizzare socnto su Gb pagina Sconti
    private String scontoApplicato;

    private String extraBundlePrezzo;
    private String extraBundleDescrizione;
    private String extraBundleMaxNumeroRinnovi;
    private String extraBundleDescrizionePDF;

    public String getExtraBundlePrezzo() {
        return extraBundlePrezzo;
    }

    public void setExtraBundlePrezzo(String extraBundlePrezzo) {
        this.extraBundlePrezzo = extraBundlePrezzo;
    }

    public String getExtraBundleDescrizione() {
        return extraBundleDescrizione;
    }

    public void setExtraBundleDescrizione(String extraBundleDescrizione) {
        this.extraBundleDescrizione = extraBundleDescrizione;
    }

    public String getExtraBundleMaxNumeroRinnovi() {
        return extraBundleMaxNumeroRinnovi;
    }

    public void setExtraBundleMaxNumeroRinnovi(String extraBundleMaxNumeroRinnovi) {
        this.extraBundleMaxNumeroRinnovi = extraBundleMaxNumeroRinnovi;
    }

    public String getExtraBundleDescrizionePDF() {
        return extraBundleDescrizionePDF;
    }

    public void setExtraBundleDescrizionePDF(String extraBundleDescrizionePDF) {
        this.extraBundleDescrizionePDF = extraBundleDescrizionePDF;
    }

//MW Aggiunto il controllo del campo in piu ScontoApplicato

    public String getScontoGigaApplicato() {
        return scontoApplicato;
    }

    public void setScontoGigaApplicato(String scontoApplicato) {
        this.scontoApplicato = scontoApplicato;
    }


    public String getIdOfferta() {
        return idOfferta;
    }

    public void setIdOfferta(String idOfferta) {
        this.idOfferta = idOfferta;
    }

    public String getDescFascia() {
        return descFascia;
    }

    public void setDescFascia(String descFascia) {
        this.descFascia = descFascia;
    }

    public String getPrezzoFascia() {
        return prezzoFascia;
    }

    public void setPrezzoFascia(String prezzoFascia) {
        this.prezzoFascia = prezzoFascia;
    }

    public String getMaxSimVendibile() {
        return maxSimVendibile;
    }

    public void setMaxSimVendibile(String maxSimVendibile) {
        this.maxSimVendibile = maxSimVendibile;
    }

    public int getSelectedNumSimStep3() {
        return selectedNumSimStep3;
    }

    public void setSelectedNumSimStep3(int selectedNumSimStep3) {
        this.selectedNumSimStep3 = selectedNumSimStep3;
    }

    public int getSelectedNumSimStep8() {
        return selectedNumSimStep8;
    }

    public void setSelectedNumSimStep8(int selectedNumSimStep8) {
        this.selectedNumSimStep8 = selectedNumSimStep8;
    }

    public String getSelectedLabelStep8() {
        return selectedLabelStep8;
    }

    public void setSelectedLabelStep8(String selectedLabelStep8) {
        this.selectedLabelStep8 = selectedLabelStep8;
    }
}
