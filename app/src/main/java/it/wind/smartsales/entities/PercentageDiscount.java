package it.wind.smartsales.entities;

/**
 * Created by luca.quaranta on 17/03/2016.
 */
public class PercentageDiscount {

    float discount;

    public PercentageDiscount(float discount){
        this.discount = discount;
    }

    public float getDiscount() {
        return discount;
    }

    public void setDiscount(float discount) {
        this.discount = discount;
    }
}
