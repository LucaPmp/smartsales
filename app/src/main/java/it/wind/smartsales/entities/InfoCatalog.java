package it.wind.smartsales.entities;

/**
 * Created by luca.quaranta on 30/03/2016.
 */
public class InfoCatalog {

    private int catalogIdentifier;
    private String catalogVersion;
    private String catalogLastUpdate;

    public int getCatalogIdentifier() {
        return catalogIdentifier;
    }

    public void setCatalogIdentifier(int catalogIdentifier) {
        this.catalogIdentifier = catalogIdentifier;
    }

    public String getCatalogVersion() {
        return catalogVersion;
    }

    public void setCatalogVersion(String catalogVersion) {
        this.catalogVersion = catalogVersion;
    }

    public String getCatalogLastUpdate() {
        return catalogLastUpdate;
    }

    public void setCatalogLastUpdate(String catalogLastUpdate) {
        this.catalogLastUpdate = catalogLastUpdate;
    }
}
