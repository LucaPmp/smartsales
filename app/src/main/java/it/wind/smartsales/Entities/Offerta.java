package it.wind.smartsales.entities;

import java.io.Serializable;

/**
 * Created by a.finocchiaro on 18/07/2016.
 * project: smartsales
 */
public abstract class Offerta implements Serializable {
    public abstract int getIdOfferta();

    public abstract String getOfferDescription();

    public abstract String getOfferDescriptionUpper();

    public abstract String getCanoneMensile();
}
