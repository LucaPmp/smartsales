package it.wind.smartsales.task;

import android.content.Context;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.util.Base64;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;

import it.wind.smartsales.commons.Constants;

/**
 * Created by luca.quaranta on 15/03/2016.
 */
public class PreviewTask {

    public static void callTask(Context context, final Handler.Callback callback, final String xml){

        Request reqXML = new StringRequest(Request.Method.POST, Constants.URLS.URL_PREVIEW, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                String pdfResponse = response.substring(response.indexOf("<pdfBytes>") + "<pdfBytes>".length(),response.indexOf("</pdfBytes>"));
                byte[] pdfByteArray = Base64.decode(pdfResponse, Base64.DEFAULT);


                try{
                    long lenghtOfFile = pdfByteArray.length;

                    //covert reponse to input stream
                    InputStream input = new ByteArrayInputStream(pdfByteArray);

                    //Create a file on desired path and write stream data to it
                    File path = Environment.getExternalStorageDirectory();
                    File file = new File(path, "windpdf.pdf");
                    BufferedOutputStream output = new BufferedOutputStream(new FileOutputStream(file));
                    byte data[] = new byte[1024];

                    long total = 0;

                    int count = 0;
                    while ((count = input.read(data)) != -1) {
                        total += count;
                        output.write(data, 0, count);
                    }

                    output.flush();

                    output.close();
                    input.close();
                }catch(IOException e){
                    e.printStackTrace();

                }



                Message msg = new Message();
                msg.what = Constants.HandlerWhat.WHAT_PREVIEW;
                callback.handleMessage(msg);

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Message msg = new Message();
                msg.what = Constants.HandlerWhat.WHAT_PREVIEW_KO;
                callback.handleMessage(msg);
            }
        }){
            @Override
            public byte[] getBody() throws AuthFailureError {

                try {
                    return xml == null ? null :
                            xml.getBytes(getParamsEncoding());
                } catch (UnsupportedEncodingException uee) {
                    return null;
                }
            }

            @Override
            public String getBodyContentType() {
                return "text/xml; charset=UTF-8";
            }
        };

        reqXML.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        VolleyTask.getInstance(context).addToRequestQueue(reqXML);
    }



}
