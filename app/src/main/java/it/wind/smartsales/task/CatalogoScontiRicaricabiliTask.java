package it.wind.smartsales.task;

import android.content.Context;
import android.os.Handler;
import android.os.Message;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

import it.wind.smartsales.commons.Constants;
import it.wind.smartsales.commons.Log;
import it.wind.smartsales.dao.CatalogoFasceScontiRicaricabiliDAO;
import it.wind.smartsales.entities.FasceScontiRicaricabili;


/**
 * Created by luca.quaranta on 15/03/2016.
 */
public class CatalogoScontiRicaricabiliTask {

    public static void callTask(final Context context, final Handler.Callback callback, JSONObject downloadCatalogoScontiRicaricabiliRequest) {
        Log.d("REQUEST", "[" + Constants.URLS.URL_CATALOGO_SCONTI_RICARICABILI + "] - " + downloadCatalogoScontiRicaricabiliRequest.toString());

        Request request = new JsonObjectRequest(Request.Method.POST, Constants.URLS.URL_CATALOGO_SCONTI_RICARICABILI, downloadCatalogoScontiRicaricabiliRequest, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d("RESPONSE", "[" + Constants.URLS.URL_CATALOGO_SCONTI_RICARICABILI + "] - " + response.toString());

                CatalogoFasceScontiRicaricabiliDAO.clearFasceScontiRicaricabiliTable(context);
                //CatalogoPianiTariffariDAO.clearPianiTariffariTable(context);

                try {
                    Message msg = new Message();

                    if (response.getString("resultCode").compareToIgnoreCase("OK") == 0) {
                        JSONObject downloadCatalogoScontiRicaricabiliResponse = response.getJSONObject("CatalogoScontiRicaricabiliResponse");

                        FasceScontiRicaricabili fasceScontiRicaricabili = new FasceScontiRicaricabili();
                        fasceScontiRicaricabili.setFasciaSpesa1(Double.valueOf(downloadCatalogoScontiRicaricabiliResponse.getString("fasciaSpesa1")).intValue());
                        fasceScontiRicaricabili.setFasciaSpesa2(Double.valueOf(downloadCatalogoScontiRicaricabiliResponse.getString("fasciaSpesa2")).intValue());
                        fasceScontiRicaricabili.setFasciaSpesa3(Double.valueOf(downloadCatalogoScontiRicaricabiliResponse.getString("fasciaSpesa3")).intValue());

                        if(downloadCatalogoScontiRicaricabiliResponse.getString("fasciaSpesa4").equals("")){
                            fasceScontiRicaricabili.setFasciaSpesa4(0);
                        }else{
                            fasceScontiRicaricabili.setFasciaSpesa4(Double.valueOf(downloadCatalogoScontiRicaricabiliResponse.getString("fasciaSpesa4")).intValue());
                        }

                        fasceScontiRicaricabili.setFasciaSconto1(Double.valueOf(downloadCatalogoScontiRicaricabiliResponse.getString("scontoFasciaSpesa1")).intValue());
                        fasceScontiRicaricabili.setFasciaSconto2(Double.valueOf(downloadCatalogoScontiRicaricabiliResponse.getString("scontoFasciaSpesa2")).intValue());
                        fasceScontiRicaricabili.setFasciaSconto3(Double.valueOf(downloadCatalogoScontiRicaricabiliResponse.getString("scontoFasciaSpesa3")).intValue());
                        fasceScontiRicaricabili.setFasciaSconto4(Double.valueOf(downloadCatalogoScontiRicaricabiliResponse.getString("scontoFasciaSpesa4")).intValue());

                        CatalogoFasceScontiRicaricabiliDAO.insertFasceScontiRicaricabili(context, fasceScontiRicaricabili);

                        msg.what = Constants.HandlerWhat.WHAT_CATALOGO_SCONTI_RICARICABILI;
                        callback.handleMessage(msg);
                    } else {
                        msg.what = Constants.HandlerWhat.WHAT_CATALOGO_SCONTI_RICARICABILI_KO;
                        callback.handleMessage(msg);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("RESPONSE", "[" + Constants.URLS.URL_CATALOGO_SCONTI_RICARICABILI + "] - " + error.networkResponse);
            }
        });
        VolleyTask.getInstance(context).addToRequestQueue(request);
    }

}
