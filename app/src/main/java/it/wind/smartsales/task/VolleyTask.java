package it.wind.smartsales.task;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

/**
 * Created by luca.quaranta on 08/02/2016.
 */
public class VolleyTask {

    private static VolleyTask instance;
    private RequestQueue requestQueue;
    private static Context context;

    private VolleyTask(){
        getRequestQueue();
    };

    public static VolleyTask getInstance(Context _context){
        if (instance == null){
            context = _context;
            instance = new VolleyTask();
        }
        return instance;
    }

    private RequestQueue getRequestQueue(){
        if (requestQueue == null){
            requestQueue = Volley.newRequestQueue(context);
        }
        return requestQueue;
    }

    public void addToRequestQueue(Request req){
        getRequestQueue().add(req);
    }

}
