package it.wind.smartsales.task;

import android.content.Context;
import android.os.Handler;
import android.os.Message;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import it.wind.smartsales.commons.Constants;
import it.wind.smartsales.commons.Log;
import it.wind.smartsales.dao.CatalogoFasceScontiDAO;
import it.wind.smartsales.dao.CatalogoPianiTariffariDAO;
import it.wind.smartsales.entities.FasceSconti;
import it.wind.smartsales.entities.PianoTariffario;

/**
 * Created by luca.quaranta on 15/03/2016.
 */
public class CatalogoScontiTask {

    public static void callTask(final Context context, final Handler.Callback callback, JSONObject downloadCatalogoScontiRequest) {
        Log.d("REQUEST", "[" + Constants.URLS.URL_CATALOGO_SCONTI + "] - " + downloadCatalogoScontiRequest.toString());

        Request request = new JsonObjectRequest(Request.Method.POST, Constants.URLS.URL_CATALOGO_SCONTI, downloadCatalogoScontiRequest, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d("RESPONSE", "[" + Constants.URLS.URL_CATALOGO_SCONTI + "] - " + response.toString());

                CatalogoFasceScontiDAO.clearFasceScontiTable(context);
                CatalogoPianiTariffariDAO.clearPianiTariffariTable(context);

                try {
                    Message msg = new Message();

                    if (response.getString("resultCode").compareToIgnoreCase("OK") == 0) {
                        JSONObject downloadCatalogoScontiResponse = response.getJSONObject("CatalogoScontiResponse");

                        FasceSconti fasceSconti = new FasceSconti();
                        fasceSconti.setFasciaSim1(Integer.parseInt(downloadCatalogoScontiResponse.getString("fasciaSim1")));
                        fasceSconti.setFasciaSim2(Integer.parseInt(downloadCatalogoScontiResponse.getString("fasciaSim2")));
                        fasceSconti.setFasciaSim3(Integer.parseInt(downloadCatalogoScontiResponse.getString("fasciaSim3")));
                        fasceSconti.setFasciaSpesa1(Integer.parseInt(downloadCatalogoScontiResponse.getString("fasciaSpesa1")));
                        fasceSconti.setFasciaSpesa2(Integer.parseInt(downloadCatalogoScontiResponse.getString("fasciaSpesa2")));

                        CatalogoFasceScontiDAO.insertFasceSconti(context, fasceSconti);

                        JSONArray tariffPlansJsonArray = downloadCatalogoScontiResponse.getJSONArray("pianiTariffari");
                        for (int i = 0; i < tariffPlansJsonArray.length(); i++) {
                            JSONObject tariffPlanJsonObject = (JSONObject) tariffPlansJsonArray.get(i);
                            PianoTariffario pianoTariffario = new PianoTariffario();

                            pianoTariffario.setIdOfferta(tariffPlanJsonObject.getString("idOfferta"));
//                            tariffPlan.setRepaymentPenalties(tariffPlanJsonObject.getString("rimborsoPenali").compareTo("YES") == 0 ? true : false);
                            pianoTariffario.setPercentualeScontoPenale(tariffPlanJsonObject.getString("percentualeScontoPenale"));

                            JSONArray percentageDiscountsJsonArray = tariffPlanJsonObject.getJSONArray("percentualiSconto");
                            for (int j = 0; j < percentageDiscountsJsonArray.length(); j++) {
                                pianoTariffario.setPercentualeSconto(((JSONObject) percentageDiscountsJsonArray.get(j)).getString("percentuale").replace(",", "."));
                                CatalogoPianiTariffariDAO.insertPianoTariffario(context, pianoTariffario);
                            }
                        }

                        msg.what = Constants.HandlerWhat.WHAT_CATALOGO_SCONTI;
                        callback.handleMessage(msg);
                    } else {
                        msg.what = Constants.HandlerWhat.WHAT_CATALOGO_SCONTI_KO;
                        callback.handleMessage(msg);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("RESPONSE", "[" + Constants.URLS.URL_CATALOGO_SCONTI + "] - " + error.networkResponse);
            }
        });
        VolleyTask.getInstance(context).addToRequestQueue(request);
    }

}
