package it.wind.smartsales.task;

import android.content.Context;
import android.os.Handler;
import android.os.Message;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

import it.wind.smartsales.commons.Constants;
import it.wind.smartsales.commons.Log;
import it.wind.smartsales.commons.Utils;
import it.wind.smartsales.entities.Dealer;
import it.wind.smartsales.entities.Session;

/**
 * Created by luca.quaranta on 15/03/2016.
 */
public class LoginTask {

    public static void callTask(final Context context, final Handler.Callback callback, JSONObject loginRequest){
        Log.d("REQUEST", "[" + Constants.URLS.URL_LOGIN + "] - " + loginRequest.toString());

        Request request = new JsonObjectRequest(Request.Method.POST, Constants.URLS.URL_LOGIN, loginRequest, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d("RESPONSE", "[" + Constants.URLS.URL_LOGIN + "] - " + response.toString());

                try {
                    Message msg = new Message();

                    if (response.getString("resultCode").compareToIgnoreCase("OK") == 0){
                        JSONObject loginResponse =  response.getJSONObject("LoginResponse");
                        Session.getInstance().setToken(loginResponse.getString("token"));

                        JSONObject infoDealer =  loginResponse.getJSONObject("InfoDealer");

                        Dealer dealer = new Dealer();
                        dealer.setDealerCode(infoDealer.getString("codiceDealer"));
                        dealer.setDealerName(infoDealer.getString("nome"));
                        dealer.setDealerSurname(infoDealer.getString("cognome"));
                        dealer.setDealerMail(infoDealer.getString("email"));
                        dealer.setEmailBackOffice(infoDealer.getString("emailBackOffice"));
                        dealer.setIsCreationOfferEnabled(Boolean.parseBoolean(infoDealer.getString("isCreationOfferEnabled")));
                        dealer.setNomeAgenziaVenditore(infoDealer.getString("nomeAgenziaVenditore"));
                        dealer.setNomeClientManager(infoDealer.getString("nomeClientManager"));
                        dealer.setDealerCity(infoDealer.getString("cityAgenzia"));
                        dealer.setCityAgenzia(infoDealer.getString("cityAgenzia"));
                        dealer.setDealerPhone(infoDealer.getString("cellulare"));
                        dealer.setDealerCountry(infoDealer.getString("countryAgenzia"));
                        dealer.setDealerRole(infoDealer.getString("ruolo"));
                        dealer.setDealerAgency(infoDealer.getString("nomeAgenziaVenditore"));

                        Session.getInstance().setDealer(dealer);
                        Session.getInstance().setAdUrl(loginResponse.getString("adUrlAndroid"));
                        Session.getInstance().setAppVersion(loginResponse.getString("appVersionAndroid"));

                        msg.what = Constants.HandlerWhat.WHAT_LOGIN;
                        callback.handleMessage(msg);
                    } else {
                        msg.what = Constants.HandlerWhat.WHAT_LOGIN_KO;
                        callback.handleMessage(msg);
                        Utils.showWarningMessage(context, "", response.getString("resultDescription"));
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("RESPONSE", "[" + Constants.URLS.URL_LOGIN + "] - " + error.networkResponse);
                Message msg = new Message();
                msg.what = Constants.HandlerWhat.WHAT_LOGIN_KO;
                callback.handleMessage(msg);
            }
        });

        request.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        VolleyTask.getInstance(context).addToRequestQueue(request);
    }

}
