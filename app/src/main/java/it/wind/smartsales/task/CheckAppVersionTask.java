package it.wind.smartsales.task;

import android.content.Context;
import android.os.Handler;
import android.os.Message;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

import it.wind.smartsales.commons.Constants;
import it.wind.smartsales.commons.Log;
import it.wind.smartsales.entities.Session;

/**
 * Created by luca.quaranta on 15/03/2016.
 */
public class CheckAppVersionTask {

    public static void callTask(Context context, final Handler.Callback callback, JSONObject checkAppVersionRequest){
        Log.d("REQUEST", "[" + Constants.URLS.URL_CHECK_APP_VERSION + "] - " + checkAppVersionRequest.toString());

        Request request = new JsonObjectRequest(Request.Method.POST, Constants.URLS.URL_CHECK_APP_VERSION, checkAppVersionRequest, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d("RESPONSE", "[" + Constants.URLS.URL_CHECK_APP_VERSION + "] - " + response.toString());

                try {
                    Message msg = new Message();

                    JSONObject checkAppVersionResponse =  response.getJSONObject("CheckAppVersionResponse");
                    if (response.getString("resultCode").compareToIgnoreCase("OK") == 0){
                        msg.what = Constants.HandlerWhat.WHAT_CHECK_APP_VERSION;
                        callback.handleMessage(msg);
                    } else {
                        Session.getInstance().setAppDownloadUrl(checkAppVersionResponse.getString("urlDownoladApp"));
                        msg.what = Constants.HandlerWhat.WHAT_CHECK_APP_VERSION_KO;
                        callback.handleMessage(msg);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("RESPONSE", "[" + Constants.URLS.URL_CHECK_APP_VERSION + "] - " + error.networkResponse);
            }
        });
        VolleyTask.getInstance(context).addToRequestQueue(request);
    }

}
