package it.wind.smartsales.task;

import android.content.Context;
import android.os.Handler;
import android.os.Message;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import it.wind.smartsales.commons.Constants;
import it.wind.smartsales.commons.Log;
import it.wind.smartsales.dao.CatalogoOfferteFissoDAO;
import it.wind.smartsales.dao.CatalogoOpzioniFissoDAO;
import it.wind.smartsales.entities.OffertaFisso;
import it.wind.smartsales.entities.OpzioneFisso;

/**
 * Created by luca.quaranta on 15/03/2016.
 */
public class CatalogoOfferteFissoTask {

    public static void callTask(final Context context, final Handler.Callback callback, JSONObject catalogoOfferteFissoRequest) {
        Log.d("REQUEST", "[" + Constants.URLS.URL_CATALOGO_OFFERTE_FISSO + "] - " + catalogoOfferteFissoRequest.toString());

        Request request = new JsonObjectRequest(Request.Method.POST, Constants.URLS.URL_CATALOGO_OFFERTE_FISSO, catalogoOfferteFissoRequest, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d("RESPONSE", "[" + Constants.URLS.URL_CATALOGO_OFFERTE_FISSO + "] - " + response.toString());

                CatalogoOfferteFissoDAO.clearOffertaFissoTable(context);
                CatalogoOpzioniFissoDAO.clearOpzioniFissoTable(context);

                try {
                    Message msg = new Message();

                    if (response.getString("resultCode").compareToIgnoreCase("OK") == 0) {
                        JSONObject downloadCatalogoOfferteFissoResponse = response.getJSONObject("CatalogoOfferteFissoResponse");

                        JSONArray offerteFissoJsonArray = downloadCatalogoOfferteFissoResponse.getJSONArray("offerteFisso");
                        for (int i = 0; i < offerteFissoJsonArray.length(); i++) {
                            JSONObject offertaFissoJsonObject = (JSONObject) offerteFissoJsonArray.get(i);
                            OffertaFisso offertaFisso = new OffertaFisso();

                            offertaFisso.setIdOfferta(Integer.parseInt(offertaFissoJsonObject.getString("idOfferta")));
                            offertaFisso.setOfferDescription(offertaFissoJsonObject.getString("offerDescription"));
                            offertaFisso.setOfferType(offertaFissoJsonObject.getString("offerType"));
                            offertaFisso.setTechnology(offertaFissoJsonObject.getString("tecnologia"));
                            offertaFisso.setContributoUTNuovaLinea(offertaFissoJsonObject.getString("costoUnaTantumNuovaLinea").replace(",", "."));
                            offertaFisso.setContributoUTLineaAttiva(offertaFissoJsonObject.getString("costoUnaTantumLineaAttiva").replace(",", "."));
                            offertaFisso.setCanoneMensile(offertaFissoJsonObject.getString("canoneMensile").replace(",", "."));
                            offertaFisso.setCanoneBimestrale(offertaFissoJsonObject.getString("canoneBimestrale").replace(",", "."));
                            offertaFisso.setCanoneBimestraleConv(offertaFissoJsonObject.getString("canoneMensileConv").replace(",", "."));
//                            offertaFisso.setCanonePromo(offertaFissoJsonObject.getString("canonePromo").replace(",", "."));
                            offertaFisso.setCallItaFissoWind(offertaFissoJsonObject.getString("callItaFissoWind"));
                            offertaFisso.setCallItaFisso(offertaFissoJsonObject.getString("callItaFisso"));
                            offertaFisso.setCallItaMobileWind(offertaFissoJsonObject.getString("callItaMobileWind"));
                            offertaFisso.setCallItaMobile(offertaFissoJsonObject.getString("callItaMobile"));
                            offertaFisso.setAdslIta(offertaFissoJsonObject.getString("adslIta"));
                            offertaFisso.setCallEstFissoArea1(offertaFissoJsonObject.getString("callEstFisso"));

                            CatalogoOfferteFissoDAO.insertOffertaFisso(context, offertaFisso);
                        }

                        JSONArray opzioniFissoJsonArray = downloadCatalogoOfferteFissoResponse.getJSONArray("opzioni");
                        for (int i = 0; i < opzioniFissoJsonArray.length(); i++) {
                            JSONObject opzioneFissoJsonObject = (JSONObject) opzioniFissoJsonArray.get(i);
                            OpzioneFisso opzioneFisso = new OpzioneFisso();

                            opzioneFisso.setIdOpzione(opzioneFissoJsonObject.getString("idOpzione"));
                            opzioneFisso.setDefault(Boolean.valueOf(opzioneFissoJsonObject.getString("isDefault")));
                            opzioneFisso.setButtonType(opzioneFissoJsonObject.getString("buttonType"));
                            opzioneFisso.setOpzioneDesc(opzioneFissoJsonObject.getString("opzioneDesc"));
                            opzioneFisso.setPdcSection(opzioneFissoJsonObject.getString("pdcSection"));
                            opzioneFisso.setOpzioneDescUpper(opzioneFissoJsonObject.getString("opzioneDescUpper"));
                            opzioneFisso.setOpzioneDescriptionIButton(opzioneFissoJsonObject.getString("opzioneDescriptionIButton"));
                            opzioneFisso.setScontoConvergenza(Boolean.valueOf(opzioneFissoJsonObject.getString("scontoConvergenza")));
                            opzioneFisso.setCostoUnaTantum(opzioneFissoJsonObject.getString("costoUnaTantum").replace(",", "."));
                            opzioneFisso.setCanoneMensile(opzioneFissoJsonObject.getString("canoneMensile").replace(",", "."));

                            JSONArray offerteJsonArray = opzioneFissoJsonObject.getJSONArray("offerte");
                            for (int j = 0; j < offerteJsonArray.length(); j++) {
                                opzioneFisso.setIdOfferta(((JSONObject) offerteJsonArray.get(j)).getString("idOfferta"));
                                CatalogoOpzioniFissoDAO.insertOpzioneFisso(context, opzioneFisso);
                            }

                        }

                        msg.what = Constants.HandlerWhat.WHAT_CATALOGO_OFFERTE_FISSO;
                        callback.handleMessage(msg);
                    } else {
                        msg.what = Constants.HandlerWhat.WHAT_CATALOGO_OFFERTE_FISSO_KO;
                        callback.handleMessage(msg);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("RESPONSE", "[" + Constants.URLS.URL_CATALOGO_OFFERTE_FISSO + "] - " + error.networkResponse);
            }
        });
        VolleyTask.getInstance(context).addToRequestQueue(request);
    }

}
