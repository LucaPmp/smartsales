package it.wind.smartsales.task.requests;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by luca.quaranta on 15/03/2016.
 */
public class DownloadCatalogoRequest extends Request {

    protected String requestName = "DownloadCatalogoRequest";

    @Override
    public String getRequestName() {
        return requestName;
    }

    public void setCatalogVersion(String catalogVersion) throws JSONException{
        put("catalogVersion", catalogVersion);
    }

    public void setCatalogIdentifier(String catalogIdentifier) throws JSONException{
        put("catalogIdentifier", catalogIdentifier);
    }

    public void setCatalogLastUpdate(String catalogLastUpdate) throws JSONException{
        put("catalogLastUpdate", catalogLastUpdate);
    }

    public void setToken(String token) throws JSONException{
        put("token", token);
    }

    @Override
    public JSONObject put(String name, Object value) throws JSONException {
        return super.put(name, value);
    }
}
