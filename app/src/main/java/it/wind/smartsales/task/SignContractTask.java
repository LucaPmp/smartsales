package it.wind.smartsales.task;

import android.content.Context;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import java.io.UnsupportedEncodingException;

import it.wind.smartsales.commons.Constants;
import it.wind.smartsales.commons.Log;

/**
 * Created by luca.quaranta on 15/03/2016.
 */
public class SignContractTask {

    public static void callTask(Context context, final TaskListener callback, final String xml){

        Request reqXML = new StringRequest(Request.Method.POST, Constants.URLS.URL_SEND_CONTRACT, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Log.i("signresp",response);
                if (response!=null && response.indexOf("<workstepId>")>=0) {
                    String workstepid = response.substring(response.indexOf("<workstepId>")+12,response.indexOf("</workstepId>"));
                    Log.i("workstepid",workstepid);
                    callback.onSuccess(workstepid);
                } else {
                    callback.onError("Error calling Xyzmo signing SDK: no response");
                }

                /*@TODO: call xyzmo sdk passing workstepid*/


//                Message msg = new Message();
//                msg.what = Constants.HandlerWhat.WHAT_SIGN_CONTRACT;
//                callback.handleMessage(msg);

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//                Message msg = new Message();
//                msg.what = Constants.HandlerWhat.WHAT_SIGN_CONTRACT_KO;
//                callback.handleMessage(msg);
                if (error!=null) {
                    callback.onError(error.getMessage());
                } else {
                    callback.onError("Error calling Xyzmo signing SDK");
                }
            }
        }){
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return xml == null ? null : xml.getBytes(getParamsEncoding());
                } catch (UnsupportedEncodingException uee) {
                    return null;
                }
            }

            @Override
            public String getBodyContentType() {
                return "text/xml; charset=UTF-8";
            }
        };

        reqXML.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        VolleyTask.getInstance(context).addToRequestQueue(reqXML);
    }

}
