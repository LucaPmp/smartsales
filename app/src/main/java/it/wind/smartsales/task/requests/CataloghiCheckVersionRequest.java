package it.wind.smartsales.task.requests;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by luca.quaranta on 15/03/2016.
 */
public class CataloghiCheckVersionRequest extends Request {

    protected String requestName = "CataloghiCheckVersionRequest";

    @Override
    public String getRequestName() {
        return requestName;
    }

    public void setToken(String token) throws JSONException{
        put("token", token);
    }

    @Override
    public JSONObject put(String name, Object value) throws JSONException {
        return super.put(name, value);
    }
}
