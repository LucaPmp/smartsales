package it.wind.smartsales.task;

import android.content.Context;
import android.os.Handler;
import android.os.Message;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import it.wind.smartsales.commons.Constants;
import it.wind.smartsales.commons.Log;
import it.wind.smartsales.dao.CatalogoAddOnMobileDAO;
import it.wind.smartsales.dao.CatalogoOfferteMobileDAO;
import it.wind.smartsales.dao.CatalogoOpzioniMobileDAO;
import it.wind.smartsales.entities.AddOnMobile;
import it.wind.smartsales.entities.OffertaMobile;
import it.wind.smartsales.entities.OpzioneMobile;

/**
 * Created by luca.quaranta on 15/03/2016.
 */
public class CatalogoOfferteMobileTask {

    public static void callTask(final Context context, final Handler.Callback callback, JSONObject catalogoOfferteMobileRequest) {
        Log.d("REQUEST", "[" + Constants.URLS.URL_CATALOGO_OFFERTE_MOBILE + "] - " + catalogoOfferteMobileRequest.toString());

        Request request = new JsonObjectRequest(Request.Method.POST, Constants.URLS.URL_CATALOGO_OFFERTE_MOBILE, catalogoOfferteMobileRequest, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d("RESPONSE", "[" + Constants.URLS.URL_CATALOGO_OFFERTE_MOBILE + "] - " + response.toString());

                CatalogoOfferteMobileDAO.clearOffertaMobileTable(context);
                CatalogoOpzioniMobileDAO.clearOpzioniMobileTable(context);
                CatalogoAddOnMobileDAO.clearAddOnMobileTable(context);

                try {
                    Message msg = new Message();

                    if (response.getString("resultCode").compareToIgnoreCase("OK") == 0) {
                        JSONObject downloadCatalogoOfferteMobileResponse = response.getJSONObject("CatalogoOfferteMobileResponse");

                        JSONArray offerteMobileJsonArray = downloadCatalogoOfferteMobileResponse.getJSONArray("offerteMobile");
                        for (int i = 0; i < offerteMobileJsonArray.length(); i++) {
                            JSONObject offertaMobileJsonObject = (JSONObject) offerteMobileJsonArray.get(i);
                            OffertaMobile offertaMobile = new OffertaMobile();

                            offertaMobile.setIdOfferta(Integer.parseInt(offertaMobileJsonObject.getString("idOfferta")));
                            offertaMobile.setOfferDescription(offertaMobileJsonObject.getString("offerDescription"));
                            offertaMobile.setOfferDescriptionUpper(offertaMobileJsonObject.getString("offerDescriptionUpper"));
                            offertaMobile.setOfferDescriptionIButton(offertaMobileJsonObject.getString("offerDescriptionIButton"));
                            offertaMobile.setOfferType(offertaMobileJsonObject.getString("offerType"));
                            offertaMobile.setCanoneMensile(offertaMobileJsonObject.getString("canoneMensile").replace(",", "."));
                            offertaMobile.setCanoneMensileConv(offertaMobileJsonObject.getString("canoneMensileConv").replace(",", "."));
                            offertaMobile.setCanonePromo(offertaMobileJsonObject.getString("canonePromo").replace(",", "."));
                            offertaMobile.setCallAzIta(offertaMobileJsonObject.getString("callAzIta"));
                            offertaMobile.setCallIta(offertaMobileJsonObject.getString("callIta"));
                            offertaMobile.setSmsIta(offertaMobileJsonObject.getString("smsIta"));
                            offertaMobile.setDatiIta(offertaMobileJsonObject.getString("datiIta"));
                            offertaMobile.setCallEst(offertaMobileJsonObject.getString("callEst"));
                            offertaMobile.setSmsEst(offertaMobileJsonObject.getString("smsEst"));
                            offertaMobile.setDatiEst(offertaMobileJsonObject.getString("datiEst"));
                            offertaMobile.setFasciaVoce(offertaMobileJsonObject.getString("fasciaVoce"));
                            offertaMobile.setFasciaDati(offertaMobileJsonObject.getString("fasciaDati"));

                            CatalogoOfferteMobileDAO.insertOffertaMobile(context, offertaMobile);
                        }

                        JSONArray opzioniMobileJsonArray = downloadCatalogoOfferteMobileResponse.getJSONArray("opzioni");
                        for (int i = 0; i < opzioniMobileJsonArray.length(); i++) {
                            JSONObject opzioneFissoJsonObject = (JSONObject) opzioniMobileJsonArray.get(i);
                            OpzioneMobile opzioneMobile = new OpzioneMobile();

                            opzioneMobile.setIdOpzione(opzioneFissoJsonObject.getString("idOpzione"));
                            opzioneMobile.setOfferType(opzioneFissoJsonObject.getString("offerType"));
                            opzioneMobile.setOpzioneDesc(opzioneFissoJsonObject.getString("opzioneDesc"));
                            opzioneMobile.setOpzioneDescUpper(opzioneFissoJsonObject.getString("opzioneDescUpper"));
                            opzioneMobile.setOpzioneDescriptionIButton(opzioneFissoJsonObject.getString("opzioneDescriptionIButton"));
                            opzioneMobile.setCanoneMensile(opzioneFissoJsonObject.getString("canoneMensile").replace(",", "."));
                            opzioneMobile.setCanoneSettimanale(opzioneFissoJsonObject.getString("canoneSettimanale").replace(",", "."));
                            opzioneMobile.setCanoneBimestrale(opzioneFissoJsonObject.getString("canoneBimestrale").replace(",", "."));
                            opzioneMobile.setVoce(opzioneFissoJsonObject.getString("voce"));
                            opzioneMobile.setSms(opzioneFissoJsonObject.getString("sms"));
                            opzioneMobile.setDati(opzioneFissoJsonObject.getString("dati"));
                            opzioneMobile.setScontoConvergenza(opzioneFissoJsonObject.getString("scontoConvergenza"));
                            opzioneMobile.setCostoUnaTantum(opzioneFissoJsonObject.getString("costoUnaTantum"));
                            opzioneMobile.setIsDefault(opzioneFissoJsonObject.getString("isDefault"));
                            opzioneMobile.setIsDoubleChecked(opzioneFissoJsonObject.getString("isDoubleChecked"));

                            if (Boolean.parseBoolean(opzioneMobile.getIsDefault())) {
                                opzioneMobile.setIdOfferta("NaN");
                                CatalogoOpzioniMobileDAO.insertOpzioneMobile(context, opzioneMobile);
                            } else {
                                JSONArray offerteJsonArray = opzioneFissoJsonObject.getJSONArray("offerte");
                                for (int j = 0; j < offerteJsonArray.length(); j++) {
                                    opzioneMobile.setIdOfferta(((JSONObject) offerteJsonArray.get(j)).getString("idOfferta"));
                                    CatalogoOpzioniMobileDAO.insertOpzioneMobile(context, opzioneMobile);
                                }
                            }

                        }

                        JSONArray addOnMobileJsonArray = downloadCatalogoOfferteMobileResponse.getJSONArray("addOn");
                        for (int i = 0; i < addOnMobileJsonArray.length(); i++) {
                            JSONObject addOnJsonObject = (JSONObject) addOnMobileJsonArray.get(i);
                            AddOnMobile addOnMobile = new AddOnMobile();

                            addOnMobile.setDescFascia(addOnJsonObject.getString("descFascia"));
                            addOnMobile.setPrezzoFascia(addOnJsonObject.getString("prezzoFascia").replace(",", "."));
                            addOnMobile.setMaxSimVendibile(addOnJsonObject.getString("maxSimVendibile"));

                            addOnMobile.setExtraBundlePrezzo(addOnJsonObject.getString("extrabundlePrezzo"));
                            addOnMobile.setExtraBundleDescrizione(addOnJsonObject.getString("extrabundleDescrizione"));
                            addOnMobile.setExtraBundleMaxNumeroRinnovi(addOnJsonObject.getString("extrabundleMaxNumeroRinnovi"));
                            addOnMobile.setExtraBundleDescrizionePDF(addOnJsonObject.getString("extrabundleDescrizionePdf"));


                            JSONArray offerteJsonArray = addOnJsonObject.getJSONArray("offerte");
                            for (int j = 0; j < offerteJsonArray.length(); j++) {
                                addOnMobile.setIdOfferta(((JSONObject) offerteJsonArray.get(j)).getString("idOfferta"));
                                CatalogoAddOnMobileDAO.insertAddOnMobile(context, addOnMobile);
                            }

                        }

                        msg.what = Constants.HandlerWhat.WHAT_CATALOGO_OFFERTE_MOBILE;
                        callback.handleMessage(msg);
                    } else {
                        msg.what = Constants.HandlerWhat.WHAT_CATALOGO_OFFERTE_MOBILE_KO;
                        callback.handleMessage(msg);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("RESPONSE", "[" + Constants.URLS.URL_CATALOGO_OFFERTE_MOBILE + "] - " + error.networkResponse);
            }
        });
        VolleyTask.getInstance(context).addToRequestQueue(request);
    }

}
