package it.wind.smartsales.task;

import android.content.Context;
import android.os.Handler;
import android.os.Message;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import java.io.UnsupportedEncodingException;

import it.wind.smartsales.commons.Constants;
import it.wind.smartsales.commons.Log;

/**
 * Created by luca.quaranta on 15/03/2016.
 */
public class SendMailTask {

    private static String TAG = "SendMailTask";

    public static void callTask(Context context, final Handler.Callback callback, final String xml){
//        Log.d("REQUEST", "[" + Constants.URLS.URL_CHECK_APP_VERSION + "] - " + checkAppVersionRequest.toString());

        Request reqXML = new StringRequest(Request.Method.POST, Constants.URLS.URL_SEND_MAIL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Message msg = new Message();
                msg.what = Constants.HandlerWhat.WHAT_MAIL;
                callback.handleMessage(msg);

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Message msg = new Message();
                msg.what = Constants.HandlerWhat.WHAT_MAIL_KO;
                callback.handleMessage(msg);
            }
        }){
            @Override
            public byte[] getBody() throws AuthFailureError {

                try {
                    return xml == null ? null :
                            xml.getBytes(getParamsEncoding());
                } catch (UnsupportedEncodingException uee) {
                    Log.i(TAG,"errore invio mail");
                    return null;
                }
            }

            @Override
            public String getBodyContentType() {
                return "application/xml; charset=" +
                        getParamsEncoding();
            }
        };

        VolleyTask.getInstance(context).addToRequestQueue(reqXML);
    }

}
