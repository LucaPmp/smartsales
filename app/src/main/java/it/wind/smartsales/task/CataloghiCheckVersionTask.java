package it.wind.smartsales.task;

import android.content.Context;
import android.os.Handler;
import android.os.Message;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import it.wind.smartsales.commons.Constants;
import it.wind.smartsales.commons.Log;
import it.wind.smartsales.entities.InfoCatalog;
import it.wind.smartsales.entities.Session;

/**
 * Created by luca.quaranta on 15/03/2016.
 */
public class CataloghiCheckVersionTask {

    public static void callTask(final Context context, final Handler.Callback callback, JSONObject cataloghiCheckVersionRequest) {
        Log.d("REQUEST", "[" + Constants.URLS.URL_CATALOGHI_CHECK_VERSION + "] - " + cataloghiCheckVersionRequest.toString());

        Request request = new JsonObjectRequest(Request.Method.POST, Constants.URLS.URL_CATALOGHI_CHECK_VERSION, cataloghiCheckVersionRequest, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d("RESPONSE", "[" + Constants.URLS.URL_CATALOGHI_CHECK_VERSION + "] - " + response.toString());

                try {
                    Message msg = new Message();

                    if (response.getString("resultCode").compareToIgnoreCase("OK") == 0) {
                        JSONObject catalogoCheckVersionResponse = response.getJSONObject("CataloghiCheckVersionResponse");

                        JSONArray infoCatalogJsonArray = catalogoCheckVersionResponse.getJSONArray("InfoCatalog");
                        ArrayList<InfoCatalog> infoCatalogs = new ArrayList<>();
                        for (int i = 0; i < infoCatalogJsonArray.length(); i++) {
                            JSONObject infoCatalogJsonObject = (JSONObject) infoCatalogJsonArray.get(i);
                            InfoCatalog infoCatalog = new InfoCatalog();

                            infoCatalog.setCatalogIdentifier(Integer.parseInt(infoCatalogJsonObject.getString("catalogIdentifier")));
                            infoCatalog.setCatalogVersion(infoCatalogJsonObject.getString("catalogVersion"));
                            infoCatalog.setCatalogLastUpdate(infoCatalogJsonObject.getString("catalogLastUpdate"));

                            infoCatalogs.add(infoCatalog);
                        }

                        Session.getInstance().setInfoCatalogs(infoCatalogs);

                        msg.what = Constants.HandlerWhat.WHAT_CATALOGHI_CHECK_VERSION;
                        callback.handleMessage(msg);
                    } else {
                        msg.what = Constants.HandlerWhat.WHAT_CATALOGHI_CHECK_VERSION_KO;
                        callback.handleMessage(msg);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("RESPONSE", "[" + Constants.URLS.URL_CATALOGHI_CHECK_VERSION + "] - " + error.networkResponse);
            }
        });
        VolleyTask.getInstance(context).addToRequestQueue(request);
    }

}
