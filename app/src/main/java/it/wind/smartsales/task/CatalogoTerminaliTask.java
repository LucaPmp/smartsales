package it.wind.smartsales.task;

import android.content.Context;
import android.os.Handler;
import android.os.Message;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import it.wind.smartsales.commons.Constants;
import it.wind.smartsales.commons.Log;
import it.wind.smartsales.dao.CatalogoTerminaliDAO;
import it.wind.smartsales.entities.Terminale;

/**
 * Created by luca.quaranta on 15/03/2016.
 */
public class CatalogoTerminaliTask {

    public static void callTask(final Context context, final Handler.Callback callback, JSONObject downloadCatalogoTerminaliRequest) {
        Log.d("REQUEST", "[" + Constants.URLS.URL_CATALOGO_TERMINALI + "] - " + downloadCatalogoTerminaliRequest.toString());

        Request request = new JsonObjectRequest(Request.Method.POST, Constants.URLS.URL_CATALOGO_TERMINALI, downloadCatalogoTerminaliRequest, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d("RESPONSE", "[" + Constants.URLS.URL_CATALOGO_TERMINALI + "] - " + response.toString());

                CatalogoTerminaliDAO.clearTerminaliTable(context);

                try {
                    Message msg = new Message();

                    if (response.getString("resultCode").compareToIgnoreCase("OK") == 0) {
                        JSONObject downloadCatalogoTerminaliResponse = response.getJSONObject("CatalogoTerminaliResponse");

                        JSONArray terminaliJsonArray = downloadCatalogoTerminaliResponse.getJSONArray("terminali");
                        for (int i = 0; i < terminaliJsonArray.length(); i++) {
                            JSONObject terminaleJsonObject = (JSONObject) terminaliJsonArray.get(i);
                            Terminale terminale = new Terminale();

                            terminale.setIdTerminale(terminaleJsonObject.getString("idTerminale"));
                            terminale.setOem(terminaleJsonObject.getString("oem"));
                            terminale.setTerminaleBrand(terminaleJsonObject.getString("terminaleBrand"));
                            terminale.setTerminaleDescription(terminaleJsonObject.getString("terminaleDescription"));
                            terminale.setMnp(terminaleJsonObject.getString("mnp"));
                            terminale.setOfferType(terminaleJsonObject.getString("offerType"));
                            terminale.setDeviceType(terminaleJsonObject.getString("deviceType"));
                            terminale.setPrezzoListino(terminaleJsonObject.getString("prezzoListino").replace(",", "."));
                            terminale.setRataIniziale(terminaleJsonObject.getString("rataIniziale").replace(",", "."));
                            terminale.setRataMensile(terminaleJsonObject.getString("rataMensile").replace(",", "."));
                            terminale.setRataFinale(terminaleJsonObject.getString("rataFinale").replace(",", "."));
                            terminale.setImageUrl(terminaleJsonObject.getString("imageUrl"));
                            terminale.setColor(terminaleJsonObject.getString("deviceColour"));
                            terminale.setCapacity(terminaleJsonObject.getString("deviceCapacity"));
                            terminale.setShortDesc(terminaleJsonObject.getString("deviceShortDesc"));

                            JSONArray offerteJsonArray = terminaleJsonObject.getJSONArray("offerte");
                            for (int j = 0; j < offerteJsonArray.length(); j++) {
                                terminale.setIdOfferta(((JSONObject) offerteJsonArray.get(j)).getString("idOfferta"));
                                CatalogoTerminaliDAO.insertTerminale(context, terminale);
                            }
                        }

                        msg.what = Constants.HandlerWhat.WHAT_CATALOGO_TERMINALI;
                        callback.handleMessage(msg);
                    } else {
                        msg.what = Constants.HandlerWhat.WHAT_CATALOGO_TERMINALI_KO;
                        callback.handleMessage(msg);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("RESPONSE", "[" + Constants.URLS.URL_CATALOGO_TERMINALI + "] - " + error.networkResponse);
            }
        });
        VolleyTask.getInstance(context).addToRequestQueue(request);
    }

}