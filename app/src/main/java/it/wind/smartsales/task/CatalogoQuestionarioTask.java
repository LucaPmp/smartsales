package it.wind.smartsales.task;

import android.content.Context;
import android.os.Handler;
import android.os.Message;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import it.wind.smartsales.commons.Constants;
import it.wind.smartsales.commons.Log;
import it.wind.smartsales.dao.CatalogoQuestionarioDAO;
import it.wind.smartsales.entities.Question;

/**
 * Created by luca.quaranta on 15/03/2016.
 */
public class CatalogoQuestionarioTask {

    public static void callTask(final Context context, final Handler.Callback callback, JSONObject downloadCatalogoQuestionarioRequest) {
        Log.d("REQUEST", "[" + Constants.URLS.URL_CATALOGO_QUESTIONARIO + "] - " + downloadCatalogoQuestionarioRequest.toString());

        Request request = new JsonObjectRequest(Request.Method.POST, Constants.URLS.URL_CATALOGO_QUESTIONARIO, downloadCatalogoQuestionarioRequest, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d("RESPONSE", "[" + Constants.URLS.URL_CATALOGO_QUESTIONARIO + "] - " + response.toString());

                CatalogoQuestionarioDAO.clearQuestionarioTable(context);

                try {
                    Message msg = new Message();

                    if (response.getString("resultCode").compareToIgnoreCase("OK") == 0) {
                        JSONObject downloadCatalogoQuestionarioResponse = response.getJSONObject("CatalogoQuestionarioResponse");

                        JSONArray questionsJsonArray = downloadCatalogoQuestionarioResponse.getJSONArray("questions");
                        for (int i = 0; i < questionsJsonArray.length(); i++) {
                            JSONObject questionJsonObject = (JSONObject) questionsJsonArray.get(i);
                            Question question = new Question();

                            question.setQuestionCategory(questionJsonObject.getString("questionCategory"));
                            question.setQuestionNumber(questionJsonObject.getString("questionNumber"));
                            question.setQuestionDescription(questionJsonObject.getString("questionDescription"));

                            CatalogoQuestionarioDAO.insertQuestion(context, question);
                        }

                        msg.what = Constants.HandlerWhat.WHAT_CATALOGO_QUESTIONARIO;
                        callback.handleMessage(msg);
                    } else {
                        msg.what = Constants.HandlerWhat.WHAT_CATALOGO_QUESTIONARIO_KO;
                        callback.handleMessage(msg);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("RESPONSE", "[" + Constants.URLS.URL_CATALOGO_QUESTIONARIO + "] - " + error.networkResponse);
            }
        });
        VolleyTask.getInstance(context).addToRequestQueue(request);
    }

}
