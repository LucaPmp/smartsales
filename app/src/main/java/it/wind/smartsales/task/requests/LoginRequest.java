package it.wind.smartsales.task.requests;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by luca.quaranta on 15/03/2016.
 */
public class LoginRequest extends Request {

    protected String requestName = "LoginRequest";

    @Override
    public String getRequestName() {
        return requestName;
    }

    public void setUsername(String username) throws JSONException{
        put("username", username);
    }

    public void setPassword(String password) throws JSONException{
        put("password", password);
    }

    public void setTabletId(String tabletId) throws JSONException{
        put("tabletId", tabletId);
    }

    public void setOS(String os) throws JSONException{
        put("os", os);
    }

    @Override
    public JSONObject put(String name, Object value) throws JSONException {
        return super.put(name, value);
    }
}
