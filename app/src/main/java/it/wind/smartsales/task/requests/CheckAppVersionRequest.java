package it.wind.smartsales.task.requests;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by luca.quaranta on 15/03/2016.
 */
public class CheckAppVersionRequest extends Request {

    protected String requestName = "CheckAppVersionRequest";

    @Override
    public String getRequestName() {
        return requestName;
    }

    public void setOS(String os) throws JSONException{
        put("OS", os);
    }

    public void setAppVersion(String appVersion) throws JSONException{
        put("appVersion", appVersion);
    }

    public void setOSVersion(String OSVersion) throws JSONException{
        put("OSVersion", OSVersion);
    }

    public void setToken(String token) throws JSONException{
        put("token", token);
    }

    @Override
    public JSONObject put(String name, Object value) throws JSONException {
        return super.put(name, value);
    }
}
