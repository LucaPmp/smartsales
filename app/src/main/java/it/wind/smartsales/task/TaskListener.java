package it.wind.smartsales.task;

/**
 * Created by cresh on 15/07/16.
 */
public interface TaskListener {
    void onSuccess(String message);

    void onError (String message);
}
