package it.wind.smartsales.commons;

/**
 * Created by luca.quaranta on 16/02/2016.
 */
public class Constants {

    public static final String IS_REMEMBERME_CHECKED = "isRememberMeChecked";
    public static final String LOGIN_USERNAME = "loginUsername";
    public static final String LOGIN_PASSWORD = "loginPassword";
    public static final String baseUrlProd = "http://wesbeta.wind.it:8180/";
    public static final String baseUrlTest = "https://wind-test.it";
    //public static final String baseUrlStub = "http://151.1.80.252:8180";
    public static final String baseUrlStub = "http://wesbeta.wind.it:8180/";

    public static void setEnvironment(String environment) {
        switch (environment) {
            case Environments.PROD:
                URLS.BASE_URL = baseUrlProd;
                URLS.URL_LOGIN = URLS.BASE_URL + "";
//                URLS.URL_PWD_RECOVERY = "https://easysales.wind.it/user/password";
                URLS.URL_LOGIN = "https://easysalesbeta.wind.it/API/user";
                URLS.URL_PWD_RECOVERY = "https://easysalesbeta.wind.it/user/password";
                URLS.URL_CHECK_APP_VERSION = URLS.BASE_URL + "";
                URLS.URL_CATALOGHI_CHECK_VERSION = URLS.BASE_URL + "";
                URLS.URL_CATALOGO_OFFERTE_FISSO = URLS.BASE_URL + "";
                URLS.URL_CATALOGO_OFFERTE_MOBILE = URLS.BASE_URL + "";
                URLS.URL_CATALOGO_SCONTI = URLS.BASE_URL + "";
                URLS.URL_CATALOGO_SCONTI_RICARICABILI = URLS.BASE_URL + "";
                URLS.URL_CATALOGO_TERMINALI = URLS.BASE_URL + "";
                URLS.URL_CATALOGO_QUESTIONARIO = URLS.BASE_URL + "";
                URLS.URL_SEND_MAIL = URLS.BASE_URL + "";
                URLS.URL_PREVIEW = URLS.BASE_URL + "";
                URLS.URL_SEND_CONTRACT = URLS.BASE_URL + "";

                break;
            case Environments.TEST:
                URLS.BASE_URL = baseUrlTest;
                URLS.URL_LOGIN = URLS.BASE_URL + "";
                URLS.URL_CHECK_APP_VERSION = URLS.BASE_URL + "";
                URLS.URL_CATALOGHI_CHECK_VERSION = URLS.BASE_URL + "";
                URLS.URL_CATALOGO_OFFERTE_FISSO = URLS.BASE_URL + "";
                URLS.URL_CATALOGO_OFFERTE_MOBILE = URLS.BASE_URL + "";
                URLS.URL_CATALOGO_SCONTI = URLS.BASE_URL + "";
                URLS.URL_CATALOGO_SCONTI_RICARICABILI = URLS.BASE_URL + "";
                URLS.URL_CATALOGO_TERMINALI = URLS.BASE_URL + "";
                URLS.URL_CATALOGO_QUESTIONARIO = URLS.BASE_URL + "";
                URLS.URL_SEND_MAIL = URLS.BASE_URL + "";
                URLS.URL_PREVIEW = URLS.BASE_URL + "";
                URLS.URL_SEND_CONTRACT = URLS.BASE_URL + "";
                break;
            case Environments.STUB:
                URLS.BASE_URL = baseUrlStub;
//                URLS.URL_LOGIN = "http://rinaldoenel.altervista.org/Wind/login.json";
//                URLS.URL_LOGIN = "http://151.1.80.172:8180/wind_smart/api/user";
                URLS.URL_LOGIN = "https://easysalesbeta.wind.it/API/user";
                URLS.URL_PWD_RECOVERY = "https://easysalesbeta.wind.it/user/password";


                URLS.URL_CHECK_APP_VERSION = "http://rinaldoenel.altervista.org/Wind/checkAppVersion.json";
                URLS.URL_CATALOGHI_CHECK_VERSION = URLS.BASE_URL + "/catalog/cataloghiCheckVersion.json";
                URLS.URL_CATALOGO_OFFERTE_FISSO = URLS.BASE_URL + "/catalog/downloadCatalogoOfferteFisso.json";
                URLS.URL_CATALOGO_OFFERTE_MOBILE = URLS.BASE_URL + "/catalog/downloadCatalogoOfferteMobile.json";
                URLS.URL_CATALOGO_SCONTI = URLS.BASE_URL + "/catalog/downloadCatalogoSconti.json";
                URLS.URL_CATALOGO_SCONTI_RICARICABILI = URLS.BASE_URL + "/catalog/downloadCatalogoScontiRicaricabili.json";
                URLS.URL_CATALOGO_TERMINALI = URLS.BASE_URL + "/catalog/downloadCatalogoTerminali.json";
                URLS.URL_CATALOGO_QUESTIONARIO = URLS.BASE_URL + "/catalog/downloadCatalogoQuestionario.json";
                URLS.URL_SEND_MAIL = URLS.BASE_URL + "/tablet-wind-ws/sendemail";
                URLS.URL_PREVIEW = URLS.BASE_URL + "/tablet-wind-ws/InsertPdaSoap";
                URLS.URL_SEND_CONTRACT = URLS.BASE_URL + "/tablet-wind-ws/InsertPdaSoap";
                break;
        }
        Log.d("ENVIRONMENT", "Set environment to: " + environment);
    }

    public static class CompatibleDimensionScreen{
        public static final float MIN_DIAGONAL = 9.7f;
        public static final float MAX_DIAGONAL = 10.9f;
    }

//    https://easysales.wind.it/API/user
//    http://151.1.80.252:8180/ base url tablet server

    public static class Environments {
        public static final String PROD = "PROD";
        public static final String TEST = "TEST";
        public static final String STUB = "STUB";
    }

    public static class URLS{
        public static String BASE_URL;
        public static String URL_LOGIN = BASE_URL;
        public static String URL_CHECK_APP_VERSION;
        public static String URL_CATALOGHI_CHECK_VERSION;
        public static String URL_CATALOGO_OFFERTE_FISSO;
        public static String URL_CATALOGO_OFFERTE_MOBILE;
        public static String URL_CATALOGO_SCONTI;
        public static String URL_CATALOGO_SCONTI_RICARICABILI;
        public static String URL_CATALOGO_TERMINALI;
        public static String URL_CATALOGO_QUESTIONARIO;
        public static String URL_SEND_MAIL;
        public static String URL_PREVIEW;
        public static String URL_SEND_CONTRACT;
        public static String URL_PWD_RECOVERY;
    }

    public static class TariffPlanPreAutirization {
        public static final String NULL = "";
        public static final String OVER_MAX = "Non Pre-Autorizzato";
        public static final String OK = "Pre-Autorizzato";
    }

    public static class CatalogIdentifiers{
        public static final int CATALOGO_OFFERTE_FISSO = 1;
        public static final int CATALOGO_OFFERTE_MOBILE = 2;
        public static final int CATALOGO_SCONTI = 3;
        public static final int CATALOGO_TERMINALI = 4;
        public static final int CATALOGO_QUESTIONARIO = 5;
        public static final int CATALOGO_6 = 6;
        public static final int CATALOGO_SCONTI_RICARICABILI = 7;

    }

    public static class Pages{
        public static final int PAGE_HOME_ADMIN = 0;
        public static final int PAGE_MOBILE_INTERNET_CREATE_OFFER = 1;
        public static final int PAGE_MOBILE_INTERNET_SELECT_OFFER = 2;
        public static final int PAGE_DRAFT = 3;
        public static final int PAGE_MOBILE_TERMINALS_SELECT = 4;
        public static final int PAGE_WIRELINE_INTERNET_CREATE_OFFER = 5;
    }

    public static class PaesiName{
        public static final String MOBILE_ZONA_UE_TAG = "Zona UE";
        public static final String MOBILE_AREA_1_TAG = "Area 1";
        public static final String MOBILE_USA_TAG = "USA";
        public static final String MOBILE_PRINCIPATO_DI_MONACO_TAG = "Principato di Monaco";
        public static final String MOBILE_SAN_MARINO_TAG = "San Marino";
        public static final String FISSO_AREA_1_TAG = "Area 1";
        public static final String FISSO_AREA_2_TAG = "Area 2";
        public static final String FISSO_AREA_3_TAG = "Area 3";
        public static final String FISSO_AREA_4_TAG = "Area 4";
        public static final String FISSO_AREA_5_TAG = "Area 5";
        public static final String FISSO_AREA_6_TAG = "Area 6";
        public static final String FISSO_AREA_7_TAG = "Area 7";
    }

    public static class PaesiTag{
        public static final String MOBILE_ZONA_UE_TAG = "mobileZonaUe";
        public static final String MOBILE_AREA_1_TAG = "mobileArea1";
        public static final String MOBILE_USA_TAG = "mobileUsa";
        public static final String MOBILE_PRINCIPATO_DI_MONACO_TAG = "mobilePrincipatoDiMonaco";
        public static final String MOBILE_SAN_MARINO_TAG = "mobileSanMarino";
        public static final String FISSO_AREA_1_TAG = "fissoArea1";
        public static final String FISSO_AREA_2_TAG = "fissoArea2";
        public static final String FISSO_AREA_3_TAG = "fissoArea3";
        public static final String FISSO_AREA_4_TAG = "fissoArea4";
        public static final String FISSO_AREA_5_TAG = "fissoArea5";
        public static final String FISSO_AREA_6_TAG = "fissoArea6";
        public static final String FISSO_AREA_7_TAG = "fissoArea7";
    }

    public class HandlerWhat {
        public static final int WHAT_LOGIN = 100;
        public static final int WHAT_LOGIN_KO = 101;
        public static final int WHAT_CHECK_APP_VERSION = 150;
        public static final int WHAT_CHECK_APP_VERSION_KO = 151;
        public static final int WHAT_CATALOGHI_CHECK_VERSION = 200;
        public static final int WHAT_CATALOGHI_CHECK_VERSION_KO = 201;

        public static final int WHAT_CATALOGO_OFFERTE_FISSO = 250;
        public static final int WHAT_CATALOGO_OFFERTE_FISSO_KO = 251;
        public static final int WHAT_CATALOGO_OFFERTE_MOBILE = 300;
        public static final int WHAT_CATALOGO_OFFERTE_MOBILE_KO = 301;
        public static final int WHAT_CATALOGO_SCONTI = 350;
        public static final int WHAT_CATALOGO_SCONTI_KO = 351;
        public static final int WHAT_CATALOGO_SCONTI_RICARICABILI = 352;
        public static final int WHAT_CATALOGO_SCONTI_RICARICABILI_KO = 353;
        public static final int WHAT_CATALOGO_TERMINALI = 400;
        public static final int WHAT_CATALOGO_TERMINALI_KO = 401;
        public static final int WHAT_CATALOGO_QUESTIONARIO = 450;
        public static final int WHAT_CATALOGO_QUESTIONARIO_KO = 451;
        public static final int WHAT_MAIL = 500;
        public static final int WHAT_MAIL_KO = 501;
        public static final int WHAT_PREVIEW = 550;
        public static final int WHAT_PREVIEW_KO = 551;
        public static final int WHAT_SEND_CONTRACT = 600;
        public static final int WHAT_SEND_CONTRACT_KO = 601;
    }
}