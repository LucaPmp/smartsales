package it.wind.smartsales.commons;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import it.wind.smartsales.R;
import it.wind.smartsales.customviews.selectoffer.SelectDeviceDialogFragment;
import it.wind.smartsales.customviews.selectoffer.SelectGbDialogFragment;
import it.wind.smartsales.customviews.selectoffer.SelectMailDialogFragment;
import it.wind.smartsales.customviews.selectoffer.SelectMnpDialogFragment;
import it.wind.smartsales.customviews.selectoffer.SelectScontiDialogFragment;
import it.wind.smartsales.entities.AddOnMobile;
import it.wind.smartsales.entities.AddOnPackage;
import it.wind.smartsales.entities.ExtraGBDetail;
import it.wind.smartsales.entities.OffertaMobile;
import it.wind.smartsales.entities.OpzioneMobile;
import it.wind.smartsales.entities.PercentualeSconto;
import it.wind.smartsales.entities.Terminale;
import it.wind.smartsales.entities.TerminaleBean;
import it.wind.smartsales.fragments.DeviceDialogFragment;
import it.wind.smartsales.fragments.ExtraGbDialogFragment;
import it.wind.smartsales.fragments.GbDialogFragment;
import it.wind.smartsales.fragments.InfoDialogFragment;
import it.wind.smartsales.fragments.MailDialogFragment;
import it.wind.smartsales.fragments.MnpDialogFragment;
import it.wind.smartsales.fragments.MobileInternetCreateOfferStep4Fragment;
import it.wind.smartsales.fragments.MobileInternetCreateOfferStep5Fragment;
import it.wind.smartsales.fragments.MobileInternetCreateOfferStep8Fragment;
import it.wind.smartsales.fragments.NoteDialogFragment;
import it.wind.smartsales.fragments.ScontiDialogFragment;
import it.wind.smartsales.fragments.selectoffer.MobileInternetSelectOfferStep5Fragment;
import it.wind.smartsales.fragments.selectoffer.MobileInternetSelectOfferStep8Fragment;

//import it.wind.smartsales.Fragments.MobileInternetCreateOfferStep4Fragment;

/**
 * Created by luca.quaranta on 15/03/2016.
 */
public class Utils {

    public static void showGenericMessage(Context context, String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton(context.getString(R.string.login_warning_ok_button_label), new android.content.DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

    public static AlertDialog showGenericMessageWithReturn(Context context, String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setCancelable(false);

        return builder.show();
    }

    public static void showWarningMessage(Context context, String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(context.getString(R.string.login_warning_title));
        builder.setMessage(message);
        builder.setCancelable(false);
        builder.setPositiveButton(context.getString(R.string.login_warning_ok_button_label), new android.content.DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

    public static void showNotCompatibilityMessage(final Activity context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(context.getString(R.string.splash_warning_title));
        builder.setMessage(context.getString(R.string.splash_warning_message));
        builder.setCancelable(false);
        builder.setPositiveButton(context.getString(R.string.splash_warning_close_button_label), new android.content.DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                context.finish();
            }
        });
        builder.show();
    }

    public static void showNewVersionMessage(final Activity context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(context.getString(R.string.splash_warning_title));
        builder.setMessage("E' disponibile una nuova versione dell'app.");
        builder.setCancelable(false);
        builder.setPositiveButton(context.getString(R.string.splash_warning_close_button_label), new android.content.DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                context.finish();
            }
        });
        builder.show();
    }

    public static boolean isEmailValid(String email) {
        boolean isValid = false;

        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        CharSequence inputStr = email;

        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        if (matcher.matches()) {
            isValid = true;
        }
        return isValid;
    }

//    public static void showProgress(ImageView imageView) {
//        imageView.setBackgroundResource(R.drawable.custom_progress);
//
//        // Get the background, which has been compiled to an AnimationDrawable object.
//        AnimationDrawable frameAnimation = (AnimationDrawable) imageView.getBackground();
//
//        // Start the animation (looped playback by default).
//        frameAnimation.start();
//    }

    public static boolean checkConn(Context ctx) {
        ConnectivityManager conMgr = (ConnectivityManager) ctx
                .getSystemService(Context.CONNECTIVITY_SERVICE);

        if (conMgr != null) {
            NetworkInfo networkInfo = conMgr.getActiveNetworkInfo();
            if (networkInfo != null && networkInfo.isConnected()) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public static ArrayList<String> getListaPaesiByTag(Context context, String tagJsonArray) {
        ArrayList<String> paesi = new ArrayList<>();
        try {
            JSONArray jsonArray = null;
            switch (tagJsonArray) {
                case Constants.PaesiTag.MOBILE_ZONA_UE_TAG:
                    jsonArray = new JSONObject(context.getString(R.string.mobile_zona_ue).replace(" ", "")).getJSONArray(tagJsonArray);
                    break;
                case Constants.PaesiTag.MOBILE_AREA_1_TAG:
                    jsonArray = new JSONObject(context.getString(R.string.mobile_area_1).replace(" ", "")).getJSONArray(tagJsonArray);
                    break;
                case Constants.PaesiTag.MOBILE_USA_TAG:
                    jsonArray = new JSONObject(context.getString(R.string.mobile_usa).replace(" ", "")).getJSONArray(tagJsonArray);
                    break;
                case Constants.PaesiTag.MOBILE_PRINCIPATO_DI_MONACO_TAG:
                    jsonArray = new JSONObject(context.getString(R.string.mobile_principato_di_monaco).replace(" ", "")).getJSONArray(tagJsonArray);
                    break;
                case Constants.PaesiTag.MOBILE_SAN_MARINO_TAG:
                    jsonArray = new JSONObject(context.getString(R.string.mobile_san_marino).replace(" ", "")).getJSONArray(tagJsonArray);
                    break;
            }

            for (int i = 0; i < jsonArray.length(); i++) {
                paesi.add(((JSONObject) jsonArray.get(i)).getString("stato"));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return paesi;
    }

    public static JSONObject createPratica() {
        JSONObject jsonObject = new JSONObject();
        try {
            JSONObject jsonPratica = new JSONObject();
            JSONArray pacchetti = new JSONArray();
            pacchetti.put(createPacchetto());
            jsonPratica.put("pacchetti", pacchetti);
            jsonPratica.put("offerteRaggruppate", new JSONArray());
            jsonPratica.put("isFuoriStandard", false);
            jsonPratica.put("inserimentoDati", new JSONObject());
            jsonPratica.put("infoPagamento", new JSONObject());
            jsonPratica.put("offerteConfigurate", new JSONArray());
            jsonPratica.put("addOnMobileAllPacchetti", new JSONArray());

            jsonObject.put("pratica", jsonPratica);
            return jsonObject;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static JSONObject createPacchetto() {
        JSONObject pacchetto = new JSONObject();
        try {
            pacchetto.put("question1Offerta", false);
            pacchetto.put("question1OffertaDevice", false);
            pacchetto.put("question2Smartphone", false);
            pacchetto.put("question2Tablet", false);
            pacchetto.put("question3Apple", false);
            pacchetto.put("question3Windows", false);
            pacchetto.put("question3Samsung", false);
            pacchetto.put("question3Altro", false);
            pacchetto.put("question4Abbonamento", false);
            pacchetto.put("question4Ricaricabile", false);
            pacchetto.put("question4AbbonamentoNumSim", 0);
            pacchetto.put("question4RicaricabileNumSim", 0);
            pacchetto.put("question5Phone", 5);
            pacchetto.put("question5Web", 5);
            pacchetto.put("question6Si", false);
            pacchetto.put("question6No", false);
            pacchetto.put("question7Scelta1", "");
            pacchetto.put("question7Scelta2", "");
            pacchetto.put("completedStep1", false);
            pacchetto.put("offerte", new JSONArray());
            pacchetto.put("completedStep2", false);

            pacchetto.put("completedStep3", false);

            return pacchetto;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static JSONObject createOffertaMobile(OffertaMobile offertaMobile) {
        JSONObject offertaJSONObject = null;
        try {
            offertaJSONObject = new JSONObject();
            offertaJSONObject.put("idOfferta", offertaMobile.getIdOfferta());
            offertaJSONObject.put("offerDescription", offertaMobile.getOfferDescription());
            offertaJSONObject.put("offerDescriptionUpper", offertaMobile.getOfferDescriptionUpper());
            offertaJSONObject.put("offerDescriptionIButton", offertaMobile.getOfferDescriptionIButton());
            offertaJSONObject.put("offerType", offertaMobile.getOfferType());
            offertaJSONObject.put("canoneMensile", offertaMobile.getCanoneMensile());
            offertaJSONObject.put("canoneMensileConv", offertaMobile.getCanoneMensileConv());
            offertaJSONObject.put("canonePromo", offertaMobile.getCanonePromo());
            offertaJSONObject.put("callAzIta", offertaMobile.getCallAzIta());
            offertaJSONObject.put("callIta", offertaMobile.getCallIta());
            offertaJSONObject.put("smsIta", offertaMobile.getSmsIta());
            offertaJSONObject.put("datiIta", offertaMobile.getDatiIta());
            offertaJSONObject.put("callEst", offertaMobile.getCallEst());
            offertaJSONObject.put("smsEst", offertaMobile.getSmsEst());
            offertaJSONObject.put("datiEst", offertaMobile.getDatiEst());
            offertaJSONObject.put("fasciaVoce", offertaMobile.getFasciaVoce());
            offertaJSONObject.put("fasciaDati", offertaMobile.getFasciaDati());
            offertaJSONObject.put("selectedNumSimStep3", offertaMobile.getSelectedNumSimStep3());


            JSONArray addOnMobileJsonArray = new JSONArray();
            if (offertaMobile.getAddOnMobileArrayList() != null) {
                for (AddOnMobile addOnMobile : offertaMobile.getAddOnMobileArrayList()) {
                    JSONObject addOnMobileJO = createAddOnMobile(addOnMobile);
                    addOnMobileJsonArray.put(addOnMobileJO);
                }
            }
            offertaJSONObject.put("addOnMobileList", addOnMobileJsonArray);

            JSONArray terminaliJsonArray = new JSONArray();
            if (offertaMobile.getTerminaleArrayList() != null) {
                for (Terminale terminale : offertaMobile.getTerminaleArrayList()) {
                    JSONObject terminaleJO = createTerminale(terminale);
                    terminaliJsonArray.put(terminaleJO);
                }
            }
            offertaJSONObject.put("terminaliList", terminaliJsonArray);

            JSONArray opzioniMobileJsonArray = new JSONArray();
            if (offertaMobile.getOpzioneMobileArrayList() != null) {
                for (OpzioneMobile opzioneMobile : offertaMobile.getOpzioneMobileArrayList()) {
                    JSONObject opzioneMobileJO = createOpzioneMobile(opzioneMobile);
                    opzioniMobileJsonArray.put(opzioneMobileJO);
                }
            }
            offertaJSONObject.put("opzioniMobileList", opzioniMobileJsonArray);

            JSONArray terminaleBeanJsonArray = new JSONArray();
            if (offertaMobile.getTerminaleBeanArrayList() != null) {
                for (TerminaleBean terminaleBean : offertaMobile.getTerminaleBeanArrayList()) {
                    JSONObject terminaleBeanJO = createTerminaleBean(terminaleBean);
                    terminaleBeanJsonArray.put(terminaleBeanJO);
                }
            }
            offertaJSONObject.put("terminaleBeanList", terminaleBeanJsonArray);

            offertaJSONObject.put("percentualeScontoPenale", offertaMobile.getPercentualeScontoPenale());

            JSONArray percentualeScontoJsonArray = new JSONArray();
            if (offertaMobile.getPercentualeScontoArrayList() != null) {
                for (PercentualeSconto percentualeSconto : offertaMobile.getPercentualeScontoArrayList()) {
                    JSONObject percentualeScontoJO = createPercentualeSconto(percentualeSconto);
                    percentualeScontoJsonArray.put(percentualeScontoJO);
                }
            }
            offertaJSONObject.put("percentualeScontoList", percentualeScontoJsonArray);

            offertaJSONObject.put("selectedNumSimStep4", offertaMobile.getSelectedNumSimStep4());
            offertaJSONObject.put("scontoApplicato", offertaMobile.getScontoApplicato());
            offertaJSONObject.put("canoneScontato", offertaMobile.getCanoneScontato());
            offertaJSONObject.put("autorizzabilita", offertaMobile.getAutorizzabilita());
            offertaJSONObject.put("isScontoPenaleApplicato", offertaMobile.isScontoPenaleApplicato());


            offertaJSONObject.put("mnpStep8", offertaMobile.getMnpStep8());
            if (offertaMobile.getAddOnMobileStep8() != null) {
                offertaJSONObject.put("addOnMobileStep8", createAddOnMobile(offertaMobile.getAddOnMobileStep8()));
            }

            JSONArray opzioneMobileArrayListStep8 = new JSONArray();
            if (offertaMobile.getOpzioneMobileArrayListStep8() != null) {
                for (OpzioneMobile opzioneMobile : offertaMobile.getOpzioneMobileArrayListStep8()) {
                    JSONObject opzioneMobileJO = createOpzioneMobile(opzioneMobile);
                    opzioneMobileArrayListStep8.put(opzioneMobileJO);
                }
            }
            offertaJSONObject.put("opzioneMobileArrayListStep8", opzioneMobileArrayListStep8);

            offertaJSONObject.put("oldICCID", offertaMobile.getOldICCID());
            offertaJSONObject.put("newICCID", offertaMobile.getNewICCID());
            offertaJSONObject.put("oldMSISDN", offertaMobile.getOldMSISDN());

            JSONArray addOnPackageArrayListStep8 = new JSONArray();
            if (offertaMobile.getAddOnPackageArrayListStep8() != null) {
                for (AddOnPackage addOnPackage : offertaMobile.getAddOnPackageArrayListStep8()) {
                    JSONObject addOnPackageJO = createAddOnPackage(addOnPackage);
                    addOnPackageArrayListStep8.put(addOnPackageJO);
                }
            }
            offertaJSONObject.put("addOnPackageArrayListStep8", addOnPackageArrayListStep8);

            JSONArray opzioniMobileSelectedArrayListStep8 = new JSONArray();
            if (offertaMobile.getOpzioniMobileSelectedArrayListStep8() != null) {
                for (OpzioneMobile opzioneMobile : offertaMobile.getOpzioniMobileSelectedArrayListStep8()) {
                    JSONObject opzioneMobileJO = createOpzioneMobile(opzioneMobile);
                    opzioniMobileSelectedArrayListStep8.put(opzioneMobileJO);
                }
            }
            offertaJSONObject.put("opzioniMobileSelectedArrayListStep8", opzioniMobileSelectedArrayListStep8);

            JSONArray terminaliToShow = new JSONArray();
            if (offertaMobile.getTerminaliToShow() != null) {
                for (Terminale terminale : offertaMobile.getTerminaliToShow()) {
                    JSONObject terminaleJO = createTerminale(terminale);
                    terminaliToShow.put(terminaleJO);
                }
            }
            offertaJSONObject.put("terminaliToShow", terminaliToShow);

            if (offertaMobile.getTerminale1() != null) {
                offertaJSONObject.put("terminale1", createTerminale(offertaMobile.getTerminale1()));
            }
            if (offertaMobile.getTerminale2() != null) {
                offertaJSONObject.put("terminale2", createTerminale(offertaMobile.getTerminale2()));
            }

            offertaJSONObject.put("isMnp", offertaMobile.isMnp());

            JSONArray terminaliToShow2 = new JSONArray();
            if (offertaMobile.getTerminaliToShow2() != null) {
                for (Terminale terminale : offertaMobile.getTerminaliToShow2()) {
                    JSONObject terminaleJO = createTerminale(terminale);
                    terminaliToShow2.put(terminaleJO);
                }
            }
            offertaJSONObject.put("terminaliToShow2", terminaliToShow2);

            JSONArray terminaliDiAppoggio = new JSONArray();
            if (offertaMobile.getTerminaliDiAppoggio() != null) {
                for (Terminale terminale : offertaMobile.getTerminaliDiAppoggio()) {
                    JSONObject terminaleJO = createTerminale(terminale);
                    terminaliDiAppoggio.put(terminaleJO);
                }
            }
            offertaJSONObject.put("terminaliDiAppoggio", terminaliDiAppoggio);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return offertaJSONObject;
    }

    public static OffertaMobile createOffertaMobile(JSONObject offerta) {
        OffertaMobile offertaMobile = new OffertaMobile();

        try {
            offertaMobile.setIdOfferta(offerta.getInt("idOfferta"));
            offertaMobile.setOfferDescription(offerta.getString("offerDescription"));
            offertaMobile.setOfferDescriptionUpper(offerta.getString("offerDescriptionUpper"));
            offertaMobile.setOfferDescriptionIButton(offerta.getString("offerDescriptionIButton"));
            offertaMobile.setOfferType(offerta.getString("offerType"));
            offertaMobile.setCanoneMensile(offerta.getString("canoneMensile"));
            offertaMobile.setCanoneMensileConv(offerta.optString("canoneMensileConv"));
            offertaMobile.setCanonePromo(offerta.optString("canonePromo"));
            offertaMobile.setCallAzIta(offerta.optString("callAzIta"));
            offertaMobile.setCallIta(offerta.optString("callIta"));
            offertaMobile.setSmsIta(offerta.optString("smsIta"));
            offertaMobile.setDatiIta(offerta.optString("datiIta"));
            offertaMobile.setCallEst(offerta.optString("callEst"));
            offertaMobile.setSmsEst(offerta.optString("smsEst"));
            offertaMobile.setDatiEst(offerta.optString("datiEst"));
            offertaMobile.setFasciaVoce(offerta.optString("fasciaVoce"));
            offertaMobile.setFasciaDati(offerta.optString("fasciaDati"));
            offertaMobile.setSelectedNumSimStep3(offerta.optInt("selectedNumSimStep3"));

            ArrayList<AddOnMobile> addOnMobileArrayList = new ArrayList<>();
            for (int i = 0; i < offerta.getJSONArray("addOnMobileList").length(); i++) {
                AddOnMobile addOnMobile = createAddOnMobile(offerta.getJSONArray("addOnMobileList").getJSONObject(i));
                addOnMobileArrayList.add(addOnMobile);
            }
            offertaMobile.setAddOnMobileArrayList(addOnMobileArrayList);

            ArrayList<Terminale> terminaliArrayList = new ArrayList<>();
            for (int i = 0; i < offerta.getJSONArray("terminaliList").length(); i++) {
                Terminale terminale = createTerminale(offerta.getJSONArray("terminaliList").getJSONObject(i));
                terminaliArrayList.add(terminale);
            }
            offertaMobile.setTerminaleArrayList(terminaliArrayList);

            ArrayList<OpzioneMobile> opzioneMobileArrayList = new ArrayList<>();
            for (int i = 0; i < offerta.getJSONArray("opzioniMobileList").length(); i++) {
                OpzioneMobile opzioneMobile = createOpzioneMobile(offerta.getJSONArray("opzioniMobileList").getJSONObject(i));
                opzioneMobileArrayList.add(opzioneMobile);
            }
            offertaMobile.setOpzioneMobileArrayList(opzioneMobileArrayList);

            ArrayList<TerminaleBean> terminaleBeanArrayList = new ArrayList<>();
            for (int i = 0; i < offerta.getJSONArray("terminaleBeanList").length(); i++) {
                TerminaleBean terminaleBean = createTerminaleBean(offerta.getJSONArray("terminaleBeanList").getJSONObject(i));
                terminaleBeanArrayList.add(terminaleBean);
            }
            offertaMobile.setTerminaleBeanArrayList(terminaleBeanArrayList);

            offertaMobile.setPercentualeScontoPenale(offerta.optString("percentualeScontoPenale"));

            ArrayList<PercentualeSconto> percentualeScontoArrayList = new ArrayList<>();
            for (int i = 0; i < offerta.getJSONArray("percentualeScontoList").length(); i++) {
                PercentualeSconto percentualeSconto = createPercentualeSconto(offerta.getJSONArray("percentualeScontoList").getJSONObject(i));
                percentualeScontoArrayList.add(percentualeSconto);
            }
            offertaMobile.setPercentualeScontoArrayList(percentualeScontoArrayList);

            offertaMobile.setSelectedNumSimStep4(offerta.optInt("selectedNumSimStep4"));
            offertaMobile.setScontoApplicato(offerta.optString("scontoApplicato"));
            offertaMobile.setCanoneScontato(offerta.optString("canoneScontato"));
            offertaMobile.setAutorizzabilita(offerta.optString("autorizzabilita"));
            offertaMobile.setScontoPenaleApplicato(offerta.optBoolean("isScontoPenaleApplicato"));

            offertaMobile.setMnpStep8(offerta.optString("mnpStep8"));
            if (offerta.optJSONObject("addOnMobileStep8") != null) {
                offertaMobile.setAddOnMobileStep8(createAddOnMobile(offerta.optJSONObject("addOnMobileStep8")));
            }

            ArrayList<OpzioneMobile> opzioneMobileArrayListStep8 = new ArrayList<>();
            for (int i = 0; i < offerta.getJSONArray("opzioneMobileArrayListStep8").length(); i++) {
                OpzioneMobile opzioneMobile = createOpzioneMobile(offerta.getJSONArray("opzioneMobileArrayListStep8").getJSONObject(i));
                opzioneMobileArrayListStep8.add(opzioneMobile);
            }
            offertaMobile.setOpzioneMobileArrayListStep8(opzioneMobileArrayListStep8);

            offertaMobile.setOldICCID(offerta.optString("oldICCID"));
            offertaMobile.setNewICCID(offerta.optString("newICCID"));
            offertaMobile.setOldMSISDN(offerta.optString("oldMSISDN"));

            ArrayList<AddOnPackage> addOnPackageArrayListStep8 = new ArrayList<>();
            for (int i = 0; i < offerta.getJSONArray("addOnPackageArrayListStep8").length(); i++) {
                AddOnPackage addOnPackage = createAddOnPackage(offerta.getJSONArray("addOnPackageArrayListStep8").getJSONObject(i));
                addOnPackageArrayListStep8.add(addOnPackage);
            }
            offertaMobile.setAddOnPackageArrayListStep8(addOnPackageArrayListStep8);

            ArrayList<OpzioneMobile> opzioniMobileSelectedArrayListStep8 = new ArrayList<>();
            for (int i = 0; i < offerta.getJSONArray("opzioniMobileSelectedArrayListStep8").length(); i++) {
                OpzioneMobile opzioneMobile = createOpzioneMobile(offerta.getJSONArray("opzioniMobileSelectedArrayListStep8").getJSONObject(i));
                opzioniMobileSelectedArrayListStep8.add(opzioneMobile);
            }
            offertaMobile.setOpzioniMobileSelectedArrayListStep8(opzioniMobileSelectedArrayListStep8);

            ArrayList<Terminale> terminaliToShow = new ArrayList<>();
            for (int i = 0; i < offerta.getJSONArray("terminaliToShow").length(); i++) {
                Terminale terminale = createTerminale(offerta.getJSONArray("terminaliToShow").getJSONObject(i));
                terminaliToShow.add(terminale);
            }
            offertaMobile.setTerminaliToShow(terminaliToShow);

            if (offerta.optJSONObject("terminale1") != null) {
                offertaMobile.setTerminale1(createTerminale(offerta.optJSONObject("terminale1")));
            }
            if (offerta.optJSONObject("terminale2") != null) {
                offertaMobile.setTerminale2(createTerminale(offerta.optJSONObject("terminale2")));
            }
            offertaMobile.setMnp(offerta.optBoolean("isMnp"));

            ArrayList<Terminale> terminaliToShow2 = new ArrayList<>();
            for (int i = 0; i < offerta.getJSONArray("terminaliToShow2").length(); i++) {
                Terminale terminale = createTerminale(offerta.getJSONArray("terminaliToShow2").getJSONObject(i));
                terminaliToShow2.add(terminale);
            }
            offertaMobile.setTerminaliToShow2(terminaliToShow2);

            ArrayList<Terminale> terminaliDiAppoggio = new ArrayList<>();
            for (int i = 0; i < offerta.getJSONArray("terminaliDiAppoggio").length(); i++) {
                Terminale terminale = createTerminale(offerta.getJSONArray("terminaliDiAppoggio").getJSONObject(i));
                terminaliDiAppoggio.add(terminale);
            }
            offertaMobile.setTerminaliDiAppoggio(terminaliDiAppoggio);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return offertaMobile;
    }

    public static JSONObject createTerminale(Terminale terminale) {
        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("idTerminale", terminale.getIdTerminale());
            jsonObject.put("oem", terminale.getOem());
            jsonObject.put("terminaleBrand", terminale.getTerminaleBrand());
            jsonObject.put("terminaleDescription", terminale.getTerminaleDescription());
            jsonObject.put("mnp", terminale.getMnp());
            jsonObject.put("offerType", terminale.getOfferType());
            jsonObject.put("deviceType", terminale.getDeviceType());
            jsonObject.put("prezzoListino", terminale.getPrezzoListino());
            jsonObject.put("rataIniziale", terminale.getRataIniziale());
            jsonObject.put("rataMensile", terminale.getRataMensile());
            jsonObject.put("rataFinale", terminale.getRataFinale());
            jsonObject.put("imageUrl", terminale.getImageUrl());
            jsonObject.put("idOfferta", terminale.getIdOfferta());
            jsonObject.put("selectedNumSimStep3", terminale.getSelectedNumSimStep3());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonObject;
    }

    public static Terminale createTerminale(JSONObject jsonObject) {
        Terminale terminale = new Terminale();

        try {
            terminale.setIdTerminale(jsonObject.getString("idTerminale"));
            terminale.setOem(jsonObject.getString("oem"));
            terminale.setTerminaleBrand(jsonObject.getString("terminaleBrand"));
            terminale.setTerminaleDescription(jsonObject.getString("terminaleDescription"));
            terminale.setMnp(jsonObject.getString("mnp"));
            terminale.setOfferType(jsonObject.getString("offerType"));
            terminale.setDeviceType(jsonObject.getString("deviceType"));
            terminale.setPrezzoListino(jsonObject.getString("prezzoListino"));
            terminale.setRataIniziale(jsonObject.getString("rataIniziale"));
            terminale.setRataMensile(jsonObject.getString("rataMensile"));
            terminale.setRataFinale(jsonObject.getString("rataFinale"));
            terminale.setImageUrl(jsonObject.getString("imageUrl"));
            terminale.setIdOfferta(jsonObject.getString("idOfferta"));
            terminale.setSelectedNumSimStep3(jsonObject.optInt("selectedNumSimStep3"));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return terminale;
    }

    public static JSONObject createAddOnMobile(AddOnMobile addOnMobile) {
        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("idOfferta", addOnMobile.getIdOfferta());
            jsonObject.put("descFascia", addOnMobile.getDescFascia());
            jsonObject.put("prezzoFascia", addOnMobile.getPrezzoFascia());
            jsonObject.put("maxSimVendibile", addOnMobile.getMaxSimVendibile());
            jsonObject.put("selectedNumSimStep3", addOnMobile.getSelectedNumSimStep3());
            jsonObject.put("selectedNumSimStep8", addOnMobile.getSelectedNumSimStep8());
            jsonObject.put("selectedLabelStep8", addOnMobile.getSelectedLabelStep8());
            //MW Aggiunto campo per salvare Sconto Giga
            jsonObject.put("scontoApplicato", addOnMobile.getScontoGigaApplicato());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonObject;
    }

    public static AddOnMobile createAddOnMobile(JSONObject jsonObject) {
        AddOnMobile addOnMobile = new AddOnMobile();

        try {
            addOnMobile.setIdOfferta(jsonObject.getString("idOfferta"));
            addOnMobile.setDescFascia(jsonObject.getString("descFascia"));
            addOnMobile.setPrezzoFascia(jsonObject.getString("prezzoFascia"));
            addOnMobile.setMaxSimVendibile(jsonObject.getString("maxSimVendibile"));
            addOnMobile.setSelectedNumSimStep3(jsonObject.optInt("selectedNumSimStep3"));
            addOnMobile.setSelectedNumSimStep8(jsonObject.optInt("selectedNumSimStep8"));
            addOnMobile.setSelectedLabelStep8(jsonObject.optString("selectedLabelStep8"));
            //MW aggiunto campo per gestire sconto Giga
            addOnMobile.setScontoGigaApplicato(jsonObject.optString("scontoApplicato"));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return addOnMobile;
    }

    public static JSONObject createOpzioneMobile(OpzioneMobile opzioneMobile) {
        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("idOpzione", opzioneMobile.getIdOpzione());
            jsonObject.put("offerType", opzioneMobile.getOfferType());
            jsonObject.put("opzioneDesc", opzioneMobile.getOpzioneDesc());
            jsonObject.put("opzioneDescUpper", opzioneMobile.getOpzioneDescUpper());
            jsonObject.put("opzioneDescriptionIButton", opzioneMobile.getOpzioneDescriptionIButton());
            jsonObject.put("idOfferta", opzioneMobile.getIdOfferta());
            jsonObject.put("canoneMensile", opzioneMobile.getCanoneMensile());
            jsonObject.put("canoneSettimanale", opzioneMobile.getCanoneSettimanale());
            jsonObject.put("canoneBimestrale", opzioneMobile.getCanoneBimestrale());
            jsonObject.put("voce", opzioneMobile.getVoce());
            jsonObject.put("sms", opzioneMobile.getSms());
            jsonObject.put("dati", opzioneMobile.getDati());
            jsonObject.put("scontoConvergenza", opzioneMobile.getScontoConvergenza());
            jsonObject.put("costoUnaTantum", opzioneMobile.getCostoUnaTantum());
            jsonObject.put("isDefault", opzioneMobile.getIsDefault());
            jsonObject.put("isDoubleChecked", opzioneMobile.getIsDoubleChecked());
            jsonObject.put("selectedNumSimStep3", opzioneMobile.getSelectedNumSimStep3());
            jsonObject.put("value", opzioneMobile.getValue());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonObject;
    }

    public static OpzioneMobile createOpzioneMobile(JSONObject jsonObject) {
        OpzioneMobile opzioneMobile = new OpzioneMobile();

        try {
            opzioneMobile.setIdOpzione(jsonObject.getString("idOpzione"));
            opzioneMobile.setOfferType(jsonObject.getString("offerType"));
            opzioneMobile.setOpzioneDesc(jsonObject.getString("opzioneDesc"));
            opzioneMobile.setOpzioneDescUpper(jsonObject.getString("opzioneDescUpper"));
            opzioneMobile.setOpzioneDescriptionIButton(jsonObject.getString("opzioneDescriptionIButton"));
            opzioneMobile.setIdOfferta(jsonObject.getString("idOfferta"));
            opzioneMobile.setCanoneMensile(jsonObject.getString("canoneMensile"));
            opzioneMobile.setCanoneSettimanale(jsonObject.getString("canoneSettimanale"));
            opzioneMobile.setCanoneBimestrale(jsonObject.getString("canoneBimestrale"));
            opzioneMobile.setVoce(jsonObject.getString("voce"));
            opzioneMobile.setSms(jsonObject.getString("sms"));
            opzioneMobile.setDati(jsonObject.getString("dati"));
            opzioneMobile.setScontoConvergenza(jsonObject.getString("scontoConvergenza"));
            opzioneMobile.setCostoUnaTantum(jsonObject.getString("costoUnaTantum"));
            opzioneMobile.setIsDefault(jsonObject.getString("isDefault"));
            opzioneMobile.setIsDoubleChecked(jsonObject.getString("isDoubleChecked"));
            opzioneMobile.setSelectedNumSimStep3(jsonObject.optInt("selectedNumSimStep3"));
            opzioneMobile.setValue(jsonObject.getString("value"));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return opzioneMobile;
    }

    public static JSONObject createTerminaleBean(TerminaleBean terminaleBean) {
        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("idTerminale", terminaleBean.getIdTerminale());
            jsonObject.put("selectedNumSimStep3Mnp", terminaleBean.getSelectedNumSimStep3Mnp());
            jsonObject.put("selectedNumSimStep3NuovaLinea", terminaleBean.getSelectedNumSimStep3NuovaLinea());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonObject;
    }

    public static TerminaleBean createTerminaleBean(JSONObject jsonObject) {
        TerminaleBean terminaleBean = new TerminaleBean();

        try {
            terminaleBean.setIdTerminale(jsonObject.getString("idTerminale"));
            terminaleBean.setSelectedNumSimStep3Mnp(jsonObject.getInt("selectedNumSimStep3Mnp"));
            terminaleBean.setSelectedNumSimStep3NuovaLinea(jsonObject.getInt("selectedNumSimStep3NuovaLinea"));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return terminaleBean;
    }

    public static JSONObject createPercentualeSconto(PercentualeSconto percentualeSconto) {
        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("percentualeSconto", percentualeSconto.getPercentualeSconto());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonObject;
    }

    public static PercentualeSconto createPercentualeSconto(JSONObject jsonObject) {
        PercentualeSconto percentualeSconto = new PercentualeSconto();

        try {
            percentualeSconto.setPercentualeSconto(jsonObject.getString("percentualeSconto"));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return percentualeSconto;
    }

    public static JSONObject createAddOnPackage(AddOnPackage addOnPackage) {
        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("desc", addOnPackage.getDesc());

            JSONArray addOnMobileArrayList = new JSONArray();
            if (addOnPackage.getAddOnMobileArrayList() != null) {
                for (AddOnMobile addOnMobile : addOnPackage.getAddOnMobileArrayList()) {
                    JSONObject addOnMobileJO = createAddOnMobile(addOnMobile);
                    addOnMobileArrayList.put(addOnMobileJO);
                }
            }
            jsonObject.put("addOnMobileArrayList", addOnMobileArrayList);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonObject;
    }

    public static AddOnPackage createAddOnPackage(JSONObject jsonObject) {
        AddOnPackage addOnPackage = new AddOnPackage();

        try {
            addOnPackage.setDesc(jsonObject.getString("desc"));

            ArrayList<AddOnMobile> addOnMobileArrayList = new ArrayList<>();
            for (int i = 0; i < jsonObject.getJSONArray("addOnMobileArrayList").length(); i++) {
                AddOnMobile addOnMobile = createAddOnMobile(jsonObject.getJSONArray("addOnMobileArrayList").getJSONObject(i));
                addOnMobileArrayList.add(addOnMobile);
            }
            addOnPackage.setAddOnMobileArrayList(addOnMobileArrayList);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return addOnPackage;
    }

    public static JSONObject createInserimentoDati() {
        JSONObject inserimentoDati = new JSONObject();
        try {
            JSONObject datiCliente = new JSONObject();
            datiCliente.put("indirizzoSedeLegale", "");
            datiCliente.put("civico", "");
            datiCliente.put("p_iva", "");
            datiCliente.put("provinciaCCIAA", "");
            datiCliente.put("cognome", "");
            datiCliente.put("cf", "");
            datiCliente.put("provincia", "");
            datiCliente.put("ragioneSociale", "");
            datiCliente.put("nome", "");
            datiCliente.put("citta", "");
            datiCliente.put("firmaSocietaria", "");
            datiCliente.put("cap", "");
            inserimentoDati.put("datiCliente", datiCliente);

            JSONObject datiRappresentanteLegale = new JSONObject();
            datiRappresentanteLegale.put("numDocIdentita", "");
            datiRappresentanteLegale.put("docIdentita", "");
            datiRappresentanteLegale.put("cf", "");
            datiRappresentanteLegale.put("cognome", "");
            datiRappresentanteLegale.put("telefono", "");
            datiRappresentanteLegale.put("provincia", "");
            datiRappresentanteLegale.put("dataDoc", "");
            datiRappresentanteLegale.put("nome", "");
            datiRappresentanteLegale.put("fax", "");
            datiRappresentanteLegale.put("luogoNascita", "");
            datiRappresentanteLegale.put("dataNascita", "");
            datiRappresentanteLegale.put("isCopiaCliente", "");
            datiRappresentanteLegale.put("nazionalita", "");
            datiRappresentanteLegale.put("email", "");
            datiRappresentanteLegale.put("rilasciatoDa", "");
            datiRappresentanteLegale.put("gender", "");
            inserimentoDati.put("datiRappresentanteLegale", datiRappresentanteLegale);

            JSONObject datiSede = new JSONObject();
            datiSede.put("indirizzo", "");
            datiSede.put("citta", "");
            datiSede.put("riferimentoSede", "");
            datiSede.put("provincia", "");
            datiSede.put("civico", "");
            datiSede.put("cap", "");
            inserimentoDati.put("datiSede", datiSede);

            JSONObject referenteContratto = new JSONObject();
            referenteContratto.put("fax", "");
            referenteContratto.put("cognome", "");
            referenteContratto.put("dettaglioChiamate", "");
            referenteContratto.put("email", "");
            referenteContratto.put("nome", "");
            referenteContratto.put("cellulare", "");
            referenteContratto.put("modFatturazione", "");
            referenteContratto.put("telefono", "");
            referenteContratto.put("agevolazioniFiscali", "");
            inserimentoDati.put("referenteContratto", referenteContratto);

            JSONObject modInvioFattura = new JSONObject();
            modInvioFattura.put("cartaceo", "");
            modInvioFattura.put("elettronico", "");
            inserimentoDati.put("modInvioFattura", modInvioFattura);

            JSONObject documentazioneCosti = new JSONObject();
            documentazioneCosti.put("pubblicaAmministrazione", "");
            documentazioneCosti.put("areaClienti", "");
            documentazioneCosti.put("supportoCartaceo", "");
            documentazioneCosti.put("supportoCD", "");
            inserimentoDati.put("documentazioneCosti", documentazioneCosti);

            JSONObject pubblicaAmministrazione = new JSONObject();
            pubblicaAmministrazione.put("codiceIdentificativoUfficio", "");
            pubblicaAmministrazione.put("splitPayment", "");
            pubblicaAmministrazione.put("modFattElettronica", "");
            pubblicaAmministrazione.put("isSelected", "");
            inserimentoDati.put("pubblicaAmministrazione", pubblicaAmministrazione);

            return inserimentoDati;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static JSONObject createPagamento() {
        JSONObject pagamento = new JSONObject();
        try {
            pagamento.put("pagamentoTramiteRid", "");

            JSONObject estremiContoCorrente = new JSONObject();
            estremiContoCorrente.put("iban", "");
            estremiContoCorrente.put("nomeBancaPosta", "");
            estremiContoCorrente.put("agenziaUfficioFiliale", "");
            pagamento.put("estremiContoCorrente", estremiContoCorrente);

            JSONObject sottoscrittoreDelModulo = new JSONObject();
            sottoscrittoreDelModulo.put("nome", "");
            sottoscrittoreDelModulo.put("cognome", "");
            sottoscrittoreDelModulo.put("gender", "");
            sottoscrittoreDelModulo.put("dataDiNascita", "");
            sottoscrittoreDelModulo.put("luogoDiNascita", "");
            sottoscrittoreDelModulo.put("cf", "");
            sottoscrittoreDelModulo.put("indirizzo", "");
            sottoscrittoreDelModulo.put("civico", "");
            sottoscrittoreDelModulo.put("citta", "");
            sottoscrittoreDelModulo.put("provincia", "");
            sottoscrittoreDelModulo.put("cap", "");
            pagamento.put("sottoscrittoreDelModulo", sottoscrittoreDelModulo);

            JSONObject intestatarioConto = new JSONObject();
            intestatarioConto.put("nome", "");
            intestatarioConto.put("cognome", "");
            intestatarioConto.put("ragioneSociale", "");
            intestatarioConto.put("cf", "");
            intestatarioConto.put("indirizzo", "");
            intestatarioConto.put("civico", "");
            intestatarioConto.put("citta", "");
            intestatarioConto.put("provincia", "");
            intestatarioConto.put("cap", "");
            pagamento.put("intestatarioConto", intestatarioConto);

            JSONObject addebitoDiretto = new JSONObject();
            addebitoDiretto.put("identificativoMandato", "");
            pagamento.put("addebitoDiretto", addebitoDiretto);

            pagamento.put("pagamentoTramiteCarta", "");

            JSONObject autorizzazionePermanenteDiAddebito = new JSONObject();
            autorizzazionePermanenteDiAddebito.put("cartaSi", "");
            autorizzazionePermanenteDiAddebito.put("visaMastercard", "");
            autorizzazionePermanenteDiAddebito.put("diners", "");
            autorizzazionePermanenteDiAddebito.put("americanExpress", "");
            autorizzazionePermanenteDiAddebito.put("deutscheCreditCard", "");
            autorizzazionePermanenteDiAddebito.put("numero", "");
            autorizzazionePermanenteDiAddebito.put("scadenza", "");
            pagamento.put("autorizzazionePermanenteDiAddebito", autorizzazionePermanenteDiAddebito);


            return pagamento;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getZonaByArea(Context context, String area) {
        String zona = "";

        switch (area) {
            case Constants.PaesiName.MOBILE_ZONA_UE_TAG:
                zona = "Zona UE";
                break;
            case Constants.PaesiName.MOBILE_AREA_1_TAG:
                zona = "Area1";
                break;
            case Constants.PaesiName.MOBILE_USA_TAG:
                zona = "USA";
                break;
            case Constants.PaesiName.MOBILE_PRINCIPATO_DI_MONACO_TAG:
                zona = "Principato di Monaco";
                break;
            case Constants.PaesiName.MOBILE_SAN_MARINO_TAG:
                zona = "San Marino";
                break;
        }

        return zona;
    }

    public static String getZonaByState(Context context, String state) {
        String zona = "";

        ArrayList<ArrayList<String>> listePaesi;
        listePaesi = new ArrayList<ArrayList<String>>();
        listePaesi.add(Utils.getListaPaesiByTag(context, Constants.PaesiTag.MOBILE_ZONA_UE_TAG));
        listePaesi.add(Utils.getListaPaesiByTag(context, Constants.PaesiTag.MOBILE_AREA_1_TAG));
        listePaesi.add(Utils.getListaPaesiByTag(context, Constants.PaesiTag.MOBILE_USA_TAG));
        listePaesi.add(Utils.getListaPaesiByTag(context, Constants.PaesiTag.MOBILE_PRINCIPATO_DI_MONACO_TAG));
        listePaesi.add(Utils.getListaPaesiByTag(context, Constants.PaesiTag.MOBILE_SAN_MARINO_TAG));

        for (int i = 0; i < listePaesi.size(); i++) {
            if (listePaesi.get(i).contains(state)) {
                switch (i) {
                    case 0:
                        zona = "Zona UE";
                        break;
                    case 1:
                        zona = "Area1";
                        break;
                    case 2:
                        zona = "USA";
                        break;
                    case 3:
                        zona = "Principato di Monaco";
                        break;
                    case 4:
                        zona = "San Marino";
                        break;
                }
            }
        }
        return zona;
    }

    public static void showInfoDialog(Activity activity, Object object) {
        InfoDialogFragment infoDialogFragment = new InfoDialogFragment();
//        infoDialogFragment.setCancelable(false);
        Bundle bundle = new Bundle();
        if (object instanceof OffertaMobile) {
            bundle.putSerializable(InfoDialogFragment.KEY, (OffertaMobile) object);
        } else if (object instanceof OpzioneMobile) {
            bundle.putSerializable(InfoDialogFragment.KEY, (OpzioneMobile) object);
        }
        infoDialogFragment.setArguments(bundle);
        infoDialogFragment.show(activity.getFragmentManager(), "info_fragment_dialog");
    }

    public static void showScontiDialog(Activity activity, int type, MobileInternetCreateOfferStep4Fragment mobileInternetCreateOfferStep4Fragment) {
        ScontiDialogFragment scontiDialogFragment = new ScontiDialogFragment(mobileInternetCreateOfferStep4Fragment);
        scontiDialogFragment.setCancelable(false);
        Bundle bundle = new Bundle();
        bundle.putInt(ScontiDialogFragment.KEY, type);
        scontiDialogFragment.setArguments(bundle);
        scontiDialogFragment.show(activity.getFragmentManager(), "sconti_fragment_dialog");
    }

    public static void showScontiDialog(Activity activity, int type, it.wind.smartsales.fragments.selectoffer.MobileInternetSelectOfferStep4Fragment mobileInternetCreateOfferStep4Fragment) {
        SelectScontiDialogFragment scontiDialogFragment = new SelectScontiDialogFragment(mobileInternetCreateOfferStep4Fragment);
        scontiDialogFragment.setCancelable(false);
        Bundle bundle = new Bundle();
        bundle.putInt(ScontiDialogFragment.KEY, type);
        scontiDialogFragment.setArguments(bundle);
        scontiDialogFragment.show(activity.getFragmentManager(), "sconti_fragment_dialog");
    }

    public static void showMailDialog(Activity activity, MobileInternetCreateOfferStep5Fragment mobileInternetCreateOfferStep5Fragment) {
        MailDialogFragment mailDialogFragment = new MailDialogFragment(mobileInternetCreateOfferStep5Fragment);
        mailDialogFragment.setCancelable(false);
        mailDialogFragment.show(activity.getFragmentManager(), "mail_fragment_dialog");
    }

    public static void showMailDialog(Activity activity, MobileInternetSelectOfferStep5Fragment mobileInternetCreateOfferStep5Fragment) {
        SelectMailDialogFragment mailDialogFragment = new SelectMailDialogFragment(mobileInternetCreateOfferStep5Fragment);
        mailDialogFragment.setCancelable(false);
        mailDialogFragment.show(activity.getFragmentManager(), "mail_fragment_dialog");
    }


    public static void showNoteDialog(Activity activity, String note) {
        NoteDialogFragment noteDialogFragment = new NoteDialogFragment(note);
        noteDialogFragment.setCancelable(false);
        noteDialogFragment.show(activity.getFragmentManager(), "note_fragment_dialog");
    }

    public static boolean checkCodiceFiscale(String codiceFiscale) {
        int i, s;
        String validi, set1, set2, setpari, setdisp;

        if (codiceFiscale == "") {
            return false;
        }
        codiceFiscale = codiceFiscale.toUpperCase();
        if (codiceFiscale.length() != 16) {
            return false;
        }

        validi = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

        for (i = 0; i < 16; i++) {
            if (validi.indexOf(codiceFiscale.charAt(i)) == -1)
                return false;
        }
        set1 = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        set2 = "ABCDEFGHIJABCDEFGHIJKLMNOPQRSTUVWXYZ";
        setpari = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        setdisp = "BAKPLCQDREVOSFTGUHMINJWZYX";
        s = 0;

        for (i = 1; i <= 13; i += 2) {
            s += setpari.indexOf(set2.charAt(set1.indexOf(codiceFiscale.charAt(i))));
        }

        for (i = 0; i <= 14; i += 2) {
            s += setdisp.indexOf(set2.charAt(set1.indexOf(codiceFiscale.charAt(i))));
        }

        if (s % 26 != codiceFiscale.charAt(15) - "A".charAt(0)) {
            return false;
        }

        return true;
    }

    public static boolean checkIban(String ibanStr) {
        if (ibanStr.length() < 5) {
            return false;
        }
        int k, r, i;
        String ibanCut = ibanStr.substring(4) + ibanStr.substring(0, 4);
        for (i = 0, r = 0; i < ibanCut.length(); i++) {
            char c = ibanCut.charAt(i);
            if (48 <= c && c <= 57) {
                if (i == ibanCut.length() - 4 || i == ibanCut.length() - 3) {
                    return false;
                }
                k = c - 48;
            } else if (65 <= c && c <= 90) {
                if (i == ibanCut.length() - 2 || i == ibanCut.length() - 1) {
                    return false;
                }
                k = c - 55;
            } else {
                return false;
            }
            if (k > 9)
                r = (100 * r + k) % 97;
            else
                r = (10 * r + k) % 97;
        }
        if (r != 1) {
            return false;
        }
        return true;
    }

    public static void showMnpDialog(Activity activity, MobileInternetCreateOfferStep8Fragment mobileInternetCreateOfferStep8Fragment, OffertaMobile offertaMobile) {
        MnpDialogFragment mnpDialogFragment = new MnpDialogFragment(mobileInternetCreateOfferStep8Fragment, offertaMobile);
        mnpDialogFragment.setCancelable(false);
//        Bundle bundle = new Bundle();
//        bundle.putInt(ScontiDialogFragment.KEY, type);
//        scontiDialogFragment.setArguments(bundle);
        mnpDialogFragment.show(activity.getFragmentManager(), "mnp_fragment_dialog");
    }

    public static void showMnpDialog(Activity activity, MobileInternetSelectOfferStep8Fragment mobileInternetCreateOfferStep8Fragment, OffertaMobile offertaMobile) {
        SelectMnpDialogFragment mnpDialogFragment = new SelectMnpDialogFragment(mobileInternetCreateOfferStep8Fragment, offertaMobile);
        mnpDialogFragment.setCancelable(false);
        mnpDialogFragment.show(activity.getFragmentManager(), "mnp_fragment_dialog");
    }

    public static void showGbDialog(Activity activity, MobileInternetCreateOfferStep8Fragment mobileInternetCreateOfferStep8Fragment, OffertaMobile offertaMobile) {
        GbDialogFragment gbDialogFragment = new GbDialogFragment(mobileInternetCreateOfferStep8Fragment, offertaMobile);
        gbDialogFragment.setCancelable(false);
//        Bundle bundle = new Bundle();
//        bundle.putInt(ScontiDialogFragment.KEY, type);
//        scontiDialogFragment.setArguments(bundle);
        gbDialogFragment.show(activity.getFragmentManager(), "gb_fragment_dialog");
    }

    public static void showExtraGbDialog(Activity activity, MobileInternetCreateOfferStep8Fragment mobileInternetCreateOfferStep8Fragment, ArrayList<ArrayList<ExtraGBDetail>> extraGigaDetail) {
        ExtraGbDialogFragment gbDialogFragment = new ExtraGbDialogFragment(mobileInternetCreateOfferStep8Fragment, extraGigaDetail);
        gbDialogFragment.setCancelable(false);
//        Bundle bundle = new Bundle();
//        bundle.putInt(ScontiDialogFragment.KEY, type);
//        scontiDialogFragment.setArguments(bundle);
        gbDialogFragment.show(activity.getFragmentManager(), "extra_gb_fragment_dialog");
    }

    public static void showGbDialog(Activity activity, MobileInternetSelectOfferStep8Fragment mobileInternetCreateOfferStep8Fragment, OffertaMobile offertaMobile) {
        SelectGbDialogFragment gbDialogFragment = new SelectGbDialogFragment(mobileInternetCreateOfferStep8Fragment, offertaMobile);
        gbDialogFragment.setCancelable(false);
        gbDialogFragment.show(activity.getFragmentManager(), "gb_fragment_dialog");
    }

    public static void showDeviceDialog(Activity activity, MobileInternetCreateOfferStep8Fragment mobileInternetCreateOfferStep8Fragment, OffertaMobile offertaMobile, int listNumber) {
        DeviceDialogFragment deviceDialogFragment = new DeviceDialogFragment(mobileInternetCreateOfferStep8Fragment, offertaMobile, listNumber);
        deviceDialogFragment.setCancelable(true);
        deviceDialogFragment.show(activity.getFragmentManager(), "device_fragment_dialog");
    }

    public static void showDeviceDialog(Activity activity, MobileInternetSelectOfferStep8Fragment mobileInternetCreateOfferStep8Fragment, OffertaMobile offertaMobile, int listNumber) {
        SelectDeviceDialogFragment deviceDialogFragment = new SelectDeviceDialogFragment(mobileInternetCreateOfferStep8Fragment, offertaMobile, listNumber);
        deviceDialogFragment.setCancelable(true);
        deviceDialogFragment.show(activity.getFragmentManager(), "device_fragment_dialog");
    }

    public static int getPlaceholderBasedOnOem(String oem) {
        switch (oem.toLowerCase()) {
            case "apple":
                return R.drawable.placeholder_apple;
            case "windows phone":
                return R.drawable.placeholder_windows;
            case "samsung":
                return R.drawable.placeholder_samsung;
            default:
                return R.drawable.placeholder_altro;
        }
    }
}
