package it.wind.smartsales.commons;

import android.view.View;

public abstract class SelectedOnClickListener implements View.OnClickListener {
    abstract public void performClick(View view);

    @Override
    public void onClick(View view) {
        view.setSelected(!view.isSelected());
        performClick(view);
    }
}