package it.wind.smartsales.commons;

/**
 * Created by luca.quaranta on 15/03/2016.
 */
public class Log {

    public static void d(String tag, String message) {
        if (Constants.URLS.BASE_URL.compareToIgnoreCase(Constants.baseUrlProd) != 0) {
            android.util.Log.d(tag, message);
        }
    }

    public static void e(String tag, String message) {
        if (Constants.URLS.BASE_URL.compareToIgnoreCase(Constants.baseUrlProd) != 0) {
            android.util.Log.e(tag, message);
        }
    }

    public static void i(String tag, String message) {
        if (Constants.URLS.BASE_URL.compareToIgnoreCase(Constants.baseUrlProd) != 0) {
            android.util.Log.i(tag, message);
        }
    }

    public static void v(String tag, String message) {
        if (Constants.URLS.BASE_URL.compareToIgnoreCase(Constants.baseUrlProd) != 0) {
            android.util.Log.v(tag, message);
        }
    }

}
