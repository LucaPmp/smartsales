package it.wind.smartsales;

import android.app.Application;
import android.graphics.Bitmap;
import android.util.LruCache;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;

import it.wind.smartsales.commons.Constants;
import it.wind.smartsales.dao.CataloghiCheckVersionDAO;
import it.wind.smartsales.dao.DbHelper;

/**
 * Created by luca.quaranta on 15/03/2016.
 */
public class SmartSalesApplication extends Application {
    private static SmartSalesApplication mInstance;
    private RequestQueue mRequestQueue;
    private ImageLoader mImageLoader;

    public static SmartSalesApplication getInstance() {
        if (mInstance == null) {
            mInstance = new SmartSalesApplication();
        }
        return mInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;

        Constants.setEnvironment(Constants.Environments.STUB);
//        Crittercism.initialize(getApplicationContext(), "730ad78aa4f04646b8a5c415a879389d00555300");
        DbHelper.getInstance(getApplicationContext());
        initializeTable();

        mRequestQueue = Volley.newRequestQueue(this);
        mImageLoader = new ImageLoader(this.mRequestQueue, new ImageLoader.ImageCache() {
            private final LruCache<String, Bitmap> mCache = new LruCache<>(10);

            public void putBitmap(String url, Bitmap bitmap) {
                mCache.put(url, bitmap);
            }

            public Bitmap getBitmap(String url) {
                return mCache.get(url);
            }
        });
    }

    private void initializeTable() {
        CataloghiCheckVersionDAO.insertCatalogCheckVersion(getApplicationContext(), Constants.CatalogIdentifiers.CATALOGO_OFFERTE_FISSO);
        CataloghiCheckVersionDAO.insertCatalogCheckVersion(getApplicationContext(), Constants.CatalogIdentifiers.CATALOGO_OFFERTE_MOBILE);
        CataloghiCheckVersionDAO.insertCatalogCheckVersion(getApplicationContext(), Constants.CatalogIdentifiers.CATALOGO_SCONTI);
        CataloghiCheckVersionDAO.insertCatalogCheckVersion(getApplicationContext(), Constants.CatalogIdentifiers.CATALOGO_TERMINALI);
        CataloghiCheckVersionDAO.insertCatalogCheckVersion(getApplicationContext(), Constants.CatalogIdentifiers.CATALOGO_QUESTIONARIO);
        CataloghiCheckVersionDAO.insertCatalogCheckVersion(getApplicationContext(), Constants.CatalogIdentifiers.CATALOGO_6);
        CataloghiCheckVersionDAO.insertCatalogCheckVersion(getApplicationContext(), Constants.CatalogIdentifiers.CATALOGO_SCONTI_RICARICABILI);
    }

    public RequestQueue getRequestQueue() {
        return this.mRequestQueue;
    }

    public ImageLoader getImageLoader() {
        return this.mImageLoader;
    }
}